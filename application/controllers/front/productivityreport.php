<?php
class productivityreport extends CI_Controller {

	public function __construct() {
		parent::__construct();
		$this->load->library(['session']); 
		$this->load->model('myAccount_model');
		$this->load->model('User_model');
		$this->load->helper('url_helper');
	}

	public function index(){


		if ($this->session->userdata('front_logged_in')) {
            $session_data = $this->session->userdata('front_logged_in');
            $data['user_id'] = $session_data['user_id'];
            $data['user_name'] = $session_data['user_name'];
            $user_id = $session_data['user_id'];
            $this->session->set_userdata('previous_url', '');

            $data['user_details'] = $this->myAccount_model->get_my_data($data['user_id']);

			$this->load->view('front/productivityreport', $data);		
         
        } else {
        	$this->session->set_userdata('previous_url', current_url());
        	redirect('/');
        }
		
	}



}

?>