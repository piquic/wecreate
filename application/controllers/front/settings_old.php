<?php 
class Settings extends CI_Controller{
 
    function __construct(){
        parent::__construct();
        $this->load->library(['session']); 
        $this->load->helper(['url','file','form']); 
        $this->load->model('Settings_model'); //load model upload 
        $this->load->model('Dashboard_model'); //load model upload 
        $this->load->model('Client_model');
        $this->load->model('User_model');
        $this->load->library('upload'); //load library upload 
        $this->load->model('Category_model');
    }

    public function index(){

        if ($this->session->userdata('front_logged_in')) {
            $session_data = $this->session->userdata('front_logged_in');
            $data['user_id'] = $session_data['user_id'];
            $data['user_type_id'] = $session_data['user_type_id'];
            $data['user_name'] = $session_data['user_name'];
            $type1 = $session_data['user_type_id'];

            $user_id= $session_data['user_id'];
            $checkquerys = $this->db->query("select * from wc_users where  user_id='$user_id' ")->result_array();

            $client_id = $checkquerys[0]['client_id'];
            $brief_permission_details=$this->db->query("select * from wc_users where client_id=$client_id and user_type_id='3'")->result_array();
            $user_permission=$brief_permission_details[0]['user_permission']; 
            $category_permission=$brief_permission_details[0]['category_permission']; 

            if($type1=='2' || ( $type1=='3' && $user_permission=='1')){
                $data['title'] = "Settings";
                $data['product_details']=$this->Settings_model->get_product_list();
                //$data['get_user_list']=$this->Settings_model->get_user_list();
              
                $user_id= $session_data['user_id'];
                $users_details=$this->db->query("select * from wc_users where user_id='$user_id'")->result_array();
                $client_id=$users_details[0]['client_id'];
                $data['users_details1']=$this->db->query("select * from wc_users where (user_type_id!='1' and user_type_id!='2' ) and deleted='0' and client_id in ( ".trim($client_id,",")." ) OR client_id LIKE '%,".trim($client_id,",").",%'  order by  user_id asc ")->result_array();

                $data['retainer_details1']=$this->db->query("select * from wc_retainer where deleted='0' and client_id ='$client_id'  order by  retainer_id asc ")->result_array();

                $data['category_details1']=$this->db->query("select * from wc_category where deleted='0' and client_id ='$client_id'  order by  category_id asc")->result_array();

                //print_r("select * from wc_users where (user_type_id!='1' and user_type_id!='2' ) and deleted='0' and client_id in ( ".trim($client_id,",")." ) OR client_id LIKE '%,".trim($client_id,",").",%'  ");
                $this->load->view('front/settings',$data);
            }
            else{
                redirect('login', 'refresh');
            }
           
            // $this->load->view('front/dashboard_new',$data);
        }
        else{
            $data['user_id'] = '';
            $data['user_type_id'] = '';
            $data['user_name'] = '';
            $user_id='';
            redirect('login', 'refresh');
        }       
    }
    public function brief_permission(){
        $client_id = $this->input->post('client_id');
        $brief_permission = $this->input->post('brief_permission');
        $user_type_id = $this->input->post('user_type_id');
        $brand_id = $this->input->post('brand_id');

        $briefassets_permission=$this->input->post('briefassets_permission');
        $user_permission=$this->input->post('user_permission');
        $category_permission = $this->input->post('category_permission');

        $set_con = array('brief_permission' => $brief_permission,'briefassets_permission' => $briefassets_permission,'user_permission' => $user_permission, 'brand_id' => $brand_id,'category_permission' => $category_permission,);
        $where_con= array('client_id'=>$client_id,'user_type_id'=>$user_type_id);
        $this->db->where($where_con);
        $report = $this->db->update('wc_users', $set_con);
        print_r($report);
    }
    public function add_product()
    {
        //$brand_id=$_REQUEST['brand_id'];
        //$brand_id=$_REQUEST['brand_id'];
        $session_data = $this->session->userdata('front_logged_in');
        $user_id= $session_data['user_id'];
        $user_type_id= $session_data['user_type_id'];
        $user_name= $session_data['user_name'];
 

        $product_name = $this->input->post('product_name');
        $product_description = $this->input->post('product_description');
        $product_price = $this->input->post('product_price');
        $competitor_price = $this->input->post('competitor_price');
        $custom_status = $this->input->post('custom_status');
        $product_stauts = $this->input->post('product_stauts');

        $productdata = array(
        'product_name'=>$product_name,
        'product_dest'=>$product_description,
        'product_price'=>$product_price,
        'competitor_price'=>$competitor_price,
        'status'=>'Active',
        'deleted'=>'0',
        'custom_status'=>$custom_status,
        );
        $report = $this->Settings_model->add_product($productdata);
        $product_id=$this->db->insert_id();
        //exit();
        if($custom_status==0){
            $data['product_details']=$this->Settings_model->get_product_list();
            $this->load->view('front/settings_list', $data);  
        }
        else if($custom_status==1 && $product_stauts=='add'){
            $product_qty = $this->input->post('product_qty');
            $brief_id = $this->input->post('brief_id');
            $subtotal_price=trim($product_qty*$product_price);

            $checkquerys_1= $this->db->query("select * from wc_users where user_id='$user_id'")->result_array();
            $add_discount=$checkquerys_1[0]['add_discount'];
            $add_extra_charges=$checkquerys_1[0]['add_extra_charges'];


            $htmt="";
            $html=" <tr id='".$brief_id."_".$product_id."'>
            <td><div class='small' style='width: 90%; white-space:nowrap; overflow:hidden; text-overflow: ellipsis;'>".$product_name."</div><input type='hidden' id='product_name".$brief_id."' name='product_name".$brief_id."[]' value='".$product_name."' />
            <input type='hidden' id='product_id".$brief_id."' name='product_id".$brief_id."[]' value='".$product_id."' />
            </td>
            <td class='small text-right'>".$product_qty." <input type='hidden' id='total_qty".$brief_id."' name='total_qty".$brief_id."[]' value='".$product_qty."' step='any' min='1'/></td>
            <td class='small text-right'>Rs.".$subtotal_price." <input type='hidden' id='subtotal_price".$brief_id."' name='subtotal_price".$brief_id."[]' value='".$subtotal_price."' readonly/></td>
            <td class='small'><i class='far fa-times-circle' onclick='price_cancel(\"".$brief_id."\",\"".$product_id."\",\"".$add_discount."\",\"".$add_extra_charges ."\")'></i></td>
            </tr>"; 
            echo $html;
        }
        else if($custom_status==1 && $product_stauts=='update'){
            $product_qty = $this->input->post('product_qty');
            $brief_id = $this->input->post('brief_id');
            $subtotal_price=trim($product_qty*$product_price);    
            
            $sql = "INSERT INTO wc_brief_product_list(`brief_id`,`product_id`,`product_quty`,`subtotal_price`)VALUES('$brief_id','$product_id','$product_qty','$subtotal_price')";
            $qry =$this->db->query($sql);

            $brief_product_id=$this->db->insert_id();

            $checkquerys_1= $this->db->query("select * from wc_users where user_id='$user_id'")->result_array();
            $add_discount=$checkquerys_1[0]['add_discount'];
            $add_extra_charges=$checkquerys_1[0]['add_extra_charges'];
           
            $total_price=0;
            $wc_brief_product_result1 = $this->db->query("select * from wc_brief_product_list where brief_id='$brief_id' ")->result_array();
            foreach($wc_brief_product_result1 as $key => $value){ 
                $total_price+=$value['subtotal_price'];
            }
            $set_con1 = array('total_price' => $total_price);
            $where_con1= array('brief_id'=>$brief_id);
            $this->db->where($where_con1);
            $report = $this->db->update('wc_brief', $set_con1);


            $htmt="";
            $html=" <tr id='view_".$brief_id."_".$brief_product_id."'>
            <td>
            <div class='small ' style='width: 90%; white-space:nowrap; overflow:hidden; text-overflow: ellipsis;'>".$product_name."</div>
            <input type='hidden' id='product_name".$brief_product_id."' name='product_name".$brief_product_id."' value='".$product_name."' />
            <input type='hidden' id='product_id".$brief_product_id."' name='product_id".$brief_product_id."' value='".$product_id."' />
            </td>
            <td class='small text-right'>
            <div class='small text-right' id='div_product_quty".$brief_product_id ."'>".$product_qty."</div>
             <div class='d-flex justify-content-between'>
             <input class='form-control d-none ' type='text' id='product_qty".$brief_product_id."' name='product_qty".$brief_product_id."' value='".$product_qty."' step='any' min='1' style='width: 4rem;' onkeyup='update_price_cal(\"".$brief_id."\",\"".$brief_product_id ."\",\"".$add_discount."\",\"".$add_extra_charges ."\")' value='".$product_qty."'/>
              <span class='pl-1'>
               <i class='far fa-check-circle d-none' id='update_product".$brief_product_id ."' onclick='update_product(\"".$brief_id ."\",\"".$brief_product_id ."\",\"".$add_discount."\",\"".$add_extra_charges ."\")'></i>
                <i class='far fa-times-circle d-none' id='cancel_product".$brief_product_id ."' onclick='cancel_product(\"".$brief_product_id ."\",\"".$add_discount."\",\"".$add_extra_charges ."\")'></i>
            </span>
            </div>
             <span class='small text-danger' id='errmsg_product_qty_".$brief_product_id."'></span>

            <script type='text/javascript'>
            $('#product_qty".$brief_product_id ."').keypress(function(e) {

            if (((e.which != 46 || (e.which == 46 && $(this).val() == '')) ||$(this).val().indexOf('.') != 0) && (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57))) {
            $('#errmsg_product_qty_".$brief_product_id."').html('Digits Only').show().fadeOut(1500);
            return false;
            }
            }).on('paste', function(e) {
            $('#errmsg_product_qty_".$brief_product_id."').html('Type the value').show().fadeOut(1500);
            return false;

            });
            $('#product_qty".$brief_product_id ."').on('input', function() {
            if (/^0/.test(this.value)) {
            this.value = this.value.replace(/^0/, '');
            $('#errmsg_product_qty_".$brief_product_id."').html('Invalid').show().fadeOut(1500);
            }

            }); 
            </script> 
             </td>
            <td><div class='small text-right' id='div_subtotal_price_".$brief_product_id ."'> Rs.".$subtotal_price."</div>
            <input type='hidden' class='subtotal_price_".$brief_id."' id='subtotal_price_".$brief_product_id."' name='subtotal_price_".$brief_product_id."' value='".$subtotal_price."' />
            </td>
            ";
             if($user_type_id=='1'||$user_type_id=='2'||$user_type_id=='3'){ 
                 $html.=" <td>
                    <i class='far fa-edit' id='edit_product".$brief_product_id ."' onclick='price_edit(\"".$brief_product_id ."\")'></i>
                    
                    <i class='far fa-trash-alt' id='delete_product".$brief_product_id ."' onclick='price_delete(\"".$brief_product_id ."\")' data-toggle='modal' data-target='#modal_confirm_product".$brief_product_id."'></i>

                    
                    <div class='modal fade' id='modal_confirm_product".$brief_product_id ."' tabindex='-1' role='dialog' aria-hidden='true'>
                      <div class='modal-dialog' role='document'>
                        <div class='modal-content'>
                          <div class='modal-header'>
                            <h5 class='modal-title text-dark' id='mod_title_".$brief_id ."'>Are you sure, you want to delete ?</h5>
                            <button type='button' class='close text-danger' data-dismiss='modal' aria-label='Close'>
                              <span aria-hidden='true' >&times;</span>
                            </button>
                          </div>
                          <div class='modal-footer'>
                            <button type='button' class='btn btn-secondary' data-dismiss='modal'>Close</button>
                            <button type='button' id='hideDel' class='btn btn-danger' onclick='delete_product(\"".$brief_id ."\",\"".$brief_product_id ."\",\"".$add_discount."\",\"".$add_extra_charges ."\")' data-dismiss='modal'>Delete</button>
                          </div>
                        </div>
                      </div>
                    </div>

                  </td>";
                }

         $html.="</tr>"; 
            echo $html;
        }
        else{
            echo "else";
        }  
        //$brief_id=$this->db->insert_id();
    }
    public function update_product()
    {
        //$brand_id=$_REQUEST['brand_id'];
        $product_id = $this->input->post('product_id');
        $product_name = $this->input->post('product_name');
        $product_description = $this->input->post('product_description');
        $product_price = $this->input->post('product_price');
        $competitor_price = $this->input->post('competitor_price');
        $productdata = array(
        'product_name'=>$product_name,
        'product_dest'=>$product_description,
        'product_price'=>$product_price,
        'competitor_price'=>$competitor_price,
        'status'=>'Active',
        'deleted'=>'0',
        
        );
        $this->Settings_model->update_product($productdata,$product_id);
        $data['product_details']=$this->Settings_model->get_product_list();
        $this->load->view('front/settings_list', $data);  
        //$brief_id=$this->db->insert_id();
    }
    public function delete_product()
    {
        $product_id=$this->input->post('product_id');
        $this->Settings_model->delete_product($product_id);
        $data['product_details']=$this->Settings_model->get_product_list();
        $this->load->view('front/settings_list', $data); 
          
    }
    public function update_project_desc()
    {
        //$brand_id=$_REQUEST['brand_id'];
     
        $client_id = $this->input->post('client_id');
        
        $data = array(
        'project_description'=>$this->input->post('project_description'),
        );
        //print_r($data);
        echo $result= $this->Client_model->client_update($data,$client_id);
        //$brief_id=$this->db->insert_id();
    }
    public function update_notifications()
    {
        $user_id = $this->input->post('user_id');
        $status = $this->input->post('status');
        $settingdata = array(
        'admin_mail_notifications'=>$status,
          
        );
       echo $this->Settings_model->update_notifications($settingdata,$user_id);
        
        
    }
    public function add_discount()
    {
        $user_id = $this->input->post('user_id');
        $status = $this->input->post('add_discount_status');
        $sql="select * from  wc_users where user_id=$user_id";

        //echo $sql;
        $query=$this->db->query($sql);

        $result=$query->result_array();
        $client_id=$result[0]['client_id'];

        $brand_details=$this->db->query("select brand_id from wc_brands where deleted='0' and status='Active' and client_id='$client_id' order by  brand_name asc")->result_array();
        $brand_id_arr="";               
        if(!empty($brand_details))
        {
            //print_r($brand_details);
            foreach($brand_details as $key => $branddetails)
            {
                $brand_id=$branddetails['brand_id'];
                //$brand_id_arr.=trim($brand_id,",");
                $brand_id_arr.="'".$brand_id."',";
            }
        }
        $brand_id=trim($brand_id_arr,",");  

        $sql="UPDATE wc_brief set add_discount='0' where brand_id IN ($brand_id)";
        $query=$this->db->query($sql);

        $settingdata = array(
        'add_discount'=>$status,
          
        );
       echo $this->Settings_model->update_discount($settingdata,$client_id);
        
        
    }

    public function add_extra_charges()
    {
        $user_id = $this->input->post('user_id');
        $status = $this->input->post('add_extra_charges_status');

        $sql="select * from  wc_users where user_id=$user_id";

        //echo $sql;
        $query=$this->db->query($sql);

        $result=$query->result_array();
        $client_id=$result[0]['client_id'];
        
        $brand_details=$this->db->query("select brand_id from wc_brands where deleted='0' and status='Active' and client_id='$client_id' order by  brand_name asc")->result_array();
        $brand_id_arr="";               
        if(!empty($brand_details))
        {
            //print_r($brand_details);
            foreach($brand_details as $key => $branddetails)
            {
                $brand_id=$branddetails['brand_id'];
                //$brand_id_arr.=trim($brand_id,",");
                $brand_id_arr.="'".$brand_id."',";
            }
        }
        $brand_id=trim($brand_id_arr,",");  

        $sql="UPDATE wc_brief set add_extra_charges='0' where brand_id IN ($brand_id)";
        $query=$this->db->query($sql);
        
        $settingdata = array(
        'add_extra_charges'=>$status,
          
        );
       echo $this->Settings_model->update_extra_charges($settingdata,$client_id);
        
        
    }
    public function add_retainer()
    {
        //$brand_id=$_REQUEST['brand_id'];
        //$brand_id=$_REQUEST['brand_id'];
        $session_data = $this->session->userdata('front_logged_in');
        $user_id= $session_data['user_id'];
        $user_type_id= $session_data['user_type_id'];
        $user_name= $session_data['user_name'];
 
        $user_id= $session_data['user_id'];
        $checkquerys = $this->db->query("select * from wc_users where  user_id='$user_id' ")->result_array();

        $client_id = $checkquerys[0]['client_id'];

        $retainer_client_id = $this->input->post('retainer_client_id');
        $retainer_name = $this->input->post('retainer_name');
        $retainer_status = $this->input->post('retainer_status');

        $retainerdata = array(
        'client_id'=>$retainer_client_id,
        'retainer_name'=>$retainer_name,
        'status'=>$retainer_status,
        'deleted'=>'0',
        );
        $report = $this->Settings_model->add_retainer($retainerdata);
    
        $data['retainer_details1']=$this->db->query("select * from wc_retainer where deleted='0' and client_id ='$client_id'  order by  retainer_id asc ")->result_array();
       // print_r($retainer_details1);
        $this->load->view('front/setting_retainer_list',$data);
   
        //$brief_id=$this->db->insert_id();
    }
    public function update_retainer()
    {
        $session_data = $this->session->userdata('front_logged_in');
        $user_id= $session_data['user_id'];
        $user_type_id= $session_data['user_type_id'];
        $user_name= $session_data['user_name'];
 
        $user_id= $session_data['user_id'];
        $checkquerys = $this->db->query("select * from wc_users where  user_id='$user_id' ")->result_array();

        $client_id = $checkquerys[0]['client_id'];

        $retainer_id = $this->input->post('retainer_id');
        $retainer_client_id = $this->input->post('retainer_client_id');
        $retainer_name = $this->input->post('retainer_name');
        $retainer_status = $this->input->post('retainer_status');


        $retainerdata = array(
        'retainer_id'=>$retainer_id,
        'client_id'=>$retainer_client_id,
        'retainer_name'=>$retainer_name,
        'status'=>$retainer_status,
        'deleted'=>'0',
        );
        $this->Settings_model->update_retainer($retainerdata,$retainer_id);
        $data['retainer_details1']=$this->db->query("select * from wc_retainer where deleted='0' and client_id ='$client_id'  order by  retainer_id asc ")->result_array();
        $this->load->view('front/setting_retainer_list',$data);
        //$brief_id=$this->db->insert_id();
    }
    public function delete_retainer()
    {
        $session_data = $this->session->userdata('front_logged_in');
        $user_id= $session_data['user_id'];
        $user_type_id= $session_data['user_type_id'];
        $user_name= $session_data['user_name'];
 
        $user_id= $session_data['user_id'];
        $checkquerys = $this->db->query("select * from wc_users where  user_id='$user_id' ")->result_array();

        $client_id = $checkquerys[0]['client_id'];

        $retainer_id=$this->input->post('retainer_id');
        $this->Settings_model->delete_retainer($retainer_id);
        $data['retainer_details1']=$this->db->query("select * from wc_retainer where deleted='0' and client_id ='$client_id'  order by  retainer_id asc ")->result_array();
        $this->load->view('front/setting_retainer_list',$data);
          
    }
    public function checkexistemail(){           // checking the email  exist  validation in add form
        $user_email = $this->input->post('user_email');
        $sql = "SELECT user_id,user_email FROM wc_users WHERE user_email = '$user_email'";
        $query = $this->db->query($sql);
        if( $query->num_rows() > 0 ){
            echo 'false';
        } else {
            echo 'true';
        }
    }

   public function checkexistmobile(){           // checking the email  exist  validation in add form
        $user_mobile = $this->input->post('user_mobile');
        $sql = "SELECT user_id,user_mobile FROM wc_users WHERE user_mobile = '$user_mobile'";
        $query = $this->db->query($sql);
        if( $query->num_rows() > 0 ){
            echo 'false';
        } else {
            echo 'true';
        }
    }



    public function insert_user(){
        
        if ($this->session->userdata('front_logged_in')) {
            $session_data = $this->session->userdata('front_logged_in');
            $created_date = date('Y-m-d');
            $userid=$session_data['id'];
            $user_id=$this->input->post('user_id');
          
            
            if($user_id!='')
            {

                  $user_details=$this->db->query("select * from wc_users where user_id='$user_id'")->result_array();
                  $wekan_member_id = $user_details[0]['wekan_member_id'];
                  $user_email_exist = $user_details[0]['user_email'];
                  $user_password=base64_decode($user_details[0]['user_password_text']);

                  $user_type_id=$this->input->post('user_type_id');
                  $user_name=$this->input->post('user_name');
                  $user_email=$this->input->post('user_email');
                  $client_id=$this->input->post('client_id');
                   $user_country=$this->input->post('user_country');

                  if($user_type_id!='4' && $user_type_id!='6' )
                    {
                    // $wekan_member_id=$this->update_member_wekan($wekan_member_id,$client_id,$user_type_id,$user_name,$user_email,$user_password);
                        $wekan_member_id="";
                    }
                    else
                    {
                        $wekan_member_id="";

                    }
                  $data = array(
                    /*'user_id'=>$this->input->post('user_id'),*/
                    'wekan_member_id'=>$wekan_member_id,
                    'user_type_id'=>$this->input->post('user_type_id'),
                    'client_id'=>$this->input->post('client_id'),
                    'brand_id'=>$this->input->post('brand_id'),
                    'user_name'=>$this->input->post('user_name'),
                    //'user_address'=>$this->input->post('user_address'),
                    'user_email'=>$this->input->post('user_email'),
                    //'user_telephone'=>$this->input->post('user_telephone'),
                    'user_mobile'=>$this->input->post('user_mobile'),
                    /*'user_password'=>$this->input->post('user_password'),*/
                    'user_country'=>$this->input->post('user_country'),
                    'status'=>$this->input->post('status'),
                    );

                    
                  $updateuserrole = $this->User_model->user_update($data,$user_id);
                  //echo $user_id;

                    $this->load->view('front/settings_user_list');

                    
                    /*********************************Send mail start*************************************/

            if($user_email_exist!=$user_email)
                { 

                $query=$this->db->query("select * from  wc_settings  WHERE setting_key='MAIL_SMTP_USER' ");
                $userDetails= $query->result_array();
                $MAIL_SMTP_USER=$userDetails[0]['setting_value'];

                $query=$this->db->query("select * from  wc_settings  WHERE setting_key='MAIL_SMTP_PASSWORD' ");
                $userDetails= $query->result_array();
                $MAIL_SMTP_PASSWORD=$userDetails[0]['setting_value'];

                $query=$this->db->query("select * from  wc_settings  WHERE setting_key='MAIL_FROM' ");
                $userDetails= $query->result_array();
                $MAIL_FROM=$userDetails[0]['setting_value'];


                $query=$this->db->query("select * from  wc_settings  WHERE setting_key='MAIL_FROM_TEXT' ");
                $userDetails= $query->result_array();
                $MAIL_FROM_TEXT=$userDetails[0]['setting_value'];


                $query=$this->db->query("select * from wc_settings WHERE setting_key='WEKAN_URL_LINK' ");
                $settingDetails= $query->result_array();
                $WEKAN_URL_LINK=$settingDetails[0]['setting_value'];

                $query=$this->db->query("select * from  wc_settings  WHERE setting_key='MAIL_BODY_WEKAN_LOGIN' ");
                $userDetails= $query->result_array();
                $MAIL_BODY=$userDetails[0]['setting_value'];

               


                $client_id=$this->input->post('client_id');
                $user_name=$this->input->post('user_name');
                $user_email=$this->input->post('user_email');
                $user_password=$user_password;






                $checkquerys = $this->db->query("select * from wc_clients where  client_id='$client_id' ")->result_array();
                $client_name=ucfirst($checkquerys[0]['client_name']);
                $client_image=$checkquerys[0]['client_image'];
                $client_email=$checkquerys[0]['client_email'];

                //$subject=$client_name.' - Wekan Login Detail';
                $subject=$client_name.' - Login User Email Changed';

                //$unique_user_id=time()."-".$user_id;
                //$activation_link=base_url()."activate-registration/".base64_encode($unique_user_id);

                    
                    //Load email library
                    $this->load->library('email');

                    $config = array();
                    //$config['protocol'] = 'smtp';
                    $config['protocol']     = 'mail';
                    $config['smtp_host'] = 'localhost';
                    $config['smtp_user'] = $MAIL_SMTP_USER;
                    $config['smtp_pass'] = $MAIL_SMTP_PASSWORD;
                    $config['smtp_port'] = 25;




                    $this->email->initialize($config);
                    $this->email->set_newline("\r\n");


                    if($client_id!='') {
                    $checkquerys = $this->db->query("select * from wc_clients where client_id='$client_id' ")->result_array();

                    $client_image=$checkquerys[0]['client_image'];
                    //$wekan_client_id=$checkquerys[0]['wekan_client_id'];

                    $client_logo=base_url()."/upload_client_logo/".$client_image;
                    }
                    else
                    {
                    $client_logo=base_url()."/website-assets/images/logo.png";
                    }
                    $client_logo="http://style.piquic.com/WeCreate//website-assets/images/logo.png";

                    $client_name=str_replace(" ","-",strtolower($checkquerys[0]['client_name']));


                    //$wekan_link=$WEKAN_URL_LINK."b/".$wekan_client_id."/".$client_name;
                    $wekan_link=$WEKAN_URL_LINK."sign-in";

                    

                    
                    $website_url=base_url();
                   $dashboard_url=base_url()."dashboard";




                    $from_email = $MAIL_FROM;
                    $to_email = $user_email;

                    $date_time=date("d/m/y");
                    /*$copy_right=$MAIL_FROM_TEXT." - ".date("Y");*/
                    $copy_right=$MAIL_FROM_TEXT;
                    
                     $find_arr = array("##WEBSITE_URL##","##DASHBOARD_LINK##","##CLIENT_LOGO##","##USER_NAME##","##WEKAN_LINK##","##USER_EMAIL##","##USER_PASSWORD##","##COPY_RIGHT##");
                    $replace_arr = array($website_url,$dashboard_url,$client_logo,$user_name,$wekan_link,$user_email,$user_password,$client_name,$copy_right);
                    $message=str_replace($find_arr,$replace_arr,$MAIL_BODY);

                    //echo $message;exit;



                    $this->email->from($from_email, $MAIL_FROM_TEXT);
                    $this->email->to($to_email);
                    /*$this->email->cc(array('poulami@mokshaproductions.in','pranav@mokshaproductions.in','raja.priya@mokshaproductions.in'));*/
                    if($_SERVER['HTTP_HOST']!='collab.piquic.com')
                    {
                        $this->email->cc(array('poulami@mokshaproductions.in','pranav@mokshaproductions.in','raja.priya@mokshaproductions.in','rajendra.prasad@mokshaproductions.in'));
                    }
                    // $this->email->bcc('pranav@mokshaproductions.in');
             //$this->email->bcc('raja.priya@mokshaproductions.in');
                    //$this->email->bcc('pranav@mokshaproductions.in');
                    $this->email->subject($subject);
                    $this->email->message($message);
                    //Send mail
                    // if($this->email->send())
                    // {

                    //     //echo "mail sent";exit;
                    //     $this->session->set_flashdata("email_sent","Congratulations Email Send Successfully.");
                    // }
                    
                    // else
                    // {
                    //     //echo $this->email->print_debugger();
                    //     //echo "mail not sent";exit;
                    //     $this->session->set_flashdata("email_sent","You have encountered an error");
                    // }

              
                }

                    /*********************************Send mail end*************************************/
                
            }else {
                
                  $user_type_id=$this->input->post('user_type_id');
                  $user_name=$this->input->post('user_name');
                  $user_email=$this->input->post('user_email');
                  $user_password=$this->input->post('user_password');
                  $client_id=$this->input->post('client_id');
                   $user_country=$this->input->post('user_country');

                  

                  $wekan_member_id='';
                  if($wekan_member_id=='0')
                  {
                    echo "wekanexist";

                  }
                  else
                  {

        
                   $data = array(
                    /*'user_id'=>$this->input->post('user_id'),*/
                    'wekan_member_id'=>$wekan_member_id,
                    'user_type_id'=>$this->input->post('user_type_id'),
                    'client_id'=>$this->input->post('client_id'),
                    'brand_id'=>$this->input->post('brand_id'),
                    'user_name'=>$this->input->post('user_name'),
                    //'user_address'=>$this->input->post('user_address'),
                    'user_email'=>$this->input->post('user_email'),
                    //'user_telephone'=>$this->input->post('user_telephone'),
                    'user_mobile'=>$this->input->post('user_mobile'),
                    'user_password'=>sha1($this->input->post('user_password')),
                    'user_password_text'=>base64_encode($this->input->post('user_password')),

                    'user_country'=>$this->input->post('user_country'),
                    'status'=>$this->input->post('status'),
                    );
                    $this->User_model->user_insert($data);
                    $user_id=$this->db->insert_id();
                    
                      $this->load->view('front/settings_user_list');
                     // exit();
                    //echo $user_id;

                /*********************************Send mail start*************************************/

                //if($user_type_id!='4')
                    //{
                    

                $query=$this->db->query("select * from  wc_settings  WHERE setting_key='MAIL_SMTP_USER' ");
                $userDetails= $query->result_array();
                $MAIL_SMTP_USER=$userDetails[0]['setting_value'];

                $query=$this->db->query("select * from  wc_settings  WHERE setting_key='MAIL_SMTP_PASSWORD' ");
                $userDetails= $query->result_array();
                $MAIL_SMTP_PASSWORD=$userDetails[0]['setting_value'];

                $query=$this->db->query("select * from  wc_settings  WHERE setting_key='MAIL_FROM' ");
                $userDetails= $query->result_array();
                $MAIL_FROM=$userDetails[0]['setting_value'];


                $query=$this->db->query("select * from  wc_settings  WHERE setting_key='MAIL_FROM_TEXT' ");
                $userDetails= $query->result_array();
                $MAIL_FROM_TEXT=$userDetails[0]['setting_value'];


                $query=$this->db->query("select * from wc_settings WHERE setting_key='WEKAN_URL_LINK' ");
                $settingDetails= $query->result_array();
                $WEKAN_URL_LINK=$settingDetails[0]['setting_value'];

                $query=$this->db->query("select * from  wc_settings  WHERE setting_key='MAIL_BODY_WEKAN_LOGIN' ");
                $userDetails= $query->result_array();
                $MAIL_BODY=$userDetails[0]['setting_value'];

                


                $client_id=$this->input->post('client_id');
                $user_name=$this->input->post('user_name');
                $user_email=$this->input->post('user_email');
                $user_password=$this->input->post('user_password');






                $checkquerys = $this->db->query("select * from wc_clients where  client_id='$client_id' ")->result_array();
                $client_name=ucfirst($checkquerys[0]['client_name']);
                $client_image=$checkquerys[0]['client_image'];
                $client_email=$checkquerys[0]['client_email'];

                 //$subject=$client_name.' - Wekan Login Detail';
                $user_type_id=$this->input->post('user_type_id');
                if($user_type_id==6)
                {
                     $subject='Creator - Login Detail';
                }
                else
                {
                    $subject=$client_name.' - Login Detail';
                }

                 //$subject=$client_name.' - Login Detail';

                //$unique_user_id=time()."-".$user_id;
                //$activation_link=base_url()."activate-registration/".base64_encode($unique_user_id);

                    
                    //Load email library
                    $this->load->library('email');

                    $config = array();
                    //$config['protocol'] = 'smtp';
                    $config['protocol']     = 'mail';
                    $config['smtp_host'] = 'localhost';
                    $config['smtp_user'] = $MAIL_SMTP_USER;
                    $config['smtp_pass'] = $MAIL_SMTP_PASSWORD;
                    $config['smtp_port'] = 25;




                    $this->email->initialize($config);
                    $this->email->set_newline("\r\n");


                    if($client_id!='') {
                    $checkquerys = $this->db->query("select * from wc_clients where client_id='$client_id' ")->result_array();

                    $client_image=$checkquerys[0]['client_image'];
                    $wekan_client_id=$checkquerys[0]['wekan_client_id'];

                    $client_logo=base_url()."/upload_client_logo/".$client_image;
                    }
                    else
                    {
                    $client_logo=base_url()."/website-assets/images/logo.png";
                    }
                    $client_logo="http://style.piquic.com/WeCreate//website-assets/images/logo.png";

                    $client_name=str_replace(" ","-",strtolower($checkquerys[0]['client_name']));


                    //$wekan_link=$WEKAN_URL_LINK."b/".$wekan_client_id."/".$client_name;
                    $wekan_link=$WEKAN_URL_LINK."sign-in";

                    

                      $website_url=base_url();
                      $dashboard_url=base_url()."dashboard";
                   




                    $from_email = $MAIL_FROM;
                    $to_email = $user_email;

                    $date_time=date("d/m/y");
                    $copy_right= date("Y");
                  
                     $find_arr = array("##WEBSITE_URL##","##DASHBOARD_LINK##","##CLIENT_LOGO##","##USER_NAME##","##WEKAN_LINK##","##USER_EMAIL##","##USER_PASSWORD##","##COPY_RIGHT##");
                    $replace_arr = array($website_url,$dashboard_url,$client_logo,$user_name,$wekan_link,$user_email,$user_password,$client_name,$copy_right);


                    $message=str_replace($find_arr,$replace_arr,$MAIL_BODY);

                    //echo $message;exit;



                    $this->email->from($from_email, $MAIL_FROM_TEXT);
                    $this->email->to($to_email);
                    /*$this->email->cc(array('poulami@mokshaproductions.in','pranav@mokshaproductions.in','raja.priya@mokshaproductions.in'));*/
                    if($_SERVER['HTTP_HOST']!='collab.piquic.com')
                    {
                        $this->email->cc(array('poulami@mokshaproductions.in','pranav@mokshaproductions.in','raja.priya@mokshaproductions.in','rajendra.prasad@mokshaproductions.in'));
                    }
                     //$this->email->bcc('pranav@mokshaproductions.in');
             //$this->email->bcc('raja.priya@mokshaproductions.in');
                    //$this->email->bcc('pranav@mokshaproductions.in');
                    $this->email->subject($subject);
                    $this->email->message($message);
                    //Send mail
                    // if($this->email->send())
                    // {

                    //     //echo "mail sent";exit;
                    //     $this->session->set_flashdata("email_sent","Congratulations Email Send Successfully.");
                    // }
                    
                    // else
                    // {
                    //     //echo $this->email->print_debugger();
                    //     //echo "mail not sent";exit;
                    //     $this->session->set_flashdata("email_sent","You have encountered an error");
                    // }

                 // }


                    /*********************************Send mail end*************************************/

                    }


                }
               
            
        }else {
        
            redirect('login', 'refresh');
            // redirect('administrator', 'refresh');
       
        }  
    }


   public function showDivClient(){
        $session_data = $this->session->userdata('front_logged_in');
        $user_id= $session_data['user_id'];
        $user_type_id= $session_data['user_type_id'];
      
        $user_type_id=$this->input->post('user_type_id');
        $client_id_sel=$this->input->post('client_id');
        $brand_id_sel=$this->input->post('brand_id');
        ?>

        <div class="form-group">
            <label class="col-form-label">Client <span style="color:#F00">*</span> </label>
            <select  name="client_id[]" id="client_id" class="custom-select" style="width: 100%" <?php if($client_id_sel!='' && !strstr($client_id_sel,",")) { ?> disabled="" <?php }  ?>>
                <option value="">Select</option>
                <?php
                $users_details=$this->db->query("select * from wc_users where user_id='$user_id'")->result_array();
                foreach($users_details as $key => $usersdetails){
                    $client_id=$usersdetails['client_id'];
                
                    $client_details=$this->db->query("select * from wc_clients where deleted='0' and status='Active' and client_id='$client_id' order by  client_name asc")->result_array();
                    //print_r("select * from wc_users where deleted='0' and status='Active' order by  client_name asc");
                    if(!empty($client_details)){
                        foreach($client_details as $key => $clientdetails){
                            //$client_id=$clientdetails['client_id'];
                            $client_name=$clientdetails['client_name'];
                            ?>
                            <option value="<?php echo $client_id;?>" ><?php echo $client_name;?></option>
                        <?php }
                    }
                }
                ?>
            </select>
            <input type='hidden' name='brand_id' value='0' id='brand_id'>
        </div>
    <?php }

    public function showDivClientMultiple(){
        $session_data = $this->session->userdata('front_logged_in');
        $user_id= $session_data['user_id'];
        $user_type_id= $session_data['user_type_id'];

        $user_type_id=$this->input->post('user_type_id');
        $client_id_sel=$this->input->post('client_id');
        $brand_id_sel=$this->input->post('brand_id');

        $client_id_arr=array();

        if(!empty($client_id_sel))
        {
            $client_id_arr=explode(",",trim($client_id_sel,","));
        }  ?>

       <div class="form-group">
            <label class="col-form-label">Client  <span style="color:#F00">*</span> </label>
            <?php
            // $users_details=$this->db->query("select * from wc_users where user_id='$user_id'")->result_array();
            // foreach($users_details as $key => $usersdetails){
            // //     $client_id=$usersdetails['client_id'];
            //     $clients_details=$this->db->query("select * from wc_clients where deleted='0' and status='Active' and client_id='$client_id' order by  client_name asc")->result_array();
                $clients_details=$this->db->query("select * from wc_clients where deleted='0' and status='Active'  order by  client_name asc")->result_array();
                if(!empty($clients_details)){
                    foreach($clients_details as $key => $clientdetails){
                        $client_id=$clientdetails['client_id'];
                        $client_name=$clientdetails['client_name'];  ?>
                        <div class="form-check">
                            <input class="form-check-input client_id_class" type="checkbox" value="<?php echo $client_id;?>" name="client_id[]" id="client_id<?php echo $client_id;?>">
                            <label class="form-check-label" for="client_id<?php echo $client_id;?>"><?php echo $client_name;?></label>
                        </div>
                    <?php  }  ?>
                    <input type='hidden' name='brand_id' value='0' id='brand_id'>
                <?php  }else{ ?>
                            There is no Clients.
                            <input type='hidden' name='client_id' value='0' id='client_id'>
                            <input type='hidden' name='brand_id' value='0' id='brand_id'>
                <?php   }
          //  } 
                ?>
        </div>
    <?php
    }


    public function showDivClientBrand(){
        $session_data = $this->session->userdata('front_logged_in');
        $user_id= $session_data['user_id'];
        $user_type_id= $session_data['user_type_id'];

        $client_id_sel=$this->input->post('client_id');
        $brand_id_sel=$this->input->post('brand_id');

        $brand_id_arr=array();
        ?>
        <div class="form-group">
            <label class="col-form-label">Client  <span style="color:#F00">*</span> </label>
            <select name="client_id" id="client_id" class="custom-select"  style="width: 100%" onchange="selectClient();" <?php if($client_id_sel!='' && !strstr($client_id_sel,",")) { ?> disabled="" <?php }  ?> >
                <option value=""  >Select</option>
                <?php
                 $users_details=$this->db->query("select * from wc_users where user_id='$user_id'")->result_array();
                foreach($users_details as $key => $usersdetails){
                    $client_id=$usersdetails['client_id'];
                    $client_details=$this->db->query("select * from wc_clients where deleted='0' and status='Active' and  client_id='$client_id' order by  client_name asc")->result_array();
                    if(!empty($client_details)){
                        foreach($client_details as $key => $clientdetails){
                      //  $client_id=$clientdetails['client_id'];
                        $client_name=$clientdetails['client_name'];
                        ?>
                            <option value="<?php echo $client_id;?>"><?php echo $client_name;?></option>
                        <?php
                        }
                    }
                }
                ?>
            </select>
        </div>
        
        <div class="form-group" id="brandDiv" style="display: none;">
            <label class="col-form-label">Brand  <span style="color:#F00">*</span>  </label>
           <?php
            $brand_details=$this->db->query("select * from wc_brands where deleted='0' and status='Active' order by  brand_name asc")->result_array();
            if(!empty($brand_details)){
                foreach($brand_details as $key => $branddetails){
                $brand_id=$branddetails['brand_id'];
                $brand_name=$branddetails['brand_name'];
                ?>
                <div class="form-check">
                    <input class="form-check-input brand_id_class" type="checkbox" value="<?php echo $brand_id;?>" name="brand_id[]" id="brand_id<?php echo $brand_id;?>">
                    <label class="form-check-label" for="brand_id<?php echo $brand_id;?>"><?php echo $brand_name;?></label>
                </div>
                <?php
                }
            }else{  ?>
                There is no brands.
                <input type='text' name='brand_id' value='0' id='brand_id'>
            <?php  }  ?>
        </div>
        <?php
    }









    public function showDivBrand(){
        $client_id_sel=$this->input->post('client_id');
        $brand_id_sel=$this->input->post('brand_id');

        if($brand_id_sel!=''){
            $brand_id_arr=explode(",",trim($brand_id_sel,","));
        }else{
            $brand_id_arr=array();
        }
        ?>
        <div class="form-group">
            <label class="col-form-label">Brand  <span style="color:#F00">*</span></label>
            <?php
            if($client_id_sel!=''){
                $brand_details=$this->db->query("select * from wc_brands where deleted='0' and status='Active' and client_id='$client_id_sel' order by  brand_name asc")->result_array();
            }else{
                $brand_details=$this->db->query("select * from wc_brands where deleted='0' and status='Active' order by  brand_name asc")->result_array();
            }
                                
            if(!empty($brand_details))
            {
                foreach($brand_details as $key => $branddetails)
                {
                    $brand_id=$branddetails['brand_id'];
                    $brand_name=$branddetails['brand_name'];
                    ?>
                    <div class="form-check">
                        <input class="form-check-input brand_id_class" type="checkbox" value="<?php echo $brand_id;?>" name="brand_id[]" id="brand_id<?php echo $brand_id;?>">
                        <label class="form-check-label" for="brand_id<?php echo $brand_id;?>"><?php echo $brand_name;?></label>
                    </div>
                    <?php
                }
            }else{  ?>
                There is no brands.
                <input type='hidden' name='brand_id' value='0' id='brand_id'>
            <?php } ?>
        </div>
    <?php
    }

 
    public function deleteuser()
    {

        if ($this->session->userdata('front_logged_in')) 
        {
            $user_id = $this->input->post('user_id');
            $result=$this->db->query("delete from wc_users where user_id='$user_id'");
            $this->load->view('front/settings_user_list');
        } 
        else 
        {
            //If no session, redirect to login page
            /*redirect('login', 'refresh');*/
            redirect('administrator', 'refresh');
        }
    }
    
    
    
    
    public function view($user_id=''){
        
        if ($this->session->userdata('front_logged_in'))      
        {  
            $session_data = $this->session->userdata('front_logged_in');
            $slang="english";
            $this->lang->load($slang, $slang);
            $data['lang']=$this->lang->language; 
            $data['title'] = "User Role";
            if($user_id!=''){
                $user_details=$this->db->query("select * from wc_users where user_id='$user_id'")->result_array();
                if(!empty($user_details)){
                    $data['user_id'] = $user_details[0]['user_id'];
                    $data['user_type_id_sel'] = $user_details[0]['user_type_id'];
                    $data['brand_id_sel'] = explode(",",trim($user_details[0]['brand_id'],","));
                    $data['user_name'] = $user_details[0]['user_name'];
                    $data['user_address'] = $user_details[0]['user_address'];
                    $data['user_email'] = $user_details[0]['user_email'];
                    $data['user_telephone'] = $user_details[0]['user_telephone'];
                    $data['user_mobile'] = $user_details[0]['user_mobile'];
                    $data['user_password'] = $user_details[0]['user_password'];
                    
                    $data['status'] = $user_details[0]['status'];
                    
                }
            }else {
                    $data['user_id'] = '';
                    $data['user_type_id_sel'] = array();
                    $data['brand_id_sel'] = '';
                    $data['user_name'] = '';
                    $data['user_address'] = '';
                    $data['user_email'] = '';
                    $data['user_telephone'] = '';
                    $data['user_mobile'] = '';
                    $data['user_password'] = '';
                    
                    $data['status'] = '';
            }
            $this->load->view('admin/user/user_view', $data);
        } 
        else 
        {
            //If no session, redirect to login page
            /*redirect('login', 'refresh');*/
            redirect('administrator', 'refresh');
        }
    }
    public function updatestatus(){               //for email exist validation in edit customer form 
        $id=trim($this->input->get('id'));
        $status=trim($this->input->get('status'));
        //echo "update wc_users set status ='$status' where customer_id ='$id'";
        $this->db->query("update wc_users set status ='$status' where user_id ='$id'");
    }

    public function showDivClient_user(){
        $session_data = $this->session->userdata('front_logged_in');
        $user_id= $session_data['user_id'];
       // $user_type_id= $session_data['user_type_id'];
      
        $user_type_id=$this->input->post('user_type_id');
        $user_id1=$this->input->post('user_id');
        $client_id_sel=$this->input->post('client_id');
        $brand_id_sel=$this->input->post('brand_id');
        ?>

        <div class="form-group">
            <label class="col-form-label">Client <span style="color:#F00">*</span> </label>
            <select  name="client_id<?php echo $user_id1; ?>[]" id="client_id<?php echo $user_id1; ?>" class="custom-select" style="width: 100%" <?php if($client_id_sel!='' && !strstr($client_id_sel,",")) { ?> disabled="" <?php }  ?>>
                <option value="">Select</option>
                <?php
                $users_details=$this->db->query("select * from wc_users where user_id='$user_id'")->result_array();
                foreach($users_details as $key => $usersdetails){
                    $client_id=$usersdetails['client_id'];
                
                    $client_details=$this->db->query("select * from wc_clients where deleted='0' and status='Active' and client_id='$client_id' order by  client_name asc")->result_array();
                    //print_r("select * from wc_users where deleted='0' and status='Active' order by  client_name asc");
                    if(!empty($client_details)){
                        foreach($client_details as $key => $clientdetails){
                            $client_id=$clientdetails['client_id'];
                            $client_name=$clientdetails['client_name'];
                            ?>
                            <option value="<?php echo $client_id;?>" <?php if($client_id_sel==$client_id) { ?> selected="selected" <?php }  ?> ><?php echo $client_name;?></option>
                        <?php }
                    }
                }
                ?>
            </select>
            <input type='hidden' name='brand_id<?php echo $user_id1; ?>' value='0' id='brand_id<?php echo $user_id1; ?>'>
        </div>
    <?php }

    public function showDivClientMultiple_user(){
        $session_data = $this->session->userdata('front_logged_in');
        $user_id= $session_data['user_id'];
       // $user_type_id= $session_data['user_type_id'];
        $user_id1=$this->input->post('user_id');
        $user_type_id=$this->input->post('user_type_id');
        $client_id_sel=$this->input->post('client_id');
        $brand_id_sel=$this->input->post('brand_id');

        $client_id_arr=array();

        if(!empty($client_id_sel))
        {
            $client_id_arr=explode(",",trim($client_id_sel,","));
        }  ?>

       <div class="form-group">
            <label class="col-form-label">Client  <span style="color:#F00">*</span> </label>
            <?php
                $clients_details=$this->db->query("select * from wc_clients where deleted='0' and status='Active' order by  client_name asc")->result_array();
               // print_r($clients_details);
                if(!empty($clients_details)){
                    foreach($clients_details as $key => $clientdetails){
                        $client_id=$clientdetails['client_id'];
                        $client_name=$clientdetails['client_name'];  ?>
                        <div class="form-check">
                            <input class="form-check-input client_id_class" type="checkbox" value="<?php echo $client_id;?>" name="client_id<?php echo $user_id1; ?>[]" id="client_id<?php echo $user_id1; ?>_<?php echo $client_id;?>" <?php if(in_array($client_id, $client_id_arr)) { ?> checked   <?php } ?> >
                            <label class="form-check-label" for="client_id<?php echo $client_id;?>"><?php echo $client_name;?></label>
                        </div>
                    <?php  }  ?>
                    <input type='hidden' name='brand_id<?php echo $user_id1; ?>' value='0' id='brand_id<?php echo $user_id1; ?>'>
                <?php  }else{ ?>
                            There is no Clients.
                            <input type='hidden' name='client_id<?php echo $user_id1; ?>' value='0' id='client_id<?php echo $user_id1; ?>'>
                            <input type='hidden' name='brand_id<?php echo $user_id1; ?>' value='0' id='brand_id<?php echo $user_id1; ?>'>
                <?php   }
           // } 
                ?>
        </div>
    <?php
    }


    public function showDivClientBrand_user(){
        $session_data = $this->session->userdata('front_logged_in');
        $user_id= $session_data['user_id'];
        //$user_type_id= $session_data['user_type_id'];
        $user_id1=$this->input->post('user_id');

        $client_id_sel=$this->input->post('client_id');
        $brand_id_sel=$this->input->post('brand_id');

        $brand_id_arr=array();
        ?>
        <div class="form-group">
            <label class="col-form-label">Client  <span style="color:#F00">*</span> </label>
            <select name="client_id<?php echo $user_id1; ?>" id="client_id<?php echo $user_id1; ?>" class="custom-select"  style="width: 100%" onchange="selectClient1();" <?php if($client_id_sel!='' && !strstr($client_id_sel,",")) { ?> disabled="" <?php }  ?> >
                <option value=""  >Select</option>
                <?php
                $users_details=$this->db->query("select * from wc_users where user_id='$user_id'")->result_array();
                foreach($users_details as $key => $usersdetails){
                    $client_id=$usersdetails['client_id'];
                    $client_details=$this->db->query("select * from wc_clients where deleted='0' and status='Active' and  client_id='$client_id' order by  client_name asc")->result_array();
                    if(!empty($client_details)){
                        foreach($client_details as $key => $clientdetails){
                            //  $client_id=$clientdetails['client_id'];
                            $client_name=$clientdetails['client_name'];
                        ?>
                            <option value="<?php echo $client_id;?>" <?php if($client_id_sel==$client_id) { ?> selected="selected" <?php }  ?>><?php echo $client_name;?></option>
                        <?php
                        }
                    }
                }
                ?>
            </select>
        </div>
      
        <div class="form-group" id="brandDiv<?php echo $user_id1; ?>" style="display: none;">
            <label class="col-form-label">Brand  <span style="color:#F00">*</span>  </label>
           <?php

            $brand_details=$this->db->query("select * from wc_brands where deleted='0' and status='Active' order by  brand_name asc")->result_array();
            if(!empty($brand_details)){
                foreach($brand_details as $key => $branddetails){
                $brand_id=$branddetails['brand_id'];
                $brand_name=$branddetails['brand_name'];
                ?>
                <div class="form-check">
                    <input class="form-check-input brand_id_class" type="checkbox" value="<?php echo $brand_id;?>" name="brand_id<?php echo $user_id1; ?>[]" id="brand_id<?php echo $user_id1; ?>_<?php echo $brand_id;?>" <?php if(in_array($brand_id, $brand_id_arr)) { ?> checked   <?php } ?>>
                    <label class="form-check-label" for="brand_id<?php echo $brand_id;?>"><?php echo $brand_name;?></label>
                </div>
                <?php
                }
            }else{  ?>
                There is no brands.
                <input type='text' name='brand_id<?php echo $user_id1; ?>' value='0' id='brand_id<?php echo $user_id1; ?>'>
            <?php  }  ?>
        </div>
        <?php
    }


    public function showDivBrand_user(){

        $user_id1=$this->input->post('user_id');
        $client_id_sel=$this->input->post('client_id');
        $brand_id_sel=$this->input->post('brand_id');

        if($brand_id_sel!=''){
            $brand_id_arr=explode(",",trim($brand_id_sel,","));
        }else{
            $brand_id_arr=array();
        }

        ?>
        <div class="form-group">
            <label class="col-form-label">Brand  <span style="color:#F00">*</span></label>
            <?php
            if($client_id_sel!=''){
                $brand_details=$this->db->query("select * from wc_brands where deleted='0' and status='Active' and client_id='$client_id_sel' order by  brand_name asc")->result_array();
            }else{
                $brand_details=$this->db->query("select * from wc_brands where deleted='0' and status='Active' order by  brand_name asc")->result_array();
            }
                                
            if(!empty($brand_details))
            {
                foreach($brand_details as $key => $branddetails)
                {
                    $brand_id=$branddetails['brand_id'];
                    $brand_name=$branddetails['brand_name'];
                    ?>
                    <div class="form-check">
                        <input class="form-check-input brand_id_class" type="checkbox" value="<?php echo $brand_id;?>" name="brand_id<?php echo $user_id1; ?>[]" id="brand_id<?php echo $user_id1; ?>_<?php echo $brand_id;?>" <?php if(in_array($brand_id, $brand_id_arr)) { ?> checked   <?php } ?>>
                        <label class="form-check-label" for="brand_id<?php echo $user_id1; ?><?php echo $brand_id;?>"><?php echo $brand_name;?></label>
                    </div>
                    <?php
                }
            }else{  ?>
                There is no brands.
                <input type='hidden' name='brand_id<?php echo $user_id1; ?>' value='0' id='brand_id<?php echo $user_id1; ?>'>
            <?php } ?>
        </div>
    <?php
    }

    public function sortuser(){  

        if ($this->session->userdata('front_logged_in')) {
            $session_data = $this->session->userdata('front_logged_in');
            $data['user_id'] = $session_data['user_id'];
            $data['user_type_id'] = $session_data['user_type_id'];
            $data['user_name'] = $session_data['user_name'];
            $type1 = $session_data['user_type_id'];

            $search_key=preg_replace('/[^a-zA-Z0-9_ -]/s','',$this->input->post('search_key'));
            $user_id= $session_data['user_id'];

            $aColumns = array( 'user_id','user_name','user_email','user_telephone','user_mobile','status','client_name','brand_name','user_type_name');

            $users_details=$this->db->query("select * from wc_users where user_id='$user_id'")->result_array();
            $client_id=$users_details[0]['client_id'];
            $sWhere="select * from wc_users 
            JOIN wc_clients ON wc_clients.client_id = wc_users.client_id 
            JOIN wc_brands ON wc_brands.client_id = wc_users.client_id 
            JOIN wc_users_type ON wc_users_type.user_type_id = wc_users.user_type_id 
            where (wc_users.user_type_id!='1' and wc_users.user_type_id!='2' ) and wc_users.deleted='0' and (wc_users.client_id in ( ".trim($client_id,",")." ) OR wc_users.client_id LIKE '%,".trim($client_id,",").",%') ";
            //print_r("select * from wc_users where (user_type_id!='1' and user_type_id!='2' ) and deleted='0' and client_id in ( ".trim($client_id,",")." ) OR client_id LIKE '%,".trim($client_id,",").",%'  ");
            if (!empty($search_key)){
                 $sWhere .= "  AND ( ";
                for ( $i=0 ; $i<count($aColumns) ; $i++ ){
                    if($aColumns[$i]=='client_name'){
                        $sWhere .= "wc_clients.".$aColumns[$i]." LIKE '%".( $search_key )."%' OR ";
                    }
                    else if($aColumns[$i]=='brand_name'){
                        $sWhere .= "wc_brands.".$aColumns[$i]." LIKE '%".( $search_key )."%' OR ";
                    }
                    else if($aColumns[$i]=='user_type_name'){
                        $sWhere .= "wc_users_type.".$aColumns[$i]." LIKE '%".( $search_key )."%' OR ";
                    }
                    else{
                        $sWhere .= "wc_users.".$aColumns[$i]." LIKE '%".( $search_key )."%' OR ";
                    }
                }

                $sWhere = substr_replace( $sWhere, "", -3 );
                $sWhere .= ') group by user_id';
            }
            //print_r($sWhere); exit();
            $data['users_details1']=$this->db->query($sWhere)->result_array();
            $this->load->view('front/settings_user_list',$data); 
        }else{
            redirect('login', 'refresh');
        }       
    }



    public function insert_category(){
        
        if ($this->session->userdata('front_logged_in')) {
            $session_data = $this->session->userdata('front_logged_in');
            $created_date = date('Y-m-d');
            $userid=$session_data['id'];
            $category_id=$this->input->post('category_id');
            $client_id=$this->input->post('client_id');
            $category_name=$this->input->post('category_name');
            $category_type=$this->input->post('category_type');
            $parent_category_id=$this->input->post('parent_category_id');
            $grand_parent_category_id=$this->input->post('grand_parent_category_id');
            
            if($category_id!=''){
                $data = array(
                    'category_id'=>$this->input->post('category_id'),
                    'client_id'=>$this->input->post('client_id'),
                    'category_name'=>$this->input->post('category_name'),
                    'status'=>$this->input->post('status'),
                    'category_type'=>$this->input->post('category_type'),
                    'parent_category_id'=>$this->input->post('parent_category_id'),
                    'grand_parent_category_id'=>$this->input->post('grand_parent_category_id'),
                );
                $updateuserrole = $this->Category_model->category_update($data,$category_id);
                echo $category_id;
                
            }else {
                
                $data = array(
                    'category_id'=>$this->input->post('category_id'),
                    'client_id'=>$this->input->post('client_id'),
                    'category_name'=>$this->input->post('category_name'),
                    'status'=>$this->input->post('status'),

                    'category_type'=>$this->input->post('category_type'),
                    'parent_category_id'=>$this->input->post('parent_category_id'),
                    'grand_parent_category_id'=>$this->input->post('grand_parent_category_id'),
                );
                $this->Category_model->category_insert($data);
                $category_id=$this->db->insert_id();
                echo $category_id;
            }
        }else{
            redirect('login', 'refresh');
       
        }  
    }
        

 
    public function deletecategory()
    {
        if ($this->session->userdata('front_logged_in')){
            $category_id = $this->input->post('category_id');
            $this->db->query("update wc_category set deleted ='1',status='Inactive' where category_id ='$category_id'");
            $this->session->set_flashdata('added', '<div align="left" style="color:green; font-size:15px; border:1px #398AB9 solid;">
            Service Deleted Successfully</div>');
        }else{
            redirect('login', 'refresh');
        }
    }
    
    public function showCategoryDiv(){
        $category_id=$this->input->post('category_id');
        $category_type=$this->input->post('category_type');
        $client_id=$this->input->post('client_id');
        $parent_category_id_sel=$this->input->post('parent_category_id');
        $grand_parent_category_id_sel=$this->input->post('grand_parent_category_id');
        if($category_type=="1") {
        ?>
            <input type='hidden' name='parent_category_id<?php echo $category_id; ?>' value='0' id='parent_category_id<?php echo $category_id; ?>'>
            <div id="subcategory_div">
                <input type='hidden' name='grand_parent_category_id<?php echo $category_id; ?>' value='0' id='grand_parent_category_id<?php echo $category_id; ?>'>
            </div>

        <?php
        }else if($category_type=="2") {
        ?>
            <div class="form-group">
                <label for="inputEmail3"  class="col-form-label">Category</label>
                <?php
                $clients_details=$this->db->query("select * from wc_category where category_type='1' and client_id='$client_id' and deleted='0' and status='Active' ")->result_array();
                if(!empty($clients_details))
                {
                ?>
                <select name="parent_category_id<?php echo $category_id; ?>" id="parent_category_id<?php echo $category_id; ?>" class="custom-select"  style="width: 100%" >
                    <option value=''>Select</option>
                    <?php
                    $clients_details=$this->db->query("select * from wc_category where category_type='1' and client_id='$client_id' and deleted='0' and status='Active' ")->result_array();
                    if(!empty($clients_details)){
                        foreach($clients_details as $key => $clientsdetails){
                            $category_id11=$clientsdetails['category_id'];
                            $category_name=$clientsdetails['category_name'];
                            ?>
                            <option value="<?php echo $category_id11;?>" <?php if($parent_category_id_sel==$category_id11) { ?> selected="selected" <?php } ?>  ><?php echo $category_name;?></option>
                            <?php

                        }
                    }
                    ?>
                </select>

                <?php }else{  ?>
                No Category Added. Please Add Category.
                    <input type='hidden' name='parent_category_id<?php echo $category_id; ?>' value='' id='parent_category_id<?php echo $category_id; ?>' style="width: 0.2%; border: 0px;">
                <?php
                }
                ?>
            </div>
            <div id="subcategory_div<?php echo $category_id; ?>">
                <input type='hidden' name='grand_parent_category_id<?php echo $category_id; ?>' value='0' id='grand_parent_category_id<?php echo $category_id; ?>'>
            </div>
        <?php
        }else if($category_type=="3"){
        ?>
            <div class="form-group">
                <label for="inputEmail3"  class="col-form-label">Category</label>
                <?php
                $clients_details=$this->db->query("select * from wc_category where category_type='1' and client_id='$client_id' and deleted='0' and status='Active' ")->result_array();
                if(!empty($clients_details))
                {
                ?>
                    <select name="grand_parent_category_id<?php echo $category_id; ?>" id="grand_parent_category_id<?php echo $category_id; ?>" class="custom-select"  style="width: 100%" onchange="getSubCategoryDiv('<?php echo $category_id ?>','<?php echo $parent_category_id_sel ?>');"  >
                        <option value=''>Select</option>
                        <?php
                         $clients_details=$this->db->query("select * from wc_category where category_type='1' and client_id='$client_id' and deleted='0' and status='Active' ")->result_array();
                        if(!empty($clients_details)){
                            foreach($clients_details as $key => $clientsdetails){
                                $category_id11=$clientsdetails['category_id'];
                                $category_name=$clientsdetails['category_name'];
                                ?>
                                <option value="<?php echo $category_id11;?>" <?php if($grand_parent_category_id_sel==$category_id11) { ?> selected="selected" <?php } ?>  ><?php echo $category_name;?></option>
                                <?php
                            }
                        }
                        ?>
                    </select>
                <?php
            }else{   ?>
                No Category Added. Please Add Category.
                <input type='hidden' name='parent_category_id<?php echo $category_id; ?>' value='' id='parent_category_id<?php echo $category_id; ?>' style="width: 0.2%; border: 0px;">
    <?php   }   ?>
            </div>
        </div>
                    
        <div id="subcategory_div<?php echo $category_id; ?>">
        <div class="form-group">
            <label for="inputEmail3"  class="col-form-label">Sub Category</label>
            <?php
            $clients_details=$this->db->query("select * from wc_category where category_type='2' and client_id='$client_id' and deleted='0' and status='Active' ")->result_array();
            if(!empty($clients_details)){

            ?>
            <!-- class="form-control" -->
            <select name="parent_category_id<?php echo $category_id; ?>" id="parent_category_id<?php echo $category_id; ?>" class="custom-select"  style="width: 100%" >
             <option value=''>Select</option>
            <?php
             $clients_details=$this->db->query("select * from wc_category where category_type='2' and client_id='$client_id' and deleted='0' and status='Active' ")->result_array();
            if(!empty($clients_details))
            {
            foreach($clients_details as $key => $clientsdetails)
            {
            $category_id11=$clientsdetails['category_id'];
            $category_name=$clientsdetails['category_name'];
            ?>
            <option value="<?php echo $category_id11;?>" <?php if($grand_parent_category_id_sel==$category_id11) { ?> selected="selected" <?php } ?>  ><?php echo $category_name;?></option>
            <?php

            }
            }
            ?>
            
             </select>

                             <?php
            }
            else
            {
                ?>
                No Sub Category Added. Please add Sub category.
                <input type='hidden' name='grand_parent_category_id<?php echo $category_id; ?>' value='' id='grand_parent_category_id<?php echo $category_id; ?>' style="width: 0.2%; border: 0px;">
                <?php
            }
                ?>
                   
            </div>
        </div>
    <?php
        }
    }






    public function showSubCategoryDiv(){
        $category_id=$this->input->post('category_id');
        $category_type=$this->input->post('category_type');
        $client_id=$this->input->post('client_id');
        $grand_parent_category_id=$this->input->post('grand_parent_category_id');
        $parent_category_id_sel=$this->input->post('parent_category_id');
        $clients_details=$this->db->query("select * from wc_category where category_type='2' and client_id='$client_id' and parent_category_id='$grand_parent_category_id' and deleted='0' and status='Active' ")->result_array();
        if(!empty($clients_details)){ ?>
            <div class="form-group">
                <label for="inputEmail3"  class="col-form-label">Sub Category</label>
                <select name="parent_category_id<?php echo $category_id; ?>" id="parent_category_id<?php echo $category_id; ?>" class="custom-select"  style="width: 100%" >
                    <option value=''>Select</option>
                    <?php
                    $clients_details=$this->db->query("select * from wc_category where category_type='2' and client_id='$client_id' and parent_category_id='$grand_parent_category_id' and deleted='0' and status='Active' ")->result_array();
                    if(!empty($clients_details)) {
                        foreach($clients_details as $key => $clientsdetails){
                            $category_id11=$clientsdetails['category_id'];
                            $category_name=$clientsdetails['category_name'];
                            ?>
                            <option value="<?php echo $category_id11;?>" <?php if($parent_category_id_sel==$category_id11) { ?> selected="selected" <?php } ?>  ><?php echo $category_name;?></option>
                            <?php

                        }
                    }
                    ?>
                </select>
            </div>
            <?php
        }else{  ?>
            No Sub Category Added
            <input type='hidden' name='grand_parent_category_id<?php echo $category_id; ?>' value='' id='grand_parent_category_id<?php echo $category_id; ?>'>
<?php   } 
    }



}

?>