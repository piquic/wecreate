<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
 
class category extends CI_Controller
{
    public function __construct()
	{
		parent::__construct();		
		$this->load->library('pagination');
		$this->load->database();		
		$this->load->library('Upload');
		$this->load->helper('security');	
		$this->load->library('Form_validation');
		$this->load->helper(array('form', 'html', 'file'));
		$this->load->library('pagination');
		$this->load->library('email');
        $this->load->model('Category_model');
        $this->load->library('userlib');
		
    	if ($this->session->userdata('logged_in')) {
            $session_data = $this->session->userdata('logged_in');
            $data['username'] = $session_data['username'];
            $data['firstname'] = $session_data['firstname'];
            $data['rolecode'] = $session_data['rolecode'];
			/* $timezone = $session_data['timezone'];
			date_default_timezone_set($timezone); */
         
        } else {
            //If no session, redirect to login page
            /*redirect('login', 'refresh');*/
            redirect('administrator', 'refresh');
        }
    }
	
    public function index()
	{
		if ($this->session->userdata('logged_in'))		
		{  
			$session_data = $this->session->userdata('logged_in');
			$data['rolecode'] = $session_data['rolecode'];
			$data['usertype'] = $session_data['rolecode'];
			$data['roles'] = $session_data['roles'];
			$data['title'] = "Users";
			$slang="english";
			$this->lang->load($slang, $slang);
			$data['lang']=$this->lang->language; 
			
			//echo "aaaaaaaaa";exit;
			$this->load->view('admin/category/category_list', $data);
		} 
		else 
		{
			//If no session, redirect to login page
			/*redirect('login', 'refresh');*/
            redirect('administrator', 'refresh');
		}
	}
	
	public function categorylist(){
		if ($this->session->userdata('logged_in')) 
        {
			$session_data=$this->session->userdata('logged_in');
			$usertype = $session_data['rolecode'];
			$userid = $session_data['id'];
			$slang="english";
		    $this->lang->load($slang, $slang);
		    $data['lang']=$this->lang->language; 
    	    $aColumns = array( 'wc_category.category_id','wc_category.category_type','wc_category.parent_category_id','wc_category.grand_parent_category_id','wc_category.category_name','wc_clients.client_name','wc_category.status');
         
			/* Indexed column (used for fast and accurate table cardinality) */
			$sIndexColumn = "category_id";
         
			/* DB table to use */
			$sTable = "wc_category";

            /*
			 * Paging
			 */
			$sLimit = "";
			if ( isset( $_GET['start'] ) && $_GET['length'] != '-1' )
			{
				$sLimit = "LIMIT ".intval( $_GET['start'] ).", ".
					intval( $_GET['length'] );
			}


    
     
			$sOrder = "";
			if ( isset( $_GET['order'] ) )
			{
				$sOrder = "ORDER BY  ";
			
						$sOrder .= $aColumns[0]."
							".($_GET['order'][0]['dir']==='desc' ? 'asc' : 'desc') .", ";
			   
				 
				$sOrder = substr_replace( $sOrder, "", -2 );
				if ( $sOrder == "ORDER BY" )
				{
					$sOrder = "";
				}
			}
	

        
        $commn="1=1 and wc_category.deleted='0' ";
        
		
		//echo $_REQUEST['columns'][1]['search']['value']; exit();
		
		if(!empty($_GET['columns'][1]['search']['value']) ){  
	
		$commn.=" AND wc_category.category_name  LIKE '".$_GET['columns'][1]['search']['value']."%' ";
		}

        $sWhere="where $commn";
         
		
		if (isset($_GET['search']['value']) && $_GET['search']['value'] != "" )
		{
			 $sWhere = "WHERE $commn   AND (";
			for ( $i=0 ; $i<count($aColumns) ; $i++ )
			{
				
				/*if ( isset($_GET['columns'][$i]['searchable']) && $_GET['columns'][$i]['searchable'] == "true" )
				{*/
					$sWhere .= $aColumns[$i]." LIKE '%".( $_GET['search']['value'] )."%' OR ";
				/*}*/
			}
			$sWhere = substr_replace( $sWhere, "", -3 );
			$sWhere .= ')';
		}
		
		
       $join=' INNER JOIN wc_clients ON wc_category.client_id=wc_clients.client_id';
        /*
         * SQL queries
         * Get data to display
         */
        $sQuery = "
            SELECT SQL_CALC_FOUND_ROWS ".str_replace(" , ", " ", implode(", ", $aColumns))."
            FROM   $sTable $join
            $sWhere
            $sOrder
            $sLimit";

       //echo  $sQuery; exit();

        $rResult = $this->db->query($sQuery);
        $rResult = $rResult->result_array();
        //$rResult = mysqli_query( $sQuery) or fatal_error( 'MySQL Error: ' . mysqli_errno() );

        
        /* Data set length after filtering */
        //$sQuery = "SELECT FOUND_ROWS()";
        //$rResultFilterTotal = mysqli_query( $sQuery) or fatal_error( 'MySQL Error: ' . mysqli_errno() );
        //$query = $this->db->query($sQuery);
        //$aResultFilterTotal = $query->result_array();
        //$aResultFilterTotal = mysqli_fetch_array($rResultFilterTotal);
       $iFilteredTotal = count( $rResult);
          
        /* Total data set length */
        $sQuery = "
            SELECT COUNT(".$sIndexColumn.") as count
            FROM   $sTable $join
            $sWhere
        ";
        //$rResultTotal = mysqli_query( $sQuery) or fatal_error( 'MySQL Error: ' . mysqli_errno() );
        //$aResultTotal = mysqli_fetch_array($rResultTotal);

        $query = $this->db->query($sQuery);
        $aResultTotal = $query->result_array();

        //echo "<pre>";
        //print_r($aResultTotal);
         $iTotal = $aResultTotal[0]['count'];
         
        /*
         * Output
         */
        $output = array(
                        "sEcho" => '',
                        "iTotalRecords" => $iFilteredTotal,
                        "iTotalDisplayRecords" => $iTotal,
                        "aaData" => array()
                        );

            $sColumns = array('client_name','category_type','parent_category_id','grand_parent_category_id','category_name','status','action');
         
            //while ( $aRow = mysqli_fetch_array( $rResult ) )
            //echo "<pre>";
           //print_r($output);
         
            foreach($rResult as $aRow)
            {
                $row = array();
            	
				$category_id=$aRow['category_id'];
				$category_type=$aRow['category_type'];
				$parent_category_id=$aRow['parent_category_id'];
				$grand_parent_category_id=$aRow['grand_parent_category_id'];
				//$category_type_id=$aRow['category_type_id'];
				//$category_type=$aRow['usertype_id'];
				//$business_id=$aRow['business_id'];
				$status=$aRow['status'];
                for ( $i=0 ; $i<count($sColumns) ; $i++ )
                {
                   /*  if ( $sColumns[$i] == "version" )
                    {
                       
                        $row[] = ($aRow[ $sColumns[$i] ]=="0") ? '-' : $aRow[ $sColumns[$i] ];
                    } */
					
            	    if($sColumns[$i] =='action')
            		{
            			

						$row[]='<a href="javascript:void(0)"  class="btn btn-success view" val="'.$category_id.'"><i class="fas fa-eye"></i></a>&nbsp;<a href="javascript:void(0)"  class="btn btn-info edit" val="'.$category_id.'"><i class="fas fa-edit"></i></a>&nbsp; <a href="javascript:void(0)"  class="btn btn-danger delete" val="'.$category_id.'"><i class="fas fa-trash-alt"></i></a>
        			  ';
						
            		}
					
					
					
					
					else if($sColumns[$i] =='category_type')
            		{
            			
            			 $category_type_name="";
			            
						 
						 
					    if($category_type=='1')
						{
						 $category_type_name="Category";
			            }
						else if($category_type=='2')
						{
						 $category_type_name="Sub Category";
			             }
						else if($category_type=='3')
						{
						 $category_type_name="Sub Sub Category";
			            }
						
            			
            			 $row[]=$category_type_name;
            			 
            		}

            		else if($sColumns[$i] =='parent_category_id')
            		{
            			if($parent_category_id!='0')
            			{
            				  $category_details=$this->db->query("select * from wc_category where category_id='$parent_category_id' and deleted='0' and status='Active' ")->result_array();
            				 $parent_category_name=$category_details[0]['category_name'];
            			}
            			else
            			{
            				 $parent_category_name="-";
            			}
            			
			            
						 
						 $row[]=$parent_category_name;
            			 
            		}

            		else if($sColumns[$i] =='grand_parent_category_id')
            		{
            			
            			if($grand_parent_category_id!='0')
            			{
            				 $category_details=$this->db->query("select * from wc_category where 
            				 	category_id='$grand_parent_category_id' and deleted='0' and status='Active' ")->result_array();
            				 $parent_sub_category_name=$category_details[0]['category_name'];
            			}
            			else
            			{
            				 $parent_sub_category_name="-";
            			}
			            
						 
						 $row[]=$parent_sub_category_name;
            			 
            		}
					
					
					
					
					else if($sColumns[$i] =='status')
            		{
            			
            			 $Active_status="";
			             $Inactive_status="";
						 
						 
					    if($status=='Active')
						{
						 $Active_status="selected";
			             $Inactive_status="";
						
						}
						else if($status=='Inactive')
						{
						 $Active_status="";
			             $Inactive_status="selected";
						 
						}
						
            			
            			 $row[]='<select name="status" id="status" class="selectpicker update_status">
								 <option value="Active@@@'.$category_id.'"  '.$Active_status.'>Active</option>
								 <option value="Inactive@@@'.$category_id.'"  '.$Inactive_status.'>Inactive</option>
								 </select>';
            			 
            		}
                    else if ( $sColumns[$i] != ' ' )
                    {
                        /* General output */
                        $row[] = $aRow[ $sColumns[$i] ];
                    }
                }
                $output['aaData'][] = $row;
            }
            //print_r($output);exit;
        echo json_encode( $output );
    }
	}
	
	
	public function add($category_id=''){
		
		if ($this->session->userdata('logged_in'))		
		{  
			$session_data = $this->session->userdata('logged_in');
			$slang="english";
		    $this->lang->load($slang, $slang);
		    $data['lang']=$this->lang->language; 
			$data['title'] = "User Role";
			if($category_id!=''){
				$category_details=$this->db->query("select * from wc_category where category_id='$category_id'")->result_array();
				if(!empty($category_details)){
					$data['category_id'] = $category_details[0]['category_id'];
					$data['client_id_sel'] = $category_details[0]['client_id'];
					$data['category_type'] = $category_details[0]['category_type'];
					$data['parent_category_id_sel'] = $category_details[0]['parent_category_id'];
					$data['grand_parent_category_id_sel'] = $category_details[0]['grand_parent_category_id'];
					$data['category_name'] = $category_details[0]['category_name'];
					$data['status'] = $category_details[0]['status'];
					
					
				}
			}else {
			        $data['category_id'] = '';
					$data['client_id_sel'] = '';
					$data['category_type'] = '';
					$data['parent_category_id_sel'] = '';
					$data['grand_parent_category_id_sel'] = '';
					$data['category_name'] = '';
					$data['status'] = '';
					
			}
			$this->load->view('admin/category/category_management', $data);
		} 
		else 
		{
			//If no session, redirect to login page
			/*redirect('login', 'refresh');*/
            redirect('administrator', 'refresh');
		}
	}
	
	
		
	  public function checkexistemail()
{			// checking the email  exist  validation in add form
	
    $category_email = $this->input->post('category_email');
    $sql = "SELECT category_id,category_email FROM wc_category WHERE category_email = '$category_email'";
    $query = $this->db->query($sql);
    if( $query->num_rows() > 0 ){
        echo 'false';
    } else {
        echo 'true';
    }
	
}

   public function checkexistmobile()
{			// checking the email  exist  validation in add form
	
    $client_phone = $this->input->post('client_phone');
    $sql = "SELECT category_id,client_phone FROM wc_category WHERE client_phone = '$client_phone'";
    $query = $this->db->query($sql);
    if( $query->num_rows() > 0 ){
        echo 'false';
    } else {
        echo 'true';
    }
	
}
	
	public function insert_category(){
		
		if ($this->session->userdata('logged_in')) {
			$session_data = $this->session->userdata('logged_in');
			$created_date = date('Y-m-d');
			$userid=$session_data['id'];
			$category_id=$this->input->post('category_id');
			$client_id=$this->input->post('client_id');
			$category_name=$this->input->post('category_name');

			$category_type=$this->input->post('category_type');
			$parent_category_id=$this->input->post('parent_category_id');
			$grand_parent_category_id=$this->input->post('grand_parent_category_id');
			
			if($category_id!='')
			{
				
				  $data = array(
				    'category_id'=>$this->input->post('category_id'),
					'client_id'=>$this->input->post('client_id'),
					'category_name'=>$this->input->post('category_name'),
					'status'=>$this->input->post('status'),

					'category_type'=>$this->input->post('category_type'),
					'parent_category_id'=>$this->input->post('parent_category_id'),
					'grand_parent_category_id'=>$this->input->post('grand_parent_category_id'),
					);
					$client_query= $this->db->query("SELECT * FROM wc_category WHERE category_id='$category_id'");
                $client_rs= $client_query->result_array(); 

                $oldclient_id=$client_rs[0]['client_id'];
                $oldcategory_name=preg_replace('/[^a-zA-Z0-9_.]/', '_',$client_rs[0]['category_name']);

                if($oldclient_id==$client_id){

                	$client_query1= $this->db->query("SELECT * FROM wc_clients WHERE client_id='$client_id'");
	                $client_rs1= $client_query1->result_array(); 

	                $client_name=preg_replace('/[^a-zA-Z0-9_.]/', '_',$client_rs1[0]['client_name']);
                	/*$old_category_name=$_SERVER['DOCUMENT_ROOT']."/"."WeCreate/upload/".$client_id."-".$client_name."/".$category_id."-".$oldcategory_name;
                	$new_category_name=$_SERVER['DOCUMENT_ROOT']."/"."WeCreate/upload/".$client_id."-".$client_name."/".$category_id."-".$category_name;*/


                	$old_category_name=$_SERVER['DOCUMENT_ROOT']."/".FOLDER_NAME."/upload/".$client_id."-".$client_name."/".$category_id."-".$oldcategory_name;
                	$new_category_name=$_SERVER['DOCUMENT_ROOT']."/".FOLDER_NAME."/upload/".$client_id."-".$client_name."/".$category_id."-".$category_name;




                	if(rename( $old_category_name, $new_category_name)){ 
	                   // echo "Successfully Renamed $old_client_name to $new_client_name" ;
	                }else{
	                    //echo "A File With The Same Name Already Exists" ;
	                }
                }
                if($oldclient_id!=$client_id){
                	$client_query1= $this->db->query("SELECT * FROM wc_clients WHERE client_id='$oldclient_id'");
	                $client_rs1= $client_query1->result_array();
	                $oldclient_name=preg_replace('/[^a-zA-Z0-9_.]/', '_',$client_rs1[0]['client_name']);

	                $new_client_query= $this->db->query("SELECT * FROM wc_clients WHERE client_id='$client_id'");
	                $new_client_rs= $new_client_query->result_array(); 
	                $newclient_name=preg_replace('/[^a-zA-Z0-9_.]/', '_',$new_client_rs[0]['client_name']);


	                // echo $_SERVER['DOCUMENT_ROOT']."WeCreate/upload/".$client_id."-".$client_rs[0]['client_name'];
	                /*$old_client_name=$_SERVER['DOCUMENT_ROOT']."/"."WeCreate/upload/".$oldclient_id."-".$oldclient_name;
	                $new_client_name=$_SERVER['DOCUMENT_ROOT']."/"."WeCreate/upload/".$client_id."-".$newclient_name;*/

	                $old_client_name=$_SERVER['DOCUMENT_ROOT']."/".FOLDER_NAME."/upload/".$oldclient_id."-".$oldclient_name;
	                $new_client_name=$_SERVER['DOCUMENT_ROOT']."/".FOLDER_NAME."/upload/".$client_id."-".$newclient_name;

	                if(rename( $old_client_name, $new_client_name)){ 
	                    /*$old_category_name=$_SERVER['DOCUMENT_ROOT']."/"."WeCreate/upload/".$client_id."-".$newclient_name."/".$category_id."-".$oldcategory_name;
	                	$new_category_name=$_SERVER['DOCUMENT_ROOT']."/"."WeCreate/upload/".$client_id."-".$newclient_name."/".$category_id."-".$category_name;*/

	                	$old_category_name=$_SERVER['DOCUMENT_ROOT']."/".FOLDER_NAME."/upload/".$client_id."-".$newclient_name."/".$category_id."-".$oldcategory_name;
	                	$new_category_name=$_SERVER['DOCUMENT_ROOT']."/".FOLDER_NAME."/upload/".$client_id."-".$newclient_name."/".$category_id."-".$category_name;

	                	if(rename( $old_category_name, $new_category_name)){ 
		                   // echo "Successfully Renamed $old_client_name to $new_client_name" ;
		                }else{
		                    //echo "A File With The Same Name Already Exists" ;
		                }
	                }else{
	                    //echo "A File With The Same Name Already Exists" ;
	                }
                }
               
				  $updateuserrole = $this->Category_model->category_update($data,$category_id);
			      echo $category_id;
				
			}else {
				
				
				
				    $data = array(
				    'category_id'=>$this->input->post('category_id'),
					'client_id'=>$this->input->post('client_id'),
					'category_name'=>$this->input->post('category_name'),
					'status'=>$this->input->post('status'),

					'category_type'=>$this->input->post('category_type'),
					'parent_category_id'=>$this->input->post('parent_category_id'),
					'grand_parent_category_id'=>$this->input->post('grand_parent_category_id'),
					);
					$this->Category_model->category_insert($data);
					$category_id=$this->db->insert_id();
					
					
					echo $category_id;
				}
			
		}else {
		
            /*redirect('login', 'refresh');*/
            redirect('administrator', 'refresh');
       
		}  
	}
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
	public function change_password(){
		
		if ($this->session->userdata('logged_in'))		
		{  
			$session_data = $this->session->userdata('logged_in');
			$slang="english";
		    $this->lang->load($slang, $slang);
		    $data['lang']=$this->lang->language; 
			
			$category_id = $session_data['id'];
			$data['category_id'] = $category_id;
			$data['title'] = "User Role";
			if($category_id!=''){
				$category_details=$this->db->query("select * from wc_category where category_id='$category_id'")->result_array();
				if(!empty($category_details)){}
			}else {}
			$this->load->view('admin/category/category_password', $data);
		} 
		else 
		{
			//If no session, redirect to login page
			/*redirect('login', 'refresh');*/
            redirect('administrator', 'refresh');
		}
	}
	
	public function update_change_password()			//for displaying the data of the staff into the table
   {
	
	       $category_id=$this->input->post('category_id');
	          $data = array(
				'category_password' => sha1($this->input->post('category_password'))
                 );
				//print_r($staffdata);
				$updateuserrole = $this->Category_model->category_update($data, $category_id);
			    echo $category_id;
				
				
				//redirect('manage-employee');
   }
   
 
	public function deletecategory()
    {
		if ($this->session->userdata('logged_in')) 
		{
			$category_id = $this->input->post('category_id');
		
			//$this->db->query("delete from wc_category where category_id='$category_id'");

			$this->db->query("update wc_category set deleted ='1',status='Inactive' where category_id ='$category_id'");
			$this->session->set_flashdata('added', '<div align="left" style="color:green; font-size:15px; border:1px #398AB9 solid;">
		    Service Deleted Successfully</div>');

			//redirect('users');
		} 
		else 
		{
			//If no session, redirect to login page
			/*redirect('login', 'refresh');*/
            redirect('administrator', 'refresh');
		}
	}
	
	
	
	
	public function view($category_id=''){
		
		if ($this->session->userdata('logged_in'))		
		{  
			$session_data = $this->session->userdata('logged_in');
			$slang="english";
		    $this->lang->load($slang, $slang);
		    $data['lang']=$this->lang->language; 
			$data['title'] = "User Role";
			if($category_id!=''){
				$category_details=$this->db->query("select * from wc_category where category_id='$category_id'")->result_array();
				if(!empty($category_details)){
					$data['category_id'] = $category_details[0]['category_id'];
					$data['client_id_sel'] = $category_details[0]['client_id'];
					$data['category_type'] = $category_details[0]['category_type'];
					$data['parent_category_id_sel'] = $category_details[0]['parent_category_id'];
					$data['grand_parent_category_id_sel'] = $category_details[0]['grand_parent_category_id'];
					$data['category_name'] = $category_details[0]['category_name'];
					$data['status'] = $category_details[0]['status'];
					
					
				}
			}else {
			        $data['category_id'] = '';
					$data['client_id_sel'] = '';
					$data['category_type'] = '';
					$data['parent_category_id_sel'] = '';
					$data['grand_parent_category_id_sel'] = '';
					$data['category_name'] = '';
					$data['status'] = '';
					
			}
			$this->load->view('admin/category/category_view', $data);
		} 
		else 
		{
			//If no session, redirect to login page
			/*redirect('login', 'refresh');*/
            redirect('administrator', 'refresh');
		}
	}
public function updatestatus()
{				//for email exist validation in edit customer form 
	
	
  $id=trim($this->input->get('id'));
  $status=trim($this->input->get('status'));
  //echo "update wc_category set status ='$status' where category_id ='$id'";
  $this->db->query("update wc_category set status ='$status' where category_id ='$id'");


		
}




	public function showCategoryDiv()			
   {
		$category_type=$this->input->post('category_type');
		$client_id=$this->input->post('client_id');
		$parent_category_id_sel=$this->input->post('parent_category_id');
		$grand_parent_category_id_sel=$this->input->post('grand_parent_category_id');
		
	    ?>

	       <?php


	       if($category_type=="1")
	       {

	       	?>
			<input type='hidden' name='parent_category_id' value='0' id='parent_category_id'>

			<div id="subcategory_div">
			<input type='hidden' name='grand_parent_category_id' value='0' id='grand_parent_category_id'>
			</div>

	       	<?php

	       }
	       else if($category_type=="2")
	       {

	       	
	       	?>
			<div class="form-group">
					    <label for="inputEmail3"  class="col-sm-2 control-label">Category
                        <!-- <span style="color:#F00">*</span> -->
                        </label>
						<div class="col-sm-8">

							<?php
							$clients_details=$this->db->query("select * from wc_category where category_type='1' and client_id='$client_id' and deleted='0' and status='Active' ")->result_array();
							if(!empty($clients_details))
							{
							?>
						    	<!-- class="form-control" -->
								<select name="parent_category_id" id="parent_category_id"  style="width: 100%" >
								 <option value=''>Select</option>
								<?php
								 $clients_details=$this->db->query("select * from wc_category where category_type='1' and client_id='$client_id' and deleted='0' and status='Active' ")->result_array();
								if(!empty($clients_details))
								{
								foreach($clients_details as $key => $clientsdetails)
								{
								$category_id=$clientsdetails['category_id'];
								$category_name=$clientsdetails['category_name'];
								?>
								<option value="<?php echo $category_id;?>" <?php if($parent_category_id_sel==$category_id) { ?> selected="selected" <?php } ?>  ><?php echo $category_name;?></option>
								<?php

								}
								}
								?>
								
								 </select>

								<?php
								}
								else
								{
								?>
								No Category Added. Please Add Category.
								<input type='text' name='parent_category_id' value='' id='parent_category_id' style="width: 0.2%; border: 0px;">
								<?php
								}
								?>
				


						</div>
						
					</div>

					

			<div id="subcategory_div">
			<input type='hidden' name='grand_parent_category_id' value='0' id='grand_parent_category_id'>
			</div>

	       	<?php

	       }
	       else if($category_type=="3")
	       {
	       		
	       	?>
			<div class="form-group">
					    <label for="inputEmail3"  class="col-sm-2 control-label">Category
                        <!-- <span style="color:#F00">*</span> -->
                        </label>
						<div class="col-sm-8">
						<?php
						$clients_details=$this->db->query("select * from wc_category where category_type='1' and client_id='$client_id' and deleted='0' and status='Active' ")->result_array();
						if(!empty($clients_details))
						{
						?>
						    	<!-- class="form-control" -->
								<select name="grand_parent_category_id" id="grand_parent_category_id"  style="width: 100%" onchange="getSubCategoryDiv();"  >
								 <option value=''>Select</option>
								<?php
								 $clients_details=$this->db->query("select * from wc_category where category_type='1' and client_id='$client_id' and deleted='0' and status='Active' ")->result_array();
								if(!empty($clients_details))
								{
								foreach($clients_details as $key => $clientsdetails)
								{
								$category_id=$clientsdetails['category_id'];
								$category_name=$clientsdetails['category_name'];
								?>
								<option value="<?php echo $category_id;?>" <?php if($grand_parent_category_id_sel==$category_id) { ?> selected="selected" <?php } ?>  ><?php echo $category_name;?></option>
								<?php

								}
								}
								?>
								
								 </select>

								<?php
								}
								else
								{
								?>
								No Category Added. Please Add Category.
								<input type='text' name='parent_category_id' value='' id='parent_category_id' style="width: 0.2%; border: 0px;">
								<?php
								}
								?>



						</div>
						
					</div>


					
			<div id="subcategory_div">
				

			<div class="form-group">
					    <label for="inputEmail3"  class="col-sm-2 control-label">Sub Category
                        <!-- <span style="color:#F00">*</span> -->
                        </label>
						<div class="col-sm-8">

							<?php
				 $clients_details=$this->db->query("select * from wc_category where category_type='2' and client_id='$client_id' and deleted='0' and status='Active' ")->result_array();
								if(!empty($clients_details))
								{

				?>
						    	<!-- class="form-control" -->
								<select name="parent_category_id" id="parent_category_id"  style="width: 100%" >
								 <option value=''>Select</option>
								<?php
								 $clients_details=$this->db->query("select * from wc_category where category_type='2' and client_id='$client_id' and deleted='0' and status='Active' ")->result_array();
								if(!empty($clients_details))
								{
								foreach($clients_details as $key => $clientsdetails)
								{
								$category_id=$clientsdetails['category_id'];
								$category_name=$clientsdetails['category_name'];
								?>
								<option value="<?php echo $category_id;?>" <?php if($grand_parent_category_id_sel==$category_id) { ?> selected="selected" <?php } ?>  ><?php echo $category_name;?></option>
								<?php

								}
								}
								?>
								
								 </select>

								 <?php
				}
				else
				{
					?>
					No Sub Category Added. Please add Sub category.
					<input type='text' name='grand_parent_category_id' value='' id='grand_parent_category_id' style="width: 0.2%; border: 0px;">
					<?php
				}
					?>
						</div>
						
					</div>

					


			</div>

	       	<?php

	       }

	    
				
				
				
   }






	public function showSubCategoryDiv()			
   {
		$category_type=$this->input->post('category_type');
		$client_id=$this->input->post('client_id');
		$grand_parent_category_id=$this->input->post('grand_parent_category_id');
		$parent_category_id_sel=$this->input->post('parent_category_id');
		
	    ?>


				<?php
				
				 $clients_details=$this->db->query("select * from wc_category where category_type='2' and client_id='$client_id' and parent_category_id='$grand_parent_category_id' and deleted='0' and status='Active' ")->result_array();
								if(!empty($clients_details))
								{

				?>

			<div class="form-group">
					    <label for="inputEmail3"  class="col-sm-2 control-label">Sub Category
                        <!-- <span style="color:#F00">*</span> -->
                        </label>
						<div class="col-sm-8">
						    	<!-- class="form-control" -->
								<select name="parent_category_id" id="parent_category_id"  style="width: 100%" >
								 <option value=''>Select</option>
								<?php
								 $clients_details=$this->db->query("select * from wc_category where category_type='2' and client_id='$client_id' and parent_category_id='$grand_parent_category_id' and deleted='0' and status='Active' ")->result_array();
								if(!empty($clients_details))
								{
								foreach($clients_details as $key => $clientsdetails)
								{
								$category_id=$clientsdetails['category_id'];
								$category_name=$clientsdetails['category_name'];
								?>
								<option value="<?php echo $category_id;?>" <?php if($parent_category_id_sel==$category_id) { ?> selected="selected" <?php } ?>  ><?php echo $category_name;?></option>
								<?php

								}
								}
								?>
								
								 </select>
						</div>
						
					</div>

					<?php
				}
				else
				{
					?>
					No Sub Category Added
					<input type='text' name='grand_parent_category_id' value='' id='grand_parent_category_id'>
					<?php
				}
					?>


			

       <?php
	    
				
				
				
   }







	
	
}
?>