<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Client extends CI_Controller
{
    public function __construct()
	{
		parent::__construct();		
		$this->load->library('pagination');
		$this->load->database();		
		$this->load->library('Upload');
		$this->load->helper('security');	
		$this->load->library('Form_validation');
		$this->load->helper(array('form', 'html', 'file'));
		$this->load->library('pagination');
		$this->load->library('email');
        $this->load->model('Client_model');
        $this->load->library('userlib');
		
    	if ($this->session->userdata('logged_in')) {
            $session_data = $this->session->userdata('logged_in');
            $data['username'] = $session_data['username'];
            $data['firstname'] = $session_data['firstname'];
            $data['rolecode'] = $session_data['rolecode'];
			/* $timezone = $session_data['timezone'];
			date_default_timezone_set($timezone); */
         
        } else {
            //If no session, redirect to login page
            /*redirect('login', 'refresh');*/
            redirect('administrator', 'refresh');
        }
    }
	
    public function index()
	{
		if ($this->session->userdata('logged_in'))		
		{  
			$session_data = $this->session->userdata('logged_in');
			$data['rolecode'] = $session_data['rolecode'];
			$data['usertype'] = $session_data['rolecode'];
			$data['roles'] = $session_data['roles'];
			$data['title'] = "Users";
			$slang="english";
			$this->lang->load($slang, $slang);
			$data['lang']=$this->lang->language; 
			
			//echo "aaaaaaaaa";exit;
			$this->load->view('admin/client/client_list', $data);
		} 
		else 
		{
			//If no session, redirect to login page
			/*redirect('login', 'refresh');*/
            redirect('administrator', 'refresh');
		}
	}
	
	public function clientlist(){
		if ($this->session->userdata('logged_in')) 
        {
			$session_data=$this->session->userdata('logged_in');
			$usertype = $session_data['rolecode'];
			$userid = $session_data['id'];
			$slang="english";
		    $this->lang->load($slang, $slang);
		    $data['lang']=$this->lang->language; 
    	    $aColumns = array( 'client_id','client_image','client_name','client_email','client_phone','status');
         
			/* Indexed column (used for fast and accurate table cardinality) */
			$sIndexColumn = "client_id";
         
			/* DB table to use */
			$sTable = "wc_clients";

            /*
			 * Paging
			 */
			$sLimit = "";
			if ( isset( $_GET['start'] ) && $_GET['length'] != '-1' )
			{
				$sLimit = "LIMIT ".intval( $_GET['start'] ).", ".
					intval( $_GET['length'] );
			}


    
     
			$sOrder = "";
			if ( isset( $_GET['order'] ) )
			{
				$sOrder = "ORDER BY  ";
			
						$sOrder .= $aColumns[0]."
							".($_GET['order'][0]['dir']==='desc' ? 'asc' : 'desc') .", ";
			   
				 
				$sOrder = substr_replace( $sOrder, "", -2 );
				if ( $sOrder == "ORDER BY" )
				{
					$sOrder = "";
				}
			}
	

        
        $commn="1=1 and deleted='0' ";
        
		
		//echo $_REQUEST['columns'][1]['search']['value']; exit();
		
		if(!empty($_GET['columns'][1]['search']['value']) ){  
	
		$commn.=" AND user.client_name  LIKE '".$_GET['columns'][1]['search']['value']."%' ";
		}

        $sWhere="where $commn";
         
		
		if (isset($_GET['search']['value']) && $_GET['search']['value'] != "" )
		{
			 $sWhere = "WHERE $commn   AND (";
			for ( $i=0 ; $i<count($aColumns) ; $i++ )
			{
				
				/*if ( isset($_GET['columns'][$i]['searchable']) && $_GET['columns'][$i]['searchable'] == "true" )
				{*/
					$sWhere .= $aColumns[$i]." LIKE '%".( $_GET['search']['value'] )."%' OR ";
				/*}*/
			}
			$sWhere = substr_replace( $sWhere, "", -3 );
			$sWhere .= ')';
		}
		
		
       $join='';
        /*
         * SQL queries
         * Get data to display
         */
        $sQuery = "
            SELECT SQL_CALC_FOUND_ROWS ".str_replace(" , ", " ", implode(", ", $aColumns))."
            FROM   $sTable $join
            $sWhere
            $sOrder
            $sLimit";

       //echo  $sQuery; exit();

        $rResult = $this->db->query($sQuery);
        $rResult = $rResult->result_array();
        //$rResult = mysqli_query( $sQuery) or fatal_error( 'MySQL Error: ' . mysqli_errno() );

        
        /* Data set length after filtering */
        //$sQuery = "SELECT FOUND_ROWS()";
        //$rResultFilterTotal = mysqli_query( $sQuery) or fatal_error( 'MySQL Error: ' . mysqli_errno() );
        //$query = $this->db->query($sQuery);
        //$aResultFilterTotal = $query->result_array();
        //$aResultFilterTotal = mysqli_fetch_array($rResultFilterTotal);
       $iFilteredTotal = count( $rResult);
          
        /* Total data set length */
        $sQuery = "
            SELECT COUNT(".$sIndexColumn.") as count
            FROM   $sTable $join
            $sWhere
        ";
        //$rResultTotal = mysqli_query( $sQuery) or fatal_error( 'MySQL Error: ' . mysqli_errno() );
        //$aResultTotal = mysqli_fetch_array($rResultTotal);

        $query = $this->db->query($sQuery);
        $aResultTotal = $query->result_array();

        //echo "<pre>";
        //print_r($aResultTotal);
         $iTotal = $aResultTotal[0]['count'];
         
        /*
         * Output
         */
        $output = array(
                        "sEcho" => '',
                        "iTotalRecords" => $iFilteredTotal,
                        "iTotalDisplayRecords" => $iTotal,
                        "aaData" => array()
                        );

            //$sColumns = array('client_id','client_image','client_name','client_phone','client_email','status','action');
            $sColumns = array('client_name','client_image','client_email','client_phone','status','action');
         
            //while ( $aRow = mysqli_fetch_array( $rResult ) )
            //echo "<pre>";
           //print_r($output);
         
            foreach($rResult as $aRow)
            {
                $row = array();
            	
				$client_id=$aRow['client_id'];
				$client_image=$aRow['client_image'];
				//$user_type_id=$aRow['user_type_id'];
				//$user_type=$aRow['usertype_id'];
				//$business_id=$aRow['business_id'];
				$status=$aRow['status'];
                for ( $i=0 ; $i<count($sColumns) ; $i++ )
                {
                   /*  if ( $sColumns[$i] == "version" )
                    {
                       
                        $row[] = ($aRow[ $sColumns[$i] ]=="0") ? '-' : $aRow[ $sColumns[$i] ];
                    } */
					
            	    if($sColumns[$i] =='action')
            		{
            			

						$row[]='<a href="javascript:void(0)"  class="btn btn-success view" val="'.$client_id.'"><i class="fas fa-eye"></i></a>&nbsp;<a href="javascript:void(0)"  class="btn btn-info edit" val="'.$client_id.'"><i class="fas fa-edit"></i></a>&nbsp; <a href="javascript:void(0)"  class="btn btn-danger delete" val="'.$client_id.'"><i class="fas fa-trash-alt"></i></a>
        			  ';
						
            		}
					
				
					else if($sColumns[$i] =='status')
            		{
            			
            			 $Active_status="";
			             $Inactive_status="";
						 
						 
					    if($status=='Active')
						{
						 $Active_status="selected";
			             $Inactive_status="";
						
						}
						else if($status=='Inactive')
						{
						 $Active_status="";
			             $Inactive_status="selected";
						 
						}
						
            			
            			 $row[]='<select name="status" id="status" class="selectpicker update_status">
								 <option value="Active@@@'.$client_id.'"  '.$Active_status.'>Active</option>
								 <option value="Inactive@@@'.$client_id.'"  '.$Inactive_status.'>Inactive</option>
								 </select>';
            			 
            		}
            		else if ( $sColumns[$i] == 'client_image' )
                    {
                        /* General output */
                         $row[]='<img src="'.base_url().'/upload_client_logo/'.$client_image.'"  style="width: 50%;">';
                    }
                    else if ( $sColumns[$i] != ' ' )
                    {
                        /* General output */
                        $row[] = $aRow[ $sColumns[$i] ];
                    }
                }
                $output['aaData'][] = $row;
            }
            //print_r($output);exit;
        echo json_encode( $output );
    }
	}
	
	
	public function add($client_id=''){
		
		if ($this->session->userdata('logged_in'))		
		{  
			$session_data = $this->session->userdata('logged_in');
			$slang="english";
		    $this->lang->load($slang, $slang);
		    $data['lang']=$this->lang->language; 
			$data['title'] = "User Role";
			if($client_id!=''){
				$user_details=$this->db->query("select * from wc_clients where client_id='$client_id'")->result_array();
				if(!empty($user_details)){
					$data['client_id'] = $user_details[0]['client_id'];
					$data['client_name'] = $user_details[0]['client_name'];
					$data['client_phone'] = $user_details[0]['client_phone'];
					$data['client_email'] = $user_details[0]['client_email'];
                    $data['project_description']=$user_details[0]['project_description'];
					$data['status'] = $user_details[0]['status'];
                    $data['allow_delivary_date_bm'] = $user_details[0]['allow_delivary_date_bm'];
					$data['pimg'] = $user_details[0]['client_image'];

                    $wekan_details_arr=$this->db->query("select wekan_board_list_id from wc_client_wekan_list_relation where client_id='$client_id'")->result_array();

                    $client_wekan_list_arr[]="";

                    foreach($wekan_details_arr as $key =>$value)
                    {
                        $client_wekan_list_arr[]=$value['wekan_board_list_id'];

                    }

                    $data['client_wekan_list_arr'] = $client_wekan_list_arr;



					
					
				}
			}else {
			        $data['client_id'] = '';
			        $data['client_name'] = '';
					$data['client_phone'] = '';
					$data['client_email'] = '';
                    $data['project_description']='Your need to add project title and attach project doc before you proceed next.';
                    
					$data['status'] = '';
                    $data['allow_delivary_date_bm'] = '';
					$data['pimg'] = '';
                    $data['client_wekan_list_arr'] =array();
					
			}
			$this->load->view('admin/client/client_management', $data);
		} 
		else 
		{
			//If no session, redirect to login page
			/*redirect('login', 'refresh');*/
            redirect('administrator', 'refresh');
		}
	}

    


    public function show_wekanboardlist()
    {           
    // checking the email  exist  validation in add form

    $client_id = $this->input->post('client_id');
    $wekan_board_list = $this->input->post('wekan_board_list');

    $wekan_details_arr=$this->db->query("select wekan_board_list_id from wc_client_wekan_list_relation where client_id='$client_id'")->result_array();

    $client_wekan_list_arr[]="";
    foreach($wekan_details_arr as $key =>$value)
    {
    $client_wekan_list_arr[]=$value['wekan_board_list_id'];
    }

    $wekan_board_list_arr=explode(",",$wekan_board_list);
    ?>

                 
                        <?php

                        //echo "<pre>";
                        //print_r($client_wekan_list_arr);

                        $clients_details=$this->db->query("select * from wc_wekan_board_list where  deleted='0' and status='Active' ")->result_array();
                        if(!empty($clients_details))
                        {
                        foreach($clients_details as $key => $clientsdetails)
                        {
                        $wekan_board_list_id=$clientsdetails['wekan_board_list_id'];
                        $wekan_board_list=$clientsdetails['wekan_board_list'];
                        ?>
                        <div class="form-check">
                        <input class="form-check-input wekan_board_list_class" type="checkbox" value="<?php echo $wekan_board_list_id;?>" name="wekan_board_list[]" id="wekan_board_list<?php echo $wekan_board_list_id;?>" <?php if(in_array($wekan_board_list_id, $wekan_board_list_arr)) { ?> checked   <?php } ?>   <?php if(in_array($wekan_board_list_id, $client_wekan_list_arr)) { ?> checked disabled  <?php } ?> >
                        <label class="form-check-label" for="wekan_board_list<?php echo $wekan_board_list_id;?>">
                        <?php echo $wekan_board_list;?>
                        </label>
                        </div>
                        <?php

                        }
                        }
                        ?>
                                
                      
    <?php

    }
	
	
		
	  public function checkexistemail()
{			// checking the email  exist  validation in add form
	
    $client_email = $this->input->post('client_email');
    $sql = "SELECT client_id,client_email FROM wc_clients WHERE client_email = '$client_email'";
    $query = $this->db->query($sql);
    if( $query->num_rows() > 0 ){
        echo 'false';
    } else {
        echo 'true';
    }
	
}

	public function checkexistmobile()
	{			// checking the email  exist  validation in add form

	$client_phone = $this->input->post('client_phone');
	$sql = "SELECT client_id,client_phone FROM wc_clients WHERE client_phone = '$client_phone'";
	$query = $this->db->query($sql);
	if( $query->num_rows() > 0 ){
	echo 'false';
	} else {
	echo 'true';
	}

	}


	public function insert_borad_wekan($client_name,$wekan_board_list){


    $query=$this->db->query("select * from wc_settings WHERE setting_key='WEKAN_LINK' ");
    $userDetails= $query->result_array();
    $WEKAN_LINK=$userDetails[0]['setting_value'];

    $query=$this->db->query("select * from wc_settings WHERE setting_key='WEKAN_USERNAME' ");
    $userDetails= $query->result_array();
    $WEKAN_USERNAME=$userDetails[0]['setting_value'];


    $query=$this->db->query("select * from wc_settings WHERE setting_key='WEKAN_PASSWORD' ");
    $userDetails= $query->result_array();
    $WEKAN_PASSWORD=$userDetails[0]['setting_value'];

    /*$query=$this->db->query("select * from wc_settings WHERE setting_key='WEKAN_BOARD_ID' ");
    $userDetails= $query->result_array();
    $WEKAN_BOARD_ID=$userDetails[0]['setting_value'];*/

   /* $query=$this->db->query("select * from wc_settings WHERE setting_key='WEKAN_LIST_ID_PENDING' ");
    $userDetails= $query->result_array();
    $WEKAN_LIST_ID_PENDING=$userDetails[0]['setting_value'];*/

    if ($this->session->userdata('wekan_integration')) {



        $wekan_session_data = $this->session->userdata('wekan_integration');
        $wekan_authorId = $wekan_session_data['wekan_authorId'];
        $wekan_token = $wekan_session_data['wekan_token'];
        $wekan_tokenExpires = $wekan_session_data['wekan_tokenExpires'];

         //echo "<pre>";
        //print_r($wekan_session_data);exit;
    }
    else
    {

        $curl = curl_init();
        curl_setopt_array($curl, array(
        CURLOPT_URL => $WEKAN_LINK."users/login",
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_ENCODING => "",
        CURLOPT_MAXREDIRS => 10,
        CURLOPT_TIMEOUT => 30,
        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
        CURLOPT_CUSTOMREQUEST => "POST",
        CURLOPT_POSTFIELDS => "username=".$WEKAN_USERNAME."&password=".$WEKAN_PASSWORD."",
        CURLOPT_HTTPHEADER => array(
        "cache-control: no-cache",
        "content-type: application/x-www-form-urlencoded"
        ),
        ));
        $response = curl_exec($curl);
        $err = curl_error($curl);

        curl_close($curl);

        if (!$err)
        {
        //var_dump($response);



        $res_arr=json_decode($response);

        //echo "<pre>";
        //print_r($res_arr);exit;
        $id=$res_arr->id;
        $token=$res_arr->token;
        $tokenExpires=$res_arr->tokenExpires;


        $sess_array = array(
        'wekan_authorId' => $id,
        'wekan_token' => $token,
        'wekan_tokenExpires' => $tokenExpires
        );
        $this->session->set_userdata('wekan_integration', $sess_array);


        $wekan_session_data = $this->session->userdata('wekan_integration');
        $wekan_authorId = $wekan_session_data['wekan_authorId'];
        $wekan_token = $wekan_session_data['wekan_token'];
        $wekan_tokenExpires = $wekan_session_data['wekan_tokenExpires'];

       

        }

    }


   // echo $client_name;echo $wekan_authorId."--".$wekan_token."---".$wekan_tokenExpires;exit;
    $ch = curl_init();

    curl_setopt($ch, CURLOPT_URL,$WEKAN_LINK."api/boards");
    curl_setopt($ch, CURLOPT_HTTPHEADER, array(
    'Content-Type: application/x-www-form-urlencoded',
    'Authorization: Bearer ' . $wekan_token
    ));
    curl_setopt($ch, CURLOPT_POST, 1);
    curl_setopt($ch, CURLOPT_POSTFIELDS,
    "title=".$client_name."&owner=".$wekan_authorId."&permission=public&color=nephritis");

    // In real life you should use something like:
    // curl_setopt($ch, CURLOPT_POSTFIELDS, 
    //          http_build_query(array('postvar1' => 'value1')));

    // Receive server response ...
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

    $server_output = curl_exec($ch);

    curl_close ($ch);
	$id_arr=json_decode($server_output);
	//echo "<pre>";
    //print_r($id_arr);exit;
	$wekan_client_id=$id_arr->_id;



  //////////////////////////////////////////custom field////////////////////////////////




  
        $ch = curl_init();

        curl_setopt_array($ch, array(
        CURLOPT_URL => $WEKAN_LINK."api/boards/".$wekan_client_id."/custom-fields",
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_ENCODING => "",
        CURLOPT_MAXREDIRS => 10,
        CURLOPT_TIMEOUT => 0,
        CURLOPT_FOLLOWLOCATION => true,
        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
        CURLOPT_CUSTOMREQUEST => "POST",
        CURLOPT_POSTFIELDS =>'{"name": "Brand Name","type": "text","settings": {},"showOnCard": true,"automaticallyOnCard": true,"showLabelOnMiniCard": false}',
        CURLOPT_HTTPHEADER => array(
        "Accept: application/json",
        "Content-Type: application/json",
        "Authorization: Bearer " . $wekan_token,
        "Content-Type: application/json"
        ),
        ));

        $server_output = curl_exec($ch);
        curl_close ($ch);
        $id_arr=json_decode($server_output);

        //echo "<pre>";
        //print_r($id_arr);

        $wekan_brand_custom_brand_id=$id_arr->_id;









	////////////////////////////////////////////////////////////////////////


/*	$ch = curl_init();

    curl_setopt($ch, CURLOPT_URL,$WEKAN_LINK."api/boards/".$wekan_client_id."/lists");
    curl_setopt($ch, CURLOPT_HTTPHEADER, array(
    'Content-Type: application/x-www-form-urlencoded',
    'Authorization: Bearer ' . $wekan_token
    ));
    curl_setopt($ch, CURLOPT_POST, 1);
    curl_setopt($ch, CURLOPT_POSTFIELDS,
    "title=Brief - Review Pending");
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
	$server_output = curl_exec($ch);
	curl_close ($ch);
	$id_arr=json_decode($server_output);

    //echo "<pre>";
    //print_r($id_arr);

    $wekan_pending_id=$id_arr->_id;*/



	////////////////////////////////////////////////////////////////////////


/*	$ch = curl_init();

    curl_setopt($ch, CURLOPT_URL,$WEKAN_LINK."api/boards/".$wekan_client_id."/lists");
    curl_setopt($ch, CURLOPT_HTTPHEADER, array(
    'Content-Type: application/x-www-form-urlencoded',
    'Authorization: Bearer ' . $wekan_token
    ));
    curl_setopt($ch, CURLOPT_POST, 1);
    curl_setopt($ch, CURLOPT_POSTFIELDS,
    "title=Ongoing task");
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
	$server_output = curl_exec($ch);
	curl_close ($ch);
	$id_arr=json_decode($server_output);

    //echo "<pre>";
    //print_r($id_arr);

    $wekan_ongoing_id=$id_arr->_id;*/



	////////////////////////////////////////////////////////////////////////


	/*$ch = curl_init();

    curl_setopt($ch, CURLOPT_URL,$WEKAN_LINK."api/boards/".$wekan_client_id."/lists");
    curl_setopt($ch, CURLOPT_HTTPHEADER, array(
    'Content-Type: application/x-www-form-urlencoded',
    'Authorization: Bearer ' . $wekan_token
    ));
    curl_setopt($ch, CURLOPT_POST, 1);
    curl_setopt($ch, CURLOPT_POSTFIELDS,
    "title=Rejected Brief");
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
	$server_output = curl_exec($ch);
	curl_close ($ch);
	$id_arr=json_decode($server_output);

    //echo "<pre>";
    //print_r($id_arr);

    $wekan_rejecting_id=$id_arr->_id;*/



	////////////////////////////////////////////////////////////////////////


    $str_list="";
    //echo "select * from wc_wekan_board_list where wekan_board_list_id in($wekan_board_list) and deleted='0' and status='Active'  ";

    $clients_details=$this->db->query("select * from wc_wekan_board_list where wekan_board_list_id in($wekan_board_list) and deleted='0' and status='Active'  ")->result_array();
    if(!empty($clients_details))
    {
    foreach($clients_details as $key => $clientsdetails)
    {
    $wekan_board_list_id=$clientsdetails['wekan_board_list_id'];
    $wekan_board_list_id=$clientsdetails['wekan_board_list_id'];
    $wekan_board_list=$clientsdetails['wekan_board_list'];


    $ch = curl_init();

    curl_setopt($ch, CURLOPT_URL,$WEKAN_LINK."api/boards/".$wekan_client_id."/lists");
    curl_setopt($ch, CURLOPT_HTTPHEADER, array(
    'Content-Type: application/x-www-form-urlencoded',
    'Authorization: Bearer ' . $wekan_token
    ));
    curl_setopt($ch, CURLOPT_POST, 1);
    curl_setopt($ch, CURLOPT_POSTFIELDS,
    "title=".$wekan_board_list);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    $server_output = curl_exec($ch);
    curl_close ($ch);
    $id_arr=json_decode($server_output);

    //echo "<pre>";
    //print_r($id_arr);

    $wekan_list_id=$id_arr->_id;


     $str_list.=$wekan_board_list_id."-----".$wekan_list_id."-----".$wekan_board_list."******";

    }

    }



  $str_list=trim($str_list,"******");




   /* return $wekan_client_id."@@@@@".$wekan_brand_custom_brand_id."@@@@@".$wekan_pending_id."@@@@@".$wekan_ongoing_id."@@@@@".$wekan_rejecting_id;*/

    return $wekan_client_id."@@@@@".$wekan_brand_custom_brand_id."@@@@@".$str_list;

    

}
















    public function update_borad_wekan($client_name,$client_id,$wekan_client_id,$wekan_board_list){


    $query=$this->db->query("select * from wc_settings WHERE setting_key='WEKAN_LINK' ");
    $userDetails= $query->result_array();
    $WEKAN_LINK=$userDetails[0]['setting_value'];

    $query=$this->db->query("select * from wc_settings WHERE setting_key='WEKAN_USERNAME' ");
    $userDetails= $query->result_array();
    $WEKAN_USERNAME=$userDetails[0]['setting_value'];


    $query=$this->db->query("select * from wc_settings WHERE setting_key='WEKAN_PASSWORD' ");
    $userDetails= $query->result_array();
    $WEKAN_PASSWORD=$userDetails[0]['setting_value'];

    

    if ($this->session->userdata('wekan_integration')) {



        $wekan_session_data = $this->session->userdata('wekan_integration');
        $wekan_authorId = $wekan_session_data['wekan_authorId'];
        $wekan_token = $wekan_session_data['wekan_token'];
        $wekan_tokenExpires = $wekan_session_data['wekan_tokenExpires'];

         //echo "<pre>";
        //print_r($wekan_session_data);exit;
    }
    else
    {

        $curl = curl_init();
        curl_setopt_array($curl, array(
        CURLOPT_URL => $WEKAN_LINK."users/login",
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_ENCODING => "",
        CURLOPT_MAXREDIRS => 10,
        CURLOPT_TIMEOUT => 30,
        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
        CURLOPT_CUSTOMREQUEST => "POST",
        CURLOPT_POSTFIELDS => "username=".$WEKAN_USERNAME."&password=".$WEKAN_PASSWORD."",
        CURLOPT_HTTPHEADER => array(
        "cache-control: no-cache",
        "content-type: application/x-www-form-urlencoded"
        ),
        ));
        $response = curl_exec($curl);
        $err = curl_error($curl);

        curl_close($curl);

        if (!$err)
        {
        //var_dump($response);



        $res_arr=json_decode($response);

        //echo "<pre>";
        //print_r($res_arr);exit;
        $id=$res_arr->id;
        $token=$res_arr->token;
        $tokenExpires=$res_arr->tokenExpires;


        $sess_array = array(
        'wekan_authorId' => $id,
        'wekan_token' => $token,
        'wekan_tokenExpires' => $tokenExpires
        );
        $this->session->set_userdata('wekan_integration', $sess_array);


        $wekan_session_data = $this->session->userdata('wekan_integration');
        $wekan_authorId = $wekan_session_data['wekan_authorId'];
        $wekan_token = $wekan_session_data['wekan_token'];
        $wekan_tokenExpires = $wekan_session_data['wekan_tokenExpires'];

       

        }

    }


   // echo $client_name;echo $wekan_authorId."--".$wekan_token."---".$wekan_tokenExpires;exit;
    /*$ch = curl_init();

    curl_setopt($ch, CURLOPT_URL,$WEKAN_LINK."api/boards");
    curl_setopt($ch, CURLOPT_HTTPHEADER, array(
    'Content-Type: application/x-www-form-urlencoded',
    'Authorization: Bearer ' . $wekan_token
    ));
    curl_setopt($ch, CURLOPT_POST, 1);
    curl_setopt($ch, CURLOPT_POSTFIELDS,
    "title=".$client_name."&owner=".$wekan_authorId."&permission=public&color=nephritis");

   
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

    $server_output = curl_exec($ch);

    curl_close ($ch);
    $id_arr=json_decode($server_output);
    
    $wekan_client_id=$id_arr->_id;
*/


    $str_list="";
    //echo "select * from wc_wekan_board_list where wekan_board_list_id in($wekan_board_list) and deleted='0' and status='Active'  ";

    $clients_details=$this->db->query("select * from wc_wekan_board_list where wekan_board_list_id in($wekan_board_list) and deleted='0' and status='Active'  ")->result_array();
    if(!empty($clients_details))
    {
    foreach($clients_details as $key => $clientsdetails)
    {
    $wekan_board_list_id=$clientsdetails['wekan_board_list_id'];
    $wekan_board_list=$clientsdetails['wekan_board_list'];


     $clients_details=$this->db->query("select * from wc_client_wekan_list_relation where client_id='$client_id' and   wekan_board_list_id='$wekan_board_list_id'  ")->result_array();
    if(empty($clients_details))
    {


    $ch = curl_init();

    curl_setopt($ch, CURLOPT_URL,$WEKAN_LINK."api/boards/".$wekan_client_id."/lists");
    curl_setopt($ch, CURLOPT_HTTPHEADER, array(
    'Content-Type: application/x-www-form-urlencoded',
    'Authorization: Bearer ' . $wekan_token
    ));
    curl_setopt($ch, CURLOPT_POST, 1);
    curl_setopt($ch, CURLOPT_POSTFIELDS,
    "title=".$wekan_board_list);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    $server_output = curl_exec($ch);
    curl_close ($ch);
    $id_arr=json_decode($server_output);

    //echo "<pre>";
    //print_r($id_arr);

    $wekan_list_id=$id_arr->_id;


     $str_list.=$wekan_board_list_id."-----".$wekan_list_id."-----".$wekan_board_list."******";



   }
   else
   {
    $clients_details=$this->db->query("select * from wc_clients where client_id='$client_id'  ")->result_array();
    $wekan_brand_custom_brand_id=$clients_details[0]['wekan_brand_custom_brand_id'];
   }



    }

    }



  $str_list=trim($str_list,"******");





    return $wekan_client_id."@@@@@".$wekan_brand_custom_brand_id."@@@@@".$str_list;

    

}
	
	public function insert_client(){
		
		if ($this->session->userdata('logged_in')) {
			$session_data = $this->session->userdata('logged_in');
			$created_date = date('Y-m-d');
			$userid=$session_data['id'];
			$client_id=$this->input->post('client_id');
            $wekan_board_list=$this->input->post('wekan_board_list');
			
			if($client_id!='')
			{

				$checkquerys = $this->db->query("select * from wc_clients where  client_id='$client_id' ")->result_array();
                $wekan_client_id=$checkquerys[0]['wekan_client_id'];

                if($wekan_client_id!='')
                {

                    $wekan_board_list=$this->input->post('wekan_board_list');
                    $wekan_all_id=$this->update_borad_wekan($this->input->post('client_name'),$client_id,$wekan_client_id,$wekan_board_list);

                    //echo $wekan_all_id;


                    $wekan_all_id_arr=explode("@@@@@",$wekan_all_id);
                    $wekan_client_id= trim($wekan_all_id_arr[0]);


                    $wekan_brand_custom_brand_id= trim($wekan_all_id_arr[1]);

                    $wekan_board_list= trim($wekan_all_id_arr[2]);
                    

                	$data = array(
				    'client_id'=>$this->input->post('client_id'),
					'client_name'=>$this->input->post('client_name'),
					'client_email'=>$this->input->post('client_email'),
                     'project_description'=>$this->input->post('project_description'),
					'client_phone'=>$this->input->post('client_phone'),
					'status'=>$this->input->post('status'),
                    'allow_delivary_date_bm'=>$this->input->post('allow_delivary_date_bm'),
					'client_image'=>$this->input->post('pimg'),
					);


                   if(trim($wekan_board_list)!='')
                   {
                    $wekan_board_list_arr=explode("******",$wekan_board_list);

                    if(!empty($wekan_board_list_arr))
                    {
                    foreach($wekan_board_list_arr as $keywekan => $valuewekan)
                    {
                   // echo $valuewekan."<br>";
                    $wekan_arr=explode("-----",$valuewekan);
                    $wekan_board_list_id= trim($wekan_arr[0]);
                    $wekan_list_id= trim($wekan_arr[1]);
                    $wekan_list_name= trim($wekan_arr[2]);

                    if($wekan_list_id!='')
                    {

                        $dataWekan = array(
                    'client_id'=>$client_id,
                    'wekan_board_list_id'=>$wekan_board_list_id,
                    'wekan_client_id'=>$wekan_client_id,
                    'wekan_list_id'=>$wekan_list_id,
                    'wekan_list_name'=>$wekan_list_name,

                    );

                    // echo "<pre>";
                    //print_r($dataWekan);
                    $this->Client_model->client_wekan_list_relation_insert($dataWekan);
                    $client_wekan_list_relation_id=$this->db->insert_id();

                    }

                    


                    }
                    }

                }
                    

                }
                else
                {

                	$wekan_board_list=$this->input->post('wekan_board_list');
                    /*$wekan_all_id=$this->insert_borad_wekan($this->input->post('client_name'),$wekan_board_list);


                

                    $wekan_all_id_arr=explode("@@@@@",$wekan_all_id);
                    $wekan_client_id= trim($wekan_all_id_arr[0]);


                    $wekan_brand_custom_brand_id= trim($wekan_all_id_arr[1]);

                    $wekan_board_list= trim($wekan_all_id_arr[2]);
                    $wekan_board_list_arr=explode("******",$wekan_board_list);*/

                    $wekan_all_id="";
                     $wekan_all_id_arr=array();
                     $wekan_client_id="";
                     $wekan_board_list="";
                     $wekan_board_list_arr=array();

				    
                	 $data = array(
				    'client_id'=>$this->input->post('client_id'),
				    /*'wekan_client_id'=>$wekan_client_id,
                    'wekan_brand_custom_brand_id'=>$wekan_brand_custom_brand_id,*/
				    /*'wekan_pending_id'=>$wekan_pending_id,
				    'wekan_ongoing_id'=>$wekan_ongoing_id,
				    'wekan_rejecting_id'=>$wekan_rejecting_id,*/
					'client_name'=>$this->input->post('client_name'),
					'client_email'=>$this->input->post('client_email'),
                     'project_description'=>$this->input->post('project_description'),
					'client_phone'=>$this->input->post('client_phone'),
					'status'=>$this->input->post('status'),
                    'allow_delivary_date_bm'=>$this->input->post('allow_delivary_date_bm'),
					'client_image'=>$this->input->post('pimg'),
					);


                    if(!empty($wekan_board_list_arr))
                    {
                    foreach($wekan_board_list_arr as $keywekan => $valuewekan)
                    {
                   // echo $valuewekan."<br>";
                    $wekan_arr=explode("-----",$valuewekan);
                    $wekan_board_list_id= trim($wekan_arr[0]);
                    $wekan_list_id= trim($wekan_arr[1]);
                    $wekan_list_name= trim($wekan_arr[2]);

                    if($wekan_list_id!='')
                    {

                    $dataWekan = array(
                    'client_id'=>$client_id,
                    'wekan_board_list_id'=>$wekan_board_list_id,
                    'wekan_client_id'=>$wekan_client_id,
                    'wekan_list_id'=>$wekan_list_id,
                    'wekan_list_name'=>$wekan_list_name,

                    );

                    // echo "<pre>";
                    //print_r($dataWekan);
                    $this->Client_model->client_wekan_list_relation_insert($dataWekan);
                    $client_wekan_list_relation_id=$this->db->insert_id();

                }


                    }
                    }
                    



                	

                }
				
				$client_query= $this->db->query("SELECT * FROM wc_clients WHERE client_id='$client_id'");
                $client_rs= $client_query->result_array(); 
                
               // echo $_SERVER['DOCUMENT_ROOT']."WeCreate/upload/".$client_id."-".$client_rs[0]['client_name'];

                $oldclient_name=preg_replace('/[^a-zA-Z0-9_.]/', '_',$client_rs[0]['client_name']);
                $newclient_name=preg_replace('/[^a-zA-Z0-9_.]/', '_',$this->input->post('client_name'));
                /*$old_client_name=$_SERVER['DOCUMENT_ROOT']."/"."WeCreate/upload/".$client_id."-".$oldclient_name;
                $new_client_name=$_SERVER['DOCUMENT_ROOT']."/"."WeCreate/upload/".$client_id."-".$newclient_name;*/

                $old_client_name=$_SERVER['DOCUMENT_ROOT']."/".FOLDER_NAME."/upload/".$client_id."-".$oldclient_name;
                $new_client_name=$_SERVER['DOCUMENT_ROOT']."/".FOLDER_NAME."/upload/".$client_id."-".$newclient_name;

                //echo $old_client_name."----". $new_client_name;
               // print_r($client_rs);
                if(rename( $old_client_name, $new_client_name)){ 
                   //echo "Successfully Renamed $old_client_name to $new_client_name" ;
                }else{
                    //echo "A File With The Same Name Already Exists" ;
                }
					
				  $updateuserrole = $this->Client_model->client_update($data,$client_id);
			      echo $client_id;


				
			}else {

                    $wekan_board_list=$this->input->post('wekan_board_list');


				    /*$wekan_all_id=$this->insert_borad_wekan($this->input->post('client_name'),$wekan_board_list);


                

				    $wekan_all_id_arr=explode("@@@@@",$wekan_all_id);
                    $wekan_client_id= trim($wekan_all_id_arr[0]);


				    $wekan_brand_custom_brand_id= trim($wekan_all_id_arr[1]);

                   $wekan_board_list= trim($wekan_all_id_arr[2]);
                    $wekan_board_list_arr=explode("******",$wekan_board_list);
*/

                     $wekan_all_id="";
                     $wekan_all_id_arr=array();
                     $wekan_client_id="";
                     $wekan_board_list="";
                     $wekan_board_list_arr=array();

				
				
				
				     $data = array(
				    'client_id'=>$this->input->post('client_id'),
				   /* 'wekan_client_id'=>$wekan_client_id,
                    'wekan_brand_custom_brand_id'=>$wekan_brand_custom_brand_id,*/
				    /*'wekan_pending_id'=>$wekan_pending_id,
				    'wekan_ongoing_id'=>$wekan_ongoing_id,
				    'wekan_rejecting_id'=>$wekan_rejecting_id,*/
					'client_name'=>$this->input->post('client_name'),
					'client_email'=>$this->input->post('client_email'),
                     'project_description'=>$this->input->post('project_description'),
					'client_phone'=>$this->input->post('client_phone'),
					'status'=>$this->input->post('status'),
                    'allow_delivary_date_bm'=>$this->input->post('allow_delivary_date_bm'),
					'client_image'=>$this->input->post('pimg'),
					);
					$this->Client_model->client_insert($data);
					$client_id=$this->db->insert_id();

                   
                    
                    if(!empty($wekan_board_list_arr))
                    {
                        foreach($wekan_board_list_arr as $keywekan => $valuewekan)
                        {
                           // echo $valuewekan."<br>";
                        $wekan_arr=explode("-----",$valuewekan);
                        $wekan_board_list_id= trim($wekan_arr[0]);
                        $wekan_list_id= trim($wekan_arr[1]);
                        $wekan_list_name= trim($wekan_arr[2]);

                        if($wekan_list_id!='')
                    {

                        $dataWekan = array(
                        'client_id'=>$client_id,
                        'wekan_board_list_id'=>$wekan_board_list_id,
                        'wekan_client_id'=>$wekan_client_id,
                        'wekan_list_id'=>$wekan_list_id,
                        'wekan_list_name'=>$wekan_list_name,

                        );

                           // echo "<pre>";
                            //print_r($dataWekan);
                            $this->Client_model->client_wekan_list_relation_insert($dataWekan);
                            $client_wekan_list_relation_id=$this->db->insert_id();

                        }


                        }
                    }
                    


					
					
					echo $client_id;





				}
			
		}else {
		
            /*redirect('login', 'refresh');*/
            redirect('administrator', 'refresh');
       
		}  
	}
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
	public function change_password(){
		
		if ($this->session->userdata('logged_in'))		
		{  
			$session_data = $this->session->userdata('logged_in');
			$slang="english";
		    $this->lang->load($slang, $slang);
		    $data['lang']=$this->lang->language; 
			
			$client_id = $session_data['id'];
			$data['client_id'] = $client_id;
			$data['title'] = "User Role";
			if($client_id!=''){
				$user_details=$this->db->query("select * from wc_clients where client_id='$client_id'")->result_array();
				if(!empty($user_details)){}
			}else {}
			$this->load->view('admin/client/user_password', $data);
		} 
		else 
		{
			//If no session, redirect to login page
			/*redirect('login', 'refresh');*/
            redirect('administrator', 'refresh');
		}
	}
	
	public function update_change_password()			//for displaying the data of the staff into the table
   {
	
	       $client_id=$this->input->post('client_id');
	          $data = array(
				'user_password' => sha1($this->input->post('user_password'))
                 );
				//print_r($staffdata);
				$updateuserrole = $this->Client_model->user_update($data, $client_id);
			    echo $client_id;
				
				
				//redirect('manage-employee');
   }
   
 
	public function deleteclient()
    {
		if ($this->session->userdata('logged_in')) 
		{
			$client_id = $this->input->post('client_id');
		    
              $this->db->query("update wc_brands set deleted ='1',status='Inactive' where client_id ='$client_id'");

              $this->db->query("update wc_users set deleted ='1',status='Inactive' where client_id in (".trim($client_id,",").")" );

			 $this->db->query("update wc_clients set deleted ='1',status='Inactive' where client_id ='$client_id'");

			$this->session->set_flashdata('added', '<div align="left" style="color:green; font-size:15px; border:1px #398AB9 solid;">
		    Service Deleted Successfully</div>');

			//redirect('users');
		} 
		else 
		{
			//If no session, redirect to login page
			/*redirect('login', 'refresh');*/
            redirect('administrator', 'refresh');
		}
	}

    public function deleteclient_old_2_6_2020()
    {
        if ($this->session->userdata('logged_in')) 
        {
            $client_id = $this->input->post('client_id');
            $client_query= $this->db->query("SELECT * FROM wc_clients WHERE client_id='$client_id'");
            $client_rs= $client_query->result_array(); 
            $client_name=preg_replace('/[^a-zA-Z0-9_.]/', '_',$client_rs[0]['client_name']);
            $folder=$_SERVER['DOCUMENT_ROOT']."/".FOLDER_NAME."/upload/".$client_id."-".$client_name;
            //$this->deleteAll($folder);


            $brands_query= $this->db->query("SELECT * FROM wc_brands WHERE client_id='$client_id'");
            $brands_rs= $brands_query->result_array(); 
            foreach($brands_rs as $key => $brandsdetails){
                $brand_id=$brandsdetails['brand_id'];
                $brief_query= $this->db->query("SELECT * FROM  wc_brief WHERE brand_id in ('$brand_id')");
                $brief_rs= $brief_query->result_array(); 
                foreach($brief_rs as $key => $briefdetails){
                    $brief_id=$briefdetails['brief_id']; 
                    $brief_records_query= $this->db->query("SELECT * FROM   wc_brief_records WHERE brief_id='$brief_id'");
                    $brief_records_rs= $brief_records_query->result_array(); 

                    foreach($brief_records_rs as $key => $brief_recordsdetails){
                       $brief_records_id=$brief_recordsdetails['brief_id']; 
                      // $this->db->query("delete from wc_brief_records where brief_record_id='$brief_records_id'");
                       
                    }
                    $brief_product_list_query= $this->db->query("SELECT * FROM   wc_brief_product_list WHERE brief_id='$brief_id'");
                    $brief_product_list_rs= $brief_product_list_query->result_array(); 

                    foreach($brief_product_list_rs as $key => $brief_product_listdetails){
                      $brief_product_id=$brief_product_listdetails['brief_id'];
                      // $this->db->query("delete from wc_brief_product_list where brief_product_id='$brief_product_id'");
                    }
                    $image_upload_query= $this->db->query("SELECT * FROM   wc_image_upload WHERE brief_id='$brief_id'");
                    $image_upload_rs= $image_upload_query->result_array(); 

                    foreach($image_upload_rs as $key => $image_uploaddetails){
                       $image_id=$image_uploaddetails['image_id'];
                        $image_annotation_query= $this->db->query("SELECT * FROM    wc_image_annotation WHERE image_id='$image_id'");
                        $image_annotation_rs= $image_annotation_query->result_array(); 

                        foreach($image_annotation_rs as $key => $image_annotationdetails){
                           $id=$image_annotationdetails['id'];
                          // $this->db->query("delete from wc_image_annotation where id='$id'");
                        }
                      //  $this->db->query("delete from wc_image_upload where image_id='$image_id'");
                    }

                    $feedback_query= $this->db->query("SELECT * FROM   wc_feedback WHERE brief_id='$brief_id'");
                    $feedback_rs= $feedback_query->result_array(); 

                    foreach($feedback_rs as $key => $feedbackdetails){
                       $feedback_id=$feedbackdetails['feedback_id'];  echo '\n';
                      // $this->db->query("delete from wc_feedback where feedback_id='$feedback_id'");
                    }

                   // $this->db->query("delete from wc_brief where brief_id='$brief_id'");
                }
                
                // $this->db->query("delete from wc_brands where brand_id='$brand_id'");
            }

   //          $this->db->query("delete from wc_users where client_id='$client_id'");
            // $this->db->query("delete from wc_clients where client_id='$client_id'");

            // $this->db->query("update wc_clients set deleted ='1',status='Disable' where client_id ='$client_id'");
            $this->session->set_flashdata('added', '<div align="left" style="color:green; font-size:15px; border:1px #398AB9 solid;">
            Service Deleted Successfully</div>');

            //redirect('users');
        } 
        else 
        {
            //If no session, redirect to login page
            /*redirect('login', 'refresh');*/
            redirect('administrator', 'refresh');
        }
    }
	
	public function deleteAll($str) {
        //It it's a file.
        if (is_file($str)) {
            //Attempt to delete it.
            return unlink($str);
        }
        //If it's a directory.
        elseif (is_dir($str)) {
            //Get a list of the files in this directory.
            $scan = glob(rtrim($str,'/').'/*');
            //Loop through the list of files.
            foreach($scan as $index=>$path) {
                //Call our recursive function.
               $this-> deleteAll($path);
            }
            //Remove the directory itself.
            return @rmdir($str);
        }
    }
	
	
	public function view($client_id=''){
		
		if ($this->session->userdata('logged_in'))		
		{  
			$session_data = $this->session->userdata('logged_in');
			$slang="english";
		    $this->lang->load($slang, $slang);
		    $data['lang']=$this->lang->language; 
			$data['title'] = "User Role";
			if($client_id!=''){
				$user_details=$this->db->query("select * from wc_clients where client_id='$client_id'")->result_array();
				if(!empty($user_details)){
					$data['client_id'] = $user_details[0]['client_id'];
					$data['client_name'] = $user_details[0]['client_name'];
					$data['client_phone'] = $user_details[0]['client_phone'];
					$data['client_email'] = $user_details[0]['client_email'];
					$data['status'] = $user_details[0]['status'];
                    $data['allow_delivary_date_bm'] = $user_details[0]['allow_delivary_date_bm'];
					$data['pimg'] = $user_details[0]['client_image'];


                    $wekan_details_arr=$this->db->query("select wekan_board_list_id from wc_client_wekan_list_relation where client_id='$client_id'")->result_array();

                    $client_wekan_list_arr[]="";
                    foreach($wekan_details_arr as $key =>$value)
                    {
                        $client_wekan_list_arr[]=$value['wekan_board_list_id'];

                    }

                    $data['client_wekan_list_arr'] = $client_wekan_list_arr;

					
				}
			}else {
			        $data['client_id'] = '';
			        $data['client_name'] = '';
					$data['client_phone'] = '';
					$data['client_email'] = '';
					$data['status'] = '';
                    $data['allow_delivary_date_bm'] = '';
					$data['pimg'] = '';
                    $data['client_wekan_list_arr'] =array();
					
			}
			$this->load->view('admin/client/client_view', $data);
		} 
		else 
		{
			//If no session, redirect to login page
			/*redirect('login', 'refresh');*/
            redirect('administrator', 'refresh');
		}
	}
public function updatestatus()
{				//for email exist validation in edit customer form 
	
	
  $id=trim($this->input->get('id'));
  $status=trim($this->input->get('status'));
  //echo "update wc_clients set status ='$status' where customer_id ='$id'";
  $this->db->query("update wc_clients set status ='$status' where client_id ='$id'");


		
}


public function upload_file(){
		
		// $config['upload_path'] = './assets/img/pages-img';
	 //        $config['allowed_types'] = 'gif|jpg|png|jpeg';
	 //       // $config['encrypt_name'] = TRUE;
	 //        $config['max_size'] = '2048';
		// 	$config['max_width'] = '2000';
		// 	$config['max_height'] = '2000';

	       
	 //                    $this->load->library('upload', $config);

	 //                    if ($this->upload->do_upload('pimg')) {
	 //                    	$data =  array('upload_data' => $this->upload->data());
	 //                        $post_image = $_FILES['pimg']['name'];
	 //                    } else {
	                    	
	 //                         $error= $this->upload->display_errors();
	 //                       print_r($error);
	 //                       exit();
	 //                       $post_image = 'noimage.png';
	 //                    }
		if(!empty($_REQUEST['folder_name']))
		{
			$dir = $_REQUEST['folder_name']."/";
		}
		else
		{
			$session_data = $this->session->userdata('logged_in');
			$user_id = $session_data['id'];


			$dir = "upload_client_logo/";

		}
		$file=time().$_FILES["pimg"]["name"];

		//echo $_FILES["pimg"]["tmp_name"]."------".$dir. $file;exit;
		move_uploaded_file($_FILES["pimg"]["tmp_name"], $dir. $file);

        $new_thumb = $dir. $file;
        $file_pat = $dir. $file;
        //$this->convertToThumb($new_thumb,$file_pat);

		echo trim($file);
	}

function convertToThumb($targetFile, $originalFile) {

  $newHeight = 100;

  $info = getimagesize($originalFile);
  $mime = $info['mime'];

  switch ($mime) {
    case 'image/jpeg':
    $image_create_func = 'imagecreatefromjpeg';
    $image_save_func = 'imagejpeg';
    $new_image_ext = 'jpg';
    break;

    case 'image/png':
    $image_create_func = 'imagecreatefrompng';
    $image_save_func = 'imagepng';
    $new_image_ext = 'png';
    break;

    // case 'image/gif':
    // $image_create_func = 'imagecreatefromgif';
    // $image_save_func = 'imagegif';
    // $new_image_ext = 'gif';
    // break;

    default: 
    throw new Exception('Unknown image type.');
  }

  $img = $image_create_func($originalFile);
  list($width, $height) = getimagesize($originalFile);

  $newWidth = ($width / $height) * $newHeight;
  $tmp = imagecreatetruecolor($newWidth, $newHeight);
  imagecopyresampled($tmp, $img, 0, 0, 0, 0, $newWidth, $newHeight, $width, $height);

  if (file_exists($targetFile)) {
    unlink($targetFile);
  }

  $image_save_func($tmp, "$targetFile");
}



public function get_unlink_path()
	{


		$session_data = $this->session->userdata('logged_in');
		$user_id = $session_data['id'];
		
		
		$dir = "./upload_client_logo/";
		
		$file=$this->input->get('image_delete_value');
		$filename= $dir. $file;
		unlink($filename);

	}

	
	
}
?>