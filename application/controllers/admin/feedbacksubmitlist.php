<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Feedbacksubmitlist extends CI_Controller
{
    public function __construct()
	{
		parent::__construct();		
		$this->load->library('pagination');
		$this->load->database();		
		$this->load->library('Upload');
		$this->load->helper('security');	
		$this->load->library('Form_validation');
		$this->load->helper(array('form', 'html', 'file'));
		$this->load->library('pagination');
		$this->load->library('email');
        $this->load->model('Feedbacksubmitlist_model');
        $this->load->library('userlib');
		
    	if ($this->session->userdata('logged_in')) {
            $session_data = $this->session->userdata('logged_in');
            $data['username'] = $session_data['username'];
            $data['firstname'] = $session_data['firstname'];
            $data['rolecode'] = $session_data['rolecode'];
			/* $timezone = $session_data['timezone'];
			date_default_timezone_set($timezone); */
         
        } else {
            //If no session, redirect to login page
            /*redirect('login', 'refresh');*/
            redirect('administrator', 'refresh');
        }
    }
	
    public function index()
	{
		if ($this->session->userdata('logged_in'))		
		{  
			$session_data = $this->session->userdata('logged_in');
			$data['rolecode'] = $session_data['rolecode'];
			$data['Feedbacksubmitlist'] = $session_data['rolecode'];
			$data['roles'] = $session_data['roles'];
			$data['title'] = "Users";
			$slang="english";
			$this->lang->load($slang, $slang);
			$data['lang']=$this->lang->language; 
			
			//echo "aaaaaaaaa";exit;
			$this->load->view('admin/feedbacksubmitlist/feedbacksubmitlist_list', $data);
		} 
		else 
		{
			//If no session, redirect to login page
			/*redirect('login', 'refresh');*/
            redirect('administrator', 'refresh');
		}
	}
	
	public function feedbacklist(){
		if ($this->session->userdata('logged_in')) 
        {
			$session_data=$this->session->userdata('logged_in');
			$Feedbacksubmitlist = $session_data['rolecode'];
			$userid = $session_data['id'];
			$slang="english";
		    $this->lang->load($slang, $slang);
		    $data['lang']=$this->lang->language; 
    	    $aColumns = array( 'wc_feedback.feedback_id','wc_feedback.unique_id','wc_feedback.brief_id','wc_brief.brief_title','wc_feedback.brand_id','wc_brands.brand_name','wc_feedback.user_id','wc_users.user_name');
         
			/* Indexed column (used for fast and accurate table cardinality) */
			$sIndexColumn = "unique_id";
         
			/* DB table to use */
			$sTable = "wc_feedback";

            /*
			 * Paging
			 */
			$sLimit = "";
			if ( isset( $_GET['start'] ) && $_GET['length'] != '-1' )
			{
				$sLimit = "LIMIT ".intval( $_GET['start'] ).", ".
					intval( $_GET['length'] );
			}


    
     
			$sOrder = "";
			if ( isset( $_GET['order'] ) )
			{
				$sOrder = "ORDER BY  ";
			
						$sOrder .= $aColumns[0]."
							".($_GET['order'][0]['dir']==='desc' ? 'asc' : 'desc') .", ";
			   
				 
				$sOrder = substr_replace( $sOrder, "", -2 );
				if ( $sOrder == "ORDER BY" )
				{
					$sOrder = "";
				}
			}
	

        
        $commn="1=1  ";
        
		
		//echo $_REQUEST['columns'][1]['search']['value']; exit();
		
		if(!empty($_GET['columns'][1]['search']['value']) ){  
	
		$commn.=" AND wc_feedback_rating.feedback_rating_id  LIKE '".$_GET['columns'][1]['search']['value']."%' ";
		}

        $sWhere="where $commn";
         
		
		if (isset($_GET['search']['value']) && $_GET['search']['value'] != "" )
		{
			 $sWhere = "WHERE $commn   AND (";
			for ( $i=0 ; $i<count($aColumns) ; $i++ )
			{
				
				/*if ( isset($_GET['columns'][$i]['searchable']) && $_GET['columns'][$i]['searchable'] == "true" )
				{*/
					$sWhere .= $aColumns[$i]." LIKE '%".( $_GET['search']['value'] )."%' OR ";
				/*}*/
			}
			$sWhere = substr_replace( $sWhere, "", -3 );
			$sWhere .= ')';
		}
		
		
       $join=' LEFT JOIN wc_brief ON wc_feedback.brief_id= wc_brief.brief_id

       LEFT JOIN wc_brands ON wc_feedback.brand_id= wc_brands.brand_id

       LEFT JOIN wc_users ON wc_feedback.user_id= wc_users.user_id


       ';
        /*
         * SQL queries
         * Get data to display
         */

        $sGroup=" GROUP BY unique_id";

        $sQuery = "
            SELECT SQL_CALC_FOUND_ROWS ".str_replace(" , ", " ", implode(", ", $aColumns))."
            FROM   $sTable $join
            $sWhere
            $sGroup
            $sOrder
            
            $sLimit";

      //echo  $sQuery; 

        $rResult = $this->db->query($sQuery);
        $rResult = $rResult->result_array();
        //$rResult = mysqli_query( $sQuery) or fatal_error( 'MySQL Error: ' . mysqli_errno() );

        
        /* Data set length after filtering */
        //$sQuery = "SELECT FOUND_ROWS()";
        //$rResultFilterTotal = mysqli_query( $sQuery) or fatal_error( 'MySQL Error: ' . mysqli_errno() );
        //$query = $this->db->query($sQuery);
        //$aResultFilterTotal = $query->result_array();
        //$aResultFilterTotal = mysqli_fetch_array($rResultFilterTotal);
       $iFilteredTotal = count( $rResult);
          
        /* Total data set length */
        /*$sQuery = "
            SELECT COUNT(".$sIndexColumn.") as count
            FROM   $sTable $join
            $sWhere
             $sGroup
        ";*/
        $sQuery = "
            SELECT COUNT(distinct ".$sIndexColumn.") as count
            FROM   $sTable $join
            $sWhere
             
        ";
        //$rResultTotal = mysqli_query( $sQuery) or fatal_error( 'MySQL Error: ' . mysqli_errno() );
        //$aResultTotal = mysqli_fetch_array($rResultTotal);

      // echo  $sQuery; exit();

        $query = $this->db->query($sQuery);
        $aResultTotal = $query->result_array();

        //echo "<pre>";
        //print_r($aResultTotal);
         $iTotal = $aResultTotal[0]['count'];
         
        /*
         * Output
         */
        $output = array(
                        "sEcho" => '',
                        "iTotalRecords" => $iFilteredTotal,
                        "iTotalDisplayRecords" => $iTotal,
                        "aaData" => array()
                        );

            $sColumns = array('unique_id','brief_title','brand_name','user_name','action');
         
            //while ( $aRow = mysqli_fetch_array( $rResult ) )
            //echo "<pre>";
           //print_r($output);
         
            foreach($rResult as $aRow)
            {
                $row = array();
            	
				$unique_id=$aRow['unique_id'];
				$feedback_id=$aRow['feedback_id'];
				//$status=$aRow['status'];
                for ( $i=0 ; $i<count($sColumns) ; $i++ )
                {
                  
            	    if($sColumns[$i] =='action')
            		{
            			

						$row[]='<a href="javascript:void(0)"  class="btn btn-success view" val="'.$unique_id.'"><i class="fas fa-eye"></i></a>
        			  ';
						
            		}
					
				
					/*else if($sColumns[$i] =='status')
            		{
            			
            			 $Active_status="";
			             $Inactive_status="";
						 
						 
					    if($status=='Active')
						{
						 $Active_status="selected";
			             $Inactive_status="";
						
						}
						else if($status=='Inactive')
						{
						 $Active_status="";
			             $Inactive_status="selected";
						 
						}
						
            			
            			 $row[]='<select name="status" id="status" class="selectpicker update_status">
								 <option value="Active@@@'.$feedback_rating_id.'"  '.$Active_status.'>Active</option>
								 <option value="Inactive@@@'.$feedback_rating_id.'"  '.$Inactive_status.'>Inactive</option>
								 </select>';
            			 
            		}*/
                    else if ( $sColumns[$i] != ' ' )
                    {
                        /* General output */
                        $row[] = $aRow[ $sColumns[$i] ];
                    }
                }
                $output['aaData'][] = $row;
            }
            //print_r($output);exit;
        echo json_encode( $output );
    }
	}
	
	
	public function add($feedback_rating_id=''){
		
		if ($this->session->userdata('logged_in'))		
		{  
			$session_data = $this->session->userdata('logged_in');
			$slang="english";
		    $this->lang->load($slang, $slang);
		    $data['lang']=$this->lang->language; 
			$data['title'] = "User Role";
			if($feedback_rating_id!=''){
				$Feedbacksubmitlist_details=$this->db->query("select * from wc_feedback_rating where feedback_rating_id='$feedback_rating_id'")->result_array();
				if(!empty($Feedbacksubmitlist_details)){
					$data['feedback_rating_id'] = $Feedbacksubmitlist_details[0]['feedback_rating_id'];
					$data['feedback_type_id_sel'] = $Feedbacksubmitlist_details[0]['feedback_type_id'];
					$data['feedback_rating_sel'] = $Feedbacksubmitlist_details[0]['feedback_rating'];
					$data['feedback_rating_text'] = $Feedbacksubmitlist_details[0]['feedback_rating_text'];
					$data['deleted'] = $Feedbacksubmitlist_details[0]['deleted'];
					
					
				}
			}else {
			        $data['feedback_rating_id'] = '';
					$data['feedback_type_id_sel'] = '';
					$data['feedback_rating_sel'] = '';
					$data['feedback_rating_text'] = '';
					$data['deleted'] = '';
					
					
			}
			$this->load->view('admin/Feedbacksubmitlist/Feedbacksubmitlist_management', $data);
		} 
		else 
		{
			//If no session, redirect to login page
			/*redirect('login', 'refresh');*/
            redirect('administrator', 'refresh');
		}
	}
	
	

	
	public function insert_Feedbacksubmitlist(){
		
		if ($this->session->userdata('logged_in')) {
			$session_data = $this->session->userdata('logged_in');
			$created_date = date('Y-m-d');
			$userid=$session_data['id'];
			$feedback_rating_id=$this->input->post('feedback_rating_id');
			
			if($feedback_rating_id!='')
			{
				
				  $data = array(
				    'feedback_rating_id'=>$this->input->post('feedback_rating_id'),
					'feedback_type_id'=>$this->input->post('feedback_type_id'),
					'feedback_rating'=>$this->input->post('feedback_rating'),
					'feedback_rating_text'=>$this->input->post('feedback_rating_text'),
					);
					
				  $updateuserrole = $this->Feedbacksubmitlist_model->Feedbacksubmitlist_update($data,$feedback_rating_id);
			      echo $feedback_rating_id;
				
			}else {
				
				
				
				   $data = array(
				    'feedback_rating_id'=>$this->input->post('feedback_rating_id'),
					'feedback_type_id'=>$this->input->post('feedback_type_id'),
					'feedback_rating'=>$this->input->post('feedback_rating'),
					'feedback_rating_text'=>$this->input->post('feedback_rating_text'),
					);
					$this->Feedbacksubmitlist_model->Feedbacksubmitlist_insert($data);
					$feedback_rating_id=$this->db->insert_id();
					
					
					echo $feedback_rating_id;
				}
			
		}else {
		
            /*redirect('login', 'refresh');*/
            redirect('administrator', 'refresh');
       
		}  
	}
		
		
		
		

 
	public function deletefeedbacksubmitlist()
    {
		if ($this->session->userdata('logged_in')) 
		{
			$feedback_rating_id = $this->input->post('feedback_rating_id');
		
			//$this->db->query("delete from user where user_id='$user_id'");
			$this->db->query("update wc_feedback_rating set deleted ='1' where feedback_rating_id ='$feedback_rating_id'");
			$this->session->set_flashdata('added', '<div align="left" style="color:green; font-size:15px; border:1px #398AB9 solid;">
		    Service Deleted Successfully</div>');

			//redirect('users');
		} 
		else 
		{
			//If no session, redirect to login page
			/*redirect('login', 'refresh');*/
            redirect('administrator', 'refresh');
		}
	}
	
	

	public function view($unique_id=''){
		
		if ($this->session->userdata('logged_in'))		
		{  
			$session_data = $this->session->userdata('logged_in');
			$slang="english";
		    $this->lang->load($slang, $slang);
		    $data['lang']=$this->lang->language; 
			$data['title'] = "User Role";
			if($unique_id!=''){
				$Feedbacksubmitlist_details=$this->db->query("select wc_feedback.*,wc_feedback_type.feedback_type from wc_feedback left join wc_feedback_type on wc_feedback.feedback_type_id=wc_feedback_type.feedback_type_id where unique_id='$unique_id'")->result_array();


				if(!empty($Feedbacksubmitlist_details)){
					$data['feedbackdetails'] =$Feedbacksubmitlist_details; 
					
					
				}
			}else {
			        $data['feedbackdetails'] =''; 
					
					
			}
			$this->load->view('admin/Feedbacksubmitlist/Feedbacksubmitlist_view', $data);
		} 
		else 
		{
			//If no session, redirect to login page
			/*redirect('login', 'refresh');*/
            redirect('administrator', 'refresh');
		}
	}



	public function updatestatus()
	{				//for email exist validation in edit customer form 


	$id=trim($this->input->get('id'));
	$status=trim($this->input->get('status'));
	//echo "update wc_feedback_rating set status ='$status' where feedback_rating_id ='$id'";
	$this->db->query("update wc_feedback_rating set status ='$status' where feedback_rating_id ='$id'");



	}

	
}
?>