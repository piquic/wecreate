<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Pagecontent extends CI_Controller
{
    public function __construct()
	{
		parent::__construct();		
		$this->load->library('pagination');
		$this->load->database();		
		$this->load->library('Upload');
		$this->load->helper('security');	
		$this->load->library('Form_validation');
		$this->load->helper(array('form', 'html', 'file'));
		$this->load->library('pagination');
		$this->load->library('email');
        $this->load->model('User_model');
        $this->load->library('userlib');
		
    	if ($this->session->userdata('logged_in')) {
            $session_data = $this->session->userdata('logged_in');
            $data['username'] = $session_data['username'];
            $data['firstname'] = $session_data['firstname'];
            $data['rolecode'] = $session_data['rolecode'];
			/* $timezone = $session_data['timezone'];
			date_default_timezone_set($timezone); */
         
        } else {
            //If no session, redirect to login page
            /*redirect('login', 'refresh');*/
            redirect('administrator', 'refresh');
        }
    }
	
    public function index()
	{
		if ($this->session->userdata('logged_in'))		
		{  
			$session_data = $this->session->userdata('logged_in');
			$data['rolecode'] = $session_data['rolecode'];
			$data['usertype'] = $session_data['rolecode'];
			$data['roles'] = $session_data['roles'];
			$data['title'] = "Users";
			$slang="english";
			$this->lang->load($slang, $slang);
			$data['lang']=$this->lang->language; 
			
			//echo "aaaaaaaaa";exit;
			$this->load->view('admin/user/user_list', $data);
		} 
		else 
		{
			//If no session, redirect to login page
			/*redirect('login', 'refresh');*/
            redirect('administrator', 'refresh');
		}
	}
	
	public function userlist(){
		if ($this->session->userdata('logged_in')) 
        {
			$session_data=$this->session->userdata('logged_in');
			$usertype = $session_data['rolecode'];
			$userid = $session_data['id'];
			$slang="english";
		    $this->lang->load($slang, $slang);
		    $data['lang']=$this->lang->language; 
    	    $aColumns = array( 'user_id','user_name','user_email','user_password','user_telephone','user_mobile','user_status');
         
			/* Indexed column (used for fast and accurate table cardinality) */
			$sIndexColumn = "user_id";
         
			/* DB table to use */
			$sTable = "wc_users";

            /*
			 * Paging
			 */
			$sLimit = "";
			if ( isset( $_GET['start'] ) && $_GET['length'] != '-1' )
			{
				$sLimit = "LIMIT ".intval( $_GET['start'] ).", ".
					intval( $_GET['length'] );
			}


    
     
			$sOrder = "";
			if ( isset( $_GET['order'] ) )
			{
				$sOrder = "ORDER BY  ";
			
						$sOrder .= $aColumns[0]."
							".($_GET['order'][0]['dir']==='desc' ? 'asc' : 'desc') .", ";
			   
				 
				$sOrder = substr_replace( $sOrder, "", -2 );
				if ( $sOrder == "ORDER BY" )
				{
					$sOrder = "";
				}
			}
	

        
        $commn="1=1 and deleted='0' ";
        
		
		//echo $_REQUEST['columns'][1]['search']['value']; exit();
		
		if(!empty($_GET['columns'][1]['search']['value']) ){  
	
		$commn.=" AND user.user_name  LIKE '".$_GET['columns'][1]['search']['value']."%' ";
		}

        $sWhere="where $commn";
         
		
		if (isset($_GET['search']['value']) && $_GET['search']['value'] != "" )
		{
			 $sWhere = "WHERE $commn   AND (";
			for ( $i=0 ; $i<count($aColumns) ; $i++ )
			{
				
				/*if ( isset($_GET['columns'][$i]['searchable']) && $_GET['columns'][$i]['searchable'] == "true" )
				{*/
					$sWhere .= $aColumns[$i]." LIKE '%".( $_GET['search']['value'] )."%' OR ";
				/*}*/
			}
			$sWhere = substr_replace( $sWhere, "", -3 );
			$sWhere .= ')';
		}
		
		
       $join='';
        /*
         * SQL queries
         * Get data to display
         */
        $sQuery = "
            SELECT SQL_CALC_FOUND_ROWS ".str_replace(" , ", " ", implode(", ", $aColumns))."
            FROM   $sTable $join
            $sWhere
            $sOrder
            $sLimit";

       //echo  $sQuery; exit();

        $rResult = $this->db->query($sQuery);
        $rResult = $rResult->result_array();
        //$rResult = mysqli_query( $sQuery) or fatal_error( 'MySQL Error: ' . mysqli_errno() );

        
        /* Data set length after filtering */
        //$sQuery = "SELECT FOUND_ROWS()";
        //$rResultFilterTotal = mysqli_query( $sQuery) or fatal_error( 'MySQL Error: ' . mysqli_errno() );
        //$query = $this->db->query($sQuery);
        //$aResultFilterTotal = $query->result_array();
        //$aResultFilterTotal = mysqli_fetch_array($rResultFilterTotal);
       $iFilteredTotal = count( $rResult);
          
        /* Total data set length */
        $sQuery = "
            SELECT COUNT(".$sIndexColumn.") as count
            FROM   $sTable
        ";
        //$rResultTotal = mysqli_query( $sQuery) or fatal_error( 'MySQL Error: ' . mysqli_errno() );
        //$aResultTotal = mysqli_fetch_array($rResultTotal);

        $query = $this->db->query($sQuery);
        $aResultTotal = $query->result_array();

        //echo "<pre>";
        //print_r($aResultTotal);
         $iTotal = $aResultTotal[0]['count'];
         
        /*
         * Output
         */
        $output = array(
                        "sEcho" => '',
                        "iTotalRecords" => $iFilteredTotal,
                        "iTotalDisplayRecords" => $iTotal,
                        "aaData" => array()
                        );

            $sColumns = array('user_name','user_email','user_mobile','user_status','action');
         
            //while ( $aRow = mysqli_fetch_array( $rResult ) )
            //echo "<pre>";
           //print_r($output);
         
            foreach($rResult as $aRow)
            {
                $row = array();
            	
				$user_id=$aRow['user_id'];
				//$user_type=$aRow['usertype_id'];
				//$business_id=$aRow['business_id'];
				$status=$aRow['user_status'];
                for ( $i=0 ; $i<count($sColumns) ; $i++ )
                {
                   /*  if ( $sColumns[$i] == "version" )
                    {
                       
                        $row[] = ($aRow[ $sColumns[$i] ]=="0") ? '-' : $aRow[ $sColumns[$i] ];
                    } */
					
            	    if($sColumns[$i] =='action')
            		{
            			if($usertype!=0){
							$roles=$session_data['roles'];
							$role=trim($roles,',');
							$role=explode(',',$role);
							if(!empty($role)){
								foreach($role as $result){
									$query=$this->db->query("select * from user_permissions where role_id='$result' and menu_id='3'")->result();
									if($query){
										foreach($query as $res){
											$new[]=$res->new_id;
											$edit[]=$res->edit_id;
											$delete[]=$res->delete_id;
											$view[]=$res->view_id;
										}
						            }
							    }
							}
							
							if(in_array('1',$edit)){
							$edit_id='<a href="javascript:void(0)"  class="btn btn-info edit" val="'.$user_id.'"><i class="far fa-edit"></i></a>';
							}else {
								$edit_id='';
							}
							if(in_array('1',$delete)){
								$delete_id=' <a href="javascript:void(0)"  class="btn btn-danger delete" val="'.$user_id.'"><i class="fas fa-trash-alt"></i></a>';
							}else {
								$delete_id='';
							}
							if(in_array('1',$view)){
								$view_id=' <a href="javascript:void(0)"  class="btn btn-success view" val="'.$user_id.'"><i class="fas fa-eye"></i></a>';
							}else {
								$view_id='';
							}
							
							 $row[]=$view_id.'&nbsp;'.$edit_id.'&nbsp;'.$delete_id;
							
						}else {

						$row[]='<a href="javascript:void(0)"  class="btn btn-success view" val="'.$user_id.'"><i class="fas fa-eye"></i></a>&nbsp;<a href="javascript:void(0)"  class="btn btn-info edit" val="'.$user_id.'"><i class="far fa-edit"></i></a>&nbsp; <a href="javascript:void(0)"  class="btn btn-danger delete" val="'.$user_id.'"><i class="fas fa-trash-alt"></i></a>
        			  ';
						}
            		}
					else if($sColumns[$i] =='usertype_id'){
						if($user_type==1){
							$row[] ='Customer';
						}elseif($user_type==2){ 
							$row[] ='Representative';
						}
						elseif($user_type==3){ 
							$row[] ='Employee';
						}
						else {
							$row[] ='Supplier';
						}
					}
					elseif($sColumns[$i] =='status'){
						if($status=='1'){
							$row[]='Active';
						
						}else {
							$row[]='Inactive';
						}
					}
					else if($sColumns[$i] =='business_id'){
						if($business_id!=0){
							//echo "select * from business where business_id='$business_id'";exit;
							$business=$this->db->query("select * from nrm_business where business_id='$business_id'")->result_array();
							if(!empty($business)){
								$row[] = $business[0]['business_name'];
							}else{
								$row[] ='';
							}
						}else {
							$row[] ='';
						}
					}
					
					
					
					
					
					
					
					
					
					
					
					else if($sColumns[$i] =='user_status')
            		{
            			
            			 $Enable_status="";
			             $Disable_status="";
						 
						 
					    if($status=='Enable')
						{
						 $Enable_status="selected";
			             $Disable_status="";
						
						}
						else if($status=='Disable')
						{
						 $Enable_status="";
			             $Disable_status="selected";
						 
						}
						
            			
            			 $row[]='<select name="status" id="status" class="selectpicker update_status">
								 <option value="Enable@@@'.$user_id.'"  '.$Enable_status.'>Enable</option>
								 <option value="Disable@@@'.$user_id.'"  '.$Disable_status.'>Disable</option>
								 </select>';
            			 
            		}
                    else if ( $sColumns[$i] != ' ' )
                    {
                        /* General output */
                        $row[] = $aRow[ $sColumns[$i] ];
                    }
                }
                $output['aaData'][] = $row;
            }
            //print_r($output);exit;
        echo json_encode( $output );
    }
	}
	
	
	public function add($user_id=''){
		
		if ($this->session->userdata('logged_in'))		
		{  
			$session_data = $this->session->userdata('logged_in');
			$slang="english";
		    $this->lang->load($slang, $slang);
		    $data['lang']=$this->lang->language; 
			$data['title'] = "User Role";
			if($user_id!=''){
				$user_details=$this->db->query("select * from wc_users where user_id='$user_id'")->result_array();
				if(!empty($user_details)){
					$data['user_id'] = $user_details[0]['user_id'];
					$data['user_name'] = $user_details[0]['user_name'];
					$data['user_address'] = $user_details[0]['user_address'];
					$data['user_email'] = $user_details[0]['user_email'];
					$data['user_telephone'] = $user_details[0]['user_telephone'];
					$data['user_mobile'] = $user_details[0]['user_mobile'];
					$data['user_password'] = $user_details[0]['user_password'];
					
					$data['user_status'] = $user_details[0]['user_status'];
					
					
				}
			}else {
			        $data['user_id'] = '';
					$data['user_name'] = '';
					$data['user_address'] = '';
					$data['user_email'] = '';
					$data['user_telephone'] = '';
					$data['user_mobile'] = '';
					$data['user_password'] = '';
					
					$data['user_status'] = '';
					
			}
			$this->load->view('admin/user/user_management', $data);
		} 
		else 
		{
			//If no session, redirect to login page
			/*redirect('login', 'refresh');*/
            redirect('administrator', 'refresh');
		}
	}
	
	
		
	  public function checkexistemail()
{			// checking the email  exist  validation in add form
	
    $user_email = $this->input->post('user_email');
    $sql = "SELECT user_id,user_email FROM wc_users WHERE user_email = '$user_email'";
    $query = $this->db->query($sql);
    if( $query->num_rows() > 0 ){
        echo 'false';
    } else {
        echo 'true';
    }
	
}

   public function checkexistmobile()
{			// checking the email  exist  validation in add form
	
    $user_mobile = $this->input->post('user_mobile');
    $sql = "SELECT user_id,user_mobile FROM wc_users WHERE user_mobile = '$user_mobile'";
    $query = $this->db->query($sql);
    if( $query->num_rows() > 0 ){
        echo 'false';
    } else {
        echo 'true';
    }
	
}
	
	public function insert_user(){
		
		if ($this->session->userdata('logged_in')) {
			$session_data = $this->session->userdata('logged_in');
			$created_date = date('Y-m-d');
			$userid=$session_data['id'];
			$user_id=$this->input->post('user_id');
			
			if($user_id!='')
			{
				
				  $data = array(
				    'user_id'=>$this->input->post('user_id'),
					
					'user_name'=>$this->input->post('user_name'),
					'user_address'=>$this->input->post('user_address'),
					'user_email'=>$this->input->post('user_email'),
					'user_telephone'=>$this->input->post('user_telephone'),
					'user_mobile'=>$this->input->post('user_mobile'),
					/*'user_password'=>$this->input->post('user_password'),*/
					
					'user_status'=>$this->input->post('user_status'),
					);
					
				  $updateuserrole = $this->User_model->user_update($data,$user_id);
			      echo $user_id;
				
			}else {
				
				
				
				   $data = array(
				    'user_id'=>$this->input->post('user_id'),
					
					'user_name'=>$this->input->post('user_name'),
					'user_address'=>$this->input->post('user_address'),
					'user_email'=>$this->input->post('user_email'),
					'user_telephone'=>$this->input->post('user_telephone'),
					'user_mobile'=>$this->input->post('user_mobile'),
					'user_password'=>$this->input->post('user_password'),
					
					'user_status'=>$this->input->post('user_status'),
					);
					$this->User_model->user_insert($data);
					$user_id=$this->db->insert_id();
					
					
					echo $user_id;
				}
			
		}else {
		
            /*redirect('login', 'refresh');*/
            redirect('administrator', 'refresh');
       
		}  
	}
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
	public function change_password(){
		
		if ($this->session->userdata('logged_in'))		
		{  
			$session_data = $this->session->userdata('logged_in');
			$slang="english";
		    $this->lang->load($slang, $slang);
		    $data['lang']=$this->lang->language; 
			
			$user_id = $session_data['id'];
			$data['user_id'] = $user_id;
			$data['title'] = "User Role";
			if($user_id!=''){
				$user_details=$this->db->query("select * from wc_users where user_id='$user_id'")->result_array();
				if(!empty($user_details)){}
			}else {}
			$this->load->view('admin/user/user_password', $data);
		} 
		else 
		{
			//If no session, redirect to login page
			/*redirect('login', 'refresh');*/
            redirect('administrator', 'refresh');
		}
	}
	
	public function update_change_password()			//for displaying the data of the staff into the table
   {
	
	       $user_id=$this->input->post('user_id');
	          $data = array(
				'user_password' => ($this->input->post('user_password'))
                 );
				//print_r($staffdata);
				$updateuserrole = $this->User_model->user_update($data, $user_id);
			    echo $user_id;
				
				
				//redirect('manage-employee');
   }
   
 
	public function deleteuser()
    {
		if ($this->session->userdata('logged_in')) 
		{
			$user_id = $this->input->post('user_id');
		
			//$this->db->query("delete from user where user_id='$user_id'");
			$this->db->query("update wc_users set deleted ='1',user_status='Disable' where user_id ='$user_id'");
			$this->session->set_flashdata('added', '<div align="left" style="color:green; font-size:15px; border:1px #398AB9 solid;">
		    Service Deleted Successfully</div>');

			//redirect('users');
		} 
		else 
		{
			//If no session, redirect to login page
			/*redirect('login', 'refresh');*/
            redirect('administrator', 'refresh');
		}
	}
	
	
	public function get_locations(){
		if ($this->session->userdata('logged_in')) 
		{
			
			$business_id = $this->input->post('business_id');
			$session_data=$this->session->userdata('logged_in');
			$usertype = $session_data['rolecode'];
			$userid = $session_data['id'];
			$data['user_id'] = $session_data['user_id'];
			
			$locations=$this->db->query("select * from nrm_location where business_id='$business_id ' and status='1'")->result();
		    if($locations){
				echo "<select  name='location' id='location'  class='form-control'>
				<option value=''>Select</option>";
					   
				foreach($locations as $result){
					$location_id=$result->location_id;
					$location_name=$result->location_name;
					echo "
						  <option value='".$location_id."'>".$location_name."</option>";
				}
				echo "</select>";
			}else {
				echo 0;
			}
		  
		} 
		else 
		{
			//If no session, redirect to login page
			/*redirect('login', 'refresh');*/
            redirect('administrator', 'refresh');
		}
	}
	
	public function view($user_id=''){
		
		if ($this->session->userdata('logged_in'))		
		{  
			$session_data = $this->session->userdata('logged_in');
			$slang="english";
		    $this->lang->load($slang, $slang);
		    $data['lang']=$this->lang->language; 
			$data['title'] = "User Role";
			if($user_id!=''){
				$user_details=$this->db->query("select * from wc_users where user_id='$user_id'")->result_array();
				if(!empty($user_details)){
					$data['user_id'] = $user_details[0]['user_id'];
					$data['user_name'] = $user_details[0]['user_name'];
					$data['user_address'] = $user_details[0]['user_address'];
					$data['user_email'] = $user_details[0]['user_email'];
					$data['user_telephone'] = $user_details[0]['user_telephone'];
					$data['user_mobile'] = $user_details[0]['user_mobile'];
					$data['user_password'] = $user_details[0]['user_password'];
					
					$data['user_status'] = $user_details[0]['user_status'];
					
				}
			}else {
			        $data['user_id'] = '';
					$data['user_name'] = '';
					$data['user_address'] = '';
					$data['user_email'] = '';
					$data['user_telephone'] = '';
					$data['user_mobile'] = '';
					$data['user_password'] = '';
					
					$data['user_status'] = '';
			}
			$this->load->view('admin/user/user_view', $data);
		} 
		else 
		{
			//If no session, redirect to login page
			/*redirect('login', 'refresh');*/
            redirect('administrator', 'refresh');
		}
	}
	public function updatestatus()
{				//for email exist validation in edit customer form 
	
	
  $id=trim($this->input->get('id'));
  $status=trim($this->input->get('status'));
  //echo "update customer_master set status ='$status' where customer_id ='$id'";
  $this->db->query("update user set user_status ='$status' where user_id ='$id'");


		
}
	
	
}
?>