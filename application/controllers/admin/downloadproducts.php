<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Downloadproducts extends CI_Controller
{
    public function __construct()
	{
		parent::__construct();		
		/*$this->load->library('pagination');
		$this->load->database();		
		$this->load->library('Upload');
		$this->load->helper('security');	
		$this->load->library('Form_validation');
		$this->load->helper(array('form', 'html', 'file'));
		$this->load->library('pagination');
		$this->load->library('email');*/
   	    $this->load->helper('url');
		$this->load->helper('download');
		$this->load->helper('file');



       //$this->load->model('Setting_model');
      
		
    	if ($this->session->userdata('logged_in')) {
            $session_data = $this->session->userdata('logged_in');
            $data['username'] = $session_data['username'];
            $data['firstname'] = $session_data['firstname'];
            $data['rolecode'] = $session_data['rolecode'];
			/* $timezone = $session_data['timezone'];
			date_default_timezone_set($timezone); */
         
        } else {
            //If no session, redirect to login page
            /*redirect('login', 'refresh');*/
            redirect('administrator', 'refresh');
        }
    }
	
    public function index()
	{
		if ($this->session->userdata('logged_in'))		
		{  
			$session_data = $this->session->userdata('logged_in');
			$data['rolecode'] = $session_data['rolecode'];
			$data['usertype'] = $session_data['rolecode'];
			$data['roles'] = $session_data['roles'];
			$data['title'] = "Users";
			$slang="english";
			$this->lang->load($slang, $slang);
			$data['lang']=$this->lang->language; 
			//$data['setting_content'] = $this->Setting_model->get_setting_content();
			//print_r($data['setting_content']);
			//echo "aaaaaaaaa";exit;
			$this->load->view('admin/downloadproduct/downloadproduct',$data);
		} 
		else 
		{
			//If no session, redirect to login page
			/*redirect('login', 'refresh');*/
            redirect('administrator', 'refresh');
		}
	}


  public function downloadproductscsv()
  {	  
$sql = "SELECT wc_brief.status,wc_clients.client_name,wc_brands.brand_name,wc_brief.brief_title,wc_product_price.product_name,wc_briefstatus.status_name,
wc_brief_product_list.product_quty,wc_product_price.product_price,wc_brief.brief_due_date 
FROM `wc_brief_product_list` INNER JOIN wc_product_price
ON wc_brief_product_list.`product_id` = wc_product_price.`product_id`
INNER JOIN wc_brief
ON wc_brief_product_list.brief_id= wc_brief.brief_id
INNER JOIN wc_brands
ON wc_brief.brand_id = wc_brands.brand_id
INNER JOIN wc_clients
ON wc_brands.client_id = wc_clients.client_id
INNER JOIN wc_briefstatus
ON wc_brief.status = wc_briefstatus.status_id";
$query = $this->db->query($sql);
$results = $query->result_array();

    if( $query->num_rows() > 0 ){
        //echo 'false';
		//print_r($results);
   $filename = 'download_'.date('Ymd').'.csv'; 
  // header("Content-Description: File Transfer"); 
   //header("Content-Disposition: attachment; filename=$filename"); 
   //header("Content-Type: application/csv;");

// file creation 
   $file = fopen('php://output', 'w');
   $header = array('Closed(Archived)', 'Client', 'Brand', 'Name', 'Labels', 'List', 'Quantity', 'UnitCost', 'Day(EndDate)'); 
   fputcsv($file, $header);

if (count($results) > 0) {
    foreach ($results as $row) {
		//print_r($row);	
        fputcsv($file, $row);
    }
	fclose($file); 
   exit; 
 
   ///////////////////
 
    $data = file_get_contents('php://output'); 
    $name = $filename;

    header('Content-Description: File Transfer');
header('Content-Type: application/csv');
header('Content-Disposition: attachment; filename='.$name);
header('Expires: 0');
header('Cache-Control: must-revalidate');
header('Pragma: public');
header('Content-Length: ' . filesize($file));

    exit();

    force_download($name, $data);
    fclose($file);
}
		
		
		
    } else {
        echo 'true';
    }


	  
	  
	  
	  
  }

	
}
?>