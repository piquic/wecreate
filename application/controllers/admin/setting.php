<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Setting extends CI_Controller
{
    public function __construct()
	{
		parent::__construct();		
		$this->load->library('pagination');
		$this->load->database();		
		$this->load->library('Upload');
		$this->load->helper('security');	
		$this->load->library('Form_validation');
		$this->load->helper(array('form', 'html', 'file'));
		$this->load->library('pagination');
		$this->load->library('email');
        $this->load->model('Setting_model');
      
		
    	if ($this->session->userdata('logged_in')) {
            $session_data = $this->session->userdata('logged_in');
            $data['username'] = $session_data['username'];
            $data['firstname'] = $session_data['firstname'];
            $data['rolecode'] = $session_data['rolecode'];
			/* $timezone = $session_data['timezone'];
			date_default_timezone_set($timezone); */
         
        } else {
            //If no session, redirect to login page
            /*redirect('login', 'refresh');*/
            redirect('administrator', 'refresh');
        }
    }
	
    public function index()
	{
		if ($this->session->userdata('logged_in'))		
		{  
			$session_data = $this->session->userdata('logged_in');
			$data['rolecode'] = $session_data['rolecode'];
			$data['usertype'] = $session_data['rolecode'];
			$data['roles'] = $session_data['roles'];
			$data['title'] = "Users";
			$slang="english";
			$this->lang->load($slang, $slang);
			$data['lang']=$this->lang->language; 
			$data['setting_content'] = $this->Setting_model->get_setting_content();
			//print_r($data['setting_content']);
			//echo "aaaaaaaaa";exit;
			$this->load->view('admin/setting/setting_management',$data);
		} 
		else 
		{
			//If no session, redirect to login page
			/*redirect('login', 'refresh');*/
            redirect('administrator', 'refresh');
		}
	}

	
	public function update_setting()			//for displaying the data of the staff into the table
   {
		if ($this->session->userdata('logged_in')) {
			$session_data = $this->session->userdata('logged_in');
			$created_date = date('Y-m-d');
			$userid=$session_data['id'];
			//echo $id=$this->input->post('id');

			 $all_arr=$this->input->post();
			/*echo "<pre>";
			 print_r($all_arr);
			 exit;*/

			 $array_id=$all_arr['setting_id'];
			 $array_key=$all_arr['setting_key'];
			 $array_value=$all_arr['setting_value'];
			
			 foreach ($array_id as $key1 => $value1) {
			 	$id= $array_id[$key1];
			 	$key= $array_key[$key1];
			 	$value= $array_value[$key1];

			 	$value = html_entity_decode($value);
				
				//exit;
			 	$report = $this->Setting_model->setting_update($id,$key,$value);
			 	
			 	  
				//$this->load->view('admin/setting/setting_management',$data);
			
			 	
			 }
			 //exit();
			 redirect('administrator/manage-setting');
			//exit();
			//upload file
	
		}else {
		
            /*redirect('login', 'refresh');*/
            redirect('administrator', 'refresh');
       
		} 
   }
  

	
}
?>