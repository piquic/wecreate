<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Administrator extends CI_Controller
{
    public function __construct()
	{
		parent::__construct();	
		$this->load->model('Administrator_model', '', TRUE);	
		$this->load->library('pagination');
		$this->load->database();		
		$this->load->library('Upload');
		$this->load->helper('security');	
		$this->load->library('Form_validation');
		$this->load->helper(array('form', 'html', 'file'));
		$this->load->library('pagination');
		$this->load->library('email');
        $this->load->library('userlib');
		
    	if ($this->session->userdata('logged_in')) {
            $session_data = $this->session->userdata('logged_in');
            $data['username'] = $session_data['username'];
            $data['firstname'] = $session_data['firstname'];
            $data['rolecode'] = $session_data['rolecode'];
			
         
        } else {
        	//echo "aaaaaa";exit;
            //If no session, redirect to login page
            //redirect('administrator', 'refresh');
        }
    }
	
   public function index()
    {
        
		
		if ($this->session->userdata('logged_in')) 
        {
            //echo 'welcome to NC';
			redirect('administrator/dashboard', 'refresh');
        } 
        else 
        {
            $this->load->helper(array('form'));
            $this->load->view('admin/signin');
			
        }
    }

      public function login()
    {
        
		
		if ($this->session->userdata('logged_in')) 
        {
            //echo 'welcome to NC';
			redirect('administrator/dashboard', 'refresh');
        } 
        else 
        {
            $this->load->helper(array('form'));
            $this->load->view('admin/signin');
			
        }
    }


    public function check_login()
    {
        if ($this->session->userdata('logged_in')) 
        {
			//redirect('manage-job', 'refresh');
			 redirect('administrator/dashboard', 'refresh');
			
        } 
        else 
        {
            $this->load->library('form_validation');
            $this->form_validation->set_rules('email', 'Username', 'trim|required|xss_clean');
            $this->form_validation->set_rules('password', 'Password', 'trim|required|xss_clean|callback_check_database');
			//$this->form_validation->set_rules('g-recaptcha-response', 'recaptcha validation', 'required|callback_validate_captcha');
			//$this->form_validation->set_message('validate_captcha', 'Please check the the captcha form');
            $this->form_validation->set_error_delimiters('<div class="alert alert-error" style="border:1px #f00 solid;"><a class="close" data-dismiss="alert">x</a><strong>', '</strong></div>');
			
            if ($this->form_validation->run() == FALSE) {
                  
				//  echo $this->session->flashdata('check_database');  exit();
                $this->load->view('admin/signin');
            } else {
				
                //redirect('manage-job', 'refresh');
				 redirect('administrator/dashboard', 'refresh');
            }
        }

    }

        public function check_database($password)
    {
        if ($this->session->userdata('logged_in')) 
        {
            redirect('administrator/dashboard', 'refresh');
        } 
        else 
        {
            $username = $this->input->post('email');
			
			
       
        
            $result = $this->Administrator_model->loginadmin($username, $password);
            if ($result) 
            {
                $sess_array = array();
                foreach ($result as $row) 
                {
				
				    $sess_array = array(
					    'id' => $row->user_id,
						'user_id' => $row->user_id,
						'email' => $row->email,
						'username' => $row->email,
						'firstname' => $row->first_name,
						'rolecode' => '0',
						'user_type' => '0',
						'roles' => '0',
						
                	);
				
				}
				
				//print_r($sess_array);exit;
                    $this->session->set_userdata('logged_in', $sess_array);
					
					 return TRUE;
            }
			else {
			 $this->form_validation->set_message('check_database', '<font style="color:#ffff">Invalid username or password</font>');
                return false;
			
				
            }
        }
    }

      public function logout()
    {
		@session_destroy();
        $this->session->unset_userdata('logged_in');
        $this->session->unset_userdata('wekan_integration');
        redirect('Administrator/login', 'refresh');
    }
	

    public function successful()
    {
        if ($this->session->userdata('logged_in')) 
        {
            //echo "welcome";exit;
			//redirect('manage-job', 'refresh');
			 redirect('administrator/dashboard', 'refresh');
        } 
        else 
        {
            $data['password_sent'] = '<font style="color:green;"><div align="center">New Password Generated successfully!. <br>Please Check Your Inbox or Spam Folder</div></font>';
            $this->load->view('administrator/login', $data);
        }
    }

    public function unsuccessful()
    {
        if ($this->session->userdata('logged_in')) 
        {
            redirect('administrator/dashboard', 'refresh');
        } 
        else 
        {
            $data['password_sent'] = '<font style="color:#F00;"><div align="center">Email Doesnot Exist!</div></font>';
            $this->load->view('login', $data);
        }
    }

    public function dashboard()
    {
        if ($this->session->userdata('logged_in')) {
			
            $session_data = $this->session->userdata('logged_in');
            $data['username'] = $session_data['username'];
            $userid = $session_data['id'];
            $data['firstname'] = $session_data['firstname'];
            $data['rolecode'] = $session_data['rolecode'];
			$data['userid'] = $session_data['id'];
            $data['email'] = $session_data['email'];
			//$data['color']=$this->getColor($session_data['id']);
            $data['title'] = "Dashboard";
            $data['title'] = 'Welcome To Admin Dashboard';	
			
			
			
			$this->load->view('admin/dashboard', $data);     
			
		}
		else {
            //If no session, redirect to login page
            redirect('administrator/login', 'refresh');
        }
    }


	
}
?>