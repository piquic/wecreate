<table class="table border-bottom text-wc" >
                        <thead>
                          <tr>
                            <th scope="col">#</th>
                            <th scope="col">Project Name</th>
                            <!--<th scope="col">Client Id</th>-->
                            <th scope="col">Status</th>
                          </tr>
                        </thead>
                        <tbody >
                  <?php 
                  $session_data = $this->session->userdata('front_logged_in');
                  $user_id = $session_data['user_id'];
                  if(!empty($retainer_details1)){
                  foreach($retainer_details1 as $key => $retainer_value){ 
                    $retainer_id=$retainer_value['retainer_id'];
                    $retainer_name=$retainer_value['retainer_name'];
                    $retainer_client_id=$retainer_value['client_id'];
                    $retainer_status=$retainer_value['status'];
                    $client_details=$this->db->query("select * from wc_clients where deleted='0' and status='Active' and client_id='$retainer_client_id'")->result_array();
                    $client_name1=$client_details[0]['client_name'];
                    ?>
                    <tr>
                        <th scope="row"><span id="retainer_edit_<?php echo $retainer_id ?>" data-toggle="modal" data-target="#retainer_editModel<?php echo $retainer_id; ?>"><i class="far fa-edit"></i></span> <span id="retainer_delete_<?php echo $retainer_id ?>" data-toggle="modal" data-target="#retainer_deleteModel<?php echo $retainer_id; ?>"><i class="far fa-trash-alt"></i></span></th>
                        <td><?php echo $retainer_name ?></td>
                        <!--<td><?php //echo $client_name1 ?></td>-->
                        <td><?php echo $retainer_status ?></td>
                        <div class="modal fade" id="retainer_editModel<?php echo $retainer_id; ?>" tabindex="-1" role="dialog" aria-labelledby="retainer_editModel<?php echo $retainer_id; ?>Label" aria-hidden="true">
                          <div class="modal-dialog" role="document">
                            <div class="modal-content">
                              <div class="modal-header">
                                <h5 class="modal-title" id="retainer_editModel<?php echo $retainer_id; ?>Label">Update Project Details</h5>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                  <span aria-hidden="true">&times;</span>
                                </button>
                              </div>
                              <div class="modal-body">
                                <form>
                                 <div class="form-group d-none">
                                    <label class="col-form-label">Client <span style="color:#F00">*</span> </label>
                                    <select  name="retainer_client_id<?php echo $retainer_id; ?>" id="retainer_client_id<?php echo $retainer_id; ?>" class="custom-select" style="width: 100%">
                                        <!--<option value="">Select</option>-->
                                        <?php
                                        $users_details=$this->db->query("select * from wc_users where user_id='$user_id'")->result_array();
                                        foreach($users_details as $key => $usersdetails){
                                            $client_id=$usersdetails['client_id'];
                                        
                                            $client_details=$this->db->query("select * from wc_clients where deleted='0' and status='Active' and client_id='$client_id' order by  client_name asc")->result_array();
                                            //print_r("select * from wc_users where deleted='0' and status='Active' order by  client_name asc");
                                            if(!empty($client_details)){
                                                foreach($client_details as $key => $clientdetails){
                                                    //$client_id=$clientdetails['client_id'];
                                                    $client_name=$clientdetails['client_name'];
                                                    ?>
                                                    <option value="<?php echo $client_id;?>"  <?php if($retainer_client_id==$client_id) { ?> selected="selected" <?php } ?> ><?php echo $client_name;?></option>
                                                <?php }
                                            }
                                        }
                                        ?>
                                    </select>
                                    <span class="small text-danger" id="errmsg_retainer_client_id<?php echo $retainer_id; ?>"></span>
                                  </div>
                                  <div class="form-group">
                                    <label for="retainer_name<?php echo $retainer_id; ?>" class="col-form-label">Project Name</label>
                                    <input  type='text'  class="form-control" id="retainer_name<?php echo $retainer_id; ?>" name="retainer_name<?php echo $retainer_id; ?>" value="<?php echo $retainer_name ?>">
                                    <span class="small text-danger" id="errmsg_retainer_name<?php echo $retainer_id; ?>"></span>
                                  </div>
                                  
                                  <div class="form-group">
                                    <label for="retainer_status" class="col-form-label">Status</label>
                                    <select name="retainer_status<?php echo $retainer_id; ?>" id="retainer_status<?php echo $retainer_id; ?>" class="form-control" >
                                      <!--<option value=''>Select</option>-->
                                      <option value="Active" <?php if($retainer_status=='Active') { ?> selected="selected" <?php } ?>>Active</option>
                                      <option value="Inactive" <?php if($retainer_status=='Inactive') { ?> selected="selected" <?php } ?>>Inactive</option>
                                    </select>
                                    <span class="small text-danger" id="errmsg_retainer_status<?php echo $retainer_id; ?>"></span>
                                  </div>
                                </form>
                              </div>
                              <div class="modal-footer">
                                <button type="button" class="btn btn-outline-wc" id="edit_product<?php echo $retainer_id ?>" onclick='edit_retainer("<?php echo $retainer_id ?>")' data-dismiss="modal">&emsp;Update&emsp;</button>
                                <button type="button" class="btn btn-outline-wc" data-dismiss="modal">&emsp;Close&emsp;</button>
                                
                              </div>
                            </div>
                          </div>
                        </div>
                        
                        <div class="modal fade" id="retainer_deleteModel<?php  echo $retainer_id; ?>" tabindex="-1" role="dialog" aria-hidden="true">
                            <div class="modal-dialog" role="document">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <h5 class="modal-title text-dark" id="modMsg">Are you sure you want to delete?</h5>
                                        <button type="button" class="close text-danger" data-dismiss="modal" aria-label="Close">
                                            <span aria-hidden="true" >&times;</span>
                                        </button>
                                    </div>
                                    <div class="modal-footer">
                                      <button type="button" id="hideDel" class="btn btn-outline-wc" onclick="delete_retainer('<?php  echo $retainer_id; ?>');" data-dismiss="modal">&emsp;Delete&emsp;</button>
                                        <button type="button" class="btn btn-outline-wc" data-dismiss="modal">&emsp;Close&emsp;</button>
                                        
                                    </div>
                                </div>
                            </div>
                        </div>
                    </tr>
                    
<?php }
}
else{
  ?>
  <tr>
   
    <td colspan="10">No Records</td>
  </tr>
  <?php
}
?>

</tbody>
                      </table>