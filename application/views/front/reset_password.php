<?php
session_start();
error_reporting(0);

$data['page_title'] = "reset password";

$this->load->view('front/includes/header',$data);
// $this->load->view('front/includes/menu');

if ($this->session->userdata('front_logged_in')) {
	$session_data = $this->session->userdata('front_logged_in');
	$user_id = $session_data['user_id'];
	$user_name = $session_data['user_name'];
	$_SESSION['user_id']=$user_id;
	$_SESSION['user_name']=$user_name;
}
else
{
	$user_id = '';
	$user_name = ''; 
	$_SESSION['user_id']=$user_id;
	$_SESSION['user_name']=$user_name;
}

?>
<style>
.error
{
clear: both;
}
</style>

<div class="container">
	<div class="row my-4">

		<div class="col-12 col-sm-12 col-md-12 col-lg-3 col-xl-3 pb-4">&nbsp;</div>
		<div class="col-12 col-sm-12 col-md-12 col-lg-6 col-xl-6 pb-4">
			<div class="shadow mt-4">
				<h1 class="display-4 py-3 text-center">Reset Password</h1>
				<form class="form form-horizontal has-validation-callback"  id="resetpassword_form" method="post">
					<div id="success_message" style="display:none;">
						<div class="alert alert-success">
							<strong>Your Password has been successfully updated!</strong>
						</div>
					</div>

					<div class="form-row">
						<div class="form-group col px-4">
							<input type="hidden" class="form-control mb-2" id="uid" name="uid" value="<?php echo $uid; ?>">

							<label class="sr-only" for="upassword">Set a password</label>
							<div class="input-group">
								<div class="input-group-prepend">
									<span class="input-group-text" id="inputGroupPrepend"><i class="fas fa-key"></i></span>
								</div>

								<input type="password" class="form-control" id="upassword" name="upassword" placeholder="Set a password" required>
								
							</div>
						</div>
					</div>
					

					<div class="form-row">
						<div class="form-group col px-4">
							<input type="hidden" class="form-control mb-2" id="uid" name="uid" value="<?php echo $uid; ?>">

							<label class="sr-only" for="ure_password">Re-enter the password</label>
							<div class="input-group">
								<div class="input-group-prepend">
									<span class="input-group-text" id="inputGroupPrepend"><i class="fas fa-key"></i></span>
								</div>
								<input type="password" class="form-control" id="ure_password" name="ure_password" placeholder="Re-enter the password" required>
								<br>
							</div>
						</div>
					</div>

					

					<div class="form-row text-center">
						<div class="form-group col px-4 pb-4">
							<button name="regSubmit" type="submit" class="btn btn-wc btn-block">Reset Password <i class="fas fa-key"></i></button>


							<span id="success" class="successMsg" style="display:none;"></span>
						</div>
					</div>

				</form>
			</div>
		</div>
		<div class="col-12 col-sm-12 col-md-12 col-lg-3 col-xl-3 pb-4">&nbsp;</div>		
	</div>
</div>

<?php
$this->load->view('front/includes/footer');
?>


<script type="text/javascript">

	$(function () {
		$("#resetpassword_form").validate({
			rules: {

				<?php
				if($user_name=='')
					{ ?>
						upassword:{
							required:true,
							minlength:5,
						},
						ure_password: {
							required:true,
							equalTo: "#upassword" ,
							minlength:5,
						},
					<?php  }  ?>

				},
				errorPlacement: function(error, element) {
					error.insertAfter( element );
				

				},
				submitHandler: function (form) {

					var uid=$('#uid').val();
					var upassword=$('#upassword').val();
					$("#loader").show();
					$.ajax({
						method:'post',
						url: "<?php echo base_url();?>front/login/reset_password_update", 
						data:"&uid="+uid+"&upassword="+upassword,
						success: function(result){
							$("#loader").hide();
							if(result)
							{
								$("#success_message").show();
								window.setTimeout(function() {
									window.location = "<?php echo base_url();?>";
								}, 5000);


							}
						}
					});
				}
			});
	});

</script>


