<?php
session_start();
error_reporting(0);

//print_r($_SESSION);

/*if ($this->session->userdata('front_logged_in')) {
$session_data = $this->session->userdata('front_logged_in');
$user_id = $session_data['user_id'];
$user_name = $session_data['user_name'];
$_SESSION['user_id']=$user_id;
$_SESSION['user_name']=$user_name;
//$type1=$this->session->userdata('user_type_id');
  $type1 = $session_data['user_type_id'];
 $user_type_id=$type1;
 $brand_access=$session_data['brand_access'];

  $checkquerys = $this->db->query("select user_type_name from wc_users_type where  user_type_id='$user_type_id' ")->result_array();
$job_title=$checkquerys[0]['user_type_name'];
	
}
else
{
$user_id = '';
$user_name = ''; 
$_SESSION['user_id']=$user_id;
$_SESSION['user_name']=$user_name;
}*/
?>

<?php $data['page_title'] = "Feedback";

$this->load->view('front/includes/header',$data);
$this->load->view('front/includes/topnav');
 ?>

<link rel="stylesheet" href="<?php echo base_url(); ?>website-assets/startRatingJquery/demo/styles.css">
<style type="text/css">
.gl-star-rating-text
{
display: block;
}

 	
.btn-wc-green {
color: #fff !important;
background-color: #76aea1 !important;
border-color: #76aea1 !important;
}

 </style>

<div class="container-fluid">
	<div class="row">

		<!-- Main Body -->
		<div class="col-12 col-sm-12 col-md-12">
			<main class="p-3">

				<div class="row">
					<div class="col-12 col-sm-12 col-md-12">
						<div class="p-3">

							<?php
							if ($this->session->userdata('front_logged_in')) {
							?>
							<p class="text-wc lead" onclick="goDashboard();"  style="cursor: pointer;"><i class="fas fa-arrow-alt-circle-left"></i>&nbsp;Back to Dashboard</p>
							<?php
						}
						?>
						</div>
					</div>
				</div>

				<div class="container">

					<div class="row">
						<div class="col-12 col-sm-12 col-md-12">
							<div class="p-3 text-center">
								<h3 class="display-4">
									Thanks for submitting feedback.
								</h3>

								<!-- <p class="lead">
									<?php echo $pagecontents[0]['content'] ?>
								</p> -->
							</div>
						</div>
					</div>

					
				

				
				</div>
			</main>
		</div>
	</div>
</div>


<script src="<?php echo base_url(); ?>website-assets/startRatingJquery/src/star-rating.js?ver=3.1.5"></script>
<script>


var destroyed = false;
var starratings = new StarRating( '.star-rating', {
onClick: function( el ) {

alert(el.selectedIndex);
console.log( 'Selected: ' + el[el.selectedIndex].text );
},
});

</script>


<?php
$this->load->view('front/includes/footer');
?>
<script>

	function getReviewValue(val,id)
		{
			//alert(val);
			
			$("#hid_review_"+id).val(val);
		}
		


	function setReviewReason(val,id,content_id)
	{
		$("#hid_review_reason_"+id).val(val);

		var val = val.replace(/ /g, '_');

		//alert(val);

		$(".qty_btn_"+id).removeClass("btn-wc-green");
		$("#qty_"+content_id).addClass("btn-wc-green");
		 

	}

	


	function submitFeedback()
	{

		var brief_id=$("#brief_id").val();
		var user_id=$("#user_id").val();

		var hid_feedback_type_id = $('input[name="hid_feedback_type_id[]"]').map(function () {
        if(this.value!='')
        {return this.value; // $(this).val()
        }
        else
        {return 0;}
		
        }).get();



        console.log(hid_feedback_type_id);

        var hid_review = $('input[name="hid_review[]"]').map(function () {
        if(this.value!='')
        {return this.value; // $(this).val()
        }
        else
        {return 0;}
		
        }).get();

        console.log(hid_review);


        /*var check_hid_review=$.inArray( 0, hid_review );
         console.log(check_hid_review);*/

        if($.inArray( 0, hid_review )==0)
        {
        	// alert("You need to enter review before proceed.");
        	$('#modal_msg').modal('show');
			$('#txtMsg').html('You need to enter review before proceed.');
        	return false;
        }



        var hid_review_reason = $('input[name="hid_review_reason[]"]').map(function () {
        if(this.value!='')
        {return this.value; // $(this).val()
        }
        else
        {return 0;}
		
        }).get();

        console.log(hid_review_reason);

         if($.inArray( 0, hid_review_reason )==0)
        {
        	// alert("You need to enter reason before proceed.");
        	$('#modal_msg').modal('show');
			$('#txtMsg').html('You need to enter reason before proceed.');
        	return false;
        }





		$.ajax({

		method:'POST',
		url: "<?php echo base_url();?>front/feedback/insertFeedback",
		/*data:"&brief_id="+brief_id+"&quality="+quality+"&quality_reason="+quality_reason+"&speed="+speed+"&speed_reason="+speed_reason,*/
		data: {user_id:user_id,brief_id:brief_id,feedback_type_id:hid_feedback_type_id,review:hid_review,review_reason:hid_review_reason},
		success: function(result){
		//alert(result);
		//if(result) {

				<?php
				if ($this->session->userdata('front_logged_in')) {
				?>
				window.location.href="<?php echo base_url();?>dashboard";
				<?php
				}
				else
				{
				?>

				window.location.href="<?php echo base_url();?>feedback-thankyou";
				<?php

				}
				?>

		


		//}
		}
		});




		
		
		/*var quality=$("#hid_quality").val();
		var quality_reason=$("#hid_quality_reason").val();

		var speed=$("#hid_speed").val();
		var speed_reason=$("#hid_speed_reason").val();




		if(quality=='')
		{
			alert("Please select Quality Rating.");
			return false;
		}
        else if(quality_reason=='')
		{
			alert("Please select Quality Reason.");
			return false;
		}
        else if(speed=='')
		{
			alert("Please select Speed Rating.");
			return false;
		}
        else if(speed_reason=='')
		{
			alert("Please select Speed Reason.");
			return false;
		}

        else
		{

		

		$.ajax({

	   			method:'POST',
	   			url: "<?php echo base_url();?>front/feedback/insertFeedback",
	   			data:"&brief_id="+brief_id+"&quality="+quality+"&quality_reason="+quality_reason+"&speed="+speed+"&speed_reason="+speed_reason,

	   			success: function(result){
	   				//alert(result);
	   				//if(result) {

	   					window.location.href="<?php echo base_url();?>dashboard";
	   					
	   					
	   				//}
	   			}
	   		});







	   }*/




	}

</script>





