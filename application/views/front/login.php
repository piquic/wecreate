<?php
session_start();
error_reporting(0);





if ($this->session->userdata('front_logged_in')) {
$session_data = $this->session->userdata('front_logged_in');
$user_id = $session_data['user_id'];
$user_name = $session_data['user_name'];
$_SESSION['user_id']=$user_id;
$_SESSION['user_name']=$user_name;
$type1=$this->session->userdata('type1');

}
else
{
$user_id = '';
$user_name = ''; 
$_SESSION['user_id']=$user_id;
$_SESSION['user_name']=$user_name;
}
?>

<?php
$data['page_title'] = "Login";
$this->load->view('front/includes/header',$data);

?>

<main>
  <div class="container">
    <div class="row">
      <div class="col-12 col-sm-12 col-md-12">
        <div class="text-center mt-5 pt-5">
          <div class="row pb-4">
            <div class="col-12 col-sm-12 col-md-4">&nbsp;</div>

            <div class="col-12 col-sm-12 col-md-4">
              <img class="pt-3 pb-3" src="<?php echo base_url();?>/website-assets/images/logo.png">

              <h4 class="h4 pt-4">MEMBER LOGIN</h4>

            <form class="login_form"  id="login_form" method="post">  

            <div id="error_message" style="display:none;">
            <div class="alert alert-danger">
            <strong>Invalid username or password!</strong>.
            </div>
            </div>

            <div id="error_message_disable" style="display:none;">
            <div class="alert alert-danger">
            <strong>Account is not active please contact Piquic Admin</strong>.
            </div>
            </div>

                <div class="form-group">
                  <!-- <input type="text" class="form-control" id="username" placeholder="Username"> -->

                  <input type="email" class="form-control" name="email" id="email" placeholder="Enter your email" <?php  if($activate_user_id!='') { ?>  value="<?php echo $activate_user_email;?>"  <?php } else if($cookie_remember_me=='1') { ?> value="<?php echo $cookie_user_name;?>" <?php }  ?>>
                  <?php echo form_error('email', '<div class="error" style="color:red;">', '</div>'); ?>
                </div>

                <div class="form-group">
                 <!--  <input type="password" class="form-control" id="password" placeholder="Password"> -->

                 <input type="password" class="form-control" name="password" id="password" placeholder="Password" <?php if($activate_user_id!='') { ?>  value=""  <?php } else if($cookie_remember_me=='1') { ?> value="<?php echo $cookie_user_password;?>" <?php } ?>>
                  <?php echo form_error('password', '<div class="error" style="color:red;">', '</div>'); ?>
                </div>

                <div class="form-group" style="text-align: left;">
                <label >
                 <!-- <input type="hidden" name="referer" id="referer" 
                  value="<?php //if ($this->agent->is_referral()) {  if ($this->agent->referrer()!="http://collabtest.piquic.com/logout") { echo $this->agent->referrer(); } } ?>" /> -->

                  <input type="hidden" name="referer" id="referer" 
                  value="<?php if ($this->session->userdata('previous_url')!='') {  if ($this->session->userdata('previous_url')!="http://collabtest.piquic.com/logout") { echo $this->session->userdata('previous_url'); } } ?>" />

              <input type="checkbox" name="remember_me" id="remember_me" value="1" 
              <?php if($activate_user_id!='') { ?>   <?php } else if($cookie_remember_me=='1') { ?> checked <?php } ?> >
              Remember me 
              </label>
                </div>

                <!-- <button type="submit" class="btn btn-wc btn-block">Submit</button> -->
                 <button id="signin" type="submit" class="btn btn-wc btn-block" name="signin">Login </button>
              </form>
            </div>

            <div class="col-12 col-sm-12 col-md-4">&nbsp;</div>
          </div>

          <div class="row pb-4">
            <div class="col-12 col-sm-12 col-md-3">&nbsp;</div>

            <div class="col-12 col-sm-12 col-md-6">
              
              <a href="#" class="btn btn-light btn-block border" data-dismiss="modal" data-toggle="modal" data-target="#forgetModal">
              Forget Password</a>
            </div>

            <div class="col-12 col-sm-12 col-md-3">&nbsp;</div>
          </div>
        </div>
      </div>
    </div>
  </div>
</main>



<!-- FORGET PASSWORD Modal -->
<div class="modal fade" id="forgetModal" tabindex="-1" role="dialog" aria-labelledby="forgetModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <!-- <img class="img-fluid" src="<?php echo PIQUIC_URL;?>images/logo-piquic-sm.png"> -->
        <button type="button" class="close" data-dismiss="modal" aria-label="Close" id="forgotclose">
          <i class="far fa-times-circle"></i>
        </button>
      </div>
      <div id="modal-tab" class="modal-header">
        <h4 class="modal-title text-black" id="forgetModalLabel">FORGET PASSWORD</h4>
      </div>
      <div class="modal-body">
        <div id="success_message1" style="display:none;">
          <div class="card-body">
            <div class=" text-center text-piquic" role="alert" style="display: table; height: 50vh; width: 100%; ">
              <div style="display: table-cell; vertical-align: middle;">
                <i class="fas fa-check-circle icon-lg"></i><br>
                <span class="h4" >Please check your email inbox/spam folder for reset password email!!!</span>
              </div>
            </div>
          </div>
          
        </div>
        <div id="error_message1" style="display:none;">
          <div class="alert alert-danger">
            <strong>Mail id is not correct!!!</strong>.
          </div>
        </div>
        <div class="tab-content" id="pills-tabContent">
          <div class="tab-pane fade show active" id="pills-login" role="tabpanel" aria-labelledby="pills-login-tab">
            <form class="form form-horizontal has-validation-callback" id="forgetreset"  method="post">   
              <table class="table table-borderless pr-4">
                <tr>
                  <td class="pt-3 text-right"><i class="fas fa-envelope"></i></td>
                  <td><input type="email" class="form-control mb-2" name="femail" id="femail"  placeholder="Enter your registered email" required>
                    <?php echo form_error('femail', '<div class="error" style="color:red;">', '</div>'); ?>
                  </td>
                </tr>
                <tr>
                  <td colspan="2" class="text-center">
                    <button name="forgotSubmit" type="submit" class="btn btn-wc btn-block">Submit </button>

                   
                  </td>
                </tr>
              </table>
            </form>
          </div>
        </div>
      </div>
      <span id="fsuccess" class="error" style="display:none;"></span>
    </div>
  </div>
</div>

<?php
    $this->load->view('front/includes/footer');

    ?>

 <!--      <script type="text/javascript" src="<?php echo site_url(); ?>website-assets/js/popper.min.js"></script>
    <script type="text/javascript" src="<?php echo site_url(); ?>website-assets/js/bootstrap.min.js"></script>

    <script src="<?php echo base_url(); ?>assets/js/jquery.validate.min.js"></script> 
    <script type="text/javascript" src="<?php echo site_url(); ?>website-assets/js/custom.js"></script> -->

    <script type="text/javascript">

  // Rajedra Prasad - 20-01-20 ---------
  /*$( document ).ready(function() {
    <?php if($user_id!='' && $type1=='') { ?>

    $("#type1").val("book");
    var type1=$("#type1").val();

    $.ajax({
      method:'post',
      url: "<?php //echo base_url();?>front/home/setTypeSession", 
      data:"&type1="+type1,
      success: function(result){
        if($.trim(result)=='success') {
          //window.location.href = "<?php //echo base_url();?>home";
        }
      }
    });

    <?php } ?>
  });*/
  // -----------------------------------

  $(function () {
    <?php
    if($user_id=='')
    {
      ?>
      $('#myModal').modal({
        backdrop: 'static',
        keyboard: false
      });

      <?php
    }
    ?>










    $("#login_form").validate({
      rules: {
        email:{
          required:true,
          email: true,
        },
        password: "required"
      },
      submitHandler: function (form) {
     /// alert("form")
     // form.submit();
     var email=$('#email').val();
     var password=$('#password').val();
     var referer=$('#referer').val();
    
     //var remember_me=$('#remember_me').val();

  if($("#remember_me").is(":checked")){
  //alert("Checkbox is checked.");
  var remember_me='1';
  }
  else if($("#remember_me").is(":not(:checked)")){
  //alert("Checkbox is unchecked.");
  var remember_me='0';
  }


     $.ajax({
      method:'post',
      url: "<?php echo base_url();?>front/login/check_login", 
      data:"&email="+email+"&password="+password+"&remember_me="+remember_me,
      success: function(result){

            //alert($.trim(result));
            if($.trim(result)=='success')
            {
              if(referer=='')
              {
              window.location.href = "<?php echo base_url();?>dashboard";
              }
              else
              {
                window.location.href =referer;
              }
            }
            else if($.trim(result)=='Inactive')
            {

              $("#error_message").hide();
              $("#error_message_disable").show();
              setTimeout(function(){
                $("#error_message_disable").hide();
              }, 15000);

              
            }
            else{
              $("#error_message").show();
              setTimeout(function(){
                $("#error_message").hide();
              }, 15000);
            }
        }
     });
  }
});
  });



  $(function () {
    $("#register_form").validate({
      rules: {
        uname:{
          required:true,
        },
        cname:{
          required:true,         
        },
        uemail:{
          required:true,
          email: true,
          <?php
          if($user_id=="")
            { ?>
              remote: {
                url: "<?php echo base_url();?>front/home/checkexistemail",
                type: "post"
              },
            <?php }  ?>   
          },
          <?php
          if($user_name=='')
            { ?>
              upassword:{
                required:true,
                minlength:5,
              },
              ure_password: {
                required:true,
                equalTo: "#upassword" ,
                minlength:5,
              },
            <?php  }  ?>
            umobile: {
              number: true,
              required: true,
              minlength:10,
              maxlength:10,
              <?php
              if($user_name=="")
                { ?>
                  remote: {
                    url: "<?php echo base_url();?>front/home/checkexistmobile",
                    type: "post"
                  },
                <?php   }  ?>   
              },
            },
            messages:
            {
              uemail:
              {
                 remote:"Mail already exists."
              },
              umobile:
              {
                 remote:"Mobile Number already exists."
              }
            },
            submitHandler: function (form) {
              var user_id='';
              var uname=$('#uname').val();
              var cname=$('#cname').val();
              var uemail=$('#uemail').val();
              var umobile=$('#umobile').val();
              var upassword=$('#upassword').val();
              $("#loader").show();
              $.ajax({
                method:'post',
                url: "<?php echo base_url();?>front/home/create_user", 
                data:"&user_id="+user_id+"&uname="+uname+"&cname="+cname+"&uemail="+uemail+"&umobile="+umobile+"&upassword="+upassword,
                success: function(result){
                  if(result)
                  {
                    $("#success_message").show();
           //                 setTimeout(function(){
                //        $("#success_message").hide();
                    // }, 5000);
                    $('#uname').val("");
                    $('#cname').val("");
                    $('#uemail').val("");
                    $('#umobile').val("");
                    $('#upassword').val("");
                    $('#ure_password').val("");
                    $('#register_form').hide();
                    // $('#pills-login').addClass('active show');
                    // $('#pills-login-tab').addClass('active');
                    // $('#pills-register').removeClass('active show');
                    // $('#pills-register-tab').removeClass('active');
                    $("#loader").hide();
                  }
                }
              });
            }
          });
  });
  $(function () {
    $("#forgetreset").validate({
      rules: {
        femail:{
          required:true,
          email: true,
          <?php
          if($user_id=="")
            { ?>
              remote: {
                url: "<?php echo base_url();?>front/login/checkexistemail",
                type: "post"
              },
            <?php }  ?>   
          },
        },
        messages:
        {
          femail:
          {
             remote:"Mail Not exists."
          },
          
        },
        submitHandler: function (form) {
      //alert('forgotpassword');
      $("#loader").show();
      var femail=$('#femail').val();

      $.ajax({
        method:'post',
        url: "<?php echo base_url();?>front/login/forgotpassword", 
        data:"&femail="+femail,
        success: function(result){
          $("#loader").hide();
          if(result)
          {
            $("#success_message1").show();
            $('#forgetreset').hide();
            // setTimeout(function(){
            //  $("#success_message1").hide();
            // }, 5000);
          } else {
            $("#error_message1").show();
            setTimeout(function(){
              $("#error_message1").hide();
            }, 15000);
          }
        }
      });
   }

});

  });




/* $(window).load(function(){        
   $('#myModal').modal('show');
}); */

</script>


