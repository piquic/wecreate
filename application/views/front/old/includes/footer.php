<!-- Optional JavaScript -->
    <script type="text/javascript" src="<?php echo site_url(); ?>website-assets/js/popper.min.js"></script>
    <script type="text/javascript" src="<?php echo site_url(); ?>website-assets/js/bootstrap.min.js"></script>
    <script type="text/javascript" src="<?php echo site_url(); ?>website-assets/js/jquery.validate.min.js"></script> 
    <script type="text/javascript" src="<?php echo site_url(); ?>website-assets/js/custom.js"></script>

    <script type="text/javascript">
    	$(document).ready(function(){
    		$(window).keydown(function(event)
    		{
    			if((event.keyCode == 107 && event.ctrlKey == true) || (event.keyCode == 109 && event.ctrlKey == true)||( event.ctrlKey == true && (event.which == '61' || event.which == '107' || event.which == '173' || event.which == '109'  || event.which == '187'  || event.which == '189'  ))){
    				event.preventDefault();
			        //Your logic here
			        console.log("Zoom Feature Disabled. Ctrl+key Pressed.");
			    }
			});

		    //Prevent zoom while using ctrl and mouse wheel
		    $(window).bind('mousewheel DOMMouseScroll', function(e)
		    {
			    //ctrl key is pressed
			    if(e.ctrlKey == true){
			    	e.preventDefault();
			        //Your logic here
			        console.log("Zoom Feature Disabled. Ctrl+Mousewheel Pressed.");        
			    }
			});
    	});
    </script>
</body>
</html>