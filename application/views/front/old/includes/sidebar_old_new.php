<?php
$class_name=$this->router->fetch_class();
 $method_name=$this->router->fetch_method();
?>

<aside class="p-3">
				<div class="row">
					<div class="col-12 col-sm-12 col-md-12">
						


							

						<?php
						 $acount_permission=$this->Dashboard_model->checkPermission($user_type_id,1,'access');
						if($acount_permission=='1')
						{
							?>

						<div class="py-3">


							<select class="custom-select" id="account_id" name="account_id" onchange="filterStatus('');">
								<option value="">Select Account</option>
								<?php
								$users_type_details=$this->db->query("select wc_users.*,wc_users_type.user_type_name from wc_users inner join wc_users_type on wc_users.user_type_id=wc_users_type.user_type_id where wc_users.deleted='0' and wc_users.status='Active' and wc_users.user_type_id not in (1,2) ")->result_array();
								if(!empty($users_type_details))
								{
								foreach($users_type_details as $key => $userstypedetails)
								{
								$user_id=$userstypedetails['user_id'];
								$user_type_id=$userstypedetails['user_type_id'];
								$user_type_name=$userstypedetails['user_type_name'];
								$user_name=$userstypedetails['user_name'];
								?>
								<option value="<?php echo $user_id;?>"   <?php if($account_id_sel==$user_id) { echo 'selected'; } ?>    ><?php echo ucfirst($user_name);?> - <?php echo $user_type_name;?></option>
								<?php

								}
								}
								?>
								
								
							</select>
						</div>

						<?php

						}
						else
						{
							?>
							<input type="hidden" name="account_id" id="account_id" value="">
							<?php
						}
						?>

						<?php

						 $account_permission=$this->Dashboard_model->checkPermission($user_type_id,1,'access');
						 $brand_permission=$this->Dashboard_model->checkPermission($user_type_id,2,'access');
						 $admin_permission=$this->Dashboard_model->checkPermission($user_type_id,3,'access');
						
						if($account_permission=='1' || $brand_permission=='1' || $admin_permission=='1')
						{
							?>

						<div class="py-3">
							<div id="divBrand">
							<select class="custom-select" name="brand_id" id="brand_id" onchange="sortbrief()">
								<option value="">Select Brand</option>
								<?php

								$sql="select * from wc_brands where status='Active' ";
								if($brand_id_str!='')
								{
									$sql.=" and  brand_id in (".$brand_id_str.") ";
								}
								


								$brand_type_details=$this->db->query($sql)->result_array();
								if(!empty($brand_type_details))
								{
								foreach($brand_type_details as $key => $brandstypedetails)
								{
								$brand_id=$brandstypedetails['brand_id'];
								$brand_name=$brandstypedetails['brand_name'];
								
								?>
								<option value="<?php echo $brand_id;?>"  <?php if($brand_id_sel==$brand_id) { echo 'selected'; } ?>   ><?php echo ucfirst($brand_name);?></option>
								<?php

								}
								}
								?>
								<!--  selected="selected" -->
							</select>
							</div>
						</div>
						<?php

						}
						else
						{
							?>
							<input type="hidden" name="brand_id" id="brand_id" value="">
							<?php
						}
						?>

						<?php
						if($user_type_id=='4')
						{
							?>


						<div class="p-3">
							<a class="text-wc h5" href="<?php echo base_url() ?>uploadbrief"><i class="fas fa-cloud-upload-alt"></i>&nbsp;UPLOAD BRIEF</a>
						</div>

						<?php

						}
						?>



						<div class="py-3">
							<p class="text-wc lead" onclick="goDashboard();">DASHBOARD</p>
						</div>









						<?php
						if($class_name=='dashboard')
						{


							if(!empty($dashboradinfo))
{

//print_r($labelimagesinfo);
foreach($dashboradinfo as $row){ 
$brief_id=$row->brief_id;
$brief_title=$row->brief_title;
$brief_doc=$row->brief_doc;
$brief_due_date=$row->brief_due_date;
$brief_due_date= date('d F Y H:i', strtotime($brief_due_date));
$status=$row->status;

	if($status==0)
	{
        $brief_review[]=$brief_id;
	}

	if($status==1)
	{
        $work_progress[]=$brief_id;
	}

	if($status==2)
	{
        $work_completed[]=$brief_id;
	}

	if($status==3)
	{
        $revision_work[]=$brief_id;
	}
	if($status==4)
	{
        $brief_rejected[]=$brief_id;
	}
	if($status==5)
	{
        $feedback_pending[]=$brief_id;
	}
	if($status==6)
	{
        $proofing_pending[]=$brief_id;
	}
	



}
}


						
						?>

						<div class="py-3">
							<p class="text-wc lead">Show by Stages</p>

							<div class="pl-3">
								
								<div class="custom-control custom-checkbox">
									<input type="checkbox" class="custom-control-input" id="brfrev" <?php if($brief_status=='0') { ?> checked <?php } ?> >
									<label class="custom-control-label roundCheck lead" for="brfrev" onclick="filterStatus('0');">Brief in Review <span class="badge badge-wc"><?php echo count($brief_review);?></span></label>
								</div>

								<div class="custom-control custom-checkbox">
									<input type="checkbox" class="custom-control-input" id="brfrej" <?php if($brief_status=='4') { ?> checked <?php } ?> >
									<label class="custom-control-label roundCheck lead" for="brfrej"  onclick="filterStatus('4');">Brief Rejected <span class="badge badge-wc"><?php echo count($brief_rejected);?></span></label>
								</div>

								<div class="custom-control custom-checkbox">
									<input type="checkbox" class="custom-control-input" id="wrkprog" <?php if($brief_status=='1') { ?> checked <?php } ?> >
									<label class="custom-control-label roundCheck lead" for="wrkprog"  onclick="filterStatus('1');">Work in Progress <span class="badge badge-wc"><?php echo count($work_progress);?></span></label>
								</div>

								<div class="custom-control custom-checkbox">
									<input type="checkbox" class="custom-control-input" id="prfpend" <?php if($brief_status=='6') { ?> checked <?php } ?> >
									<label class="custom-control-label roundCheck lead" for="prfpend"  onclick="filterStatus('6');">Proofing Pending <span class="badge badge-wc"><?php echo count($proofing_pending);?></span></label>
								</div>

								<div class="custom-control custom-checkbox">
									<input type="checkbox" class="custom-control-input" id="revwrk" <?php if($brief_status=='3') { ?> checked <?php } ?> >
									<label class="custom-control-label roundCheck lead" for="revwrk"  onclick="filterStatus('3');">Revision Work <span class="badge badge-wc"><?php echo count($revision_work);?></span></label>
								</div>

								<div class="custom-control custom-checkbox">
									<input type="checkbox" class="custom-control-input" id="wrkcomp" <?php if($brief_status=='2') { ?> checked <?php } ?> >
									<label class="custom-control-label roundCheck lead" for="wrkcomp"  onclick="filterStatus('2');">Work Complete <span class="badge badge-wc"><?php echo count($work_completed);?></span></label>
								</div>								

								<div class="custom-control custom-checkbox">
									<input type="checkbox" class="custom-control-input" id="fdbkpend" <?php if($brief_status=='5') { ?> checked <?php } ?> >
									<label class="custom-control-label roundCheck lead" for="fdbkpend"  onclick="filterStatus('5');">Feedback Pending <span class="badge badge-wc"><?php echo count($feedback_pending);?></span></label>
								</div>

							</div>
						</div>

						<?php
						

						}
						?>
					</div>
				</div>
			</aside>

			<script type="text/javascript">
				function goDashboard()
				{
					//alert("aaaaa");
					window.location.href="<?php echo base_url();?>dashboard";
				}
			</script>