
<?php
 $class_name=$this->router->fetch_class();
?>
<aside class="p-3">
				<div class="row">
					<div class="col-12 col-sm-12 col-md-12">
					<!-- <?php if ($type1==4) { ?>
						<div class="p-3">
							<a class="text-wc h5" href="#"><i class="fas fa-cloud-upload-alt"></i>&nbsp;UPLOAD BRIEF</a>
						</div>
					<?php } elseif($type1==1 || $type1==2 || $type1==3) { ?>



						<div class="py-3">
							<select class="custom-select" name="brand_id" id="brand_id" onchange="sortbrief()">
								<option selected value="">Select Brand</option>
								<?php 	if(!empty($brandsinfo))
											{
												foreach($brandsinfo as $keybill => $brands)
												{ 
												$brand_id=$brands['brand_id'];
													$brand_name=$brands['brand_name'];
												?>
								<option value="<?php echo $brand_id; ?>"><?php echo $brand_name; ?></option>
											<?php } }?>
							</select>
						</div>						
					<?php } else { ?>
					<div class="py-3">
					
					</div>
					
					<?php } ?> -->


						<?php
						 $acount_permission=$this->Dashboard_model->checkPermission($user_type_id,1,'access');
						if($acount_permission=='1')
						{
							?>

						<div class="py-3">


							<select class="custom-select" id="account_id" name="account_id" onchange="selectAccount('');">
								<option value="">Select Account</option>
								<?php
								$users_type_details=$this->db->query("select wc_users.*,wc_users_type.user_type_name from wc_users inner join wc_users_type on wc_users.user_type_id=wc_users_type.user_type_id where wc_users.deleted='0' and wc_users.status='Active' and wc_users.user_type_id not in (1,2) ")->result_array();
								if(!empty($users_type_details))
								{
								foreach($users_type_details as $key => $userstypedetails)
								{
								$user_id=$userstypedetails['user_id'];
								$user_type_id=$userstypedetails['user_type_id'];
								$user_type_name=$userstypedetails['user_type_name'];
								$user_name=$userstypedetails['user_name'];
								?>
								<option value="<?php echo $user_id;?>"   <?php if($account_id_sel==$user_id) { echo 'selected'; } ?>    ><?php echo ucfirst($user_name);?> - <?php echo $user_type_name;?></option>
								<?php

								}
								}
								?>
								
								
							</select>
						</div>

						<?php

						}
						else
						{
							?>
							<input type="hidden" name="account_id" id="account_id" value="">
							<?php
						}
						?>

						<?php

						 $account_permission=$this->Dashboard_model->checkPermission($user_type_id,1,'access');
						 $brand_permission=$this->Dashboard_model->checkPermission($user_type_id,2,'access');
						 $admin_permission=$this->Dashboard_model->checkPermission($user_type_id,3,'access');
						
						if($account_permission=='1' || $brand_permission=='1' || $admin_permission=='1')
						{
							?>

						<div class="py-3">
							<div id="divBrand">
							<select class="custom-select" name="brand_id" id="brand_id" onchange="sortbrief()">
								<option value="">Select Brand</option>
								<?php

								$sql="select * from wc_brands where status='Active' ";
								if($brand_id_str!='')
								{
									$sql.=" and  brand_id in (".$brand_id_str.") ";
								}
								


								$brand_type_details=$this->db->query($sql)->result_array();
								if(!empty($brand_type_details))
								{
								foreach($brand_type_details as $key => $brandstypedetails)
								{
								$brand_id=$brandstypedetails['brand_id'];
								$brand_name=$brandstypedetails['brand_name'];
								
								?>
								<option value="<?php echo $brand_id;?>"  <?php if($brand_id_sel==$brand_id) { echo 'selected'; } ?>   ><?php echo ucfirst($brand_name);?></option>
								<?php

								}
								}
								?>
								<!--  selected="selected" -->
							</select>
							</div>
						</div>
						<?php

						}
						else
						{
							?>
							<input type="hidden" name="brand_id" id="brand_id" value="">
							<?php
						}
						?>

						<?php
						if($user_type_id=='4')
						{
							?>


						<div class="p-3">
							<a class="text-wc h5" href="<?php echo base_url() ?>uploadbrief"><i class="fas fa-cloud-upload-alt"></i>&nbsp;UPLOAD BRIEF</a>
						</div>

						<?php

						}
						?>


						<div class="p-3">
							<p class="text-wc lead" onclick="goDashboard();">DASHBOARD</p>
						</div>

						<?php
						if($class_name=='dashboard')
						{
						?>


						<div class="p-3">
							<p class="text-wc lead">Show by Stages</p>

							<div class="pl-3">
								<div class="custom-control custom-checkbox">
									<input type="checkbox" class="custom-control-input" name="breif_status[]" id="brfrev"  value="0" onchange="sortbrief()">
									<label class="custom-control-label roundCheck lead" for="brfrev">Brief in Review <span class="badge badge-wc" id="bir"><?php echo $briefinreview; ?></span></label>
								</div>

								<div class="custom-control custom-checkbox">
									<input type="checkbox" class="custom-control-input" name="breif_status[]" id="brfrej" value="4" onchange="sortbrief()">
									<label class="custom-control-label roundCheck lead" for="brfrej">Brief Rejected <span class="badge badge-wc" id="br"><?php echo $briefrejected; ?></span></label>
								</div>

								<div class="custom-control custom-checkbox">
									<input type="checkbox" class="custom-control-input" name="breif_status[]" id="wrkprog" value="1" onchange="sortbrief()">
									<label class="custom-control-label roundCheck lead" for="wrkprog">Work in Progress <span class="badge badge-wc" id="wip"><?php echo $workinprogress; ?></span></label>
								</div>

								<div class="custom-control custom-checkbox">
									<input type="checkbox" class="custom-control-input" name="breif_status[]" id="prfpend" value="6" onchange="sortbrief()">
									<label class="custom-control-label roundCheck lead" for="prfpend">Proofing Pending <span class="badge badge-wc" id="pp"><?php echo $proffing_pending; ?></span></label>
								</div>

								<div class="custom-control custom-checkbox">
									<input type="checkbox" class="custom-control-input" name="breif_status[]" id="revwrk" value="3" onchange="sortbrief()">
									<label class="custom-control-label roundCheck lead" for="revwrk">Revision Work <span class="badge badge-wc" id="rw"><?php echo $revision_work; ?></span></label>
								</div>

								<div class="custom-control custom-checkbox">
									<input type="checkbox" class="custom-control-input" name="breif_status[]" id="wrkcomp" value="2" onchange="sortbrief()">
									<label class="custom-control-label roundCheck lead" for="wrkcomp">Work Complete <span class="badge badge-wc" id="wc"><?php echo $work_complete; ?></span></label>
								</div>								

								<div class="custom-control custom-checkbox">
									<input type="checkbox" class="custom-control-input" name="breif_status[]" id="fdbkpend" value="5" onchange="sortbrief()">
									<label class="custom-control-label roundCheck lead" for="fdbkpend">Feedback Pending <span class="badge badge-wc" id="fp"><?php echo $feedback_pending; ?></span></label>
								</div>
							</div>
						</div>


<?php
						}
						?>


					</div>
				</div>
			</aside>
		<script type="text/javascript">
				function goDashboard()
				{
					//alert("aaaaa");
					window.location.href="<?php echo base_url();?>dashboard";
				}
			</script>