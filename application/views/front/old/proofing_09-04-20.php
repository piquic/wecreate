<?php
session_start();
error_reporting(0);
$data['page_title'] = "Proofing";

// $this->load->view('front/includes/header',$data);


// // $this->load->view('front/includes/menu');
// $this->load->view('front/includes/nav');


if ($this->session->userdata('front_logged_in')) {
	$session_data = $this->session->userdata('front_logged_in');
	$user_id = $session_data['user_id'];
	$user_name = $session_data['user_name'];
	$_SESSION['user_id']=$user_id;
	$_SESSION['user_name']=$user_name;
	$type1=$this->session->userdata('type1');

}
else
{
	$user_id = '';
	$user_name = ''; 
	$_SESSION['user_id']=$user_id;
	$_SESSION['user_name']=$user_name;
}
$this->load->view('front/includes/header',$data);
$this->load->view('front/includes/topnav');
?>
<!-----------poulami start-------------->
	<!-- <link rel="stylesheet" href="<?php echo base_url();?>/website-assets/folderTree/css/filetree.css" type="text/css" >
	-->
	<!-----------poulami end-------------->
<style type="text/css">
	.upload-btn-wrapper {
  position: relative;
  overflow: hidden;
  display: inline-block;
}
input[type=file] {
  font-size: 100px;
  position: absolute;
  left: 0;
  top: 0;
  opacity: 0;
}
</style>
<div class="container-fluid">
	<div class="row">

		<!-- Main Body -->
		<div class="col-12 col-sm-12 col-md-12">
			<main class="p-3">

				<div class="row">
					<div class="col-12 col-sm-12 col-md-12">
						<div class="p-3">
							<a href="<?php echo base_url() ?>dashboard"><p class="text-wc lead"><i class="fas fa-arrow-alt-circle-left"></i>&nbsp;Back to Dashboard</p></a>
						</div>
					</div>
				</div>

				<div class="row">
					<div class="col-12 col-sm-12 col-md-12">
						<div class="p-3 text-wc">
							<h3 class="h3 text-uppercase">
								<?php
								$wc_brief_sql = "SELECT * FROM `wc_brief` where brief_id='$brief_id'";
								$wc_brief_query = $this->db->query($wc_brief_sql);

								$wc_brief_result=$wc_brief_query->result_array();
								//print_r($wc_brief_result);
								// $wc_brief_qrydata = mysqli_fetch_array($wc_brief_query);
								$brief_title=$wc_brief_result[0]['brief_title'];
								echo $brief_title." (ID: ".$brief_id.")";
								?>
							</h3>
						</div>

						<div class="p-3 text-wc d-flex justify-content-start">
							<div class="p-1 select_div">
								
								
								
							</div>

							<div class="p-1">
								<select class="custom-select download">
									<option>Select Download</option>
									<option value="download_single">Download Single</option>
									<option value="download_all">Download All</option>
								</select>
							</div>
						</div>
					</div>
				</div>

				<div class="row" id='image_click'>
					
				</div>
				<div class="row">
					<div class="" id="filter_div">
						<div class="d-flex justify-content-around">
							<div style="width: 20rem;">
								<select class="custom-select" id="filter_status">
									<option value="all">Select All</option>
									<option value="0">New Upload Image</option>
									<option value="1">Approved</option>
									<option value="2">Review</option>
									<option value="3">Under Revision</option>
									
									
								</select>
							</div>
						</div>

						<div class="p-4">
							<div class="row " id="filter_result">
							</div>
						</div>
					</div>
				</div>

			</main>
		</div>

	</div>
</div>

<!-- <div class="w-100 p-3 border-top bg-light">
	<h4>Notes:</h4>
	<textarea class="form-control bg-light" rows="10"></textarea>
</div>
 -->

<?php
$this->load->view('front/includes/footer');
?>


<script src='<?php echo base_url();?>/website-assets/js/dragdropfolder.js'></script>

<script type="text/javascript">


$(document).ready(function(){

<?php

//$main_folder_sql = "SELECT * FROM `wc_image_upload` where brief_id='$brief_id'";
$main_folder_sql = "SELECT * FROM wc_image_upload WHERE brief_id='$brief_id' and image_id IN (SELECT MAX(image_id) FROM wc_image_upload  GROUP BY parent_img )order by parent_img asc";
$main_folder_query = $this->db->query($main_folder_sql);
// print_r($main_folder_query);
$main_folder_count=$main_folder_query->num_rows();
if($main_folder_count>="1")
{

	$main_folder_result=$main_folder_query->result_array();
	$img_status=$main_folder_result[0]['img_status'];
	$image_id=$main_folder_result[0]['image_id'];
	$parent_img=$main_folder_result[0]['parent_img'];
	$image_path=$main_folder_result[0]['image_path'];
	$select_value="";
?>
	useimg('<?php echo $brief_id; ?>','<?php echo $image_path; ?>','<?php echo $image_id; ?>','<?php echo $img_status; ?>','<?php echo $select_value; ?>');
<?php
	if($img_status=="2" || $img_status=="3") {
?>
	review('<?php echo $image_id; ?>','<?php echo $brief_id; ?>','<?php echo $image_path; ?>','<?php echo $select_value; ?>');
<?php
	}
}
else{
	$result= "no image";
	?>

	$("#image_click").html("<span class='lead'><?php echo $result ?></span>");
	<?php
}
?>
	var filter_status="all"
	var brief_id ='<?php echo $brief_id; ?>';
	$.ajax({
		type: "POST", 
		url: "<?php echo base_url()?>front/proofing/filter_status", 
		data: "&filter_status="+filter_status+"&brief_id="+brief_id,
		success: function(response){
			$('#filter_result').empty();
			$('#filter_result').append(response);
			// var url="<?php echo base_url(); ?>/"+response;
			// window.location = url;
	  	}
	});
});
$( "#filter_status" ).change(function() {
	var filter_status=$( this ).val();
	var brief_id ='<?php echo $brief_id; ?>';
	$.ajax({
		type: "POST", 
		url: "<?php echo base_url()?>front/proofing/filter_status", 
		data: "&filter_status="+filter_status+"&brief_id="+brief_id,
		success: function(response){
			$('#filter_result').empty();
			$('#filter_result').append(response);
			// var url="<?php echo base_url(); ?>/"+response;
			// window.location = url;
	  	}
	});

});
$( ".download" ).change(function() {
	//alert( $( this ).val());
	var download_value=$( this ).val();
	//alert(download_value);
	if(download_value=="download_single"){
	var image_src=$('.img_download').attr('src');
	//alert(image_src);
	$("#loader").show();
	$.ajax({
			type: "POST", 
			url: "<?php echo base_url()?>front/proofing/download_image", 
			data: "download_value="+download_value+"&image_src="+image_src,
			success: function(response){
				$("#loader").hide();
				var url="<?php echo base_url(); ?>/"+response;
				window.location = url;
		  	}
		});

	}
	else if(download_value=="download_all"){
		var image_src = $('.img_download1').map(function(i) {
        return $(this).attr('src');
      	}).get();
		//alert(image_src);
		$("#loader").show();
		$.ajax({
			type: "POST", 
			url: "<?php echo base_url()?>front/proofing/download_image", 
			data: "download_value="+download_value+"&image_src="+image_src,
			success: function(response){
				$("#loader").hide();
				var url="<?php echo base_url(); ?>/"+response;
				window.location = url;
		  	}
		});
	}

});

// 
function updateApprove(image_id,brief_id)
{
	$.ajax({
		type: "POST", 
		url: "<?php echo base_url()?>front/proofing/update_status", 
		data: "&image_id="+image_id+"&brief_id="+brief_id+"&img_status=1",
		success: function(response){
			//alert(data);
			if(response=="1")
			{
				$("#update_approve").html('<button type="submit" class="btn btn-wc btn-block m-2 px-5">&emsp;&emsp;Approved&nbsp;<i class="fas fa-check"></i>&emsp;&emsp;</button>');
			}
	  	}
	});

}

function review(image_id,brief_id,image_path,select_value)
{
	
	$.ajax({
		type: "POST", 
		url: "<?php echo base_url()?>front/proofing/review", 
		data: "&image_id="+image_id+"&brief_id="+brief_id+"&image_path="+image_path,
		success: function(response){
			$('#image_click').empty();
			$('#image_click').append(response);
			
			if(select_value=="")
			{
				//alert(select_value);
				viewtag(image_id);
				annotation(image_id);
				$.ajax({
				type: "POST", 
				url: "<?php echo base_url()?>front/proofing/select_revision", 
				data: "&image_id="+image_id+"&brief_id="+brief_id,
				success: function(response){
					$('.select_div').empty();
					$('.select_div').append(response);
					$( ".img_filter" ).change(function() {
						//alert( $( this ).val());
						//alert(select_value);
						var img_filter_value=$( this ).val();
						//alert(img_filter_value);

						var select_value="change";
						console.log("if ------"+select_value);
						var img_status='4';
						var image_path="";
						useimg(brief_id,image_path,img_filter_value,img_status,select_value);
						if(select_value==""){
							viewtag(image_id);
						}else if(select_value=="change"){
							viewtag1(img_filter_value,select_value);	
						}

						//viewtag1(img_filter_value,select_value);	
						//window.location.href="<?php echo base_url()?>open-proofing/"+img_filter_value;
					});
				}
			});
		}

			
			
			// 	}
			// });
			

			$(".upload_revised_file").on("change", function(e) {	
				//alert("sdf");
				//var files = e.target.files,
				var brief_id = $("#brief_id"). val();
				var image_id = $("#image_id"). val();
				$("#loader").show();
				var files = e.target.files,
				filesLength = files.length;
				for (var i = 0; i < filesLength; i++) {

					var f = files[i];
					//var f = files;
					var fileReader = new FileReader();
					fileReader.onload = (function(e) {
						var file = e.target;
						var input = document.getElementById("files");
						file = input.files[0];



						if(file != undefined){
							if(file != undefined && (file.type=='image/png' || file.type=='image/jpg'   || file.type=='image/jpeg' || file.type=='video/webm' || file.type=='video/mp4' || file.type=='video/3gpp' || file.type=='video/x-sgi-movie' || file.type=='video/x-msvideo' || file.type=='video/mpeg' || file.type=='video/x-ms-wmv' )    ){
							formData= new FormData();

//alert("xx");
							var ext = $("#files").val().split('.',2).pop().toLowerCase();

							//alert(ext);
							//if ($.inArray(ext, ['gif','png','jpg','jpeg']) == 1)
							if (1 == 1)
							{
								formData.append("image", file);
								formData.append("brief_id", brief_id);
								formData.append("image_id", image_id);
								//alert("aaaa");
								
								var data = $.ajax({
									url: "<?php echo base_url();?>front/proofing/upload_files_breif",
									type: "POST",
									data: formData,
									processData: false,
									contentType: false,
									async: false
								}).responseText;
								if($.trim(data)=='sessionout') {
									$("#loader").hide();
									// alert("Session out. Please login first!.");
									$('#modal_msg').modal('show');
									$('#txtMsg').html('Session out!<br>Please do the login first.');
									window.location.href="<?php echo base_url();?>home";
									return false;
								} else if($.trim(data)=='notsupport') {
									$("#loader").hide();
									// alert("There is some issue with image format!.");
									$('#modal_msg').modal('show');
									$('#txtMsg').html('There is some issue with image format!.');
									window.location.href="<?php echo base_url();?>open-proofing/"+brief_id;
									return false;
								} else {
									var ctn=0;
									var lastCount=0;
									console.log("completed");

									var browname=myCheckBrowserFunction();
									console.log(browname);

									if(browname=='Chrome') {
										//alert('iam here');
										if(ctn==lastCount) {
											//alert('yes');
											$("#loader").hide();
										} else {
											//alert(ctn);
											ctn++;
										}
									} else {
										$("#loader").hide();
									}
				
								}
								//alert(data);
								if(data=="success")
								{
									$("#loader").hide();
									$.ajax({
										type: "POST", 
										url: "<?php echo base_url()?>front/proofing/annotation_status", 
										data: "&image_id="+image_id,
										success: function(response){
											if(response=="1")
											{
												 location.reload();
											}
									  	}
									});
								}
							}
						}
						}
						else{
						alert('Input something!');
						}
				});
				fileReader.readAsDataURL(f);
				}
			});
	  	}
	});

} 

function sendRevision(image_id,brief_id)
{
	$.ajax({
		type: "POST", 
		url: "<?php echo base_url()?>front/proofing/update_status", 
		data: "&image_id="+image_id+"&brief_id="+brief_id+"&img_status=3",
		success: function(response){
			if(response>="1")
			{
				$("#sendRevision").html('<button type="submit" class="btn btn-outline-wc btn-block px-5 mb-2">&emsp;&emsp;Revision'+response+' Sent&nbsp;<i class="fas fa-sync-alt"></i>&emsp;&emsp;</button>');
			}
	  	}
	});
 
}
function reviewCancel(image_id,brief_id)
{
	//alert("reviewCancel");
	$.ajax({
		type: "POST", 
		url: "<?php echo base_url()?>front/proofing/update_status", 
		data: "&image_id="+image_id+"&brief_id="+brief_id+"&img_status=0",
		success: function(response){
			if(response>="1")
			{
				var image_path="";
				var img_status="0";
				useimg(brief_id,image_path,image_id,img_status)
			}
	  	}
	});
 
}
function useimg(brief_id,image_path,image_id,img_status,select_value){
	

	//alert(image_id);
	if(img_status=='1' || img_status=='0')
	{
		$.ajax({
			type: "POST", 
			url: "<?php echo base_url()?>front/proofing/use_img", 
			data: "brief_id="+brief_id+"&image_path="+image_path+"&image_id="+image_id+"&img_status="+img_status,
			success: function(response){
				$('#image_click').empty();
				$('#image_click').append(response);

			}
		});
		$('#filter_div').removeClass("col-12 col-sm-12 col-md-9");
		$('#filter_div').addClass("col-12 col-sm-12 col-md-12");
	}
	else{
		review(image_id,brief_id,image_path,select_value);		
		$('#filter_div').removeClass("col-12 col-sm-12 col-md-12");
		$('#filter_div').addClass("col-12 col-sm-12 col-md-9");
	}
}

function viewtag( image_id )
{
  	$.post( "<?php echo base_url()?>front/proofing/annotation_list" ,  "image_id=" + image_id, function( data ) {
	     //console.log(data.lists);
	    $('#list-id').empty();
	    $('#list-id').html(data.lists);
	    // $('#tagbox'+image_id).empty();
	    $('#tagbox').html(data.boxes);
	    $('#notes').empty();
	    $('#notes').html(data.notes);
	    // if(data.send_revision=="0"){
	    // 	$("#sendRevision").addClass('d-none');
	    // }
	    // else if(data.send_revision=="1"){
	    // 	$("#sendRevision").removeClass('d-none');
	    // }
	}, "json");
} 
function viewtag1( image_id,select_value )
{
	console.log("viewtag1"+select_value);
  	$.post( "<?php echo base_url()?>front/proofing/annotation_list" ,  "image_id=" + image_id+"&select_value=" +select_value, function( data ) {
	    //console.log(data.send_revision);
	    $('#list-id').empty();
	    $('#list-id').html(data.lists);
	    $('#tagbox').empty();
	    // $('#tagbox'+image_id).empty();
	    $('#tagbox').html(data.boxes);
	    $('#notes').empty();
	    $('#notes').html(data.notes);
	    if(data.send_revision=="0"){
	    	$("#sendRevision").addClass('d-none');
	    }
	    else if(data.send_revision=="1"){
	    	$("#sendRevision").removeClass('d-none');
	    }
	}, "json");
} 
function btnsave(image_id,mouseX,mouseY)
{
	var descr = $('#tagname').val();
	// tagid=$('#tag_id').val();
	//alert(id);
	// alert(name);
	//alert(img_src);
	$.ajax({
		type: "POST", 
		url: "<?php echo base_url()?>front/proofing/annotation_crud", 
		data: "image_id=" + image_id +"&descr=" + descr + "&position_left=" + mouseX + "&position_top=" + mouseY + "&type=insert",
		cache: true, 
		success: function(data){
			// alert(data);
			viewtag(image_id);
			$('#tagit').fadeOut();
		}
	});
}

function edit_button(id){
	//  alert("work edit");
	$("#name"+id).removeClass("d-none");
	$("#span_name"+id).addClass("d-none");
	$("#editBtn"+id).addClass("d-none");
	$("#delete_"+id).addClass("d-none");
	$("#updateBtn"+id).removeClass("d-none");
	$("#cancleBtn"+id).removeClass("d-none");    
}
function cancle_button(id){
	$("#name"+id).addClass("d-none");
	$("#span_name"+id).removeClass("d-none");
	$("#editBtn"+id).removeClass("d-none");
	$("#delete_"+id).removeClass("d-none");
	$("#updateBtn"+id).addClass("d-none");
	$("#cancleBtn"+id).addClass("d-none");

}
function cancle_button1(id){
	$("#name"+id).addClass("d-none");
	$("#span_name"+id).removeClass("d-none");
	$("#editBtn"+id).removeClass("d-none");
	$("#delete_"+id).removeClass("d-none");
	$("#remove_"+id).addClass("d-none");
	$("#cancleBtn1"+id).addClass("d-none");
	$("")
}
function remove1(id)
{
	//alert(id);
	$("#remove_"+id).removeClass("d-none");
	$("#delete_"+id).addClass("d-none");
	$("#cancleBtn1"+id).removeClass("d-none");
	$("#editBtn"+id).addClass("d-none");

}
function remove(id,image_id)
{
  	$.ajax({
        type: "POST", 
        url: "<?php echo base_url()?>front/proofing/annotation_crud", 
        data: "id=" + id + "&type=remove",
        success: function(data) {
			var img = $('#imgtag').find( 'img' );
			var img_id = $(img).attr( 'id' );
			//get tags if present
			// var files_id=$('#files_id').val(); 
			viewtag(image_id);
    	}
  	});
	//}
}
function annotation(image_id){
	//alert("clicked");
	var counter = 0;
	var mouseX = 0;
	var mouseY = 0;
	$('img[id^="imgtag"]').click(function(e) {
		// make sure the image is click
		//  imgtag=this.id;
		var imgtag = $(this).parent();
		// var img_id=this.id;
		// var id=img_id.substring(6)
		mouseX = ( e.pageX - $(imgtag).offset().left ) - 10; // x and y axis
		mouseY = ( e.pageY - $(imgtag).offset().top ) - 10;
		// alert(mouseX);
		// alert(mouseY);
		$( '#tagit').remove(); // remove any tagit div first
		$(imgtag).append( '<div id="tagit" ><div class="box"></div><div class="name editor" ><textarea class="editor-text" type="text" name="txtname" id="tagname" placeholder="Add a Comment..." ></textarea><input class="editor-button" type="button" name="btnsave" value="Save" id="btnsave" onclick=btnsave('+image_id+','+mouseX+','+mouseY+') /><input class="editor-button" type="button" name="btncancel" value="Cancel" id="btncancel" /></div></div>' );

		$( '#tagit' ).css({ top:mouseY, left:mouseX });

		$('#tagname').focus();
	});

	// Cancel the tag box.
	$( document ).on( 'click', '#tagit #btncancel', function() {
		$('#tagit').fadeOut();
	});
	
	  // mouseover the list-id 
	$('div[id^="list-id"]').on( 'mouseover', 'p', function( ) {
	    id = $(this).attr("id");
	    //alert(id);
	    $('#view_' + id).css({ opacity: 1.0 });
	 }).on( 'mouseout', 'p', function() {
	    $('#view_' + id).css({ opacity: 0.0 });
	 });
}


function update_button(image_id,id,position_left,position_top)
{
	// alert("Update");
	var descr = $("#name"+id).val();  
	$.ajax({
		type: "POST", 
		url: "savetag.php", 
		url: "<?php echo base_url()?>front/proofing/annotation_crud", 
		data: "id="+id+"&image_id=" + image_id +"&descr=" + descr + "&position_left=" + position_left + "&position_top=" + position_top + "&type=update",
		cache: true, 
		success: function(data){
			//alert(data);
			viewtag( image_id );
			$('#tagit').fadeOut();   
			$("#span_name"+id).removeClass("d-none");
			$("#editBtn"+id).removeClass("d-none");
			$("#delete_"+id).removeClass("d-none");
			$("#updateBtn"+id).addClass("d-none");
			$("#cancleBtn"+id).addClass("d-none"); 
			$("#name"+id).addClass("d-none");

		}
	});
}



</script>