<?php
session_start();
error_reporting(0);
$data['page_title'] = "upload image";




if ($this->session->userdata('front_logged_in')) {
	$session_data = $this->session->userdata('front_logged_in');
	$user_id = $session_data['user_id'];
	$user_name = $session_data['user_name'];
	$_SESSION['user_id']=$user_id;
	$_SESSION['user_name']=$user_name;
	$type1=$this->session->userdata('type1');

}
else
{
	$user_id = '';
	$user_name = ''; 
	$_SESSION['user_id']=$user_id;
	$_SESSION['user_name']=$user_name;
}

$this->load->view('front/includes/header',$data);

$this->load->view('front/includes/topnav');
?>

<div class="container-fluid">
	<div class="row">

		<!-- Sidebar -->
		<div class="col-12 col-sm-12 col-md-3">
			<?php
			$this->load->view('front/includes/sidebar');
			?>
		</div>

		<!-- Main Body -->
		<div class="col-12 col-sm-12 col-md-9">
			<main class="border-left p-3">

				<div class="row">
					<div class="col-12 col-sm-12 col-md-12">
						<div class="p-3">
							<h2 class="display-4"><?php echo $pagecontents[0]['page_title'] ?></h2>

							<?php echo $pagecontents[0]['content'] ?>
						</div>
					</div>
				</div>

				<div class="row">
					<div class="col-12 col-sm-12 col-md-12">
						<div class="p-3">
							<div class="border px-3 mt-3 text-justify">
								<span class="b-text bg-white p-2 font-weight-bold">BRIEF TITLE</span><br>
								<input type="text" name="brief_title" id="brief_title" class="form-control mb-3 border-0">
							</div>
						</div>
					</div>
				</div>

				<div class="row">
					<div class="col-12 col-sm-12 col-md-6">
						<div class="mb-3 p-3">
							<div class="bg-light" style="min-height: 22rem;">
								<form action="" method="post" enctype="multipart/form-data" id="docForm" name="docForm">
									<div class="custom-file">
										<input type="file" class="custom-file-input customfileinput" accept=".doc,.docx" id="upld_doc" name="upld_doc" onchange="uploadFile()">
										<label class="custom-file-label customfilelabel text-center" for="upld_doc"><i class="fas fa-upload fa-3x"></i><br><br>Drop the Brief file to upload or <i><u>browse</u></i>.</label>
									</div>
								</form>


								<!-- <form action="" method="post" enctype="multipart/form-data" id="zipForm" name="zipForm">
										<div class="custom-file">
											<input type="file" class="custom-file-input" accept="application/document" id="upld_doc" name="upld_doc" onchange="uploadFile()">
											<label class="custom-file-label text-center" for="upld_doc"><i class="fas fa-upload fa-3x"></i><br><br>Drop the ZIP file to upload or <i><u>browse</u></i>.</label>
										</div>
									</form> -->
							</div>
						</div>
					</div>

					<div class="col-12 col-sm-12 col-md-6">
						<div class="w-100 mb-3 p-3">							
							<!-- <div class="row" id="up_ld_doc" >
								<div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
									<div class="bg-light d-flex justify-content-between pt-1">
										<span class="lead w-75 px-3" id="doc_nm">zdbvzbj.doc</span>
										<input type="hidden" name="doc_nm_hid" id="doc_nm_hid" value="">
										<button type="button" class="btn"><i class="fas fa-times"></i></button>
									</div>
									<div class="progress" style="height: .2rem;">
										<div class="progress-bar bg-piquic" role="progressbar" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100" id="file_prg"></div>
									</div>
								</div>
							</div> -->
							<div class="row d-none" id="upmgs" >
										<div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
											<div class="bg-light d-flex justify-content-start pt-1">
												<span class="lead w-75" id="file_nm"></span>
												<input type="hidden" name="file_nm_hid" id="file_nm_hid" value="">
												<input type="hidden" name="temp_folder" id="temp_folder" value="<?php echo $user_id.time();?>">
												<!-- <button type="button" class="btn"><i class="fas fa-times"></i></button> -->
											</div>
											<div class="progress" style="height: .2rem;">
												<div class="progress-bar bg-piquic" role="progressbar" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100" id="file_pc"></div>
											</div>
										</div>
									</div>
						</div>

						<!-- <div class="w-100 mb-3 p-3">

							<div class="row">
								<div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
									<div class="alert alert-success text-center lead d-none" id="docSuccess" role="alert">
										<i class="far fa-thumbs-up fa-2x"></i><br>
										<span id="txtSuccess"></span>
									</div>

									<div class="alert alert-danger text-center lead d-none" id="docDanger" role="alert">
										<i class="far fa-thumbs-down fa-2x"></i><br>
										<span id="txtDanger"></span>
									</div>
								</div>
							</div>

						</div> -->

						<div class="row">
										<div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">

											<div  id="fileList_doc">

												

											</div>

										</div>
									</div>




						<div class="w-100 mb-3">

									<div class="row pt-3">
										<div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
											<div class="alert alert-success text-center lead d-none" id="zipSuccess" role="alert">
												<i class="far fa-thumbs-up fa-2x"></i><br>
												<span id="txtSuccess"></span>
											</div>

											<div class="alert alert-danger text-center lead d-none" id="docDanger" role="alert">
												<i class="far fa-thumbs-down fa-2x"></i><br>
												<span id="txtDanger"></span>
											</div>
										</div>
									</div>

									<div class="row pt-3  d-none" id="endocimg">
										<div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
											<span class="btn btn-outline-secondary btn-lg w-100 " id="docimg" onclick="saveBrief();" >Submit</span>
											<!-- <button type="button" class="btn btn-outline-secondary btn-lg w-100">Label your images</button> -->
										</div>
									</div>

									<div class="modal fade" id="modal_zip_label" tabindex="-1" role="dialog" aria-hidden="true">
										<div class="modal-dialog" role="document">
											<div class="modal-content">
												<div class="modal-header">
													<h5 class="modal-title text-dark" id="modMsg">Same SKU already exist! Are you sure you want to proceed?</h5>
													<button type="button" class="close text-danger" data-dismiss="modal" aria-label="Close" onclick="rnZipImg(false);">
														<span aria-hidden="true" >&times;</span>
													</button>
												</div>
												<div class="modal-footer">
													<button type="button" id="hideDel" class="btn btn-danger" onclick="rnZipImg(true);">OK</button>
													<button type="button" class="btn btn-secondary" data-dismiss="modal" onclick="rnZipImg(false);">Cancel</button>													
												</div>
											</div>
										</div>
									</div>

								</div>
							</div>


					</div>
				</div>

			</main>
		</div>

	</div>
</div>

<?php
$this->load->view('front/includes/footer');
?>

<script src='<?php echo base_url();?>/website-assets/js/dragdropfolder.js'></script>

<script type="text/javascript">
	// Folder Upload

	$(document).ready(function() {
		$("#skufolderupld_doc").click(function(e) {
			e.preventDefault();
		});
	});







   //added for zip rajendra 25-Feb-2020

   function removeZIPFolder(foldername, status){
	  // alert(foldername);
	$("div").remove("#foldername");
	// var r = confirm("Are you sure to delete! "+ foldername);
	var r = status;
	if (r == true) {
	  	// console.log("deleted");
	  
	  	var foldernamePath = 'zip/<?php echo $user_id;?>';
		var skufolderfile_nm = $('#file_nm_hid').val();

		$.ajax({
			method:"post",
			url: "<?php echo base_url();?>front/uploadbrief/removeZIPFolder",
			data:'&foldername='+foldername+'&path='+foldernamePath+'&skufolderfile_nm='+skufolderfile_nm,
			success: function(result){

				//alert(result);

				var element = document.getElementById(foldername);
				element.parentNode.removeChild(element);
				
				var element = document.getElementById(foldername+"_images");
				element.parentNode.removeChild(element);

				var element123 = document.getElementById("file_nm_hid").value;
				var folname=foldername+'.zip';
				var updatedvalue = element123.replace(folname,"");
				//alert (updatedvalue);
				document.getElementById("file_nm_hid").value= updatedvalue;

				if(updatedvalue=='') {
					window.location.href="<?php echo base_url();?>uploadimage";
				}
				
			 	$('#modal_' + foldername + '').modal('hide');
			 	$('#modal_msg').modal('show');
				$('#txtMsg').html(result);
			}
		});

		// console.log(skufolderfile_nm);
  
	} else {
	   // alert ("let it be");
	   // $('#modal_msg').modal('show');
		// $('#txtMsg').html('Let It Be.');
	}
}




   function openZIPFolder(foldername){

   	var foldernamePath='zip/<?php echo $user_id;?>';
   	var skufolderfile_nm=$('#file_nm_hid').val();
	var folder= skufolderfile_nm.split(".");
    var skufolderfile_nm = folder[0];
    $("#divOpenZIPFolder_"+foldername).hide();
    $("#divCloseZIPFolder_"+foldername).show();
   	

   	//alert("aaaaa");

   	$.ajax({
   		method:"post",
   		url: "<?php echo base_url();?>front/uploadbrief/openZIPFolder",
   		data:'&foldername='+foldername+'&path='+foldernamePath+'&skufolderfile_nm='+skufolderfile_nm,
   		success: function(result){
   			//alert(result);
   			$("#"+foldername+"_images").html(result);
   			$("#"+foldername+"_images").toggle();
   		}
   	});
   }






   

      function closeZIPFolder(foldername){
   	var foldernamePath='temp_folder_upload/<?php echo $user_id;?>';
   	var skufolderfile_nm=$('#file_nm_hid').val();
	var folder= skufolderfile_nm.split(".");
    var skufolderfile_nm = folder[0];
   	 $("#divOpenZIPFolder_"+foldername).show();
     $("#divCloseZIPFolder_"+foldername).hide();

   	  $("#"+foldername+"_images").toggle();
   }
   
   
   

   

	// Zip Upload
	function _(el){
		return document.getElementById(el);
	}




function uploadFile(){
		var file = _("upld_doc").files[0];
		var docfilepath='&emsp;<i class="fas fa-file-archive text-warning"></i>&emsp;'+file.name;
		$('#file_nm').html(docfilepath);
		//$('#file_nm_hid').val(file.name);
		var temp_folder =$('#temp_folder').val();

		var file1=file.name;
		var fileExtension = file1.split(".");
		var fileType=file.type;
        //alert(fileType);
		//if(fileType === "application/x-zip-compressed")
		if(fileType === "application/msword" || fileType === "application/vnd.openxmlformats-officedocument.wordprocessingml.document") {
			$('#upmgs').removeClass('d-none'); 
			var formdata = new FormData();
			formdata.append("upld_doc", file);
			formdata.append("temp_folder", temp_folder);
			var ajax = new XMLHttpRequest();
			ajax.upload.addEventListener("progress", progressHandler, false);
			ajax.addEventListener("load", completeHandler1, false);
			ajax.addEventListener("error", errorHandler, false);
			ajax.addEventListener("abort", abortHandler, false);
			ajax.open("POST", "<?php echo base_url();?>front/uploadbrief/uploadfile");
			ajax.send(formdata);
		} else {
			//alert('its not a zip file');
			$("#docDanger").removeClass("d-none");
			_("txtDanger").innerHTML = "Please Upload Doc/Docx File Only";
		}
	}


	function progressHandler(event){
		var percent = (event.loaded / event.total) * 100;
		console.log(percent);
		$("#file_pc").attr("aria-valuenow", Math.round(percent));
		$("#file_pc").css("width", Math.round(percent) + "%");
	}

	function completeHandler1(event){
			//alert(event);
	var responseText = $.ajax({
	 	type:'POST',
	 	url:"<?php echo base_url();?>front/uploadbrief/logincheck",
	 	//data: {'skufolderfile_nm':skufolderfile_nm},
	 	async: false
	 }).responseText;
	 //alert (responseText);
	 if($.trim(responseText)=='sessionout')
		{
		//$("#loader").hide();
		// alert("Session out please login first.");
		$('#modal_msg').modal('show');
        $('#txtMsg').html('Session out please login first.');
		window.location.href="<?php echo base_url();?>";
		return false;
		}
		else {

			/*$("#zipSuccess").removeClass("d-none");
			$('#endocimg').removeClass('d-none');
			_("txtSuccess").innerHTML = event.target.responseText;
*/

         var fileName =$('#file_nm_hid').val();
		   
		   console.log(event.target.responseText);
		 var response=event.target.responseText;
		//alert(response);
		var responseArr=response.split("@@@@@");
      	var doc_file_name=$.trim(responseArr[0]);
		var res_text=$.trim(responseArr[1]);
		if(fileName=='')
		{
			var fileNameCurrent=",,,"+doc_file_name+",,,";
		}
		else
		{
			var fileNameCurrent=fileName+doc_file_name+",,,";
		}
        
      	$('#file_nm_hid').val(fileNameCurrent);




	 
	 var fileName =$('#file_nm_hid').val();
	 var temp_folder =$('#temp_folder').val();

		//alert(fileName);
      	if(fileName!='') {
      		$('#fileList_doc').empty();

      		var fileNameStr=$.trim(fileName,",,,");
      		var fileNameArr=fileNameStr.split(",,,");

      			for(var k=0;k<fileNameArr.length;k++)
      			{
      				var itemFileName=fileNameArr[k];
      				if(itemFileName!='')
      				{
      					//alert(itemFileName);
      					var fileNameAllDiv='<div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12"> <span> <i class="fas fa-file-archive text-warning"></i> <a style="color:black;" href="<?php echo base_url();?>temp_brief_upload/'+temp_folder+'/'+itemFileName+'">'+itemFileName+'</a></span>   </div>';

      					/*<span id="" class="text-secondary lead" style="cursor:hand;" data-toggle="modal" data-target="#modal_,,,goto.docx,,,"><i class="far fa-times-circle" style="cursor:hand;"></i></span>
*/

      			        $('#fileList_doc').append(fileNameAllDiv);
      				}
      				
      				
      			}

      	} 
	
	 
	 
		
		}
		
		
		
		$("#zipSuccess").removeClass("d-none");
		$('#endocimg').removeClass('d-none');
		//_("txtSuccess").innerHTML = event.target.responseText;
    		_("txtSuccess").innerHTML = res_text;

	}






	function completeHandler(event){
		$("#zipSuccess").removeClass("d-none");
		$('#endocimg').removeClass('d-none');
		_("txtSuccess").innerHTML = event.target.responseText;
	}
	function errorHandler(event){
		$("#docDanger").removeClass("d-none");
		_("txtDanger").innerHTML = "Upload Failed";
	}
	function abortHandler(event){
		$("#docDanger").removeClass("d-none");
		_("txtDanger").innerHTML = "Upload Aborted";
	}

	function rnZipImg(status){
		var file_nm=$('#file_nm_hid').val();
		$("#loader").show();

		var x = status;
		if (x) {
			// console.log('inside true - ' + x);
			var responseText1 = $.ajax({
				type:'POST',
				url:"<?php echo base_url();?>front/uploadbrief/generateSKUForExistZip",
				data: {'file_nm':file_nm},
				async: false
			}).responseText;

			if(responseText1!='')
			{
				$.ajax({
					method:"post",
					url: "<?php echo base_url();?>front/uploadbrief/moveunzipfile",
					data:'file_nm='+file_nm,
					success: function(result){

						$("#loader").hide();
						window.location.href="<?php echo base_url();?>label";
					}
				});
			}
		} else {
			// console.log('inside false - ' + x);
			var responseText1 = $.ajax({
				type:'POST',
				url:"<?php echo base_url();?>front/uploadbrief/deleteCancelSKUForExistZip",
				data: {'file_nm':file_nm},
				async: false
			}).responseText;

			if(responseText1!='')
			{
				window.location.href="<?php echo base_url();?>uploadimage";
				$("#loader").hide();	
			}

			return false;
		}
	}

	function saveBrief(){



		var file_nm=$('#file_nm_hid').val();
		var brief_title =$('#brief_title').val();
		var temp_folder =$('#temp_folder').val();

		if(brief_title=="")
		{
 			alert("Please Enter Brief");
 			return false;
		}
		else
		{
			$("#loader").show();

		/*var responseText = $.ajax({
			type:'POST',
			url:"<?php echo base_url();?>front/uploadbrief/checkSKULenExistMoveunzipfile",
			data: {'file_nm':file_nm},
			async: false
		}).responseText;

		if($.trim(responseText)=='sessionout') {

			$("#loader").hide();
			// alert("Session out please login first.");
			$('#modal_msg').modal('show');
			$('#txtMsg').html('Session out!<br>Please do the login first.');
			window.location.href="<?php echo base_url();?>";
			return false;

		} else if($.trim(responseText)=='not_image') {

			$("#loader").hide();
			// alert("Files are not image.");
			$('#modal_msg').modal('show');
			$('#txtMsg').html('Files are not image.');
			window.location.href="<?php echo base_url();?>uploadimage";
			return false;

		} else if($.trim(responseText)=='less_strlen') {

			$("#loader").hide();
			// alert("SKU length should be minimum 3.");
			$('#modal_msg').modal('show');
			$('#txtMsg').html('SKU length should be minimum 3.');
			window.location.href="<?php echo base_url();?>uploadimage";
			return false;

		} else if($.trim(responseText)=='exist') {

			$("#loader").hide();
			$('#modal_zip_label').modal('show');

		} else {*/

			//alert('&brief_title='+brief_title+'&temp_folder='+temp_folder+'&file_nm='+file_nm);

			$.ajax({
				method:"post",
				url: "<?php echo base_url();?>front/uploadbrief/movedocfile",
				data:'&brief_title='+brief_title+'&temp_folder='+temp_folder+'&file_nm='+file_nm,
				success: function(result){

					//alert(result);
					$("#loader").hide();
					window.location.href="<?php echo base_url();?>dashboard";
				}
			});

		//}

		}
		
	}

	$(".upload_sku_files").on("change", function(e) {
		$("#loader").show();
		var files = e.target.files,
		filesLength = files.length;
		var f = files[0];
		var fileReader = new FileReader();
		fileReader.onload = (function(e) {
			var file = e.target;

			console.log(e.target.result);

			var input = document.getElementById("sku_files");
			var inputArr=input.files;
			var check_complete='';
			for(var k=0;k<inputArr.length;k++)
			{
				var file=inputArr[k];

				console.log("--------------");
				console.log(file);
				console.log(file.type);
				console.log("--------------");

				if(file != undefined && (file.type=='image/png' || file.type=='image/jpg'   || file.type=='image/jpeg') ){
					formData= new FormData();
					formData.append("image", file);

					var data = $.ajax({
						url: "<?php echo base_url();?>front/labelimages/upload_files_fromsku",
						type: "POST",
						data: formData,
						processData: false,
						contentType: false,
						async: false
					}).responseText;

					if($.trim(data)=='sessionout') {
						$("#loader").hide();
						// alert("Session out. Please login first!.");
						$('#modal_msg').modal('show');
						$('#txtMsg').html('Session out!<br>Please do the login first.');
						window.location.href="<?php echo base_url();?>home";
						return false;
					} else if($.trim(data)=='notsupport') {
						$("#loader").hide();
						// alert("There is some issue with image format!.");
						$('#modal_msg').modal('show');
						$('#txtMsg').html('There is some issue with image format!.');
						window.location.href="<?php echo base_url();?>uploadimage";
						return false;
					} else {
						var pimgVal=$.trim($("#pimg").val());
						if(pimgVal!='') {
							var item_chek=","+$.trim(data)+",";
							var check=(pimgVal.indexOf(item_chek));
							var pimgValItem=pimgVal+$.trim(data)+",";
						} else {
							var pimgValItem=","+$.trim(data)+",";
						}

						$("#pimg").val(pimgValItem);

						var pimgValCheckCount=$.trim($("#pimg").val());
						var pimgValCheckCount = pimgValCheckCount.replace(/^,|,$/g, '');

						var checkArr=pimgValCheckCount.split(",");
						var checkLength=checkArr.length;

						if(checkLength<=10) {
							var show_pimg="<div class='col-6 col-sm-6 col-md-3 col-lg-3 col-xl-3'><img class='w-100 border rounded mb-2' src='temp_upload/<?php echo $user_id;?>/"+"thumb_"+data+"' ></div>";

							$("#skuImageUploadDiv").append(show_pimg);

							$("#div_ctn_img").removeClass("d-none");
							$("#ctn_img").html(checkLength);
						} else {
							// alert("You exceed your limit. Only 10 images you can upload at a time");
							$('#modal_msg').modal('show');
							$('#txtMsg').html('You exceed your limit. Only 10 images you can upload at a time.');
						}

						var lastCount=parseInt(inputArr.length)-1;

						if(k==lastCount) {
							var ctn=0;
							$("img").on('load', function(){

								if (this.complete) {
									console.log("completed");

									var browname=myCheckBrowserFunction();
									console.log(browname);

									if(browname=='Chrome') {
										if(ctn==lastCount) {
											$("#loader").hide();
										} else {
											ctn++;
										}
									} else {
										$("#loader").hide();
									}
								} else {
									$(this).on('load', function() { // image now loaded
									});
								}
							});
						}
					}
				} else {
					$("#loader").hide();
					// alert('Uploaded file is not an image!');
					$('#modal_msg').modal('show');
					$('#txtMsg').html('Uploaded file is not an image!');
					window.location.href="<?php echo base_url();?>uploadimage";
				}
			}
		});

		fileReader.readAsDataURL(f);

		$('#upldImg').hide();
		$('#btnCretSKU').removeClass('d-none');
	});

	jQuery.validator.addMethod("alphanumeric", function(value, element) {
		return this.optional(element) || /^[a-z0-9\_]+$/i.test(value);
	}, "Enter only Letters, numbers, and underscore for SKU!.");

	jQuery(function ($) {
		$('#single_sku_form').validate({
			rules: {

	   		txtSKUNew:{
	   			required:true,
	   			minlength: 3,
	   			alphanumeric: true,
	   			remote: {
	   				url: "<?php echo base_url();?>front/uploadbrief/checkexistsku",
	   				type: "post"
	   			},
	   		},
	   	},

	   	messages: {
	   		txtSKUNew:{
	   			remote:"This sku already exists.",
	   			alphanumeric: 'Letters, numbers, and underscore only please'
	   		},
	   		
	   	},
	   	submitHandler: function(form) {
	   		$("#loader").show();

	   		var user_id='<?php echo $user_id;?>';
	   		var txtSKUNew=$('#txtSKUNew').val();
	   		var pimg=$('#pimg').val();

	   		$.ajax({
	   			method:'POST',
	   			url: "<?php echo base_url();?>front/uploadbrief/insertSKUFromSidebar",
	   			data:"&user_id="+user_id+"&txtSKUNew="+txtSKUNew+"&pimg="+pimg,

	   			success: function(result){
	   				if(result) {
	   					$("#loader").hide();
	   					window.location.href="<?php echo base_url();?>label";
	   				}
	   			}
	   		});
	   	},
	   });
	});

</script>