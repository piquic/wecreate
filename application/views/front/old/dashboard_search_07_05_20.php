<div id="tblbrf" class="row">

              <?php
              if(!empty($dashboradinfo))
              {
                $i=1;
                foreach($dashboradinfo as $keybill => $brief)
                { 
                  $brief_id=$brief['brief_id'];
                  $added_by_user_id=$brief['added_by_user_id'];
                  $brief_title=$brief['brief_title'];
                  $brief_name=preg_replace('/[^a-zA-Z0-9_.]/', '_',$brief_title);
                  $brief_doc=$brief['brief_doc'];
                  $brief_due_date=$brief['brief_due_date'];

                  $status=$brief['status'];
                  $brand_id=$brief['brand_id'];
                  $rejected_reasons=$brief['rejected_reasons'];

                  $checkquerys = $this->db->query("select wc_brands.*,wc_clients.client_name from wc_users 
                    inner join wc_brands on wc_users.brand_id=wc_brands.brand_id  
                    inner join wc_clients on wc_brands.client_id=wc_clients.client_id 
                    where  wc_users.user_id='$added_by_user_id' ")->result_array();

                  $brand_id=$checkquerys[0]['brand_id'];
                  $brand_name=preg_replace('/[^a-zA-Z0-9_.]/', '_',$checkquerys[0]['brand_name']);
                  $client_id=$checkquerys[0]['client_id'];
                  $client_name=preg_replace('/[^a-zA-Z0-9_.]/', '_',$checkquerys[0]['client_name']);

                  $dst = "upload/".$client_id."-".$client_name."/".$brand_id."-".$brand_name."/".$brief_id."-".$brief_name;

                  $download_rej_brief=base_url()."/upload/".$client_id."-".$client_name."/".$brand_id."-".$brand_name."/".$brief_id."-".$brief_name."/".$brief_doc;
                  $status_query1= $this->db->query("SELECT * FROM wc_image_upload WHERE brief_id='$brief_id'");
                  $status_result1= $status_query1->result_array();
                  $count=$status_query1->num_rows();

                  $accept_reject_permission=$this->Dashboard_model->checkPermission($user_type_id,3,'accept_reject');
                  ?>
                  <div class="col-12 col-sm-12 col-md-4 col-lg-3 col-xl-3 mb-4">
                    <div class="card m-auto" style="max-width: 22.6rem; ">

                      <?php 
                      if($count>0){
                        $image_id=$status_result1[0]['image_id'];
                        $image_path=$status_result1[0]['image_path'];
                        $checkquerys1 = $this->db->query("select * from wc_image_upload where image_id='$image_id' ")->result_array();
                        $parent_img=$checkquerys1[0]['parent_img'];
                        $version_num=$checkquerys1[0]['version_num'];
                        $file_type=$checkquerys1[0]['file_type'];


                        $dst = "upload/".$client_id."-".$client_name."/".$brand_id."-".$brand_name."/".$brief_id."-".$brief_name."/".$parent_img."/Revision".$version_num;  
                        $html=""; 



                        $image_org="upload/".$client_id."-".$client_name."/".$brand_id."-".$brand_name."/".$brief_id."-".$brief_name."/".$image_id."/Revision".$version_num."/thumb_".$image_path;

                        ?>
                        <div class="thumb-div rounded-top" style="background-image: url(<?php echo base_url()."/".$image_org; ?>)" <?php 
                        if($accept_reject_permission=='0' && ($status=='0' ||$status=='1'||$status=='2'||$status=='4'||$status=='5'||$status=='6'||$status=='7')){  ?>
                          data-toggle="collapse" data-target="#click_<?php echo $brief_id;?>" aria-expanded="false" aria-controls="click_<?php echo $brief_id;?>"
                          <?php } else if($accept_reject_permission=='1' && ($status=='0'||$status=='1'||$status=='2'||$status=='6'||$status=='7')){  ?> data-toggle="collapse" data-target="#click_<?php echo $brief_id;?>" aria-expanded="false" aria-controls="click_<?php echo $brief_id;?>" <?php } ?> ></div>
                          <?php
                           $checkquerys2 = $this->db->query("select * from wc_image_upload where brief_id='$brief_id' and img_status='1' and image_id IN (SELECT MAX(image_id) FROM wc_image_upload  GROUP BY parent_img )");
                          $approved_img_count=$checkquerys2->num_rows();
                           $checkquerys3 = $this->db->query("select * from wc_image_upload where brief_id='$brief_id' and (img_status='0' || img_status='2' || img_status='3' || img_status='4') and image_id IN (SELECT MAX(image_id) FROM wc_image_upload  GROUP BY parent_img )");
                          $other_img_count=$checkquerys3->num_rows();
                        }
                        else{
                          ?>
                          <div class="thumb-div rounded-top" style="background-image: url(<?php echo base_url() ?>website-assets/images/no-image.png);" <?php 

                          if($accept_reject_permission=='0' && ($status=='0' ||$status=='1'||$status=='2'||$status=='4'||$status=='5'||$status=='6'||$status=='7')){  ?>
                            data-toggle="collapse" data-target="#click_<?php echo $brief_id;?>" aria-expanded="false" aria-controls="click_<?php echo $brief_id;?>"
                            <?php } else if($accept_reject_permission=='1' && ($status=='0'||$status=='1'||$status=='2'||$status=='6'||$status=='7')){  ?> data-toggle="collapse" data-target="#click_<?php echo $brief_id;?>" aria-expanded="false" aria-controls="click_<?php echo $brief_id;?>" <?php } ?> ></div>

                            <?php
                          }
                          ?>
                          <div class="card-body">
                            <h5 class="card-title"><?php echo $brief_title;?></h5>
                            <div class="card-text mb-3">
                              <div class="d-flex justify-content-between flex-wrap">
                                <span class="py-1">
                                  <i class="far fa-clock"></i>&nbsp;
                                  <?php
                                  if($status=='0' ) 
                                    { 
                                      //echo "N/A"; 
                                      if($brief_due_date=='0000-00-00 00:00:00') { 
                                      echo "N/A";
                                       }
                                    else {
                                      $brief_due_date= date('d M y H:i', strtotime($brief_due_date));
                                      echo $brief_due_date;
                                    }    
                                   }
                                  else {

                                    if($brief_due_date=='0000-00-00 00:00:00') { 
                                      echo "N/A";
                                       }
                                    else {
                                      $brief_due_date= date('d M y H:i', strtotime($brief_due_date));
                                      echo $brief_due_date;
                                    }                                           
                                  }
                                  ?>
                                </span>
                                <?php if($count>0){ ?>
                                  <span>
                                    <span class="c-ico"><?php echo $approved_img_count ?></span>&nbsp;<span class="c-ico bg-secondary"><?php echo $other_img_count ?></span>
                                  </span>
                                  <?php
                                }
                                ?>
                              </div>

                              <p>#P<?php echo $brief_id;?></p>
                            </div>
                            <div class="d-flex justify-content-between">
                              <small class="py-2 px-3 text-wc border-wc rounded-pill">
                                <?php
                                if($status=='0') { echo "Brief in Review"; }
                                else if($status=='1') { echo "Work in Progress"; }
                                else if($status=='2') { echo "Work Completed";  }
                                else if($status=='3') { echo "Revision Work"; }
                                else if($status=='4') { echo "Brief Rejected"; }
                                else if($status=='5') { echo "Feedback Pending"; }
                                else if($status=='6') { echo "Proofing Pending"; }
                                else if($status=='7') { echo "Archived"; }                                  
                                ?>
                              </small>
                              <?php 
                              $accept_reject_permission=$this->Dashboard_model->checkPermission($user_type_id,3,'accept_reject');
                              if($accept_reject_permission=='0' && ($status=='0' ||$status=='1'||$status=='2'||$status=='4'||$status=='5'||$status=='6'||$status=='7')){  ?>
                                <span class="btn btn-link text-dark" data-toggle="collapse" data-target="#click_<?php echo $brief_id;?>" aria-expanded="false" aria-controls="click_<?php echo $brief_id;?>"><i class="fas fa-ellipsis-h"></i></span>
                              <?php  }else if($accept_reject_permission=='1' && ($status=='0'||$status=='1'||$status=='2'||$status=='6'||$status=='7')){  ?>
                                <span class="btn btn-link text-dark" data-toggle="collapse" data-target="#click_<?php echo $brief_id;?>" aria-expanded="false" aria-controls="click_<?php echo $brief_id;?>"><i class="fas fa-ellipsis-h"></i></span>
                              <?php  } ?>
                            </div>
                          </div>
                          <?php if($user_type_id=='1' || $user_type_id=='2'){
                          ?>
                          <span class="position-absolute" style="top: -0.56rem; right:-.9rem;" data-toggle="modal" data-target="#modal_confirm_<?php echo $brief_id; ?>"><i class="fas fa-times-circle icon-md text-danger bg-white rounded-circle" style="padding: .1rem"></i></span>
                              
                          <div class="modal fade" id="modal_confirm_<?php echo $brief_id; ?>" tabindex="-1" role="dialog" aria-hidden="true">
                            <div class="modal-dialog" role="document">
                              <div class="modal-content">
                                <div class="modal-header">
                                  <h5 class="modal-title text-dark" id="mod_title_<?php echo $brief_id; ?>">Are you sure, you want to delete the project #P<?php echo $brief_id; ?>?</h5>
                                  <button type="button" class="close text-danger" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true" >&times;</span>
                                  </button>
                                </div>
                                <div class="modal-footer">
                                  <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                  <button type="button" id="hideDel" class="btn btn-danger" onclick="delete_card('<?php echo $brief_id; ?>');">Delete</button>
                                </div>
                              </div>
                            </div>
                          </div>
                            <?php
                          } ?>
                        </div>
                        
                        <div id="click_<?php echo $brief_id;?>" class="collapse" data-parent="#tblbrf">
                          <div class="border rounded-lg pt-3 px-3 bg-white" style="width: 85%; bottom: 5%; right: 5%; position: absolute; z-index: 1113;">
                            <div class="d-flex justify-content-around">
                              <p class="flex-grow-1">Actions</p>
                              <span data-toggle="collapse" data-target="#click_<?php echo $brief_id;?>" aria-expanded="false" aria-controls="collapseAccount"><i class="fas fa-times"></i></span>
                            </div>

                            <hr class="border-top p-0 mt-0">
                            <?php
                            if($status=='0' && $accept_reject_permission=='1'){ ?>


                                <!-- <div > -->
                                  <div id="brief_approve_reject_<?php echo $brief_id ?>" class="d-flex justify-content-around mb-3">
                                    <div class="flex-grow-1 pt-2">Approve brief?</div>
                                    <div>
                                      <button type="button" class="btn btn-outline-wc rounded-pill"  name="brief_status" id="brief_status<?php echo $brief_id;?>" onclick="changeBriefStatus('<?php echo $brief_id;?>','1')">Yes</button>
                                      <button type="button" class="btn btn-outline-wc rounded-pill"  name="brief_status" id="brief_status<?php echo $brief_id;?>" onclick="changeBriefStatus('<?php echo $brief_id;?>','4')">No</button>
                                    </div>
                                  </div>
                                  <!-- </div> -->
                                  <div class="d-none" id="div_brief_due_date<?php echo $brief_id; ?>">
                                    <div class="mb-3" >
                                      <div class="pt-2 table-responsive tbl-scrl" style="height: 10rem !important;">
                                        <table class="table text-wc">
                                          <thead>
                                            <tr>
                                              <th scope="col">Item(s)</th>
                                              <th scope="col">Qty.</th>
                                              <th scope="col">Price</th>
                                              <th scope="col"></th>
                                            </tr>
                                          </thead>
                                          <tbody id="product-list">
                                            <tr id="no-result">
                                              <td>
                                                <div style='width:100%; white-space:nowrap; overflow:hidden; text-overflow: ellipsis; text-align: center;'>No Result Found.</div>
                                              </td>
                                            </tr>
                                          </tbody>
                                        </table>
                                      </div>
                                      
                                      <div class="pt-2">
                                        <p class="lead text-wc">Total: Rs.<span id="total_price_values">0</span></p>
                                        <input type='hidden' id='total_price' name='total_price' />
                                        
                                        <?php 
                                        $price_query1= $this->db->query("SELECT * FROM wc_product_price");
                                        $price_result1= $price_query1->result_array();
                                        //print_r($price_result1);
                                        ?>
                                        <div class="d-flex">
                                          <div class="flex-grow-1 m-1">
                                            <select class="custom-select" id="product_id_<?php echo $brief_id; ?>" >
                                              <option >Select Item</option>
                                              <?php
                                              foreach($price_result1 as $keyprice => $price_val){ 
                                                
                                                ?>
                                                <option value="<?php echo $price_val['product_id']; ?>"><?php echo $price_val['product_name']; ?></option>
                                                <?php
                                              }
                                              ?>
                                              
                                            </select>
                                          </div>
                                          <div class="m-1">
                                            <input type="text" name="txtQty_<?php echo $brief_id; ?>" id="txtQty_<?php echo $brief_id; ?>" class="form-control" style="width: 4rem;">
                                          </div>

                                          <div class="m-1">
                                            <button type="submit" class="btn btn-wc" id="btnadditem_<?php echo $brief_id; ?> " onclick="price_cal('<?php echo $brief_id; ?>')">Add</button>
                                          </div>
                                        </div>
                                      </div>
                                    </div>

                                    <hr class="border-top p-0 mt-0">
                                    <div class="mb-3">
                                      <div class="pt-2">
                                        

                                          <!-- <input type="text" class="form-control datePick " name="brief_due_date" id="brief_due_date<?php echo $brief_id;?>" value="<?php echo date("Y-m-d H:i")?>" placeholder="<?php echo date("Y-m-d H:i")?>"> -->

                                      <?php
                                      if($status=='0' && $brief_due_date!='0000-00-00 00:00:00')
                                      {
                                        $sel_brief_due_date=date("d-M-Y H:i",strtotime($brief_due_date));
                                        $disabledFlag='disabled';

                                        ?>
                                      <p class="text-wc"><strong> Delivery Date : </strong><span><?php echo $sel_brief_due_date ?></span></p>

                                        <?php
                                      }
                                      else
                                      {
                                        $sel_brief_due_date=date("Y-m-d H:i");
                                        ?>
                                        Enter Delivery Date to approve:

                                        <div class="input-group mb-2 mr-sm-2 " >
                                          <div class="input-group-prepend ">
                                            <div class="input-group-text "><i class="far fa-calendar-alt"></i></div>
                                          </div>
                                        <input type="text" class="form-control datePick " name="brief_due_date" id="brief_due_date<?php echo $brief_id;?>" value="<?php echo $sel_brief_due_date;?>" placeholder="<?php echo $sel_brief_due_date;?>">
                                        <script type="text/javascript">
                                            $(function () {
                                              $('#brief_due_date<?php echo $brief_id;?>').datetimepicker({
                                                  //language:  'fr',
                                                  weekStart: 1,
                                                  todayBtn:  1,
                                                  autoclose: 1,
                                                  todayHighlight: 1,
                                                  startView: 2,
                                                  forceParse: 0,
                                                  showMeridian: 1
                                               });

                                            });
                                          </script>

                                        </div>
                                        <?php
                                      }
                                      ?>
                                      </div>
                                    </div>
                                  </div>
                                  

                                  <div class="mb-3 d-none" id="brief_review_approve_<?php echo $brief_id ?>">
                                    <div class="d-flex justify-content-around flex-wrap">
                                      <button type="button" class="btn btn-outline-wc flex-grow-1 m-1" onclick="submitCorfirm(<?php echo $brief_id;?>,'1');">Approve</button>
                                      <button type="button" class="btn btn-outline-wc flex-grow-1 m-1" onclick="submitCancel(<?php echo $brief_id;?>);">Cancel</button>
                                    </div>
                                  </div>
                                  <div class="mb-3 d-none" id="brief_review_reject_<?php echo $brief_id ?>">
                                    <div class="pt-2">
                                      <span class="b-text bg-white p-2 ">REASON FOR REJECTION</span><br>
                                      <input type="text" name="rejected_reasons" id="rejected_reasons<?php echo $brief_id;?>" class="form-control mb-3 border" value="<?php echo $rejected_reasons;?>" >
                                      <div>
                                        <button type="submit" class="btn btn-outline-wc rounded-pill" onclick="submitCorfirm(<?php echo $brief_id;?>,'4');">Confirm</button>
                                        <button type="submit" class="btn btn-outline-wc rounded-pill" onclick="submitCancel(<?php echo $brief_id;?>);">Cancel</button>

                                      </div>
                                    </div>
                                  </div>
                                  <hr class="border-top p-0 mt-0">
                                  <?php
                              }
                              if($status=='0'||$status=='6'){
                                ?>
                                <p><a class="btn btn-outline-dark btn-block text-left border-0" href="<?php echo $download_rej_brief;?>">Download Brief</a></p>
                                <?php
                              }
                              if($status=='1'||$status=='2'||$status=='5'||$status=='6'||$status=='7'){ ?>

                                <p><a class="btn btn-outline-dark btn-block text-left border-0" href="<?php echo base_url();?>view-files/<?php echo $brief_id; ?>">View Files</a></p>

                                <p><a class="btn btn-outline-dark btn-block text-left border-0" data-toggle="collapse" role="button" href="#items_<?php echo $brief_id;?>" aria-expanded="false" aria-controls="items_<?php echo $brief_id;?>">View Items</a></p>

                            
                                <div class="mb-3 d-none" id="div_view_items<?php echo $brief_id; ?>">
                                  
                                </div>

                                <?php

                              }
                              if($status=='0' && $accept_reject_permission=='0'){ 
                                ?>
                                <p><a class="btn btn-outline-dark btn-block text-left border-0"  data-toggle="collapse" role="button" href="#ModifiedDate_<?php echo $brief_id;?>" aria-expanded="false" aria-controls="ModifiedDate_<?php echo $brief_id;?>">Modfied Date</a></p>
                                <?php
                              }

                              if($status=='1' &&$accept_reject_permission=='1')
                                { ?>
                                  <p><a class="btn btn-outline-dark btn-block text-left border-0" href="<?php echo base_url();?>uploadimages/<?php echo $brief_id; ?>">Upload Files</a></p>
                                <?php } 
                                
                              if($status=='2') { ?>
                                
                                <p><a class="btn btn-outline-dark btn-block text-left border-0" href="<?php echo base_url() ?>feedback/<?php echo $brief_id;?>">Add Feedback</a></p>

                              <?php }
                              if($status=='3') { echo "Revision Work"; }
                              if($accept_reject_permission=='0'&& $status=='4') {?>

                                <p><a class="btn btn-outline-dark btn-block text-left border-0" href="<?php echo $download_rej_brief;?>">Download Rejected Brief</a></p>
                                <!-- <p><label class="w3-button w3-blue w3-round"> -->
                                  <p><label class="btn btn-outline-dark btn-block text-left border-0">
                                    Upload New Brief
                                    <input type="file" style="display: none" class="re_upload_doc fas fa-download fa-2x" id="files1" alt='1' name="files1[]"  multiple val="<?php echo $brief_id;?>"  folder=""/>
                                  </label>

                                  <input type="hidden" name="file_nm_hid_<?php echo $brief_id;?>" id="file_nm_hid_<?php echo $brief_id;?>" value=""></p>

                                  <hr class="border-top p-0 mt-0">

                                  <p>Reason of Rejection:</p>
                                  <div class="mb-3 scrl" style="height: 6rem !important;">
                                    <small><?php echo $rejected_reasons;?></small>
                                  </div>

                                <?php }
                                if($accept_reject_permission=='0'&& $status=='5') { ?> 
                                  <p><a class="btn btn-outline-dark btn-block text-left border-0" href="<?php echo base_url() ?>feedback/<?php echo $brief_id;?>">Add Feedback</a></p>
                                <?php }
                                if($status=='6') { ?>
                                  <p><a class="btn btn-outline-dark btn-block text-left border-0" href="<?php echo base_url() ?>open-proofing\<?php echo $brief_id;?>">View Proofing</a></p>
                                  
                                <?php }                              
                                ?>
                              </div>

                            </div>
                            <div id="items_<?php echo $brief_id;?>" data-parent="#tblbrf" class="collapse border rounded-lg pt-3 px-3 bg-white" style="width: 85%; bottom: 5%; right: 5%; position: absolute; z-index: 1113;">
                            <div class="d-flex justify-content-around">
                              <p class="flex-grow-1">Items</p>
                              <span data-toggle="collapse" role="button" href="#items_<?php echo $brief_id;?>" aria-expanded="false" aria-controls="items_<?php echo $brief_id;?>"><i class="fas fa-times"></i></span>
                            </div>

                            <hr class="border p-0 mt-0">

                            <div class="mb-3">
                              <div class="pt-2 table-responsive tbl-scrl" style="height: 10rem !important;">
                                    <table class="table text-wc">
                                      <thead>
                                        <tr>
                                          <th scope="col">Item(s)</th>
                                          <th scope="col">Qty.</th>
                                          <th scope="col">Price</th>
                                          
                                        </tr>
                                      </thead>
                                      <?php 
                                      $product_query1= $this->db->query("SELECT * FROM wc_brief_product_list where brief_id='$brief_id'");
                                      $product_result1= $product_query1->result_array();

                                      $total_price_query1= $this->db->query("SELECT total_price FROM wc_brief where brief_id='$brief_id'");
                                      $total_price_result1= $total_price_query1->result_array();

                                      //
                                      ?>
                                      <tbody id="view_product-list">
                                        <?php 
                                        if(!empty($product_result1))
                                        {
                                          //print_r($product_result1);
                                          foreach($product_result1 as $key => $product_value) {
                                            $brief_product_id=$product_value['brief_product_id'];
                                            $product_id=$product_value['product_id'];

                                            $product_quty=$product_value['product_quty'];
                                            $subtotal_price=$product_value['subtotal_price'];

                                            $product_name_query1= $this->db->query("SELECT product_name FROM wc_product_price where product_id='$product_id'");
                                            $product_name_result1= $product_name_query1->result_array();
                                            //print_r($product_name_result1);
                                            $product_name=$product_name_result1[0]['product_name'];
                                            ?>

                                        <tr id="view_<?php echo $brief_product_id ?>">
                                          <td><div class="small" style="width: 90%; white-space:nowrap; overflow:hidden; text-overflow: ellipsis;"><?php echo $product_name ?></div></td>
                                          <td class="small text-right"><?php echo $product_quty ?></td>
                                          <td class="small text-right">Rs.<?php echo $subtotal_price ?></td>
                                         
                                        </tr>
                                        <?php
                                          }
                                        }
                                        if(empty($product_result1)){
                                        ?>
                                        <tr id="no-result">
                                          <td>
                                            <div style='width:100%; white-space:nowrap; overflow:hidden; text-overflow: ellipsis; text-align: center;'>No Result Found</div>
                                          </td>
                                        </tr>
                                        <?php 
                                      } ?>
                                      </tbody>
                                    </table>
                                  </div>
                                  
                                  <div class="pt-2">
                                    <?php if(!empty($total_price_result1[0]['total_price'])){
                                      ?>
                                      <p class="lead text-wc">Total: Rs.<span><?php echo $total_price_result1[0]['total_price'] ?></span></p>
                                      <?php
                                    } if(empty($total_price_result1[0]['total_price'])){
                                        ?>
                                      <p class="lead text-wc">Total: Rs.<span>0</span></p>
                                      <?php
                                    }
                                    ?>
                                    
                                  </div>
                              
                            </div>

                            <hr class="border p-0 mt-0">
                            <?php
                            if($brief_due_date!='0000-00-00 00:00:00')
                            {
                              $sel_brief_due_date=date("d-M-Y H:i",strtotime($brief_due_date));
                            ?>
                              <p class="text-wc"><strong> Delivery Date : </strong><span><?php echo $sel_brief_due_date ?></span></p>
                            <?php
                            } ?>
                            <!--  -->
                          </div>
                          <div id="ModifiedDate_<?php echo $brief_id;?>" data-parent="#tblbrf" class="collapse border rounded-lg pt-3 px-3 bg-white" style="width: 85%; bottom: 5%; right: 5%; position: absolute; z-index: 1113;">
                            <div class="d-flex justify-content-around">
                              <p class="flex-grow-1">Modified Date</p>
                              <span data-toggle="collapse" role="button" href="#ModifiedDate_<?php echo $brief_id;?>" aria-expanded="false" aria-controls="ModifiedDate_<?php echo $brief_id;?>"><i class="fas fa-times"></i></span>
                            </div>

                            <hr class="border p-0 mt-0">
                            Enter Delivery Date to approve:

                            <div class="input-group mb-2 mr-sm-2 " >
                              <div class="input-group-prepend ">
                                <div class="input-group-text "><i class="far fa-calendar-alt"></i></div>
                              </div>
                              <?php $sel_brief_due_date=date("Y-m-d H:i"); ?>
                            <input type="text" class="form-control datePick " name="brief_due_date" id="brief_due_date<?php echo $brief_id;?>" value="<?php echo $sel_brief_due_date;?>" placeholder="<?php echo $sel_brief_due_date;?>">

                            <script type="text/javascript">
                                $(function () {
                                  $('#brief_due_date<?php echo $brief_id;?>').datetimepicker({
                                    //language:  'fr',
                                    weekStart: 1,
                                    todayBtn:  1,
                                    autoclose: 1,
                                    todayHighlight: 1,
                                    startView: 2,
                                    forceParse: 0,
                                    showMeridian: 1
                                 });

                                });
                              </script>

                            </div>
                            <div class="mb-3" id="brief_review_approve_<?php echo $brief_id ?>">
                                    <div class="d-flex justify-content-around flex-wrap">
                                      <button type="button" class="btn btn-outline-wc flex-grow-1 m-1" onclick="submitCorfirm(<?php echo $brief_id;?>,'0');">Approve</button>
                                      <button type="button" class="btn btn-outline-wc flex-grow-1 m-1" onclick="submitCancel(<?php echo $brief_id;?>);">Cancel</button>
                                    </div>
                                  </div>
                          </div>
                          </div>
                          <!-- <div class="col-12 col-sm-12 col-md-4 col-lg-3 col-xl-3 mb-4">&nbsp;</div> -->

                        <?php } }else{?>
                          <div class="col-12 col-sm-12 col-md-12 mb-4">
                            <p class="text-wc lead"><i class="fas fa-info-circle icon-md"></i>&nbsp;No Records Found. Click Add Project to add a new project.</p>
                          </div>
                          <?php

                        }?> 

                      </div>