<?php
session_start();
error_reporting(0);
if ($this->session->userdata('front_logged_in')) {
$session_data = $this->session->userdata('front_logged_in');
$user_id = $session_data['user_id'];
$user_name = $session_data['user_name'];
$_SESSION['user_id']=$user_id;
$_SESSION['user_name']=$user_name;
//$type1=$this->session->userdata('user_type_id');
 $type1 = $session_data['user_type_id'];
 $brand_access=$session_data['brand_access'];
	if($type1==3)
	{
		$job_title="Project Manager";
	}
	else
	{
		$job_title="Branch Manager";
	}
}
else
{
$user_id = '';
$user_name = ''; 
$_SESSION['user_id']=$user_id;
$_SESSION['user_name']=$user_name;
}

$wc_brief_sql = "SELECT * FROM `wc_brief` where brief_id='$brief_id'";
								$wc_brief_query = $this->db->query($wc_brief_sql);
								$wc_brief_result=$wc_brief_query->result_array();
								$brief_title=$wc_brief_result[0]['brief_title'];



?>

<?php $data['page_title'] = "upload image";

$this->load->view('front/includes/header',$data);
$this->load->view('front/includes/topnav');
 ?>
<div class="container-fluid">
	<div class="row">

		<!-- Main Body -->
		<div class="col-12 col-sm-12 col-md-12">
			<main class="p-3">

				<div class="row">
					<div class="col-12 col-sm-12 col-md-12">
						<div class="p-3">
							<p class="text-wc lead"><i class="fas fa-arrow-alt-circle-left"></i><a href="<?php echo $base_url; ?>../dashboard">&nbsp;Back to Dashboard</a></p>
						</div>
					</div>
				</div>

				<div class="row">
					<div class="col-12 col-sm-12 col-md-12">
						<div class="p-3 text-wc">
							<h3 class="h3 text-uppercase">
								<?php echo $brief_title; ?> (ID: <?php echo $brief_id; ?>) - Upload - Project Manager
							</h3>
						</div>
					</div>
				</div>

				<div class="row">
					<div class="col-12 col-sm-12 col-md-12">
						<div class="p-2">

							<div class="row">
								<div class="col-12 col-sm-12 col-md-3">
									<div class="text-center">V1</div>
								</div>

								<div class="col-12 col-sm-12 col-md-3">
									<div class="text-center">V2</div>
								</div>

								<div class="col-12 col-sm-12 col-md-3">
									<div class="text-center">V3</div>
								</div>

								<div class="col-12 col-sm-12 col-md-3">
									<div class="text-center">V4</div>
								</div>
							</div>
						</div>
					</div>
				</div>

				<hr class="border-wc">
								<div class="row">
					<div class="col-12 col-sm-12 col-md-12">
						<div class="p-4">

							<div class="row">
								<div class="col-12 col-sm-12 col-md-3 p-2">
									<div class="mb-3">
										<div class="bg-light" style="min-height: 20rem;">
											<form action="" method="post" enctype="multipart/form-data" id="upldForm1" name="upldForm1">
												<div class="custom-file">
													<input type="file" class="custom-file-input customfileinput upload_brief_files" id="upld_img1" name="upld_img1" onchange="uploadFile()">
													<label class="custom-file-label customfilelabel text-center" for="upld_img1"><i class="fas fa-upload fa-3x"></i><br><br>Drop the file to upload or <i><u>browse</u></i>.</label>
												</div>
											</form>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
						<?php 
							$wc_brief_sql = "SELECT * FROM `wc_image_upload` where brief_id='$brief_id' and parent_img=0 ORDER BY `wc_image_upload`.`image_id` DESC";
								$wc_brief_query = $this->db->query($wc_brief_sql);

								$wc_brief_result=$wc_brief_query->result_array();
								foreach($wc_brief_result as $keybill => $briefimages)
								{   
								    $img_id=$briefimages['image_id'];
									$ststus=$briefimages['img_status'];
									$version=$briefimages['version_num'];
									$img_title=$briefimages['img_title'];
									$img_status=$briefimages['img_status'];
								?>
								
				<div class="row">
					<div class="col-12 col-sm-12 col-md-12">
						<div class="p-4">

							<div class="row">
								<div class="col-12 col-sm-12 col-md-3 p-2">
									<div class="card">
										<img src="https://placeimg.com/1366/1024/any" class="card-img-top" alt="...">
										<div class="card-body">
											<div class="d-flex justify-content-between">
												<span class="lead"><?php echo $img_title; ?></span>

												<?php if($img_status==1) { ?>
												<i class="fas fa-circle text-wc fa-2x"></i>
											<?php } elseif($img_status==2) {?>
												<i class="fas fa-circle text-danger fa-2x"></i>
												<?php } elseif($img_status==3) {?>
												
								<?php } else { ?>
								
								<?php } ?>
											</div>
										</div>
									</div>
								</div>
								<?php 
							 $wc_brief_sql_sub = "SELECT * FROM `wc_image_upload` where brief_id='$brief_id' and parent_img=$img_id ORDER BY `version_num` ASC";
								$wc_brief_query_sub = $this->db->query($wc_brief_sql_sub);

								$wc_brief_result_sub=$wc_brief_query_sub->result_array();
								foreach($wc_brief_result_sub as $keybill_sub => $briefimages_sub)
								{   
								    $img_id_sub=$briefimages_sub['image_id'];
									$ststus_sub=$briefimages_sub['img_status'];
									$version_sub=$briefimages_sub['version_num'];
									$img_title_sub=$briefimages_sub['img_title'];
									$img_status_sub=$briefimages_sub['img_status'];
									
								?>
								
								<div class="col-12 col-sm-12 col-md-3 p-2">
									<div class="card">
										<img src="https://placeimg.com/1366/1024/any" class="card-img-top" alt="...">
										<div class="card-body">
											<div class="d-flex justify-content-between">
												<span class="lead"><?php echo $img_title_sub; ?></span>
											<?php if($img_status_sub==1) { ?>
												<i class="fas fa-circle text-wc fa-2x"></i>
											<?php } elseif($img_status_sub==2) {?>
												<i class="fas fa-circle text-danger fa-2x"></i>
												<?php } elseif($img_status_sub==3) {?>
												<i class="fas fa-sync-alt fa-2x"></i>

												
								<?php } else { ?>
								
								<?php } ?>
											</div>
										</div>
									</div>
								</div>
								<?php } 
								 $wc_brief_sql_ver = "SELECT version_num,img_status FROM `wc_image_upload` where brief_id='$brief_id' and parent_img=$img_id ORDER BY `version_num` DESC";
								$wc_brief_query_ver = $this->db->query($wc_brief_sql_ver);
							   $wc_brief_result_ver=$wc_brief_query_ver->result_array();
								//print_r($wc_brief_result_ver);
								 $max_ver=$wc_brief_result_ver[0]['version_num'];
								 $img_status_ver=$wc_brief_result_ver[0]['img_status'];
								if(($max_ver<4)&&($img_status_ver!=1)&&($img_status_ver!=3)&&($img_status!=1))
								{
								?>		
								
								
								<div class="col-12 col-sm-12 col-md-3 p-2">
									<div class="mb-3">
										<div class="bg-light" style="min-height: 20rem;">
											<form action="" method="post" enctype="multipart/form-data" id="upldForm2" name="upldForm2">
											<input type="hidden" name="img_id" value="<?php echo $img_id; ?>">
												<div class="custom-file">
													<input type="file" class="custom-file-input customfileinput upload_brief_files" id="upld_img2" name="upld_img2" onchange="uploadFile()">
													<label class="custom-file-label customfilelabel text-center" for="upld_img2"><i class="fas fa-upload fa-3x"></i><br><br>Drop the file to upload or <i><u>browse</u></i>.</label>
												</div>
											</form>
										</div>
									</div>
								</div>
								<?php } ?>
							</div>
						</div>
					</div>
				</div>
								<?php } ?>
				<!--<div class="row">
					<div class="col-12 col-sm-12 col-md-12">
						<div class="p-4">

							<div class="row">
								<div class="col-12 col-sm-12 col-md-3 p-2">
									<div class="card">
										<img src="https://placeimg.com/1366/1024/any" class="card-img-top" alt="...">
										<div class="card-body">
											<div class="d-flex justify-content-between">
												<span class="lead">Image Name</span>

												<i class="fas fa-circle text-wc fa-2x"></i>
											</div>
										</div>
									</div>
								</div>

								<div class="col-12 col-sm-12 col-md-3 p-2">
									<div class="mb-3">
										<div class="bg-light" style="min-height: 20rem;">
											<form action="" method="post" enctype="multipart/form-data" id="upldForm2" name="upldForm2">
												<div class="custom-file">
													<input type="file" class="custom-file-input customfileinput" id="upld_img2" name="upld_img2" onchange="uploadFile()">
													<label class="custom-file-label customfilelabel text-center" for="upld_img2"><i class="fas fa-upload fa-3x"></i><br><br>Drop the file to upload or <i><u>browse</u></i>.</label>
												</div>
											</form>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>

				<div class="row">
					<div class="col-12 col-sm-12 col-md-12">
						<div class="p-4">

							<div class="row">
								<div class="col-12 col-sm-12 col-md-3 p-2">
									<div class="card">
										<img src="https://placeimg.com/1366/1024/any" class="card-img-top" alt="...">
										<div class="card-body">
											<div class="d-flex justify-content-between">
												<span class="lead">Image Name</span>

												<i class="fas fa-circle text-danger fa-2x"></i>
											</div>
										</div>
									</div>
								</div>

								<div class="col-12 col-sm-12 col-md-3 p-2">
									<div class="card">
										<img src="https://placeimg.com/1366/1024/any" class="card-img-top" alt="...">
										<div class="card-body">
											<div class="d-flex justify-content-between">
												<span class="lead">Image Name</span>

												<i class="fas fa-circle text-danger fa-2x"></i>
											</div>
										</div>
									</div>
								</div>

								<div class="col-12 col-sm-12 col-md-3 p-2">
									<div class="mb-3">
										<div class="bg-light" style="min-height: 20rem;">
											<form action="" method="post" enctype="multipart/form-data" id="upldForm3" name="upldForm3">
												<div class="custom-file">
													<input type="file" class="custom-file-input customfileinput" id="upld_img3" name="upld_img3" onchange="uploadFile()">
													<label class="custom-file-label customfilelabel text-center" for="upld_img3"><i class="fas fa-upload fa-3x"></i><br><br>Drop the file to upload or <i><u>browse</u></i>.</label>
												</div>
											</form>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				-->
			</main>
		</div>

	</div>
</div>

<script>
$(".upload_brief_files").on("change", function(e) {
	alert('hi');
		$("#loader").show();
		var files = e.target.files,
		filesLength = files.length;
		var f = files[0];
		var fileReader = new FileReader();
		fileReader.onload = (function(e) {
			var file = e.target;

			console.log(e.target.result);

			var input = document.getElementById("upld_img1");
			var inputArr=input.files;
			var check_complete='';
			for(var k=0;k<inputArr.length;k++)
			{
				var file=inputArr[k];

				console.log("--------------");
				console.log(file);
				console.log(file.type);
				console.log("--------------");

				if(file != undefined && (file.type=='image/png' || file.type=='image/jpg'   || file.type=='image/jpeg') ){
					formData= new FormData();
					formData.append("image", file);

					var data = $.ajax({
						url: "<?php echo base_url();?>front/labelimages/upload_files_fromsku",
						type: "POST",
						data: formData,
						processData: false,
						contentType: false,
						async: false
					}).responseText;

					if($.trim(data)=='sessionout') {
						$("#loader").hide();
						// alert("Session out. Please login first!.");
						$('#modal_msg').modal('show');
						$('#txtMsg').html('Session out!<br>Please do the login first.');
						window.location.href="<?php echo base_url();?>home";
						return false;
					} else if($.trim(data)=='notsupport') {
						$("#loader").hide();
						// alert("There is some issue with image format!.");
						$('#modal_msg').modal('show');
						$('#txtMsg').html('There is some issue with image format!.');
						window.location.href="<?php echo base_url();?>uploadimage";
						return false;
					} else {
						var pimgVal=$.trim($("#pimg").val());
						if(pimgVal!='') {
							var item_chek=","+$.trim(data)+",";
							var check=(pimgVal.indexOf(item_chek));
							var pimgValItem=pimgVal+$.trim(data)+",";
						} else {
							var pimgValItem=","+$.trim(data)+",";
						}

						$("#pimg").val(pimgValItem);

						var pimgValCheckCount=$.trim($("#pimg").val());
						var pimgValCheckCount = pimgValCheckCount.replace(/^,|,$/g, '');

						var checkArr=pimgValCheckCount.split(",");
						var checkLength=checkArr.length;

						if(checkLength<=10) {
							var show_pimg="<div class='col-6 col-sm-6 col-md-3 col-lg-3 col-xl-3'><img class='w-100 border rounded mb-2' src='temp_upload/<?php echo $user_id;?>/"+"thumb_"+data+"' ></div>";

							$("#skuImageUploadDiv").append(show_pimg);

							$("#div_ctn_img").removeClass("d-none");
							$("#ctn_img").html(checkLength);
						} else {
							// alert("You exceed your limit. Only 10 images you can upload at a time");
							$('#modal_msg').modal('show');
							$('#txtMsg').html('You exceed your limit. Only 10 images you can upload at a time.');
						}

						var lastCount=parseInt(inputArr.length)-1;

						if(k==lastCount) {
							var ctn=0;
							$("img").on('load', function(){

								if (this.complete) {
									console.log("completed");

									var browname=myCheckBrowserFunction();
									console.log(browname);

									if(browname=='Chrome') {
										if(ctn==lastCount) {
											$("#loader").hide();
										} else {
											ctn++;
										}
									} else {
										$("#loader").hide();
									}
								} else {
									$(this).on('load', function() { // image now loaded
									});
								}
							});
						}
					}
				} else {
					$("#loader").hide();
					 alert('Uploaded file is not an image!');
					$('#modal_msg').modal('show');
					$('#txtMsg').html('Uploaded file is not an image!');
					window.location.href="<?php echo base_url();?>uploadimages/<?php echo $brief_id; ?>";
				}
			}
		});

		fileReader.readAsDataURL(f);

		$('#upldImg').hide();
		$('#btnCretSKU').removeClass('d-none');
	});



</script>

<?php
$this->load->view('front/includes/footer');
?>
