<table class="table border-bottom text-wc" >
                        <thead>
                          <tr>
                            <th scope="col"></th>
                            <th scope="col">User Id</th>
                            <th scope="col">User Type Id</th>
                            <th scope="col">Client Name</th>
                            <th scope="col">Brand Name</th>
                            <th scope="col">User Name</th>
                            <th scope="col">User Email</th>
                            
                            <th scope="col">User Telephone</th>
                            <th scope="col">User Mobile</th>
                            <th scope="col">Status</th>
                          
                          </tr>
                        </thead>
                        <tbody >

                        <?php
                        $session_data = $this->session->userdata('front_logged_in');
                        $user_id= $session_data['user_id'];
                        $users_details=$this->db->query("select * from wc_users where user_id='$user_id'")->result_array();
                        $client_id=$users_details[0]['client_id'];
                        $users_details1=$this->db->query("select * from wc_users where (user_type_id!='1' and user_type_id!='2' ) and deleted='0' and client_id in ( ".trim($client_id,",")." ) OR client_id LIKE '%,".trim($client_id,",").",%'  order by  user_id asc ")->result_array();
                        //print_r("select * from wc_users where (user_type_id!='1' and user_type_id!='2' ) and deleted='0' and client_id in ( ".trim($client_id,",")." ) OR client_id LIKE '%,".trim($client_id,",").",%'  ");
                        foreach($users_details1 as $key => $users_value){ 
                          $user_id1=$users_value['user_id'];
                          $user_type_id=$users_value['user_type_id'];
                          $client_id=$users_value['client_id'];
                          $brand_id=$users_value['brand_id'];
                          $user_name=$users_value['user_name'];
                          $user_email=$users_value['user_email'];
                          $user_telephone=$users_value['user_telephone'];
                          $user_mobile=$users_value['user_mobile'];
                          $status=$users_value['status'];
                          
                        ?>
                      <tr>
                        <th scope="row"><span id="edit_<?php echo $user_id1 ?>" data-toggle="modal" data-target="#usereditModel<?php echo $user_id1; ?>"><i class="far fa-edit"></i></span> <span id="delete_<?php echo $user_id1 ?>" data-toggle="modal" data-target="#userdeleteModel_<?php echo $user_id1; ?>"><i class="far fa-trash-alt"></i></span></th>
                        <td><?php echo $user_id1 ?></td>
                        <td><?php 
                            $users_type_details=$this->db->query("select * from wc_users_type where user_type_id='$user_type_id'  ")->result_array();
                            $user_type_name=$users_type_details[0]['user_type_name'];

                            echo $user_type_name; 
                            ?></td>
                        <td><?php
                          $sql="select * from wc_clients where client_id in ( ".trim($client_id,",")." ) OR client_id LIKE '%,".trim($client_id,",").",%'";
                          $client_name_details=$this->db->query($sql)->result_array();

                          $all_client_name ="<ul style=' margin-left: -25px;'>";
                          if(!empty($client_name_details)){
                            foreach($client_name_details as $key => $value){
                              $all_client_name.="<li>".$value['client_name']."</li>";
                            }
                            $all_client_name.="</ul>";
                             echo $all_client_name;
                          }
                        ?></td>
                        <td><?php
                          if($user_type_id=='3'){
                            $brand_details=$this->db->query("select * from wc_brands where client_id ='$client_id'")->result_array();
                            $brand_name_all="<ul style=' margin-left: -25px;'>";
                            foreach($brand_details as $key=>$value){
                              $brand_name_arr[]=$value['brand_name'];
                              $brand_name_all.="<li>".$value['brand_name']."</li>";
                            }
                            $brand_name_all.="</ul>";
                          }elseif ($brand_id!=="") {
                             $brand_details=$this->db->query("select * from wc_brands where brand_id in ( ".trim($brand_id,",")." ) OR brand_id LIKE '%,".trim($brand_id,",").",%'")->result_array();
                            // echo "select * from wc_brands where brand_id in ( ".trim($brand_id,",")." ) OR brand_id LIKE '%,".trim($brand_id,",").",%' order by brand_name asc ";
                              $brand_name_all="<ul style=' margin-left: -25px;'>";
                              foreach($brand_details as $key=>$value){
                                $brand_name_arr[]=$value['brand_name'];
                                $brand_name_all.="<li>".$value['brand_name']."</li>";
                              }
                              $brand_name_all.="</ul>";
                          }elseif ($brand_id==""){
                            $brand_name_all="";
                          }
                          echo $brand_name_all;
                        ?></td>
                        <td><?php echo $user_name ?></td>
                        <td><?php echo $user_email ?></td>
                      
                        <td><?php echo $user_telephone ?></td>
                        <td><?php echo $user_mobile ?></td>
                        <td><?php echo $status ?></td>
                       
                        <div class="modal fade" id="usereditModel<?php echo $user_id1; ?>" tabindex="-1" role="dialog" aria-labelledby="usereditModel<?php echo $user_id1; ?>Label" aria-hidden="true">
                          <div class="modal-dialog" role="document">
                            <div class="modal-content">
                                <?php  $array = array('id'=>'User_form'.$user_id1, 'role'=>'form', 'class'=>'form  col-sm-12 form-horizontal has-validation-callback');
                                  echo form_open_multipart('User/insert_user', $array); ?>
                              <div class="modal-header">
                                <h5 class="modal-title" id="usereditModel<?php echo $user_id1; ?>Label">Update User Details</h5>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                  <span aria-hidden="true">&times;</span>
                                </button>
                              </div>
                              <div class="modal-body">
                                <div class="form-group">
                                  <label for="product_name" class="col-form-label">User Type</label>
                                  <input type='hidden' name='user_id<?php echo $user_id1; ?>' value='<?php echo $user_id1;?>' id='user_id<?php echo $user_id1; ?>'>
                                  <select name="user_type_id<?php echo $user_id1; ?>" id="user_type_id<?php echo $user_id1; ?>" class="custom-select"  onchange="selectUserType1();" disabled style="width: 100%" >
                                     <option value=''>Select</option>
                                    <?php
                                    $session_data = $this->session->userdata('front_logged_in');
                                  
                                    //$user_name= $session_data['user_name'];
                                    $users_type_details=$this->db->query("select * from wc_users_type where user_type_id!='1' and user_type_id!=2 and deleted='0' and status='Active' ")->result_array();
                                    if(!empty($users_type_details))
                                    {
                                      foreach($users_type_details as $key => $userstypedetails)
                                      {
                                        $user_type_id1=$userstypedetails['user_type_id'];
                                        $user_type_name=$userstypedetails['user_type_name'];
                                       // echo $user_type_id1."==". $user_type_id;
                                      ?>
                                      <option value="<?php echo $user_type_id1;?>"  <?php if($user_type_id1==$user_type_id) { ?> selected="selected" <?php } ?>><?php echo $user_type_name;?></option>
                                      <?php
                                      }
                                    }
                                    ?>
                                    
                                     </select>
                                  <span class="small text-danger" id="errmsg_product_name"></span>
                                </div>
                                
                                <div id="divClientBrand<?php echo $user_id1; ?>" style="display:none; ">
                                  <input type='text' name='client_id<?php echo $user_id1; ?>' value='0' id='client_id<?php echo $user_id1; ?>'>
                                  <input type='text' name='brand_id<?php echo $user_id1; ?>' value='0' id='brand_id<?php echo $user_id1; ?>'>
                                </div>
                                <div class="form-group">
                                  <label for="product_description" class="col-form-label">User Name</label>
                                  <input  type='text'  class="form-control" id="user_name<?php echo $user_id1; ?>" name="user_name<?php echo $user_id1; ?>" value='<?php echo $user_name; ?>'>
                                  <span class="small text-danger" id="errmsg_product_description"></span>
                                </div>
                                <div class="form-group">
                                  <label for="product_price" class="col-form-label">User Email</label>
                                   <input  type='text'  class="form-control" id="user_email<?php echo $user_id1; ?>" name="user_email<?php echo $user_id1; ?>" value='<?php echo $user_email;?>'>
                                  
                                  <span class="small text-danger" id="errmsg_product_price"></span>
                                </div>
                               <!--  <div class="form-group">
                                  <label for="product_price" class="col-form-label">User Password</label>
                                   <input  type='text'  class="form-control" id="user_password" name="user_password" value='<?php echo $user_password;?>'>
                                  <span class="small text-danger" id="errmsg_product_price"></span>
                                </div>
                                <div class="form-group">
                                  <label for="product_price" class="col-form-label">User Confirm Password</label>
                                  <input  type='text'  class="form-control" id="user_cpassword" name="user_cpassword">
                                  <span class="small text-danger" id="errmsg_product_price"></span>
                                </div> -->
                                <div class="form-group">
                                  <label for="product_price" class="col-form-label">User Mobile</label>
                                  <input  type='text'  class="form-control" id="user_mobile<?php echo $user_id1; ?>" name="user_mobile<?php echo $user_id1; ?>" value='<?php echo $user_mobile;?>'>
                                  <span class="small text-danger" id="errmsg_product_price"></span>
                                </div>
                                <div class="form-group">
                                  <label for="product_price" class="col-form-label">Status</label>
                                 <select name="status<?php echo $user_id1; ?>" id="status<?php echo $user_id1; ?>" class="form-control" >
                                   <!--<option value=''>Select</option>-->
                                   <option value="Active" <?php if($status=='Active') { ?> selected="selected" <?php } ?>>Active</option>
                                   <option value="Inactive" <?php if($status=='Inactive') { ?> selected="selected" <?php } ?>>Inactive</option>
                                   </select>
                                  <span class="small text-danger" id="errmsg_product_price"></span>
                                </div>
                                
                              </div>
                              <!-- <div class="modal-footer">
                                <button type="submit" class="btn btn-outline-wc" id="edit_product<?php echo $user_id1 ?>" onclick='edit_product("<?php echo $user_id1 ?>")' data-dismiss="modal">&emsp;Update&emsp;</button>
                                <button type="button" class="btn btn-outline-wc" data-dismiss="modal">&emsp;Close&emsp;</button>
                                
                              </div> -->
                              <div class="modal-footer">
        <input class="m-1 btn btn-wc" type="submit" value="Update">
        <button type="button" class="btn btn-outline-wc" data-dismiss="modal">&emsp;Close&emsp;</button>
      </div>
       <?php echo form_close();?> 
                            </div>
                          </div>
                        </div>
                     <script type="text/javascript">
            </script>
                        

                    </tr>
                    <div class="modal fade" id="userdeleteModel_<?php  echo $user_id1; ?>" tabindex="-1" role="dialog" aria-hidden="true">
                            <div class="modal-dialog" role="document">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <h5 class="modal-title text-dark">Are you sure you want to delete?</h5>
                                        <button type="button" class="close text-danger" data-dismiss="modal" aria-label="Close">
                                            <span aria-hidden="true" >&times;</span>
                                        </button>
                                    </div>
                                    <div class="modal-footer">
                                      <button type="button" id="delUsr" class="btn btn-outline-wc" onclick="delete_user('<?php  echo $user_id1; ?>');" data-dismiss="modal">&emsp;Delete&emsp;</button>
                                        <button type="button" class="btn btn-outline-wc" data-dismiss="modal">&emsp;Close&emsp;</button>
                                        
                                    </div>
                                </div>
                            </div>
                        </div>
                        <script type="text/javascript">
                          selectUserType1('<?php echo $user_id1; ?>','<?php echo $client_id; ?>','<?php echo $brand_id; ?>');

                           // selectUserType1('<?php echo $user_id1; ?>','<?php echo $client_id; ?>','<?php echo $brand_id; ?>');
     jQuery(function ($) {
    //$('#user_type_id').select2();

    jQuery.validator.addMethod("alphanumeric", function(value, element) {
        return this.optional(element) || /^[a-zA-Z\-\s]+$/.test(value);
        }, "It must contain only letters."); 
    
    jQuery.validator.addMethod("atLeastOneLetter", function (value, element) {
    return this.optional(element) || /[a-zA-Z]+/.test(value);
    }, "Must have at least one  letter");
    
    jQuery.validator.addMethod("phoneno", function(value, element) {
        return this.optional(element) || /^[0-9+() ]*$/.test(value);
        }, "Enter Valid  phone number."); 
    
    
    
    $('#User_form'+'<?php echo $user_id1; ?>').validate({
    
      rules: {
        user_type_id: {
          required: true,
        },

        "client_id[]":{
          required:true,
        },
        "brand_id[]":{
          required:true,
        },
        user_name:{
          required:true,
          minlength: 2,
        },
        user_email:{
          required:true,
          email: true,
          remote: {
            url: "<?php echo base_url();?>front/Settings/checkexistemail",
            type: "post"
          },
        
        },
      
        
        user_password:{
          required:true,
          minlength:5,
        },
        user_cpassword: {
          required:true,
          equalTo: "#user_password" ,
          minlength:5,
        },
      
        user_mobile: {
          required: true,
          minlength:10,
          phoneno: true,
          remote: {
            url: "<?php echo base_url();?>front/Settings/checkexistmobile",
            type: "post"
           },
        },
    
        status: {
          required: true,
        }
        
      },
      messages: {
        user_email:{
          remote:"This email already exists"
        },
        user_mobile: {
          digits: "This field can only contain numbers.",
          /*maxlength: "this must be 10 digit number.",*/
          remote:"This mobile already exists",
        },
        <?php  if($user_id=='') { ?>
          user_cpassword: {
            equalTo:"password not match",
          },
        <?php } ?>   
      },
      submitHandler: function(form) {
      //alert("Asd");
        $("#loader").show();
        var user_id=$('#user_id'+<?php echo $user_id1; ?>).val();
        var user_type_id=$('#user_type_id'+<?php echo $user_id1; ?>).val();
        
        if(user_type_id=='6'){
          var client_id_all = [];
          var client_id_list = $('input:checkbox:checked.client_id_class').map(function(){
            client_id_all.push(this.value);
          return this.value; }).get().join(",");

          if($.trim(client_id_all)!=''){
            var client_id =","+client_id_all+",";
          }else{
            var client_id ="";
          }
          //console.log(client_id);
        }else{
          var client_id=$('#client_id'+<?php echo $user_id1; ?>).val();
        }
        var brand_id_all = [];
        var brand_id_list = $('input:checkbox:checked.brand_id_class').map(function(){
        brand_id_all.push(this.value);
        return this.value; }).get().join(",");

        if($.trim(brand_id_all)!=''){
          var brand_id =","+brand_id_all+",";
        }else{
          var brand_id ="";
        }
        //alert(brand_id);
      //console.log(brand_id);
        var user_name=$('#user_name'+<?php echo $user_id1; ?>).val();
        var user_email=$('#user_email'+<?php echo $user_id1; ?>).val();
        var user_mobile=$('#user_mobile'+<?php echo $user_id1; ?>).val();
        var user_password=$('#user_password'+<?php echo $user_id1; ?>).val();
        var status=$('#status'+<?php echo $user_id1; ?>).val();
        //alert("&user_type_id="+user_type_id+"&client_id="+client_id+"&brand_id="+brand_id+"&user_id="+user_id+"&user_name="+user_name+"&user_email="+user_email+"&user_mobile="+user_mobile+"&user_password="+user_password+"&status="+status);
        $.ajax({
          method:'post',
          url: "<?php echo base_url();?>front/Settings/insert_user", 
          data:"&user_type_id="+user_type_id+"&client_id="+client_id+"&brand_id="+brand_id+"&user_id="+user_id+"&user_name="+user_name+"&user_email="+user_email+"&user_mobile="+user_mobile+"&user_password="+user_password+"&status="+status,
          cache: true,
          timeout: 5000
        }).done(function( result ) {
          console.log( "SUCCESS: " + result );
          $('#loader').hide();
         // $('#User_form'+<?php echo $user_id1; ?>).trigger("reset");
         //  $('#user_details_table').html("");
         // $('#user_details_table').html(result);
            var url="<?php echo base_url(); ?>settings#user-details";
              window.location.replace(url);
              location.reload();
      //onload_user();
     // selectUserType1('<?php echo $user_id1; ?>','<?php echo $client_id; ?>','<?php echo $brand_id; ?>');
          
        
        }).fail(function() {
          //console.log( "Request failed");
        //  $('#User_form'+<?php echo $user_id1; ?>).trigger("reset");
         // $('#loader').hide();
          

    var url="<?php echo base_url(); ?>settings#user-details";
              window.location.replace(url);
              location.reload();
          //$("#success_message").show();
        }); 
      } 
    });
  });
                        </script>
                        
                    
<?php }
?>
</tbody>
                      </table>

