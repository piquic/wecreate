<?php
session_start();
error_reporting(0);
 
if ($this->session->userdata('front_logged_in')) {
	$session_data = $this->session->userdata('front_logged_in');
	$user_id = $session_data['user_id'];
	$user_name = $session_data['user_name'];
	$_SESSION['user_id']=$user_id;
	$_SESSION['user_name']=$user_name;
	$type1 = $session_data['user_type_id'];
	$user_type_id=$type1;
	$brand_access=$session_data['brand_access'];
 
	$checkquerys = $this->db->query("select user_type_name from wc_users_type where user_type_id='$user_type_id' ")->result_array();
	$job_title=$checkquerys[0]['user_type_name'];
	$client_access=$session_data['client_access'];

	$checkquerys = $this->db->query("select admin_mail_notifications,client_id from wc_users where user_id='$user_id' ")->result_array();
	$admin_mail_notifications=$checkquerys[0]['admin_mail_notifications'];
	$client_id=$checkquerys[0]['client_id'];


	$brief_permission_details=$this->db->query("select * from wc_users where client_id=$client_id and user_type_id='3'")->result_array();
	//print_r($brief_permission_details);
	$brief_permission=$brief_permission_details[0]['brief_permission'];
	$briefassets_permission=$brief_permission_details[0]['briefassets_permission'];
	$user_permission=$brief_permission_details[0]['user_permission'];
		if($user_type_id=='3' && $user_permission=='1'){

			?>
			<script type="text/javascript">
				var url="<?php echo base_url(); ?>settings#user-details";
          		window.location.replace(url);
			</script>
			<?php
		}

}
else
{
	$user_id = '';
	$user_name = ''; 
	$_SESSION['user_id']=$user_id;
	$_SESSION['user_name']=$user_name;
}
?>

<?php $data['page_title'] = "Settings";

$this->load->view('front/includes/header',$data);
$this->load->view('front/includes/topnav');
?>
<script src="<?php echo base_url(); ?>assets/js/jquery.validate.min.js"></script>
<style type="text/css">
	.errmsg{
		color:red;
	}
	#divClientBrand{
		width: 100%;
	}
</style>
<script type="text/javascript">
	$(document).ready(function() {

  	var pageURL = $(location).attr("href");
		var values=$.trim(pageURL.split("settings")[1]);
		console.log(values);
		if(values=="#user-details"){
			 $("#user-details-tab").tab('show');
		}
		else{
			 $("#general-tab").tab('show');
		}

});
	 function selectUserType1(user_id1,client_id,brand_id)
  {
    //console.log("selectUserType1");
    //var user_id1='<?php echo $user_id1; ?>';
    var user_type_id=$('#user_type_id'+user_id1).val();
    // var client_id='<?php echo $client_id;?>';
    // var brand_id='<?php echo $brand_id;?>';
    //console.log(user_type_id);
    if(user_type_id==""){
     // console.log(user_type_id);
      var hid_html="<input type='text' name='client_id"+user_id1+"' value='0' id='client_id"+user_id1+"'><input type='text' name='brand_id"+user_id1+"' value='0' id='brand_id"+user_id1+"'>";
      $('#divClientBrand'+user_id1).html(hid_html);
      $('#divClientBrand'+user_id1).hide();

    }
    else if(user_type_id=='3' ||  user_type_id=='5'){
     // console.log(user_type_id);
      $.ajax({
        method:'post',
        url: "<?php echo base_url();?>front/Settings/showDivClient_user", 
        data:"&user_type_id="+user_type_id+"&client_id="+client_id+"&brand_id="+brand_id+"&user_id="+user_id1+"&user_type_id="+user_type_id,
        success: function(result){
          if(result){
            $('#divClientBrand'+user_id1).html(result);
            $('#divClientBrand'+user_id1).show();
            //$('#client_id').select2();
          }
        }
      });
    }
    else if( user_type_id=='6'){
    // console.log(user_type_id);
      $.ajax({
        method:'post',
        url: "<?php echo base_url();?>front/Settings/showDivClientMultiple_user", 
        data:"&user_type_id="+user_type_id+"&client_id="+client_id+"&brand_id="+brand_id+"&user_id="+user_id1+"&user_type_id="+user_type_id,
        success: function(result){
          if(result){
            $('#divClientBrand'+user_id1).html(result);
            $('#divClientBrand'+user_id1).show();
            //$('#client_id').select2();
          }
        }
      });
    }
    else if( user_type_id=='4' ){
     //console.log(user_type_id);
      $.ajax({
        method:'post',
        url: "<?php echo base_url();?>front/Settings/showDivClientBrand_user", 
        data:"&user_type_id="+user_type_id+"&client_id="+client_id+"&brand_id="+brand_id+"&user_id="+user_id1+"&user_type_id="+user_type_id,
        success: function(result){
          if(result){
            $('#divClientBrand'+user_id1).html(result);
            $('#divClientBrand'+user_id1).show();
            //$('#client_id').select2();
             //selectClient1(user_id1,brand_id);
          	if(user_type_id==4){
              selectClient1(user_id1,brand_id);
          	}
            
          }
        }
      });
    }
  }



  function selectClient1(user_id1,brand_id)
  {
   // console.log("selectClient1");
    //var user_id1='<?php echo $user_id1; ?>';

    var user_type_id=$('#user_type_id'+user_id1).val();
    var client_id=$('#client_id'+user_id1).val();
   // alert(user_type_id);
    // var brand_id='<?php echo $brand_id;?>';
    if(user_type_id=='4'){
      $.ajax({
        method:'post',
        url: "<?php echo base_url();?>front/Settings/showDivBrand_user", 
        data:"&client_id="+client_id+"&brand_id="+brand_id+"&user_id="+user_id1+"&user_type_id="+user_type_id,
        success: function(result){
        
          if(result){

            $('#brandDiv'+user_id1).html(result);
           
            $('#brandDiv'+user_id1).show();
          }
        }
      });
    }
  }
</script>
<div class="container-fluid">
	<div class="row">

		<!-- Main Body -->
		<div class="col-12 col-sm-12 col-md-12">
			<main class="p-3">

				<!-- <div class="row">
					<div class="col-12 col-sm-12 col-md-12">
						<div class="px-3">
							<a href="<?php echo base_url() ?>dashboard"><p class="text-wc lead"><i class="fas fa-arrow-alt-circle-left"></i>&nbsp;Back to Dashboard</p></a>
						</div>
					</div>
				</div> -->

				<div class="row">
					<div class="col-12 col-sm-12 col-md-12">
						<div class="px-3 text-wc">
							<h2>Settings</h2>
						</div>
					</div>
				</div>

				<!-- <hr class="text-center w-50 "> -->

				<div class="row">
					<div class="col-12 col-sm-12 col-md-12">
						<div class="px-3 py-2">
							<ul class="nav nav-tabs flex-column flex-sm-row text-center" id="myTab" role="tablist">
								<?php if($user_type_id=='2'){ ?>
								<li class="nav-item">
									<a class="nav-link text-wc text-uppercase" id="general-tab" data-toggle="tab" href="#general" role="tab" aria-controls="general" aria-selected="true">General</a>
								</li>
								<?php  }  if($user_type_id=='2'){ ?>
								<li class="nav-item">
									<a class="nav-link text-wc text-uppercase" id="items-tab" data-toggle="tab" href="#items" role="tab" aria-controls="items" aria-selected="false">Items</a>
								</li> 
								<?php  }  if($user_type_id=='2'){ ?>
								<li class="nav-item">
									<a class="nav-link text-wc text-uppercase" id="permission-tab" data-toggle="tab" href="#permission" role="tab" aria-controls="permission" aria-selected="false">Permission</a>
								</li>
								<?php  } 
									

								if($user_type_id=='2' || ( $user_type_id=='3' && $user_permission=='1')){ ?>
								<li class="nav-item">
									<a class="nav-link text-wc text-uppercase" id="user-details-tab" data-toggle="tab" href="#user-details" role="tab" aria-controls="user-details" aria-selected="false">User List</a>
								</li>
							<?php  }  ?>
								<!-- /*<?php if($type1==2){ ?>
								<li class="nav-item">
									<a class="nav-link text-wc text-uppercase" id="notifications-tab" data-toggle="tab" href="#notifications" role="tab" aria-controls="notifications" aria-selected="false">Email Notifications</a>
								</li>
								<?php } ?>*/ -->
							</ul>
						</div>
						<div class="px-3 py-2">
							<div class="tab-content" id="myTabContent">

								<div class="tab-pane fade" id="general" role="tabpanel" aria-labelledby="general-tab">
									<div class="p-4">
										<div class="custom-control custom-checkbox">
											<input type="checkbox" class="custom-control-input" id="chkActivateLink">
											<label class="custom-control-label roundCheck" for="chkActivateLink">Activate Link Sharing</label>
										</div>
									</div>
									<hr>

									<style type="text/css">
										.tox-statusbar__branding { display: none; }
									</style>

									<div class="p-4">
										<p class="h4 mb-4">Project Description</p>
										<?php 
										$checkquerys = $this->db->query("select * from wc_clients where client_id='$client_access' ")->result_array();
										
									    $project_description=$checkquerys[0]['project_description'];
									    ?>
										<div class="form-group">
											<textarea id="project_description" name="project_description"><?php echo $project_description ?></textarea>
											<span class="small text-danger" id="errmsg_project_description"></span>
											<input type="hidden" id="client_id" name="client_id" value="<?php echo $client_access ?>">
										</div>
										<button type="button" class="btn btn-wc" id="update_project_desc">Submit</button>
									</div>
								</div>

								<div class="tab-pane fade" id="items" role="tabpanel" aria-labelledby="items-tab">
									<div class="p-4">
										<div class="d-flex justify-content-between flex-wrap">
											<div class="p-1">
												<span class="text-wc lead" data-toggle="modal" data-target="#addModel"><i class="far fa-plus-square"></i>&nbsp;Add Item</span>
											</div>
											<?php 
											$checkquerys = $this->db->query("select * from wc_users where user_id='$user_id' ")->result_array();
											$add_discount=$checkquerys[0]['add_discount'];
											$add_extra_charges=$checkquerys[0]['add_extra_charges'];
											if($add_discount=='0'){ 
												$add_discount_status='1'; 
											}else if($add_discount=='1'){ 
												$add_discount_status='0'; 
											}	
											if($add_extra_charges=='0'){ 
												$add_extra_charges_status='1'; 
											}else if($add_extra_charges=='1'){ 
												$add_extra_charges_status='0'; 
											}	
											?>
											<div class="d-flex justify-content-end flex-wrap">

												<div class="p-1 pt-2 pr-2">
													<div class="custom-control custom-checkbox">
														<input type="checkbox" <?php if($add_discount=='1'){ echo "checked"; } ?> class="custom-control-input" id="chkDiscount" onclick="add_discount('<?php echo $user_id; ?>','<?php echo $add_discount_status; ?>')">
														<label class="custom-control-label" for="chkDiscount">Add Discount</label>
													</div>
												</div>

												<div class="p-1 pt-2 pr-2">
													<div class="custom-control custom-checkbox">
														<input type="checkbox" <?php if($add_extra_charges=='1'){ echo "checked"; } ?> class="custom-control-input" id="chkEC" onclick="add_extra_charges('<?php echo $user_id; ?>','<?php echo $add_extra_charges_status; ?>')">
														<label class="custom-control-label" for="chkEC">Add Extra Charge</label>
													</div>
												</div>
											</div>
										</div>
										<div class="table-responsive" id="product_details_table">
											<?php $this->load->view('front/settings_list', $product_details);   ?>
												
										</div>
									</div>
								</div>
								<div class="tab-pane fade" id="permission" role="tabpanel" aria-labelledby="general-tab">
									<div class="p-4">
										<?php 
										
										
										$user_type_id=$brief_permission_details[0]['user_type_id'];
										$brand_details=$this->db->query("select brand_id from wc_brands where deleted='0' and status='Active' and client_id='$client_id' order by  brand_name asc")->result_array();
										$brand_id_arr="";				
										if(!empty($brand_details))
										{
											//print_r($brand_details);
											foreach($brand_details as $key => $branddetails)
											{
												$brand_id=$branddetails['brand_id'];
												//$brand_id_arr.=trim($brand_id,",");
												$brand_id_arr.=",".$brand_id;
											}
										}
								   		$brand_id_arr=$brand_id_arr.",";
										?>
										<div class="custom-control custom-checkbox">
											<input type="checkbox" class="custom-control-input" <?php if($brief_permission==1){ echo "checked"; } ?>  id="upload_brief_pm">
											<label class="custom-control-label roundCheck" for="upload_brief_pm">Enable Brief Upload for PM</label>											
										</div>

										<div class="custom-control custom-checkbox">
											
											<input type="checkbox" class="custom-control-input" <?php if($briefassets_permission==1){ echo "checked"; } ?>  id="status_brief_pm">
											<label class="custom-control-label roundCheck" for="status_brief_pm">Enable PM to Approve or Reject Assets</label>
										</div>
										<div class="custom-control custom-checkbox">
											
											<input type="checkbox" class="custom-control-input" <?php if($user_permission==1){ echo "checked"; } ?>  id="status_user_pm">
											<label class="custom-control-label roundCheck" for="status_user_pm">Enable PM to Create User</label>
										</div>
										<hr>
										<button id="btn_brief_permission" type="button" class="btn btn-wc" onclick="brief_upload_permission('<?php echo $client_id; ?>','<?php echo $user_type_id; ?>','<?php echo $brand_id_arr ?>')">Save</button>
									</div>
									
								</div>

								<div class="tab-pane fade" id="notifications" role="tabpanel" aria-labelledby="notifications-tab">
									<div class="p-4">
										<div class="custom-control custom-checkbox">
											<input type="checkbox" class="custom-control-input" id="chkActivatenotification" <?php if($admin_mail_notifications==1){ echo "checked"; } ?> onclick="return activate(<?php echo $user_id; ?>)">
											<label class="custom-control-label roundCheck" for="chkActivatenotification">Activate Board Email Notification</label>
										</div>
									</div>
									<hr>
								</div>

								<div class="tab-pane fade" id="user-details" role="tabpanel" aria-labelledby="general-tab">
									<div class="p-4">
										<div class="d-flex justify-content-between flex-wrap">
											<div class="p-1">
												<span class="text-wc lead" data-toggle="modal" data-target="#create_user_Model"><i class="far fa-plus-square"></i>&nbsp;Create User</span>
											</div>
										</div>
										<div class="table-responsive" id="user_details_table">
											<?php $this->load->view('front/settings_user_list');   ?>
												
										</div>
									</div>
								</div>


							</div>
						</div>
					</div>
				</div>
			</main>
		</div>

	</div>
</div>



<div class="modal fade" id="addModel" tabindex="-1" role="dialog" aria-labelledby="addModelLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
    
      <div class="modal-header">
        <h5 class="modal-title" id="addModelLabel">Add Product Details</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        
          <div class="form-group">
            <label for="product_name" class="col-form-label">Product Name</label>
            <input type="text" class="form-control" id="product_name" name="product_name" required >
            <span class="small text-danger" id="errmsg_product_name"></span>
          </div>
          <div class="form-group">
            <label for="product_description" class="col-form-label">Product Description</label>
            <textarea class="form-control" id="product_description" name="product_description" required ></textarea>
            <span class="small text-danger" id="errmsg_product_description"></span>
          </div>
          <div class="form-group">
            <label for="product_price" class="col-form-label">Product Price</label>
            <input type="text" class="form-control" id="product_price" name="product_price" required >
            <span class="small text-danger" id="errmsg_product_price"></span>
          </div>

       
      </div>
      <div class="modal-footer">
      	<button type="button" class="btn btn-outline-wc" id="add_product">&emsp;Add&emsp;</button>
        <button type="button" class="btn btn-outline-wc" data-dismiss="modal">&emsp;Close&emsp;</button>
        
      </div>
       
    </div>
  </div>
</div>


<div class="modal fade" id="create_user_Model" tabindex="-1" role="dialog" aria-labelledby="create_user_Label" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
    	<?php  $array = array('id'=>'User_form', 'role'=>'form', 'class'=>'form  col-sm-12 form-horizontal has-validation-callback');
				    				echo form_open_multipart('User/insert_user', $array); ?>
      <div class="modal-header">
        <h5 class="modal-title" id="create_user_Label">Add User Details</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        
				<div class="form-group">
          <label for="product_name" class="col-form-label">User Type</label>
          <input type='hidden' name='user_id' id='user_id'>
         	<select name="user_type_id" id="user_type_id" class="custom-select"  onchange="selectUserType();" style="width: 100%" >
						 <option value=''>Select</option>
						<?php
						$session_data = $this->session->userdata('front_logged_in');
            $user_id= $session_data['user_id'];
            $user_type_id = $session_data['user_type_id'];
            $user_name= $session_data['user_name'];
						$users_type_details=$this->db->query("select * from wc_users_type where user_type_id!='1' and user_type_id!=2 and deleted='0' and status='Active' ")->result_array();
						if(!empty($users_type_details))
						{
							foreach($users_type_details as $key => $userstypedetails)
							{
							$user_type_id=$userstypedetails['user_type_id'];
							$user_type_name=$userstypedetails['user_type_name'];
							?>
							<option value="<?php echo $user_type_id;?>"><?php echo $user_type_name;?></option>
							<?php
							}
						}
						?>
						
						 </select>
          <span class="small text-danger" id="errmsg_product_name"></span>
        </div>
        <div id="divClientBrand" style="display:none; ">
          <input type='text' name='client_id' value='0' id='client_id'>
          <input type='text' name='brand_id' value='0' id='brand_id'>
        </div>
         <div class="form-group">
            <label for="product_price" class="col-form-label">Country</label>
            <select name="user_country" id="user_country" class="custom-select"  style="width: 100%" >
            	<option value=''>Select</option>
	            <?php
	            $country_details=$this->db->query("select * from wc_country where 1=1 ")->result_array();
	            if(!empty($country_details))
	            {
		            foreach($country_details as $key => $countrydetails)
		            {
		            $country_id=$countrydetails['country_id'];
		            $country_name=$countrydetails['country_name'];
		            ?>
		            <option value="<?php echo $country_id;?>" ><?php echo $country_name;?></option>
		            <?php

		            }
	            }
	            ?>
            
            </select>
       </div>
        <div class="form-group">
          <label for="product_description" class="col-form-label">User Name</label>
          <input  type='text'  class="form-control" id="user_name" name="user_name">
          <span class="small text-danger" id="errmsg_product_description"></span>
        </div>
        <div class="form-group">
          <label for="product_price" class="col-form-label">User Email</label>
           <input  type='text'  class="form-control" id="user_email" name="user_email">
          
          <span class="small text-danger" id="errmsg_product_price"></span>
        </div>
        <div class="form-group">
          <label for="product_price" class="col-form-label">User Password</label>
           <input  type='text'  class="form-control" id="user_password" name="user_password">
          <span class="small text-danger" id="errmsg_product_price"></span>
        </div>
        <div class="form-group">
          <label for="product_price" class="col-form-label">User Confirm Password</label>
          <input  type='text'  class="form-control" id="user_cpassword" name="user_cpassword">
          <span class="small text-danger" id="errmsg_product_price"></span>
        </div>
        <div class="form-group">
          <label for="product_price" class="col-form-label">User Mobile</label>
          <input  type='text'  class="form-control" id="user_mobile" name="user_mobile">
          <span class="small text-danger" id="errmsg_product_price"></span>
        </div>
       
        <div class="form-group">
          <label for="product_price" class="col-form-label">Status</label>
         <select name="status" id="status" class="form-control" >
					 <!--<option value=''>Select</option>-->
					 <option value="Active">Active</option>
					 <option value="Inactive">Inactive</option>
					 </select>
          <span class="small text-danger" id="errmsg_product_price"></span>
        </div>
					      
      </div>
      <div class="modal-footer">
      	<input class="m-1 btn btn-wc" type="submit" value="Save">
				<button type="button" class="btn btn-outline-wc" data-dismiss="modal">&emsp;Close&emsp;</button>
      </div>
       <?php echo form_close();?> 
    </div>
  </div>
</div>

<?php $this->load->view('front/includes/footer'); ?>
<link type="text/css" href="<?php echo base_url();?>assets/form-select2/select2.css" rel="stylesheet"/>
<script type="text/javascript" src="<?php echo base_url();?>assets/form-select2/select2.min.js"></script>
<script src="//cdn.ckeditor.com/4.13.0/standard/ckeditor.js"></script>


<script type="text/javascript">
	function brief_upload_permission(client_id,user_type_id,brand_id_arr){
		if ($('#upload_brief_pm').is(":checked")){
		  var brief_permission=1;
		  var brand_id=brand_id_arr;
		}
		else{
			var brief_permission=0;
			var brand_id="";
		}

		if ($('#status_brief_pm').is(":checked")){
		  var briefassets_permission=1;
		  //var brand_id=brand_id_arr;
		}
		else{
			var briefassets_permission=0;
			//var brand_id="";
		}


		if ($('#status_user_pm').is(":checked")){
		  var user_permission=1;
		  //var brand_id=brand_id_arr;
		}
		else{
			var user_permission=0;
			//var brand_id="";
		}


		//console.log(brand_id_arr);
		$.ajax({
		type: "POST",
		url: "<?php echo base_url();?>front/Settings/brief_permission", 
		dataType: 'html',
		data: 'client_id='+client_id+"&brief_permission="+brief_permission+"&user_type_id="+user_type_id+"&brand_id="+brand_id+"&briefassets_permission="+briefassets_permission+"&user_permission="+user_permission,
		success: function(data){
		 	$("#loader").hide();
				if(data=="1"){
					//alert(data);
					location.reload();
				}
			}
		});
        
	}
	$("#product_price").keypress(function(e) {
		//console.log("hai");
		if (((e.which != 46 || (e.which == 46 && $(this).val() == '')) ||$(this).val().indexOf('.') != 0) && (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57))) {
			$("#errmsg_product_price").html("Digits Only").show().fadeOut(1500);
			return false;
		}
	}).on('paste', function(e) {
		$("#errmsg_product_price").html("Type the value").show().fadeOut(1500);
		return false;
	});
	$("#product_price").on("input", function() {
		if (/^0/.test(this.value)) {
			this.value = this.value.replace(/^0/, "");
			$("#errmsg_product_price").html("Invalid").show().fadeOut(1500);
		}

	}); 
	
	$('#update_project_desc').click(function(){
		var project_description = encodeURIComponent(CKEDITOR.instances['project_description'].getData());
		var client_id=$("#client_id").val();
		//alert(project_description);

		if(project_description!=""){
			$("#loader").show();
			$.ajax({
		    type: "POST",
		    url: "<?php echo base_url();?>front/Settings/update_project_desc", 
		    dataType: 'html',
		    data: 'client_id='+client_id+"&project_description="+project_description,
		    success: function(data){
		     	$("#loader").hide();
				if(data=="1"){
					//alert(data);
					location.reload();
				}
		  	}
		  });
		}
		else{
			if(project_description==""){
				$("#errmsg_project_description").html("Enter the Some Content").show().fadeOut(1500);
			}
			
		}
	});
	$('#add_product').click(function(){
		var product_name=$("#product_name").val();
		var product_description=$("#product_description").val();
		var product_price = $("#product_price").val();
		//alert(product_name+product_description+product_price);
		if(product_name!="" &&product_description!=""&& product_price!=""){
			$("#loader").show();
			$.ajax({
		    type: "POST",
		    url: "<?php echo base_url();?>front/Settings/add_product", 
		    dataType: 'html',
		    data: 'product_name='+product_name+"&product_description="+product_description+"&product_price="+product_price,
		    success: function(data){
				$("#loader").hide();
				$('#product_details_table').html('');
				$('#product_details_table').html(data);
				$('#addModel').modal('hide');
				$("#product_name").val("");
				$("#product_description").val("");
				$("#product_price").val("");
		  	}
		  });
		}
		else{
			if(product_name==""){
				$("#errmsg_product_name").html("Enter the value").show().fadeOut(1500);
			}
			if(product_description==""){
				$("#errmsg_product_description").html("Enter the Value").show().fadeOut(1500);
			}
			if(product_price==""){
				$("#errmsg_product_price").html("Enter the Value").show().fadeOut(1500);
			}
		}
	})
	function edit_product(product_id){
		//alert(product_id);
		var product_name=$("#product_name_"+product_id).val();
		var product_description=$("#product_description_"+product_id).val();
		var product_price = $("#product_price_"+product_id).val();
		if(product_name!="" &&product_description!=""&& product_price!=""){
			$("#loader").show();
			$.ajax({
		    type: "POST",
		    url: "<?php echo base_url();?>front/Settings/update_product", 
		    data: 'product_id='+product_id+"&product_name="+product_name+"&product_description="+product_description+"&product_price="+product_price,
		    success: function(data){
		    	$('#product_details_table').html("");
				$('#product_details_table').html(data);
				// $('#editModel'+product_id).modal('hide');
				// $("#product_name"+product_id).val("");
				// $("#product_description"+product_id).val("");
				// $("#product_price"+product_id).val("");
			    $("#loader").hide();
			    //alert(data);
				
		  	}
		  });
		}
		else{
			if(product_name==""){
				$("#errmsg_product_name"+product_id).html("Enter the value").show().fadeOut(1500);
			}
			if(product_description==""){
				$("#errmsg_product_description"+product_id).html("Enter the Value").show().fadeOut(1500);
			}
			if(product_price==""){
				$("#errmsg_product_price"+product_id).html("Enter the Value").show().fadeOut(1500);
			}
		}
		
	}
	function delete_product(product_id){
    $("#loader").show();
	  $.ajax({
      type: "POST",
      url: "<?php echo base_url();?>front/Settings/delete_product", 
      data: 'product_id='+product_id,
      success: function(data){
      	$('#product_details_table').html("");
        $('#product_details_table').html(data);
        //$('#deleteModel'+product_id).modal('hide');
        $("#loader").hide();
        
    	}
		});
	}
	function add_discount(user_id,add_discount_status){
      $("#loader").show();
	  $.ajax({
	      type: "POST",
	      url: "<?php echo base_url();?>front/Settings/add_discount", 
	      data: 'user_id='+user_id+"&add_discount_status="+add_discount_status,
	      success: function(data){
	      	$("#loader").hide();
	      	$("#chkDiscount").removeAttr("onclick","add_discount('"+user_id+"','"+add_discount_status+"')");
			if(add_discount_status==1){
				add_discount_status=0;
			}
			else if(add_discount_status==0){
				add_discount_status=1;
			}
			//alert(add_discount_status);
			$("#chkDiscount").attr("onclick","add_discount('"+user_id+"','"+add_discount_status+"')");
	      	$('#modal_msg').modal('show');
			$('#txtMsg').html(data);
	        
	    	}
		});
	}
	function add_extra_charges(user_id,add_extra_charges_status){
    $("#loader").show();
	  $.ajax({
	      type: "POST",
	      url: "<?php echo base_url();?>front/Settings/add_extra_charges", 
	      data: 'user_id='+user_id+"&add_extra_charges_status="+add_extra_charges_status,
	      success: function(data){
	      	$("#loader").hide();
	      	$("#chkEC").removeAttr("onclick","add_extra_charges('"+user_id+"','"+add_extra_charges_status+"')");
			if(add_extra_charges_status==1){
				add_extra_charges_status=0;
			}
			else if(add_extra_charges_status==0){
				add_extra_charges_status=1;
			}
			//alert(add_extra_charges_status);
			$("#chkEC").attr("onclick","add_extra_charges('"+user_id+"','"+add_extra_charges_status+"')");
	      	$('#modal_msg').modal('show');
			$('#txtMsg').html(data);
	       
	        
	    	}
		});
	}

	

$(document).ready(function() {
	CKEDITOR.replace('project_description', {
		allowedContent:true,
	});
});
function activate(user_id)
{
	var checkBox = document.getElementById("chkActivatenotification");
  // If the checkbox is checked, display the output text
  if (checkBox.checked == true){
    var status=1;
  } else {
    var status=0;
  }
  
  $.ajax({
      type: "POST",
      url: "<?php echo base_url();?>front/Settings/update_notifications", 
      data: "user_id="+user_id+"&status="+status,
		success: function(data){
			$('#modal_msg').modal('show');
			$('#txtMsg').html(data);
    }
	});
}

	selectUserType();


 	function selectUserType()
 	{
 		var user_type_id=$('#user_type_id').val();
 		var client_id='<?php echo $client_id_sel;?>';
 		var brand_id='<?php echo $brand_id_sel;?>';
 		if(user_type_id==""){
 			var hid_html="<input type='text' name='client_id' value='0' id='client_id'><input type='text' name='brand_id' value='0' id='brand_id'>";
			$('#divClientBrand').html(hid_html);
			$('#divClientBrand').hide();

 		}
		else if(user_type_id=='2' ||  user_type_id=='3' ||  user_type_id=='5'){
		 	$.ajax({
				method:'post',
				url: "<?php echo base_url();?>front/Settings/showDivClient", 
				data:"&user_type_id="+user_type_id+"&client_id="+client_id+"&brand_id="+brand_id+"&user_id="+user_id+"&user_type_id="+user_type_id,
				success: function(result){
					if(result){
						$('#divClientBrand').html(result);
						$('#divClientBrand').show();
						//$('#client_id').select2();
					}
				}
			});
		}
		else if( user_type_id=='6'){
		 	$.ajax({
				method:'post',
				url: "<?php echo base_url();?>front/Settings/showDivClientMultiple", 
				data:"&user_type_id="+user_type_id+"&client_id="+client_id+"&brand_id="+brand_id+"&user_id="+user_id+"&user_type_id="+user_type_id,
				success: function(result){
					if(result){
						$('#divClientBrand').html(result);
						$('#divClientBrand').show();
						//$('#client_id').select2();
					}
				}
			});
		}
		else if( user_type_id=='4' ){
		 	$.ajax({
				method:'post',
				url: "<?php echo base_url();?>front/Settings/showDivClientBrand", 
				data:"&user_type_id="+user_type_id+"&client_id="+client_id+"&brand_id="+brand_id+"&user_id="+user_id+"&user_type_id="+user_type_id,
				success: function(result){
					if(result){
						$('#divClientBrand').html(result);
						$('#divClientBrand').show();
						//$('#client_id').select2();
						<?php if( $user_type_id_sel=='4' ){ ?>
							selectClient();
						<?php  }  ?>
					}
				}
			});
		}
 	}



 	function selectClient()
 	{
 		var user_type_id=$('#user_type_id').val();
 		var client_id=$('#client_id').val();
 		var brand_id='<?php echo $brand_id_sel;?>';
		if(user_type_id=='4'){
		  $.ajax({
				method:'post',
				url: "<?php echo base_url();?>front/Settings/showDivBrand", 
				data:"&client_id="+client_id+"&brand_id="+brand_id+"&user_id="+user_id+"&user_type_id="+user_type_id,
				success: function(result){
					if(result){
					  $('#brandDiv').html(result);
					  $('#brandDiv').show();
					}
				}
			});
		}
 	}
  


	jQuery(function ($) {
	 	//$('#user_type_id').select2();

		jQuery.validator.addMethod("alphanumeric", function(value, element) {
        return this.optional(element) || /^[a-zA-Z\-\s]+$/.test(value);
        }, "It must contain only letters."); 
		
		jQuery.validator.addMethod("atLeastOneLetter", function (value, element) {
		return this.optional(element) || /[a-zA-Z]+/.test(value);
		}, "Must have at least one  letter");
		
		jQuery.validator.addMethod("phoneno", function(value, element) {
        return this.optional(element) || /^[0-9+() ]*$/.test(value);
        }, "Enter Valid  phone number."); 
		
		
		
		$('#User_form').validate({
		
      rules: {
				user_type_id: {
	        required: true,
				},

				"client_id[]":{
					required:true,
				},
				"brand_id[]":{
					required:true,
				},
        user_name:{
					required:true,
					minlength: 2,
				},
				user_email:{
					required:true,
					email: true,
					remote: {
						url: "<?php echo base_url();?>front/Settings/checkexistemail",
						type: "post"
					},
				
				},
			
				
				user_password:{
					required:true,
					minlength:5,
				},
				user_cpassword: {
					required:true,
	        equalTo: "#user_password" ,
					minlength:5,
	      },
			
			  user_mobile: {
					required: true,
					minlength:10,
					phoneno: true,
					remote: {
					  url: "<?php echo base_url();?>front/Settings/checkexistmobile",
					  type: "post"
					 },
				},
		
				status: {
	        required: true,
				}
				
			},
			messages: {
				user_email:{
					remote:"This email already exists"
				},
				user_mobile: {
					digits: "This field can only contain numbers.",
					/*maxlength: "this must be 10 digit number.",*/
					remote:"This mobile already exists",
				},
	      <?php  if($user_id=='')	{ ?>
	        user_cpassword: {
	          equalTo:"password not match",
				  },
				<?php	}	?>   
			},
			submitHandler: function(form) {
				$("#loader").show();
				var user_id=$('#user_id').val();
				var user_type_id=$('#user_type_id').val();
				
				if(user_type_id=='6'){
					var client_id_all = [];
					var client_id_list = $('input:checkbox:checked.client_id_class').map(function(){
						client_id_all.push(this.value);
					return this.value; }).get().join(",");

					if($.trim(client_id_all)!=''){
						var client_id =","+client_id_all+",";
					}else{
						var client_id ="";
					}
					//console.log(client_id);
				}else{
					var client_id=$('#client_id').val();
				}
				var brand_id_all = [];
				var brand_id_list = $('input:checkbox:checked.brand_id_class').map(function(){
				brand_id_all.push(this.value);
				return this.value; }).get().join(",");

				if($.trim(brand_id_all)!=''){
					var brand_id =","+brand_id_all+",";
				}else{
					var brand_id ="";
				}
				//alert(brand_id);
			//console.log(brand_id);
				var user_name=$('#user_name').val();
				var user_email=$('#user_email').val();
				var user_mobile=$('#user_mobile').val();
				var user_password=$('#user_password').val();
				var status=$('#status').val();

				var user_country=$('#user_country').val();
				$.ajax({
					method:'post',
					url: "<?php echo base_url();?>front/Settings/insert_user", 
					data:"&user_type_id="+user_type_id+"&client_id="+client_id+"&brand_id="+brand_id+"&user_id="+user_id+"&user_name="+user_name+"&user_email="+user_email+"&user_mobile="+user_mobile+"&user_password="+user_password+"&status="+status+"&user_country="+user_country,
					cache: true,
					timeout: 5000
				}).done(function( result ) {
					//console.log( "SUCCESS: " + result );
					$('#loader').hide();
		   			var url="<?php echo base_url(); ?>settings#user-details";
          		window.location.replace(url);
							location.reload();
				}).fail(function() {
					console.log( "Request failed");
					$('#User_form').trigger("reset");
					$('#loader').hide();
							var url="<?php echo base_url(); ?>settings#user-details";
          		window.location.replace(url);
							location.reload();
				});	
			} 
		});
	});


	  function delete_user(user_id){
    $("#loader").show();
        $.ajax({
          method:"post",
          url: "<?php echo base_url();?>front/Settings/deleteuser",
          data:'user_id='+user_id,
          success: function(result){
          	$("#loader").hide();
          	// if($('div.modal-backdrop').hasClass('show')){
          	// 	$('div.modal-backdrop').removeClass('show');
          	// }
          		var url="<?php echo base_url(); ?>settings#user-details";
          		window.location.replace(url);
							location.reload();
		        // $('#user_details_table').html("");
		        // $('#user_details_table').html(result);
		        //$('#hideDel'+user_id).trigger("submit");
		     // onload_user();
		        
      }
    });
   
  }

	
</script>

