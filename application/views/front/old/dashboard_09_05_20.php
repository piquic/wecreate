<?php
session_start();
error_reporting(0);

if ($this->session->userdata('front_logged_in')) {
	$session_data = $this->session->userdata('front_logged_in');
	$user_id = $session_data['user_id'];
	$user_name = $session_data['user_name'];
	$_SESSION['user_id']=$user_id;
	$_SESSION['user_name']=$user_name;
	$type1 = $session_data['user_type_id'];
	$user_type_id=$type1;
	$brand_access=$session_data['brand_access'];

	$checkquerys = $this->db->query("select user_type_name from wc_users_type where user_type_id='$user_type_id' ")->result_array();
	$job_title=$checkquerys[0]['user_type_name'];

}
else
{
	$user_id = '';
	$user_name = ''; 
	$_SESSION['user_id']=$user_id;
	$_SESSION['user_name']=$user_name;
}
?>

<?php $data['page_title'] = "dashboard";

$this->load->view('front/includes/header',$data);
$this->load->view('front/includes/topnav');
?>

<div class="bg-wc px-5" id="div_filter">
	<div class="row">
		<div class="col-12 col-sm-12 col-md-10">
	<div class="d-flex justify-content-start flex-wrap">
		<?php
		$acount_permission=$this->Dashboard_model->checkPermission($user_type_id,1,'access');
		if($acount_permission=='1') {
			?>

			<div class="py-2 mr-2">
				<span class="btn btn-wc" data-toggle="collapse" data-target="#collapseAccount" aria-expanded="false" aria-controls="collapseAccount">Select Account&emsp;<i class="fas fa-chevron-down"></i></span>

				<div id="collapseAccount" class="card collapse" style="width: 20rem; position: absolute; z-index: 2221;" data-parent="#div_filter">
					<div class="card-body p-3">
						<div class="d-flex justify-content-around">
							<p class="flex-grow-1">Filter by Account</p>
							<span data-toggle="collapse" data-target="#collapseAccount" aria-expanded="false" aria-controls="collapseAccount"><i class="fas fa-times"></i></span>
						</div>

						<hr class="pt-0 mt-0">

						<div class="pl-3">
							<?php
							$users_type_details=$this->db->query("select * from  wc_clients where wc_clients.deleted='0' and wc_clients.status='Active'  ")->result_array();

							if(!empty($users_type_details)) {
								foreach($users_type_details as $key => $userstypedetails) {
									$client_id=$userstypedetails['client_id'];
									$client_name=$userstypedetails['client_name'];
									?>

									<div class="custom-control custom-radio">
										<input type="radio" class="custom-control-input account_id" id="account<?php echo $client_id;?>" name="<?php echo $client_id;?>" >
										<label class="custom-control-label roundCheck" for="account<?php echo $client_id;?>"><?php echo ucfirst($client_name);?></label>
									</div>

								<?php } } ?>
							</div>
						</div>
					</div>
				</div>

			<?php } else { ?>
				<input type="hidden" name="account_id" id="account_id" value="">
			<?php } ?>

			<?php
			$account_permission=$this->Dashboard_model->checkPermission($user_type_id,1,'access');
			$brand_permission=$this->Dashboard_model->checkPermission($user_type_id,2,'access');
			$admin_permission=$this->Dashboard_model->checkPermission($user_type_id,3,'access');

			if($account_permission=='1' || $brand_permission=='1' || $admin_permission=='1')
			{

				$checkquerys = $this->db->query("select * from wc_users where  user_id='$user_id' ")->result_array();
				$brand_id = $checkquerys[0]['brand_id'];
				$client_id = $checkquerys[0]['client_id'];


				?>

				<div class="py-2 mr-2">
					<span class="btn btn-wc" data-toggle="collapse" data-target="#collapseBrand" aria-expanded="false" aria-controls="collapseBrand">Select Brand&emsp;<i class="fas fa-chevron-down"></i></span>

					<div id="collapseBrand" class="card collapse" style="width: 20rem; position: absolute; z-index: 2222;" data-parent="#div_filter">
						<div class="card-body p-3">
							<div class="d-flex justify-content-around">
								<p class="flex-grow-1">Filter by Brand</p>
								<span data-toggle="collapse" data-target="#collapseBrand" aria-expanded="false" aria-controls="collapseBrand"><i class="fas fa-times"></i></span>
							</div>

							<hr class="pt-0 mt-0">

							<div class="pl-3">
								<?php

								$sql="select * from wc_brands where status='Active' ";

								if( ($user_type_id=='2' || $user_type_id=='3') &&  $client_id!='0')
								{
									$sql.=" and brand_id in (select brand_id from wc_brands where client_id='".$client_id."') ";
								}

								if( $user_type_id=='4' &&  $brand_id!='0' )
								{
									$sql.=" and brand_id in (".$brand_id.") ";
								}

								$brand_type_details=$this->db->query($sql)->result_array();
								if(!empty($brand_type_details)){
									foreach($brand_type_details as $key => $brandstypedetails){
										$brand_id=$brandstypedetails['brand_id'];
										$brand_name=$brandstypedetails['brand_name'];

										?>

										<div class="custom-control custom-radio">
											<input type="radio" class="custom-control-input brand_id" id="brand_<?php echo $brand_id;?>" name="brand_id[]" onchange="sortbrief()" value='<?php echo $brand_id; ?>'>
											<label class="custom-control-label roundCheck" for="brand_<?php echo $brand_id;?>"><?php echo ucfirst($brand_name);?></label>
										</div>

									<?php } } ?>
								</div>
							</div>
						</div>
					</div>

				<?php } else { ?>
					<input type="hidden" name="brand_id" id="brand_id" value="">
					<!-- <input type="hidden" name="search_key" id="search_key" value=""> -->
				<?php } ?>

				<!-- <div class="p-2 icon-md">|</div> -->
				<div class="pt-2 mr-4 mt-1">
					<?php if($user_type_id=='4') { ?>
						<a class="text-wc lead" href="<?php echo base_url() ?>uploadbrief"><i class="fas fa-cloud-upload-alt"></i>&nbsp;ADD PROJECT</a>
					<?php } ?>
				</div>

				<div class="py-2 mr-2">
					<span class="btn btn-wc" data-toggle="collapse" data-target="#collapseFilter" aria-expanded="false" aria-controls="collapseFilter">Filter&emsp;<i class="fas fa-chevron-down"></i></span>

					<div id="collapseFilter" class="card collapse" style="width: 20rem; position: absolute; z-index: 2223;" data-parent="#div_filter">
						<div class="card-body p-3">
							<div class="d-flex justify-content-around">
								<p class="flex-grow-1">Filter by Stages</p>
								<span data-toggle="collapse" data-target="#collapseFilter" aria-expanded="false" aria-controls="collapseFilter"><i class="fas fa-times"></i></span>
							</div>

							<hr class="pt-0 mt-0">

							<div class="pl-3">
								<div class="custom-control custom-radio">
									<input type="radio" class="custom-control-input status_filter" name="breif_status[]" id="bsf0"  value="0" onchange="sortbrief()">
									<label class="custom-control-label roundCheck lead" for="bsf0">Brief in Review <span class="badge badge-wc" id="bir"><?php echo $briefinreview; ?></span></label>
								</div>

								<div class="custom-control custom-radio">
									<input type="radio" class="custom-control-input status_filter" name="breif_status[]" id="bsf4" value="4" onchange="sortbrief()">
									<label class="custom-control-label roundCheck lead" for="bsf4">Brief Rejected <span class="badge badge-wc" id="br"><?php echo $briefrejected; ?></span></label>
								</div>

								<div class="custom-control custom-radio">
									<input type="radio" class="custom-control-input status_filter" name="breif_status[]" id="bsf1" value="1" onchange="sortbrief()">
									<label class="custom-control-label roundCheck lead" for="bsf1">Work in Progress <span class="badge badge-wc" id="wip"><?php echo $workinprogress; ?></span></label>
								</div>

								<div class="custom-control custom-radio">
									<input type="radio" class="custom-control-input status_filter" name="breif_status[]" id="bsf6" value="6" onchange="sortbrief()">
									<label class="custom-control-label roundCheck lead" for="bsf6">Proofing Pending <span class="badge badge-wc" id="pp"><?php echo $proffing_pending; ?></span></label>
								</div>

								<div class="custom-control custom-radio">
									<input type="radio" class="custom-control-input status_filter" name="breif_status[]" id="bsf3" value="3" onchange="sortbrief()">
									<label class="custom-control-label roundCheck lead" for="bsf3">Revision Work <span class="badge badge-wc" id="rw"><?php echo $revision_work; ?></span></label>
								</div>

								<div class="custom-control custom-radio">
									<input type="radio" class="custom-control-input status_filter" name="breif_status[]" id="bsf2" value="2" onchange="sortbrief()">
									<label class="custom-control-label roundCheck lead" for="bsf2">Work Complete <span class="badge badge-wc" id="wc"><?php echo $work_complete; ?></span></label>
								</div>                              

                        <!-- <div class="custom-control custom-radio">
                            <input type="radio" class="custom-control-input" name="breif_status[]" id="bsf5" value="5" onchange="sortbrief()">
                            <label class="custom-control-label roundCheck lead" for="bsf5">Feedback Pending <span class="badge badge-wc" id="fp"><?php echo $feedback_pending; ?></span></label>
                         </div> -->

                         <div class="custom-control custom-radio">
                         	<input type="radio" class="custom-control-input status_filter" name="breif_status[]" id="bsf7" value="7" onchange="sortbrief()">
                         	<label class="custom-control-label roundCheck lead" for="bsf7">Archived <span class="badge badge-wc" id="fp"><?php echo $feedback_pending; ?></span></label>
                         </div>
                      </div>
                   </div>
                </div>
             </div>
             <!-- <div class="pt-2 mt-1">
             	<input type="hidden" class="form-control" id="search_key" name="search_key" placeholder="Search Brief Id" onkeyup="sortbrief();">
             </div> -->

             <!-- <div class="pt-1 mr-2 mt-1">
             	<form>
             		<input type="text" class="form-control" id="search_key" name="search_key" placeholder="Search By Id, Title" onkeyup="sortbrief();">
             	</form>

             </div>-->

          </div>
          </div>
          <div class="col-12 col-sm-12 col-md-2">
          	<div class="pt-1 mt-1">
             	<form>
             		<input type="text" class="form-control" id="search_key" name="search_key" placeholder="Search By Id, Title" onkeyup="sortbrief();">
             	</form>

             </div>
          </div>
       </div>
      </div>

       <div class="d-flex justify-content-start flex-wrap p-2 pt-4 px-5 mb-3" id="div_sel_filter">
       	<!-- <span class="py-1 px-2 text-wc border-wc rounded-pill">In Review&emsp;<i class="fas fa-times text-danger"></i></span> -->
       </div>

       <div class="container-fluid px-5">
       	<div class="row">

       		<!-- Main Body -->
       		<div class="col-12 col-sm-12 col-md-12" id="myResponse">
       			<div id="tblbrf" class="row">

       				<?php
       				if(!empty($dashboradinfo))
       				{
       					$i=1;
       					foreach($dashboradinfo as $keybill => $brief)
       					{ 
       						$brief_id=$brief['brief_id'];
       						$added_by_user_id=$brief['added_by_user_id'];
       						$brief_title=$brief['brief_title'];
       						$brief_name=preg_replace('/[^a-zA-Z0-9_.]/', '_',$brief_title);
       						$brief_doc=$brief['brief_doc'];
       						$brief_due_date=$brief['brief_due_date'];

       						$status=$brief['status'];
       						$brand_id=$brief['brand_id'];
       						$rejected_reasons=$brief['rejected_reasons'];

       						$checkquerys = $this->db->query("select wc_brands.*,wc_clients.client_name from wc_users 
       							inner join wc_brands on wc_users.brand_id=wc_brands.brand_id  
       							inner join wc_clients on wc_brands.client_id=wc_clients.client_id 
       							where  wc_users.user_id='$added_by_user_id' ")->result_array();

       						$brand_id=$checkquerys[0]['brand_id'];
       						$brand_name=preg_replace('/[^a-zA-Z0-9_.]/', '_',$checkquerys[0]['brand_name']);
       						$client_id=$checkquerys[0]['client_id'];
       						$client_name=preg_replace('/[^a-zA-Z0-9_.]/', '_',$checkquerys[0]['client_name']);

       						$dst = "upload/".$client_id."-".$client_name."/".$brand_id."-".$brand_name."/".$brief_id."-".$brief_name;

       						$download_rej_brief=base_url()."/upload/".$client_id."-".$client_name."/".$brand_id."-".$brand_name."/".$brief_id."-".$brief_name."/".$brief_doc;
       						$status_query1= $this->db->query("SELECT * FROM wc_image_upload WHERE brief_id='$brief_id'");
       						$status_result1= $status_query1->result_array();
       						$count=$status_query1->num_rows();

       						$accept_reject_permission=$this->Dashboard_model->checkPermission($user_type_id,3,'accept_reject');
       						$brief_group_id=explode('.',$brief_doc);
       						$brief_group_id=$brief_group_id[0];
       						$doc_query1= $this->db->query("SELECT * FROM wc_brief_records WHERE brief_group_id='$brief_group_id' and brief_id='$brief_id'");
       						$doc_result1= $doc_query1->result_array();
       						$doc_file="upload/".$client_id."-".$client_name."/".$brand_id."-".$brand_name."/".$brief_id."-".$brief_name."/".$doc_result1[0]['brief_doc'];
									$mime = mime_content_type($doc_file);
									$checkquerys2 = $this->db->query("select * from wc_image_upload where brief_id='$brief_id' and img_status='1' and image_id IN (SELECT MAX(image_id) FROM wc_image_upload  GROUP BY parent_img )");
									$approved_img_count=$checkquerys2->num_rows();
									 $checkquerys3 = $this->db->query("select * from wc_image_upload where brief_id='$brief_id' and (img_status='0' || img_status='2' || img_status='3' || img_status='4') and image_id IN (SELECT MAX(image_id) FROM wc_image_upload  GROUP BY parent_img )");
									$other_img_count=$checkquerys3->num_rows();
       						?>
       						<div class="col-12 col-sm-12 col-md-4 col-lg-3 col-xl-3 mb-4">
       							<div class="card m-auto" style="max-width: 22.6rem; ">

       								<?php 
       								if(strstr($mime, "image/")){
												?>
												<div class="thumb-div rounded-top" style="background-image: url(<?php echo base_url()."/".$doc_file; ?>)" <?php 
       									if($accept_reject_permission=='0' && ($status=='1'||$status=='2'||$status=='4'||$status=='5'||$status=='6'||$status=='7')){  ?>
       										data-toggle="collapse" data-target="#click_<?php echo $brief_id;?>" aria-expanded="false" aria-controls="click_<?php echo $brief_id;?>"
       										<?php } else if($accept_reject_permission=='1' && ($status=='0'||$status=='1'||$status=='2'||$status=='6'||$status=='7')){  ?> data-toggle="collapse" data-target="#click_<?php echo $brief_id;?>" aria-expanded="false" aria-controls="click_<?php echo $brief_id;?>" <?php } ?> ></div>
												<?php
											}
											else{
												if($count>0){
       									$image_id=$status_result1[0]['image_id'];
       									$image_path=$status_result1[0]['image_path'];
       									$checkquerys1 = $this->db->query("select * from wc_image_upload where image_id='$image_id' ")->result_array();
       									$parent_img=$checkquerys1[0]['parent_img'];
       									$version_num=$checkquerys1[0]['version_num'];
       									$file_type=$checkquerys1[0]['file_type'];


       									$dst = "upload/".$client_id."-".$client_name."/".$brand_id."-".$brand_name."/".$brief_id."-".$brief_name."/".$parent_img."/Revision".$version_num;  
       									$html=""; 



       									$image_org="upload/".$client_id."-".$client_name."/".$brand_id."-".$brand_name."/".$brief_id."-".$brief_name."/".$image_id."/Revision".$version_num."/thumb_".$image_path;

       									?>
       									<div class="thumb-div rounded-top" style="background-image: url(<?php echo base_url()."/".$image_org; ?>)" <?php 
       									if($accept_reject_permission=='0' && ($status=='1'||$status=='2'||$status=='4'||$status=='5'||$status=='6'||$status=='7')){  ?>
       										data-toggle="collapse" data-target="#click_<?php echo $brief_id;?>" aria-expanded="false" aria-controls="click_<?php echo $brief_id;?>"
       										<?php } else if($accept_reject_permission=='1' && ($status=='0'||$status=='1'||$status=='2'||$status=='6'||$status=='7')){  ?> data-toggle="collapse" data-target="#click_<?php echo $brief_id;?>" aria-expanded="false" aria-controls="click_<?php echo $brief_id;?>" <?php } ?> ></div>
       										<?php
       										 
       									}
       									else{
       										?>
       										<div class="thumb-div rounded-top" style="background-image: url(<?php echo base_url() ?>website-assets/images/no-image.png);" <?php 

       										if($accept_reject_permission=='0' && ($status=='1'||$status=='2'||$status=='4'||$status=='5'||$status=='6'||$status=='7')){  ?>
       											data-toggle="collapse" data-target="#click_<?php echo $brief_id;?>" aria-expanded="false" aria-controls="click_<?php echo $brief_id;?>"
       											<?php } else if($accept_reject_permission=='1' && ($status=='0'||$status=='1'||$status=='2'||$status=='6'||$status=='7')){  ?> data-toggle="collapse" data-target="#click_<?php echo $brief_id;?>" aria-expanded="false" aria-controls="click_<?php echo $brief_id;?>" <?php } ?> ></div>

       											<?php
       										}
											}
       								
       										?>
       										<div class="card-body">
       											<h5 class="card-title"><?php echo $brief_title;?></h5>
       											<div class="card-text mb-3">
       												<div class="d-flex justify-content-between flex-wrap">
       													<span class="py-1">
       														<i class="far fa-clock"></i>&nbsp;
       														<?php
       														if($status=='0' ) 
       															{ 
       																//echo "N/A"; 
       																if($brief_due_date=='0000-00-00 00:00:00') { 
       																echo "N/A";
       																 }
       															else {
       																$brief_due_date= date('d M y H:i', strtotime($brief_due_date));
       																echo $brief_due_date;
       															}    
       													   }
       														else {

       															if($brief_due_date=='0000-00-00 00:00:00') { 
       																echo "N/A";
       																 }
       															else {
       																$brief_due_date= date('d M y H:i', strtotime($brief_due_date));
       																echo $brief_due_date;
       															}                                           
       														}
       														?>
       													</span>
       													<?php if($count>0){ ?>
       														<span>
       															<span class="c-ico"><?php echo $approved_img_count ?></span>&nbsp;<span class="c-ico bg-secondary"><?php echo $other_img_count ?></span>
       														</span>
       														<?php
       													}
       													?>
       												</div>

       												<p>#P<?php echo $brief_id;?></p>
       											</div>
       											<div class="d-flex justify-content-between">
       												<small class="py-2 px-3 text-wc border-wc rounded-pill">
       													<?php
       													if($status=='0') { echo "Brief in Review"; }
       													else if($status=='1') { echo "Work in Progress"; }
       													else if($status=='2') { echo "Work Completed";  }
       													else if($status=='3') { echo "Revision Work"; }
       													else if($status=='4') { echo "Brief Rejected"; }
       													else if($status=='5') { echo "Feedback Pending"; }
       													else if($status=='6') { echo "Proofing Pending"; }
       													else if($status=='7') { echo "Archived"; }                                  
       													?>
       												</small>
       												<?php 
       												$accept_reject_permission=$this->Dashboard_model->checkPermission($user_type_id,3,'accept_reject');
       												if($accept_reject_permission=='0' && ($status=='0'||$status=='1'||$status=='2'||$status=='4'||$status=='5'||$status=='6'||$status=='7')){  ?>
       													<span class="btn btn-link text-dark" data-toggle="collapse" data-target="#click_<?php echo $brief_id;?>" aria-expanded="false" aria-controls="click_<?php echo $brief_id;?>"><i class="fas fa-ellipsis-h"></i></span>
       												<?php  }else if($accept_reject_permission=='1' && ($status=='0'||$status=='1'||$status=='2'||$status=='6'||$status=='7')){  ?>
       													<span class="btn btn-link text-dark" data-toggle="collapse" data-target="#click_<?php echo $brief_id;?>" aria-expanded="false" aria-controls="click_<?php echo $brief_id;?>"><i class="fas fa-ellipsis-h"></i></span>
       												<?php  } ?>
       											</div>
       										</div>
       										<?php if($user_type_id=='1' || $user_type_id=='2'){
       										?>
       										<span class="position-absolute" style="top: -0.56rem; right:-.9rem;" data-toggle="modal" data-target="#modal_confirm_<?php echo $brief_id; ?>"><i class="fas fa-times-circle icon-md text-danger bg-white rounded-circle" style="padding: .1rem"></i></span>
                              
                          <div class="modal fade" id="modal_confirm_<?php echo $brief_id; ?>" tabindex="-1" role="dialog" aria-hidden="true">
                          	<div class="modal-dialog" role="document">
                          		<div class="modal-content">
                          			<div class="modal-header">
                          				<h5 class="modal-title text-dark" id="mod_title_<?php echo $brief_id; ?>">Are you sure, you want to delete the project #P<?php echo $brief_id; ?>?</h5>
                          				<button type="button" class="close text-danger" data-dismiss="modal" aria-label="Close">
                          					<span aria-hidden="true" >&times;</span>
                          				</button>
                          			</div>
                          			<div class="modal-footer">
                          				<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                          				<button type="button" id="hideDel" class="btn btn-danger" onclick="delete_card('<?php echo $brief_id; ?>');">Delete</button>
                          			</div>
                          		</div>
                          	</div>
                          </div>
                            <?php
                          } ?>
       									</div>
       									
       									<div id="click_<?php echo $brief_id;?>" class="collapse" data-parent="#tblbrf">
       										<div class="border rounded-lg pt-3 px-3 bg-white" style="width: 85%; bottom: 5%; right: 5%; position: absolute; z-index: 1113;">
       											<div class="d-flex justify-content-around">
       												<p class="flex-grow-1 text-center font-weight-bold">Actions</p>
       												<span data-toggle="collapse" data-target="#click_<?php echo $brief_id;?>" aria-expanded="false" aria-controls="collapseAccount"><i class="fas fa-times"></i></span>
       											</div>

       											<hr class="border-top p-0 mt-0">
       											<?php
       											if($status=='0' && $accept_reject_permission=='1'){ ?>


       													<!-- <div > -->
       														<div id="brief_approve_reject_<?php echo $brief_id ?>" class="d-flex justify-content-around mb-3">
       															<div class="flex-grow-1 pt-2">Approve brief?</div>
       															<div>
       																<button type="button" class="btn btn-outline-wc rounded-pill"  name="brief_status" id="brief_status<?php echo $brief_id;?>" onclick="changeBriefStatus('<?php echo $brief_id;?>','1')">Yes</button>
       																<button type="button" class="btn btn-outline-wc rounded-pill"  name="brief_status" id="brief_status<?php echo $brief_id;?>" onclick="changeBriefStatus('<?php echo $brief_id;?>','4')">No</button>
       															</div>
       														</div>
       														<!-- </div> -->
       														<div class="d-none" id="div_brief_due_date<?php echo $brief_id; ?>">
       															<div class="mb-3" >
	       															<div class="pt-2 table-responsive tbl-scrl" style="height: 10rem !important;">
	       																<table class="table text-wc">
	       																	<thead>
	       																		<tr>
	       																			<th scope="col">Item(s)</th>
	       																			<th scope="col">Qty.</th>
	       																			<th scope="col">Price</th>
	       																			<th scope="col"></th>
	       																		</tr>
	       																	</thead>
	       																	<tbody id="product-list">
	       																		<tr id="no-result">
	       																			<td>
	       																				<div style='width:100%; white-space:nowrap; overflow:hidden; text-overflow: ellipsis; text-align: center;'>No Result Found</div>
	       																			</td>
	       																		</tr>
	       																	</tbody>
	       																</table>
	       															</div>
																			
	       															<div class="pt-2">
	       																<p class="lead text-wc">Total: Rs.<span id="total_price_values">0</span></p>
	       																<input type='hidden' id='total_price' name='total_price' />
	       																
	       																<?php 
	 																			$price_query1= $this->db->query("SELECT * FROM wc_product_price");
	   																		$price_result1= $price_query1->result_array();
	   																		//print_r($price_result1);
	   																		?>
	       																<div class="d-flex">
	       																	<div class="flex-grow-1 m-1">
	       																		<select class="custom-select" id="product_id_<?php echo $brief_id; ?>" >
	       																			<option >Select Item</option>
	       																			<?php
	       																			foreach($price_result1 as $keyprice => $price_val){ 
				       																	
				       																	?>
				       																	<option value="<?php echo $price_val['product_id']; ?>"><?php echo $price_val['product_name']; ?></option>
				       																	<?php
				       																}
	       																			?>
	       																			
	       																		</select>
	       																	</div>
	       																	<div class="m-1">
	       																		<input type="text" name="txtQty_<?php echo $brief_id; ?>" id="txtQty_<?php echo $brief_id; ?>" class="form-control" style="width: 4rem;">
	       																	</div>

	       																	<div class="m-1">
	       																		<button type="submit" class="btn btn-wc" id="btnadditem_<?php echo $brief_id; ?> " onclick="price_cal('<?php echo $brief_id; ?>')">Add</button>
	       																	</div>
	       																</div>
	       															</div>
	       														</div>

	       														<hr class="border-top p-0 mt-0">
	       														<div class="mb-3">
	       															<div class="pt-2">
	       																

	       																	<!-- <input type="text" class="form-control datePick " name="brief_due_date" id="brief_due_date<?php echo $brief_id;?>" value="<?php echo date("Y-m-d H:i")?>" placeholder="<?php echo date("Y-m-d H:i")?>"> -->

	       															<?php
	   																	if($status=='0' && $brief_due_date!='0000-00-00 00:00:00')
	   																	{
	   																		$sel_brief_due_date=date("d-M-Y H:i",strtotime($brief_due_date));
	   																		$disabledFlag='disabled';

	   																		?>
																			<p class="text-wc"><strong> Delivery Date : </strong><span><?php echo $sel_brief_due_date ?></span></p>

	   																		<?php
	   																	}
	   																	else
	   																	{
	   																		$sel_brief_due_date=date("Y-m-d H:i");
	   																		?>
	   																		Enter Delivery Date to approve:

	       																<div class="input-group mb-2 mr-sm-2 " >
	       																	<div class="input-group-prepend ">
	       																		<div class="input-group-text "><i class="far fa-calendar-alt"></i></div>
	       																	</div>
	   																		<input type="text" class="form-control datePick " name="brief_due_date" id="brief_due_date<?php echo $brief_id;?>" value="<?php echo $sel_brief_due_date;?>" placeholder="<?php echo $sel_brief_due_date;?>" readonly>
	   																		<script type="text/javascript">
	       																		$(function () {
	       																			$('#brief_due_date<?php echo $brief_id;?>').datetimepicker({
	                                                //language:  'fr',
	                                                weekStart: 1,
	                                                todayBtn:  1,
	                                                autoclose: 1,
	                                                todayHighlight: 1,
	                                                startView: 2,
	                                                forceParse: 0,
	                                                showMeridian: 1
	                                             });

	       																		});
	       																	</script>

	       																</div>
	   																		<?php
	   																	}
	   																	?>
	       															</div>
	       														</div>
       														</div>
       														

       														<div class="mb-3 d-none" id="brief_review_approve_<?php echo $brief_id ?>">
       															<div class="d-flex justify-content-around flex-wrap">
       																<button type="button" class="btn btn-outline-wc flex-grow-1 m-1" onclick="submitCorfirm(<?php echo $brief_id;?>,'1');">Approve</button>
       																<button type="button" class="btn btn-outline-wc flex-grow-1 m-1" onclick="submitCancel(<?php echo $brief_id;?>);">Cancel</button>
       															</div>
       														</div>
       														<div class="mb-3 d-none" id="brief_review_reject_<?php echo $brief_id ?>">
       															<div class="pt-2">
       																<span class="b-text bg-white p-2 ">REASON FOR REJECTION</span><br>
       																<input type="text" name="rejected_reasons" id="rejected_reasons<?php echo $brief_id;?>" class="form-control mb-3 border" value="<?php echo $rejected_reasons;?>" >
       																<div>
       																	<button type="submit" class="btn btn-outline-wc rounded-pill" onclick="submitCorfirm(<?php echo $brief_id;?>,'4');">Confirm</button>
       																	<button type="submit" class="btn btn-outline-wc rounded-pill" onclick="submitCancel(<?php echo $brief_id;?>);">Cancel</button>

       																</div>
       															</div>
       														</div>
       														<hr class="border-top p-0 mt-0">
       														<?php
       												}
       												if($status=='0' && $accept_reject_permission=='0'){ 
       													?>
       													<p><a class="btn btn-outline-dark btn-block text-left border-0"  data-toggle="collapse" role="button" href="#ModifiedDate_<?php echo $brief_id;?>" aria-expanded="false" aria-controls="ModifiedDate_<?php echo $brief_id;?>">Modfied Date</a></p>
       													<?php
       												}

       												if($status=='0'||$status=='6'){
       													?>
       													<p><a class="btn btn-outline-dark btn-block text-left border-0" href="<?php echo $download_rej_brief;?>">Download Brief</a></p>
       													<?php
       												}
       												if($status=='1'||$status=='2'||$status=='5'||$status=='6'||$status=='7'){ ?>

       													<p><a class="btn btn-outline-dark btn-block text-left border-0" href="<?php echo base_url();?>view-files/<?php echo $brief_id; ?>">View Files</a></p>

       													<p><a class="btn btn-outline-dark btn-block text-left border-0" data-toggle="collapse" role="button" href="#items_<?php echo $brief_id;?>" aria-expanded="false" aria-controls="items_<?php echo $brief_id;?>">View Items</a></p>

       											
       													<div class="mb-3 d-none" id="div_view_items<?php echo $brief_id; ?>">
     															
     														</div>

       													<?php

       												}
       												if($status=='1' &&$accept_reject_permission=='1')
       													{ ?>
       														<p><a class="btn btn-outline-dark btn-block text-left border-0" href="<?php echo base_url();?>uploadimages/<?php echo $brief_id; ?>">Upload Files</a></p>
       													<?php } 
       													
       												if($status=='2') { ?>
       													
       													<p><a class="btn btn-outline-dark btn-block text-left border-0" href="<?php echo base_url() ?>feedback/<?php echo $brief_id;?>">Add Feedback</a></p>

       												<?php }
       												if($status=='3') { echo "Revision Work"; }
       												if($accept_reject_permission=='0'&& $status=='4') {?>

       													<p><a class="btn btn-outline-dark btn-block text-left border-0" href="<?php echo $download_rej_brief;?>">Download Rejected Brief</a></p>
       													<!-- <p><label class="w3-button w3-blue w3-round"> -->
       														<p><label class="btn btn-outline-dark btn-block text-left border-0">
       															Upload New Brief
       															<input type="file" style="display: none" class="re_upload_doc fas fa-download fa-2x" id="files1" alt='1' name="files1[]"  multiple val="<?php echo $brief_id;?>"  folder=""/>
       														</label>

       														<input type="hidden" name="file_nm_hid_<?php echo $brief_id;?>" id="file_nm_hid_<?php echo $brief_id;?>" value=""></p>

       														<hr class="border-top p-0 mt-0">

       														<p>Reason of Rejection:</p>
       														<div class="mb-3 scrl" style="height: 6rem !important;">
       															<small><?php echo $rejected_reasons;?></small>
       														</div>

       													<?php }
       													if($accept_reject_permission=='0'&& $status=='5') { ?> 
       														<p><a class="btn btn-outline-dark btn-block text-left border-0" href="<?php echo base_url() ?>feedback/<?php echo $brief_id;?>">Add Feedback</a></p>
       													<?php }
       													if($status=='6') { ?>
       														<p><a class="btn btn-outline-dark btn-block text-left border-0" href="<?php echo base_url() ?>open-proofing\<?php echo $brief_id;?>">View Proofing</a></p>
       														
       													<?php }                              
       													?>
       												</div>

       											</div>
       											<div id="items_<?php echo $brief_id;?>" data-parent="#tblbrf" class="collapse border rounded-lg pt-3 px-3 bg-white" style="width: 85%; bottom: 5%; right: 5%; position: absolute; z-index: 1113;">
														<div class="d-flex justify-content-around">
															<p class="flex-grow-1 text-center font-weight-bold">Items</p>
															<span data-toggle="collapse" role="button" href="#items_<?php echo $brief_id;?>" aria-expanded="false" aria-controls="items_<?php echo $brief_id;?>"><i class="fas fa-times"></i></span>
														</div>

														<hr class="border p-0 mt-0">

														<div class="mb-3">
															<div class="pt-2 table-responsive tbl-scrl" style="height: 10rem !important;">
     																<table class="table text-wc">
     																	<thead>
     																		<tr>
     																			<th scope="col">Item(s)</th>
     																			<th scope="col">Qty.</th>
     																			<th scope="col">Price</th>
     																			
     																		</tr>
     																	</thead>
     																	<?php 
																			$product_query1= $this->db->query("SELECT * FROM wc_brief_product_list where brief_id='$brief_id'");
 																			$product_result1= $product_query1->result_array();

 																			$total_price_query1= $this->db->query("SELECT total_price FROM wc_brief where brief_id='$brief_id'");
 																			$total_price_result1= $total_price_query1->result_array();

 																			//
 																			?>
     																	<tbody id="view_product-list">
     																		<?php 
     																		if(!empty($product_result1))
     																		{
     																			//print_r($product_result1);
     																			foreach($product_result1 as $key => $product_value) {
     																				$brief_product_id=$product_value['brief_product_id'];
																						$product_id=$product_value['product_id'];

																						$product_quty=$product_value['product_quty'];
																						$subtotal_price=$product_value['subtotal_price'];

																						$product_name_query1= $this->db->query("SELECT product_name FROM wc_product_price where product_id='$product_id'");
 																						$product_name_result1= $product_name_query1->result_array();
 																						//print_r($product_name_result1);
 																						$product_name=$product_name_result1[0]['product_name'];
 																						?>

 																				<tr id="vire<?php echo $brief_product_id ?>">
											                    <td><div class="small" style="width: 90%; white-space:nowrap; overflow:hidden; text-overflow: ellipsis;"><?php echo $product_name ?></div></td>
											                    <td class="small text-right"><?php echo $product_quty ?></td>
											                    <td class="small text-right">Rs.<?php echo $subtotal_price ?></td>
											                   
											                  </tr>
											 									<?php
											 										}
     																		}
     																		if(empty($product_result1)){
     																		?>
     																		<tr id="no-result">
     																			<td>
     																				<div style='width:100%; white-space:nowrap; overflow:hidden; text-overflow: ellipsis; text-align: center;'>No Result Found</div>
     																			</td>
     																		</tr>
     																		<?php 
     																	} ?>
     																	</tbody>
     																</table>
     															</div>
																	
     															<div class="pt-2">
     																<?php if(!empty($total_price_result1[0]['total_price'])){
     																	?>
     																	<p class="lead text-wc">Total: Rs.<span><?php echo $total_price_result1[0]['total_price'] ?></span></p>
     																	<?php
     																} if(empty($total_price_result1[0]['total_price'])){
     																		?>
     																	<p class="lead text-wc">Total: Rs.<span>0</span></p>
     																	<?php
     																}
     																?>
     																
     															</div>
															
														</div>

														<hr class="border p-0 mt-0">
														<?php
														if($brief_due_date!='0000-00-00 00:00:00')
	   												{
	   													$sel_brief_due_date=date("d-M-Y H:i",strtotime($brief_due_date));
	   												?>
															<p class="text-wc"><strong> Delivery Date : </strong><span><?php echo $sel_brief_due_date ?></span></p>
														<?php
	   												} ?>
														<!--  -->
													</div>

													<div id="ModifiedDate_<?php echo $brief_id;?>" data-parent="#tblbrf" class="collapse border rounded-lg pt-3 px-3 bg-white" style="width: 85%; bottom: 5%; right: 5%; position: absolute; z-index: 1113;">
														<div class="d-flex justify-content-around">
															<p class="flex-grow-1 text-center font-weight-bold">Modified Date</p>
															<span data-toggle="collapse" role="button" href="#ModifiedDate_<?php echo $brief_id;?>" aria-expanded="false" aria-controls="ModifiedDate_<?php echo $brief_id;?>"><i class="fas fa-times"></i></span>
														</div>

														<hr class="border p-0 mt-0">
														Enter Delivery Date to approve:

														<div class="input-group mb-2 mr-sm-2 " >
															<div class="input-group-prepend ">
																<div class="input-group-text "><i class="far fa-calendar-alt"></i></div>
															</div>
															<?php $sel_brief_due_date=date("Y-m-d H:i"); ?>
														<input type="text" class="form-control datePick " name="brief_due_date" id="brief_due_date<?php echo $brief_id;?>" value="<?php echo $sel_brief_due_date;?>" placeholder="<?php echo $sel_brief_due_date;?>" readonly>

														<script type="text/javascript">
																$(function () {
																	$('#brief_due_date<?php echo $brief_id;?>').datetimepicker({
                                    //language:  'fr',
                                    weekStart: 1,
                                    todayBtn:  1,
                                    autoclose: 1,
                                    todayHighlight: 1,
                                    startView: 2,
                                    forceParse: 0,
                                    showMeridian: 1
                                 });

																});
															</script>

														</div>
														<div class="mb-3" id="brief_review_approve_<?php echo $brief_id ?>">
       															<div class="d-flex justify-content-around flex-wrap">
       																<button type="button" class="btn btn-outline-wc flex-grow-1 m-1" onclick="submitCorfirm(<?php echo $brief_id;?>,'0');">Update</button>
       																<button type="button" class="btn btn-outline-wc flex-grow-1 m-1" onclick="submitCancel(<?php echo $brief_id;?>);">Cancel</button>
       															</div>
       														</div>
													</div>

       										</div>
       										<!-- <div class="col-12 col-sm-12 col-md-4 col-lg-3 col-xl-3 mb-4">&nbsp;</div> -->

       									<?php } }else{?>
       										<div class="col-12 col-sm-12 col-md-12 mb-4">
       											<p class="text-wc lead"><i class="fas fa-info-circle icon-md"></i>
													&nbsp;No Records Found. 
       												<?php
       												if( $user_type_id=='4' )
       												{
       												?>
       											Click Add Project to add a new project.

       											<?php
       												}
       												?>


       										</p>
       										</div>
       										<?php

       									}?> 

       								</div>
       							</div>

       						</div>
       					</div>

<?php $this->load->view('front/includes/footer'); ?>
<link href="<?php echo base_url(); ?>website-assets/datetimepicker/bootstrap/css/bootstrap-datetimepicker.min.css" rel="stylesheet" media="screen">
<link href="<?php echo base_url(); ?>website-assets/datetimepicker/css/bootstrap-datetimepicker.min.css" rel="stylesheet" media="screen">

<script type="text/javascript" src="<?php echo base_url(); ?>website-assets/datetimepicker/js/bootstrap-datetimepicker.js" charset="UTF-8"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>website-assets/datetimepicker/js/locales/bootstrap-datetimepicker.fr.js" charset="UTF-8"></script>
<script type="text/javascript">
function delete_card(brief_id){
  $("#loader").show();
  $.ajax({
        type: "POST",
        url: "<?php echo base_url();?>front/dashboard/delete_card", 
        dataType: 'html',
        data: 'brief_id='+brief_id,
        success: function(data){
          $("#loader").show();
          var url="<?php echo base_url(); ?>dashboard";
          window.location = url;
      }
  });
}

function price_cal(brief_id){
	$("#no-result").addClass('d-none');
	var product_id=$("#product_id_"+brief_id).val();
	var txtQty=$("#txtQty_"+brief_id).val();
	var calculated_total_sum = 0;
	$.ajax({
    type: "POST",
    url: "<?php echo base_url();?>front/dashboard/price_cal", 
    dataType: 'html',
    data: 'product_id='+product_id+"&txtQty="+txtQty+"&brief_id="+brief_id,
    success: function(data){
      $('#product-list').append(data);
      $('option[value=' +product_id + ']').prop('disabled', true);
      $("#product_id_"+brief_id).prop("selectedIndex", 0);
      $("#txtQty_"+brief_id).val("");
      $("#product-list #subtotal_price").each(function () {
						var get_textbox_value = $(this).val();
						if ($.isNumeric(get_textbox_value)) {
							calculated_total_sum += parseFloat(get_textbox_value);
						}                  
					});
			$("#total_price_values").html(calculated_total_sum);
			$("#total_price").val(calculated_total_sum);
  	}
  });
}
function price_cancel(product_id){
	$('#'+product_id).remove();
	$('option[value=' +product_id + ']').prop('disabled', false);
	var calculated_total_sum = 0;
	$("#product-list #subtotal_price").each(function () {
			var get_textbox_value = $(this).val();
			if ($.isNumeric(get_textbox_value)) {
				calculated_total_sum += parseFloat(get_textbox_value);
			}                  
		});
	$("#total_price_values").html(calculated_total_sum);
}

function changeBriefStatus(brief_id,brief_status){
  //var brief_status=$("#brief_status"+brief_id).val();

  if(brief_status=='1')
  {
    //alert(brief_status);
    //$('.collapse').collapse('show');
    $("#div_brief_due_date"+brief_id).removeClass('d-none');
 
    $("#brief_review_approve_"+brief_id).removeClass('d-none');
    $("#brief_review_reject_"+brief_id).addClass('d-none');
   
    // $("#brief_approve_reject_"+brief_id).addClass('d-none');
   // $("#brief_approve_reject_"+brief_id).attr('style', 'display:none!important');
    //$(".brief_review_reject_"+brief_id).addClass('d-none');
    //$(".brief_download_"+brief_id).removeClass('d-none');
  }
	else if(brief_status=='4')
	{
		$("#rejected_reasons"+brief_id).val('');
		$("#div_brief_due_date"+brief_id).addClass('d-none');
		$("#brief_review_approve_"+brief_id).addClass('d-none');
		$("#brief_review_reject_"+brief_id).removeClass('d-none');
		//$("#brief_approve_reject_"+brief_id).addClass('d-none');
	//	$("#brief_approve_reject_"+brief_id).attr('style', 'display:none!important');
		$("#brief_review_reject_"+brief_id).removeClass('d-none');
		$( "#tr_"+brief_id ).removeClass( "border-bottom" );
	}
	else
	{

		$("#brief_review_approve_"+brief_id).addClass('d-none');
		$("#brief_review_reject_"+brief_id).addClass('d-none');

	}
}
function submitCancel(brief_id)
{
	sortbrief();
}

function submitCorfirm(brief_id,brief_status)
{


	// var brief_status=$("#brief_status"+brief_id).val();
	//alert("--"+brief_status);
	if(brief_status=='')
	{
	// alert("Please select Status");
	$('#modal_msg').modal('show');
	$('#txtMsg').html('Please select Status');
	}
	else
	{



	if(brief_status=='1')
	{
		var brief_due_date=$("#brief_due_date"+brief_id).val();
		//alert(brief_due_date);
		//var brief_due_date = $("#brief_due_date"+brief_id).attr('placeholder'); 

		//alert(brief_due_date);
		var rejected_reasons='';
		var product_id = $("input[id='product_id']")
		.map(function(){return $(this).val();}).get();
		var total_qty = $("input[id='total_qty']")
		  .map(function(){return $(this).val();}).get();
		var subtotal_price = $("input[id='subtotal_price']")
		  .map(function(){return $(this).val();}).get();
		var total_price = $("#total_price").val();
		$("#loader").show();


	}
	else if(brief_status=='4')
	{
		var brief_due_date='';
		var rejected_reasons=$("#rejected_reasons"+brief_id).val();
		var product_id = '';
		var total_qty = '';
		var subtotal_price = '';
		var total_price = '';
		if(rejected_reasons=='')
		{
		  // alert("Please Give Reason");
		  $('#modal_msg').modal('show');
		  $('#txtMsg').html('Please Give Reason');
		  return false;
		}
		else
		{
			$("#loader").show();
		}

	}
	else if(brief_status=='0')
	{
		var brief_due_date=$("#brief_due_date"+brief_id).val();
		//alert(brief_due_date);
		//var brief_due_date = $("#brief_due_date"+brief_id).attr('placeholder'); 

		//alert(brief_due_date);
		var rejected_reasons='';
		var product_id = '';
		var total_qty = '';
		var subtotal_price = '';
		var total_price = '';
		$("#loader").show();


	}
	//alert(total_price);

	//alert("<?php //echo base_url();?>front/uploadbrief/updateBriefStatus?&brief_id="+brief_id+"&brief_status="+brief_status+"&brief_due_date="+brief_due_date+"&rejected_reasons="+rejected_reasons);

	$.ajax({
	method:'POST',
	url: "<?php echo base_url();?>front/uploadbrief/updateBriefStatus",
	data:"&brief_id="+brief_id+"&brief_status="+brief_status+"&brief_due_date="+brief_due_date+"&rejected_reasons="+rejected_reasons+"&total_price="+total_price+"&product_id="+product_id+"&total_qty="+total_qty+"&subtotal_price="+subtotal_price,

	success: function(result){
	//alert(result);
	if(result) {

	$("#loader").hide();
	$('#click_'+brief_id).removeClass('show');
	sortbrief();
	}
	}
	});

	}
}


  /*$(document).ready(function() {*/

  	function selectAccount(brand_id_sel)
  	{

        //alert(brand_id_sel);
        var account_id=$('#account_id').val();
         //alert(account_id);
         $.ajax({
         	method:'POST',
         	url: "<?php echo base_url();?>front/dashboard/getBrandDropDown",
         	data:"&account_id="+account_id+"&brand_id_sel="+brand_id_sel,

         	success: function(result){
                    //alert(result);
                    if(result) {
                    	$("#divBrand").html(result);
                    	sortbrief();

                    }
                 }
              });
      }

      function removeFilter(filter_id) {
        //alert(filter_id);
        document.getElementById("bsf"+filter_id).checked = false;
        sortbrief();
     }

     function sortbrief() {

     	$("#loader").show();

        //alert("aaaaaa");
        
        // var status_filter = new Array();
        // $(".status_filter:checked").each(function() {
        //  status_filter.push($(this).val());
        // });

        var status_filter = new Array();
        var status_filter_name = "";
        $(".status_filter:checked").each(function() {
        	status_filter.push($(this).val());

        	var filter_id=$(this).val();



        	if(filter_id=="0")
        	{
        		var filt_name="Brief in Review";
        	}

        	if(filter_id=="1")
        	{
        		var filt_name="Work in Progress";

        	}
        	if(filter_id=="2")
        	{
        		var filt_name="Work Completed";

        	}
        	if(filter_id=="3")
        	{
        		var filt_name="Revision Work";

        	}
        	if(filter_id=="4")
        	{
        		var filt_name="Brief Rejected";

        	}
        	if(filter_id=="5")
        	{
        		var filt_name="Feedback Pending";

        	}
        	if(filter_id=="6")
        	{
        		var filt_name="Proofing Pending";

        	}
        	if(filter_id=="7")
        	{
        		var filt_name="Archived";

        	}

        	status_filter_name+='<span class="py-1 px-2 mr-2 text-wc border-wc rounded-pill">'+filt_name+'&emsp;<span onclick="removeFilter('+filter_id+');"><i class="fas fa-times" ></i></span></span>';


        });

//alert(status_filter_name);

$("#div_sel_filter").html(status_filter_name);



var brand_id = new Array();
$(".brand_id:checked").each(function() {
	brand_id.push($(this).val());
});

var account_id = new Array();
$(".account_id:checked").each(function() {
	account_id.push($(this).val());
});
        // var status_filter = $('.status_filter:checked').map(function() {
        //     return this.value;
        // }).get().join(', ')
        //alert($('.status_filter').val());

        // var brand_id = $(".brand_id").val();
        // var account_id = $("#account_id").val();
        var search_key = $("#search_key").val();
        console.log(status_filter);
        console.log(brand_id);
        console.log(account_id);
        // alert(brand_id);
        // alert(account_id);
        // alert(search_key);

        $.ajax({
        	type: "POST",
        	url: "<?php echo base_url();?>front/dashboard/getbriefsort",
        	dataType: 'html',
        	data: 'search_key='+search_key+'&brand_id='+brand_id+'&account_id='+account_id+'&status_filter='+status_filter,
        	success: function(data){

                //alert(data);
                $('#myResponse').html(data)

                getbir(brand_id);
                getbr(brand_id);
                getwip(brand_id);
                getrw(brand_id);
                getpp(brand_id);
                getwc(brand_id);
                getfp(brand_id);

                $("#loader").hide();
             }
          });
        return false;
     }

     function getbir(brand_id) {
     	var account_id=$('#account_id').val();
     	var search_key = $("#search_key").val();

     	$.ajax({
     		type: "POST",
     		url: "<?php echo base_url();?>front/dashboard/getbir", 
     		dataType: 'html',
     		data: '&search_key='+search_key+'&account_id='+account_id+'&brand_id='+brand_id,
     		success: function(data){
                //alert(data);
                $('#bir').html(data)            
             }
          });
     }

     function getbr(brand_id) {

     	var account_id=$('#account_id').val();
     	var search_key = $("#search_key").val();
     	$.ajax({
     		type: "POST",
     		url: "<?php echo base_url();?>front/dashboard/getbr", 
     		dataType: 'html',
     		data: '&search_key='+search_key+'&account_id='+account_id+'&brand_id='+brand_id,
     		success: function(data){
                //alert(data);
                $('#br').html(data)         
             }
          });
     }

     function getwip(brand_id) {

     	var account_id=$('#account_id').val();
     	var search_key = $("#search_key").val();
     	$.ajax({
     		type: "POST",
     		url: "<?php echo base_url();?>front/dashboard/getwip", 
     		dataType: 'html',
     		data: '&search_key='+search_key+'&account_id='+account_id+'&brand_id='+brand_id,
     		success: function(data){
                //alert(data);
                $('#wip').html(data)            
             }
          });
     }

     function getrw(brand_id) {

     	var account_id=$('#account_id').val();
     	var search_key = $("#search_key").val();
     	$.ajax({
     		type: "POST",
     		url: "<?php echo base_url();?>front/dashboard/getrw", 
     		dataType: 'html',
     		data: '&search_key='+search_key+'&account_id='+account_id+'&brand_id='+brand_id,
     		success: function(data){
                //alert(data);
                $('#rw').html(data)
             }
          });
     }

     function getpp(brand_id)
     {

     	var account_id=$('#account_id').val();
     	var search_key = $("#search_key").val();
     	$.ajax({
     		type: "POST",
     		url: "<?php echo base_url();?>front/dashboard/getpp", 
     		dataType: 'html',
     		data: '&search_key='+search_key+'&account_id='+account_id+'&brand_id='+brand_id,
     		success: function(data){
                //alert(data);
                $('#pp').html(data)         
             }
          });
     }

     function getwc(brand_id)
     {

     	var account_id=$('#account_id').val();
     	var search_key = $("#search_key").val();
     	$.ajax({
     		type: "POST",
     		url: "<?php echo base_url();?>front/dashboard/getwc", 
     		dataType: 'html',
     		data: '&search_key='+search_key+'&account_id='+account_id+'&brand_id='+brand_id,
     		success: function(data){
                //alert(data);
                $('#wc').html(data)         
             }
          });
     }

     function getfp(brand_id)
     {

     	var account_id=$('#account_id').val();
     	var search_key = $("#search_key").val();
     	$.ajax({
     		type: "POST",
     		url: "<?php echo base_url();?>front/dashboard/getfp", 
     		dataType: 'html',
     		data: '&search_key='+search_key+'&account_id='+account_id+'&brand_id='+brand_id,
     		success: function(data){
                //alert(data);
                $('#fp').html(data)         
             }
          });
     }
     var functionUpload = function(e,no,brief_id) {


        //alert("goko");
        

        //alert(brief_id);

        var responseText = $.ajax({
        	type:'POST',
        	url:"<?php echo base_url();?>front/dashboard/getTempFolder",
        //data: {'skufolderfile_nm':skufolderfile_nm},
        async: false
     }).responseText;

        $(this).attr("folder",responseText);


        var folder= $(this).attr('folder') ;

        //alert(folder);

        var files = e.target.files,
        filesLength = files.length;

        console.log(files);

        for (var i = 0; i < filesLength; i++) {
        	var file = files[i];



        	console.log(file);
        	console.log("bbbbbbbbbb");



        	if(file != undefined){
        		formData= new FormData();

        //var ext = $("#files"+no).val().split('.',2).pop().toLowerCase();

        //alert(ext);


        //if ($.inArray(ext, ['doc','docx']) == 1)
        /* if (ext=='doc' || ext=='docx')
        {*/
        	formData.append("doc_file", file);
        	formData.append("brief_id", brief_id);
        	formData.append("folder", folder);
        	formData.append("check", i);



        //alert("aaaa");
        $.ajax({
        	url: "<?php echo base_url();?>front/dashboard/upload_file",
        	type: "POST",
        	data: formData,
        	processData: false,
        	contentType: false,
        	success: function(response){

        //alert("koko");
        //alert(data);

        var responseArr=response.split("@@@@@@@@@@@");
        var doc_file_name=$.trim(responseArr[0]);
        var check=$.trim(responseArr[1]);

        


        var fileName =$('#file_nm_hid_'+brief_id).val();
        //alert(fileName);

        if(fileName=='')
        {
        	var fileNameCurrent=",,,"+doc_file_name+",,,";
        }
        else
        {
        	if(fileName.indexOf(",,,"+doc_file_name+",,,")==-1)
        	{
        		var fileNameCurrent=fileName+doc_file_name+",,,";
        	}
        	else
        	{
        		var fileNameCurrent=fileName;
        	}

        }

        $('#file_nm_hid_'+brief_id).val(fileNameCurrent);


        if(check==parseInt(filesLength)-1)
        {
        	functionMove(e,no,brief_id);
        }

        



     }
  });
        /*}else{

        alert("Please Upload Doc");
        return false;
        }
        */

     }
     else{
        //alert('Input something!');
     }



        //alert(i+"-----"+filesLength);

        

        
     }



    //functionMove(e,no,brief_id);
    //return deferred;
 }

 var functionMove = function(e,no,brief_id) {


    // alert("koko");


    var file_nm =$('#file_nm_hid_'+brief_id).val();

    //alert('&brief_id='+brief_id+'&temp_folder='+folder+'&file_nm='+file_nm);
    $("#loader").show();

    $.ajax({
    	method:"post",
    	url: "<?php echo base_url();?>front/dashboard/move_upload_file",
    	data:'&brief_id='+brief_id+'&temp_folder='+folder+'&file_nm='+file_nm,
    	success: function(result){

    //alert(result);
    $("#loader").hide();
    sortbrief();

    $('.re_upload_doc').on('change', function (e) {

    	var no = $(this).attr('alt') ;
    	var brief_id= $(this).attr('val') ;
    	functionUpload(e,no,brief_id);

    	/*functionUpload(e,no,brief_id).then(functionMove(e,no,brief_id));*/
    });


    //window.location.href="<?php echo base_url();?>dashboard";
 }
});



 }




 $('.re_upload_doc').on('change', function (e) {

 	var no = $(this).attr('alt') ;
 	var brief_id= $(this).attr('val') ;
 	functionUpload(e,no,brief_id);

 	/*functionUpload(e,no,brief_id).then(functionMove(e,no,brief_id));*/
 });



 $(".re_upload_doc_old").on("change", function(e) {

 //alert("goko");
 var no = $(this).attr('alt') ;
 var brief_id= $(this).attr('val') ;

//alert(brief_id);

var responseText = $.ajax({
	type:'POST',
	url:"<?php echo base_url();?>front/dashboard/getTempFolder",
        //data: {'skufolderfile_nm':skufolderfile_nm},
        async: false
     }).responseText;

$(this).attr("folder",responseText);


var folder= $(this).attr('folder') ;

 //alert(folder);

 var files = e.target.files,
 filesLength = files.length;

 console.log(files);
 for (var i = 0; i < filesLength; i++) {
 	var f = files[i]
 	var fileReader = new FileReader();
 	fileReader.onload = (function(e) {
 		var file = e.target;

 		console.log(file);
 		console.log("aaaaaaaaaaaaaa");




//alert(no);
var input = document.getElementById("files"+no);
//file = input.files[0];

for (var k = 0; k < filesLength; k++) {
	file = input.files[k];

	console.log(file);
	console.log("bbbbbbbbbb");



	if(file != undefined){
		formData= new FormData();

		var ext = $("#files"+no).val().split('.',2).pop().toLowerCase();

    //alert(ext);


    //if ($.inArray(ext, ['doc','docx']) == 1)
   /* if (ext=='doc' || ext=='docx')
   {*/
   	formData.append("doc_file", file);
   	formData.append("brief_id", brief_id);
   	formData.append("folder", folder);



  //alert("aaaa");
  $.ajax({
  	url: "<?php echo base_url();?>front/dashboard/upload_file",
  	type: "POST",
  	data: formData,
  	processData: false,
  	contentType: false,
  	success: function(data){

     //alert("koko");
     //alert(data);

     var doc_file_name=$.trim(data);


     var fileName =$('#file_nm_hid_'+brief_id).val();

     if(fileName=='')
     {
     	var fileNameCurrent=",,,"+doc_file_name+",,,";
     }
     else
     {
     	if(fileName.indexOf(",,,"+doc_file_name+",,,")==-1)
     	{
     		var fileNameCurrent=fileName+doc_file_name+",,,";
     	}
     	else
     	{
     		var fileNameCurrent=fileName;
     	}

     }

     $('#file_nm_hid_'+brief_id).val(fileNameCurrent);



  }
});
/*}else{

    alert("Please Upload Doc");
    return false;
}
*/

}
else{
// alert('Input something!');
$('#modal_msg').modal('show');
$('#txtMsg').html('Input something!');
}


}



});
 	fileReader.readAsDataURL(f);
 }






});




</script>