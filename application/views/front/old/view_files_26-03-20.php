<?php
session_start();
error_reporting(0);
if ($this->session->userdata('front_logged_in')) {
$session_data = $this->session->userdata('front_logged_in');
$user_id = $session_data['user_id'];
$user_name = $session_data['user_name'];
$_SESSION['user_id']=$user_id;
$_SESSION['user_name']=$user_name;
//$type1=$this->session->userdata('user_type_id');
 $type1 = $session_data['user_type_id'];
 $brand_access=$session_data['brand_access'];
	if($type1==3)
	{
		$job_title="Project Manager";
	}
	else
	{
		$job_title="Branch Manager";
	}
}
else
{
$user_id = '';
$user_name = ''; 
$_SESSION['user_id']=$user_id;
$_SESSION['user_name']=$user_name;
}
$wc_brief_sql = "SELECT * FROM `wc_brief` where brief_id='$brief_id'";
								$wc_brief_query = $this->db->query($wc_brief_sql);

								$wc_brief_result=$wc_brief_query->result_array();
								//print_r($wc_brief_result);
								// $wc_brief_qrydata = mysqli_fetch_array($wc_brief_query);
								$brief_title=$wc_brief_result[0]['brief_title'];
								//echo $brief_title." (ID: ".$brief_id.")";


?>

<?php $data['page_title'] = "upload image";

$this->load->view('front/includes/header',$data);
$this->load->view('front/includes/topnav');
 //echo $brief_id;
 ?>

<div class="container-fluid">
	<div class="row">

		<!-- Main Body -->
		<div class="col-12 col-sm-12 col-md-12">
			<main class="p-3">

				<div class="row">
					<div class="col-12 col-sm-12 col-md-12">
						<div class="p-3">
							<p class="text-wc lead"><i class="fas fa-arrow-alt-circle-left"></i><a href="dashboard">&nbsp;Back to Dashboard</a></p>
						</div>
					</div>
				</div>

				<div class="row">
					<div class="col-12 col-sm-12 col-md-12">
						<div class="p-3 text-wc">
							<h3 class="h3 text-uppercase">
								<?php echo $brief_title; ?> (ID: <?php echo $brief_id; ?>) - Project Manager
							</h3>
						</div>

						<div class="p-3 text-wc d-flex justify-content-start">
							<div class="p-1">
								<select class="custom-select">
									<option selected>Status</option>
									<option value="s1">Status 1</option>
									<option value="s2">Status 2</option>
									<option value="s3">Status 3</option>
								</select>
							</div>

							<div class="p-1">
								<select class="custom-select">
									<option selected>Download</option>
									<option value="d1">Download 1</option>
									<option value="d2">Download 2</option>
									<option value="d3">Download 3</option>
								</select>
							</div>

							<div class="p-1">
								<select class="custom-select" onchange="uploadimages()">
									<option selected>Upload</option>
									<option value="u1">Upload 1</option>
									<option value="u2">Upload 2</option>
									<option value="u3">Upload 3</option>
								</select>
							</div>

							<div class="p-1">
								<select class="custom-select">
									<option selected>Delete</option>
									<option value="del1">Delete 1</option>
									<option value="del2">Delete 2</option>
									<option value="del3">Delete 3</option>
								</select>
							</div>
						</div>
					</div>
				</div>

				<div class="row">
					<div class="col-12 col-sm-12 col-md-12">
						<div class="p-4">

							<div class="row">
							<?php 
							$wc_brief_sql = "SELECT * FROM `wc_image_upload` where brief_id='$brief_id'";
								$wc_brief_query = $this->db->query($wc_brief_sql);

								$wc_brief_result=$wc_brief_query->result_array();
								foreach($wc_brief_result as $keybill => $briefimages)
								{
									 $status=$briefimages['img_status'];
								?>
								
								<div class="col-12 col-sm-12 col-md-3 p-2">
									<div class="card">
										<!--<img src="/brief_upload/Bried_id/image/<?php echo $briefimages['image_path'] ?>" class="card-img-top" alt="<?php echo $briefimages['img_title'] ?>">-->
										<img src="https://placeimg.com/1366/1024/any" class="card-img-top" alt="<?php echo $briefimages['img_title'] ?>">
										<div class="card-body">
											<div class="d-flex justify-content-between">
												<span class="lead"><?php echo $briefimages['img_title']; ?></span>
                                             <?php if($status==2) { ?>
												<i class="fas fa-circle text-danger fa-2x"></i>
											 <?php } elseif($status==3) { ?>
												
												<i class="fas fa-sync-alt fa-2x"></i>
											<?php } elseif($status==1) { ?>
												<i class="fas fa-circle text-wc fa-2x"></i>
											<?php } ?>
											</div>
										</div>
									</div>
								</div>
								<?php } ?>



<!--
								<div class="col-12 col-sm-12 col-md-3 p-2">
									<div class="card">
										<img src="..." class="card-img-top" alt="...">
										<div class="card-body">
											<div class="d-flex justify-content-between">
												<span class="lead">Image Name</span>

												<i class="fas fa-sync-alt fa-2x"></i>
											</div>
										</div>
									</div>
								</div>

								<div class="col-12 col-sm-12 col-md-3 p-2">
									<div class="card">
										<img src="..." class="card-img-top" alt="...">
										<div class="card-body">
											<div class="d-flex justify-content-between">
												<span class="lead">Image Name</span>

												<i class="fas fa-circle text-wc fa-2x"></i>
											</div>
										</div>
									</div>
								</div>

								<div class="col-12 col-sm-12 col-md-3 p-2">
									<div class="card">
										<img src="..." class="card-img-top" alt="...">
										<div class="card-body">
											<div class="d-flex justify-content-between">
												<span class="lead">Image Name</span>

												<i class="fas fa-circle text-wc fa-2x"></i>
											</div>
										</div>
									</div>
								</div>

								<div class="col-12 col-sm-12 col-md-3 p-2">
									<div class="card">
										<img src="..." class="card-img-top" alt="...">
										<div class="card-body">
											<div class="d-flex justify-content-between">
												<span class="lead">Image Name</span>

												<i class="fas fa-circle text-danger fa-2x"></i>
											</div>
										</div>
									</div>
								</div>

								<div class="col-12 col-sm-12 col-md-3 p-2">
									<div class="card">
										<img src="..." class="card-img-top" alt="...">
										<div class="card-body">
											<div class="d-flex justify-content-between">
												<span class="lead">Image Name</span>

												<i class="fas fa-sync-alt fa-2x"></i>
											</div>
										</div>
									</div>
								</div>

								<div class="col-12 col-sm-12 col-md-3 p-2">
									<div class="card">
										<img src="..." class="card-img-top" alt="...">
										<div class="card-body">
											<div class="d-flex justify-content-between">
												<span class="lead">Image Name</span>

												<i class="fas fa-circle text-wc fa-2x"></i>
											</div>
										</div>
									</div>
								</div>
-->
							</div>
						</div>
					</div>
				</div>
			</main>
		</div>

	</div>
</div>
<script>
function sortbrief() {
$(document).ready(function() {
        var myCheckboxes = new Array();
        $("input:checked").each(function() {
           myCheckboxes.push($(this).val());
        });
		var brand_id = $("#brand_id").val()
        $.ajax({
            type: "POST",
            url: "<?php echo base_url();?>front/dashboard/getbriefsort", 
            dataType: 'html',
            data: 'brand_id='+brand_id+'&myCheckboxes='+myCheckboxes,
            success: function(data){
                $('#myResponse').html(data)
               getbir(brand_id);
			   getbr(brand_id);
			   getwip(brand_id);
			   getrw(brand_id);
			   getpp(brand_id);
			   getwc(brand_id);
			   getfp(brand_id);
            }
        });
		
        return false;
});
}
function getbir(brand_id)
{

	$.ajax({
            type: "POST",
            url: "<?php echo base_url();?>front/dashboard/getbir", 
            dataType: 'html',
            data: 'brand_id='+brand_id,
            success: function(data){
				//alert(data);
                $('#bir').html(data)			
            }
        });
}


function getbr(brand_id)
{
	$.ajax({
            type: "POST",
            url: "<?php echo base_url();?>front/dashboard/getbr", 
            dataType: 'html',
            data: 'brand_id='+brand_id,
            success: function(data){
				//alert(data);
                $('#br').html(data)			
            }
        });
}

function getwip(brand_id)
{
	$.ajax({
            type: "POST",
            url: "<?php echo base_url();?>front/dashboard/getwip", 
            dataType: 'html',
            data: 'brand_id='+brand_id,
            success: function(data){
				//alert(data);
                $('#wip').html(data)			
            }
        });
}


function getwc(brand_id)
{
	$.ajax({
            type: "POST",
            url: "<?php echo base_url();?>front/dashboard/getwc", 
            dataType: 'html',
            data: 'brand_id='+brand_id,
            success: function(data){
				//alert(data);
                $('#wc').html(data)			
            }
        });
}

function getrw(brand_id)
{
	$.ajax({
            type: "POST",
            url: "<?php echo base_url();?>front/dashboard/getrw", 
            dataType: 'html',
            data: 'brand_id='+brand_id,
            success: function(data){
				//alert(data);
                $('#rw').html(data)			
            }
        });
}

function getpp(brand_id)
{
	$.ajax({
            type: "POST",
            url: "<?php echo base_url();?>front/dashboard/getpp", 
            dataType: 'html',
            data: 'brand_id='+brand_id,
            success: function(data){
				//alert(data);
                $('#pp').html(data)			
            }
        });
}

function getfp(brand_id)
{
	$.ajax({
            type: "POST",
            url: "<?php echo base_url();?>front/dashboard/getfp", 
            dataType: 'html',
            data: 'brand_id='+brand_id,
            success: function(data){
				//alert(data);
                $('#fp').html(data)			
            }
        });
}


function uploadimages()
{
	//alert('hi');
		url="<?php echo base_url();?>uploadimages/"+<?php echo $brief_id; ?>;
	window.location = url;
}




function getbranddetails(brand_id)
{
	alert(brand_id);
}




</script>

<?php
$this->load->view('front/includes/footer');
?>
