<?php 
$session_data = $this->session->userdata('front_logged_in');
$user_type_id_current = $session_data['user_type_id'];
?>
<table class="table border-bottom text-wc" >
                        <thead>
                          <tr>
                            <th scope="col"></th>
                            <th scope="col">Category Id</th>
                            <th>Client Name</th>
                            <th>Category Type</th> 
                            <th>Category</th>  
                            <th>Brand level 1</th> 
                            <th>Brand level 2</th>       
                            <th>Status</th>
                          </tr>
                        </thead>
                        <tbody >

                        <?php
                        if(!empty($category_details1)){
                          //print_r($category_details1);
                          foreach($category_details1 as $key => $category_value){ 
                            $category_id=$category_value['category_id'];
                            $client_id_sel=$category_value['client_id'];
                            $category_type=$category_value['category_type'];
                            $parent_category_id=$category_value['parent_category_id'];
                            $grand_parent_category_id=$category_value['grand_parent_category_id'];
                            $category_name=$category_value['category_name'];
                            $status=$category_value['status'];
                        ?>
                      <tr>
                        <th scope="row">
                          <span id="categoryedit_<?php echo $category_id ?>" data-toggle="modal" data-target="#categoryeditModel<?php echo $category_id; ?>"><i class="far fa-edit"></i></span> 
                          <span id="categorydelete_<?php echo $category_id ?>" data-toggle="modal" data-target="#categorydeleteModel_<?php echo $category_id; ?>"><i class="far fa-trash-alt"></i></span>
                        </th>
                        <td><?php echo $category_id ?></td>
                        <td><?php
                          $sql="select * from wc_clients where client_id in ( ".trim($client_id_sel,",")." ) OR client_id LIKE '%,".trim($client_id_sel,",").",%'";
                          $client_name_details=$this->db->query($sql)->result_array();

                          $all_client_name ="<ul style=' margin-left: -25px;'>";
                          if(!empty($client_name_details)){
                            foreach($client_name_details as $key => $value){
                              $all_client_name.="<li>".$value['client_name']."</li>";
                            }
                            $all_client_name.="</ul>";
                             echo $all_client_name;
                          }
                        ?></td>

                        <td>
                          <?php 
                          if($category_type=='1'){
                            $category_type_name="Category";
                          }else if($category_type=='2'){
                            $category_type_name="Brand Level 1";
                          }else if($category_type=='3'){
                            $category_type_name="Brand Level 2";
                          }
                          echo $category_type_name;  ?>
                        </td>
                        <td>
                          <?php 
                          if($category_type=='1'){
                            $show_category_name=$category_name;
                          }else if($category_type=='2'){
                            $category_details=$this->db->query("select * from wc_category where category_id='$parent_category_id' and deleted='0' and status='Active' ")->result_array();
                            $parent_category_name=$category_details[0]['category_name'];
                            $show_category_name=$parent_category_name;
                          }else if($category_type=='3'){
                            $category_details=$this->db->query("select * from wc_category where category_id='$grand_parent_category_id' and deleted='0' and status='Active' ")->result_array();
                            $parent_category_name=$category_details[0]['category_name'];
                            $show_category_name=$parent_category_name;
                          }
                          echo $show_category_name;
                          ?>
                        </td>
                        <td>
                          <?php
                          if($category_type=='1'){
                            $parent_category_name='-';
                          }else if($category_type=='2'){

                            if($parent_category_id!='0'){
                              $parent_category_name=$category_name;
                            }else{
                              $parent_category_name="-";
                            }
                          }else if($category_type=='3'){
                            if($parent_category_id!='0'){
                              $category_details=$this->db->query("select * from wc_category where category_id='$parent_category_id' and deleted='0' and status='Active' ")->result_array();
                              $parent_category_name=$category_details[0]['category_name'];
                            }else{
                              $parent_category_name="-";
                            }
                          }
                          echo $parent_category_name;
                          ?>
                        </td>
                        <td>
                          <?php 
                          if($category_type=='1'){
                            $parent_sub_category_name='-';
                          }else if($category_type=='2'){
                            if($grand_parent_category_id!='0'){
                              $parent_sub_category_name=$category_name;
                            }else{
                              $parent_sub_category_name="-";
                            }
                          }else if($category_type=='3'){
                            if($grand_parent_category_id!='0'){
                              $parent_sub_category_name=$category_name;
                            }else{
                              $parent_sub_category_name="-";
                            }
                          }
                          echo $parent_sub_category_name;
                          ?>
                        </td>
                        <td><?php echo $status ?></td>
                       
                        <div class="modal fade" id="categoryeditModel<?php echo $category_id; ?>" tabindex="-1" role="dialog" aria-labelledby="categoryeditModel<?php echo $category_id; ?>Label" aria-hidden="true">
                          <div class="modal-dialog" role="document">
                            <div class="modal-content">
                                <?php  $array = array('id'=>'category_form'.$category_id, 'role'=>'form', 'class'=>'form  col-sm-12 form-horizontal has-validation-callback');
                                  echo form_open_multipart('User/insert_user', $array); ?>
                              <div class="modal-header">
                                <h5 class="modal-title" id="categoryeditModel<?php echo $category_id; ?>Label">Update User Details</h5>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                  <span aria-hidden="true">&times;</span>
                                </button>
                              </div>
                              <div class="modal-body">
                                <input type='hidden' name='category_id<?php  echo $category_id; ?>' id='category_id<?php  echo $category_id; ?>' value='<?php echo $category_id;?>'>

                                <?php
                                $users_details=$this->db->query("select * from wc_users where user_id='$user_id'")->result_array();
                                foreach($users_details as $key => $usersdetails){
                                    $client_id=$usersdetails['client_id'];
                                
                                    $client_details=$this->db->query("select * from wc_clients where deleted='0' and status='Active' and client_id='$client_id' order by  client_name asc")->result_array();
                                    //print_r("select * from wc_users where deleted='0' and status='Active' order by  client_name asc");
                                    if(!empty($client_details)){
                                        foreach($client_details as $key => $clientdetails){
                                          //$client_id=$clientdetails['client_id'];
                                          $client_name=$clientdetails['client_name'];
                                          ?>
                                         <input type="hidden" readonly name="client_id<?php echo $category_id; ?>" id="client_id<?php echo $category_id; ?>" value="<?php echo $client_id;?>">
                                  <?php }
                                    }
                                }
                                ?>
                               
                                <div class="form-group">
                                  <label for="inputEmail3"  class="col-form-label">Category Type <span style="color:#F00">*</span></label>
                                  <select name="category_type<?php echo $category_id; ?>" id="category_type<?php echo $category_id; ?>" class="custom-select" style="width: 100%"  onchange="getCategoryDiv('<?php echo $category_id ?>','<?php echo  $parent_category_id ?>','<?php echo $grand_parent_category_id ?>');">
                                    <option value=''>Select</option>
                                    <option value='1' <?php if($category_type=="1") { ?> selected="selected" <?php } ?>>Category</option>
                                    <option value='2' <?php if($category_type=="2") { ?> selected="selected" <?php } ?>>Brand level 1</option>
                                    <option value='3' <?php if($category_type=="3") { ?> selected="selected" <?php } ?>>Brand level 2</option>
                                  </select>
                                </div>
                                <div id="category_div<?php echo $category_id; ?>">
                                  <input type='hidden' name='parent_category_id<?php echo $category_id; ?>' value='0' id='parent_category_id<?php echo $category_id; ?>'>
                                  <div id="subcategory_div<?php echo $category_id; ?>">
                                    <input type='hidden' name='grand_parent_category_id<?php echo $category_id; ?>' value='0' id='grand_parent_category_id<?php echo $category_id; ?>'>
                                  </div>
                                </div>
                                <div class="form-group">
                                  <label for="inputEmail3"  class="col-form-label">Name <span style="color:#F00">*</span> </label>
                                  <input  type='text'  class="form-control" id="category_name<?php echo $category_id; ?>" name="category_name<?php echo $category_id; ?>"  value='<?php echo $category_name;?>'>
                                </div>
                                <input  type='hidden'  class="form-control" id="category_email<?php echo $category_id; ?>" name="category_email<?php echo $category_id; ?>">
                                <div class="form-group">
                                  <label for="inputEmail3"  class="col-form-label"><?php echo $lang['status'];?></label>
                                  <select name="status<?php echo $category_id; ?>" id="status<?php echo $category_id; ?>" class="form-control" >
                                     <!--<option value=''>Select</option>-->
                                     <option value="Active" <?php if($status=='Active') { ?> selected="selected" <?php } ?>  >Active</option>
                                     <option value="Inactive" <?php if($status=='Inactive') { ?> selected="selected" <?php } ?>   >Inactive</option>
                                  </select>
                                </div>  
                              </div>
                          
                              <div class="modal-footer">
                                <input class="m-1 btn btn-wc" type="submit" value="Update">
                                <button type="button" class="btn btn-outline-wc" data-dismiss="modal">&emsp;Close&emsp;</button>
                              </div>
                              <?php echo form_close();?> 
                            </div>
                          </div>
                        </div>
                    </tr>
                    <div class="modal fade" id="categorydeleteModel_<?php  echo $category_id; ?>" tabindex="-1" role="dialog" aria-hidden="true">
                            <div class="modal-dialog" role="document">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <h5 class="modal-title text-dark">Are you sure you want to delete?</h5>
                                        <button type="button" class="close text-danger" data-dismiss="modal" aria-label="Close">
                                            <span aria-hidden="true" >&times;</span>
                                        </button>
                                    </div>
                                    <div class="modal-footer">
                                      <button type="button" id="delUsr" class="btn btn-outline-wc" onclick="delete_category('<?php  echo $category_id; ?>');" data-dismiss="modal">&emsp;Delete&emsp;</button>
                                        <button type="button" class="btn btn-outline-wc" data-dismiss="modal">&emsp;Close&emsp;</button>
                                        
                                    </div>
                                </div>
                            </div>
                        </div>
                        <script type="text/javascript">
                         // selectUserType1('<?php echo $category_id; ?>','<?php echo $client_id; ?>','<?php echo $brand_id; ?>');

                           // selectUserType1('<?php echo $category_id; ?>','<?php echo $client_id; ?>','<?php echo $brand_id; ?>');
     jQuery(function ($) {
    //$('#user_type_id').select2();

    jQuery.validator.addMethod("alphanumeric", function(value, element) {
        return this.optional(element) || /^[a-zA-Z\-\s]+$/.test(value);
        }, "It must contain only letters."); 
    
    jQuery.validator.addMethod("atLeastOneLetter", function (value, element) {
    return this.optional(element) || /[a-zA-Z]+/.test(value);
    }, "Must have at least one  letter");
    
    jQuery.validator.addMethod("phoneno", function(value, element) {
        return this.optional(element) || /^[0-9+() ]*$/.test(value);
        }, "Enter Valid  phone number."); 
    
    
    
      $('#category_form'+<?php  echo $category_id; ?>).validate({
        rules: {
          client_id:{
            required:true,
          },
          category_type:
          {
            required:true,
          },
          parent_category_id:
          {
            required:true,
          },
          grand_parent_category_id:
          {
            required:true,
          },
          category_name: {
            required: true,
          },
          status: {
            required: true,
          }
      },
      messages: {
          },
      submitHandler: function(form) {
        var category_id=$('#category_id'+<?php  echo $category_id; ?>).val();
        var client_id=$('#client_id'+<?php  echo $category_id; ?>).val();
        var category_name=$('#category_name'+<?php  echo $category_id; ?>).val();
        var status=$('#status'+<?php  echo $category_id; ?>).val();
        var category_type=$('#category_type'+<?php  echo $category_id; ?>).val();
        var parent_category_id=$('#parent_category_id'+<?php  echo $category_id; ?>).val();
        var grand_parent_category_id=$('#grand_parent_category_id'+<?php  echo $category_id; ?>).val();
       
        $.ajax({
          method:'post',
          url: "<?php echo base_url();?>front/settings/insert_category", 
          data:"&category_id="+category_id+"&client_id="+client_id+"&category_name="+category_name+"&status="+status+"&category_type="+category_type+"&parent_category_id="+parent_category_id+"&grand_parent_category_id="+grand_parent_category_id,
          success: function(result){
            if(result){
                var url="<?php echo base_url(); ?>settings#category";
                window.location.replace(url);
                location.reload();
              }
          }
        });
      } 
    });
  });

      <?php
  if($parent_category_id!="0" && $parent_category_id!="")
  {
   ?>
 getCategoryDiv('<?php  echo $category_id; ?>','<?php echo  $parent_category_id ?>','<?php echo $grand_parent_category_id ?>');
 <?php
 }
 ?>
                        </script>
                        
                    
<?php }
}
else{
  ?>
  <tr>
   
    <td colspan="10">No Records</td>
  </tr>
  <?php
}
?>
</tbody>
                      </table>





