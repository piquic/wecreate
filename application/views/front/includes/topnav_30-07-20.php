<?php
$class_name=$this->router->fetch_class();
$method_name=$this->router->fetch_method();


if ($this->session->userdata('front_logged_in')) {
	$session_data = $this->session->userdata('front_logged_in');
	$user_id = $session_data['user_id'];
	$user_name = $session_data['user_name'];
	$type1=$this->session->userdata('type1');
}
else
{
	$user_id = '';
	$user_name = '';
}


$temp = explode(' ', $user_name);

if(isset($temp[1]) != '')
	$uico = substr($temp[0], 0, 1) . substr($temp[1], 0, 1);
else
	$uico = substr($temp[0], 0, 2); 



?>

<header class="bg-light border-bottom">
	<div class="container-fluid">
		<div class="row">
			<div class="col-12 col-sm-12 col-md-12">
				<nav class="navbar navbar-expand-lg navbar-light bg-light text-center">

					<?php
					$session_data = $this->session->userdata('front_logged_in');
					$user_id = $session_data['user_id'];
					$user_name = $session_data['user_name'];

					$type1 = $session_data['user_type_id'];
					$user_type_id=$type1;
					$client_access=$session_data['client_access'];

					if($client_access!='' && $user_type_id!='') {
						$checkquerys = $this->db->query("select * from wc_clients where client_id='$client_access' ")->result_array();

						@$client_image=$checkquerys[0]['client_image'];

						$header_logo=base_url()."/upload_client_logo/".$client_image;
					}
					else
					{
						$header_logo=base_url()."/website-assets/images/logo.png";
					}
					
					?>
					<a class="navbar-brand" href="<?php echo base_url();?>dashboard">
						<div class="d-flex justify-content-around" style="height: 50px; width: 100px; text-align: center; background: url(<?php echo $header_logo;?>) no-repeat center; background-size: contain;">
							<!-- <img class="h-100" src="<?php // echo $header_logo;?>"> -->
						</div>
					</a>
					<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
						<span class="navbar-toggler-icon"></span>
					</button>

					<?php
					if($type1=='1') {
						$query=$this->db->query("select * from wc_settings WHERE setting_key='WEKAN_URL_LINK' ");
						$settingDetails= $query->result_array();
						$WEKAN_URL_LINK=$settingDetails[0]['setting_value'];


						$wekan_link_url=$WEKAN_URL_LINK;
					} else {
						$query=$this->db->query("select * from wc_settings WHERE setting_key='WEKAN_URL_LINK' ");
						$settingDetails= $query->result_array();
						$WEKAN_URL_LINK=$settingDetails[0]['setting_value'];

						$session_data = $this->session->userdata('front_logged_in');
						$user_id = $session_data['user_id'];
						$user_name = $session_data['user_name'];

						$type1 = $session_data['user_type_id'];
						$user_type_id=$type1;
						$client_access=$session_data['client_access'];

						$checkquerys = $this->db->query("select * from wc_clients where client_id='$client_access' ")->result_array();

						@$wekan_client_id=$checkquerys[0]['wekan_client_id'];
						//$client_name=$checkquerys[0]['client_name'];
						@$client_name=str_replace(" ","-",strtolower($checkquerys[0]['client_name']));


						$wekan_link_url=$WEKAN_URL_LINK."b/".$wekan_client_id."/".$client_name;	
					}


					$query=$this->db->query("select * from wc_settings WHERE setting_key='REPORTICO_LINK' ");
					$settingDetails= $query->result_array();
					$REPORTICO_LINK=$settingDetails[0]['setting_value'];
					?>
					<div class="collapse navbar-collapse" id="navbarSupportedContent">
						<ul class="navbar-nav mr-auto">
							<li class="nav-item pl-1 pr-2">
								<a class="nav-link lead <?php if($class_name == 'dashboard'){ echo "tn active"; } else { echo "text-dark"; } ?>" href="<?php echo base_url();?>dashboard">Dashboard</a>
							</li>

							<?php if($type1=='2') { ?>
							<li class="nav-item pl-1 pr-2">
								<a class="nav-link lead <?php if($class_name == 'reports'){ echo "tn active"; } else { echo "text-dark"; } ?>" href="<?php echo base_url();?>reports">Reports</a>
							</li>
							<?php } ?>

							<?php if($type1=='2' || $type1=='3') { ?>
							<li class="nav-item pl-1 pr-2">
								<a class="nav-link lead <?php if($class_name == 'team'){ echo "tn active"; } else { echo "text-dark"; } ?>" data-toggle="collapse" href="#pm_log_div">Logs</a>
							</li>
							<?php } ?>
						</ul>

						<?php if ($this->session->userdata('front_logged_in')) { ?>

						<div class="dropdown p-2 py-3 mt-1 pr-3 ml-auto">
							<a class="text-dark lead dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
								<!-- <?php //echo $user_name;?> <span class="usr-ico"><?php  //echo substr(strtoupper($user_name), 0, 1);  // abcd;?></span> -->
								<?php echo $user_name;?> <span class="usr-ico" style="height: 32px; width: 32px; line-height: 32px; font-size: 14px;"><?php  echo strtoupper($uico);?></span>
							</a>
							<div class="dropdown-menu" aria-labelledby="navbarDropdown">
								<a class="dropdown-item" href="<?php echo base_url();?>myaccount">My Account</a>

								<?php if($user_type_id=='2'){ ?>
								<a class="dropdown-item" href="<?php echo base_url();?>settings">Settings</a>
								<?php } ?>

								<a class="dropdown-item" href="<?php echo base_url();?>logout">Logout</a>
							</div>
						</div>

						<?php } ?>
					</div>
				</nav>
			</div>

			<?php /*<div class="col-12 col-sm-12 col-md-3">
				<?php if ($this->session->userdata('front_logged_in')) { ?>

				<div class="dropdown text-center p-2 py-3 mt-1 pr-3">
					<a class="text-dark dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
						<!-- <?php //echo $user_name;?> <span class="usr-ico"><?php  //echo substr(strtoupper($user_name), 0, 1);  // abcd;?></span> -->
						<?php echo $user_name;?> <span class="usr-ico" style="height: 32px; width: 32px; line-height: 32px; font-size: 14px;"><?php  echo strtoupper($uico);?></span>
					</a>
					<div class="dropdown-menu" aria-labelledby="navbarDropdown">
						<a class="dropdown-item" href="<?php echo base_url();?>myaccount">My Account</a>

						<?php if($user_type_id=='2'){ ?>
						<a class="dropdown-item" href="<?php echo base_url();?>settings">Settings</a>
						<?php } ?>

						<a class="dropdown-item" href="<?php echo base_url();?>logout">Logout</a>
					</div>
				</div>

				<?php } ?>
			</div>*/?>
		</div>
	</div>

	<?php /*<div class="d-flex justify-content-between flex-wrap">
		<div class="d-flex justify-content-start flex-wrap">
			<div class="p-2">

				<?php
				$session_data = $this->session->userdata('front_logged_in');
				$user_id = $session_data['user_id'];
				$user_name = $session_data['user_name'];

				$type1 = $session_data['user_type_id'];
				$user_type_id=$type1;
				$client_access=$session_data['client_access'];

				if($client_access!='' && $user_type_id!='') {
					$checkquerys = $this->db->query("select * from wc_clients where client_id='$client_access' ")->result_array();

					@$client_image=$checkquerys[0]['client_image'];

					$header_logo=base_url()."/upload_client_logo/".$client_image;
				}
				else
				{
					$header_logo=base_url()."/website-assets/images/logo.png";
				}
				
				?>
				<a class="" href="<?php echo base_url();?>dashboard">
					<div class="d-flex justify-content-around" style="height: 50px; width: 100px; text-align: center; background: url(<?php echo $header_logo;?>) no-repeat center; background-size: contain;">
						<!-- <img class="h-100" src="<?php // echo $header_logo;?>"> -->
					</div>
				</a>
			</div>

			<?php
			if($type1=='1') {
				$query=$this->db->query("select * from wc_settings WHERE setting_key='WEKAN_URL_LINK' ");
				$settingDetails= $query->result_array();
				$WEKAN_URL_LINK=$settingDetails[0]['setting_value'];


				$wekan_link_url=$WEKAN_URL_LINK;
			} else {
				$query=$this->db->query("select * from wc_settings WHERE setting_key='WEKAN_URL_LINK' ");
				$settingDetails= $query->result_array();
				$WEKAN_URL_LINK=$settingDetails[0]['setting_value'];

				$session_data = $this->session->userdata('front_logged_in');
				$user_id = $session_data['user_id'];
				$user_name = $session_data['user_name'];

				$type1 = $session_data['user_type_id'];
				$user_type_id=$type1;
				$client_access=$session_data['client_access'];

				$checkquerys = $this->db->query("select * from wc_clients where client_id='$client_access' ")->result_array();

				@$wekan_client_id=$checkquerys[0]['wekan_client_id'];
				//$client_name=$checkquerys[0]['client_name'];
				@$client_name=str_replace(" ","-",strtolower($checkquerys[0]['client_name']));


				$wekan_link_url=$WEKAN_URL_LINK."b/".$wekan_client_id."/".$client_name;	
			}


			$query=$this->db->query("select * from wc_settings WHERE setting_key='REPORTICO_LINK' ");
			$settingDetails= $query->result_array();
			$REPORTICO_LINK=$settingDetails[0]['setting_value'];
			?>

			<div class="p-2 py-4">
				<div class="d-flex justify-content-start flex-wrap">
					<a class="lead pt-1 pl-2 pr-3 <?php if($class_name == 'dashboard'){ echo "tn active"; } else { echo "text-dark"; } ?>" href="<?php echo base_url();?>dashboard">Dashboard</a>

					<?php if($type1=='2') { ?>
					 <a class="lead pt-1 pl-2 pr-3 <?php if($class_name == 'reports'){ echo "tn active"; } else { echo "text-dark"; } ?>"  role="button" href="<?php echo base_url();?>reports">Reports</a>
					<?php
					}
					?>
					

					<?php
					if($type1=='2' || $type1=='3')
			        {
					?>
					 <a class="lead pt-1 pl-2 pr-3 <?php if($class_name == 'team'){ echo "tn active"; } else { echo "text-dark"; } ?>" data-toggle="collapse" role="button" href="#pm_log_div" target="_blank">Logs</a>
					<?php
					}
					?>


					<?php
					if($type1!='4')
			        {
					?>
					<!--<a class="lead pt-1 pl-2 pr-3 <?php if($class_name == 'team'){ echo "tn active"; } else { echo "text-dark"; } ?>" href="<?php echo $wekan_link_url;?>" target="_blank">Team</a>-->
					<?php
					}
					?>


					<!-- <a class="lead pt-1 pl-2 pr-3 <?php if($class_name == 'report'){ echo "tn active"; } else { echo "text-dark"; } ?>" href="<?php echo $REPORTICO_LINK;?>" target="_blank">Report</a>					 -->
				</div>
			</div>
		</div>

		<?php if ($this->session->userdata('front_logged_in')) { ?>

		<div class="dropdown text-center p-2 py-3 mt-1 pr-3">
			<a class="text-dark dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
				<!-- <?php //echo $user_name;?> <span class="usr-ico"><?php  //echo substr(strtoupper($user_name), 0, 1);  // abcd;?></span> -->
				<?php echo $user_name;?> <span class="usr-ico" style="height: 32px; width: 32px; line-height: 32px; font-size: 14px;"><?php  echo strtoupper($uico);?></span>
			</a>
			<div class="dropdown-menu" aria-labelledby="navbarDropdown">
				<a class="dropdown-item" href="<?php echo base_url();?>myaccount">My Account</a>

				<?php if($user_type_id=='2'){ ?>
				<a class="dropdown-item" href="<?php echo base_url();?>settings">Settings</a>
				<?php } ?>

				<a class="dropdown-item" href="<?php echo base_url();?>logout">Logout</a>
			</div>
		</div>

		<?php } ?>
	</div> */?>
</header>




<div id="pm_log_div" class="collapse border rounded-lg pt-3 px-3 bg-white" style="width: 25rem;top: 8%;left: 15%; position: absolute; z-index: 1113;">
	<div class="d-flex justify-content-around">
		<p class="flex-grow-1 text-center font-weight-bold">Display Logs</p>
		<span data-toggle="collapse" data-target="#pm_log_div" aria-expanded="false" aria-controls="pm_log_div"><i class="fas fa-times"></i></span>
	</div>

	<hr class="border p-0 mt-0">

	<div class="bg-light" style="min-height: 25rem; margin: 0 0 1rem 0;">
		<div class="mb-3 " id="viewlogdiv">
			<div class="input-group mb-2" >
				<div class="input-group-prepend">
					<div class="input-group-text">From&nbsp;&nbsp;<i class="far fa-calendar-alt"></i></div>
				</div>
				<?php  $sel_brief_due_date=date("d M Y"); ?>
				<input type="text" class="form-control datePick " name="log_date" id="view_log_fromdate" value="<?php echo $sel_brief_due_date;?>" placeholder="<?php echo $sel_brief_due_date;?>" readonly onchange="listallviewlogs()">

				<script type="text/javascript">
					$(function () {
						var date = new Date();
						date.setDate(date.getDate());

						$('#view_log_fromdate').datetimepicker({
							weekStart: 1,
							format: 'dd M yyyy',
							todayBtn:  1,
							autoclose: 1,
							todayHighlight: 1,
							startView: 2,
							forceParse: 0,
							showMeridian: 1,
							minView: 2,
							pickTime: false,
							startDate: '01/01/2020',
							endDate: date
						});
					});
				</script>
			</div>



			<div class="input-group mb-2" >
				<div class="input-group-prepend">
					<div class="input-group-text">To&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<i class="far fa-calendar-alt"></i></div>
				</div>
				<?php  $sel_brief_due_date=date("d M Y"); ?>
				<input type="text" class="form-control datePick " name="log_date" id="view_log_todate" value="<?php echo $sel_brief_due_date;?>" placeholder="<?php echo $sel_brief_due_date;?>" readonly onchange="listallviewlogs()">

				<script type="text/javascript">
					$(function () {
						var date = new Date();
						date.setDate(date.getDate());

						$('#view_log_todate').datetimepicker({
							weekStart: 1,
							format: 'dd M yyyy',
							todayBtn:  1,
							autoclose: 1,
							todayHighlight: 1,
							startView: 2,
							forceParse: 0,
							showMeridian: 1,
							minView: 2,
							pickTime: false,
							startDate: '01/01/2020',
							endDate: date
						});
					});
				</script>
			</div>
		</div>

		<select class="custom-select" name="empl_list" id="employee_list" onchange="listallviewlogs()" multiple>
			<!--<option value="" disabled="disabled">Select Creator User</option>-->
			<option value="" onclick="selectAll()"><b>Select ALL</b></option>
			<?php 
			$session_data = $this->session->userdata('front_logged_in');
			$user_id= $session_data['user_id'];
			$checkquerys = $this->db->query("select * from wc_users where  user_id='$user_id' ")->result_array();
			$client_id = $checkquerys[0]['client_id'];
			$user_details=$this->db->query("select * from wc_users where user_type_id=6")->result_array(); 	
			$i=0;
			foreach ($user_details as $row) {
				$emp_id = $user_details[$i]['user_id'];		
				$emp_name = $user_details[$i]['user_name'];
				$emp_client_id = $user_details[$i]['client_id'];
				if($emp_client_id!='0')
				{
					$emp_client_id_ar = explode(',', $emp_client_id);
					if (in_array($client_id, $emp_client_id_ar))
					{

			?>

			<option value="<?php echo $emp_id; ?>"><?php echo $emp_name ?></option>

			<?php } } $i++; } ?>
		</select>

		<!-- <input type="button" name="Button" value="Select All" id="select_all_button" onclick="selectAll()" /> -->
		<!--<button class="btn btn-wc btn-sm my-2" type="button" name="Button" id="select_all_button" onclick="selectAll()">Select All</button>-->

		<hr class="border p-0 mt-0 mb-3">

		<div id="listallsortview">
			<div class="mb-3 scrl d-none" style="max-height: 255px;" >
				<p class="w-100 font-weight-bold text-center text-wc"><?php echo date("d M Y"); ?></p>
				<?php

				$log_details=$this->db->query("select wc_brieflogs.log_id,wc_brieflogs.brief_id,wc_brieflogs.worked_hours,wc_brieflogs.worked_mins,wc_brief.brief_title from wc_brieflogs INNER JOIN wc_brief WHERE wc_brieflogs.brief_id=wc_brief.brief_id")->result_array(); 	
				if(!empty($log_details))
				{
					$hh=0;
					$mm=0;
					foreach($log_details as $key => $viewlogdetails)
					{
						$brief_id=$viewlogdetails['brief_id'];
						$brief_title=$viewlogdetails['brief_title'];
						$worked_hours=$viewlogdetails['worked_hours'];
						$worked_mins=$viewlogdetails['worked_mins'];
						$id_log=$viewlogdetails['log_id'];
						$hh+=$worked_hours;
						$mm+=$worked_mins;

						$button='<span class="btn btn-default p-0 remove_button432" id="delete_270" onclick="removelog('.$id_log.','.$brief_id.')" title="Delete"><i class="far fa-trash-alt text-wc"></i></span>';
				?>

				<div class="d-flex justify-content-between flex-wrap">
					<div class="py-1 text-wc">
						<span>#P<?php echo $brief_id; ?>&nbsp;<?php echo $brief_title; ?></span>
					</div>

					<div class="d-flex justify-content-between">
						<span class="px-2 pt-1 text-wc"><?php echo $worked_hours.":".$worked_mins; ?></span>
						<?php //echo $button; ?>
					</div>
				</div>
				<?php } ?>

				<hr class="border p-0 mt-0">

				<div class="d-flex justify-content-between flex-wrap" style="margin-bottom:1rem">
					<div class="py-1 text-wc">
						<span>Total working Hours</span>
					</div>

					<div class="d-flex justify-content-between">
						<span class="px-2 pt-1 text-wc"><?php echo $hh.":".$mm; ?></span>
						<?php //echo $button; ?>
					</div>
				</div>

				<hr class="border p-0 mt-0">

				<?php } else {
				echo '<p class="w-100 font-weight-bold text-center text-wc">No logs found</p>';
				} ?>
			</div>
		</div>
	</div>		
</div>

<script>

function selectAll() { 
    $('#employee_list option').prop('selected', true);
	//$("#select_all_button").attr("onclick","deselectAll()");
	//$("#select_all_button").attr("value","Unselect All");
     listallviewlogs();
    }
function deselectAll()
{
	//selectAll(document.getElementById('selectbox2'),false));
	    $('#employee_list option').prop('selected', false);
		//$("#select_all_button").attr("onclick","selectAll()");
		//$("#select_all_button").attr("value","Select All");
        listallviewlogs();
	
}
function listallviewlogs()
{
	
	var logfromdate=$('#view_log_fromdate').val();
	var logtodate=$('#view_log_todate').val();
	var emp_id=$('#employee_list').val();
	//alert($('#employee_list').val());
	//alert('logfromdate='+logfromdate+'&logtodate='+logtodate+'&emp_id='+emp_id);
	$.ajax({
		type: "POST",
		url: "<?php echo base_url();?>front/dashboard/listalllog", 
		dataType: 'html',
		data: 'logfromdate='+logfromdate+'&logtodate='+logtodate+'&emp_id='+emp_id,
		success: function(data){
			$('#listallsortview').html(data);
			//alert(data);

		}
	});	
}
</script>

