<!DOCTYPE html>
<html lang="en">
	<head>
		<!-- Required meta tags -->
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
		<!-- Bootstrap CSS -->
		<link rel="stylesheet" href="<?php echo base_url(); ?>website-assets/css/bootstrap.min.css">

		<!-- <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.1/css/all.css" integrity="sha384-50oBUHEmvpQ+1lW4y57PTFmhCaXp0ML5d60M1M7uH2+nqUivzIebhndOJK28anvf" crossorigin="anonymous"> -->
		<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>website-assets/fontawsome/css/all.css">

		<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>website-assets/css/custom.css">

		<title><?php echo ucwords($page_title); ?> - Piquic</title>

<!-- 		<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script> -->
		<script src="https://code.jquery.com/jquery-1.12.4.js"></script>
	<!-- 	<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>   -->
		<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>


		<!-- <?php
		//$session_data = $this->session->userdata('front_logged_in');
		//$user_id = $session_data['user_id'];
		//if($user_id!='')
		//{
		?>	
		<script type="text/javascript">


		setInterval(function(){ 
		var session_id="<?php //echo $user_id;?>"; 
		var responseText = $.ajax({
		type:'POST',
		url:"<?php //echo base_url();?>front/home/checkSession",
		data: {},
		async: false
		}).responseText;

		if($.trim(responseText)=='0')
		{
		window.location.href="<?php //echo base_url();?>";
		}


		}, 500);
		</script>

		<?php
		//}
		?> -->

		<script type="text/javascript">

			function myCheckBrowserFunction() {

//console.log(navigator.userAgent);
if((navigator.userAgent.indexOf("Opera") || navigator.userAgent.indexOf('OPR')) != -1 )
{
//alert('Opera');
return 'Opera';
}
else if(navigator.userAgent.indexOf("Chrome") != -1 )
{
//alert('Chrome');
return 'Chrome';
}
else if(navigator.userAgent.indexOf("Safari") != -1)
{
//alert('Safari');
return 'Safari';
}
else if(navigator.userAgent.indexOf("Firefox") != -1 )
{
//alert('Firefox');
return 'Firefox';
}
else if((navigator.userAgent.indexOf("MSIE") != -1 ) || (!!document.documentMode == true )) //IF IE > 10
{
//alert('IE');
return 'IE';
}
else
{
//alert('unknown');
return 'unknown';
}
}
</script>

	</head>

	<body id="<?php echo str_replace(" ","-",$page_title); ?>">
		<?php //$this->load->view('front/includes/menu'); ?>

		<div id="loader" style="display: none;"><i class="fas fa-spinner fa-spin icon"></i></div>

		<div class="modal fade" id="modal_msg" tabindex="-1" role="dialog" aria-hidden="true">
			<div class="modal-dialog" role="document">
				<div class="modal-content">
					<div class="modal-header">
						<h5 class="modal-title text-dark" id="txtMsg"></h5>
						<button type="button" class="close text-wc" data-dismiss="modal" aria-label="Close">
							<span aria-hidden="true" >&times;</span>
						</button>
					</div>
				</div>
			</div>
		</div>