<!-- Optional JavaScript -->
    <script type="text/javascript" src="<?php echo site_url(); ?>website-assets/js/popper.min.js?<?php echo date('l jS \of F Y h:i:s A'); ?>"></script>
    <script type="text/javascript" src="<?php echo site_url(); ?>website-assets/js/bootstrap.min.js?<?php echo date('l jS \of F Y h:i:s A'); ?>"></script>
    <script type="text/javascript" src="<?php echo site_url(); ?>website-assets/js/jquery.validate.min.js?<?php echo date('l jS \of F Y h:i:s A'); ?>"></script> 
    
<script type="text/javascript" src="<?php echo base_url(); ?>website-assets/datetimepicker/js/bootstrap-datetimepicker.js?<?php echo date('l jS \of F Y h:i:s A'); ?>" charset="UTF-8"></script>
<!--<script type="text/javascript" src="<?php //echo base_url(); ?>website-assets/datetimepicker/js/locales/bootstrap-datetimepicker.fr.js?<?php //echo date('l jS \of F Y h:i:s A'); ?>" charset="UTF-8"></script>-->


    <script type="text/javascript" src="<?php echo site_url(); ?>website-assets/js/custom.js?<?php echo date('l jS \of F Y h:i:s A'); ?>"></script>



    <script type="text/javascript">
    	$(document).ready(function(){
    		$(window).keydown(function(event)
    		{
    			if((event.keyCode == 107 && event.ctrlKey == true) || (event.keyCode == 109 && event.ctrlKey == true)||( event.ctrlKey == true && (event.which == '61' || event.which == '107' || event.which == '173' || event.which == '109'  || event.which == '187'  || event.which == '189'  ))){
    				event.preventDefault();
			        //Your logic here
			        console.log("Zoom Feature Disabled. Ctrl+key Pressed.");
			    }
			});

		    //Prevent zoom while using ctrl and mouse wheel
		    $(window).bind('mousewheel DOMMouseScroll', function(e)
		    {
			    //ctrl key is pressed
			    if(e.ctrlKey == true){
			    	e.preventDefault();
			        //Your logic here
			        console.log("Zoom Feature Disabled. Ctrl+Mousewheel Pressed.");        
			    }
			});
    	});
    </script>

    <script type="text/javascript">
    	function circular_progress() {

			var value = $("#circular_progress_data").attr('data-value');
			console.log(value);
			var left = $("#circular_progress_data").find('.prog-ress-left .prog-ress-bar');
			var right = $("#circular_progress_data").find('.prog-ress-right .prog-ress-bar');

			if (value > 0) {
				if (value <= 50) {
					right.css('transform', 'rotate(' + percentageToDegrees(value) + 'deg)')
				} else {
					right.css('transform', 'rotate(180deg)')
					left.css('transform', 'rotate(' + percentageToDegrees(value - 50) + 'deg)')
				}
			}
		}

		function percentageToDegrees(percentage) {

			return percentage / 100 * 360

		}

	</script>
</body>
</html>