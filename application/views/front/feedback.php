<?php
session_start();
error_reporting(0);

//print_r($_SESSION);

/*if ($this->session->userdata('front_logged_in')) {
$session_data = $this->session->userdata('front_logged_in');
$user_id = $session_data['user_id'];
$user_name = $session_data['user_name'];
$_SESSION['user_id']=$user_id;
$_SESSION['user_name']=$user_name;
//$type1=$this->session->userdata('user_type_id');
  $type1 = $session_data['user_type_id'];
 $user_type_id=$type1;
 $brand_access=$session_data['brand_access'];

  $checkquerys = $this->db->query("select user_type_name from wc_users_type where  user_type_id='$user_type_id' ")->result_array();
$job_title=$checkquerys[0]['user_type_name'];
	
}
else
{
$user_id = '';
$user_name = ''; 
$_SESSION['user_id']=$user_id;
$_SESSION['user_name']=$user_name;
}*/
?>

<?php $data['page_title'] = "Feedback";

$this->load->view('front/includes/header',$data);
$this->load->view('front/includes/topnav');
 ?>

<link rel="stylesheet" href="<?php echo base_url(); ?>website-assets/startRatingJquery/demo/styles.css">
<style type="text/css">
.gl-star-rating-text
{
display: block;
text-align: center;
}

 	
.btn-wc-green {
color: #fff !important;
background-color: #76aea1 !important;
border-color: #76aea1 !important;
}

 </style>

<div class="container-fluid">
	<div class="row">

		<!-- Main Body -->
		<div class="col-12 col-sm-12 col-md-12">
			<main class="p-3">

				<div class="row">
					<div class="col-12 col-sm-12 col-md-12">
						<div class="p-3">

							<?php
							if ($this->session->userdata('front_logged_in')) {
							?>
							<p class="text-wc lead" onclick="goDashboard();"  style="cursor: pointer;"><i class="fas fa-arrow-alt-circle-left"></i>&nbsp;Back to Dashboard</p>
							<?php
						}
						?>
						</div>
					</div>
				</div>

				<div class="container">

					<div class="row">
						<div class="col-12 col-sm-12 col-md-12">
							<div class="p-3 text-center">
								<h3 class="display-4">
									<!-- WeCreate #<?php echo $brief_id;?> -->
									<?php
									$checkquerys = $this->db->query("select * from wc_brief where  brief_id='$brief_id' ")->result_array();
									$brief_title=$checkquerys[0]['brief_title'];
									?>
									#<?php echo $brief_id;?> - <?php echo $brief_title;?>
								</h3>

								<p class="lead">
									<?php echo $pagecontents[0]['content'] ?>
								</p>
							</div>
						</div>
					</div>

					<?php  $array = array('id'=>'feedback_form', 'role'=>'form', 'class'=>'form form-horizontal has-validation-callback');
				    echo form_open_multipart('front/feedback/insertFeedback', $array); ?>

					<input type="hidden" name="brief_id" id="brief_id" value="<?php echo $brief_id;?>">
					<input type="hidden" name="user_id" id="user_id" value="<?php echo $user_id;?>">

					<div class="row">


						<?php

					$feedback_type_sql = "SELECT * FROM `wc_feedback_type` where status='Active' and deleted='0' ";

					$feedback_type_query = $this->db->query($feedback_type_sql);
					$feedback_type_result=$feedback_type_query->result_array();

					if(!empty($feedback_type_result))
					{
					foreach($feedback_type_result as $key => $value)
					{
					$feedback_type_id=$value['feedback_type_id'];
					$feedback_type=$value['feedback_type'];

						?>



						<div class="col-12 col-sm-12 col-md-6">
							<div class="py-1 px-3 text-center">
								<h3 class="display-4 pb-3">
									<?php echo $feedback_type;?>
									<input type="hidden" name="hid_feedback_type_id[]" id="hid_feedback_type_id" value="<?php echo $feedback_type_id;?>">

								</h3>

								<div class="text-center pb-3">
									<!-- <i class="fas fa-star icon-md"></i>
									<i class="fas fa-star icon-md"></i>
									<i class="fas fa-star icon-md"></i>
									<i class="fas fa-star icon-md"></i>
									<i class="far fa-star icon-md"></i> -->
									<?php

									?>

									<select id="glsr-ltr" required class="star-rating" onchange="getReviewValue(this.value,'<?php echo $feedback_type_id;?>');">
									<option value="">Select a rating</option>

									<?php

									$feedback_rating_sql = "SELECT * FROM `wc_feedback_rating` where feedback_type_id='$feedback_type_id' and deleted ='0' order by feedback_rating desc ";

									$feedback_rating_query = $this->db->query($feedback_rating_sql);
									$feedback_rating_result=$feedback_rating_query->result_array();

									if(!empty($feedback_rating_result))
									{
									foreach($feedback_rating_result as $key => $value)
									{
									$feedback_rating_id=$value['feedback_rating_id'];
									$feedback_type_id=$value['feedback_type_id'];
									$feedback_rating=$value['feedback_rating'];
									$feedback_rating_text=$value['feedback_rating_text'];

									?>


									<option value="<?php echo $feedback_rating;?>"><?php echo $feedback_rating_text;?></option>

									<?php
									}
									}
									?>
									
									</select>
				
									<!-- <div>&nbsp;</div> -->
								</div>

								<input type="hidden" name="hid_review[]" id="hid_review_<?php echo $feedback_type_id;?>" value="">

								<!-- <p class="lead">
									OK, but I had an issue
								</p> -->

								<div class="p-2">

									<div id="feedback_content_<?php echo $feedback_type_id;?>">

									
									</div>

									
								</div>

								<input type="hidden" name="hid_review_reason[]" id="hid_review_reason_<?php echo $feedback_type_id;?>" value="">
							</div>
						</div>


							<?php
							}
							}
							?>





					</div>

				</form>

					<div class="row">						
						<div class="col-12 col-sm-12 col-md-3">&nbsp;</div>
						<div class="col-12 col-sm-12 col-md-6">
							<div class="p-3 text-center">
								<button type="button" onclick="submitFeedback();" id="sndfb" class="btn btn-wc btn-block">Send Feedback</button>
							</div>
						</div>
						<div class="col-12 col-sm-12 col-md-3">&nbsp;</div>
					</div>
				</div>
			</main>
		</div>
	</div>
</div>


<script src="<?php echo base_url(); ?>website-assets/startRatingJquery/src/star-rating.js?ver=3.1.5"></script>
<script>


var destroyed = false;
var starratings = new StarRating( '.star-rating', {
onClick: function( el ) {

alert(el.selectedIndex);
console.log( 'Selected: ' + el[el.selectedIndex].text );
},
});

</script>


<?php
$this->load->view('front/includes/footer');
?>
<script>

	function getReviewValue(val,id)
		{
			//alert(val);

			/*var arr=val.split("-");
			var feedback_rating_id=$.trim(arr[0]);
			var feedback_rating=$.trim(arr[1]);*/
			var feedback_rating_id='';
			var feedback_rating=val;
			
			$("#hid_review_"+id).val(feedback_rating);


			$.ajax({

			method:'POST',
			url: "<?php echo base_url();?>front/feedback/getFeedbackContent",
			/*data:"&brief_id="+brief_id+"&quality="+quality+"&quality_reason="+quality_reason+"&speed="+speed+"&speed_reason="+speed_reason,*/
			data: {feedback_rating_id:feedback_rating_id,feedback_rating:feedback_rating,feedback_type_id:id},
			success: function(result){
			//alert(result);
			if(result) {

				

				$("#feedback_content_"+id).html(result);

			}
			}
			});


		}
		


	function setReviewReason(val,id,content_id)
	{
		$("#hid_review_reason_"+id).val(val);

		var val = val.replace(/ /g, '_');

		//alert(val);

		$(".qty_btn_"+id).removeClass("btn-wc-green");
		$("#qty_"+content_id).addClass("btn-wc-green");
		 

	}

	


	function submitFeedback()
	{

		var brief_id=$("#brief_id").val();
		var user_id=$("#user_id").val();

		var hid_feedback_type_id = $('input[name="hid_feedback_type_id[]"]').map(function () {
        if(this.value!='')
        {return this.value; // $(this).val()
        }
        else
        {return 0;}
		
        }).get();



        console.log(hid_feedback_type_id);

        var hid_review = $('input[name="hid_review[]"]').map(function () {
        if(this.value!='')
        {return this.value; // $(this).val()
        }
        else
        {return 0;}
		
        }).get();

        console.log(hid_review);


        /*var check_hid_review=$.inArray( 0, hid_review );
         console.log(check_hid_review);*/

        if($.inArray( 0, hid_review )==0||$.inArray( 0, hid_review )==1)
        {
        	// alert("You need to enter review before proceed.");
        	$('#modal_msg').modal('show');
			$('#txtMsg').html('You need to enter review before proceed.');
        	return false;
        }



        var hid_review_reason = $('input[name="hid_review_reason[]"]').map(function () {
        if(this.value!='')
        {return this.value; // $(this).val()
        }
        else
        {return 0;}
		
        }).get();

        console.log(hid_review_reason);

         if($.inArray( 0, hid_review_reason )==0||$.inArray( 0, hid_review_reason )==1)
        {
        	// alert("You need to enter reason before proceed.");
        	$('#modal_msg').modal('show');
			$('#txtMsg').html('You need to enter reason before proceed.');
        	return false;
        }





		$.ajax({

		method:'POST',
		url: "<?php echo base_url();?>front/feedback/insertFeedback",
		/*data:"&brief_id="+brief_id+"&quality="+quality+"&quality_reason="+quality_reason+"&speed="+speed+"&speed_reason="+speed_reason,*/
		data: {user_id:user_id,brief_id:brief_id,feedback_type_id:hid_feedback_type_id,review:hid_review,review_reason:hid_review_reason},
		success: function(result){
		//alert(result);
		//if(result) {

				<?php
				if ($this->session->userdata('front_logged_in')) {
				?>
				window.location.href="<?php echo base_url();?>dashboard";
				<?php
				}
				else
				{
				?>

				window.location.href="<?php echo base_url();?>feedback-thankyou";
				<?php

				}
				?>

		


		//}
		}
		});




		
		
		/*var quality=$("#hid_quality").val();
		var quality_reason=$("#hid_quality_reason").val();

		var speed=$("#hid_speed").val();
		var speed_reason=$("#hid_speed_reason").val();




		if(quality=='')
		{
			alert("Please select Quality Rating.");
			return false;
		}
        else if(quality_reason=='')
		{
			alert("Please select Quality Reason.");
			return false;
		}
        else if(speed=='')
		{
			alert("Please select Speed Rating.");
			return false;
		}
        else if(speed_reason=='')
		{
			alert("Please select Speed Reason.");
			return false;
		}

        else
		{

		

		$.ajax({

	   			method:'POST',
	   			url: "<?php echo base_url();?>front/feedback/insertFeedback",
	   			data:"&brief_id="+brief_id+"&quality="+quality+"&quality_reason="+quality_reason+"&speed="+speed+"&speed_reason="+speed_reason,

	   			success: function(result){
	   				//alert(result);
	   				//if(result) {

	   					window.location.href="<?php echo base_url();?>dashboard";
	   					
	   					
	   				//}
	   			}
	   		});







	   }*/




	}

</script>





