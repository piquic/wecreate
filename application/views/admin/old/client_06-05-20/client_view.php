<?php  $this->load->view('admin/includes/globalcss');?>


<style>
/*.table-bordered {
    border: 1px solid #666666;
}*/
.showdbutton {
    cursor:pointer; 
	background-color:#666666; 
	margin:10px 0px; 
	padding:10px; 
	color:#FFFFFF;
	width:100%;
}

.disabledbutton {
    pointer-events: none;
    opacity: 0.9;
	
	cursor:pointer; 
	background-color:#999999; 
	margin:10px 0px; 
	padding:10px; 
	color:#FFFFFF;
	width:100%;
}
label.error{
 color:#FF0000;
 font-size:12px;
}
</style>
<div>
  <!-- Section content header -->
 
  <!-- Section content -->
    <section class="content">
		<div class="col-md-12">
		  <!-- start Information Box -->
			<div class="box">
			<!-- box header style start-->
				<div class="col-md-12 box-header with-border" ><h3 class="box-title"><?php if($client_id==''){ echo "Create Client"; }else {echo "Update Client"; }?></h3></div>
					<!-- /.box-header -->
					<!-- form start -->
					
					<div id="success_message" style="display:none;">
					<div class="alert alert-success">
					<strong>Updated successfully!</strong>.
					</div>
				    </div>
					
				
				<div class="box-body">
				
				
				   <div class="col-md-12">
						<ul class="nav nav-tabs">
							<li id="li_home" class="active"><a data-toggle="tab" href="#home">Client Information</a></li>
							
							
						</ul>
					</div>
				
				
				<div class="addmargin10">&nbsp;</div>
					<div class="tab-content">
					
				     <div id="home" class="tab-pane fade in active">
					 
					 		    <?php  $array = array('id'=>'client_form', 'role'=>'form', 'class'=>'form form-horizontal has-validation-callback');
				    echo form_open_multipart('client/insert_client', $array); ?>
					
					
					<input type='hidden' name='client_id' value='<?php echo $client_id;?>' id='client_id'>
					
					
					
					
					
					
					
					
					<div class="form-group">
					    <label for="inputEmail3"  class="col-sm-2 control-label">Client Name
                        <span style="color:#F00">*</span>
                        </label>
						<div class="col-sm-8">
						    <input  type='text'  class="form-control" id="client_name" name="client_name" value='<?php echo $client_name;?>'>
                            <?php echo form_error('client_name', '<div class="error" style="color:red;">', '</div>'); ?>
						</div>
						
					</div>


					<div class="form-group">
								<label for="inputEmail3"  class="col-sm-2 control-label">Client Logo (logo dimentions must be 200 X 100)
									
								</label>
								<div class="col-sm-10">
														   
									<input type="file" class="upload_files fileInput css_uploadFile" id="files" val=""  name="files[]" multiple />

									<input  type='hidden'  class="form-control" id="pimg" name="pimg" value='<?php echo $pimg; ?>'>
									<?php echo form_error('pimg', '<div class="error" style="color:red;">', '</div>'); ?>

									<div id="show_pimg">
									<span class="pip" >

									<?php
									if($pimg!='')
									{
									?>
									<img src="<?php echo base_url(); ?>/upload_client_logo/<?php echo $pimg; ?>"  style="width: 60%;"><br/>
									<span class='remove' id="" value='<?php echo $pimg; ?>'>Remove</span>

									<?php
									}
									?>
									</span>

									</div>
								</div>

								



							</div>
					
					
					<div class="form-group">
					    <label for="inputEmail3"  class="col-sm-2 control-label">Client Email
                        
                        </label>
						<div class="col-sm-8">
						    <input  type='text'  class="form-control" id="client_email" name="client_email" value='<?php echo $client_email;?>'>
                            <?php echo form_error('client_email', '<div class="error" style="color:red;">', '</div>'); ?>
						</div>
						
					</div>
					
			
				
				<div class="form-group">
					    <label for="inputEmail3"  class="col-sm-2 control-label">Client Phone
                        </label>
						<div class="col-sm-8">
						    <input  type='text'  class="form-control" id="client_phone" name="client_phone" value='<?php echo $client_phone;?>'>
                            <?php echo form_error('client_phone', '<div class="error" style="color:red;">', '</div>'); ?>
						</div>
						
					</div>
					


				<div class="form-group">
					<label for="inputEmail3"  class="col-sm-2 control-label">Allow BM To ADD Delivery Date</label>
					<div class="col-sm-8">
				
							
								<select name="allow_delivary_date_bm" id="allow_delivary_date_bm" class="form-control" >
								 <!--<option value=''>Select</option>-->
								 <option value="0" <?php if($allow_delivary_date_bm=='0') { ?> selected="selected" <?php } ?>  >No</option>
								 <option value="1" <?php if($allow_delivary_date_bm=='1') { ?> selected="selected" <?php } ?>   >Yes</option>
								 </select>
					
					</div>
					</div>
					
					
			
					
					
					<div class="form-group">
					<label for="inputEmail3"  class="col-sm-2 control-label"><?php echo $lang['status'];?></label>
					<div class="col-sm-8">
				
							
								<select name="status" id="status" class="form-control" >
								 <!--<option value=''>Select</option>-->
								 <option value="Active" <?php if($status=='Active') { ?> selected="selected" <?php } ?>  >Active</option>
								 <option value="Inactive" <?php if($status=='Inactive') { ?> selected="selected" <?php } ?>   >Inactive</option>
								 </select>
					
					</div>
					</div>
					
					
					
					
					
					
				
					
					
					
					<!-- Contractor type fields end-->
					<!-- <div class="row">
					<?php if($client_id!=''){
						$button_name='Update';
					}else {
						$button_name='Save';
					} ?>
						<div class="col-md-6"><input class="btn btn-success pull-right" type="submit" value="<?php echo $button_name;?>"></div>	   
						<div class="col-md-6"><input class="btn btn-danger pull-left" type="reset"></div>
					</div>
					 -->
					
					
					
					
					<?php echo form_close();?> 
					 
					 
					 
				    </div>
					
					
					
					
				
					
					
				    </div>
					
					
					
					
				
		
					
					
					
				</div>
					
			</div>
		</div>
		
	</section>
</div>

<?php $this->load->view('admin/includes/globaljs');?>
<link type="text/css" href="<?php echo base_url();?>assets/form-select2/select2.css" rel="stylesheet"/>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootswatch/3.3.7/lumen/bootstrap.min.css">



<script type="text/javascript" src="<?php echo base_url();?>assets/form-select2/select2.min.js"></script>



<!--<script  type="text/javascript" src="<?php echo base_url();?>assets/dist/password-score.js"></script>
<script  type="text/javascript" src="<?php echo base_url();?>assets/dist/password-score-options.js"></script>
<script  type="text/javascript" src="<?php echo base_url();?>assets/dist/bootstrap-strength-meter.js"></script>-->

 <script>

    
	jQuery(function ($) {
	   // $('.content-wrapper').css('min-height','900px');
		
		//alert("popo");
		
		
		jQuery.validator.addMethod("alphanumeric", function(value, element) {
        return this.optional(element) || /^[a-zA-Z\-\s]+$/.test(value);
        }, "It must contain only letters."); 
		
		jQuery.validator.addMethod("atLeastOneLetter", function (value, element) {
		return this.optional(element) || /[a-zA-Z]+/.test(value);
		}, "Must have at least one  letter");
		
		/*jQuery.validator.addMethod("phoneno", function(phone_number, element) {
    	    phone_number = phone_number.replace(/\s+/g, "");
    	    return this.optional(element) || phone_number.length > 9 && 
    	    phone_number.match(/^((\+[1-9]{1,4}[ \-]*)|(\([0-9]{2,3}\)[ \-]*)|([0-9]{2,4})[ \-]*)*?[0-9]{3,4}?[ \-]*[0-9]{3,4}?$/);
    	}, "<br />Please specify a valid phone number");*/
		
		/*jQuery.validator.addMethod('fnType', function(value, element) {
		return value.match(/^\+(?:[0-9] ?){6,14}[0-9]$/);
		},'Enter Valid  phone number');*/
		
		/*jQuery.validator.addMethod('fnType', function(value, element) {
		return /^\d{10}$/.test(value.replace(/[()\s+-]/g,''));
		},'Enter Valid  phone number');*/
		
		
		/*jQuery.validator.addMethod('fnType', function(value, element) {
		return /^\d{10}$/.test(value.replace(/[()\s+-]/g,''));
		},'Enter Valid  phone number');*/
		
		jQuery.validator.addMethod("phoneno", function(value, element) {
        return this.optional(element) || /^[0-9+() ]*$/.test(value);
        }, "Enter Valid  phone number."); 
		
		
		
		$('#client_form').validate({
		
           rules: {
			
			
            client_name:{
				required:true,
				minlength: 2,
				//alphanumeric:true,
				//atLeastOneLetter:true,
			},
			pimg:
			{
			required:true,
			},
			client_email:{
				//required:true,
				email: true,
				<?php
				if($client_id=="")
				{
				?>
				remote: {
				url: "<?php echo base_url();?>admin/client/checkexistemail",
				type: "post"
				},
				<?php
				}
				?>	 
				 
			 },
			
			
			
		   client_phone: {
		   //required: true,
		   /*digits: true,*/
		   minlength:10,
		  /* maxlength:10,*/
		   phoneno: true,
		   /*number: true,*/
		  /* maxlength: 10,*/
		    <?php
		   if($client_id=="")
		   {
		   ?>
		    remote: {
                    url: "<?php echo base_url();?>admin/client/checkexistmobile",
                    type: "post"
                   },
				 
		   <?php
		   }
		   ?>		 
				 
		},
		
		allow_delivary_date_bm: {
                required: true,
			},
		status: {
                required: true,
			}
			
			
		},
		 messages: {
		 
		    client_email:{
		      remote:"This email already exists"
		     },
			client_mobile: {
			digits: "This field can only contain numbers.",
			/*maxlength: "this must be 10 digit number.",*/
			remote:"This mobile already exists",
			
			},
			
			 /*client_mobile:{
		      remote:"This email already exists"
		     }*/
                
		  
			
        },
				submitHandler: function(form) {
		
		      // alert("koko");
		
		        var client_id=$('#client_id').val();
				

			    var client_name=$('#client_name').val();
			    var client_email=$('#client_email').val();
				var client_phone=$('#client_phone').val();
				var status=$('#status').val();
				var allow_delivary_date_bm=$('#allow_delivary_date_bm').val();
				var pimg=$('#pimg').val();
				
				
				
				
					
					
				/*alert("<?php echo base_url();?>admin/client/insert_client?&client_type_id="+client_type_id+"&client_id="+client_id+"&client_name="+client_name+"&client_address="+client_address+"&client_email="+client_email+"&client_telephone="+client_telephone+"&client_mobile="+client_mobile+"&client_password="+client_password+"&status="+status);	
					*/
				$.ajax({
				method:'post',
				url: "<?php echo base_url();?>admin/client/insert_client", 
				data:"&client_id="+client_id+"&client_name="+client_name+"&client_email="+client_email+"&client_phone="+client_phone+"&status="+status+"&pimg="+pimg+"&allow_delivary_date_bm="+allow_delivary_date_bm,
				success: function(result){
				if(result)
				{
				    //alert(result);
					
					//alert("koko");
					
					var val=$.trim(result);
					//alert(val);
					
					$("#success_message").show();
				  setTimeout(function(){setInterval(function(){
				  //alert("ppp");
				  parent.$.fancybox.close();
				  
				  }, 3000)}, 3000);
				  
				  
					
					parent.$.fancybox.close();
					
					//alert("fofo");
					//alert("koko");
					
					parent.$('#example1').dataTable().fnStandingRedraw();
					
					
				  
					
				
				  
				  
				 
				
				}
				}
				});
					
					
					
					
					
						
							
					
					   
		
		} 
		 
		
		});
		
		
		
	$('#client_password_form').validate({
        rules: {
			client_change_password:{
				
				required:true
			},
			
			 client_change_cpassword: {
				required:true,
                equalTo: "#client_change_password"              
            }
			
        },
		 messages: {
          
			client_change_cpassword: {
            	equalTo:"password not match"
			}
			
        },
		submitHandler: function(form) {
		        var client_id=$('#client_id').val();
				var client_password=$('#client_change_password').val();
				
				
				
				
					
					
				//alert("<?php echo base_url();?>client/addclient?&client_id="+client_id+"&clientName="+clientName+"&clientComments="+clientComments+"&clientIsActive="+clientIsActive);	
					
				$.ajax({
				method:'post',
				url: "<?php echo base_url();?>admin/client/update_change_password", 
				data:"&client_id="+client_id+"&client_password="+client_password,
				success: function(result){
				if(result)
				{
				// alert(result);
					
					
					
					var val=$.trim(result);
					
					
					$("#success_message").show();
				  setTimeout(function(){setInterval(function(){
				  //alert("ppp");
				  parent.$.fancybox.close();
				  
				  }, 3000)}, 3000);
					
					//alert(val);
					parent.$.fancybox.close();
					parent.$('#example1').dataTable().fnStandingRedraw();
					
				
				  
				  
				 
				
				}
				}
				});
				
				
				
		 },
    });
	
		
		
		
		
	});	






$(".remove").click(function(){
	
	//alert("joko");
	
	var remove_value = $(this).attr('value') ;
	
		 //  alert(remove_value);
		 
		 var temp_pimg = $("#pimg").val();
		 
		 var main_pimg = temp_pimg.replace(remove_value, '')
		 
		 $("#pimg").val(main_pimg);
		 
		 $(this).parent(".pip").remove();
		 
		 
		//    //alert("<?php echo base_url();?>admin/Project/get_unlink_path");
		
		$.ajax({
			method:'get',
			url: "<?php echo base_url();?>admin/client/get_unlink_path", 
			data:"&image_delete_value="+remove_value,
			success: function(result){
				if(result)
				{
					//alert(result);
				}
			}
		});			   
		
		return false;			
	});	



$('.upload_files').on('change', function (e) {

	
	var id= $(this).attr('val') ;



		//alert(folder);

		var files = e.target.files,
		filesLength = files.length;

		console.log(files);

		for (var i = 0; i < filesLength; i++) {
		var file = files[i];



		console.log(file);
		console.log("bbbbbbbbbb");



		if(file != undefined){
		formData= new FormData();

		var ext = $("#files").val().split('.',2).pop().toLowerCase();

		//alert(ext);

 		var arr_ext=['jpg','JPG','jpeg','JPEG','png','PNG','gif','GIF'];
		//if ($.inArray(ext, ['jpg','JPG','jpeg','JPEG','png','PNG','gif','GIF']) == -1)
		 //if (ext=='doc' || ext=='docx')
		if(arr_ext.indexOf(ext)!=-1)
		{
		formData.append("pimg", file);
		formData.append("id", id);
		formData.append("check", i);



		//alert("aaaa");
		$.ajax({
		url: "<?php echo base_url();?>admin/client/upload_file",
		type: "POST",
		data: formData,
		processData: false,
		contentType: false,
		success: function(data){

		//alert("koko");
		//alert(data);

		

		 var pimg=$.trim(data);
		 var dir = "<?php echo base_url(); ?>/upload_client_logo/";

   /* $("<span class='pip'>" +
    	"<img class='imageThumb' src='" + dir  + pimg +"' title='" + file.name + "'/>" +
    	"<br/><span class='remove' value='"+pimg+"'>Remove</span>" +
    	"</span>").insertAfter("#files");*/

    	 var show_pimg_div="<span class='pip' id= 'show_pimg' >" +
    	"<img class='imageThumb' src='" + dir  + pimg +"' title='" + file.name + "'/>" +
    	"<br/><span class='remove' value='"+pimg+"'>Remove</span>" +
    	"</span>";

    $("#show_pimg").html($.trim(show_pimg_div));

    
    $("#pimg").val($.trim(data));

   //   alert('success');


   $(".remove").click(function(){

	      //alert("joko");

	      var remove_value = $(this).attr('value') ;
//
	      //alert(remove_value);

	      var temp_pimg = $("#pimg").val();

	      var main_pimg = temp_pimg.replace(remove_value, '')

	      $("#pimg").val(main_pimg);

	      $(this).parent(".pip").remove();


		  // alert("<?php echo base_url();?>admin/Project/get_unlink_path");

		  $.ajax({
		  	method:'get',
		  	url: "<?php echo base_url();?>admin/client/get_unlink_path", 
		  	data:"&image_delete_value="+remove_value,
		  	success: function(result){
		  		if(result)
		  		{
		  			//alert(result);
		  		}
		  	}
		  });

		  return false;			
		});

		



		}
		});

	    }
	    else
	    {
	    	alert('Upload Image Files!');
	    }



		

		}
		else{
		alert('Input something!');
		}



		//alert(i+"-----"+filesLength);

		

		
		}


 });


</script>   
<script>
 $(document).ready(function() {
 //alert("popo");
	  /* $('#example-getting-started-input').strengthMeter('text', {
            container: $('#example-getting-started-text')
        });
        $('#example-tooltip').strengthMeter('tooltip');*/
		/*$('#client_passowrd').strengthMeter('progressBar', {
            container: $('#example-progress-bar-container')
        });*/
    });
</script>
<script type="text/javascript" src="<?php echo base_url();?>theme/assets/source/jquery.fancybox.js?v=2.1.5"></script>
<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>theme/assets/source/jquery.fancybox.css?v=2.1.5" media="screen" />				