<?php  $this->load->view('admin/includes/globalcss');?>


<style>
/*.table-bordered {
    border: 1px solid #666666;
}*/
.showdbutton {
    cursor:pointer; 
	background-color:#666666; 
	margin:10px 0px; 
	padding:10px; 
	color:#FFFFFF;
	width:100%;
}

.disabledbutton {
    pointer-events: none;
    opacity: 0.9;
	
	cursor:pointer; 
	background-color:#999999; 
	margin:10px 0px; 
	padding:10px; 
	color:#FFFFFF;
	width:100%;
}
label.error{
 color:#FF0000;
 font-size:12px;
}
</style>
<div>
  <!-- Section content header -->
 
  <!-- Section content -->
    <section class="content">
		<div class="col-md-12">
		  <!-- start Information Box -->
			<div class="box">
			<!-- box header style start-->
				<div class="col-md-12 box-header with-border" ><h3 class="box-title"><?php if($brand_id==''){ echo 'Create Brand'; }else {echo 'Update Brand'; }?></h3></div>
					<!-- /.box-header -->
					<!-- form start -->
					
					<div id="success_message" style="display:none;">
					<div class="alert alert-success">
					<strong>Updated successfully!</strong>.
					</div>
				    </div>
					
				
				<div class="box-body">
				
				
				   <div class="col-md-12">
						<ul class="nav nav-tabs">
							<li id="li_home" class="active"><a data-toggle="tab" href="#home">Brand Information</a></li>
							
							
						</ul>
					</div>
				
				
				<div class="addmargin10">&nbsp;</div>
					<div class="tab-content">
					
				     <div id="home" class="tab-pane fade in active">
					 
					 		    <?php  $array = array('id'=>'Brand_form', 'role'=>'form', 'class'=>'form form-horizontal has-validation-callback');
				    echo form_open_multipart('User/insert_user', $array); ?>
					
					
					<input type='hidden' name='brand_id' value='<?php echo $brand_id;?>' id='brand_id'>
					
					
					
					<div class="form-group">
					    <label for="inputEmail3"  class="col-sm-2 control-label">Client Name
                        <span style="color:#F00">*</span>
                        </label>
						<div class="col-sm-8">
						    	<!-- class="form-control" -->
								<select name="client_id" id="client_id"  style="width: 100%" onchange="getCategoryDiv();" >
								 <option value=''>Select</option>
								<?php
								$clients_details=$this->db->query("select * from wc_clients where deleted='0' and status='Active' ")->result_array();
								if(!empty($clients_details))
								{
								foreach($clients_details as $key => $clientsdetails)
								{
								$client_id=$clientsdetails['client_id'];
								$client_name=$clientsdetails['client_name'];
								?>
								<option value="<?php echo $client_id;?>" <?php if($client_id_sel==$client_id) { ?> selected="selected" <?php } ?>  ><?php echo $client_name;?></option>
								<?php

								}
								}
								?>
								
								 </select>
						</div>
						
					</div>

					<div id="category_div">
					
					
					
					<!-- <div class="form-group">
					    <label for="inputEmail3"  class="col-sm-2 control-label">Category
                       <span style="color:#F00">*</span>
                        </label>
						<div class="col-sm-8">
						    	
								<select name="category_id" id="category_id"  style="width: 100%" >
								 <option value=''>Select</option>
								<?php
								$clients_details=$this->db->query("select * from wc_category where deleted='0' and status='Active' ")->result_array();
								if(!empty($clients_details))
								{
								foreach($clients_details as $key => $clientsdetails)
								{
								$category_id=$clientsdetails['category_id'];
								$category_name=$clientsdetails['category_name'];
								?>
								<option value="<?php echo $category_id;?>" <?php if($category_id_sel==$category_id) { ?> selected="selected" <?php } ?>  ><?php echo $category_name;?></option>
								<?php

								}
								}
								?>
								
								 </select>
						</div>
						
					</div>
					
					



					<div class="form-group">
					    <label for="inputEmail3"  class="col-sm-2 control-label">Brand Level 1
                        <span style="color:#F00">*</span>
                        </label>
						<div class="col-sm-8">
						    	
								<select name="category_id1" id="category_id1"  style="width: 100%" >
								 <option value=''>Select</option>
								<?php
								$clients_details=$this->db->query("select * from wc_category where deleted='0' and status='Active' ")->result_array();
								if(!empty($clients_details))
								{
								foreach($clients_details as $key => $clientsdetails)
								{
								$category_id=$clientsdetails['category_id'];
								$category_name=$clientsdetails['category_name'];
								?>
								<option value="<?php echo $category_id;?>" <?php if($category_id1_sel==$category_id) { ?> selected="selected" <?php } ?>  ><?php echo $category_name;?></option>
								<?php

								}
								}
								?>
								
								 </select>
						</div>
						
					</div>



					<div class="form-group">
					    <label for="inputEmail3"  class="col-sm-2 control-label">Brand level 2
                        <span style="color:#F00">*</span>
                        </label>
						<div class="col-sm-8">
						    
								<select name="category_id2" id="category_id2"  style="width: 100%" >
								 <option value=''>Select</option>
								<?php
								$clients_details=$this->db->query("select * from wc_category where deleted='0' and status='Active' ")->result_array();
								if(!empty($clients_details))
								{
								foreach($clients_details as $key => $clientsdetails)
								{
								$category_id=$clientsdetails['category_id'];
								$category_name=$clientsdetails['category_name'];
								?>
								<option value="<?php echo $category_id;?>" <?php if($category_id2_sel==$category_id) { ?> selected="selected" <?php } ?>  ><?php echo $category_name;?></option>
								<?php

								}
								}
								?>
								
								 </select>
						</div>
						
					</div>
					
					 -->

					</div>











					



					<div class="form-group">
					    <label for="inputEmail3"  class="col-sm-2 control-label">Brand Name
                        <span style="color:#F00">*</span>
                        </label>
						<div class="col-sm-8">
						    <input  type='text'  class="form-control" id="brand_name" name="brand_name" value='<?php echo $brand_name;?>'>
                            <?php echo form_error('brand_name', '<div class="error" style="color:red;">', '</div>'); ?>
						</div>
						
					</div>



					<!-- <div class="form-group">
					    <label for="inputEmail3"  class="col-sm-2 control-label">Brand Email
                        <span style="color:#F00">*</span>
                        </label>
						<div class="col-sm-8">
						    <input  type='text'  class="form-control" id="brand_email" name="brand_email" value='<?php //echo $brand_email;?>'>
                            <?php //echo form_error('brand_email', '<div class="error" style="color:red;">', '</div>'); ?>
						</div>
						
					</div> -->

					<input  type='hidden'  class="form-control" id="brand_email" name="brand_email" value='<?php echo $brand_email;?>'>
					
					
					
					
					
				
				
			
					
					
					<div class="form-group">
					<label for="inputEmail3"  class="col-sm-2 control-label"><?php echo $lang['status'];?></label>
					<div class="col-sm-8">
				
							
								<select name="status" id="status" class="form-control" >
								 <!--<option value=''>Select</option>-->
								 <option value="Active" <?php if($status=='Active') { ?> selected="selected" <?php } ?>  >Active</option>
								 <option value="Inactive" <?php if($status=='Inactive') { ?> selected="selected" <?php } ?>   >Inactive</option>
								 </select>
					
					</div>
					</div>
					
					
					
					<!-- Contractor type fields end-->
					<div class="row">
					<?php if($brand_id!=''){
						$button_name='Update';
					}else {
						$button_name='Save';
					} ?>
						<div class="col-md-6"><input class="btn btn-success pull-right" type="submit" value="<?php echo $button_name;?>"></div>	   
						<div class="col-md-6"><input class="btn btn-danger pull-left" type="reset"></div>
					</div>
					
					
					
					
					
					
					
					
					
					
					
					
					
					
					
					<?php echo form_close();?> 
					 
					 
					 
				    </div>
					
					
					
				
					
				    </div>
					
					
					
					
				
		
					
					
					
				</div>
					
			</div>
		</div>
		
	</section>
</div>

<?php $this->load->view('admin/includes/globaljs');?>
<link type="text/css" href="<?php echo base_url();?>assets/form-select2/select2.css" rel="stylesheet"/>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootswatch/3.3.7/lumen/bootstrap.min.css">



<script type="text/javascript" src="<?php echo base_url();?>assets/form-select2/select2.min.js"></script>



<!--<script  type="text/javascript" src="<?php echo base_url();?>assets/dist/password-score.js"></script>
<script  type="text/javascript" src="<?php echo base_url();?>assets/dist/password-score-options.js"></script>
<script  type="text/javascript" src="<?php echo base_url();?>assets/dist/bootstrap-strength-meter.js"></script>-->

 <script>

 	getCategoryDiv();


 	function getCategoryDiv()
 	{
 		var client_id=$('#client_id').val();
 		var category_id='<?php echo $category_id_sel;?>';
 		var category_id1='<?php echo $category_id1_sel;?>';
 		var category_id2='<?php echo $category_id2_sel;?>';
 		//alert(client_id);
 		


		 $.ajax({
				method:'post',
				url: "<?php echo base_url();?>admin/brand/showCategoryDiv", 
				data:"&client_id="+client_id+"&category_id="+category_id+"&category_id1="+category_id1+"&category_id2="+category_id2,
				success: function(result){
				if(result)
				{
				// alert(result);

				 $('#category_div').html(result);

		        
				}
				}
				});



		
		
 	}



 	function getSubCategoryDiv()
 	{
 		var client_id=$('#client_id').val();
 		var category_id=$('#category_id').val();
 		var category_id1='<?php echo $category_id1_sel;?>';
 		var category_id2='<?php echo $category_id2_sel;?>';
 		//alert(client_id);
 		


		 $.ajax({
				method:'post',
				url: "<?php echo base_url();?>admin/brand/showSubCategoryDiv", 
				data:"&client_id="+client_id+"&category_id="+category_id+"&category_id1="+category_id1+"&category_id2="+category_id2,
				success: function(result){
				if(result)
				{
				// alert(result);

				 $('#main_sub_category_div').html(result);

		        
				}
				}
				});



		
		
 	}


 		function getSubSubCategoryDiv()
 	{
 		var client_id=$('#client_id').val();
 		var category_id=$('#category_id').val();
 		var category_id1=$('#category_id1').val();
 		var category_id2='<?php echo $category_id2_sel;?>';
 		//alert(client_id);
 		


		 $.ajax({
				method:'post',
				url: "<?php echo base_url();?>admin/brand/showSubSubCategoryDiv", 
				data:"&client_id="+client_id+"&category_id="+category_id+"&category_id1="+category_id1+"&category_id2="+category_id2,
				success: function(result){
				if(result)
				{
				// alert(result);

				 $('#sub_sub_category_div').html(result);

		        
				}
				}
				});



		
		
 	}



    
    
	jQuery(function ($) {
	   // $('.content-wrapper').css('min-height','900px');
		
		
		
	 //$('#entity_id').select2();
		
		
		/*$('#user_password').strengthMeter('progressBar', {
            container: $('#example-progress-bar-container')
        });*/
		
		
		
	    /*$.validator.addMethod("regex", function(value, element, regexpr) {          
		 return regexpr.test(value);
		}, "Please enter a valid Mobile Number.");*/
		
		
		
		
		
		
		
		
		
		//alert("popo");
		
		
		jQuery.validator.addMethod("alphanumeric", function(value, element) {
        return this.optional(element) || /^[a-zA-Z\-\s]+$/.test(value);
        }, "It must contain only letters."); 
		
		jQuery.validator.addMethod("atLeastOneLetter", function (value, element) {
		return this.optional(element) || /[a-zA-Z]+/.test(value);
		}, "Must have at least one  letter");
		
		/*jQuery.validator.addMethod("phoneno", function(phone_number, element) {
    	    phone_number = phone_number.replace(/\s+/g, "");
    	    return this.optional(element) || phone_number.length > 9 && 
    	    phone_number.match(/^((\+[1-9]{1,4}[ \-]*)|(\([0-9]{2,3}\)[ \-]*)|([0-9]{2,4})[ \-]*)*?[0-9]{3,4}?[ \-]*[0-9]{3,4}?$/);
    	}, "<br />Please specify a valid phone number");*/
		
		/*jQuery.validator.addMethod('fnType', function(value, element) {
		return value.match(/^\+(?:[0-9] ?){6,14}[0-9]$/);
		},'Enter Valid  phone number');*/
		
		/*jQuery.validator.addMethod('fnType', function(value, element) {
		return /^\d{10}$/.test(value.replace(/[()\s+-]/g,''));
		},'Enter Valid  phone number');*/
		
		
		/*jQuery.validator.addMethod('fnType', function(value, element) {
		return /^\d{10}$/.test(value.replace(/[()\s+-]/g,''));
		},'Enter Valid  phone number');*/
		
		jQuery.validator.addMethod("phoneno", function(value, element) {
        return this.optional(element) || /^[0-9+() ]*$/.test(value);
        }, "Enter Valid  phone number."); 
		
		
		
		$('#Brand_form').validate({
		
           rules: {
			client_id:{
				required:true,
			},
			brand_name: {
                required: true,
			},
			
			/*brand_email:{
				required:true,
				email: true,
				<?php
				//if($brand_id=="")
				{
				?>
				remote: {
				url: "<?php //echo base_url();?>admin/brand/checkexistemail",
				type: "post"
				},
				<?php
				}
				?>	 
				 
			 },*/
			status: {
                required: true,
			}
			
			
		},
		 messages: {
		 
		   brand_email:{
		      remote:"This email already exists"
		     }
			
			
        },
				submitHandler: function(form) {
		
		       //alert("koko");
		
		        var brand_id=$('#brand_id').val();
				var client_id=$('#client_id').val();
			    var brand_name=$('#brand_name').val();
			    var brand_email=$('#brand_email').val();
				var status=$('#status').val();


				var category_id=$('#category_id').val();
				var category_id1=$('#category_id1').val();
				var category_id2=$('#category_id2').val();
				
					
				//alert("<?php echo base_url();?>admin/brand/insert_brand?&brand_id="+brand_id+"&client_id="+client_id+"&brand_name="+brand_name+"&brand_email="+brand_email+"&status="+status);	
					
				$.ajax({
				method:'post',
				url: "<?php echo base_url();?>admin/brand/insert_brand", 
				data:"&brand_id="+brand_id+"&client_id="+client_id+"&brand_name="+brand_name+"&brand_email="+brand_email+"&status="+status+"&category_id="+category_id+"&category_id1="+category_id1+"&category_id2="+category_id2,
				success: function(result){
				if(result)
				{
				    //alert(result);
					
					//alert("koko");
					
					var val=$.trim(result);
					//alert(val);
					
					$("#success_message").show();
				  setTimeout(function(){setInterval(function(){
				  //alert("ppp");
				  parent.$.fancybox.close();
				  
				  }, 3000)}, 3000);
				  
				  
					
					parent.$.fancybox.close();
					
					//alert("fofo");
					//alert("koko");
					
					parent.$('#example1').dataTable().fnStandingRedraw();
					
					
				  
					
				
				  
				  
				 
				
				}
				}
				});
					
					
					
					
					
						
							
					
					   
		
		} 
		 
		
		});
		
		
		
	$('#User_password_form').validate({
        rules: {
			user_change_password:{
				
				required:true
			},
			
			 user_change_cpassword: {
				required:true,
                equalTo: "#user_change_password"              
            }
			
        },
		 messages: {
          
			user_change_cpassword: {
            	equalTo:"password not match"
			}
			
        },
		submitHandler: function(form) {
		        var brand_id=$('#brand_id').val();
				var user_password=$('#user_change_password').val();
				
				
				
				
					
					
				//alert("<?php echo base_url();?>user/addUser?&brand_id="+brand_id+"&UserName="+UserName+"&UserComments="+UserComments+"&UserIsActive="+UserIsActive);	
					
				$.ajax({
				method:'post',
				url: "<?php echo base_url();?>admin/user/update_change_password", 
				data:"&brand_id="+brand_id+"&user_password="+user_password,
				success: function(result){
				if(result)
				{
				// alert(result);
					
					
					
					var val=$.trim(result);
					
					
					$("#success_message").show();
				  setTimeout(function(){setInterval(function(){
				  //alert("ppp");
				  parent.$.fancybox.close();
				  
				  }, 3000)}, 3000);
					
					//alert(val);
					parent.$.fancybox.close();
					parent.$('#example1').dataTable().fnStandingRedraw();
					
				
				  
				  
				 
				
				}
				}
				});
				
				
				
		 },
    });
	
		
		
		
		
	});	
</script>   
<script>
 $(document).ready(function() {
 //alert("popo");
	  /* $('#example-getting-started-input').strengthMeter('text', {
            container: $('#example-getting-started-text')
        });
        $('#example-tooltip').strengthMeter('tooltip');*/
		/*$('#user_passowrd').strengthMeter('progressBar', {
            container: $('#example-progress-bar-container')
        });*/
    });
</script>
<script type="text/javascript" src="<?php echo base_url();?>theme/assets/source/jquery.fancybox.js?v=2.1.5"></script>
<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>theme/assets/source/jquery.fancybox.css?v=2.1.5" media="screen" />				