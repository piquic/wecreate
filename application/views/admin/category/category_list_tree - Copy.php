<!-- Last Edited: KW 25/09/16 -->
<?php
$page_plugins = "";
$this->load->view('admin/includes/header');
$this->load->view('admin/includes/sidebar');
?>
<style>
	table.dataTable tbody tr.myeven{
		background-color:#F9843E;
	}
	table.dataTable tbody tr.myodd{
		/* background-color:#00FF00;*/
	}
</style>
<div class="content-wrapper">
	<!-- Content Header (Page header) -->
	<section class="content-header">
		<div class="col-md-12"> <h1> Category Management
		</h1></div>

	</section>

	<!-- Main content -->
	<section class="content">
		<div class="row" >
			<div class="col-md-12">

				<div class="box">

	  	<!--	<?php if($rolecode==1){ ?>
			<input type="text" name="fbusiness" id="fbusiness" placeholder="Search Business" class="form-control input-sm service" >
			<?php } ?>-->
			
			
			<!-- box header style start-->
			<br> 


			<input type="button" name="create_category" id="create_category"  value='Create category' class="fancybox-manual-a btn btn-success pull-right">

			<!-- <br> <br>

			<input type="button" name="view_tree_view" id="view_tree_view"  value='View Tree View' class="btn btn-success pull-right">
 -->



			<!-- box header style end-->
			<br> <br>

			<!-- /.box-header -->
			<div class="box-body table-responsive-vertical shadow-z-1">

			<table id="tree-table" class="table table-hover table-bordered">
			<tbody>
			<th>Client</th>
			<th>Category</th>

			<?php

				$category_details=$this->db->query("select * from wc_category where category_type='1' ")->result_array();
				if(!empty($category_details))
				{
				foreach($category_details as $key => $value)
				{
					$category_id=$value['category_id'];
					$category_type=$value['category_type'];
					$category_name=$value['category_name'];

					$client_id=$value['client_id'];

					 $clients_details=$this->db->query("select * from wc_clients where client_id='$client_id'  ")->result_array();
            		 $client_name=$clients_details[0]['client_name'];
					
					?>
						<tr data-id="<?php echo $category_id;?>" data-parent="0" data-level="1">
							<td ><?php echo $client_name;?></td>
						<td data-column="name"><?php echo $category_name;?></td>	
						
                        </tr>
									<?php

									$category_details=$this->db->query("select * from wc_category where category_type='2' and parent_category_id='$category_id' ")->result_array();
									if(!empty($category_details))
									{
									foreach($category_details as $key => $value)
									{
									$category_id1=$value['category_id'];
									$category_type=$value['category_type'];
									$category_name=$value['category_name'];

									?>
									<tr data-id="<?php echo $category_id1;?>" data-parent="<?php echo $category_id;?>" data-level="2">
										<td >&nbsp;</td>
									<td data-column="name">&nbsp&nbsp<?php echo $category_name;?></td>

									</tr>


									<?php

									$category_details=$this->db->query("select * from wc_category where category_type='3' and parent_category_id='$category_id1' ")->result_array();
									if(!empty($category_details))
									{
									foreach($category_details as $key => $value)
									{
									$category_id2=$value['category_id'];
									$category_type=$value['category_type'];
									$category_name=$value['category_name'];

									?>
									<tr data-id="<?php echo $category_id2;?>" data-parent="<?php echo $category_id1;?>" data-level="3">
										<td >&nbsp;</td>
									<td data-column="name">&nbsp&nbsp&nbsp&nbsp<?php echo $category_name;?></td>

									</tr>

									<?php

									}
									}

									?>


									<?php

									}
									}

									?>


					<?php
					
				}
				}

			?>


			
			


<!-- 
			<tr data-id="1" data-parent="0" data-level="1">
			<td data-column="name">Node 1</td>
			</tr>
			<tr data-id="2" data-parent="1" data-level="2">
			<td data-column="name">Node 1 -0</td>
			
			</tr>
			<tr data-id="3" data-parent="1" data-level="2">
			<td data-column="name">Node 1 -1</td>
			
			</tr>
			<tr data-id="4" data-parent="3" data-level="3">
			<td data-column="name">Node 1 -1-1</td>
			
			</tr>
			<tr data-id="5" data-parent="3" data-level="3">
			<td data-column="name">Node 1 -1-2</td>
			
			</tr>
 -->









			</tbody>

			</table>










				<!-- <table id="example1" class="table table-hover table-mc-light-blue table-bordered   ">
					<thead>
						<tr> 
							<th> Client Name</th>	
							<th> Brand Name</th>
							<th> Category Type</th>	
							<th> Category</th>	
							<th> Brand level 1</th>	
							<th> Brand level 2</th>				
							<th >Status</th>
							<th >Action</th>
						</tr>
					</thead>
					<tbody>
						

					</tbody>

				</table>
 -->












			</div>
			<!-- /.box-body -->
		</div>
		<!-- /.box -->
	</div>
	<!-- /.col -->
</div>
<!-- /.row -->
</section>
<!-- /.content -->
</div>



<?php  
$this->load->view('admin/includes/footer');
$this->load->view('admin/includes/globaljs');?>



<!-- scripts -->


<script>
	$(document).ready(function () {

		$('#view_tree_view').click(function(){
			window.location.href="<?php echo base_url();?>manage-category-tree";
			
		});





		$('#create_category').click(function(){
			$('.fancybox').fancybox();
			
			$.fancybox.open({
				href : "<?php echo base_url();?>admin/category/add_tree",
				type : 'iframe',
				padding : 5,
				'helpers'     : { 
					'overlay' : {'closeClick': false}
				},

			});
		});

	});
</script>
<script>









	$(function () {
    var
        $table = $('#tree-table'),
        rows = $table.find('tr');

    rows.each(function (index, row) {
        var
            $row = $(row),
            level = $row.data('level'),
            id = $row.data('id'),
            $columnName = $row.find('td[data-column="name"]'),
            children = $table.find('tr[data-parent="' + id + '"]');

        if (children.length) {
            var expander = $columnName.prepend('' +
                '<span class="treegrid-expander glyphicon glyphicon-chevron-right"></span>' +
                '');

            children.hide();

            expander.on('click', function (e) {
                var $target = $(e.target);
                if ($target.hasClass('glyphicon-chevron-right')) {
                    $target
                        .removeClass('glyphicon-chevron-right')
                        .addClass('glyphicon-chevron-down');

                    children.show();
                } else {
                    $target
                        .removeClass('glyphicon-chevron-down')
                        .addClass('glyphicon-chevron-right');

                    reverseHide($table, $row);
                }
            });
        }

        $columnName.prepend('' +
            '<span class="treegrid-indent" style="width:' + 15 * level + 'px"></span>' +
            '');
    });

    // Reverse hide all elements
    reverseHide = function (table, element) {
        var
            $element = $(element),
            id = $element.data('id'),
            children = table.find('tr[data-parent="' + id + '"]');

        if (children.length) {
            children.each(function (i, e) {
                reverseHide(table, e);
            });

            $element
                .find('.glyphicon-chevron-down')
                .removeClass('glyphicon-chevron-down')
                .addClass('glyphicon-chevron-right');

            children.hide();
        }
    };
});


	var table=$('#example1').DataTable({
		"rowCallback": function( row, data, index ) {
	       // alert(data[0]);
			//alert(data["leave_status"]);
            /*if(index%2 == 0){
                $(row).removeClass('myodd myeven');
                $(row).addClass('myodd');
            }else{
                $(row).removeClass('myodd myeven');
                 $(row).addClass('myeven');
              }*/
			/*if(data["leave_status"] == 0){
                $(row).removeClass('myodd myeven');
                $(row).addClass('myodd');
            }else{
                $(row).removeClass('myodd myeven');
                 $(row).addClass('myeven');
              }*/


           },


           /*"sDom": "<'row'<'col-md-2 no1'l><'col-md-3 no2'><'col-md-6 yes'f>><'table-scrollable' t><'row'<'col-md-6'i><'col-md-6'p>>",*/
           "sDom": "<'row'<'col-md-2 no1'l><'col-md-4 no2'><'col-md-6 yes'f>><'table-responsive' t><'row'<'col-md-6'i><'col-md-6'p>>",
           "processing": true,
           "serverSide": true,
           "responsive": true,
           "bLengthChange": true,
	/*"autoWidth": false,
	"scrollY": false,
	"scrollX": false,*/
	"language": {
		"infoFiltered": ""
	},
	"columnDefs": [
	{
		"targets": 0,
		"orderable": true
	},	
	{
		"targets": 1,
		"orderable": false
	}
	],
	"ajax": {
		"url": "<?php echo base_url();?>admin/category/categorylist",
		"type": "GET"
	}

});

/*    $("#fbusiness").appendTo(".no2");
		 
  	$("#fbusiness").on('keyup ',function(e){
     		    	// for text boxes 
		var a =$("#fbusiness").val(); 
		table.columns('0').search(a).draw();
	
	});*/
	
		
	$( document ).on( 'click', '.edit_brand_relation', function() {
		var val=$(this).attr('val');
	//alert(val);
		   // $('.fancybox').fancybox();

		   $.fancybox.open({
		   	href : "<?php echo base_url();?>admin/category/addCategoryBrand/"+val,
		   	type : 'iframe',
		   	padding : 5,
		   	'helpers'     : { 
		   		'overlay' : {'closeClick': true}
		   	},
		   	'afterClose': function() {  },
		   	'closeClick': true	

		   });


		});
	
	
	$( document ).on( 'click', '.edit', function() {
		var val=$(this).attr('val');
	//alert(val);
		   // $('.fancybox').fancybox();

		   $.fancybox.open({
		   	href : "<?php echo base_url();?>admin/category/add/"+val,
		   	type : 'iframe',
		   	padding : 5,
		   	'helpers'     : { 
		   		'overlay' : {'closeClick': true}
		   	},
		   	'afterClose': function() {  },
		   	'closeClick': true	

		   });


		});
	$( document ).on( 'click', '.view', function() {
		var val=$(this).attr('val');
	//alert(val);
	$('.fancybox').fancybox();

	$.fancybox.open({
		href : "<?php echo base_url();?>admin/category/view/"+val,
		type : 'iframe',
		padding : 5,
		'helpers'     : { 
			'overlay' : {'closeClick': false}
		},

	});
	
	
});	
	
	
	

	$( document ).on( 'click', '.delete', function() {

		var oTable=$('#example1').DataTable();
		var val=$(this).attr('val');
		BootstrapDialog.confirm({
			title: 'WARNING',
			message: 'Are you sure want to delete?',
		type: BootstrapDialog.TYPE_WARNING, // <-- Default value is BootstrapDialog.TYPE_PRIMARY
		closable: true, // <-- Default value is false
		draggable: true, // <-- Default value is false
		btnCancelLabel: 'Cancel', // <-- Default value is 'Cancel',
		btnOKLabel: 'Ok', // <-- Default value is 'OK',
		btnOKClass: 'btn-warning', // <-- If you didn't specify it, dialog type will be used,
		callback: function(result) {
			// result will be true if button was click, while it will be false if category close the dialog directly.
			if(result) {
				$.ajax({
					method:"post",
					url: "<?php echo base_url();?>admin/category/deletecategory",
					data:'category_id='+val,
					success: function(result){
				//oTable.ajax.reload();
				$('#example1').dataTable().fnStandingRedraw();
			}
		});
			}
			else{

			}
		}
	});
		

	});
	$.fn.dataTableExt.oApi.fnStandingRedraw = function(oSettings) {
    //redraw to account for filtering and sorting
    // concept here is that (for client side) there is a row got inserted at the end (for an add)
    // or when a record was modified it could be in the middle of the table
    // that is probably not supposed to be there - due to filtering / sorting
    // so we need to re process filtering and sorting
    // BUT - if it is server side - then this should be handled by the server - so skip this step
    if(oSettings.oFeatures.bServerSide === false){
    	var before = oSettings._iDisplayStart;
    	oSettings.oApi._fnReDraw(oSettings);
        //iDisplayStart has been reset to zero - so lets change it back
        oSettings._iDisplayStart = before;
        oSettings.oApi._fnCalculateEnd(oSettings);
     }

    //draw the 'current' page
    oSettings.oApi._fnDraw(oSettings);
 };


var prev_val;
$(document).on('focus', '#status',function() {
    prev_val = $(this).val();
}).on('change', '#status',function() {


	var oTable=$('#example1').DataTable();
	var val=this.value;
	BootstrapDialog.confirm({
		title: 'CONFIRM',
		message: 'Are you sure you want to change this?',
		type: BootstrapDialog.TYPE_WARNING, // <-- Default value is BootstrapDialog.TYPE_PRIMARY
		closable: true, // <-- Default value is false
		draggable: true, // <-- Default value is false
		btnCancelLabel: 'Cancel', // <-- Default value is 'Cancel',
		btnOKLabel: 'Ok', // <-- Default value is 'OK',
		btnOKClass: 'btn-warning', // <-- If you didn't specify it, dialog type will be used,
		callback: function(result) {
			// result will be true if button was click, while it will be false if category close the dialog directly.
			if(result) {
			
		//alert(result);
	   //alert(val);
	   var arr=val.split("@@@");
	
	   var status=arr[0];
	   var id=arr[1];
	   
	   //alert("id="+id+"&status="+status);
		
	   $.ajax({
		   method:'get',
			url: "<?php echo base_url();?>admin/category/updatestatus", 
			data:"id="+id+"&status="+status,
			success: function(result){
				if(result)
				{
				    //alert(result);
					// return false;
					
				}
		      }
		});
	
	 }
			else{
			
		//alert(prev_val);
        $('#status').val(prev_val);
       // alert('unchanged');
       // return false; 
			  
			}
		}
    });




});


</script>


<script type="text/javascript" src="<?php echo base_url();?>theme/assets/source/jquery.fancybox.js?v=2.1.5"></script>
<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>theme/assets/source/jquery.fancybox.css?v=2.1.5" media="screen" />
