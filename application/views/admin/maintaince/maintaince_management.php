<?php  //$this->load->view('admin/includes/globalcss');?>

<!-- Last Edited: KW 25/09/16 -->
<?php
$page_plugins = "";
$this->load->view('admin/includes/header');
$this->load->view('admin/includes/sidebar');
?><!-- 
<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>theme/assets/source/jquery.fancybox.css?v=2.1.5" media="screen" />	 -->

<!-- <link type="text/css" href="<?php echo base_url();?>assets/form-select2/select2.css" rel="stylesheet"/> -->
<!-- <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootswatch/3.3.7/lumen/bootstrap.min.css"> -->


<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>website-assets/datetimepicker/css/bootstrap-datetimepicker.min.css">

<style>
/*.table-bordered {
    border: 1px solid #666666;
}*/
.showdbutton {
    cursor:pointer; 
	background-color:#666666; 
	margin:10px 0px; 
	padding:10px; 
	color:#FFFFFF;
	width:100%;
}

.disabledbutton {
    pointer-events: none;
    opacity: 0.9;	
	cursor:pointer; 
	background-color:#999999; 
	margin:10px 0px; 
	padding:10px; 
	color:#FFFFFF;
	width:100%;
}
label.error{
 color:#FF0000;
 font-size:12px;

}
.table-condensed{
	width: 100% !important;
}
.datetimepicker{
	    width: 15% !important;
}
.datetimepicker td, .datetimepicker th{
	width: 50px !important;
	height: 20px !important;
}

    
.datetimepicker th span.glyphicon {
    border: solid black !important;
    border-width: 0 3px 3px 0 !important;
    display: inline-block !important;
    padding: 0px !important;
    font-size: 10px;
    line-height: 1.0 !important;
}
.datetimepicker th span.glyphicon { color: transparent !important; }
.datetimepicker th:hover { background-color: transparent !important; }
</style>
<div class="content-wrapper">
  <!-- Section content header -->
 	<section class="content-header" >
     <div class="col-md-12" > 
     	<h1> Maintenance Management
      </h1></div>

    </section>

  <!-- Section content -->
    <section class="content">
		<div class="col-md-12" style='overflow-y:scroll; overflow-x:hidden; height:80vh;'>
		  <!-- start Information Box -->
		
		<br>
					<!-- /.box-header -->
					<!-- form start -->
					
					
					
				    <div id="home" class="tab-pane fade in active">
				    	<div id="success_message" style="display:none;">
					<div class="alert alert-success">
					<strong>Updated successfully!</strong>.
					</div>
				    </div>
				    <div id="error_message" style="display:none;">
					<div class="alert alert-danger">
					<strong>Something wrong!</strong>.
					</div>
				    </div>
				    	<form id='setting_form' class='form form-horizontal has-validation-callback'>
					<?php  
					// $array = array('id'=>'setting_form', 'role'=>'form', 'class'=>'form form-horizontal has-validation-callback');
				 //   echo form_open_multipart('', $array); 

					// print_r($setting_content) ;
				 
					foreach ($setting_content as $setting_key1 => $setting_value1) {
						$setting_id=$setting_content[$setting_key1]['setting_id'];
						$setting_key=$setting_content[$setting_key1]['setting_key'];
						$setting_value=$setting_content[$setting_key1]['setting_value'];
						?>
						<input type='hidden' name='setting_id[]' value='<?php echo $setting_id; ?>' id='setting_id'>
						<input type='hidden' name='setting_key[]' value='<?php echo $setting_key; ?>' id='setting_key'>
							<div class="form-group">
					    <label for="inputEmail3"  class="col-sm-3 control-label"><?php echo $setting_key;?>
                        <span style="color:#F00">*</span>
                        </label>
						<div class="col-sm-8">
						<?php
						if((strpos($setting_key, 'MAINTENANCE_MODE_ALLOW_IPS')!== false) && $setting_id==3)
						{
							?>
						    <br><textarea id="<?php echo $setting_key; ?>"  class="form-control mb-3 border-0 form-control" name="setting_value[]" style="height: 200px !important;" row="50" ><?php echo $setting_value;?></textarea>
						    <?php echo form_error('setting_value', '<div class="error" style="color:red;">', '</div>'); ?>
						    <!-- <script>
 $(document).ready(function() {
 	 
		CKEDITOR.replace('<?php echo $setting_key; ?>', {
			allowedContent:true,
			//extraAllowedContent : '*(*)',
			// readOnly:true,

});
    });
</script> -->
						<?php

						}
						elseif((strpos($setting_key, 'MAINTENANCE_MODE')!== false) && $setting_id==1)
						{
							?>
						    <select name="setting_value[]" id="<?php echo $setting_key; ?>"  class="form-control mb-3 border-0 form-control">
							<option value="0" <?php if($setting_value=='0') { echo "selected"; }  ?>>No</option>
							<option value="1" <?php if($setting_value=='1') { echo "selected"; }  ?>>Yes</option>
							</select>
							<?php echo form_error('setting_value', '<div class="error" style="color:red;">', '</div>'); ?>
						    <!-- <script>
 $(document).ready(function() {
 	 
		CKEDITOR.replace('<?php echo $setting_key; ?>', {
			allowedContent:true,
			//extraAllowedContent : '*(*)',
			// readOnly:true,

});
    });
</script> -->
						<?php

						}
						else{

							?>
							
									
							
							

							<input type='text' class="form-control mb-3 border-0 datePick form-control" name='setting_value[]' value='<?php echo $setting_value; ?>' id='<?php echo $setting_key; ?>'  placeholder="" readonly>
						    <script type="text/javascript">

									$(function () {
										
									var date = new Date();
									//alert(date);
                                    date.setDate(date.getDate());
									$('#MAINTENANCE_MODE_UPTO').datetimepicker({
									//language:  'fr',
									weekStart: 1,
									todayBtn:  1,
									autoclose: 1,
									todayHighlight: 1,
									startView: 2,
									forceParse: 0,
									showMeridian: 1,
 									startDate: date
									});

									});
									</script>
						    <?php echo form_error('setting_value', '<div class="error" style="color:red;">', '</div>'); ?>
						
							<?php

						}
						?>
						</div>
						
					</div>
							
						<?php
					}

					// echo($setting_content[0]['setting_key']);

					 ?>
			
					<!-- Contractor type fields end-->
					<div class="row">
					<?php if($setting_id!=''){
						$button_name='Update';
					}else {
						$button_name='Save';
					} ?>
						<div class="col-md-6"><input class="btn btn-success pull-right" type="button" value="<?php echo $button_name;?>" id="submit"></div>	   
						<div class="col-md-6"><input class="btn btn-danger pull-left" type="reset"></div>
					</div>
					</form>
					<?php // echo form_close();?> 
				    </div>
				    
				
					
				</div>
		
	</section>
</div>

<?php $this->load->view('admin/includes/globaljs');?>

<script src="<?php echo base_url(); ?>assets/ckeditor/ckeditor.js"></script>

<script type="text/javascript" src="<?php echo base_url(); ?>website-assets/datetimepicker/js/bootstrap-datetimepicker.js" charset="UTF-8"></script
>
<script type="text/javascript" src="<?php echo base_url();?>assets/form-select2/select2.min.js"></script>



<!--<script  type="text/javascript" src="<?php echo base_url();?>assets/dist/password-score.js"></script>
<script  type="text/javascript" src="<?php echo base_url();?>assets/dist/password-score-options.js"></script>
<script  type="text/javascript" src="<?php echo base_url();?>assets/dist/bootstrap-strength-meter.js"></script>-->

<!-- <script type="text/javascript" src="<?php echo base_url();?>theme/assets/source/jquery.fancybox.js?v=2.1.5"></script>

 -->
<!-- <script type="text/javascript" src="<?php echo base_url(); ?>website-assets/datetimepicker/js/locales/bootstrap-datetimepicker.fr.js" charset="UTF-8"></script> -->
			
<script type="text/javascript">
	

	$(document).ready(function() {
	   // $('.content-wrapper').css('min-height','900px');	   $('#entity_id').select2();
	 
	   	$('#submit').click(function(){
			var formData = new FormData($('#setting_form')[0]);
			//alert("submit");
			// console.log(formData);
		   $.ajax({
				method:'post',
				url: "<?php echo base_url();?>admin/maintaince/update_maintaince_setting", 
				data:formData,
				processData: false,
				contentType: false
				}).done(function( result ) {
					//alert(result);
					$("#success_message").show();
					setTimeout(function(){setInterval(function(){
						$("#success_message").hide();
						window.location.reload();
					}, 3000)}, 3000);
					//
				}).fail(function() {
					$("#error_message").show();
					setTimeout(function(){setInterval(function(){
						$("#error_message").hide();
						window.location.reload();
					}, 3000)}, 3000);
					//window.location.reload();
				});	
		 //  document.setting_form.submit();
		});	
	});	


</script>