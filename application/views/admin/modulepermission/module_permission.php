<!-- Last Edited: KW 25/09/16 -->
<?php
$page_plugins = "";
$this->load->view('admin/includes/header');
$this->load->view('admin/includes/sidebar');
?>
<style>
table.dataTable tbody tr.myeven{
    background-color:#F9843E;
}
table.dataTable tbody tr.myodd{
   /* background-color:#00FF00;*/
}
</style>
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
     <div class="col-md-12"> <h1> Module Permission Management
      </h1></div>

    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row" >
        <div class="col-md-12">

          <div class="box">
      
	  	<!-- box header style start-->
				   <br> 
		 
		 
	
				
			<!-- /.box-header -->
			<div class="box-body table-responsive-vertical shadow-z-1">


			

			<?php  $array = array('id'=>'frm_module_permission', 'role'=>'form', 'class'=>'form form-horizontal has-validation-callback');
				    echo form_open_multipart('admin/Modulepermission/update_module_permission', $array); ?>

			<div class="container">
			<h2>Module Permission</h2>
			<!-- <p>The .table class adds basic styling (light padding and only horizontal dividers) to a table:</p>             -->
			<table class="table">
			<thead>
			<tr>
			<th>&nbsp;</th>
			<?php

			
			$user_type_details=$this->db->query("select * from wc_users_type where deleted='0' and status='Active' order by user_type_id ASC ")->result_array();

			if(!empty($user_type_details))
			{
			foreach($user_type_details as $key => $usertypedetails)
			{
			$user_type_id=$usertypedetails['user_type_id'];
			$user_type_name=$usertypedetails['user_type_name'];
			?>
			<th><?php echo $user_type_name;?></th>
			<?php
			}
			}
			?>



			</tr>
			</thead>
			<tbody>
            
			<?php
			$module_details=$this->db->query("select * from wc_module where deleted='0' and status='Active' ")->result_array();
			if(!empty($module_details))
			{
			foreach($module_details as $key => $moduledetails)
			{
			$module_id=$moduledetails['module_id'];
			$module_name=$moduledetails['module_name'];
			?>

			<tr>
			<td><h4><?php echo $module_name;?></h4></td>
			<td></td>
			<td></td>
			<td></td>
			<td></td>
			</tr>

			</tr>


			<?php
            if($module_id=='1')
            {
            	$str_sel='access';
            }
            elseif ($module_id=='2') {
             	$str_sel='access';
             }
            elseif ($module_id=='3') {
             	$str_sel='upload,accept_reject';
             }
             elseif ($module_id=='4') {
             	$str_sel='view,edit,approve_reject,add_revision';
             } 
             elseif ($module_id=='5') {
             	$str_sel='view,edit,upload';
             } 
             elseif ($module_id=='6') {
             	$str_sel='view_export';
             } 
             elseif ($module_id=='7') {
             	$str_sel='view,export';
             } 
             elseif ($module_id=='8') {
             	$str_sel='access';
             } 
             elseif ($module_id=='9') {
             	$str_sel='access';
             } 
             elseif ($module_id=='10') {
             	$str_sel='access';
             } 
             elseif ($module_id=='11') {
             	$str_sel='access';
             }   


            $arr_select=explode(",",$str_sel);

            for($i=0;$i<count($arr_select);$i++)
            {
                $str_select=$arr_select[$i];
            ?>

			<tr>

			<td>
			<h6><?php echo str_replace("_","/",ucfirst($str_select));?></h6>
			</td>


           <?php

            

            /*$sql="select ".$str_select." as str_select_field from wc_module_permission where module_id='$module_id' order by user_type_id ASC  ";
            echo $sql; exit;
			$module_permission_details=$this->db->query()->result_array($sql);*/

			//echo "select module_permission_id,module_id,user_type_id,".$str_select." as str_select_field from wc_module_permission where module_id='$module_id' order by user_type_id ASC  ";
			$module_permission_details=$this->db->query("select module_permission_id,module_id,user_type_id,".$str_select." as str_select_field from wc_module_permission where module_id='$module_id' order by user_type_id ASC  ")->result_array();

			if(!empty($module_permission_details))
			{
			foreach($module_permission_details as $key => $modulepermissiondetails)
			{

			$module_permission_id=$modulepermissiondetails['module_permission_id'];	
			$module_id=$modulepermissiondetails['module_id'];
			$user_type_id=$modulepermissiondetails['user_type_id'];
			$str_select_field=$modulepermissiondetails['str_select_field'];
			
			?>

			<td>
			<div class="checkbox">
			<label>
				<?php ///echo  $module_permission_id."--".$str_select_field."<br>";?>
				<input type="checkbox" name="<?php echo $str_select; ?>[<?php echo $module_permission_id;?>]" id="<?php echo $str_select; ?>_<?php echo $module_permission_id;?>" value="<?php echo $str_select_field; ?>"  <?php if($str_select_field=='1') { ?> checked <?php } ?> >
			</label>
			</div>	
			</td>

			
			<?php

			}
		   }

			?>





			</tr>

			<?php

            }

            ?>

		

			<?php

			}
			}
			?>
			
			
			</tbody>
			</table>

			<div class="row">

			<div class="col-md-6"><input class="btn btn-success pull-right" type="submit" value="Save"></div>	   
			<!-- <div class="col-md-6"><input class="btn btn-danger pull-left" type="reset"></div> -->
			</div>
			</div>
			<!-- Contractor type fields end-->
			


			</div>
		</form>
			<!-- /.box-body -->
			</div>
			<!-- /.box -->
			</div>
			<!-- /.col -->
			</div>
			<!-- /.row -->
			</section>
			<!-- /.content -->
			</div>



 <?php  
  $this->load->view('admin/includes/footer');
  $this->load->view('admin/includes/globaljs');?>


	
<!-- scripts -->


<script>
    $(document).ready(function () {
		$('#create_module').click(function(){
		    $('.fancybox').fancybox();
			
				$.fancybox.open({
					href : "<?php echo base_url();?>admin/module/add",
					type : 'iframe',
					padding : 5,
						'helpers'     : { 
						'overlay' : {'closeClick': false}
						},
					
				});
	    });
	
	});
</script>
<script>

 var table=$('#example1').DataTable({
      "rowCallback": function( row, data, index ) {
	       // alert(data[0]);
			//alert(data["leave_status"]);
            /*if(index%2 == 0){
                $(row).removeClass('myodd myeven');
                $(row).addClass('myodd');
            }else{
                $(row).removeClass('myodd myeven');
                 $(row).addClass('myeven');
            }*/
			/*if(data["leave_status"] == 0){
                $(row).removeClass('myodd myeven');
                $(row).addClass('myodd');
            }else{
                $(row).removeClass('myodd myeven');
                 $(row).addClass('myeven');
            }*/
			
			
          },
 
 
	/*"sDom": "<'row'<'col-md-2 no1'l><'col-md-3 no2'><'col-md-6 yes'f>><'table-scrollable' t><'row'<'col-md-6'i><'col-md-6'p>>",*/
	"sDom": "<'row'<'col-md-2 no1'l><'col-md-4 no2'><'col-md-6 yes'f>><'table-responsive' t><'row'<'col-md-6'i><'col-md-6'p>>",
	"processing": true,
	"serverSide": true,
	"responsive": true,
	"bLengthChange": true,
	/*"autoWidth": false,
	"scrollY": false,
	"scrollX": false,*/
	"language": {
		"infoFiltered": ""
	},
	"columnDefs": [
		{
		"targets": 0,
		"orderable": true
		},	
		{
		"targets": 1,
		"orderable": false
		}
	],
	"ajax": {
		"url": "<?php echo base_url();?>admin/module/modulelist",
		"type": "GET"
	}
		 
  });


	
	
	
	
$( document ).on( 'click', '.edit', function() {
	var val=$(this).attr('val');
	       //alert(val);
		   // $('.fancybox').fancybox();
			
				$.fancybox.open({
					href : "<?php echo base_url();?>admin/module/add/"+val,
					type : 'iframe',
					padding : 5,
					'helpers'     : { 
						'overlay' : {'closeClick': true}
						},
					'afterClose': function() {  },
                    'closeClick': true	
					
				});
	
	
});
$( document ).on( 'click', '.view', function() {
		var val=$(this).attr('val');
		//alert(val);
		$('.fancybox').fancybox();

		$.fancybox.open({
		href : "<?php echo base_url();?>admin/module/view/"+val,
		type : 'iframe',
		padding : 5,
		'helpers'     : { 
		'overlay' : {'closeClick': false}
		},

		});


});	

	
	
	 
$( document ).on( 'click', '.delete', function() {
	
	var oTable=$('#example1').DataTable();
	var val=$(this).attr('val');
	BootstrapDialog.confirm({
		title: 'WARNING',
		message: 'Are you sure want to delete?',
		type: BootstrapDialog.TYPE_WARNING, // <-- Default value is BootstrapDialog.TYPE_PRIMARY
		closable: true, // <-- Default value is false
		draggable: true, // <-- Default value is false
		btnCancelLabel: 'Cancel', // <-- Default value is 'Cancel',
		btnOKLabel: 'Ok', // <-- Default value is 'OK',
		btnOKClass: 'btn-warning', // <-- If you didn't specify it, dialog type will be used,
		callback: function(result) {
			// result will be true if button was click, while it will be false if users close the dialog directly.
			if(result) {
			 $.ajax({
				 method:"post",
				url: "<?php echo base_url();?>admin/module/deletemodule",
				data:'module_id='+val,
				success: function(result){
				//oTable.ajax.reload();
				$('#example1').dataTable().fnStandingRedraw();
				}
			});
			}
			else{
			  
			}
		}
    });
		

});
		$.fn.dataTableExt.oApi.fnStandingRedraw = function(oSettings) {
    //redraw to account for filtering and sorting
    // concept here is that (for client side) there is a row got inserted at the end (for an add)
    // or when a record was modified it could be in the middle of the table
    // that is probably not supposed to be there - due to filtering / sorting
    // so we need to re process filtering and sorting
    // BUT - if it is server side - then this should be handled by the server - so skip this step
    if(oSettings.oFeatures.bServerSide === false){
        var before = oSettings._iDisplayStart;
        oSettings.oApi._fnReDraw(oSettings);
        //iDisplayStart has been reset to zero - so lets change it back
        oSettings._iDisplayStart = before;
        oSettings.oApi._fnCalculateEnd(oSettings);
    }
      
    //draw the 'current' page
    oSettings.oApi._fnDraw(oSettings);
};


var prev_val;
$(document).on('focus', '#status',function() {
    prev_val = $(this).val();
}).on('change', '#status',function() {


	var oTable=$('#example1').DataTable();
	var val=this.value;
	BootstrapDialog.confirm({
		title: 'CONFIRM',
		message: 'Are you sure you want to change this?',
		type: BootstrapDialog.TYPE_WARNING, // <-- Default value is BootstrapDialog.TYPE_PRIMARY
		closable: true, // <-- Default value is false
		draggable: true, // <-- Default value is false
		btnCancelLabel: 'Cancel', // <-- Default value is 'Cancel',
		btnOKLabel: 'Ok', // <-- Default value is 'OK',
		btnOKClass: 'btn-warning', // <-- If you didn't specify it, dialog type will be used,
		callback: function(result) {
			// result will be true if button was click, while it will be false if users close the dialog directly.
			if(result) {
			
		//alert(result);
	   //alert(val);
	   var arr=val.split("@@@");
	
	   var status=arr[0];
	   var id=arr[1];
	   
	   //alert("id="+id+"&status="+status);
		
	   $.ajax({
		   method:'get',
			url: "<?php echo base_url();?>admin/module/updatestatus", 
			data:"id="+id+"&status="+status,
			success: function(result){
				if(result)
				{
				   // alert(result);
					// return false;
					
				}
		      }
		});
	
	 }
			else{
			
		//alert(prev_val);
        $('#status').val(prev_val);
       // alert('unchanged');
       // return false; 
			  
			}
		}
    });




});


</script>
	

<script type="text/javascript" src="<?php echo base_url();?>theme/assets/source/jquery.fancybox.js?v=2.1.5"></script>
<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>theme/assets/source/jquery.fancybox.css?v=2.1.5" media="screen" />
