<?php
class Myaccount_model extends CI_Model {

	public function __construct()
    {
        $this->load->database();
        $this->load->library('session');
    }
	
	public function get_my_data($user_id) {

        $sql = "select * from wc_users where user_id = '$user_id'";
        $query = $this->db->query($sql);
        return $query->result_array();
    }

	public function update_me($userdata, $user_id)
    {
        $this->db->where('user_id', $user_id);

        $usr_update = $this->db->update('wc_users', $userdata);

        if ($usr_update) {
            return true;
        } else {
            return false;
        }
    }

    public function update_my_pswd($userpswd, $user_id)
    {
        $this->db->where('user_id', $user_id);

        $pswd_update = $this->db->update('wc_users', $userpswd);

        if ($pswd_update) {
            return true;
        } else {
            return false;
        }
    }

}


?>