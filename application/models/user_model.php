<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
 
class User_model extends CI_Model
{
    public function __construct()
    {
        $this->load->database();
        $this->load->library('session');
    }
	
	public function user_insert($data)
	{
        $this->db->insert('wc_users', $data);
    }
	
	
	
	
	public function user_update($userdata, $user_id)
    {
        $this->db->where('user_id', $user_id);
        $report = $this->db->update('wc_users', $userdata);
        if ($report) {
            return true;
        } else {
            return false;
        }
    }
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	

    public function count_items()
    {
        return $this->db->count_all('customer_master');
    }

    public function get_customer_list($limit, $start)
    {
        $sql = 'select * from customer_master order by name DESC limit ' . $start . ', ' . $limit;
        $query = $this->db->query($sql);
        return $query->result();
    }

    public function delete_customer($customer_id)
    {
        $this->db->where('customer_id', $customer_id);
        $this->db->delete('customer_master');
    }
	
 public function customer_insert($data)
    {
        $this->db->insert('customer_master', $data);
    }
	
	
	 public function get_singlecustomer($customer_id)
    {
        if ($customer_id != FALSE) {
            $query = $this->db->get_where('customer_master', array('customer_id' => $customer_id));
            return $query->row_array();
        } else {
            return FALSE;
        }

    }
	public function customerupdate($customerdata, $customer_id)
    {
        $this->db->where('customer_id', $customer_id);
        $report = $this->db->update('customer_master', $customerdata);
        if ($report) 
        {
            return true;
        } 
        else 
        {
            return false;
        }
    }
	
	
	
	
	
	 public function get_singleadmin($user_id)
    {
        if ($user_id != FALSE) {
            $query = $this->db->get_where('pj_user', array('user_id' => $user_id));
            return $query->row_array();
        } else {
            return FALSE;
        }

    }
	
	
		
	
	 public function get_singleBrand($Brand_id)
    {
        if ($Brand_id != FALSE) {
            $query = $this->db->get_where('tblbrands', array('Brand_id' => $Brand_id));
            return $query->row_array();
        } else {
            return FALSE;
        }

    }
	
	
	
	
	
	
	
	public function brand_insert($data)
	{
        $this->db->insert('tblbrands', $data);
    }

	
public function get_Brand($BrandID)
{
    if ($BrandID != FALSE) 
    {
        $query = $this->db->get_where('tblbrands', array('BrandID' => $BrandID));
        return $query->row_array();
    } 
    else 
    {
        return FALSE;
    }

}
	
	public function brand_update($data, $BrandID)
    {
        $this->db->where('BrandID', $BrandID);
        $report = $this->db->update('tblbrands', $data);
        if ($report) {
            return true;
        } else {
            return false;
        }
    }
	
	
	
		
	public function Brand_Fitting_update($data, $BFittingID)
    {
        $this->db->where('BFittingID', $BFittingID);
        $report = $this->db->update('tblbrandfittings', $data);
		
		$sql = $this->db->last_query();
        if ($report) {
            return true;
        } else {
            return false;
        }
    }
	
		public function Brand_Fitting_insert($data)
	{
        $this->db->insert('tblbrandfittings', $data);
    }
	
	
	
	
	
	
	
	
	
		public function admin_update($data, $user_id)
    {
        $this->db->where('user_id', $user_id);
        $report = $this->db->update('pj_user', $data);
        if ($report) {
            return true;
        } else {
            return false;
        }
    }
	
	public function delete_brand($BrandID)
    {
        $this->db->where('BrandID', $BrandID);
        $this->db->delete('tblbrands');
    }
	
	public function delete_brand_fitting($BFittingID)
    {
        $this->db->where('BFittingID', $BFittingID);
        $this->db->delete('tblbrandfittings');
    }
	
	public function days_insert($data1)
	{
		$date_from=$data1['date_from'];
		$date_to=$data1['date_to'];
		$owner_id=$data1['owner_id'];
		$service_id=$data1['service_id'];

		for($i=0;$i<count($date_from);$i++)
		{
			
			$this->db->insert('nonworking_days', array('date_from'=>date('Y-m-d H:i:s',strtotime($date_from[$i])),'date_to'=>date('Y-m-d H:i:s',strtotime($date_to[$i])),
			'created_on'=> date('Y-m-d H:i:s'),'owner_id'=>$owner_id,'service_id'=>$service_id));
		}   
	}

	public function days_update($data1,$id)
	{
		$date_from=$data1['date_from'];
		$date_to=$data1['date_to'];
		$owner_id=$data1['owner_id'];
		$service_id=$data1['service_id'];

		for($i=0;$i<count($date_from);$i++)
		{
			$this->db->where('id', $id);
			$this->db->update('nonworking_days', array('date_from'=>date('Y-m-d H:i:s',strtotime($date_from[$i])),'date_to'=>date('Y-m-d H:i:s',strtotime($date_to[$i])),
			'created_on'=> date('Y-m-d H:i:s'),'owner_id'=>$owner_id,'service_id'=>$service_id));
		}   
	}
	
	
	
	 public function get_setting_accordion($setting_key)
    {
        if ($setting_key != FALSE) {
            $query = $this->db->get_where('setting', array('setting_key' => $setting_key));
            return $query->row_array();
        } else {
            return FALSE;
        }

    }
	
	
	public function get_allcountries()
	{
	
		$sql = 'select * from countries order by name ASC  ';
        $query = $this->db->query($sql);
        return $query->result();
	
	
	}
	public function get_allstates()
	{
	
		$sql = 'select * from states order by name ASC  ';
        $query = $this->db->query($sql);
        return $query->result();
	
	
	}
	
	
	public function get_allcities()
	{
	
		$sql = 'select * from cities order by name ASC  ';
        $query = $this->db->query($sql);
        return $query->result();
	
	
	}
	
	public function Brand_leave_insert($data)
	{
	$this->db->insert('tblbrands_leave', $data);
	}
	
	public function get_Brand_leave($leave_id)
	{
	if ($Brand_id != FALSE) 
	{
	$query = $this->db->get_where('tblbrands_leave', array('leave_id' => $leave_id));
	return $query->row_array();
	} 
	else 
	{
	return FALSE;
	}
	
	}
	
	public function Brand_leave_update($data, $leave_id)
	{
	$this->db->where('leave_id', $leave_id);
	$report = $this->db->update('tblbrands_leave', $data);
	if ($report) {
	return true;
	} else {
	return false;
	}
	}
	
	
	
	/* public function getBrandleave_count()
    {
        return $this->db->count_all('tblbrands_leave');
    }*/
	 public function getBrandleave_count($Brand_id)
    {
         $sql = 'select * from tblbrands_leave where Brand_id="'.$Brand_id.'" ';
        $query = $this->db->query($sql);
        return $query->result();
    }

    public function getallBrandleave($Brand_id,$limit,$offset)
    {
        $sql = 'select * from tblbrands_leave where Brand_id="'.$Brand_id.'" order by leave_id DESC limit ' . $offset . ', ' . $limit;
        $query = $this->db->query($sql);
        return $query->result();
    }
	
	
	
	
	 public function delete_brand_leave($leave_id)
    {
        $this->db->where('leave_id', $leave_id);
        $this->db->delete('tblbrands_leave');
    }
	
	
	
	

	
	}

