<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
 
class Category_model extends CI_Model
{
    public function __construct()
    {
        $this->load->database();
        $this->load->library('session');
    }
	
	public function category_insert($data)
	{
        $this->db->insert('wc_category', $data);
    }
	
	
	
	
	public function category_update($categorydata, $category_id)
    {
        $this->db->where('category_id', $category_id);
        $report = $this->db->update('wc_category', $categorydata);
        if ($report) {
            return true;
        } else {
            return false;
        }
    }




    public function category_brand_relation_insert($data)
	{
        $this->db->insert('wc_category_brand_relation', $data);
    }
	
	
	
	
	public function category_brand_relation_update($categorydata, $category_id)
    {
        $this->db->where('category_id', $category_id);
        $report = $this->db->update('wc_category_brand_relation', $categorydata);
        if ($report) {
            return true;
        } else {
            return false;
        }
    }
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	

    public function count_items()
    {
        return $this->db->count_all('customer_master');
    }

    public function get_customer_list($limit, $start)
    {
        $sql = 'select * from customer_master order by name DESC limit ' . $start . ', ' . $limit;
        $query = $this->db->query($sql);
        return $query->result();
    }

    public function delete_customer($customer_id)
    {
        $this->db->where('customer_id', $customer_id);
        $this->db->delete('customer_master');
    }
	
 public function customer_insert($data)
    {
        $this->db->insert('customer_master', $data);
    }
	
	
	 public function get_singlecustomer($customer_id)
    {
        if ($customer_id != FALSE) {
            $query = $this->db->get_where('customer_master', array('customer_id' => $customer_id));
            return $query->row_array();
        } else {
            return FALSE;
        }

    }
	public function customerupdate($customerdata, $customer_id)
    {
        $this->db->where('customer_id', $customer_id);
        $report = $this->db->update('customer_master', $customerdata);
        if ($report) 
        {
            return true;
        } 
        else 
        {
            return false;
        }
    }
	
	
	
	
	
	 public function get_singleadmin($category_id)
    {
        if ($category_id != FALSE) {
            $query = $this->db->get_where('pj_category', array('category_id' => $category_id));
            return $query->row_array();
        } else {
            return FALSE;
        }

    }
	
	
		
	
	 public function get_singlecategory($category_id)
    {
        if ($category_id != FALSE) {
            $query = $this->db->get_where('tblcategory', array('category_id' => $category_id));
            return $query->row_array();
        } else {
            return FALSE;
        }

    }
	
	
	
	
	
	
	
	
public function get_category($categoryID)
{
    if ($categoryID != FALSE) 
    {
        $query = $this->db->get_where('tblcategory', array('categoryID' => $categoryID));
        return $query->row_array();
    } 
    else 
    {
        return FALSE;
    }

}

	
	
		
	public function category_Fitting_update($data, $BFittingID)
    {
        $this->db->where('BFittingID', $BFittingID);
        $report = $this->db->update('tblcategoryfittings', $data);
		
		$sql = $this->db->last_query();
        if ($report) {
            return true;
        } else {
            return false;
        }
    }
	
		public function category_Fitting_insert($data)
	{
        $this->db->insert('tblcategoryfittings', $data);
    }
	
	
	
	
	
	
	
	
	
		public function admin_update($data, $category_id)
    {
        $this->db->where('category_id', $category_id);
        $report = $this->db->update('pj_category', $data);
        if ($report) {
            return true;
        } else {
            return false;
        }
    }
	
	public function delete_category($categoryID)
    {
        $this->db->where('categoryID', $categoryID);
        $this->db->delete('tblcategory');
    }
	
	public function delete_category_fitting($BFittingID)
    {
        $this->db->where('BFittingID', $BFittingID);
        $this->db->delete('tblcategoryfittings');
    }
	
	public function days_insert($data1)
	{
		$date_from=$data1['date_from'];
		$date_to=$data1['date_to'];
		$owner_id=$data1['owner_id'];
		$service_id=$data1['service_id'];

		for($i=0;$i<count($date_from);$i++)
		{
			
			$this->db->insert('nonworking_days', array('date_from'=>date('Y-m-d H:i:s',strtotime($date_from[$i])),'date_to'=>date('Y-m-d H:i:s',strtotime($date_to[$i])),
			'created_on'=> date('Y-m-d H:i:s'),'owner_id'=>$owner_id,'service_id'=>$service_id));
		}   
	}

	public function days_update($data1,$id)
	{
		$date_from=$data1['date_from'];
		$date_to=$data1['date_to'];
		$owner_id=$data1['owner_id'];
		$service_id=$data1['service_id'];

		for($i=0;$i<count($date_from);$i++)
		{
			$this->db->where('id', $id);
			$this->db->update('nonworking_days', array('date_from'=>date('Y-m-d H:i:s',strtotime($date_from[$i])),'date_to'=>date('Y-m-d H:i:s',strtotime($date_to[$i])),
			'created_on'=> date('Y-m-d H:i:s'),'owner_id'=>$owner_id,'service_id'=>$service_id));
		}   
	}
	
	
	
	 public function get_setting_accordion($setting_key)
    {
        if ($setting_key != FALSE) {
            $query = $this->db->get_where('setting', array('setting_key' => $setting_key));
            return $query->row_array();
        } else {
            return FALSE;
        }

    }
	
	
	public function get_allcountries()
	{
	
		$sql = 'select * from countries order by name ASC  ';
        $query = $this->db->query($sql);
        return $query->result();
	
	
	}
	public function get_allstates()
	{
	
		$sql = 'select * from states order by name ASC  ';
        $query = $this->db->query($sql);
        return $query->result();
	
	
	}
	
	
	public function get_allcities()
	{
	
		$sql = 'select * from cities order by name ASC  ';
        $query = $this->db->query($sql);
        return $query->result();
	
	
	}
	
	public function category_leave_insert($data)
	{
	$this->db->insert('tblcategory_leave', $data);
	}
	
	public function get_category_leave($leave_id)
	{
	if ($category_id != FALSE) 
	{
	$query = $this->db->get_where('tblcategory_leave', array('leave_id' => $leave_id));
	return $query->row_array();
	} 
	else 
	{
	return FALSE;
	}
	
	}
	
	public function category_leave_update($data, $leave_id)
	{
	$this->db->where('leave_id', $leave_id);
	$report = $this->db->update('tblcategory_leave', $data);
	if ($report) {
	return true;
	} else {
	return false;
	}
	}
	
	
	
	/* public function getcategoryleave_count()
    {
        return $this->db->count_all('tblcategory_leave');
    }*/
	 public function getcategoryleave_count($category_id)
    {
         $sql = 'select * from tblcategory_leave where category_id="'.$category_id.'" ';
        $query = $this->db->query($sql);
        return $query->result();
    }

    public function getallcategoryleave($category_id,$limit,$offset)
    {
        $sql = 'select * from tblcategory_leave where category_id="'.$category_id.'" order by leave_id DESC limit ' . $offset . ', ' . $limit;
        $query = $this->db->query($sql);
        return $query->result();
    }
	
	
	
	
	 public function delete_category_leave($leave_id)
    {
        $this->db->where('leave_id', $leave_id);
        $this->db->delete('tblcategory_leave');
    }
	
	
	
	

	
	}

