<?php
class Settings_model extends CI_Model{
    function get_product_list(){
        if ($this->session->userdata('front_logged_in')) {
            $session_data = $this->session->userdata('front_logged_in');
            $user_id = $session_data['user_id'];
            $user_type_id = $session_data['user_type_id'];
            $user_name = $session_data['user_name'];
            $type1 = $session_data['user_type_id'];
            $brand_access=$session_data['brand_access'];
            $client_access=$session_data['client_access'];
            $user_type_id=$type1;
        }
        else{
            $user_id = '';
            $user_type_id = '';
            $user_name = '';
        }
       
        $sql="select * from  wc_product_price where custom_status='0'";

        //echo $sql;
        $query=$this->db->query($sql);
        //return $query->result();
        return $query->result_array();
    }
    public function add_product($productdata){
        $report = $this->db->insert('wc_product_price', $productdata);
        if ($report) {
            return true;
        } else {
            return false;
        }
    }
    public function update_product($productdata, $product_id){
        $this->db->where('product_id', $product_id);
        $report = $this->db->update('wc_product_price', $productdata);
        if ($report) {
            return true;
        } else {
            return false;
        }
    }
    public function delete_product($product_id){
        $array = array('product_id' => $product_id);
        $this->db->where($array); 
        $result=$this->db->delete('wc_product_price');
    }

    public function add_retainer($retainerdata){
        $report = $this->db->insert('wc_retainer', $retainerdata);
        if ($report) {
            return true;
        } else {
            return false;
        }
    }
    public function update_retainer($retainerdata, $retainer_id){
        $this->db->where('retainer_id', $retainer_id);
        $report = $this->db->update('wc_retainer', $retainerdata);
        if ($report) {
            return true;
        } else {
            return false;
        }
    }
    public function delete_retainer($retainer_id){
        $array = array('retainer_id' => $retainer_id);
        $this->db->where($array); 
        $result=$this->db->delete('wc_retainer');
    }
    
    public function update_notifications($settingdata,$user_id){
        $this->db->where('user_id', $user_id);
        $report = $this->db->update('wc_users', $settingdata);
    //print_r($settingdata);
    if($settingdata['admin_mail_notifications']==1)
    {
        $status="Activated";
    }
    else{
        $status="DeActivated";
    }
      echo "Email Notifications susessfully ".$status;
    }


    public function update_discount($settingdata,$client_id){
        $this->db->where('client_id', $client_id);
        $report = $this->db->update('wc_users', $settingdata);
        //print_r($settingdata);
        if($settingdata['add_discount']==1){
            $status="Activated";
        }
        else{
            $status="DeActivated";
        }
        echo "Discount Option is ".$status;
    }
    public function update_extra_charges($settingdata,$client_id){
        $this->db->where('client_id', $client_id);
        $report = $this->db->update('wc_users', $settingdata);
        //print_r($settingdata);
        if($settingdata['add_extra_charges']==1){
            $status="Activated";
        }
        else{
            $status="DeActivated";
        }
        echo "Extra Charges Option is ".$status;
    }
}