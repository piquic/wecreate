<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Login_model extends CI_Model
{
    public function __construct()
    {
        $this->load->database();
        $this->load->library('session');
    }

public function islogin($data){  
    /*$query=$this->db->get_where('wc_users',array('user_email'=>$data['user_email'],'user_password'=>$data['user_password'],'user_status'=>'Enable'));*/  
    /*$query=$this->db->get_where('wc_users',array('user_email'=>$data['user_email'],'user_password'=>$data['user_password'])); */
    $query=$this->db->get_where('wc_users',array('user_email'=>$data['user_email'],'user_password'=>$data['user_password'],'deleted'=>'0')); 
    $query->num_rows();  
    if ($query->num_rows() == 1) {
            return $query->result();
        } else {

            return 0;
        }
}  



















public function loginfront($username, $password){
        $this->db->select('*');
        $this->db->from('wc_users');
     //   $this->db->where('u_rolecode', 1);
        $this->db->where('user_email', $username);
        $this->db->where('user_password', $password);
        $this->db->limit(1);

        $query = $this->db->get();

//echo $query->num_rows(); exit();
        if ($query->num_rows() == 1) {
            return $query->result();
        } else {
            return false;
        }
}
    


public function get_page($id="")
        {
            

            

        $this->db->select('*');
        $this->db->from('wc_page_content');
        $this->db->where('id', $id);


        $query = $this->db->get();

        //echo $query->num_rows(); exit();
        if ($query->num_rows() == 1) {
        return $query->result_array();
        } else {
        return false;
        }

        }

        public function rest_password_update($user_id,$user_password)
        {   
            $data=array('user_password'=>$user_password);
            $this->db->set('user_password','user_password',false);
            $this->db->where('user_id', $user_id);
            $report = $this->db->update('wc_users', $data);
            if ($report) {
                return true;
            } else {
                return false;
            }
        }

    public function upload_img_insert($data)
    {
        $this->db->insert('wc_upload_img', $data);
    }
    
    
    
    
    public function upload_img_update($userdata, $upimg_id)
    {
        $this->db->where('upimg_id', $upimg_id);
        $report = $this->db->update('wc_upload_img', $userdata);
        if ($report) {
            return true;
        } else {
            return false;
        }
    }


    public function upload_images_insert($data)
    {
        $this->db->insert('wc_upload_images', $data);
    }
    
    
    
    
    public function upload_images_update($userdata, $id)
    {
        $this->db->where('id', $id);
        $report = $this->db->update('wc_upload_images', $userdata);
        if ($report) {
            return true;
        } else {
            return false;
        }
    }
        
    public function add_user($data)
    {
        $this->db->insert('wc_users', $data);
    }
    











    
    public function getOwnerlist()
    {
        $sql = 'select * from nc_user where usertype="2"';
        $query = $this->db->query($sql);
        return $query->num_rows();
    }
    
    public function getCustomerlist()
    {
        $sql = 'select * from customer_master';
        $query = $this->db->query($sql);
        return $query->num_rows();
    }   
    
    public function getServicelist()
    {
        $sql = 'select * from service_master where status="Active"';
        $query = $this->db->query($sql);
        return $query->num_rows();
    }
    
    public function getEventlist()
    {
        $sql = 'select * from event_master';
        $query = $this->db->query($sql);
        return $query->num_rows();
    }
    
    
    
    public function sgetCustomerlist($userid)
    {
        $sql = "select * from customer_master where owner_id='$userid'";
        $query = $this->db->query($sql);
        return $query->num_rows();
    }   
    
    public function sgetServicelist($userid)
    {
        $sql = "select * from service_master where owner_id='$userid' and status='Active'";
        $query = $this->db->query($sql);
        return $query->num_rows();
    }
    
    public function sgetEventlist($userid)
    {
        $sql = "select * from event_master where owner_id='$userid'";
        $query = $this->db->query($sql);
        return $query->num_rows();
    }
        public function sgetBookinglist($userid)
    {
        $sql = "select * from manual_booking where owner_id='$userid'";
        $query = $this->db->query($sql);
        return $query->num_rows();
    }
    
    
    
    
    
    
    
    
    
    
    
    
    public function getBookingweekly($date)
    {
    
         $session_data = $this->session->userdata('logged_in');
         $rolecode = $session_data['rolecode'];          
        
        if($rolecode=='2' || $rolecode=='3')
        {
        $owner_id=$session_data['id'];
        $sql = "select * from manual_booking  WHERE (start_date) = '".$date."' AND  owner_id = '".$owner_id."' ";
        } 
        else 
        {
        $sql = "select * from manual_booking  WHERE (start_date) = '".$date."'  ";
        }
        $query = $this->db->query($sql);
        return $query->num_rows();
    }

    
    public function getBookingweek($day)
    {
    
        $year=date("Y");
        $month=date("m");
         $session_data = $this->session->userdata('logged_in');
         $rolecode = $session_data['rolecode'];          
        
        if($rolecode=='2' || $rolecode=='3')
        {
        $owner_id=$session_data['id'];
        
        $sql = 'select * from manual_booking  WHERE YEAR(start_date) = '.$year.' AND MONTH(start_date) = '.$month.' AND DAY(start_date) = '.$day.'  AND  owner_id = '.$owner_id;
        } 
        else 
        {
        
        $sql = 'select * from manual_booking  WHERE YEAR(start_date) = '.$year.' AND MONTH(start_date) = '.$month.' AND DAY(start_date) = '.$day.'';
        
        }
        $query = $this->db->query($sql);
        return $query->num_rows();
    }
    
        public function getBookingmonth($year,$month)
    {
    
         $session_data = $this->session->userdata('logged_in');
         $rolecode = $session_data['rolecode'];          
        
        if($rolecode=='2' || $rolecode=='3')
        {
        $owner_id=$session_data['id'];
        
        $sql = 'select * from manual_booking  WHERE YEAR(start_date) = '.$year.' AND MONTH(start_date) = '.$month.'  AND  owner_id = '.$owner_id;
        } 
        else 
        {
         $sql = 'select * from manual_booking  WHERE YEAR(start_date) = '.$year.' AND MONTH(start_date) = '.$month.' ';
        
        }
        $query = $this->db->query($sql);
        return $query->num_rows();
    }
    
    
    public function getBookingyear($year)
    {
    
         $session_data = $this->session->userdata('logged_in');
         $rolecode = $session_data['rolecode'];          
        
        if($rolecode=='2' || $rolecode=='3')
        {
        $owner_id=$session_data['id'];
        
        $sql = 'select * from manual_booking  WHERE YEAR(start_date) = '.$year.'   AND  owner_id = '.$owner_id;
        } 
        else 
        {
        
        $sql = 'select * from manual_booking  WHERE YEAR(start_date) = '.$year.' ';
        
        }
        $query = $this->db->query($sql);
        return $query->num_rows();
    }
    


    public function getBookinglist($owner_id="")
    {
    
        $session_data = $this->session->userdata('logged_in');
        $rolecode = $session_data['rolecode'];          
        
        if($rolecode=='2' || $rolecode=='3')
        {
        $owner_id=$session_data['id'];
        
        $sql = 'select * from manual_booking  WHERE status="Pending"   AND  owner_id = "'.$owner_id.'" order by manual_booking_id desc  limit 7 ';
        } 
        else
        {
        $sql = 'select * from manual_booking WHERE status="Pending" order by manual_booking_id desc   limit 7';
        }
        
        $query = $this->db->query($sql);
        return $query->result();
    }
    

    public function getBookinglistwithservice($owner_id="")
    {
    
        $session_data = $this->session->userdata('logged_in');
        $rolecode = $session_data['rolecode'];          
        
        if($rolecode=='2' || $rolecode=='3')
        {
        $owner_id=$session_data['id'];
        
        $sql = 'select manual_booking.manual_booking_id,manual_booking.unique_booking_id,manual_booking.start_date,manual_booking.status,service_master.service_title from manual_booking inner join service_master on manual_booking.service_id=service_master.service_id  WHERE manual_booking.status="Confirmed"   AND  manual_booking.owner_id = "'.$owner_id.'" order by manual_booking_id desc  limit 7 ';
        } 
        else
        {
        $sql = 'select manual_booking.manual_booking_id,manual_booking.unique_booking_id,manual_booking.start_date,manual_booking.status,service_master.service_title from manual_booking inner join service_master on manual_booking.service_id=service_master.service_id WHERE manual_booking.status="Confirmed"  order by manual_booking_id desc   limit 7';
        }
        
        $query = $this->db->query($sql);
        return $query->result();
    }
    

    public function getTodaysalecount($userid)
    {
         $today_date=date("Y-m-d");
         if($userid!='')
         {
            
            $sql1='select sum(service_price) as total_price from manual_booking inner join service_master on manual_booking.service_id=service_master.service_id WHERE manual_booking.confirmed_date="'.$today_date.'" AND manual_booking.owner_id="'.$userid.'" and manual_booking.status="Confirmed" and manual_booking.unique_id="" ';
            $query1= $this->db->query($sql1)->result_array();
            if($query1['0']['total_price']!='')
            {
                $service_price=$query1['0']['total_price'];
            }else {
                 $service_price=0;
            }
            $sql2='select service_price as course_price from manual_booking inner join service_master on manual_booking.service_id=service_master.service_id WHERE manual_booking.confirmed_date="'.$today_date.'" AND manual_booking.owner_id="'.$userid.'" and manual_booking.status="Confirmed" and manual_booking.unique_id!="" group by unique_id';
           //$sql = 'select sum(price) as total_price from manual_booking inner join service_master on manual_booking.service_id=service_master.service_id WHERE manual_booking.created_date="'.$today_date.'" AND manual_booking.owner_id="'.$userid.'" ';
            $query2 = $this->db->query($sql2)->result();
            $course_price_array=array();
            if($query2){
                foreach($query2 as $result2){
                    $course_price_array[]=$result2->course_price;
                }
            }
            $course_price=array_sum($course_price_array);
            $total_price=$service_price+$course_price;
        
        }
        else
        {
            //$sql='select sum(service_price) as total_price from manual_booking inner join service_master on manual_booking.service_id=service_master.service_id WHERE manual_booking.confirmed_date="'.$today_date.'" and manual_booking.status="Confirmed" ';
           //$sql = 'select sum(price) as total_price from manual_booking inner join service_master on manual_booking.service_id=service_master.service_id WHERE manual_booking.created_date="'.$today_date.'" ';
        
           $sql1='select sum(service_price) as total_price from manual_booking inner join service_master on manual_booking.service_id=service_master.service_id WHERE manual_booking.confirmed_date="'.$today_date.'" and manual_booking.status="Confirmed" and manual_booking.unique_id="" ';
            $query1= $this->db->query($sql1)->result_array();
            if($query1['0']['total_price']!='')
            {
                $service_price=$query1['0']['total_price'];
            }
            $sql2='select service_price as course_price from manual_booking inner join service_master on manual_booking.service_id=service_master.service_id WHERE manual_booking.confirmed_date="'.$today_date.'" and manual_booking.status="Confirmed" and manual_booking.unique_id!="" group by unique_id';
           //$sql = 'select sum(price) as total_price from manual_booking inner join service_master on manual_booking.service_id=service_master.service_id WHERE manual_booking.created_date="'.$today_date.'" AND manual_booking.owner_id="'.$userid.'" ';
            $query2 = $this->db->query($sql2)->result();
            $course_price_array=array();
            if($query2){
                foreach($query2 as $result2){
                    $course_price_array[]=$result2->course_price;
                }
            }
            $course_price=array_sum($course_price_array);
            $total_price=$service_price+$course_price;
        
        
        }
        
        //echo $sql; exit;
        /*  $query = $this->db->query($sql)->result_array();
         
         if($query['0']['total_price']!='')
         {
         $total_price=$query['0']['total_price'];
         }
         else
         {
         $total_price="0";
         } */
         return  $total_price;
        
        
    }
    
	
	
	
}

?>