-- phpMyAdmin SQL Dump
-- version 4.8.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: May 26, 2020 at 04:41 AM
-- Server version: 10.1.37-MariaDB
-- PHP Version: 5.6.40

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `piquic_collab`
--

-- --------------------------------------------------------

--
-- Table structure for table `ci_cookies`
--

CREATE TABLE `ci_cookies` (
  `id` int(11) NOT NULL,
  `cookie_id` varchar(255) DEFAULT NULL,
  `netid` varchar(255) DEFAULT NULL,
  `ip_address` varchar(255) DEFAULT NULL,
  `user_agent` varchar(255) DEFAULT NULL,
  `orig_page_requested` varchar(120) DEFAULT NULL,
  `php_session_id` varchar(40) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `ci_sessions`
--

CREATE TABLE `ci_sessions` (
  `session_id` varchar(40) NOT NULL DEFAULT '0',
  `ip_address` varchar(45) NOT NULL DEFAULT '0',
  `user_agent` varchar(120) NOT NULL,
  `last_activity` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `user_data` text NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `ci_sessions`
--

INSERT INTO `ci_sessions` (`session_id`, `ip_address`, `user_agent`, `last_activity`, `user_data`) VALUES
('949681f205284532aceab09b2bfd1f33', '122.161.68.157', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10.14; rv:76.0) Gecko/20100101 Firefox/76.0', 1590410629, 'a:2:{s:9:\"user_data\";s:0:\"\";s:15:\"front_logged_in\";a:6:{s:2:\"id\";s:1:\"7\";s:7:\"user_id\";s:1:\"7\";s:12:\"user_type_id\";s:1:\"4\";s:12:\"brand_access\";s:7:\",1,5,6,\";s:13:\"client_access\";s:1:\"1\";s:9:\"user_name\";s:26:\"india harmony demo account\";}}'),
('3d96ec975b9de52b1eeb2184f8d37db9', '103.100.37.119', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:76.0) Gecko/20100101 Firefox/76.0', 1590408364, 'a:2:{s:9:\"user_data\";s:0:\"\";s:15:\"front_logged_in\";a:6:{s:2:\"id\";s:1:\"5\";s:7:\"user_id\";s:1:\"5\";s:12:\"user_type_id\";s:1:\"3\";s:12:\"brand_access\";s:1:\"0\";s:13:\"client_access\";s:1:\"2\";s:9:\"user_name\";s:19:\"pm.zara@inintra.com\";}}'),
('f954ef318baa2cef8844be3e3e12ebd9', '122.161.68.157', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_2) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/83.0.4103.61 Safari/537.36', 1590412599, 'a:3:{s:9:\"user_data\";s:0:\"\";s:15:\"front_logged_in\";a:6:{s:2:\"id\";s:2:\"11\";s:7:\"user_id\";s:2:\"11\";s:12:\"user_type_id\";s:1:\"2\";s:12:\"brand_access\";s:1:\"0\";s:13:\"client_access\";s:1:\"1\";s:9:\"user_name\";s:25:\"serge piquic admin marico\";}s:9:\"logged_in\";a:8:{s:2:\"id\";s:1:\"1\";s:7:\"user_id\";s:1:\"1\";s:5:\"email\";s:15:\"admin@gmail.com\";s:8:\"username\";s:15:\"admin@gmail.com\";s:9:\"firstname\";s:5:\"Admin\";s:8:\"rolecode\";s:1:\"0\";s:9:\"user_type\";s:1:\"0\";s:5:\"roles\";s:1:\"0\";}}'),
('088ace8aa66345b3b18e7a2c9dd78cf5', '27.4.245.163', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/79.0.3945.88 Safari/537.36', 1590425783, 'a:2:{s:9:\"user_data\";s:0:\"\";s:15:\"front_logged_in\";a:6:{s:2:\"id\";s:2:\"10\";s:7:\"user_id\";s:2:\"10\";s:12:\"user_type_id\";s:1:\"3\";s:12:\"brand_access\";s:1:\"0\";s:13:\"client_access\";s:1:\"1\";s:9:\"user_name\";s:17:\"Shikha- PM Marico\";}}'),
('6402649f6ccc580a3c55420729efce96', '13.64.93.136', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/63.0.3239.84 Safari/537.36', 1590425453, ''),
('fe3b6a4717b2e74b234b776fc3edfa4b', '54.184.197.192', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/72.0.3626.109 Safari/537.36', 1590425528, '');

-- --------------------------------------------------------

--
-- Table structure for table `login_admin`
--

CREATE TABLE `login_admin` (
  `user_id` int(11) NOT NULL,
  `usertype_id` int(11) NOT NULL,
  `role_id` int(11) NOT NULL,
  `first_name` varchar(50) NOT NULL,
  `email` varchar(100) NOT NULL,
  `username` varchar(50) NOT NULL,
  `password` text NOT NULL,
  `usertype` int(11) NOT NULL,
  `mobile` bigint(20) NOT NULL,
  `phone` varchar(255) NOT NULL,
  `address` text NOT NULL,
  `zip` varchar(255) NOT NULL,
  `country` varchar(100) NOT NULL,
  `state` varchar(50) NOT NULL,
  `city` varchar(50) NOT NULL,
  `status` enum('Active','Inactive','Cancel') NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `login_admin`
--

INSERT INTO `login_admin` (`user_id`, `usertype_id`, `role_id`, `first_name`, `email`, `username`, `password`, `usertype`, `mobile`, `phone`, `address`, `zip`, `country`, `state`, `city`, `status`) VALUES
(1, 0, 0, 'Admin', 'admin@gmail.com', 'admin@gmail.com', 'd033e22ae348aeb5660fc2140aec35850c4da997', 1, 0, '66666666666', '', '', '', '', '', 'Active'),
(2, 0, 0, 'Manager', 'koko@gmail.com', 'koko@gmail.com', 'd033e22ae348aeb5660fc2140aec35850c4da997', 2, 0, '', '', '', '', '', '', 'Active');

-- --------------------------------------------------------

--
-- Table structure for table `wc_brands`
--

CREATE TABLE `wc_brands` (
  `brand_id` int(11) NOT NULL,
  `client_id` int(11) NOT NULL,
  `brand_name` varchar(255) NOT NULL,
  `brand_email` varchar(200) NOT NULL,
  `status` enum('Active','Inactive') NOT NULL,
  `deleted` enum('0','1') NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `wc_brands`
--

INSERT INTO `wc_brands` (`brand_id`, `client_id`, `brand_name`, `brand_email`, `status`, `deleted`) VALUES
(1, 1, 'meri2', 'meri@gmail.com', 'Active', '0'),
(2, 2, 'zara classic', 'zaraclassic@inintra.com', 'Active', '0'),
(3, 3, 'Brand 1', 'serge@mokshaproductions.in', 'Active', '0'),
(4, 3, 'brand 2', 'serge.gianchandani@gmail.com', 'Active', '0'),
(5, 1, 'Saffola', 'ragini.sabnavis@marico.com', 'Active', '0'),
(6, 1, 'Veggie Clean', 'neha.dhanuka@marico.com', 'Active', '0'),
(7, 2, 'zara vingate', 'infodddd@inintra.com', 'Active', '0'),
(8, 4, 'TestBrand', 'testbrand@gmail.com', 'Active', '0'),
(9, 1, 'Hair and Care', 'vivian.gonsalves@marico.com', 'Active', '0'),
(10, 1, 'Marico travel', 'temp2@marico.com', 'Active', '0'),
(11, 1, 'House Protect', 'temp3@marico.com', 'Active', '0'),
(12, 1, 'Mediker', 'priyanka.save@marico.com', 'Active', '0'),
(13, 1, 'Revive', 'temp4@marico.com', 'Active', '0'),
(14, 1, 'Nihar', 'seher.contractor@marico.com', 'Active', '0');

-- --------------------------------------------------------

--
-- Table structure for table `wc_brief`
--

CREATE TABLE `wc_brief` (
  `brief_id` int(11) NOT NULL,
  `wekan_unique_id` text NOT NULL,
  `brief_title` text NOT NULL,
  `brief_group_id` varchar(50) NOT NULL,
  `brief_doc` text NOT NULL,
  `total_price` varchar(255) NOT NULL,
  `brief_due_date` datetime NOT NULL,
  `added_by_user_id` int(11) NOT NULL,
  `notes` text NOT NULL,
  `brand_id` text NOT NULL,
  `last_updated_on` datetime NOT NULL,
  `rejected_reasons` text NOT NULL,
  `rejected_by` int(11) NOT NULL,
  `rejected_on` datetime NOT NULL,
  `status` varchar(255) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `wc_brief`
--

INSERT INTO `wc_brief` (`brief_id`, `wekan_unique_id`, `brief_title`, `brief_group_id`, `brief_doc`, `total_price`, `brief_due_date`, `added_by_user_id`, `notes`, `brand_id`, `last_updated_on`, `rejected_reasons`, `rejected_by`, `rejected_on`, `status`) VALUES
(4, 'xnueYvmmbuYEmDTNq', 'new', '31589544201', '31589544201.zip', '12960', '2020-05-15 14:03:00', 3, '', '1', '2020-05-15 14:03:32', '', 0, '0000-00-00 00:00:00', '6'),
(5, 'W9uFdLZXJZSwdJQ3w', 'Demo1', '31589545609', '31589545609.zip', '23520', '2020-05-15 17:58:00', 3, '', '1', '2020-05-15 14:27:29', '', 0, '0000-00-00 00:00:00', '7'),
(6, 'hBhjCWE4ScyRwJMbM', 'Demo 2', '31589546709', '31589546709.zip', '37440', '2020-05-15 14:45:00', 3, '', '1', '2020-05-15 14:45:22', '', 0, '0000-00-00 00:00:00', '1'),
(7, 'bjezEW7MfRRSMw29H', 'Demo 3', '31589651866', '31589651866.zip', '9180', '2020-05-16 19:58:00', 3, '', '1', '2020-05-16 19:58:03', '', 0, '0000-00-00 00:00:00', '1'),
(12, '5pHRBPTnnayWL2Yqz', 'Project  Brand 1', '81589695140', '81589695140.zip', '32160', '2020-05-17 08:03:00', 8, '', '3', '2020-05-17 07:59:37', '', 0, '0000-00-00 00:00:00', '6'),
(15, 'sBpv8jstLNa2YT3NG', 'project 2 brand 1', '81589696067', '81589696067.zip', '21600', '2020-05-17 08:16:00', 8, '', '3', '2020-05-17 08:14:41', '', 0, '0000-00-00 00:00:00', '6'),
(17, 'Q9stMk8HY9XNcjiE4', 'project 3 brand 1', '81589696520', '81589696520.zip', '25920', '2020-05-17 08:22:00', 8, '', '3', '2020-05-17 08:22:24', '', 0, '0000-00-00 00:00:00', '6'),
(18, 'axbH82XCsfLLnS57Z', 'prooject 6', '81589700644', '81589701711.zip', '48000', '2020-05-17 09:49:00', 8, '', '3', '2020-05-17 09:48:34', '', 0, '0000-00-00 00:00:00', '7'),
(21, 'kgp8wi93fqEzfoq3r', 'Project test3', '71589719041', '71589719041.zip', '64800', '2020-05-17 14:39:00', 7, '', '1', '2020-05-17 14:38:22', '', 0, '0000-00-00 00:00:00', '7'),
(23, 'w4fKAzcEW7XHi54GY', 'Moksha HR Team Temaplate Creation', '141589784687', '141589784687.zip', '9600', '2020-05-18 09:01:00', 14, '', '1', '2020-05-18 08:52:37', '', 0, '0000-00-00 00:00:00', '7'),
(24, 'j4eGKMssEiX2n8dXf', 'Test', '31589861742', '31589861875.zip', '13920', '2020-05-19 06:18:00', 3, '', '1', '2020-05-19 06:18:05', '', 0, '0000-00-00 00:00:00', '7'),
(25, 'pwLbjCHx4EsDBSW49', 'Test2', '31589862126', '31589862239.zip', '28800', '2020-05-19 06:24:00', 3, '', '1', '2020-05-19 06:24:02', '', 0, '0000-00-00 00:00:00', '7'),
(29, 'dgWRv6hWGtzNiWhcP', 'Test saffola', '71589867975', '71589867975.zip', '4320', '2020-05-20 05:50:00', 7, '', '5', '2020-05-19 08:00:10', '', 0, '0000-00-00 00:00:00', '6'),
(30, 'cPreSJo8i9xS4Jt8u', 'test7', '71589869017', '71589869017.zip', '28800', '2020-05-21 11:42:00', 7, '', '1', '2020-05-19 08:17:19', '', 0, '0000-00-00 00:00:00', '1'),
(31, 'mWHtrur5r49aGAYmP', 'Saffola fitify Web Banner Design Amazon', '141589883744', '141589884088.zip', '9600', '2020-05-22 14:00:00', 14, '', '1', '2020-05-19 12:28:10', '', 0, '0000-00-00 00:00:00', '7'),
(32, 'WaNfkrrEBfqt65Q53', 'Test 123', '161589946318', '161589946318.zip', '4320', '2020-05-20 07:14:00', 16, '', '5', '2020-05-20 05:46:12', '', 0, '0000-00-00 00:00:00', '6'),
(33, 'aTNLxnQGKaGQdmSmn', 'Test 222', '161589946623', '161589946623.zip', '19200', '2020-05-20 05:52:00', 16, '', '5', '2020-05-20 05:51:32', '', 0, '0000-00-00 00:00:00', '1'),
(34, 'a6HxezvZaDk26g3BL', 'test', '31589946748', '31589946748.zip', '27600', '2020-05-20 05:52:00', 3, '', '1', '2020-05-20 05:52:54', '', 0, '0000-00-00 00:00:00', '6'),
(35, 'SkezH36YZXSNgP7PE', 'Test333', '161589947195', '161589947195.zip', '', '2020-05-20 06:00:00', 16, '', '1', '2020-05-20 06:00:08', '', 0, '0000-00-00 00:00:00', '6'),
(36, 'Y4MfEmQ4e3jAttFzv', 'Test 444', '161589948610', '161589948610.zip', '9600', '2020-05-20 06:47:00', 16, '', '1', '2020-05-20 06:23:43', '', 0, '0000-00-00 00:00:00', '6'),
(39, 'dmwB3FTyj76ra2Ntn', 'Test-notes', '31589951149', '31589951149.zip', '11040', '2020-05-20 07:06:00', 3, '', '1', '2020-05-20 07:06:19', '', 0, '0000-00-00 00:00:00', '6'),
(41, '7uroGZzFebi9zmeeE', 'gfbvn', '221590034188', '221590034188.zip', '24060', '2020-05-21 06:11:00', 22, '', '8', '2020-05-21 06:10:09', '', 0, '0000-00-00 00:00:00', '6'),
(43, 'kNnAZdWk4WnpD8CEY', 'Test mail', '221590049945', '221590049945.zip', '', '0000-00-00 00:00:00', 22, '', '8', '2020-05-21 10:33:10', 'Reject for test', 25, '2020-05-21 10:52:14', '4'),
(44, 'dWi6p5EFeyamzKASo', 'Test_mail', '261590050813', '261590050813.zip', '4500', '2020-05-21 10:48:00', 26, '', '8', '2020-05-21 10:47:16', '', 0, '0000-00-00 00:00:00', '7'),
(45, 'fTSHabbCxSXXdC7qx', 'Test Reject', '261590051239', '261590051239.zip', '', '0000-00-00 00:00:00', 26, '', '8', '2020-05-21 10:54:39', 'Reject for test', 25, '2020-05-21 10:55:36', '4'),
(46, 'XR8WngGfYMaeFf9wa', 'Set wet Digital Creative For vocal for local', '141590059324', '141590059654.zip', '', '2020-05-23 15:30:00', 14, '', '1', '2020-05-21 13:14:56', '', 0, '0000-00-00 00:00:00', '7'),
(47, 'ZYfRo5KrLe2HzbbcT', 'Test_22-05-20', '261590118633', '261590118633.zip', '27600', '2020-05-22 05:39:00', 26, '', '8', '2020-05-22 05:37:53', '', 0, '0000-00-00 00:00:00', '6'),
(49, 'RKmJypSMf7MGZ43kp', 'Travel and House Protect Brand Stores', '301590142768', '301590142768.zip', '18000', '2020-05-22 12:25:00', 30, '', '10', '2020-05-22 12:21:08', '', 0, '0000-00-00 00:00:00', '1'),
(50, 'BbHPaDNwKCwkR964H', 'Revive Laundry Sanitizer Refill Pouch 400ml', '311590147371', '311590147371.zip', '', '0000-00-00 00:00:00', 31, '', '13', '2020-05-22 13:40:48', 'As discussed please change the timelines wont be possible to send it by 12:00PM.', 10, '2020-05-22 14:26:11', '4'),
(51, 'to7GnnHkCdF7TedWE', 'Revive Laundry Sanitizer Refill Pouch 400ml', '311590150975', '311590150975.zip', '960', '2020-05-22 16:16:00', 31, '', '13', '2020-05-22 14:37:33', '', 0, '0000-00-00 00:00:00', '6'),
(52, 'a6zAuKJFJRFjekNZQ', 'test', '61590317433', '61590317433.zip', '4320', '2020-05-24 12:51:00', 6, '', '2', '2020-05-24 12:50:41', '', 0, '0000-00-00 00:00:00', '6'),
(53, 'TDWZzpvoMTTCXh7Gk', 'ssss', '61590384194', '61590384194.zip', '4320', '2020-05-25 10:50:00', 6, '', '2', '2020-05-25 07:23:21', '', 0, '0000-00-00 00:00:00', '6'),
(54, 'cKutKESJ3x5ZRCuYg', 'test667', '71590391069', '71590391069.zip', '11040', '2020-05-25 09:18:00', 7, '', '1', '2020-05-25 09:18:12', '', 0, '0000-00-00 00:00:00', '1'),
(55, 'zvzduL2Xb5rgytW3s', 'zdnv ', '221590397166', '221590397166.zip', '4320', '2020-05-25 11:00:00', 22, '', '8', '2020-05-25 10:59:35', '', 0, '0000-00-00 00:00:00', '6');

-- --------------------------------------------------------

--
-- Table structure for table `wc_briefstatus`
--

CREATE TABLE `wc_briefstatus` (
  `status_id` int(10) NOT NULL,
  `status_name` varchar(40) NOT NULL,
  `status_value` int(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `wc_briefstatus`
--

INSERT INTO `wc_briefstatus` (`status_id`, `status_name`, `status_value`) VALUES
(1, 'Brief in review', 0),
(2, 'Work in progress', 1),
(3, 'Work completd', 2),
(4, 'Revision work', 3),
(5, 'Brief rejected', 4),
(6, 'Feedback pending', 5),
(7, 'Proofing pending', 6),
(8, 'Feedback sent', 7);

-- --------------------------------------------------------

--
-- Table structure for table `wc_brief_product_list`
--

CREATE TABLE `wc_brief_product_list` (
  `brief_product_id` int(11) NOT NULL,
  `brief_id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `product_quty` int(11) NOT NULL,
  `subtotal_price` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `wc_brief_product_list`
--

INSERT INTO `wc_brief_product_list` (`brief_product_id`, `brief_id`, `product_id`, `product_quty`, `subtotal_price`) VALUES
(1, 1, 1, 2, '8640'),
(2, 3, 1, 1, '4320'),
(3, 2, 1, 1, '9600'),
(5, 2, 2, 1, '9600'),
(6, 3, 2, 1, '9600'),
(7, 3, 3, 1, '180'),
(8, 4, 1, 3, '12960'),
(9, 5, 1, 1, '4320'),
(10, 5, 2, 2, '19200'),
(12, 6, 1, 2, '8640'),
(13, 6, 2, 3, '28800'),
(14, 6, 0, 0, '0'),
(15, 7, 1, 2, '8640'),
(16, 7, 3, 3, '540'),
(17, 8, 1, 1, '4320'),
(18, 11, 1, 1, '4320'),
(19, 11, 2, 1, '9600'),
(20, 12, 1, 3, '12960'),
(21, 12, 2, 2, '19200'),
(22, 13, 1, 1, '4320'),
(23, 15, 1, 5, '21600'),
(24, 17, 1, 6, '25920'),
(25, 16, 0, 0, ''),
(26, 18, 2, 5, '48000'),
(27, 8, 16, 3, '54000'),
(28, 8, 2, 2, '19200'),
(29, 8, 21, 1, '720'),
(31, 19, 2, 0, ''),
(32, 20, 3, 1, '180'),
(33, 21, 2, 3, '28800'),
(34, 21, 16, 2, '36000'),
(35, 22, 1, 1, '4320'),
(36, 22, 1, 0, ''),
(37, 23, 2, 1, '9600'),
(38, 24, 2, 1, '9600'),
(39, 24, 1, 1, '4320'),
(40, 25, 2, 3, '28800'),
(41, 26, 1, 1, '4320'),
(42, 31, 2, 1, '9600'),
(43, 29, 1, 1, '4320'),
(44, 33, 2, 2, '19200'),
(45, 33, 2, 0, ''),
(46, 34, 2, 1, '9600'),
(47, 34, 16, 1, '18000'),
(48, 34, 16, 0, ''),
(49, 35, 0, 0, ''),
(50, 37, 1, 1, '4320'),
(51, 36, 2, 1, '9600'),
(52, 38, 1, 1, '4320'),
(53, 39, 2, 1, '9600'),
(55, 32, 1, 1, '4320'),
(56, 40, 1, 1, '4320'),
(57, 41, 1, 1, '4320'),
(58, 41, 2, 2, '19200'),
(59, 41, 3, 3, '540'),
(60, 42, 1, 1, '4320'),
(61, 44, 1, 1, '4320'),
(62, 44, 3, 1, '180'),
(63, 30, 2, 3, '28800'),
(64, 46, 2, 0, ''),
(65, 47, 2, 1, '9600'),
(66, 47, 16, 1, '18000'),
(67, 48, 1, 1, '4320'),
(69, 49, 18, 1, '18000'),
(70, 49, 0, 0, ''),
(71, 51, 3, 2, '960'),
(72, 51, 2, 0, ''),
(73, 52, 1, 1, '4320'),
(74, 39, 3, 3, '1440'),
(75, 54, 2, 1, '9600'),
(76, 54, 3, 3, '1440'),
(77, 53, 1, 1, '4320'),
(78, 55, 1, 1, '4320');

-- --------------------------------------------------------

--
-- Table structure for table `wc_brief_records`
--

CREATE TABLE `wc_brief_records` (
  `brief_record_id` int(11) NOT NULL,
  `brief_id` int(11) NOT NULL,
  `brief_group_id` varchar(50) NOT NULL,
  `brief_doc` text NOT NULL,
  `last_updated_on` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `wc_brief_records`
--

INSERT INTO `wc_brief_records` (`brief_record_id`, `brief_id`, `brief_group_id`, `brief_doc`, `last_updated_on`) VALUES
(5, 4, '31589544201', '24-dabur-chyawanprash.png', '2020-05-15 14:03:32'),
(6, 5, '31589545609', 'jsdliugkbs.docx', '2020-05-15 14:27:29'),
(7, 6, '31589546709', 'jsdliugkbs.docx', '2020-05-15 14:45:22'),
(8, 7, '31589651866', 'pexels-photo-414612.jpeg', '2020-05-16 19:58:04'),
(13, 12, '81589695140', 'WhatsApp Image 2020-05-08 at 21.50.20.jpeg', '2020-05-17 07:59:38'),
(14, 12, '81589695140', 'veggie clean gif.mp4', '2020-05-17 07:59:38'),
(17, 15, '81589696067', 'WhatsApp Image 2020-05-08 at 21.50.21.jpeg', '2020-05-17 08:14:42'),
(19, 17, '81589696520', 'brief.docx', '2020-05-17 08:22:24'),
(20, 18, '81589700644', 'brief.docx', '2020-05-17 09:31:23'),
(21, 18, '81589700644', 'implementation.docx', '2020-05-17 09:31:23'),
(22, 18, '81589701711', 'brief.docx', '2020-05-17 09:48:34'),
(25, 21, '71589719041', 'Copy of Ecom listing details_body lotion_hand cream (002.xlsx', '2020-05-17 14:38:23'),
(26, 21, '71589719041', 'brief.docx', '2020-05-17 14:38:23'),
(28, 23, '141589784687', '4 day workout plan Akash.docx', '2020-05-18 08:52:38'),
(29, 24, '31589861742', 'Image_Upload_Guidelines.pdf', '2020-05-19 06:16:08'),
(30, 24, '31589861875', 'Image_Upload_Guidelines.pdf', '2020-05-19 06:18:06'),
(31, 25, '31589862126', 'pexels-photo-57905.jpeg', '2020-05-19 06:22:27'),
(32, 25, '31589862239', 'photo-1500100586562-f75ff6540087.png', '2020-05-19 06:24:02'),
(36, 29, '71589867975', '12121.jpg', '2020-05-19 08:00:11'),
(37, 30, '71589869017', 'hh.docx', '2020-05-19 08:17:20'),
(38, 31, '141589883744', '4 day workout plan Akash.docx', '2020-05-19 12:23:42'),
(39, 31, '141589884088', '4 day workout plan Akash.docx', '2020-05-19 12:28:10'),
(40, 32, '161589946318', '12121.jpg', '2020-05-20 05:46:13'),
(41, 33, '161589946623', '7788.jpeg', '2020-05-20 05:51:32'),
(42, 34, '31589946748', 'Image_Upload_Guidelines.pdf', '2020-05-20 05:52:55'),
(43, 35, '161589947195', '4466.jpeg', '2020-05-20 06:00:08'),
(44, 36, '161589948610', '121.jpeg', '2020-05-20 06:23:44'),
(47, 39, '31589951149', 'Image_Upload_Guidelines.pdf', '2020-05-20 07:06:19'),
(49, 41, '221590034188', 'jsdliugkbs.docx', '2020-05-21 06:10:10'),
(51, 43, '221590049945', 'Image_Upload_Guidelines.pdf', '2020-05-21 10:33:15'),
(52, 44, '261590050813', 'Image_Upload_Guidelines.pdf', '2020-05-21 10:48:01'),
(53, 45, '261590051239', 'Image_Upload_Guidelines.pdf', '2020-05-21 10:55:23'),
(54, 46, '141590059324', '4 day workout plan Akash.docx', '2020-05-21 13:10:14'),
(55, 46, '141590059654', '4 day workout plan Akash.docx', '2020-05-21 13:14:56'),
(56, 47, '261590118633', 'Image_Upload_Guidelines.pdf', '2020-05-22 05:38:37'),
(58, 49, '301590142768', 'Brief Format- Brand Store.docx', '2020-05-22 12:21:52'),
(59, 49, '301590142768', 'Amazon_Stores_Creative_Asset_Requirements.pdf', '2020-05-22 12:21:52'),
(60, 50, '311590147371', 'Revive Laundry Sanitizer Refill 400ml.docx', '2020-05-22 13:41:35'),
(61, 51, '311590150975', 'Revive Laundry Sanitizer Refill 400ml.docx', '2020-05-22 14:38:19'),
(62, 52, '61590317433', '24-dabur-chyawanprash-OK.jpg', '2020-05-24 12:51:30'),
(63, 53, '61590384194', '24-dabur-chyawanprash-OK.jpg', '2020-05-25 07:23:24'),
(64, 54, '71590391069', 'veggi clean tomato 2.jpeg', '2020-05-25 09:18:19'),
(65, 55, '221590397166', 'jsdliugkbs.docx', '2020-05-25 11:00:32');

-- --------------------------------------------------------

--
-- Table structure for table `wc_clients`
--

CREATE TABLE `wc_clients` (
  `client_id` int(11) NOT NULL,
  `wekan_client_id` text NOT NULL,
  `wekan_brand_custom_brand_id` text NOT NULL,
  `wekan_pending_id` text NOT NULL,
  `wekan_ongoing_id` text NOT NULL,
  `wekan_rejecting_id` text NOT NULL,
  `client_name` varchar(100) NOT NULL,
  `client_image` varchar(255) NOT NULL,
  `client_phone` varchar(50) NOT NULL,
  `client_email` varchar(50) NOT NULL,
  `project_description` longtext NOT NULL,
  `allow_delivary_date_bm` enum('0','1') NOT NULL,
  `status` enum('Active','Inactive') NOT NULL,
  `deleted` enum('0','1') NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `wc_clients`
--

INSERT INTO `wc_clients` (`client_id`, `wekan_client_id`, `wekan_brand_custom_brand_id`, `wekan_pending_id`, `wekan_ongoing_id`, `wekan_rejecting_id`, `client_name`, `client_image`, `client_phone`, `client_email`, `project_description`, `allow_delivary_date_bm`, `status`, `deleted`) VALUES
(1, '7qZWPRK3oHY97XF8E', 'yP3PDQszNeujdd8aa', '', '', '', 'Marico', '158978096615895409341589510215marico2.png', '7655456776', 'marico@gmail.com', '<p>Your need to add project title and attach project doc before you proceed next. Ensure that you use the <span>brief template provided. <a href=\"https://drive.google.com/file/d/1eqmBQp4BjKatnRSYa7E3rm8Hg0tGfDut/view?usp=sharing\" target=\"_blank\"><strong><u>Download the Brief template here</u></strong></a></span></p>\n', '1', 'Active', '0'),
(2, 'EF2npBjbQ8m5LnsQ7', '9wn6RWPLy6KsXEPAA', '', '', '', 'zara', '1589694059zara.png', '9901962219', 'zara@inintra.com', '<p>Your need to add project title and attach project doc before you proceed next.</p>\n', '1', 'Active', '0'),
(3, 'CA3SovPSBHpQcsDvt', 'ykRr7PuymKf7ZbaAa', '', '', '', 'demo Account', '1589694537unnamed (12).jpg', ' 919599598833', 'india.harmony@gmail.com', '<p>Your need to add project title and attach project doc before you proceed next.</p>\n', '1', 'Active', '0'),
(4, 'xzaoqNgw9HXD9T88a', 'QoZdSzrM5wxqa4RR4', '', '', '', 'We Create Test', '1590033673logo_old.png', '', '', '<p>Your need to add project title and attach project doc before you proceed next.</p>\n', '1', 'Active', '0');

-- --------------------------------------------------------

--
-- Table structure for table `wc_client_wekan_list_relation`
--

CREATE TABLE `wc_client_wekan_list_relation` (
  `client_wekan_list_relation_id` int(11) NOT NULL,
  `client_id` int(11) NOT NULL,
  `wekan_board_list_id` int(11) NOT NULL,
  `wekan_client_id` text NOT NULL,
  `wekan_list_id` text NOT NULL,
  `wekan_list_name` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `wc_client_wekan_list_relation`
--

INSERT INTO `wc_client_wekan_list_relation` (`client_wekan_list_relation_id`, `client_id`, `wekan_board_list_id`, `wekan_client_id`, `wekan_list_id`, `wekan_list_name`) VALUES
(1, 1, 1, '7qZWPRK3oHY97XF8E', 'Dxug5mXw4Q8MmLMxu', 'Brief - Review Pending'),
(2, 1, 2, '7qZWPRK3oHY97XF8E', '69tLEsPoFpQjcaMi8', 'Rejected Brief'),
(3, 1, 3, '7qZWPRK3oHY97XF8E', 'AuM4K7iLn6jPEwBQg', 'Ongoing Tasks'),
(4, 1, 4, '7qZWPRK3oHY97XF8E', '5DvFFEKD3sLDE5xhE', 'Task Completed'),
(5, 1, 5, '7qZWPRK3oHY97XF8E', 'oAJGDqqfCM3jt5NCZ', 'Feedbacks'),
(6, 1, 6, '7qZWPRK3oHY97XF8E', 'KKW6xPDwavxF8Qnhn', 'Pending for Approval by Marico SPOC'),
(7, 1, 7, '7qZWPRK3oHY97XF8E', 'dKLsxboTnfsZPyBRf', 'Pending for Approval by CD'),
(8, 2, 1, 'EF2npBjbQ8m5LnsQ7', 'vyJ3ERbc8rbGdZLWF', 'Brief - Review Pending'),
(9, 2, 2, 'EF2npBjbQ8m5LnsQ7', 'Nukx3rnBfWrCzT49p', 'Rejected Brief'),
(10, 2, 3, 'EF2npBjbQ8m5LnsQ7', 'NmeMntEM6ExFnLDZw', 'Ongoing Tasks'),
(11, 2, 4, 'EF2npBjbQ8m5LnsQ7', 'ukCPBiXnwQin9WBqr', 'Task Completed'),
(12, 2, 5, 'EF2npBjbQ8m5LnsQ7', '9cQhbCRKvJ7vDYn4J', 'Feedbacks'),
(13, 2, 6, 'EF2npBjbQ8m5LnsQ7', 'YSgquzYQuGfJfvP9s', 'Pending for Approval by Marico SPOC'),
(14, 2, 7, 'EF2npBjbQ8m5LnsQ7', 'tLFkJdh4Jpz6MgPbf', 'Pending for Approval by CD'),
(15, 3, 1, 'CA3SovPSBHpQcsDvt', 'ZtzCAYJKbTPwu7F9j', 'Brief - Review Pending'),
(16, 3, 2, 'CA3SovPSBHpQcsDvt', 'SjadiZzHDmYP9S47n', 'Rejected Brief'),
(17, 3, 3, 'CA3SovPSBHpQcsDvt', 'hgcwvpfLqpceGkw5z', 'Ongoing Tasks'),
(18, 3, 4, 'CA3SovPSBHpQcsDvt', 'nvRgt7swNNAjqiGNA', 'Task Completed'),
(19, 3, 5, 'CA3SovPSBHpQcsDvt', 'JyMqJTpktKFDkwuBk', 'Feedbacks'),
(20, 3, 6, 'CA3SovPSBHpQcsDvt', 'kr37FdSSWByWcmdN5', 'Pending for Approval by Marico SPOC'),
(21, 3, 7, 'CA3SovPSBHpQcsDvt', 'wCkRtD4eJYyENKYHT', 'Pending for Approval by CD'),
(22, 4, 1, 'xzaoqNgw9HXD9T88a', '3gK3m4bzoeY6y9Lt6', 'Brief - Review Pending'),
(23, 4, 2, 'xzaoqNgw9HXD9T88a', 'YqedrExFWcynGtS2h', 'Rejected Brief'),
(24, 4, 3, 'xzaoqNgw9HXD9T88a', 'BCTbmSWwBni4q5AGG', 'Ongoing Tasks'),
(25, 4, 4, 'xzaoqNgw9HXD9T88a', 'LmoMQBqL3JMkQbDGx', 'Task Completed'),
(26, 4, 5, 'xzaoqNgw9HXD9T88a', 'acfzmCqkbBxdSQnE8', 'Feedbacks'),
(27, 4, 6, 'xzaoqNgw9HXD9T88a', 'XnRHEhMrCin7253E5', 'Pending for Approval by Marico SPOC'),
(28, 4, 7, 'xzaoqNgw9HXD9T88a', 'zM8M7jTK7oBNXW723', 'Pending for Approval by CD');

-- --------------------------------------------------------

--
-- Table structure for table `wc_feedback`
--

CREATE TABLE `wc_feedback` (
  `feedback_id` int(11) NOT NULL,
  `unique_id` text NOT NULL,
  `brief_id` int(11) NOT NULL,
  `brand_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `feedback_type_id` int(11) NOT NULL,
  `feedback_rating` int(11) NOT NULL,
  `feedback_reason` text NOT NULL,
  `feedback_datetime` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `wc_feedback`
--

INSERT INTO `wc_feedback` (`feedback_id`, `unique_id`, `brief_id`, `brand_id`, `user_id`, `feedback_type_id`, `feedback_rating`, `feedback_reason`, `feedback_datetime`) VALUES
(1, '158954652718453', 5, 1, 3, 1, 5, 'Simply fantastic', '2020-05-15 14:42:07'),
(2, '158954652718453', 5, 1, 3, 2, 3, 'Good', '2020-05-15 14:42:07'),
(3, '1589697535143576', 11, 2, 6, 1, 4, 'Simply Great', '2020-05-17 08:38:55'),
(4, '1589697535143576', 11, 2, 6, 2, 3, 'Good', '2020-05-17 08:38:55'),
(5, '1589708406145648', 18, 3, 8, 1, 3, 'Its Good', '2020-05-17 11:40:06'),
(6, '1589708406145648', 18, 3, 8, 2, 3, 'Good', '2020-05-17 11:40:06'),
(7, '158971985739917', 21, 1, 7, 1, 4, 'Simply Great', '2020-05-17 14:50:57'),
(8, '158971985739917', 21, 1, 7, 2, 5, 'Fantastic', '2020-05-17 14:50:57'),
(9, '15897863712988114', 23, 1, 14, 1, 5, 'Simply fantastic', '2020-05-18 09:19:31'),
(10, '15897863712988114', 23, 1, 14, 2, 5, 'Awesome', '2020-05-18 09:19:32'),
(11, '1589862807295493', 24, 1, 3, 1, 4, 'Simply Great', '2020-05-19 06:33:27'),
(12, '1589862807295493', 24, 1, 3, 2, 3, 'Good', '2020-05-19 06:33:27'),
(13, '1589885050935214', 31, 1, 14, 1, 4, 'Simply Great', '2020-05-19 12:44:10'),
(14, '1589885050935214', 31, 1, 14, 2, 4, 'Simply  Great', '2020-05-19 12:44:11'),
(15, '1590036946290886', 42, 0, 6, 1, 5, 'It is fantastic', '2020-05-21 06:55:46'),
(16, '1590036946290886', 42, 0, 6, 2, 4, 'Simply  Great', '2020-05-21 06:55:47'),
(17, '15900516412264626', 44, 0, 26, 1, 4, 'Simply Great', '2020-05-21 11:00:41'),
(18, '15900516412264626', 44, 0, 26, 2, 4, 'Simply  Great', '2020-05-21 11:00:41'),
(19, '1590060730736814', 25, 1, 14, 1, 5, 'Simply fantastic', '2020-05-21 13:32:10'),
(20, '1590060730736814', 25, 1, 14, 2, 4, 'Simply  Great', '2020-05-21 13:32:10'),
(21, '15900613482413014', 46, 1, 14, 1, 5, 'Simply fantastic', '2020-05-21 13:42:28'),
(22, '15900613482413014', 46, 1, 14, 2, 5, 'Fantastic', '2020-05-21 13:42:28');

-- --------------------------------------------------------

--
-- Table structure for table `wc_feedback_content`
--

CREATE TABLE `wc_feedback_content` (
  `feedback_content_id` int(11) NOT NULL,
  `feedback_type_id` int(11) NOT NULL,
  `feedback_rating_id` int(11) NOT NULL,
  `feedback_rating` int(11) NOT NULL,
  `feedback_content` text NOT NULL,
  `deleted` enum('0','1') NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `wc_feedback_content`
--

INSERT INTO `wc_feedback_content` (`feedback_content_id`, `feedback_type_id`, `feedback_rating_id`, `feedback_rating`, `feedback_content`, `deleted`) VALUES
(1, 1, 0, 0, 'Brief', '0'),
(2, 1, 0, 0, 'Copy', '0'),
(3, 1, 0, 0, 'Graphics', '0'),
(4, 1, 0, 0, 'Other', '0'),
(5, 2, 0, 0, 'Late delivery', '0'),
(6, 2, 0, 0, 'Other', '0'),
(7, 2, 0, 0, 'ddddd', '1'),
(8, 2, 6, 1, 'It\'s bad', '0'),
(9, 2, 6, 1, 'Too bad', '0'),
(10, 2, 7, 2, 'Bad ', '0'),
(11, 2, 7, 2, 'Not Cool', '0'),
(12, 2, 8, 3, 'Good', '0'),
(13, 2, 8, 3, 'Simply Good', '0'),
(14, 2, 9, 4, 'Simply  Great', '0'),
(15, 2, 9, 4, 'Its Great', '0'),
(16, 2, 10, 5, 'Awesome', '0'),
(17, 2, 10, 5, 'Fantastic', '0'),
(18, 2, 10, 5, 'Fantabulas', '0'),
(19, 1, 1, 1, 'Late Delivery', '0'),
(20, 1, 1, 1, 'Need to improve delivery', '0'),
(21, 1, 2, 2, 'Its very Poor', '0'),
(22, 1, 2, 2, 'Ok', '0'),
(23, 1, 3, 3, 'Its Good', '0'),
(24, 1, 3, 3, 'Fine', '0'),
(25, 1, 4, 4, 'Simply Great', '0'),
(26, 1, 5, 5, 'It is fantastic', '0'),
(27, 1, 5, 5, 'Simply fantastic', '0'),
(28, 1, 5, 5, 'Teriffic', '0');

-- --------------------------------------------------------

--
-- Table structure for table `wc_feedback_rating`
--

CREATE TABLE `wc_feedback_rating` (
  `feedback_rating_id` int(11) NOT NULL,
  `feedback_type_id` int(11) NOT NULL,
  `feedback_rating` int(11) NOT NULL,
  `feedback_rating_text` text NOT NULL,
  `deleted` enum('0','1') NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `wc_feedback_rating`
--

INSERT INTO `wc_feedback_rating` (`feedback_rating_id`, `feedback_type_id`, `feedback_rating`, `feedback_rating_text`, `deleted`) VALUES
(1, 1, 1, 'Terrible', '0'),
(2, 1, 2, 'Poor', '0'),
(3, 1, 3, 'Good', '0'),
(4, 1, 4, 'Great', '0'),
(5, 1, 5, 'Fantastic', '0'),
(6, 2, 1, 'Terrible', '0'),
(7, 2, 2, 'Poor', '0'),
(8, 2, 3, 'Good', '0'),
(9, 2, 4, 'Great', '0'),
(10, 2, 5, 'Fantastic', '0');

-- --------------------------------------------------------

--
-- Table structure for table `wc_feedback_type`
--

CREATE TABLE `wc_feedback_type` (
  `feedback_type_id` int(11) NOT NULL,
  `feedback_type` varchar(50) NOT NULL,
  `status` enum('Active','Inactive') NOT NULL,
  `deleted` enum('0','1') NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `wc_feedback_type`
--

INSERT INTO `wc_feedback_type` (`feedback_type_id`, `feedback_type`, `status`, `deleted`) VALUES
(1, 'Quality', 'Active', '0'),
(2, 'Speed', 'Active', '0'),
(3, 'fffffff', 'Inactive', '1');

-- --------------------------------------------------------

--
-- Table structure for table `wc_image_annotation`
--

CREATE TABLE `wc_image_annotation` (
  `id` int(255) NOT NULL,
  `user_id` int(11) NOT NULL,
  `image_id` int(11) NOT NULL,
  `revision_id` int(11) NOT NULL,
  `file_type` enum('0','1') NOT NULL,
  `descr` text NOT NULL,
  `position_left` varchar(255) NOT NULL,
  `position_top` varchar(255) NOT NULL,
  `parent_id` int(255) NOT NULL,
  `target_selector` text NOT NULL,
  `frametrail_attributes` text NOT NULL,
  `create_date` date NOT NULL,
  `img_an_status` enum('0','1') NOT NULL,
  `apply_all` enum('0','1') NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `wc_image_annotation`
--

INSERT INTO `wc_image_annotation` (`id`, `user_id`, `image_id`, `revision_id`, `file_type`, `descr`, `position_left`, `position_top`, `parent_id`, `target_selector`, `frametrail_attributes`, `create_date`, `img_an_status`, `apply_all`) VALUES
(80, 16, 78, 1, '0', 'Hazy Image', '611.0173568725586', '285.3680419921875', 80, '', '', '2020-05-20', '0', '0'),
(83, 3, 76, 2, '0', 'test 124', '275', '256', 83, '', '', '2020-05-20', '0', '1'),
(88, 11, 78, 1, '0', 'dlksnfs', '760', '143', 88, '', '', '2020-05-20', '0', '0'),
(91, 3, 74, 1, '0', 'test 124', '275', '256', 83, '', '', '2020-05-20', '0', '1'),
(92, 3, 75, 1, '0', 'test 124', '275', '256', 83, '', '', '2020-05-20', '0', '1'),
(93, 3, 74, 1, '0', 'test 124', '275', '256', 83, '', '', '2020-05-20', '0', '1'),
(94, 3, 75, 1, '0', 'test 124', '275', '256', 83, '', '', '2020-05-20', '0', '1'),
(95, 3, 74, 1, '0', 'test 124', '275', '256', 83, '', '', '2020-05-20', '0', '1'),
(96, 3, 75, 1, '0', 'test 124', '275', '256', 83, '', '', '2020-05-20', '0', '1'),
(97, 3, 81, 1, '0', 'test 11111', '420', '286', 97, '', '', '2020-05-20', '0', '1'),
(98, 3, 82, 1, '0', 'test 11111', '420', '286', 97, '', '', '2020-05-20', '0', '1'),
(99, 3, 83, 1, '0', 'test 11111', '420', '286', 97, '', '', '2020-05-20', '0', '1'),
(100, 6, 80, 1, '1', 'xccxcxc', '', '', 0, 't=0,4&xywh=percent:47.221698095841184,27.58260875259561,30,30', '&lt;p&gt;xccxcxc&lt;br&gt;&lt;/p&gt;', '2020-05-20', '0', '0'),
(103, 3, 88, 1, '0', 'change the backgourd', '542', '212', 103, '', '', '2020-05-20', '0', '0'),
(104, 3, 89, 1, '0', 'change the backgourd', '542', '212', 103, '', '', '2020-05-20', '0', '1'),
(106, 3, 90, 1, '0', 'change font color', '520', '250', 106, '', '', '2020-05-20', '0', '0'),
(107, 3, 86, 2, '0', 'jjjjjjjjj', '743', '85', 107, '', '', '2020-05-20', '0', '0'),
(108, 16, 91, 1, '0', 'Change the Backgourd', '536', '237', 108, '', '', '2020-05-20', '0', '1'),
(109, 16, 92, 1, '0', 'Change the Backgourd', '536', '237', 108, '', '', '2020-05-20', '0', '1'),
(110, 16, 93, 1, '0', 'Change the Backgourd', '536', '237', 108, '', '', '2020-05-20', '0', '1'),
(111, 16, 94, 1, '0', 'Change the Backgourd', '536', '237', 108, '', '', '2020-05-20', '0', '1'),
(112, 6, 85, 1, '0', '111111111', '476', '344.3999938964844', 112, '', '', '2020-05-20', '0', '0'),
(113, 6, 84, 1, '0', '111111111111', '588', '769.4000244140625', 113, '', '', '2020-05-20', '0', '0'),
(114, 6, 95, 1, '0', 'fdfdf', '518', '600.3999938964844', 114, '', '', '2020-05-20', '0', '0'),
(115, 22, 97, 1, '0', 'test', '865', '59', 115, '', '', '2020-05-21', '0', '1'),
(117, 26, 102, 1, '0', 'Test', '505', '240', 117, '', '', '2020-05-21', '0', '0'),
(121, 14, 108, 1, '1', 'Please change font to futura and blue in color', '', '', 0, 't=1.955414,5.955414&xywh=percent:4.535241740867105,7.037252780782779,40.82827106044215,63.72916146570137', '&lt;p&gt;Please change font to futura and blue in color&lt;/p&gt;', '2020-05-21', '0', '0'),
(122, 14, 108, 1, '1', 'Please change logo to logo type version', '', '', 0, 't=0,4&xywh=percent:71.93399485889604,6.155504574476907,30,30', '&lt;p&gt;Please change logo to logo type version&lt;/p&gt;', '2020-05-21', '0', '0'),
(123, 14, 108, 1, '1', 'Product placement looks fake. Please revise', '', '', 0, 't=5.237629,7.083&xywh=percent:40.09231874844523,72.49232980458277,30,30', '&lt;p&gt;Product placement looks fake. Please revise&lt;/p&gt;', '2020-05-21', '0', '0'),
(124, 14, 109, 1, '0', 'remove logo', '845', '73', 124, '', '', '2020-05-21', '0', '0'),
(125, 14, 109, 1, '0', 'please change font', '179', '170', 125, '', '', '2020-05-21', '0', '0'),
(126, 14, 109, 1, '0', 'replace this with the jar', '613', '525', 126, '', '', '2020-05-21', '0', '0'),
(138, 26, 113, 1, '0', 'Testtt111', '197', '219', 138, '', '', '2020-05-22', '0', '1'),
(139, 26, 114, 1, '0', 'Testtt111', '197', '219', 138, '', '', '2020-05-22', '0', '1'),
(140, 26, 115, 1, '0', 'Testtt111', '197', '219', 138, '', '', '2020-05-22', '0', '1'),
(141, 26, 116, 1, '0', 'Testtt111', '197', '219', 138, '', '', '2020-05-22', '0', '1'),
(143, 6, 123, 1, '1', 'fffffffff', '', '', 0, 't=0,4&xywh=percent:63.558487288426804,34.73531153816848,30,30', '&lt;p&gt;fffffffff&lt;br&gt;&lt;/p&gt;', '2020-05-22', '0', '0'),
(144, 6, 123, 1, '1', 'ddddd', '', '', 0, 't=0,4&xywh=percent:17.815477549187086,21.323993815219346,30,30', '&lt;p&gt;ddddd&lt;br&gt;&lt;/p&gt;', '2020-05-22', '0', '0'),
(146, 6, 121, 1, '0', '11111', '707', '913.4000244140625', 146, '', '', '2020-05-22', '0', '0'),
(147, 6, 125, 2, '0', 'ttttttt', '574', '574.3999938964844', 147, '', '', '2020-05-22', '0', '0'),
(148, 26, 127, 1, '1', 'change color', '', '', 0, 't=1.121718,5.1217179999999995&xywh=percent:40.559220860823125,35.958390058295805,30,30', '&lt;p&gt;change color&lt;/p&gt;', '2020-05-22', '0', '0'),
(154, 7, 70, 1, '0', 'Change the background in grey #78789\n', '157', '111', 154, '', '', '2020-05-22', '0', '1'),
(157, 7, 71, 1, '0', 'Change the background in grey #78789\n', '157', '111', 154, '', '', '2020-05-22', '0', '1'),
(158, 7, 72, 1, '0', 'Change the background in grey #78789\n', '157', '111', 154, '', '', '2020-05-22', '0', '1'),
(159, 6, 129, 1, '0', 'comment for all cards, images are looking wel.. Please improve quality. Thks', '687', '52.399993896484375', 159, '', '', '2020-05-24', '0', '1'),
(160, 6, 130, 1, '0', 'comment for all cards, images are looking wel.. Please improve quality. Thks', '687', '52.399993896484375', 159, '', '', '2020-05-24', '0', '1'),
(161, 6, 131, 1, '0', 'comment for all cards, images are looking wel.. Please improve quality. Thks', '687', '52.399993896484375', 159, '', '', '2020-05-24', '0', '1'),
(162, 6, 132, 1, '0', 'comment for all cards, images are looking wel.. Please improve quality. Thks', '687', '52.399993896484375', 159, '', '', '2020-05-24', '0', '1'),
(163, 6, 132, 1, '0', 'here, its not looking well', '714', '577.3999938964844', 163, '', '', '2020-05-24', '0', '0'),
(164, 6, 134, 1, '0', 'aaaaaaaaaaaa', '608', '70.39999389648438', 164, '', '', '2020-05-24', '0', '0'),
(165, 6, 134, 1, '0', 'aaaaaaaaaaa', '591', '143.39999389648438', 165, '', '', '2020-05-24', '0', '0'),
(166, 6, 134, 1, '0', 'aaaaaaaaaaa', '581', '212.39999389648438', 166, '', '', '2020-05-24', '0', '1'),
(167, 6, 129, 1, '0', 'aaaaaaaaaaa', '581', '212.39999389648438', 166, '', '', '2020-05-24', '0', '1'),
(168, 6, 130, 1, '0', 'aaaaaaaaaaa', '581', '212.39999389648438', 166, '', '', '2020-05-24', '0', '1'),
(169, 6, 131, 1, '0', 'aaaaaaaaaaa', '581', '212.39999389648438', 166, '', '', '2020-05-24', '0', '1'),
(170, 6, 133, 2, '0', 'aaaaaaaaaaa', '581', '212.39999389648438', 166, '', '', '2020-05-24', '0', '1'),
(171, 6, 133, 2, '0', 'ddddddddddddddddd', '720', '112.40000915527344', 171, '', '', '2020-05-24', '0', '1'),
(172, 6, 129, 1, '0', 'ddddddddddddddddd', '720', '112.40000915527344', 171, '', '', '2020-05-24', '0', '1'),
(173, 6, 130, 1, '0', 'ddddddddddddddddd', '720', '112.40000915527344', 171, '', '', '2020-05-24', '0', '1'),
(174, 6, 131, 1, '0', 'ddddddddddddddddd', '720', '112.40000915527344', 171, '', '', '2020-05-24', '0', '1'),
(175, 6, 134, 1, '0', 'ddddddddddddddddd', '720', '112.40000915527344', 171, '', '', '2020-05-24', '0', '1'),
(176, 22, 144, 1, '0', 'test', '375', '208', 176, '', '', '2020-05-25', '0', '0'),
(177, 22, 144, 1, '0', 'test2', '617', '325', 177, '', '', '2020-05-25', '0', '0'),
(178, 22, 145, 1, '0', 'mnb ', '330', '170', 178, '', '', '2020-05-25', '0', '0'),
(179, 6, 135, 1, '0', '1111111111111111', '670', '241.39999389648438', 179, '', '', '2020-05-25', '0', '0');

-- --------------------------------------------------------

--
-- Table structure for table `wc_image_upload`
--

CREATE TABLE `wc_image_upload` (
  `image_id` int(11) NOT NULL,
  `img_title` varchar(200) NOT NULL,
  `image_path` varchar(200) NOT NULL,
  `file_type` enum('0','1') NOT NULL,
  `brand_id` int(11) NOT NULL,
  `brief_id` int(11) NOT NULL,
  `revision_id` int(11) NOT NULL,
  `parent_img` int(11) NOT NULL,
  `version_num` varchar(10) NOT NULL DEFAULT '1',
  `added_on` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `added_by` int(11) NOT NULL,
  `img_status` enum('0','1','2','3','4') NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `wc_image_upload`
--

INSERT INTO `wc_image_upload` (`image_id`, `img_title`, `image_path`, `file_type`, `brand_id`, `brief_id`, `revision_id`, `parent_img`, `version_num`, `added_on`, `added_by`, `img_status`) VALUES
(1, 'Test meri', '12121_1589541537.jpg', '0', 1, 1, 1, 1, '1', '2020-05-15 11:20:18', 2, '3'),
(2, 'test 15-05-2020', '51_honey_1589541655.jpg', '0', 1, 3, 2, 2, '1', '2020-05-15 11:41:34', 2, '4'),
(3, 'test 15-05-2020', '111_vatika_anti_dandruff_shampoo_1589541655.jpg', '0', 1, 3, 3, 3, '1', '2020-05-15 11:36:17', 2, '1'),
(4, 'test 15-05-2020', '24_dabur_chyawanprash_1589541657.png', '0', 1, 3, 4, 4, '1', '2020-05-15 11:42:22', 2, '4'),
(5, 'test 15-05-2020', '51_honey_OK_1589542894.jpg', '0', 1, 3, 0, 2, '2', '2020-05-15 11:50:56', 2, '4'),
(6, 'test 15-05-2020', '24_dabur_chyawanprash_OK_1589542942.jpg', '0', 1, 3, 0, 4, '2', '2020-05-15 11:49:46', 2, '1'),
(7, 'test 15-05-2020', '51_honey_1589543456.jpg', '0', 1, 3, 0, 2, '3', '2020-05-15 11:50:56', 2, '2'),
(8, 'Demo1', 'img1_1589545763.jpeg', '0', 1, 5, 5, 8, '1', '2020-05-15 12:31:27', 2, '1'),
(9, 'Demo1', 'img0_1589545763.jpg', '0', 1, 5, 6, 9, '1', '2020-05-15 12:35:50', 2, '4'),
(10, 'Demo1', 'StayHome_1589545806.mp4', '1', 1, 5, 7, 10, '1', '2020-05-15 12:36:22', 2, '4'),
(11, 'Demo1', 'img0_1589546150.jpg', '0', 1, 5, 0, 9, '2', '2020-05-15 12:37:52', 2, '1'),
(12, 'Demo1', 'StayHome_1589546182.mp4', '1', 1, 5, 0, 10, '2', '2020-05-15 12:37:57', 2, '1'),
(15, 'Demo 4', '13333_1589655026.jpg', '0', 1, 8, 10, 15, '1', '2020-05-17 10:47:22', 2, '4'),
(16, 'Demo 4', '777788_1589655059.jpeg', '0', 1, 8, 11, 16, '1', '2020-05-16 18:56:30', 2, '2'),
(17, 'Demo 4', 'BirdNoSound_1589655165.mp4', '1', 1, 8, 12, 17, '1', '2020-05-17 05:38:06', 2, '4'),
(18, 'Demo 4', 'WhatsApp_Video_2020_05_10_at_13.04.48_1589693886.mp4', '1', 1, 8, 0, 17, '2', '2020-05-17 08:54:53', 1, '3'),
(19, 'bbb', '6_1589695165.jpg', '0', 2, 11, 13, 19, '1', '2020-05-17 06:00:43', 5, '1'),
(20, 'bbb', '2_1589695165.jpg', '0', 2, 11, 14, 20, '1', '2020-05-17 06:01:57', 5, '4'),
(21, 'bbb', '4_1589695166.jpg', '0', 2, 11, 15, 21, '1', '2020-05-17 06:02:12', 5, '4'),
(22, 'bbb', '2_1589695317.jpg', '0', 2, 11, 0, 20, '2', '2020-05-17 06:03:53', 5, '1'),
(23, 'bbb', '4_1589695332.jpg', '0', 2, 11, 0, 21, '2', '2020-05-17 06:03:57', 5, '1'),
(24, 'Project  Brand 1', 'WhatsApp_Image_2020_05_10_at_13.17.48_1589695459.jpeg', '0', 3, 12, 16, 24, '1', '2020-05-17 06:07:22', 9, '3'),
(25, 'Project  Brand 1', 'WhatsApp_Image_2020_05_10_at_13.18.42_1589695459.jpeg', '0', 3, 12, 17, 25, '1', '2020-05-17 06:04:19', 9, '0'),
(26, 'Project  Brand 1', 'veggie_clean_gif_1589695463.mp4', '1', 3, 12, 18, 26, '1', '2020-05-17 06:04:23', 9, '0'),
(27, 'video test', '20200517_075414_1589695508.mp4', '1', 2, 13, 19, 27, '1', '2020-05-17 06:08:33', 5, '4'),
(28, 'video test', '20200517_075414_1589695713.mp4', '1', 2, 13, 0, 27, '2', '2020-05-17 06:32:47', 5, '1'),
(29, 'project 2 brand 1', 'veggi_clean_tomato_2_1589696194.jpeg', '0', 3, 15, 20, 29, '1', '2020-05-17 06:16:35', 7, '0'),
(30, 'project 2 brand 1', 'WhatsApp_Image_2020_05_10_at_13.17.47_1589696195.jpeg', '0', 3, 15, 21, 30, '1', '2020-05-17 06:16:35', 7, '0'),
(35, 'prooject 6', 'veggi_clean_tomato_2_1589701818.jpeg', '0', 3, 18, 26, 35, '1', '2020-05-17 09:10:39', 9, '1'),
(36, '', 'veggie_clean_gif_1589701825.mp4', '', 0, 0, 27, 36, '1', '2020-05-17 07:50:25', 9, '0'),
(38, '', '122_1589705889.mp4', '', 0, 0, 29, 38, '1', '2020-05-17 08:58:09', 2, '0'),
(41, 'Demo 4', 'veggi_clean_tomato_2_1589712442.jpeg', '0', 1, 8, 0, 15, '2', '2020-05-17 10:47:22', 11, '2'),
(42, 'video2', '20200515_041414_1589712657.mp4', '1', 2, 20, 31, 42, '1', '2020-05-17 10:58:35', 5, '0'),
(43, 'video2', '2_1589713037.jpg', '0', 2, 20, 32, 43, '1', '2020-05-17 10:57:17', 5, '0'),
(44, 'Demo 4', 'WhatsApp_Image_2020_05_10_at_13.18.42_1589713266.jpeg', '0', 1, 8, 33, 44, '1', '2020-05-17 11:01:07', 11, '0'),
(45, 'Project test3', 'Veggi_clean___tomato_1589719384.jpeg', '0', 1, 21, 34, 45, '1', '2020-05-17 12:47:25', 10, '4'),
(48, 'Project test3', 'WhatsApp_Image_2020_05_10_at_13.17.48_1589719645.jpeg', '0', 1, 21, 0, 45, '2', '2020-05-17 12:48:18', 10, '1'),
(49, 'ggggg', '111_vatika_anti_dandruff_shampoo_1589775724.jpg', '0', 2, 22, 37, 49, '1', '2020-05-18 04:22:04', 5, '0'),
(50, 'Moksha HR Team Temaplate Creation', 'Set_Wet_Color_Op_1__1589785498.mp4', '1', 1, 23, 38, 50, '1', '2020-05-18 07:13:38', 10, '4'),
(52, 'Moksha HR Team Temaplate Creation', 'Product_Cards_18_5_26_1589786018.jpg', '0', 1, 23, 0, 50, '2', '2020-05-18 07:15:07', 10, '1'),
(53, 'Test', '111_1589861973.jfif', '0', 1, 24, 40, 53, '1', '2020-05-19 04:23:31', 2, '4'),
(54, 'Test', '1323343_1589862083.jfif', '0', 1, 24, 41, 54, '1', '2020-05-19 04:31:48', 2, '1'),
(55, 'Test', 'any__2__1589862097.jfif', '0', 1, 24, 42, 55, '1', '2020-05-19 04:23:43', 2, '1'),
(56, 'Test', 'any_1589862102.jfif', '0', 1, 24, 43, 56, '1', '2020-05-19 04:31:51', 2, '1'),
(57, 'Test', '1323343_1589862211.jfif', '0', 1, 24, 0, 53, '2', '2020-05-19 04:31:43', 2, '1'),
(58, 'Test2', '13333_1589862326.jpg', '0', 1, 25, 44, 58, '1', '2020-05-19 04:30:06', 2, '4'),
(59, 'Test2', '12121_1589862327.jpg', '0', 1, 25, 45, 59, '1', '2020-05-19 04:28:29', 2, '1'),
(60, 'ssssss', '12_fem_fairness_naturals_gold_bleach_1589862364.jpg', '0', 2, 26, 46, 60, '1', '2020-05-19 04:29:59', 5, '1'),
(61, 'Test2', 'BirdNoSound_1589862413.mp4', '1', 1, 25, 47, 61, '1', '2020-05-19 04:31:14', 2, '4'),
(62, 'Test2', '1122333_1589862606.jpeg', '0', 1, 25, 0, 58, '2', '2020-05-19 04:31:27', 2, '1'),
(63, 'Test2', 'BirdNoSound_1589862674.mp4', '1', 1, 25, 0, 61, '2', '2020-05-19 04:31:31', 2, '1'),
(64, 'Saffola fitify Web Banner Design Amazon', 'Revised_video__1589884290.mp4', '1', 1, 31, 48, 64, '1', '2020-05-19 10:40:56', 10, '4'),
(65, 'Saffola fitify Web Banner Design Amazon', 'Product_Cards_14_5_25_1589884324.jpg', '0', 1, 31, 49, 65, '1', '2020-05-19 10:40:09', 10, '4'),
(66, 'Saffola fitify Web Banner Design Amazon', 'Product_Cards_14_5_25_1589884809.jpg', '0', 1, 31, 0, 65, '2', '2020-05-19 10:42:15', 10, '1'),
(67, 'Saffola fitify Web Banner Design Amazon', 'Revised_video__1589884856.mp4', '1', 1, 31, 0, 64, '2', '2020-05-19 10:41:43', 10, '1'),
(68, 'new', '1122333_1589893165.jfif', '0', 1, 4, 50, 68, '1', '2020-05-19 13:00:10', 2, '2'),
(69, 'new', '1323343_1589893174.jfif', '0', 1, 4, 51, 69, '1', '2020-05-19 12:59:34', 2, '0'),
(70, 'Test saffola', '2_1589946651.jpg', '0', 5, 29, 52, 70, '1', '2020-05-21 17:23:33', 2, '2'),
(71, 'Test saffola', '111_1589946665.jfif', '0', 5, 29, 53, 71, '1', '2020-05-21 17:23:43', 2, '2'),
(72, 'Test saffola', '1122333_1589946668.jfif', '0', 5, 29, 54, 72, '1', '2020-05-21 17:23:43', 2, '2'),
(73, 'test', '1122333_1589946831.jfif', '0', 1, 34, 55, 73, '1', '2020-05-20 03:56:06', 2, '4'),
(74, 'test', 'any__2__1589946841.jfif', '0', 1, 34, 56, 74, '1', '2020-05-20 04:11:56', 2, '2'),
(75, 'test', '1323343_1589946846.jfif', '0', 1, 34, 57, 75, '1', '2020-05-20 04:11:56', 2, '2'),
(76, 'test', 'MFethnx8907923444695_1_1589946966.jpg', '0', 1, 34, 0, 73, '2', '2020-05-20 04:46:19', 2, '3'),
(77, 'Test333', 'BirdNoSound_1589947294.mp4', '1', 1, 35, 58, 77, '1', '2020-05-20 04:44:31', 15, '2'),
(78, 'Test333', 'pexels_photo_414612_1589947357.jpeg', '0', 1, 35, 59, 78, '1', '2020-05-20 04:07:50', 15, '2'),
(80, 'aaaa', '20200520_064110_1589949999.mp4', '1', 2, 37, 61, 80, '1', '2020-05-20 08:42:31', 5, '2'),
(81, 'Test 444', '2_1589950132.jpg', '0', 1, 36, 62, 81, '1', '2020-05-20 04:54:27', 2, '4'),
(82, 'Test 444', '111_1589950142.jfif', '0', 1, 36, 63, 82, '1', '2020-05-20 04:53:37', 2, '3'),
(83, 'Test 444', '1122333_1589950147.jfif', '0', 1, 36, 64, 83, '1', '2020-05-20 04:49:22', 2, '2'),
(84, 'aaaa', '24_dabur_chyawanprash_1589950149.png', '0', 2, 37, 65, 84, '1', '2020-05-20 08:42:44', 5, '3'),
(85, 'dddd', '24_dabur_chyawanprash_1589950356.png', '0', 2, 38, 66, 85, '1', '2020-05-20 08:40:52', 5, '3'),
(86, 'Test 444', '1_1589950467.jpg', '0', 1, 36, 0, 81, '2', '2020-05-20 04:54:27', 2, '2'),
(87, 'dddd', '51_honey_1589950748.jpg', '0', 2, 38, 67, 87, '1', '2020-05-22 04:46:56', 5, '1'),
(88, 'Test-notes', '2_1589951230.jpg', '0', 1, 39, 68, 88, '1', '2020-05-20 05:11:15', 2, '3'),
(89, 'Test-notes', '1122333_1589951269.jfif', '0', 1, 39, 69, 89, '1', '2020-05-22 08:28:21', 2, '4'),
(90, 'Test-notes', '111_1589951270.jfif', '0', 1, 39, 70, 90, '1', '2020-05-20 05:08:56', 2, '2'),
(91, 'Test 123', '2_1589951759.jpg', '0', 5, 32, 71, 91, '1', '2020-05-20 05:17:58', 2, '3'),
(92, 'Test 123', '111_1589951793.jfif', '0', 5, 32, 72, 92, '1', '2020-05-20 05:17:52', 2, '2'),
(93, 'Test 123', '1122333_1589951798.jfif', '0', 5, 32, 73, 93, '1', '2020-05-20 05:17:52', 2, '2'),
(94, 'Test 123', '1323343_1589951816.jfif', '0', 5, 32, 74, 94, '1', '2020-05-20 05:17:52', 2, '2'),
(95, 'ssss', '24_dabur_chyawanprash_OK_1589970037.jpg', '0', 2, 40, 75, 95, '1', '2020-05-21 04:43:41', 5, '4'),
(96, 'gfbvn', 'img8_1590034372.jpg', '0', 8, 41, 76, 96, '1', '2020-05-21 04:13:28', 21, '1'),
(97, 'gfbvn', 'img7_1590034372.jpg', '0', 8, 41, 77, 97, '1', '2020-05-21 10:19:05', 21, '4'),
(98, 'gfbvn', 'img6_1590034373.jpeg', '0', 8, 41, 78, 98, '1', '2020-05-21 05:11:30', 21, '1'),
(99, 'ssss', '24_dabur_chyawanprash_OK_1590036221.jpg', '0', 2, 40, 0, 95, '2', '2020-05-21 04:45:09', 5, '1'),
(100, 'sssss', '111_vatika_anti_dandruff_shampoo_1590036697.jpg', '0', 2, 42, 79, 100, '1', '2020-05-21 04:52:20', 5, '1'),
(101, 'sssss', '24_dabur_chyawanprash_1590036697.png', '0', 2, 42, 80, 101, '1', '2020-05-21 04:52:24', 5, '1'),
(102, 'Test_mail', '1122333_1590051025.jfif', '0', 8, 44, 81, 102, '1', '2020-05-21 08:57:10', 25, '4'),
(103, 'Test_mail', '1323343_1590051027.jfif', '0', 8, 44, 82, 103, '1', '2020-05-21 08:58:20', 25, '1'),
(104, 'Test_mail', 'any__2__1590051027.jfif', '0', 8, 44, 83, 104, '1', '2020-05-21 08:58:24', 25, '1'),
(105, 'Test_mail', 'any_1590051027.jfif', '0', 8, 44, 84, 105, '1', '2020-05-21 08:58:28', 25, '1'),
(106, 'Test_mail', 'any__2__1590051430.jfif', '0', 8, 44, 0, 102, '2', '2020-05-21 08:58:17', 25, '1'),
(107, 'gfbvn', 'mqdefault__1__1590056345.jpg', '0', 8, 41, 0, 97, '2', '2020-05-21 10:19:05', 25, '2'),
(108, 'Set wet Digital Creative For vocal for local', 'Revised_video__1590059842.mp4', '1', 1, 46, 85, 108, '1', '2020-05-21 11:24:36', 19, '4'),
(109, 'Set wet Digital Creative For vocal for local', 'Product_Cards_14_5_19_1590059896.jpg', '0', 1, 46, 86, 109, '1', '2020-05-21 11:25:45', 19, '4'),
(110, 'Set wet Digital Creative For vocal for local', 'Revised_video__1590060276.mp4', '1', 1, 46, 0, 108, '2', '2020-05-21 11:28:16', 19, '1'),
(111, 'Set wet Digital Creative For vocal for local', 'Product_Cards_14_5_19_1590060345.jpg', '0', 1, 46, 0, 109, '2', '2020-05-21 11:27:49', 19, '1'),
(112, 'Test_22-05-20', '1_1590118906.jpg', '0', 8, 47, 87, 112, '1', '2020-05-22 03:43:45', 25, '1'),
(113, 'Test_22-05-20', '111_1590118927.jfif', '0', 8, 47, 88, 113, '1', '2020-05-22 05:41:49', 25, '4'),
(114, 'Test_22-05-20', '121_1590118932.jfif', '0', 8, 47, 89, 114, '1', '2020-05-22 03:44:07', 25, '2'),
(115, 'Test_22-05-20', '1122333_1590118959.jfif', '0', 8, 47, 90, 115, '1', '2020-05-22 03:44:07', 25, '2'),
(116, 'Test_22-05-20', '1323343_1590118967.jfif', '0', 8, 47, 91, 116, '1', '2020-05-22 03:44:07', 25, '2'),
(117, 'Test_22-05-20', 'any__1__1590119151.jfif', '0', 8, 47, 92, 117, '1', '2020-05-22 03:48:11', 25, '2'),
(118, 'Test_22-05-20', 'qwerwqer_1590119162.jfif', '0', 8, 47, 93, 118, '1', '2020-05-22 03:46:03', 25, '0'),
(119, 'Test_22-05-20', 'any__1_aesdfawe_1590119163.jfif', '0', 8, 47, 94, 119, '1', '2020-05-22 03:46:03', 25, '0'),
(121, 'aaa', '51_honey_OK_1590122979.jpg', '0', 2, 48, 96, 121, '1', '2020-05-22 05:05:33', 5, '4'),
(122, 'aaa', '111_vatika_anti_dandruff_shampoo_1590122980.jpg', '0', 2, 48, 97, 122, '1', '2020-05-22 04:56:17', 5, '2'),
(123, 'aaa', '20200521_115237_1590123122.mp4', '1', 2, 48, 98, 123, '1', '2020-05-22 04:53:25', 5, '2'),
(125, 'aaa', '51_honey_OK_1590123933.jpg', '0', 2, 48, 0, 121, '2', '2020-05-22 05:05:33', 5, '2'),
(126, 'Test_22-05-20', '1122333_1590126109.jfif', '0', 8, 47, 0, 113, '2', '2020-05-22 05:41:49', 25, '2'),
(127, 'Test_22-05-20', 'screen_recorder_video_2020_24_3_20_59_20_1590128219.mp4', '1', 8, 47, 100, 127, '1', '2020-05-22 06:19:06', 25, '2'),
(128, 'Test-notes', 'WhatsApp_Image_2020_05_10_at_13.17.47_1590136101.jpeg', '0', 1, 39, 0, 89, '2', '2020-05-22 08:43:48', 11, '2'),
(129, 'test', '51_honey_OK_1590317568.jpg', '0', 2, 52, 101, 129, '1', '2020-05-24 10:54:56', 5, '2'),
(130, 'test', '111_vatika_anti_dandruff_shampoo_1590317568.jpg', '0', 2, 52, 102, 130, '1', '2020-05-24 10:55:21', 5, '2'),
(131, 'test', '12_fem_fairness_naturals_gold_bleach_1590317568.jpg', '0', 2, 52, 103, 131, '1', '2020-05-24 10:55:21', 5, '2'),
(132, 'test', '24_dabur_chyawanprash_1590317569.png', '0', 2, 52, 104, 132, '1', '2020-05-24 11:11:38', 5, '4'),
(133, 'test', '24_dabur_chyawanprash_OK_1590318698.jpg', '0', 2, 52, 0, 132, '2', '2020-05-24 11:11:38', 5, '2'),
(134, 'test', 'WhatsApp_Image_2020_05_08_at_21.50.21_1589101777_1590318808.jpeg', '0', 2, 52, 105, 134, '1', '2020-05-24 11:14:35', 5, '2'),
(135, 'ssss', '12_fem_fairness_naturals_gold_bleach_1590397123.jpg', '0', 2, 53, 106, 135, '1', '2020-05-25 09:28:35', 5, '3'),
(136, 'ssss', '24_dabur_chyawanprash_OK_1590397184.jpg', '0', 2, 53, 107, 136, '1', '2020-05-25 08:59:44', 5, '0'),
(137, 'ssss', '51_honey_OK_1590397197.jpg', '0', 2, 53, 108, 137, '1', '2020-05-25 08:59:57', 5, '0'),
(138, 'ssss', '111_vatika_anti_dandruff_shampoo_ok_1590397197.jpg', '0', 2, 53, 109, 138, '1', '2020-05-25 08:59:57', 5, '0'),
(139, 'ssss', '111_vatika_anti_dandruff_shampoo_1590397202.jpg', '0', 2, 53, 110, 139, '1', '2020-05-25 09:00:02', 5, '0'),
(141, 'zdnv ', 'img1_1590397358.jpeg', '0', 8, 55, 112, 141, '1', '2020-05-25 09:06:20', 21, '1'),
(142, '', 'img0_1590397364.jpg', '', 0, 0, 113, 142, '1', '2020-05-25 09:02:44', 21, '0'),
(143, 'zdnv ', 'img0_1590397442.jpg', '0', 8, 55, 114, 143, '1', '2020-05-25 09:06:24', 21, '1'),
(144, 'zdnv ', 'img3_1590397504.jpg', '0', 8, 55, 115, 144, '1', '2020-05-25 09:06:47', 21, '3'),
(145, 'zdnv ', 'img2_1590397505.jpg', '0', 8, 55, 116, 145, '1', '2020-05-25 09:11:32', 21, '4'),
(146, 'zdnv ', 'img2_1590397892.jpg', '0', 8, 55, 0, 145, '2', '2020-05-25 09:11:32', 21, '2'),
(147, 'ssss', '2_1590398660.jpg', '0', 2, 53, 117, 147, '1', '2020-05-25 09:24:20', 5, '0'),
(148, 'ssss', '5_1590398660.jpg', '0', 2, 53, 118, 148, '1', '2020-05-25 09:24:20', 5, '0'),
(149, 'ssss', '1_1590398660.jpg', '0', 2, 53, 119, 149, '1', '2020-05-25 09:24:20', 5, '0'),
(150, 'ssss', '4_1590398660.jpg', '0', 2, 53, 120, 150, '1', '2020-05-25 09:24:20', 5, '0'),
(151, 'Revive Laundry Sanitizer Refill Pouch 400ml', 'Revive_Liquid_Detergent_400ml_refill_Pouch_copy_page_002_1590399555.jpg', '0', 13, 51, 121, 151, '1', '2020-05-25 09:39:15', 10, '0'),
(152, 'Revive Laundry Sanitizer Refill Pouch 400ml', 'Revive_Liquid_Detergent_400ml_refill_Pouch_copy_page_001_1590399639.jpg', '0', 13, 51, 122, 152, '1', '2020-05-25 09:40:39', 10, '0');

-- --------------------------------------------------------

--
-- Table structure for table `wc_module`
--

CREATE TABLE `wc_module` (
  `module_id` int(11) NOT NULL,
  `module_name` varchar(255) NOT NULL,
  `status` enum('Active','Inactive') NOT NULL,
  `deleted` enum('0','1') NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `wc_module`
--

INSERT INTO `wc_module` (`module_id`, `module_name`, `status`, `deleted`) VALUES
(1, 'Accounts', 'Active', '0'),
(2, 'Brand', 'Active', '0'),
(3, 'Brief', 'Active', '0'),
(4, 'Proofing', 'Active', '0'),
(5, 'Files', 'Active', '0'),
(6, 'Feedback Form', 'Active', '0'),
(7, 'Reports', 'Active', '0'),
(8, 'Trello Integration', 'Active', '0'),
(9, 'Reporting', 'Active', '0'),
(10, 'Rules Creation', 'Active', '0'),
(11, 'Google drive Integration', 'Inactive', '0');

-- --------------------------------------------------------

--
-- Table structure for table `wc_module_permission`
--

CREATE TABLE `wc_module_permission` (
  `module_permission_id` int(11) NOT NULL,
  `module_id` int(11) NOT NULL,
  `user_type_id` int(11) NOT NULL,
  `access` int(11) NOT NULL,
  `upload` int(11) NOT NULL,
  `accept_reject` int(11) NOT NULL,
  `view` int(11) NOT NULL,
  `edit` int(11) NOT NULL,
  `approve_reject` int(11) NOT NULL,
  `add_revision` int(11) NOT NULL,
  `view_export` int(11) NOT NULL,
  `export` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `wc_module_permission`
--

INSERT INTO `wc_module_permission` (`module_permission_id`, `module_id`, `user_type_id`, `access`, `upload`, `accept_reject`, `view`, `edit`, `approve_reject`, `add_revision`, `view_export`, `export`) VALUES
(1, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0),
(2, 2, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0),
(3, 3, 1, 0, 1, 1, 0, 0, 0, 0, 0, 0),
(4, 4, 1, 0, 0, 0, 1, 1, 1, 1, 0, 0),
(5, 5, 1, 0, 1, 0, 1, 1, 0, 0, 0, 0),
(6, 6, 1, 0, 0, 0, 0, 0, 0, 0, 1, 0),
(7, 7, 1, 0, 0, 0, 1, 0, 0, 0, 0, 1),
(8, 8, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0),
(9, 9, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0),
(10, 10, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0),
(11, 11, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0),
(12, 1, 2, 0, 0, 0, 0, 0, 0, 0, 0, 0),
(13, 2, 2, 1, 0, 0, 0, 0, 0, 0, 0, 0),
(14, 3, 2, 0, 1, 1, 0, 0, 0, 0, 0, 0),
(15, 4, 2, 0, 0, 0, 1, 1, 1, 1, 0, 0),
(16, 5, 2, 0, 1, 0, 1, 1, 0, 0, 0, 0),
(17, 6, 2, 0, 0, 0, 0, 0, 0, 0, 1, 0),
(18, 7, 2, 0, 0, 0, 1, 0, 0, 0, 0, 1),
(19, 8, 2, 1, 0, 0, 0, 0, 0, 0, 0, 0),
(20, 9, 2, 1, 0, 0, 0, 0, 0, 0, 0, 0),
(21, 10, 2, 1, 0, 0, 0, 0, 0, 0, 0, 0),
(22, 11, 2, 0, 0, 0, 0, 0, 0, 0, 0, 0),
(23, 1, 3, 0, 0, 0, 0, 0, 0, 0, 0, 0),
(24, 2, 3, 1, 0, 0, 0, 0, 0, 0, 0, 0),
(25, 3, 3, 0, 0, 1, 0, 0, 0, 0, 0, 0),
(26, 4, 3, 0, 0, 0, 1, 0, 1, 0, 0, 0),
(27, 5, 3, 0, 1, 0, 1, 1, 0, 0, 0, 0),
(28, 6, 3, 0, 0, 0, 0, 0, 0, 0, 0, 0),
(29, 7, 3, 0, 0, 0, 0, 0, 0, 0, 0, 0),
(30, 8, 3, 1, 0, 0, 0, 0, 0, 0, 0, 0),
(31, 9, 3, 1, 0, 0, 0, 0, 0, 0, 0, 0),
(32, 10, 3, 1, 0, 0, 0, 0, 0, 0, 0, 0),
(33, 11, 3, 0, 0, 0, 0, 0, 0, 0, 0, 0),
(34, 1, 4, 0, 0, 0, 0, 0, 0, 0, 0, 0),
(35, 2, 4, 0, 0, 0, 0, 0, 0, 0, 0, 0),
(36, 3, 4, 0, 1, 0, 0, 0, 0, 0, 0, 0),
(37, 4, 4, 0, 0, 0, 1, 1, 0, 1, 0, 0),
(38, 5, 4, 0, 0, 0, 1, 0, 0, 0, 0, 0),
(39, 6, 4, 0, 0, 0, 0, 0, 0, 0, 0, 0),
(40, 7, 4, 0, 0, 0, 0, 0, 0, 0, 0, 0),
(41, 8, 4, 0, 0, 0, 0, 0, 0, 0, 0, 0),
(42, 9, 4, 0, 0, 0, 0, 0, 0, 0, 0, 0),
(43, 10, 4, 1, 0, 0, 0, 0, 0, 0, 0, 0),
(44, 11, 4, 0, 0, 0, 0, 0, 0, 0, 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `wc_module_permission_backup`
--

CREATE TABLE `wc_module_permission_backup` (
  `module_permission_id` int(11) NOT NULL,
  `module_id` int(11) NOT NULL,
  `user_type_id` int(11) NOT NULL,
  `access` int(11) NOT NULL,
  `upload` int(11) NOT NULL,
  `accept_reject` int(11) NOT NULL,
  `view` int(11) NOT NULL,
  `edit` int(11) NOT NULL,
  `approve_reject` int(11) NOT NULL,
  `add_revision` int(11) NOT NULL,
  `view_export` int(11) NOT NULL,
  `export` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `wc_module_permission_backup`
--

INSERT INTO `wc_module_permission_backup` (`module_permission_id`, `module_id`, `user_type_id`, `access`, `upload`, `accept_reject`, `view`, `edit`, `approve_reject`, `add_revision`, `view_export`, `export`) VALUES
(1, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0),
(2, 2, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0),
(3, 3, 1, 0, 1, 1, 0, 0, 0, 0, 0, 0),
(4, 4, 1, 0, 0, 0, 1, 1, 1, 1, 0, 0),
(5, 5, 1, 0, 1, 0, 1, 1, 0, 0, 0, 0),
(6, 6, 1, 0, 0, 0, 0, 0, 0, 0, 1, 0),
(7, 7, 1, 0, 0, 0, 1, 0, 0, 0, 0, 1),
(8, 8, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0),
(9, 9, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0),
(10, 10, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0),
(11, 11, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0),
(12, 1, 2, 1, 0, 0, 0, 0, 0, 0, 0, 0),
(13, 2, 2, 1, 0, 0, 0, 0, 0, 0, 0, 0),
(14, 3, 2, 0, 1, 1, 0, 0, 0, 0, 0, 0),
(15, 4, 2, 0, 0, 0, 1, 1, 1, 1, 0, 0),
(16, 5, 2, 0, 1, 0, 1, 1, 0, 0, 0, 0),
(17, 6, 2, 0, 0, 0, 0, 0, 0, 0, 1, 0),
(18, 7, 2, 0, 0, 0, 1, 0, 0, 0, 0, 1),
(19, 8, 2, 1, 0, 0, 0, 0, 0, 0, 0, 0),
(20, 9, 2, 1, 0, 0, 0, 0, 0, 0, 0, 0),
(21, 10, 2, 1, 0, 0, 0, 0, 0, 0, 0, 0),
(22, 11, 2, 1, 0, 0, 0, 0, 0, 0, 0, 0),
(23, 1, 3, 1, 0, 0, 0, 0, 0, 0, 0, 0),
(24, 2, 3, 1, 0, 0, 0, 0, 0, 0, 0, 0),
(25, 3, 3, 0, 0, 1, 0, 0, 0, 0, 0, 0),
(26, 4, 3, 0, 0, 0, 1, 0, 1, 0, 0, 0),
(27, 5, 3, 0, 1, 0, 1, 1, 0, 0, 0, 0),
(28, 6, 3, 0, 0, 0, 0, 0, 0, 0, 0, 0),
(29, 7, 3, 0, 0, 0, 0, 0, 0, 0, 0, 0),
(30, 8, 3, 1, 0, 0, 0, 0, 0, 0, 0, 0),
(31, 9, 3, 1, 0, 0, 0, 0, 0, 0, 0, 0),
(32, 10, 3, 1, 0, 0, 0, 0, 0, 0, 0, 0),
(33, 11, 3, 1, 0, 0, 0, 0, 0, 0, 0, 0),
(34, 1, 4, 1, 0, 0, 0, 0, 0, 0, 0, 0),
(35, 2, 4, 1, 0, 0, 0, 0, 0, 0, 0, 0),
(36, 3, 4, 0, 1, 0, 0, 0, 0, 0, 0, 0),
(37, 4, 4, 0, 0, 0, 1, 1, 0, 1, 0, 0),
(38, 5, 4, 0, 0, 0, 1, 0, 0, 0, 0, 0),
(39, 6, 4, 0, 0, 0, 0, 0, 0, 0, 0, 0),
(40, 7, 4, 0, 0, 0, 0, 0, 0, 0, 0, 0),
(41, 8, 4, 1, 0, 0, 0, 0, 0, 0, 0, 0),
(42, 9, 4, 1, 0, 0, 0, 0, 0, 0, 0, 0),
(43, 10, 4, 1, 0, 0, 0, 0, 0, 0, 0, 0),
(44, 11, 4, 1, 0, 0, 0, 0, 0, 0, 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `wc_page_content`
--

CREATE TABLE `wc_page_content` (
  `id` int(11) NOT NULL,
  `page_title` varchar(255) NOT NULL,
  `content` longtext NOT NULL,
  `pimg` varchar(255) NOT NULL,
  `updated_datetime` datetime NOT NULL,
  `status` int(50) NOT NULL,
  `deleted` enum('0','1') NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `wc_page_content`
--

INSERT INTO `wc_page_content` (`id`, `page_title`, `content`, `pimg`, `updated_datetime`, `status`, `deleted`) VALUES
(1, 'Add Project', '<p class=\"lead\">Your need to add project title and attach project doc before you proceed next.</p>\n', 'noimage.png', '2020-05-07 00:00:00', 0, '0'),
(2, 'Feed back', '<p ><strong>Help us improve. Rate your Experience:</strong></p>\n <p ><br />\nThank you for using Wecreate studio. Please let us know your overall experience by filling a short feedback Below:</p>\n', 'noimage.png', '2020-05-17 00:00:00', 0, '0'),
(4, 'Upload your Zip file containing the images', '<p>Your Zip file should contain a list of folders. Each folder name be the SKU reference. Each folder can contain one or multiple images(JPG, PNG) associated to the SKU. Check our <em><b><a class=\"text-dark\" download=\"\" href=\"website-assets/pdf/Image_Upload_Guidelines.pdf\" target=\"_blank\">guidelines</a></b></em>.</p>\n\n<p><small class=\"text-danger\">* Images should be vertical.</small></p>\n', 'noimage.png', '2020-01-09 00:00:00', 0, '0'),
(5, 'Upload Single SKU', '<p>Supports only JPG. Max. 10 files per upload. Check our <em><b><a class=\"text-dark\" download=\"\" href=\"website-assets/pdf/Image_Upload_Guidelines.pdf\" target=\"_blank\">guidelines</a></b></em> for more details.</p>\n\n<p><small class=\"text-danger\">* Images should be vertical.</small></p>\n', 'noimage.png', '2020-01-16 00:00:00', 0, '0'),
(6, 'Drag SKU Folder', '<p>Your folder should contain either a single folder or a list of folders. Each folder name be the SKU reference. Each folder can contain one or multiple images(JPG, PNG) associated to the SKU. Check our <em><b><a class=\"text-dark\" download=\"\" href=\"website-assets/pdf/Image_Upload_Guidelines.pdf\" target=\"_blank\">guidelines</a></b></em>.</p>\n\n<p><small class=\"text-danger\">* Images should be vertical.</small></p>', 'noimage.png', '2020-01-16 00:00:00', 0, '0'),
(7, 'Upload Images', '<p>dfasd</p>\n', 'noimage.png', '2020-01-09 00:00:00', 1, '1'),
(8, 'testing', '<p><strong>Lorem Ipsum</strong> is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry&#39;s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum</p>\n <table border=\"1\" cellpadding=\"1\" cellspacing=\"1\" >\n <tbody>\n  <tr>\n   <td> </td>\n   <td> </td>\n  </tr>\n  <tr>\n   <td> </td>\n   <td> </td>\n  </tr>\n  <tr>\n   <td> </td>\n   <td> </td>\n  </tr>\n </tbody>\n</table>\n\n<p> </p>\n', '1581033189test.php', '2020-02-07 00:00:00', 0, '1'),
(9, 'testing', '<p><strong>Lorem Ipsum</strong> is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry&#39;s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum</p>\n <table border=\"1\" cellpadding=\"1\" cellspacing=\"1\" >\n <tbody>\n  <tr>\n   <td> </td>\n   <td> </td>\n  </tr>\n  <tr>\n   <td> </td>\n   <td> </td>\n  </tr>\n  <tr>\n   <td> </td>\n   <td> </td>\n  </tr>\n </tbody>\n</table>\n\n<p> </p>\n', '1581033189test.php', '2020-02-07 00:00:00', 0, '1'),
(10, 'About Us', '<p>About Us </p>\n', 'noimage.png', '2020-02-25 00:00:00', 0, '0'),
(11, 'Contact Us', '', 'noimage.png', '2020-03-04 00:00:00', 0, '0'),
(12, 'Return Policy', '<p>Return Policy</p>\n', 'noimage.png', '2020-02-25 00:00:00', 0, '0'),
(13, 'Refund Policy', '<p>Refund Policy</p>\n', 'noimage.png', '2020-02-25 00:00:00', 0, '0'),
(14, 'Cancellation Policy', '<p>Cancellation Policy</p>\n', 'noimage.png', '2020-02-25 00:00:00', 0, '0'),
(15, 'Disclaimer', '<p>Disclaimer</p>\n', 'noimage.png', '2020-02-25 00:00:00', 0, '0'),
(16, 'Intellectual Property', '<p>Intellectual Property</p>\n', 'noimage.png', '2020-02-25 00:00:00', 0, '0'),
(17, 'Payments and logistic', '<p>Payments and logistic</p>\n', 'noimage.png', '2020-02-25 00:00:00', 0, '0'),
(18, 'Privacy Policy', '<p>Privacy Policy</p>\n', 'noimage.png', '2020-02-25 00:00:00', 0, '0'),
(19, 'Terms & Conditions', '<p>Terms & Conditions</p>\n', 'noimage.png', '2020-02-25 00:00:00', 0, '0'),
(20, 'Cookies Policy', '<p>Cookies Policy</p>\n', 'noimage.png', '2020-02-25 00:00:00', 0, '0');

-- --------------------------------------------------------

--
-- Table structure for table `wc_product_price`
--

CREATE TABLE `wc_product_price` (
  `product_id` int(11) NOT NULL,
  `product_name` varchar(255) NOT NULL,
  `product_dest` text NOT NULL,
  `product_price` varchar(255) NOT NULL,
  `status` varchar(255) NOT NULL,
  `deleted` enum('0','1') NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `wc_product_price`
--

INSERT INTO `wc_product_price` (`product_id`, `product_name`, `product_dest`, `product_price`, `status`, `deleted`) VALUES
(1, 'Post', 'All social media post', '4320', 'Active', ''),
(2, 'Master creative', 'Ads, Packaging, POSM', '9600', 'Active', '0'),
(3, 'Adaptations', 'Only Still Images', '480', 'Active', ''),
(16, 'Videos', 'Digital film, only adaptations, Resizing and Moodboard Creation)', '18000', 'Active', ''),
(17, 'Production Management', 'Moodboard Creation and Management of project', '12215', 'Active', ''),
(18, 'Web Page Design', ' Wireframe, A  content', '18000', 'Active', ''),
(19, 'Text Ads', 'Text Ads', '480', 'Active', ''),
(20, 'Carousels', 'Carousels', '4800', 'Active', ''),
(21, 'Cover Pictures, Thumbnails', 'Cover Pictures, Thumbnails', '720', 'Active', '');

-- --------------------------------------------------------

--
-- Table structure for table `wc_revision`
--

CREATE TABLE `wc_revision` (
  `revision_id` int(11) NOT NULL,
  `brief_id` int(11) NOT NULL,
  `image_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `revision_name` varchar(50) NOT NULL,
  `revised_file` varchar(100) NOT NULL,
  `revised_status` text NOT NULL,
  `revision_datetime` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `wc_revision`
--

INSERT INTO `wc_revision` (`revision_id`, `brief_id`, `image_id`, `user_id`, `revision_name`, `revised_file`, `revised_status`, `revision_datetime`) VALUES
(1, 1, 1, 2, 'Revision1', '', '0', '0000-00-00 00:00:00'),
(2, 3, 2, 2, 'Revision1', '', '0', '0000-00-00 00:00:00'),
(3, 3, 3, 2, 'Revision1', '', '0', '0000-00-00 00:00:00'),
(4, 3, 4, 2, 'Revision1', '', '0', '0000-00-00 00:00:00'),
(5, 5, 8, 2, 'Revision1', '', '0', '0000-00-00 00:00:00'),
(6, 5, 9, 2, 'Revision1', '', '0', '0000-00-00 00:00:00'),
(7, 5, 10, 2, 'Revision1', '', '0', '0000-00-00 00:00:00'),
(8, 7, 13, 2, 'Revision1', '', '0', '0000-00-00 00:00:00'),
(9, 7, 14, 2, 'Revision1', '', '0', '0000-00-00 00:00:00'),
(10, 8, 15, 2, 'Revision1', '', '0', '0000-00-00 00:00:00'),
(11, 8, 16, 2, 'Revision1', '', '0', '0000-00-00 00:00:00'),
(12, 8, 17, 2, 'Revision1', '', '0', '0000-00-00 00:00:00'),
(13, 11, 19, 5, 'Revision1', '', '0', '0000-00-00 00:00:00'),
(14, 11, 20, 5, 'Revision1', '', '0', '0000-00-00 00:00:00'),
(15, 11, 21, 5, 'Revision1', '', '0', '0000-00-00 00:00:00'),
(16, 12, 24, 9, 'Revision1', '', '0', '0000-00-00 00:00:00'),
(17, 12, 25, 9, 'Revision1', '', '0', '0000-00-00 00:00:00'),
(18, 12, 26, 9, 'Revision1', '', '0', '0000-00-00 00:00:00'),
(19, 13, 27, 5, 'Revision1', '', '0', '0000-00-00 00:00:00'),
(20, 15, 29, 7, 'Revision1', '', '0', '0000-00-00 00:00:00'),
(21, 15, 30, 7, 'Revision1', '', '0', '0000-00-00 00:00:00'),
(22, 17, 31, 7, 'Revision1', '', '0', '0000-00-00 00:00:00'),
(23, 17, 32, 7, 'Revision1', '', '0', '0000-00-00 00:00:00'),
(24, 17, 33, 7, 'Revision1', '', '0', '0000-00-00 00:00:00'),
(25, 18, 34, 9, 'Revision1', '', '0', '0000-00-00 00:00:00'),
(26, 18, 35, 9, 'Revision1', '', '0', '0000-00-00 00:00:00'),
(27, 0, 36, 9, 'Revision1', '', '0', '0000-00-00 00:00:00'),
(28, 18, 37, 9, 'Revision1', '', '0', '0000-00-00 00:00:00'),
(29, 0, 38, 2, 'Revision1', '', '0', '0000-00-00 00:00:00'),
(30, 19, 39, 5, 'Revision1', '', '0', '0000-00-00 00:00:00'),
(31, 20, 42, 5, 'Revision1', '', '0', '0000-00-00 00:00:00'),
(32, 20, 43, 5, 'Revision1', '', '0', '0000-00-00 00:00:00'),
(33, 8, 44, 11, 'Revision1', '', '0', '0000-00-00 00:00:00'),
(34, 21, 45, 10, 'Revision1', '', '0', '0000-00-00 00:00:00'),
(35, 21, 46, 10, 'Revision1', '', '0', '0000-00-00 00:00:00'),
(36, 21, 47, 10, 'Revision1', '', '0', '0000-00-00 00:00:00'),
(37, 22, 49, 5, 'Revision1', '', '0', '0000-00-00 00:00:00'),
(38, 23, 50, 10, 'Revision1', '', '0', '0000-00-00 00:00:00'),
(39, 7, 51, 11, 'Revision1', '', '0', '0000-00-00 00:00:00'),
(40, 24, 53, 2, 'Revision1', '', '0', '0000-00-00 00:00:00'),
(41, 24, 54, 2, 'Revision1', '', '0', '0000-00-00 00:00:00'),
(42, 24, 55, 2, 'Revision1', '', '0', '0000-00-00 00:00:00'),
(43, 24, 56, 2, 'Revision1', '', '0', '0000-00-00 00:00:00'),
(44, 25, 58, 2, 'Revision1', '', '0', '0000-00-00 00:00:00'),
(45, 25, 59, 2, 'Revision1', '', '0', '0000-00-00 00:00:00'),
(46, 26, 60, 5, 'Revision1', '', '0', '0000-00-00 00:00:00'),
(47, 25, 61, 2, 'Revision1', '', '0', '0000-00-00 00:00:00'),
(48, 31, 64, 10, 'Revision1', '', '0', '0000-00-00 00:00:00'),
(49, 31, 65, 10, 'Revision1', '', '0', '0000-00-00 00:00:00'),
(50, 4, 68, 2, 'Revision1', '', '0', '0000-00-00 00:00:00'),
(51, 4, 69, 2, 'Revision1', '', '0', '0000-00-00 00:00:00'),
(52, 29, 70, 2, 'Revision1', '', '0', '0000-00-00 00:00:00'),
(53, 29, 71, 2, 'Revision1', '', '0', '0000-00-00 00:00:00'),
(54, 29, 72, 2, 'Revision1', '', '0', '0000-00-00 00:00:00'),
(55, 34, 73, 2, 'Revision1', '', '0', '0000-00-00 00:00:00'),
(56, 34, 74, 2, 'Revision1', '', '0', '0000-00-00 00:00:00'),
(57, 34, 75, 2, 'Revision1', '', '0', '0000-00-00 00:00:00'),
(58, 35, 77, 15, 'Revision1', '', '0', '0000-00-00 00:00:00'),
(59, 35, 78, 15, 'Revision1', '', '0', '0000-00-00 00:00:00'),
(60, 37, 79, 5, 'Revision1', '', '0', '0000-00-00 00:00:00'),
(61, 37, 80, 5, 'Revision1', '', '0', '0000-00-00 00:00:00'),
(62, 36, 81, 2, 'Revision1', '', '0', '0000-00-00 00:00:00'),
(63, 36, 82, 2, 'Revision1', '', '0', '0000-00-00 00:00:00'),
(64, 36, 83, 2, 'Revision1', '', '0', '0000-00-00 00:00:00'),
(65, 37, 84, 5, 'Revision1', '', '0', '0000-00-00 00:00:00'),
(66, 38, 85, 5, 'Revision1', '', '0', '0000-00-00 00:00:00'),
(67, 38, 87, 5, 'Revision1', '', '0', '0000-00-00 00:00:00'),
(68, 39, 88, 2, 'Revision1', '', '0', '0000-00-00 00:00:00'),
(69, 39, 89, 2, 'Revision1', '', '0', '0000-00-00 00:00:00'),
(70, 39, 90, 2, 'Revision1', '', '0', '0000-00-00 00:00:00'),
(71, 32, 91, 2, 'Revision1', '', '0', '0000-00-00 00:00:00'),
(72, 32, 92, 2, 'Revision1', '', '0', '0000-00-00 00:00:00'),
(73, 32, 93, 2, 'Revision1', '', '0', '0000-00-00 00:00:00'),
(74, 32, 94, 2, 'Revision1', '', '0', '0000-00-00 00:00:00'),
(75, 40, 95, 5, 'Revision1', '', '0', '0000-00-00 00:00:00'),
(76, 41, 96, 21, 'Revision1', '', '0', '0000-00-00 00:00:00'),
(77, 41, 97, 21, 'Revision1', '', '0', '0000-00-00 00:00:00'),
(78, 41, 98, 21, 'Revision1', '', '0', '0000-00-00 00:00:00'),
(79, 42, 100, 5, 'Revision1', '', '0', '0000-00-00 00:00:00'),
(80, 42, 101, 5, 'Revision1', '', '0', '0000-00-00 00:00:00'),
(81, 44, 102, 25, 'Revision1', '', '0', '0000-00-00 00:00:00'),
(82, 44, 103, 25, 'Revision1', '', '0', '0000-00-00 00:00:00'),
(83, 44, 104, 25, 'Revision1', '', '0', '0000-00-00 00:00:00'),
(84, 44, 105, 25, 'Revision1', '', '0', '0000-00-00 00:00:00'),
(85, 46, 108, 19, 'Revision1', '', '0', '0000-00-00 00:00:00'),
(86, 46, 109, 19, 'Revision1', '', '0', '0000-00-00 00:00:00'),
(87, 47, 112, 25, 'Revision1', '', '0', '0000-00-00 00:00:00'),
(88, 47, 113, 25, 'Revision1', '', '0', '0000-00-00 00:00:00'),
(89, 47, 114, 25, 'Revision1', '', '0', '0000-00-00 00:00:00'),
(90, 47, 115, 25, 'Revision1', '', '0', '0000-00-00 00:00:00'),
(91, 47, 116, 25, 'Revision1', '', '0', '0000-00-00 00:00:00'),
(92, 47, 117, 25, 'Revision1', '', '0', '0000-00-00 00:00:00'),
(93, 47, 118, 25, 'Revision1', '', '0', '0000-00-00 00:00:00'),
(94, 47, 119, 25, 'Revision1', '', '0', '0000-00-00 00:00:00'),
(95, 48, 120, 5, 'Revision1', '', '0', '0000-00-00 00:00:00'),
(96, 48, 121, 5, 'Revision1', '', '0', '0000-00-00 00:00:00'),
(97, 48, 122, 5, 'Revision1', '', '0', '0000-00-00 00:00:00'),
(98, 48, 123, 5, 'Revision1', '', '0', '0000-00-00 00:00:00'),
(99, 48, 124, 5, 'Revision1', '', '0', '0000-00-00 00:00:00'),
(100, 47, 127, 25, 'Revision1', '', '0', '0000-00-00 00:00:00'),
(101, 52, 129, 5, 'Revision1', '', '0', '0000-00-00 00:00:00'),
(102, 52, 130, 5, 'Revision1', '', '0', '0000-00-00 00:00:00'),
(103, 52, 131, 5, 'Revision1', '', '0', '0000-00-00 00:00:00'),
(104, 52, 132, 5, 'Revision1', '', '0', '0000-00-00 00:00:00'),
(105, 52, 134, 5, 'Revision1', '', '0', '0000-00-00 00:00:00'),
(106, 53, 135, 5, 'Revision1', '', '0', '0000-00-00 00:00:00'),
(107, 53, 136, 5, 'Revision1', '', '0', '0000-00-00 00:00:00'),
(108, 53, 137, 5, 'Revision1', '', '0', '0000-00-00 00:00:00'),
(109, 53, 138, 5, 'Revision1', '', '0', '0000-00-00 00:00:00'),
(110, 53, 139, 5, 'Revision1', '', '0', '0000-00-00 00:00:00'),
(111, 51, 140, 10, 'Revision1', '', '0', '0000-00-00 00:00:00'),
(112, 55, 141, 21, 'Revision1', '', '0', '0000-00-00 00:00:00'),
(113, 0, 142, 21, 'Revision1', '', '0', '0000-00-00 00:00:00'),
(114, 55, 143, 21, 'Revision1', '', '0', '0000-00-00 00:00:00'),
(115, 55, 144, 21, 'Revision1', '', '0', '0000-00-00 00:00:00'),
(116, 55, 145, 21, 'Revision1', '', '0', '0000-00-00 00:00:00'),
(117, 53, 147, 5, 'Revision1', '', '0', '0000-00-00 00:00:00'),
(118, 53, 148, 5, 'Revision1', '', '0', '0000-00-00 00:00:00'),
(119, 53, 149, 5, 'Revision1', '', '0', '0000-00-00 00:00:00'),
(120, 53, 150, 5, 'Revision1', '', '0', '0000-00-00 00:00:00'),
(121, 51, 151, 10, 'Revision1', '', '0', '0000-00-00 00:00:00'),
(122, 51, 152, 10, 'Revision1', '', '0', '0000-00-00 00:00:00'),
(123, 49, 153, 10, 'Revision1', '', '0', '0000-00-00 00:00:00'),
(124, 49, 154, 10, 'Revision1', '', '0', '0000-00-00 00:00:00'),
(125, 49, 155, 10, 'Revision1', '', '0', '0000-00-00 00:00:00'),
(126, 49, 156, 10, 'Revision1', '', '0', '0000-00-00 00:00:00'),
(127, 49, 157, 10, 'Revision1', '', '0', '0000-00-00 00:00:00'),
(128, 49, 158, 10, 'Revision1', '', '0', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `wc_settings`
--

CREATE TABLE `wc_settings` (
  `setting_id` int(11) NOT NULL,
  `setting_key` text NOT NULL,
  `setting_value` text NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `wc_settings`
--

INSERT INTO `wc_settings` (`setting_id`, `setting_key`, `setting_value`) VALUES
(1, 'MAIL_SMTP_USER', 'collab@piquic.com'),
(2, 'MAIL_SMTP_PASSWORD', 'AaAa2121#@!'),
(3, 'MAIL_BODY_PASSWORD', '<link href=\'https://fonts.googleapis.com/css?family=Asap:400,400italic,700,700italic\' rel=\'stylesheet\' type=\'text/css\'>\r\n\r\n<table align=\"center\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" height=\"100%\" id=\"bodyTable\" style=\"border-collapse: collapse; mso-table-lspace: 0; mso-table-rspace: 0; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%; background-color: #fff; height: 100%; margin: 0; padding: 0; width: 100%\" width=\"100%;\">\r\n	<tr>\r\n		<td align=\"center\" id=\"bodyCell\" style=\"mso-line-height-rule: exactly;-ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%; border-top: 0;height: 100%; margin: 0; padding: 0; width: 100%;\" valign=\"top\">\r\n			<table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" class=\"templateContainer\" style=\"border-collapse: collapse; mso-table-lspace: 0; mso-table-rspace: 0;  box-shadow: 8px 13px 44px 0px #000000; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%; max-width: 600px; border: 0\" width=\"100%\">\r\n				<tr>\r\n					<td id=\"templatePreheader\" style=\"mso-line-height-rule: exactly; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%; background-color: #ffffff; border-top: 0; border-bottom: 1px solid #76aea169; padding-top: 16px; padding-bottom: 8px\" valign=\"top\">\r\n						<table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" class=\"mcnTextBlock\" style=\"border-collapse: collapse; mso-table-lspace: 0; mso-table-rspace: 0; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%; min-width:100%;\" width=\"100%\">\r\n							<tbody class=\"mcnTextBlockOuter\">\r\n								<tr>\r\n									<td class=\"mcnTextBlockInner\" style=\"mso-line-height-rule: exactly; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%\" valign=\"top\">\r\n										<table align=\"left\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" class=\"mcnTextContentContainer\" style=\"border-collapse: collapse; mso-table-lspace: 0; mso-table-rspace: 0; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%; min-width:100%;\" width=\"100%\">\r\n											<tbody>\r\n												<tr>\r\n													<td class=\"mcnTextContent\" style=\'mso-line-height-rule: exactly; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%; word-break: break-word; color: #2a2a2a; font-family: \"Asap\", Helvetica, sans-serif; font-size: 12px; line-height: 150%; text-align: center; padding-top:9px; padding-right: 18px; padding-bottom: 9px; padding-left: 18px;\' valign=\"top\">\r\n\r\n														<img align=\"none\" alt=\"Piquic\" height=\"52\" src=\"##WEBSITE_URL##/website-assets/images/logo.png\" style=\"-ms-interpolation-mode: bicubic; border: 0; outline: none; text-decoration: none; height: auto; width: 200px;  margin: 0px;\"  />\r\n													</td>\r\n												</tr>\r\n											</tbody>\r\n										</table>\r\n									</td>\r\n								</tr>\r\n							</tbody>\r\n						</table>\r\n					</td>\r\n				</tr>\r\n\r\n				<tr>\r\n					<td id=\"templateBody\" style=\"mso-line-height-rule: exactly; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%; background-color: #ffffff; border-top: 0; border-bottom: 0; padding-top: 0; padding-bottom: 20px\" valign=\"top\">\r\n\r\n						<table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" class=\"mcnButtonBlock\" style=\"border-collapse: collapse; mso-table-lspace: 0; mso-table-rspace: 0; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%; min-width:100%;\" width=\"100%\">\r\n							<tbody class=\"mcnButtonBlockOuter\">\r\n								<tr>\r\n									<td align=\"center\" class=\"mcnButtonBlockInner\" style=\"mso-line-height-rule: exactly; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%; padding-top:18px; padding-right:18px; padding-bottom:18px; padding-left:18px;\" valign=\"top\">\r\n\r\n										<table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" class=\"mcnButtonBlock\" style=\"border-collapse: collapse; mso-table-lspace: 0; mso-table-rspace: 0; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%; min-width:100%;\" width=\"100%\">\r\n											<tbody class=\"mcnButtonBlockOuter\">\r\n												<tr>\r\n													<td align=\"center\" class=\"mcnButtonBlockInner\" style=\"mso-line-height-rule: exactly; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%; padding-top:0; padding-right:18px; padding-bottom:18px; padding-left:18px;\" valign=\"top\">\r\n														<table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" class=\"mcnButtonContentContainer\" style=\"border-collapse: collapse; mso-table-lspace: 0; mso-table-rspace: 0; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%; border-collapse: separate !important;border-radius: 48px;\">\r\n															<tbody>\r\n																<tr>\r\n																	<td align=\"center\" class=\"mcnButtonContent\" style=\"mso-line-height-rule: exactly; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%; font-family: \'Asap\', Helvetica, sans-serif; font-size: 16px; padding-top:24px; padding-right:48px; padding-bottom:24px; padding-left:48px;\" valign=\"middle\">\r\n\r\n																		<p class=\"null\" style=\'color: #2a2a2a; font-family: \"Asap\", Helvetica, sans-serif; font-size: 20px; font-style: normal; line-height: 125%; letter-spacing: 1px; text-align: center; display: block; margin: 0; padding: 0 0 40px 0\'>\r\n																			Not to worry, we got you! Let’s get you a new password. \r\n																			                                    \r\n																		</p>\r\n\r\n																		<p class=\"null\" style=\'color: #2a2a2a; font-family: \"Asap\", Helvetica, sans-serif; font-size: 24px; font-style: normal; line-height: 125%; letter-spacing: 1px; text-align: center; display: block; margin: 0; padding: 0 0 40px 0; text-decoration: none;\'>\r\n																			##USER_EMAIL##\r\n																		</p>\r\n\r\n																		<p class=\"null\" style=\'color: #2a2a2a; font-family: \"Asap\", Helvetica, sans-serif; font-size: 16px; font-style: normal; line-height: 125%; letter-spacing: 1px; text-align: center; display: block; margin: 0; padding: 0 0 40px 0\'>\r\n																			<b>Note: </b> After 3 hours this will link will be expired.  \r\n																		</p>\r\n\r\n																		<p class=\"null\" style=\'color: #2a2a2a; font-family: \"Asap\", Helvetica, sans-serif; font-size: 12px; font-style: normal; line-height: 125%; letter-spacing: 1px; text-align: center; display: block; margin: 0; padding: 0\'>\r\n																			If you didn\'t mean to reset your password, then you can just ignore this email, your password will not change.\r\n																		</p>\r\n\r\n\r\n																		\r\n\r\n																																			\r\n																	</td>\r\n																</tr>\r\n															</tbody>\r\n														</table>\r\n													</td>\r\n												</tr>\r\n											</tbody>\r\n										</table>\r\n									</td>\r\n								</tr>\r\n							</tbody>\r\n						</table>\r\n\r\n						<table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" class=\"mcnButtonBlock\" style=\"border-collapse: collapse; mso-table-lspace: 0; mso-table-rspace: 0; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%; min-width:100%;\" width=\"100%\">\r\n							<tbody class=\"mcnButtonBlockOuter\">\r\n								<tr>\r\n									<td align=\"center\" class=\"mcnButtonBlockInner\" style=\"mso-line-height-rule: exactly; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%; padding: 0 50px;\" valign=\"top\">\r\n\r\n										<table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" class=\"mcnButtonBlock\" style=\"border-collapse: collapse; mso-table-lspace: 0; mso-table-rspace: 0; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%; min-width:100%; background-color: #5bad9a;\" width=\"100%\">\r\n											<tbody class=\"mcnButtonBlockOuter\">\r\n												\r\n												<tr>\r\n													<td align=\"center\" class=\"mcnButtonContent\" style=\"mso-line-height-rule: exactly; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%; font-family: \'Asap\', Helvetica, sans-serif; font-size: 16px; padding: 24px;\" valign=\"middle\">\r\n\r\n														<a class=\"mcnButton \" href=\"##URL##\" style=\"mso-line-height-rule: exactly; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%; display: block; text-decoration: none; letter-spacing: 1px; line-height: 100%; text-align: center; text-decoration: none; color: #ffffff; text-transform: uppercase;\" target=\"_blank\" title=\"Reset password\">Reset password</a>\r\n\r\n														 \r\n													</td>\r\n\r\n												</tr>\r\n											</tbody>\r\n										</table>\r\n									</td>\r\n								</tr>\r\n							</tbody>\r\n						</table>\r\n\r\n					</td>\r\n				</tr>\r\n\r\n				<tr>\r\n					<td id=\"templatePreheader\" style=\"mso-line-height-rule: exactly; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%; background-color: #ffffff; border-bottom: 0; border-top: 1px solid #76aea169; padding-top: 16px; padding-bottom: 8px\" valign=\"top\">\r\n						<table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" class=\"mcnTextBlock\" style=\"border-collapse: collapse; mso-table-lspace: 0; mso-table-rspace: 0; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%; min-width:100%;\" width=\"100%\">\r\n							<tbody class=\"mcnTextBlockOuter\">\r\n								<tr>\r\n									\r\n									<td class=\"mcnTextContent\" style=\'mso-line-height-rule: exactly; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%; word-break: break-word; color: #2a2a2a; font-family: \"Asap\", Helvetica, sans-serif; font-size: 12px; line-height: 150%; text-align: center; padding-top:2px; padding-bottom: 2px;\' valign=\"top\">\r\n\r\n										<p class=\"null\" style=\'color: #2a2a2a; font-family: \"Asap\", Helvetica, sans-serif; font-size: 12px; font-style: normal; line-height: 125%; letter-spacing: 1px; text-align: center; display: block; margin: 0; padding: 0\'>\r\n\r\n											Learn More About <a class=\"mcnButton \" href=\"#\" style=\"text-decoration: none; letter-spacing: 1px; line-height: 100%; text-align: center; color: #5bad9a; text-transform: uppercase;\" target=\"_blank\" title=\"##COPY_RIGHT##\">##COPY_RIGHT##</a>\r\n										</p>\r\n\r\n										<p class=\"null\" style=\'color: #2a2a2a; font-family: \"Asap\", Helvetica, sans-serif; font-size: 12px; font-style: normal; line-height: 125%; letter-spacing: 1px; text-align: center; display: block; margin: 0; padding: 10px 0;\'>\r\n\r\n											<a class=\"mcnButton \" href=\"#\" style=\"text-decoration: none; letter-spacing: 1px; line-height: 100%; text-align: center; color: #5bad9a; text-transform: uppercase;\" target=\"_blank\" title=\"About Piquic\">About Piquic</a><span style=\"font-weight: bolder;\"> &middot; </span><a class=\"mcnButton \" href=\"#\" style=\"text-decoration: none; letter-spacing: 1px; line-height: 100%; text-align: center; color: #5bad9a; text-transform: uppercase;\" target=\"_blank\" title=\"Help\">Help</a><span style=\"font-weight: bolder;\"> &middot; </span><a class=\"mcnButton \" href=\"#\" style=\"text-decoration: none; letter-spacing: 1px; line-height: 100%; text-align: center; color: #5bad9a; text-transform: uppercase;\" target=\"_blank\" title=\"Legal\">Legal</a><span style=\"font-weight: bolder;\"> &middot; </span><a class=\"mcnButton \" href=\"#\" style=\"text-decoration: none; letter-spacing: 1px; line-height: 100%; text-align: center; color: #5bad9a; text-transform: uppercase;\" target=\"_blank\" title=\"Report Spam\">Report Spam</a>\r\n										</p>\r\n									</td>\r\n								</tr>\r\n							</tbody>\r\n						</table>\r\n					</td>\r\n				</tr>\r\n			</table>\r\n		</td>\r\n	</tr>\r\n</table>'),
(4, 'MAIL_BODY_PASSWORD_SUCCESS', '<link href=\'https://fonts.googleapis.com/css?family=Asap:400,400italic,700,700italic\' rel=\'stylesheet\' type=\'text/css\'>\r\n\r\n<table align=\"center\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" height=\"100%\" id=\"bodyTable\" style=\"border-collapse: collapse; mso-table-lspace: 0; mso-table-rspace: 0; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%; background-color: #fff; height: 100%; margin: 0; padding: 0; width: 100%\" width=\"100%;\">\r\n	<tr>\r\n		<td align=\"center\" id=\"bodyCell\" style=\"mso-line-height-rule: exactly;-ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%; border-top: 0;height: 100%; margin: 0; padding: 0; width: 100%;\" valign=\"top\">\r\n			<table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" class=\"templateContainer\" style=\"border-collapse: collapse; mso-table-lspace: 0; mso-table-rspace: 0;  box-shadow: 8px 13px 44px 0px #000000; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%; max-width: 600px; border: 0\" width=\"100%\">\r\n				<tr>\r\n					<td id=\"templatePreheader\" style=\"mso-line-height-rule: exactly; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%; background-color: #ffffff; border-top: 0; border-bottom: 1px solid #76aea169; padding-top: 16px; padding-bottom: 8px\" valign=\"top\">\r\n						<table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" class=\"mcnTextBlock\" style=\"border-collapse: collapse; mso-table-lspace: 0; mso-table-rspace: 0; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%; min-width:100%;\" width=\"100%\">\r\n							<tbody class=\"mcnTextBlockOuter\">\r\n								<tr>\r\n									<td class=\"mcnTextBlockInner\" style=\"mso-line-height-rule: exactly; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%\" valign=\"top\">\r\n										<table align=\"left\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" class=\"mcnTextContentContainer\" style=\"border-collapse: collapse; mso-table-lspace: 0; mso-table-rspace: 0; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%; min-width:100%;\" width=\"100%\">\r\n											<tbody>\r\n												<tr>\r\n													<td class=\"mcnTextContent\" style=\'mso-line-height-rule: exactly; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%; word-break: break-word; color: #2a2a2a; font-family: \"Asap\", Helvetica, sans-serif; font-size: 12px; line-height: 150%; text-align: center; padding-top:9px; padding-right: 18px; padding-bottom: 9px; padding-left: 18px;\' valign=\"top\">\r\n\r\n														<img align=\"none\" alt=\"Piquic\" height=\"52\" src=\"##WEBSITE_URL##/website-assets/images/logo.png\" style=\"-ms-interpolation-mode: bicubic; border: 0; outline: none; text-decoration: none; height: auto; width: 200px;  margin: 0px;\"  />\r\n													</td>\r\n												</tr>\r\n											</tbody>\r\n										</table>\r\n									</td>\r\n								</tr>\r\n							</tbody>\r\n						</table>\r\n					</td>\r\n				</tr>\r\n\r\n				<tr>\r\n					<td id=\"templateBody\" style=\"mso-line-height-rule: exactly; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%; background-color: #ffffff; border-top: 0; border-bottom: 0; padding-top: 0; padding-bottom: 20px\" valign=\"top\">\r\n\r\n						<table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" class=\"mcnButtonBlock\" style=\"border-collapse: collapse; mso-table-lspace: 0; mso-table-rspace: 0; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%; min-width:100%;\" width=\"100%\">\r\n							<tbody class=\"mcnButtonBlockOuter\">\r\n								<tr>\r\n									<td align=\"center\" class=\"mcnButtonBlockInner\" style=\"mso-line-height-rule: exactly; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%; padding-top:18px; padding-right:18px; padding-bottom:18px; padding-left:18px;\" valign=\"top\">\r\n\r\n										<table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" class=\"mcnButtonBlock\" style=\"border-collapse: collapse; mso-table-lspace: 0; mso-table-rspace: 0; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%; min-width:100%;\" width=\"100%\">\r\n											<tbody class=\"mcnButtonBlockOuter\">\r\n												<tr>\r\n													<td align=\"center\" class=\"mcnButtonBlockInner\" style=\"mso-line-height-rule: exactly; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%; padding-top:0; padding-right:18px; padding-bottom:18px; padding-left:18px;\" valign=\"top\">\r\n														<table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" class=\"mcnButtonContentContainer\" style=\"border-collapse: collapse; mso-table-lspace: 0; mso-table-rspace: 0; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%; border-collapse: separate !important;border-radius: 48px;\">\r\n															<tbody>\r\n																<tr>\r\n																	<td align=\"center\" class=\"mcnButtonContent\" style=\"mso-line-height-rule: exactly; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%; font-family: \'Asap\', Helvetica, sans-serif; font-size: 16px; padding-top:24px; padding-right:48px; padding-bottom:24px; padding-left:48px;\" valign=\"middle\">\r\n\r\n																		<p class=\"null\" style=\'color: #2a2a2a; font-family: \"Asap\", Helvetica, sans-serif; font-size: 24px; font-style: normal; line-height: 125%; letter-spacing: 1px; text-align: center; display: block; margin: 0; padding: 0 0 40px 0\'>\r\n																			Congratulations!! ##USER_NAME##\r\n																		</p>\r\n\r\n																		<p class=\"null\" style=\'color: #2a2a2a; font-family: \"Asap\", Helvetica, sans-serif; font-size: 20px; font-style: normal; line-height: 125%; letter-spacing: 1px; text-align: center; display: block; margin: 0; padding: 0 0 40px 0\'>\r\n																			You have successfully updated your password\r\n																		</p>\r\n\r\n																		<p class=\"null\" style=\'color: #2a2a2a; font-family: \"Asap\", Helvetica, sans-serif; font-size: 16px; font-style: normal; line-height: 125%; letter-spacing: 1px; text-align: center; display: block; margin: 0; padding: 0 0 40px 0\'>\r\n																			If not you, Please login and reset the password immidiatly.                                    \r\n																		</p>\r\n																																			\r\n																	</td>\r\n																</tr>\r\n															</tbody>\r\n														</table>\r\n													</td>\r\n												</tr>\r\n											</tbody>\r\n										</table>\r\n									</td>\r\n								</tr>\r\n							</tbody>\r\n						</table>\r\n\r\n						<table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" class=\"mcnButtonBlock\" style=\"border-collapse: collapse; mso-table-lspace: 0; mso-table-rspace: 0; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%; min-width:100%;\" width=\"100%\">\r\n							<tbody class=\"mcnButtonBlockOuter\">\r\n								<tr>\r\n									<td align=\"center\" class=\"mcnButtonBlockInner\" style=\"mso-line-height-rule: exactly; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%; padding: 0 50px;\" valign=\"top\">\r\n\r\n										<table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" class=\"mcnButtonBlock\" style=\"border-collapse: collapse; mso-table-lspace: 0; mso-table-rspace: 0; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%; min-width:100%; background-color: #5bad9a;\" width=\"100%\">\r\n											<tbody class=\"mcnButtonBlockOuter\">\r\n												\r\n												<tr>\r\n													<td align=\"center\" class=\"mcnButtonContent\" style=\"mso-line-height-rule: exactly; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%; font-family: \'Asap\', Helvetica, sans-serif; font-size: 16px; padding: 24px;\" valign=\"middle\">\r\n\r\n														<a class=\"mcnButton \" href=\"##DASHBOARD_LINK##\" style=\"mso-line-height-rule: exactly; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%; display: block; text-decoration: none; letter-spacing: 1px; line-height: 100%; text-align: center; text-decoration: none; color: #ffffff; text-transform: uppercase;\" target=\"_blank\" title=\"Go To Dashboard\">Go To Dashboard</a>\r\n													</td>\r\n\r\n												</tr>\r\n											</tbody>\r\n										</table>\r\n									</td>\r\n								</tr>\r\n							</tbody>\r\n						</table>\r\n\r\n					</td>\r\n				</tr>\r\n\r\n				<tr>\r\n					<td id=\"templatePreheader\" style=\"mso-line-height-rule: exactly; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%; background-color: #ffffff; border-bottom: 0; border-top: 1px solid #76aea169; padding-top: 16px; padding-bottom: 8px\" valign=\"top\">\r\n						<table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" class=\"mcnTextBlock\" style=\"border-collapse: collapse; mso-table-lspace: 0; mso-table-rspace: 0; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%; min-width:100%;\" width=\"100%\">\r\n							<tbody class=\"mcnTextBlockOuter\">\r\n								<tr>\r\n									\r\n									<td class=\"mcnTextContent\" style=\'mso-line-height-rule: exactly; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%; word-break: break-word; color: #2a2a2a; font-family: \"Asap\", Helvetica, sans-serif; font-size: 12px; line-height: 150%; text-align: center; padding-top:2px; padding-bottom: 2px;\' valign=\"top\">\r\n\r\n										<p class=\"null\" style=\'color: #2a2a2a; font-family: \"Asap\", Helvetica, sans-serif; font-size: 12px; font-style: normal; line-height: 125%; letter-spacing: 1px; text-align: center; display: block; margin: 0; padding: 0\'>\r\n\r\n											Learn More About <a class=\"mcnButton \" href=\"#\" style=\"text-decoration: none; letter-spacing: 1px; line-height: 100%; text-align: center; color: #5bad9a; text-transform: uppercase;\" target=\"_blank\" title=\"##COPY_RIGHT##\">##COPY_RIGHT##</a>\r\n										</p>\r\n\r\n										<p class=\"null\" style=\'color: #2a2a2a; font-family: \"Asap\", Helvetica, sans-serif; font-size: 12px; font-style: normal; line-height: 125%; letter-spacing: 1px; text-align: center; display: block; margin: 0; padding: 10px 0;\'>\r\n\r\n											<a class=\"mcnButton \" href=\"#\" style=\"text-decoration: none; letter-spacing: 1px; line-height: 100%; text-align: center; color: #5bad9a; text-transform: uppercase;\" target=\"_blank\" title=\"About Piquic\">About Piquic</a><span style=\"font-weight: bolder;\"> &middot; </span><a class=\"mcnButton \" href=\"#\" style=\"text-decoration: none; letter-spacing: 1px; line-height: 100%; text-align: center; color: #5bad9a; text-transform: uppercase;\" target=\"_blank\" title=\"Help\">Help</a><span style=\"font-weight: bolder;\"> &middot; </span><a class=\"mcnButton \" href=\"#\" style=\"text-decoration: none; letter-spacing: 1px; line-height: 100%; text-align: center; color: #5bad9a; text-transform: uppercase;\" target=\"_blank\" title=\"Legal\">Legal</a><span style=\"font-weight: bolder;\"> &middot; </span><a class=\"mcnButton \" href=\"#\" style=\"text-decoration: none; letter-spacing: 1px; line-height: 100%; text-align: center; color: #5bad9a; text-transform: uppercase;\" target=\"_blank\" title=\"Report Spam\">Report Spam</a>\r\n										</p>\r\n									</td>\r\n								</tr>\r\n							</tbody>\r\n						</table>\r\n					</td>\r\n				</tr>\r\n			</table>\r\n		</td>\r\n	</tr>\r\n</table>'),
(5, 'MAIL_BODY_REJECTION_BRIEF', '<link href=\'https://fonts.googleapis.com/css?family=Asap:400,400italic,700,700italic\' rel=\'stylesheet\' type=\'text/css\'>\r\n\r\n<table align=\"center\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" height=\"100%\" id=\"bodyTable\" style=\"border-collapse: collapse; mso-table-lspace: 0; mso-table-rspace: 0; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%; background-color: #fff; height: 100%; margin: 0; padding: 0; width: 100%\" width=\"100%;\">\r\n	<tr>\r\n		<td align=\"center\" id=\"bodyCell\" style=\"mso-line-height-rule: exactly;-ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%; border-top: 0;height: 100%; margin: 0; padding: 0; width: 100%;\" valign=\"top\">\r\n			<table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" class=\"templateContainer\" style=\"border-collapse: collapse; mso-table-lspace: 0; mso-table-rspace: 0;  box-shadow: 8px 13px 44px 0px #000000; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%; max-width: 600px; border: 0\" width=\"100%\">\r\n				<tr>\r\n					<td id=\"templatePreheader\" style=\"mso-line-height-rule: exactly; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%; background-color: #ffffff; border-top: 0; border-bottom: 1px solid #76aea169; padding-top: 16px; padding-bottom: 8px\" valign=\"top\">\r\n						<table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" class=\"mcnTextBlock\" style=\"border-collapse: collapse; mso-table-lspace: 0; mso-table-rspace: 0; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%; min-width:100%;\" width=\"100%\">\r\n							<tbody class=\"mcnTextBlockOuter\">\r\n								<tr>\r\n									<td class=\"mcnTextBlockInner\" style=\"mso-line-height-rule: exactly; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%\" valign=\"top\">\r\n										<table align=\"left\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" class=\"mcnTextContentContainer\" style=\"border-collapse: collapse; mso-table-lspace: 0; mso-table-rspace: 0; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%; min-width:100%;\" width=\"100%\">\r\n											<tbody>\r\n												<tr>\r\n													<td class=\"mcnTextContent\" style=\'mso-line-height-rule: exactly; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%; word-break: break-word; color: #2a2a2a; font-family: \"Asap\", Helvetica, sans-serif; font-size: 12px; line-height: 150%; text-align: center; padding-top:9px; padding-right: 18px; padding-bottom: 9px; padding-left: 18px;\' valign=\"top\">\r\n\r\n														<img align=\"none\" alt=\"Piquic\" height=\"52\" src=\"##WEBSITE_URL##/website-assets/images/logo.png\" style=\"-ms-interpolation-mode: bicubic; border: 0; outline: none; text-decoration: none; height: auto; width: 200px;  margin: 0px;\"  />\r\n													</td>\r\n												</tr>\r\n											</tbody>\r\n										</table>\r\n									</td>\r\n								</tr>\r\n							</tbody>\r\n						</table>\r\n					</td>\r\n				</tr>\r\n\r\n				<tr>\r\n					<td id=\"templateBody\" style=\"mso-line-height-rule: exactly; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%; background-color: #ffffff; border-top: 0; border-bottom: 0; padding-top: 0; padding-bottom: 20px\" valign=\"top\">\r\n\r\n						<table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" class=\"mcnButtonBlock\" style=\"border-collapse: collapse; mso-table-lspace: 0; mso-table-rspace: 0; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%; min-width:100%;\" width=\"100%\">\r\n							<tbody class=\"mcnButtonBlockOuter\">\r\n								<tr>\r\n									<td align=\"center\" class=\"mcnButtonBlockInner\" style=\"mso-line-height-rule: exactly; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%; padding-top:18px; padding-right:18px; padding-bottom:18px; padding-left:18px;\" valign=\"top\">\r\n\r\n										<table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" class=\"mcnButtonBlock\" style=\"border-collapse: collapse; mso-table-lspace: 0; mso-table-rspace: 0; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%; min-width:100%;\" width=\"100%\">\r\n											<tbody class=\"mcnButtonBlockOuter\">\r\n												<tr>\r\n													<td align=\"center\" class=\"mcnButtonBlockInner\" style=\"mso-line-height-rule: exactly; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%; padding-top:0; padding-right:18px; padding-bottom:18px; padding-left:18px;\" valign=\"top\">\r\n														<table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" class=\"mcnButtonContentContainer\" style=\"border-collapse: collapse; mso-table-lspace: 0; mso-table-rspace: 0; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%; border-collapse: separate !important;border-radius: 48px;\">\r\n															<tbody>\r\n																<tr>\r\n																	<td align=\"center\" class=\"mcnButtonContent\" style=\"mso-line-height-rule: exactly; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%; font-family: \'Asap\', Helvetica, sans-serif; font-size: 16px; padding-top:24px; padding-right:48px; padding-bottom:24px; padding-left:48px;\" valign=\"middle\">\r\n\r\n																		<p class=\"null\" style=\'color: #2a2a2a; font-family: \"Asap\", Helvetica, sans-serif; font-size: 24px; font-style: normal; line-height: 125%; letter-spacing: 1px; text-align: center; display: block; margin: 0; padding: 0 0 40px 0\'>\r\n																			The brief of your project<br>\"<i>##PROJECT_NAME##</i>\"<br>has been rejected.\r\n																		</p>\r\n\r\n																		<!-- <p class=\"null\" style=\'color: #2a2a2a; font-family: \"Asap\", Helvetica, sans-serif; font-size: 20px; font-style: normal; line-height: 125%; letter-spacing: 1px; text-align: center; display: block; margin: 0; padding: 0 0 40px 0\'>\r\n																			Provide your feedback for this project.\r\n																		</p>\r\n\r\n																		<img align=\"none\" alt=\"Add Feedback\" src=\"add-feedback.gif\" style=\"-ms-interpolation-mode: bicubic; border: 0; outline: none; text-decoration: none; height: auto; width: 450px;  margin: 0px; box-shadow: 0 1px 10px 1px #5bad9a69;\"  /> -->																\r\n																	</td>\r\n																</tr>\r\n															</tbody>\r\n														</table>\r\n													</td>\r\n												</tr>\r\n											</tbody>\r\n										</table>\r\n									</td>\r\n								</tr>\r\n							</tbody>\r\n						</table>\r\n\r\n						<table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" class=\"mcnButtonBlock\" style=\"border-collapse: collapse; mso-table-lspace: 0; mso-table-rspace: 0; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%; min-width:100%;\" width=\"100%\">\r\n							<tbody class=\"mcnButtonBlockOuter\">\r\n								<tr>\r\n									<td align=\"center\" class=\"mcnButtonBlockInner\" style=\"mso-line-height-rule: exactly; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%; padding: 0 50px;\" valign=\"top\">\r\n\r\n										<table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" class=\"mcnButtonBlock\" style=\"border-collapse: collapse; mso-table-lspace: 0; mso-table-rspace: 0; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%; min-width:100%; background-color: #5bad9a;\" width=\"100%\">\r\n											<tbody class=\"mcnButtonBlockOuter\">\r\n												\r\n												<tr>\r\n													<td align=\"center\" class=\"mcnButtonContent\" style=\"mso-line-height-rule: exactly; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%; font-family: \'Asap\', Helvetica, sans-serif; font-size: 16px; padding: 24px;\" valign=\"middle\">\r\n\r\n														<a class=\"mcnButton \" href=\"##DASHBOARD_LINK##\" style=\"mso-line-height-rule: exactly; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%; display: block; text-decoration: none; letter-spacing: 1px; line-height: 100%; text-align: center; text-decoration: none; color: #ffffff; text-transform: uppercase;\" target=\"_blank\" title=\"Go To Dashboard\">Go To Dashboard</a>\r\n													</td>\r\n\r\n												</tr>\r\n											</tbody>\r\n										</table>\r\n									</td>\r\n								</tr>\r\n							</tbody>\r\n						</table>\r\n\r\n					</td>\r\n				</tr>\r\n\r\n				<tr>\r\n					<td id=\"templatePreheader\" style=\"mso-line-height-rule: exactly; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%; background-color: #ffffff; border-bottom: 0; border-top: 1px solid #76aea169; padding-top: 16px; padding-bottom: 8px\" valign=\"top\">\r\n						<table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" class=\"mcnTextBlock\" style=\"border-collapse: collapse; mso-table-lspace: 0; mso-table-rspace: 0; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%; min-width:100%;\" width=\"100%\">\r\n							<tbody class=\"mcnTextBlockOuter\">\r\n								<tr>\r\n									\r\n									<td class=\"mcnTextContent\" style=\'mso-line-height-rule: exactly; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%; word-break: break-word; color: #2a2a2a; font-family: \"Asap\", Helvetica, sans-serif; font-size: 12px; line-height: 150%; text-align: center; padding-top:2px; padding-bottom: 2px;\' valign=\"top\">\r\n\r\n										<p class=\"null\" style=\'color: #2a2a2a; font-family: \"Asap\", Helvetica, sans-serif; font-size: 12px; font-style: normal; line-height: 125%; letter-spacing: 1px; text-align: center; display: block; margin: 0; padding: 0\'>\r\n\r\n											Learn More About <a class=\"mcnButton \" href=\"#\" style=\"text-decoration: none; letter-spacing: 1px; line-height: 100%; text-align: center; color: #5bad9a; text-transform: uppercase;\" target=\"_blank\" title=\"##COPY_RIGHT##\">##COPY_RIGHT##</a>\r\n										</p>\r\n\r\n										<p class=\"null\" style=\'color: #2a2a2a; font-family: \"Asap\", Helvetica, sans-serif; font-size: 12px; font-style: normal; line-height: 125%; letter-spacing: 1px; text-align: center; display: block; margin: 0; padding: 10px 0;\'>\r\n\r\n											<a class=\"mcnButton \" href=\"#\" style=\"text-decoration: none; letter-spacing: 1px; line-height: 100%; text-align: center; color: #5bad9a; text-transform: uppercase;\" target=\"_blank\" title=\"About Piquic\">About Piquic</a><span style=\"font-weight: bolder;\"> &middot; </span><a class=\"mcnButton \" href=\"#\" style=\"text-decoration: none; letter-spacing: 1px; line-height: 100%; text-align: center; color: #5bad9a; text-transform: uppercase;\" target=\"_blank\" title=\"Help\">Help</a><span style=\"font-weight: bolder;\"> &middot; </span><a class=\"mcnButton \" href=\"#\" style=\"text-decoration: none; letter-spacing: 1px; line-height: 100%; text-align: center; color: #5bad9a; text-transform: uppercase;\" target=\"_blank\" title=\"Legal\">Legal</a><span style=\"font-weight: bolder;\"> &middot; </span><a class=\"mcnButton \" href=\"#\" style=\"text-decoration: none; letter-spacing: 1px; line-height: 100%; text-align: center; color: #5bad9a; text-transform: uppercase;\" target=\"_blank\" title=\"Report Spam\">Report Spam</a>\r\n										</p>\r\n									</td>\r\n								</tr>\r\n							</tbody>\r\n						</table>\r\n					</td>\r\n				</tr>\r\n			</table>\r\n		</td>\r\n	</tr>\r\n</table>'),
(6, 'MAIL_BODY_NOTIFY_FILEUPLOAD', '<link href=\'https://fonts.googleapis.com/css?family=Asap:400,400italic,700,700italic\' rel=\'stylesheet\' type=\'text/css\'>\r\n\r\n<table align=\"center\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" height=\"100%\" id=\"bodyTable\" style=\"border-collapse: collapse; mso-table-lspace: 0; mso-table-rspace: 0; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%; background-color: #fff; height: 100%; margin: 0; padding: 0; width: 100%\" width=\"100%;\">\r\n	<tr>\r\n		<td align=\"center\" id=\"bodyCell\" style=\"mso-line-height-rule: exactly;-ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%; border-top: 0;height: 100%; margin: 0; padding: 0; width: 100%;\" valign=\"top\">\r\n			<table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" class=\"templateContainer\" style=\"border-collapse: collapse; mso-table-lspace: 0; mso-table-rspace: 0;  box-shadow: 8px 13px 44px 0px #000000; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%; max-width: 600px; border: 0\" width=\"100%\">\r\n				<tr>\r\n					<td id=\"templatePreheader\" style=\"mso-line-height-rule: exactly; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%; background-color: #ffffff; border-top: 0; border-bottom: 1px solid #76aea169; padding-top: 16px; padding-bottom: 8px\" valign=\"top\">\r\n						<table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" class=\"mcnTextBlock\" style=\"border-collapse: collapse; mso-table-lspace: 0; mso-table-rspace: 0; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%; min-width:100%;\" width=\"100%\">\r\n							<tbody class=\"mcnTextBlockOuter\">\r\n								<tr>\r\n									<td class=\"mcnTextBlockInner\" style=\"mso-line-height-rule: exactly; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%\" valign=\"top\">\r\n										<table align=\"left\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" class=\"mcnTextContentContainer\" style=\"border-collapse: collapse; mso-table-lspace: 0; mso-table-rspace: 0; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%; min-width:100%;\" width=\"100%\">\r\n											<tbody>\r\n												<tr>\r\n													<td class=\"mcnTextContent\" style=\'mso-line-height-rule: exactly; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%; word-break: break-word; color: #2a2a2a; font-family: \"Asap\", Helvetica, sans-serif; font-size: 12px; line-height: 150%; text-align: center; padding-top:9px; padding-right: 18px; padding-bottom: 9px; padding-left: 18px;\' valign=\"top\">\r\n\r\n														<img align=\"none\" alt=\"Piquic\" height=\"52\" src=\"##WEBSITE_URL##/website-assets/images/logo.png\" style=\"-ms-interpolation-mode: bicubic; border: 0; outline: none; text-decoration: none; height: auto; width: 200px;  margin: 0px;\"  />\r\n													</td>\r\n												</tr>\r\n											</tbody>\r\n										</table>\r\n									</td>\r\n								</tr>\r\n							</tbody>\r\n						</table>\r\n					</td>\r\n				</tr>\r\n\r\n				<tr>\r\n					<td id=\"templateBody\" style=\"mso-line-height-rule: exactly; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%; background-color: #ffffff; border-top: 0; border-bottom: 0; padding-top: 0; padding-bottom: 20px\" valign=\"top\">\r\n\r\n						<table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" class=\"mcnButtonBlock\" style=\"border-collapse: collapse; mso-table-lspace: 0; mso-table-rspace: 0; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%; min-width:100%;\" width=\"100%\">\r\n							<tbody class=\"mcnButtonBlockOuter\">\r\n								<tr>\r\n									<td align=\"center\" class=\"mcnButtonBlockInner\" style=\"mso-line-height-rule: exactly; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%; padding-top:18px; padding-right:18px; padding-bottom:18px; padding-left:18px;\" valign=\"top\">\r\n\r\n										<table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" class=\"mcnButtonBlock\" style=\"border-collapse: collapse; mso-table-lspace: 0; mso-table-rspace: 0; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%; min-width:100%;\" width=\"100%\">\r\n											<tbody class=\"mcnButtonBlockOuter\">\r\n												<tr>\r\n													<td align=\"center\" class=\"mcnButtonBlockInner\" style=\"mso-line-height-rule: exactly; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%; padding-top:0; padding-right:18px; padding-bottom:18px; padding-left:18px;\" valign=\"top\">\r\n														<table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" class=\"mcnButtonContentContainer\" style=\"border-collapse: collapse; mso-table-lspace: 0; mso-table-rspace: 0; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%; border-collapse: separate !important;border-radius: 48px;\">\r\n															<tbody>\r\n																<tr>\r\n																	<td align=\"center\" class=\"mcnButtonContent\" style=\"mso-line-height-rule: exactly; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%; font-family: \'Asap\', Helvetica, sans-serif; font-size: 16px; padding-top:24px; padding-right:48px; padding-bottom:24px; padding-left:48px;\" valign=\"middle\">\r\n\r\n																		<p class=\"null\" style=\'color: #2a2a2a; font-family: \"Asap\", Helvetica, sans-serif; font-size: 24px; font-style: normal; line-height: 125%; letter-spacing: 1px; text-align: center; display: block; margin: 0; padding: 0 0 40px 0\'>\r\n																			New files are waiting for your<br>review in \"<i>##PROJECT_NAME##</i>\"\r\n																		</p>\r\n\r\n																		<p class=\"null\" style=\'color: #2a2a2a; font-family: \"Asap\", Helvetica, sans-serif; font-size: 20px; font-style: normal; line-height: 125%; letter-spacing: 1px; text-align: center; display: block; margin: 0; padding: 0 0 40px 0\'>\r\n																			View all reviews on your dashboard\r\n																		</p>\r\n\r\n																		<img align=\"none\" alt=\"Review Waiting\" src=\"##WEBSITE_URL##/website-assets/images/review-waiting.jpg\" style=\"-ms-interpolation-mode: bicubic; border: 0; outline: none; text-decoration: none; height: auto; width: 450px;  margin: 0px; box-shadow: 0 1px 10px 1px #5bad9a69;\"  />																		\r\n																	</td>\r\n																</tr>\r\n															</tbody>\r\n														</table>\r\n													</td>\r\n												</tr>\r\n											</tbody>\r\n										</table>\r\n									</td>\r\n								</tr>\r\n							</tbody>\r\n						</table>\r\n\r\n						<table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" class=\"mcnButtonBlock\" style=\"border-collapse: collapse; mso-table-lspace: 0; mso-table-rspace: 0; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%; min-width:100%;\" width=\"100%\">\r\n							<tbody class=\"mcnButtonBlockOuter\">\r\n								<tr>\r\n									<td align=\"center\" class=\"mcnButtonBlockInner\" style=\"mso-line-height-rule: exactly; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%; padding: 0 50px;\" valign=\"top\">\r\n\r\n										<table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" class=\"mcnButtonBlock\" style=\"border-collapse: collapse; mso-table-lspace: 0; mso-table-rspace: 0; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%; min-width:100%; background-color: #5bad9a;\" width=\"100%\">\r\n											<tbody class=\"mcnButtonBlockOuter\">\r\n												\r\n												<tr>\r\n													<td align=\"center\" class=\"mcnButtonContent\" style=\"mso-line-height-rule: exactly; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%; font-family: \'Asap\', Helvetica, sans-serif; font-size: 16px; padding: 24px;\" valign=\"middle\">\r\n\r\n														<a class=\"mcnButton \" href=\"##DASHBOARD_LINK##\" style=\"mso-line-height-rule: exactly; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%; display: block; text-decoration: none; letter-spacing: 1px; line-height: 100%; text-align: center; text-decoration: none; color: #ffffff; text-transform: uppercase;\" target=\"_blank\" title=\"Go To Dashboard\">Go To Dashboard</a>\r\n													</td>\r\n\r\n												</tr>\r\n											</tbody>\r\n										</table>\r\n									</td>\r\n								</tr>\r\n							</tbody>\r\n						</table>\r\n\r\n					</td>\r\n				</tr>\r\n\r\n				<tr>\r\n					<td id=\"templatePreheader\" style=\"mso-line-height-rule: exactly; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%; background-color: #ffffff; border-bottom: 0; border-top: 1px solid #76aea169; padding-top: 16px; padding-bottom: 8px\" valign=\"top\">\r\n						<table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" class=\"mcnTextBlock\" style=\"border-collapse: collapse; mso-table-lspace: 0; mso-table-rspace: 0; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%; min-width:100%;\" width=\"100%\">\r\n							<tbody class=\"mcnTextBlockOuter\">\r\n								<tr>\r\n									\r\n									<td class=\"mcnTextContent\" style=\'mso-line-height-rule: exactly; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%; word-break: break-word; color: #2a2a2a; font-family: \"Asap\", Helvetica, sans-serif; font-size: 12px; line-height: 150%; text-align: center; padding-top:2px; padding-bottom: 2px;\' valign=\"top\">\r\n\r\n										<p class=\"null\" style=\'color: #2a2a2a; font-family: \"Asap\", Helvetica, sans-serif; font-size: 12px; font-style: normal; line-height: 125%; letter-spacing: 1px; text-align: center; display: block; margin: 0; padding: 0\'>\r\n\r\n											Learn More About <a class=\"mcnButton \" href=\"#\" style=\"text-decoration: none; letter-spacing: 1px; line-height: 100%; text-align: center; color: #5bad9a; text-transform: uppercase;\" target=\"_blank\" title=\"##COPY_RIGHT##\">##COPY_RIGHT##</a>\r\n										</p>\r\n\r\n										<p class=\"null\" style=\'color: #2a2a2a; font-family: \"Asap\", Helvetica, sans-serif; font-size: 12px; font-style: normal; line-height: 125%; letter-spacing: 1px; text-align: center; display: block; margin: 0; padding: 10px 0;\'>\r\n\r\n											<a class=\"mcnButton \" href=\"#\" style=\"text-decoration: none; letter-spacing: 1px; line-height: 100%; text-align: center; color: #5bad9a; text-transform: uppercase;\" target=\"_blank\" title=\"About Piquic\">About Piquic</a><span style=\"font-weight: bolder;\"> &middot; </span><a class=\"mcnButton \" href=\"#\" style=\"text-decoration: none; letter-spacing: 1px; line-height: 100%; text-align: center; color: #5bad9a; text-transform: uppercase;\" target=\"_blank\" title=\"Help\">Help</a><span style=\"font-weight: bolder;\"> &middot; </span><a class=\"mcnButton \" href=\"#\" style=\"text-decoration: none; letter-spacing: 1px; line-height: 100%; text-align: center; color: #5bad9a; text-transform: uppercase;\" target=\"_blank\" title=\"Legal\">Legal</a><span style=\"font-weight: bolder;\"> &middot; </span><a class=\"mcnButton \" href=\"#\" style=\"text-decoration: none; letter-spacing: 1px; line-height: 100%; text-align: center; color: #5bad9a; text-transform: uppercase;\" target=\"_blank\" title=\"Report Spam\">Report Spam</a>\r\n										</p>\r\n									</td>\r\n								</tr>\r\n							</tbody>\r\n						</table>\r\n					</td>\r\n				</tr>\r\n			</table>\r\n		</td>\r\n	</tr>\r\n</table>');
INSERT INTO `wc_settings` (`setting_id`, `setting_key`, `setting_value`) VALUES
(7, 'MAIL_BODY_FEEDBACK', '<link href=\'https://fonts.googleapis.com/css?family=Asap:400,400italic,700,700italic\' rel=\'stylesheet\' type=\'text/css\'>\r\n\r\n<table align=\"center\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" height=\"100%\" id=\"bodyTable\" style=\"border-collapse: collapse; mso-table-lspace: 0; mso-table-rspace: 0; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%; background-color: #fff; height: 100%; margin: 0; padding: 0; width: 100%\" width=\"100%;\">\r\n	<tr>\r\n		<td align=\"center\" id=\"bodyCell\" style=\"mso-line-height-rule: exactly;-ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%; border-top: 0;height: 100%; margin: 0; padding: 0; width: 100%;\" valign=\"top\">\r\n			<table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" class=\"templateContainer\" style=\"border-collapse: collapse; mso-table-lspace: 0; mso-table-rspace: 0;  box-shadow: 8px 13px 44px 0px #000000; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%; max-width: 600px; border: 0\" width=\"100%\">\r\n				<tr>\r\n					<td id=\"templatePreheader\" style=\"mso-line-height-rule: exactly; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%; background-color: #ffffff; border-top: 0; border-bottom: 1px solid #76aea169; padding-top: 16px; padding-bottom: 8px\" valign=\"top\">\r\n						<table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" class=\"mcnTextBlock\" style=\"border-collapse: collapse; mso-table-lspace: 0; mso-table-rspace: 0; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%; min-width:100%;\" width=\"100%\">\r\n							<tbody class=\"mcnTextBlockOuter\">\r\n								<tr>\r\n									<td class=\"mcnTextBlockInner\" style=\"mso-line-height-rule: exactly; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%\" valign=\"top\">\r\n										<table align=\"left\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" class=\"mcnTextContentContainer\" style=\"border-collapse: collapse; mso-table-lspace: 0; mso-table-rspace: 0; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%; min-width:100%;\" width=\"100%\">\r\n											<tbody>\r\n												<tr>\r\n													<td class=\"mcnTextContent\" style=\'mso-line-height-rule: exactly; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%; word-break: break-word; color: #2a2a2a; font-family: \"Asap\", Helvetica, sans-serif; font-size: 12px; line-height: 150%; text-align: center; padding-top:9px; padding-right: 18px; padding-bottom: 9px; padding-left: 18px;\' valign=\"top\">\r\n\r\n														<img align=\"none\" alt=\"Piquic\" height=\"52\" src=\"##WEBSITE_URL##/website-assets/images/logo.png\" style=\"-ms-interpolation-mode: bicubic; border: 0; outline: none; text-decoration: none; height: auto; width: 200px;  margin: 0px;\"  />\r\n													</td>\r\n												</tr>\r\n											</tbody>\r\n										</table>\r\n									</td>\r\n								</tr>\r\n							</tbody>\r\n						</table>\r\n					</td>\r\n				</tr>\r\n\r\n				<tr>\r\n					<td id=\"templateBody\" style=\"mso-line-height-rule: exactly; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%; background-color: #ffffff; border-top: 0; border-bottom: 0; padding-top: 0; padding-bottom: 20px\" valign=\"top\">\r\n\r\n						<table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" class=\"mcnButtonBlock\" style=\"border-collapse: collapse; mso-table-lspace: 0; mso-table-rspace: 0; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%; min-width:100%;\" width=\"100%\">\r\n							<tbody class=\"mcnButtonBlockOuter\">\r\n								<tr>\r\n									<td align=\"center\" class=\"mcnButtonBlockInner\" style=\"mso-line-height-rule: exactly; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%; padding-top:18px; padding-right:18px; padding-bottom:18px; padding-left:18px;\" valign=\"top\">\r\n\r\n										<table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" class=\"mcnButtonBlock\" style=\"border-collapse: collapse; mso-table-lspace: 0; mso-table-rspace: 0; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%; min-width:100%;\" width=\"100%\">\r\n											<tbody class=\"mcnButtonBlockOuter\">\r\n												<tr>\r\n													<td align=\"center\" class=\"mcnButtonBlockInner\" style=\"mso-line-height-rule: exactly; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%; padding-top:0; padding-right:18px; padding-bottom:18px; padding-left:18px;\" valign=\"top\">\r\n														<table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" class=\"mcnButtonContentContainer\" style=\"border-collapse: collapse; mso-table-lspace: 0; mso-table-rspace: 0; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%; border-collapse: separate !important;border-radius: 48px;\">\r\n															<tbody>\r\n																<tr>\r\n																	<td align=\"center\" class=\"mcnButtonContent\" style=\"mso-line-height-rule: exactly; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%; font-family: \'Asap\', Helvetica, sans-serif; font-size: 16px; padding-top:24px; padding-right:48px; padding-bottom:24px; padding-left:48px;\" valign=\"middle\">\r\n\r\n																		<p class=\"null\" style=\'color: #2a2a2a; font-family: \"Asap\", Helvetica, sans-serif; font-size: 24px; font-style: normal; line-height: 125%; letter-spacing: 1px; text-align: center; display: block; margin: 0; padding: 0 0 40px 0\'>\r\n																			The project<br>\"<i>##PROJECT_NAME##</i>\"<br>has been completed.\r\n																		</p>\r\n\r\n																		<p class=\"null\" style=\'color: #2a2a2a; font-family: \"Asap\", Helvetica, sans-serif; font-size: 20px; font-style: normal; line-height: 125%; letter-spacing: 1px; text-align: center; display: block; margin: 0; padding: 0 0 40px 0\'>\r\n																			Provide your feedback for this project<br> ##FEEDBACK_SUBMIT_LINK##\r\n																		</p>\r\n																		<img align=\"none\" alt=\"Add Feedback\" src=\"##WEBSITE_URL##/website-assets/images/add-feedback.gif\" style=\"-ms-interpolation-mode: bicubic; border: 0; outline: none; text-decoration: none; height: auto; width: 450px;  margin: 0px; box-shadow: 0 1px 10px 1px #5bad9a69;\"  />\r\n																																			\r\n																	</td>\r\n																</tr>\r\n															</tbody>\r\n														</table>\r\n													</td>\r\n												</tr>\r\n											</tbody>\r\n										</table>\r\n									</td>\r\n								</tr>\r\n							</tbody>\r\n						</table>\r\n\r\n						<table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" class=\"mcnButtonBlock\" style=\"border-collapse: collapse; mso-table-lspace: 0; mso-table-rspace: 0; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%; min-width:100%;\" width=\"100%\">\r\n							<tbody class=\"mcnButtonBlockOuter\">\r\n								<tr>\r\n									<td align=\"center\" class=\"mcnButtonBlockInner\" style=\"mso-line-height-rule: exactly; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%; padding: 0 50px;\" valign=\"top\">\r\n\r\n										<table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" class=\"mcnButtonBlock\" style=\"border-collapse: collapse; mso-table-lspace: 0; mso-table-rspace: 0; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%; min-width:100%; background-color: #5bad9a;\" width=\"100%\">\r\n											<tbody class=\"mcnButtonBlockOuter\">\r\n												\r\n												<tr>\r\n													<td align=\"center\" class=\"mcnButtonContent\" style=\"mso-line-height-rule: exactly; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%; font-family: \'Asap\', Helvetica, sans-serif; font-size: 16px; padding: 24px;\" valign=\"middle\">\r\n\r\n														<a class=\"mcnButton \" href=\"##DASHBOARD_LINK##\" style=\"mso-line-height-rule: exactly; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%; display: block; text-decoration: none; letter-spacing: 1px; line-height: 100%; text-align: center; text-decoration: none; color: #ffffff; text-transform: uppercase;\" target=\"_blank\" title=\"Go To Dashboard\">Go To Dashboard</a>\r\n													</td>\r\n\r\n												</tr>\r\n											</tbody>\r\n										</table>\r\n									</td>\r\n								</tr>\r\n							</tbody>\r\n						</table>\r\n\r\n					</td>\r\n				</tr>\r\n\r\n				<tr>\r\n					<td id=\"templatePreheader\" style=\"mso-line-height-rule: exactly; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%; background-color: #ffffff; border-bottom: 0; border-top: 1px solid #76aea169; padding-top: 16px; padding-bottom: 8px\" valign=\"top\">\r\n						<table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" class=\"mcnTextBlock\" style=\"border-collapse: collapse; mso-table-lspace: 0; mso-table-rspace: 0; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%; min-width:100%;\" width=\"100%\">\r\n							<tbody class=\"mcnTextBlockOuter\">\r\n								<tr>\r\n									\r\n									<td class=\"mcnTextContent\" style=\'mso-line-height-rule: exactly; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%; word-break: break-word; color: #2a2a2a; font-family: \"Asap\", Helvetica, sans-serif; font-size: 12px; line-height: 150%; text-align: center; padding-top:2px; padding-bottom: 2px;\' valign=\"top\">\r\n\r\n										<p class=\"null\" style=\'color: #2a2a2a; font-family: \"Asap\", Helvetica, sans-serif; font-size: 12px; font-style: normal; line-height: 125%; letter-spacing: 1px; text-align: center; display: block; margin: 0; padding: 0\'>\r\n\r\n											Learn More About <a class=\"mcnButton \" href=\"#\" style=\"text-decoration: none; letter-spacing: 1px; line-height: 100%; text-align: center; color: #5bad9a; text-transform: uppercase;\" target=\"_blank\" title=\"##COPY_RIGHT##\">##COPY_RIGHT##</a>\r\n										</p>\r\n\r\n										<p class=\"null\" style=\'color: #2a2a2a; font-family: \"Asap\", Helvetica, sans-serif; font-size: 12px; font-style: normal; line-height: 125%; letter-spacing: 1px; text-align: center; display: block; margin: 0; padding: 10px 0;\'>\r\n\r\n											<a class=\"mcnButton \" href=\"#\" style=\"text-decoration: none; letter-spacing: 1px; line-height: 100%; text-align: center; color: #5bad9a; text-transform: uppercase;\" target=\"_blank\" title=\"About Piquic\">About Piquic</a><span style=\"font-weight: bolder;\"> &middot; </span><a class=\"mcnButton \" href=\"#\" style=\"text-decoration: none; letter-spacing: 1px; line-height: 100%; text-align: center; color: #5bad9a; text-transform: uppercase;\" target=\"_blank\" title=\"Help\">Help</a><span style=\"font-weight: bolder;\"> &middot; </span><a class=\"mcnButton \" href=\"#\" style=\"text-decoration: none; letter-spacing: 1px; line-height: 100%; text-align: center; color: #5bad9a; text-transform: uppercase;\" target=\"_blank\" title=\"Legal\">Legal</a><span style=\"font-weight: bolder;\"> &middot; </span><a class=\"mcnButton \" href=\"#\" style=\"text-decoration: none; letter-spacing: 1px; line-height: 100%; text-align: center; color: #5bad9a; text-transform: uppercase;\" target=\"_blank\" title=\"Report Spam\">Report Spam</a>\r\n										</p>\r\n									</td>\r\n								</tr>\r\n							</tbody>\r\n						</table>\r\n					</td>\r\n				</tr>\r\n			</table>\r\n		</td>\r\n	</tr>\r\n</table>'),
(8, 'WEKAN_LINK', 'http://172.25.0.92:80/'),
(9, 'WEKAN_USERNAME', 'Collab'),
(10, 'WEKAN_PASSWORD', 'AaAa2121'),
(11, 'WEKAN_URL_LINK', 'http://collabteam.piquic.com/'),
(12, 'WEKAN_LIST_ID_PENDING', 'WHwovEqmniLjA8bsq'),
(13, 'WEKAN_LIST_ID_ONGOING', 'iYTdBeuPrbYno6uc4'),
(14, 'WEKAN_LIST_ID_REJECTED', 'WP9fhpy9RMmCk6qqb'),
(15, 'MAIL_BODY_APPROVED', '<link href=\'https://fonts.googleapis.com/css?family=Asap:400,400italic,700,700italic\' rel=\'stylesheet\' type=\'text/css\'>\r\n\r\n<table align=\"center\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" height=\"100%\" id=\"bodyTable\" style=\"border-collapse: collapse; mso-table-lspace: 0; mso-table-rspace: 0; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%; background-color: #fff; height: 100%; margin: 0; padding: 0; width: 100%\" width=\"100%;\">\r\n	<tr>\r\n		<td align=\"center\" id=\"bodyCell\" style=\"mso-line-height-rule: exactly;-ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%; border-top: 0;height: 100%; margin: 0; padding: 0; width: 100%;\" valign=\"top\">\r\n			<table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" class=\"templateContainer\" style=\"border-collapse: collapse; mso-table-lspace: 0; mso-table-rspace: 0;  box-shadow: 8px 13px 44px 0px #000000; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%; max-width: 600px; border: 0\" width=\"100%\">\r\n				<tr>\r\n					<td id=\"templatePreheader\" style=\"mso-line-height-rule: exactly; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%; background-color: #ffffff; border-top: 0; border-bottom: 1px solid #76aea169; padding-top: 16px; padding-bottom: 8px\" valign=\"top\">\r\n						<table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" class=\"mcnTextBlock\" style=\"border-collapse: collapse; mso-table-lspace: 0; mso-table-rspace: 0; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%; min-width:100%;\" width=\"100%\">\r\n							<tbody class=\"mcnTextBlockOuter\">\r\n								<tr>\r\n									<td class=\"mcnTextBlockInner\" style=\"mso-line-height-rule: exactly; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%\" valign=\"top\">\r\n										<table align=\"left\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" class=\"mcnTextContentContainer\" style=\"border-collapse: collapse; mso-table-lspace: 0; mso-table-rspace: 0; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%; min-width:100%;\" width=\"100%\">\r\n											<tbody>\r\n												<tr>\r\n													<td class=\"mcnTextContent\" style=\'mso-line-height-rule: exactly; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%; word-break: break-word; color: #2a2a2a; font-family: \"Asap\", Helvetica, sans-serif; font-size: 12px; line-height: 150%; text-align: center; padding-top:9px; padding-right: 18px; padding-bottom: 9px; padding-left: 18px;\' valign=\"top\">\r\n\r\n														<img align=\"none\" alt=\"Piquic\" height=\"52\" src=\"##WEBSITE_URL##/website-assets/images/logo.png\" style=\"-ms-interpolation-mode: bicubic; border: 0; outline: none; text-decoration: none; height: auto; width: 200px;  margin: 0px;\"  />\r\n													</td>\r\n												</tr>\r\n											</tbody>\r\n										</table>\r\n									</td>\r\n								</tr>\r\n							</tbody>\r\n						</table>\r\n					</td>\r\n				</tr>\r\n\r\n				<tr>\r\n					<td id=\"templateBody\" style=\"mso-line-height-rule: exactly; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%; background-color: #ffffff; border-top: 0; border-bottom: 0; padding-top: 0; padding-bottom: 20px\" valign=\"top\">\r\n\r\n						<table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" class=\"mcnButtonBlock\" style=\"border-collapse: collapse; mso-table-lspace: 0; mso-table-rspace: 0; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%; min-width:100%;\" width=\"100%\">\r\n							<tbody class=\"mcnButtonBlockOuter\">\r\n								<tr>\r\n									<td align=\"center\" class=\"mcnButtonBlockInner\" style=\"mso-line-height-rule: exactly; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%; padding-top:18px; padding-right:18px; padding-bottom:18px; padding-left:18px;\" valign=\"top\">\r\n\r\n										<table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" class=\"mcnButtonBlock\" style=\"border-collapse: collapse; mso-table-lspace: 0; mso-table-rspace: 0; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%; min-width:100%;\" width=\"100%\">\r\n											<tbody class=\"mcnButtonBlockOuter\">\r\n												<tr>\r\n													<td align=\"center\" class=\"mcnButtonBlockInner\" style=\"mso-line-height-rule: exactly; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%; padding-top:0; padding-right:18px; padding-bottom:18px; padding-left:18px;\" valign=\"top\">\r\n														<table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" class=\"mcnButtonContentContainer\" style=\"border-collapse: collapse; mso-table-lspace: 0; mso-table-rspace: 0; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%; border-collapse: separate !important;border-radius: 48px;\">\r\n															<tbody>\r\n																<tr>\r\n																	<td align=\"center\" class=\"mcnButtonContent\" style=\"mso-line-height-rule: exactly; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%; font-family: \'Asap\', Helvetica, sans-serif; font-size: 16px; padding-top:24px; padding-right:48px; padding-bottom:24px; padding-left:48px;\" valign=\"middle\">\r\n\r\n																		<p class=\"null\" style=\'color: #2a2a2a; font-family: \"Asap\", Helvetica, sans-serif; font-size: 24px; font-style: normal; line-height: 125%; letter-spacing: 1px; text-align: center; display: block; margin: 0; padding: 0 0 40px 0\'>\r\n																			The brief of your project<br>\"<i>##PROJECT_NAME##</i>\"<br>has been approved.\r\n																		</p>\r\n\r\n																		<!-- <p class=\"null\" style=\'color: #2a2a2a; font-family: \"Asap\", Helvetica, sans-serif; font-size: 20px; font-style: normal; line-height: 125%; letter-spacing: 1px; text-align: center; display: block; margin: 0; padding: 0 0 40px 0\'>\r\n																			Provide your feedback for this project.\r\n																		</p>\r\n\r\n																		<img align=\"none\" alt=\"Add Feedback\" src=\"add-feedback.gif\" style=\"-ms-interpolation-mode: bicubic; border: 0; outline: none; text-decoration: none; height: auto; width: 450px;  margin: 0px; box-shadow: 0 1px 10px 1px #5bad9a69;\"  /> -->																\r\n																	</td>\r\n																</tr>\r\n															</tbody>\r\n														</table>\r\n													</td>\r\n												</tr>\r\n											</tbody>\r\n										</table>\r\n									</td>\r\n								</tr>\r\n							</tbody>\r\n						</table>\r\n\r\n						<table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" class=\"mcnButtonBlock\" style=\"border-collapse: collapse; mso-table-lspace: 0; mso-table-rspace: 0; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%; min-width:100%;\" width=\"100%\">\r\n							<tbody class=\"mcnButtonBlockOuter\">\r\n								<tr>\r\n									<td align=\"center\" class=\"mcnButtonBlockInner\" style=\"mso-line-height-rule: exactly; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%; padding: 0 50px;\" valign=\"top\">\r\n\r\n										<table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" class=\"mcnButtonBlock\" style=\"border-collapse: collapse; mso-table-lspace: 0; mso-table-rspace: 0; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%; min-width:100%; background-color: #5bad9a;\" width=\"100%\">\r\n											<tbody class=\"mcnButtonBlockOuter\">\r\n												\r\n												<tr>\r\n													<td align=\"center\" class=\"mcnButtonContent\" style=\"mso-line-height-rule: exactly; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%; font-family: \'Asap\', Helvetica, sans-serif; font-size: 16px; padding: 24px;\" valign=\"middle\">\r\n\r\n														<a class=\"mcnButton \" href=\"##DASHBOARD_LINK##\" style=\"mso-line-height-rule: exactly; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%; display: block; text-decoration: none; letter-spacing: 1px; line-height: 100%; text-align: center; text-decoration: none; color: #ffffff; text-transform: uppercase;\" target=\"_blank\" title=\"Go To Dashboard\">Go To Dashboard</a>\r\n													</td>\r\n\r\n												</tr>\r\n											</tbody>\r\n										</table>\r\n									</td>\r\n								</tr>\r\n							</tbody>\r\n						</table>\r\n\r\n					</td>\r\n				</tr>\r\n\r\n				<tr>\r\n					<td id=\"templatePreheader\" style=\"mso-line-height-rule: exactly; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%; background-color: #ffffff; border-bottom: 0; border-top: 1px solid #76aea169; padding-top: 16px; padding-bottom: 8px\" valign=\"top\">\r\n						<table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" class=\"mcnTextBlock\" style=\"border-collapse: collapse; mso-table-lspace: 0; mso-table-rspace: 0; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%; min-width:100%;\" width=\"100%\">\r\n							<tbody class=\"mcnTextBlockOuter\">\r\n								<tr>\r\n									\r\n									<td class=\"mcnTextContent\" style=\'mso-line-height-rule: exactly; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%; word-break: break-word; color: #2a2a2a; font-family: \"Asap\", Helvetica, sans-serif; font-size: 12px; line-height: 150%; text-align: center; padding-top:2px; padding-bottom: 2px;\' valign=\"top\">\r\n\r\n										<p class=\"null\" style=\'color: #2a2a2a; font-family: \"Asap\", Helvetica, sans-serif; font-size: 12px; font-style: normal; line-height: 125%; letter-spacing: 1px; text-align: center; display: block; margin: 0; padding: 0\'>\r\n\r\n											Learn More About <a class=\"mcnButton \" href=\"#\" style=\"text-decoration: none; letter-spacing: 1px; line-height: 100%; text-align: center; color: #5bad9a; text-transform: uppercase;\" target=\"_blank\" title=\"##COPY_RIGHT##\">##COPY_RIGHT##</a>\r\n										</p>\r\n\r\n										<p class=\"null\" style=\'color: #2a2a2a; font-family: \"Asap\", Helvetica, sans-serif; font-size: 12px; font-style: normal; line-height: 125%; letter-spacing: 1px; text-align: center; display: block; margin: 0; padding: 10px 0;\'>\r\n\r\n											<a class=\"mcnButton \" href=\"#\" style=\"text-decoration: none; letter-spacing: 1px; line-height: 100%; text-align: center; color: #5bad9a; text-transform: uppercase;\" target=\"_blank\" title=\"About Piquic\">About Piquic</a><span style=\"font-weight: bolder;\"> &middot; </span><a class=\"mcnButton \" href=\"#\" style=\"text-decoration: none; letter-spacing: 1px; line-height: 100%; text-align: center; color: #5bad9a; text-transform: uppercase;\" target=\"_blank\" title=\"Help\">Help</a><span style=\"font-weight: bolder;\"> &middot; </span><a class=\"mcnButton \" href=\"#\" style=\"text-decoration: none; letter-spacing: 1px; line-height: 100%; text-align: center; color: #5bad9a; text-transform: uppercase;\" target=\"_blank\" title=\"Legal\">Legal</a><span style=\"font-weight: bolder;\"> &middot; </span><a class=\"mcnButton \" href=\"#\" style=\"text-decoration: none; letter-spacing: 1px; line-height: 100%; text-align: center; color: #5bad9a; text-transform: uppercase;\" target=\"_blank\" title=\"Report Spam\">Report Spam</a>\r\n										</p>\r\n									</td>\r\n								</tr>\r\n							</tbody>\r\n						</table>\r\n					</td>\r\n				</tr>\r\n			</table>\r\n		</td>\r\n	</tr>\r\n</table>'),
(16, 'MAIL_FROM', 'collab@piquic.com'),
(17, 'MAIL_FROM_TEXT', 'Piquic Collab'),
(18, 'MAIL_BODY_WEKAN_LOGIN', '<link href=\'https://fonts.googleapis.com/css?family=Asap:400,400italic,700,700italic\' rel=\'stylesheet\' type=\'text/css\'>\r\n\r\n<table align=\"center\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" height=\"100%\" id=\"bodyTable\" style=\"border-collapse: collapse; mso-table-lspace: 0; mso-table-rspace: 0; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%; background-color: #fff; height: 100%; margin: 0; padding: 0; width: 100%\" width=\"100%;\">\r\n	<tr>\r\n		<td align=\"center\" id=\"bodyCell\" style=\"mso-line-height-rule: exactly;-ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%; border-top: 0;height: 100%; margin: 0; padding: 0; width: 100%;\" valign=\"top\">\r\n			<table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" class=\"templateContainer\" style=\"border-collapse: collapse; mso-table-lspace: 0; mso-table-rspace: 0;  box-shadow: 8px 13px 44px 0px #000000; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%; max-width: 600px; border: 0\" width=\"100%\">\r\n				<tr>\r\n					<td id=\"templatePreheader\" style=\"mso-line-height-rule: exactly; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%; background-color: #ffffff; border-top: 0; border-bottom: 1px solid #76aea169; padding-top: 16px; padding-bottom: 8px\" valign=\"top\">\r\n						<table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" class=\"mcnTextBlock\" style=\"border-collapse: collapse; mso-table-lspace: 0; mso-table-rspace: 0; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%; min-width:100%;\" width=\"100%\">\r\n							<tbody class=\"mcnTextBlockOuter\">\r\n								<tr>\r\n									<td class=\"mcnTextBlockInner\" style=\"mso-line-height-rule: exactly; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%\" valign=\"top\">\r\n										<table align=\"left\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" class=\"mcnTextContentContainer\" style=\"border-collapse: collapse; mso-table-lspace: 0; mso-table-rspace: 0; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%; min-width:100%;\" width=\"100%\">\r\n											<tbody>\r\n												<tr>\r\n													<td class=\"mcnTextContent\" style=\'mso-line-height-rule: exactly; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%; word-break: break-word; color: #2a2a2a; font-family: \"Asap\", Helvetica, sans-serif; font-size: 12px; line-height: 150%; text-align: center; padding-top:9px; padding-right: 18px; padding-bottom: 9px; padding-left: 18px;\' valign=\"top\">\r\n\r\n														<img align=\"none\" alt=\"Piquic\" height=\"52\" src=\"##WEBSITE_URL##/website-assets/images/logo.png\" style=\"-ms-interpolation-mode: bicubic; border: 0; outline: none; text-decoration: none; height: auto; width: 200px;  margin: 0px;\"  />\r\n													</td>\r\n												</tr>\r\n											</tbody>\r\n										</table>\r\n									</td>\r\n								</tr>\r\n							</tbody>\r\n						</table>\r\n					</td>\r\n				</tr>\r\n\r\n				<tr>\r\n					<td id=\"templateBody\" style=\"mso-line-height-rule: exactly; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%; background-color: #ffffff; border-top: 0; border-bottom: 0; padding-top: 0; padding-bottom: 20px\" valign=\"top\">\r\n\r\n						<table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" class=\"mcnButtonBlock\" style=\"border-collapse: collapse; mso-table-lspace: 0; mso-table-rspace: 0; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%; min-width:100%;\" width=\"100%\">\r\n							<tbody class=\"mcnButtonBlockOuter\">\r\n								<tr>\r\n									<td align=\"center\" class=\"mcnButtonBlockInner\" style=\"mso-line-height-rule: exactly; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%; padding-top:18px; padding-right:18px; padding-bottom:18px; padding-left:18px;\" valign=\"top\">\r\n\r\n										<table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" class=\"mcnButtonBlock\" style=\"border-collapse: collapse; mso-table-lspace: 0; mso-table-rspace: 0; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%; min-width:100%;\" width=\"100%\">\r\n											<tbody class=\"mcnButtonBlockOuter\">\r\n												<tr>\r\n													<td align=\"center\" class=\"mcnButtonBlockInner\" style=\"mso-line-height-rule: exactly; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%; padding-top:0; padding-right:18px; padding-bottom:18px; padding-left:18px;\" valign=\"top\">\r\n														<table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" class=\"mcnButtonContentContainer\" style=\"border-collapse: collapse; mso-table-lspace: 0; mso-table-rspace: 0; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%; border-collapse: separate !important;border-radius: 48px;\">\r\n															<tbody>\r\n																<tr>\r\n																	<td align=\"center\" class=\"mcnButtonContent\" style=\"mso-line-height-rule: exactly; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%; font-family: \'Asap\', Helvetica, sans-serif; font-size: 16px; padding-top:24px; padding-right:48px; padding-bottom:24px; padding-left:48px;\" valign=\"middle\">\r\n\r\n																		<p class=\"null\" style=\'color: #2a2a2a; font-family: \"Asap\", Helvetica, sans-serif; font-size: 24px; font-style: normal; line-height: 125%; letter-spacing: 1px; text-align: center; display: block; margin: 0; padding: 0 0 40px 0\'>\r\n																			Hi ##USER_NAME##,\r\n																		</p>\r\n\r\n																		<p class=\"null\" style=\'color: #2a2a2a; font-family: \"Asap\", Helvetica, sans-serif; font-size: 20px; font-style: normal; line-height: 125%; letter-spacing: 1px; text-align: center; display: block; margin: 0; padding: 0 0 40px 0\'>\r\n																			You need to click given link and login with given credentials.\r\n																		</p>\r\n\r\n																		<p class=\"null\" style=\'color: #2a2a2a; font-family: \"Asap\", Helvetica, sans-serif; font-size: 24px; font-style: normal; line-height: 125%; letter-spacing: 1px; text-align: center; display: block; margin: 0; padding: 0 0 40px 0\'>\r\n																			<b>Username</b> : ##USER_EMAIL##<br>\r\n																			<b>Password</b> : ##USER_PASSWORD##\r\n																		</p>\r\n\r\n																	</td>\r\n																</tr>\r\n															</tbody>\r\n														</table>\r\n													</td>\r\n												</tr>\r\n											</tbody>\r\n										</table>\r\n									</td>\r\n								</tr>\r\n							</tbody>\r\n						</table>\r\n\r\n						<table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" class=\"mcnButtonBlock\" style=\"border-collapse: collapse; mso-table-lspace: 0; mso-table-rspace: 0; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%; min-width:100%;\" width=\"100%\">\r\n							<tbody class=\"mcnButtonBlockOuter\">\r\n								<tr>\r\n									<td align=\"center\" class=\"mcnButtonBlockInner\" style=\"mso-line-height-rule: exactly; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%; padding: 0 50px;\" valign=\"top\">\r\n\r\n										<table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" class=\"mcnButtonBlock\" style=\"border-collapse: collapse; mso-table-lspace: 0; mso-table-rspace: 0; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%; min-width:100%; background-color: #5bad9a;\" width=\"100%\">\r\n											<tbody class=\"mcnButtonBlockOuter\">\r\n												\r\n												<tr>\r\n													<td align=\"center\" class=\"mcnButtonContent\" style=\"mso-line-height-rule: exactly; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%; font-family: \'Asap\', Helvetica, sans-serif; font-size: 16px; padding: 24px;\" valign=\"middle\">\r\n\r\n														<a class=\"mcnButton \" href=\"##DASHBOARD_LINK##\" style=\"mso-line-height-rule: exactly; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%; display: block; text-decoration: none; letter-spacing: 1px; line-height: 100%; text-align: center; text-decoration: none; color: #ffffff; text-transform: uppercase;\" target=\"_blank\" title=\"Go To Dashboard\">Go To Dashboard</a>\r\n													</td>\r\n\r\n												</tr>\r\n											</tbody>\r\n										</table>\r\n									</td>\r\n								</tr>\r\n							</tbody>\r\n						</table>\r\n\r\n					</td>\r\n				</tr>\r\n\r\n				<tr>\r\n					<td id=\"templatePreheader\" style=\"mso-line-height-rule: exactly; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%; background-color: #ffffff; border-bottom: 0; border-top: 1px solid #76aea169; padding-top: 16px; padding-bottom: 8px\" valign=\"top\">\r\n						<table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" class=\"mcnTextBlock\" style=\"border-collapse: collapse; mso-table-lspace: 0; mso-table-rspace: 0; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%; min-width:100%;\" width=\"100%\">\r\n							<tbody class=\"mcnTextBlockOuter\">\r\n								<tr>\r\n									\r\n									<td class=\"mcnTextContent\" style=\'mso-line-height-rule: exactly; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%; word-break: break-word; color: #2a2a2a; font-family: \"Asap\", Helvetica, sans-serif; font-size: 12px; line-height: 150%; text-align: center; padding-top:2px; padding-bottom: 2px;\' valign=\"top\">\r\n\r\n										<p class=\"null\" style=\'color: #2a2a2a; font-family: \"Asap\", Helvetica, sans-serif; font-size: 12px; font-style: normal; line-height: 125%; letter-spacing: 1px; text-align: center; display: block; margin: 0; padding: 0\'>\r\n\r\n											Learn More About <a class=\"mcnButton \" href=\"#\" style=\"text-decoration: none; letter-spacing: 1px; line-height: 100%; text-align: center; color: #5bad9a; text-transform: uppercase;\" target=\"_blank\" title=\"##COPY_RIGHT##\">##COPY_RIGHT##</a>\r\n										</p>\r\n\r\n										<p class=\"null\" style=\'color: #2a2a2a; font-family: \"Asap\", Helvetica, sans-serif; font-size: 12px; font-style: normal; line-height: 125%; letter-spacing: 1px; text-align: center; display: block; margin: 0; padding: 10px 0;\'>\r\n\r\n											<a class=\"mcnButton \" href=\"#\" style=\"text-decoration: none; letter-spacing: 1px; line-height: 100%; text-align: center; color: #5bad9a; text-transform: uppercase;\" target=\"_blank\" title=\"About Piquic\">About Piquic</a><span style=\"font-weight: bolder;\"> &middot; </span><a class=\"mcnButton \" href=\"#\" style=\"text-decoration: none; letter-spacing: 1px; line-height: 100%; text-align: center; color: #5bad9a; text-transform: uppercase;\" target=\"_blank\" title=\"Help\">Help</a><span style=\"font-weight: bolder;\"> &middot; </span><a class=\"mcnButton \" href=\"#\" style=\"text-decoration: none; letter-spacing: 1px; line-height: 100%; text-align: center; color: #5bad9a; text-transform: uppercase;\" target=\"_blank\" title=\"Legal\">Legal</a><span style=\"font-weight: bolder;\"> &middot; </span><a class=\"mcnButton \" href=\"#\" style=\"text-decoration: none; letter-spacing: 1px; line-height: 100%; text-align: center; color: #5bad9a; text-transform: uppercase;\" target=\"_blank\" title=\"Report Spam\">Report Spam</a>\r\n										</p>\r\n									</td>\r\n								</tr>\r\n							</tbody>\r\n						</table>\r\n					</td>\r\n				</tr>\r\n			</table>\r\n		</td>\r\n	</tr>\r\n</table>'),
(19, 'REPORTICO_LINK', 'http://collabreport.piquic.com'),
(20, 'MAIL_BODY_NOTIFY_UPLOADFILE_REVISION', '<link href=\'https://fonts.googleapis.com/css?family=Asap:400,400italic,700,700italic\' rel=\'stylesheet\' type=\'text/css\'>\r\n\r\n<table align=\"center\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" height=\"100%\" id=\"bodyTable\" style=\"border-collapse: collapse; mso-table-lspace: 0; mso-table-rspace: 0; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%; background-color: #fff; height: 100%; margin: 0; padding: 0; width: 100%\" width=\"100%;\">\r\n	<tr>\r\n		<td align=\"center\" id=\"bodyCell\" style=\"mso-line-height-rule: exactly;-ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%; border-top: 0;height: 100%; margin: 0; padding: 0; width: 100%;\" valign=\"top\">\r\n			<table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" class=\"templateContainer\" style=\"border-collapse: collapse; mso-table-lspace: 0; mso-table-rspace: 0;  box-shadow: 8px 13px 44px 0px #000000; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%; max-width: 600px; border: 0\" width=\"100%\">\r\n				<tr>\r\n					<td id=\"templatePreheader\" style=\"mso-line-height-rule: exactly; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%; background-color: #ffffff; border-top: 0; border-bottom: 1px solid #76aea169; padding-top: 16px; padding-bottom: 8px\" valign=\"top\">\r\n						<table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" class=\"mcnTextBlock\" style=\"border-collapse: collapse; mso-table-lspace: 0; mso-table-rspace: 0; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%; min-width:100%;\" width=\"100%\">\r\n							<tbody class=\"mcnTextBlockOuter\">\r\n								<tr>\r\n									<td class=\"mcnTextBlockInner\" style=\"mso-line-height-rule: exactly; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%\" valign=\"top\">\r\n										<table align=\"left\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" class=\"mcnTextContentContainer\" style=\"border-collapse: collapse; mso-table-lspace: 0; mso-table-rspace: 0; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%; min-width:100%;\" width=\"100%\">\r\n											<tbody>\r\n												<tr>\r\n													<td class=\"mcnTextContent\" style=\'mso-line-height-rule: exactly; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%; word-break: break-word; color: #2a2a2a; font-family: \"Asap\", Helvetica, sans-serif; font-size: 12px; line-height: 150%; text-align: center; padding-top:9px; padding-right: 18px; padding-bottom: 9px; padding-left: 18px;\' valign=\"top\">\r\n\r\n														<img align=\"none\" alt=\"Piquic\" height=\"52\" src=\"##WEBSITE_URL##/website-assets/images/logo.png\" style=\"-ms-interpolation-mode: bicubic; border: 0; outline: none; text-decoration: none; height: auto; width: 200px;  margin: 0px;\"  />\r\n													</td>\r\n												</tr>\r\n											</tbody>\r\n										</table>\r\n									</td>\r\n								</tr>\r\n							</tbody>\r\n						</table>\r\n					</td>\r\n				</tr>\r\n\r\n				<tr>\r\n					<td id=\"templateBody\" style=\"mso-line-height-rule: exactly; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%; background-color: #ffffff; border-top: 0; border-bottom: 0; padding-top: 0; padding-bottom: 20px\" valign=\"top\">\r\n\r\n						<table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" class=\"mcnButtonBlock\" style=\"border-collapse: collapse; mso-table-lspace: 0; mso-table-rspace: 0; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%; min-width:100%;\" width=\"100%\">\r\n							<tbody class=\"mcnButtonBlockOuter\">\r\n								<tr>\r\n									<td align=\"center\" class=\"mcnButtonBlockInner\" style=\"mso-line-height-rule: exactly; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%; padding-top:18px; padding-right:18px; padding-bottom:18px; padding-left:18px;\" valign=\"top\">\r\n\r\n										<table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" class=\"mcnButtonBlock\" style=\"border-collapse: collapse; mso-table-lspace: 0; mso-table-rspace: 0; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%; min-width:100%;\" width=\"100%\">\r\n											<tbody class=\"mcnButtonBlockOuter\">\r\n												<tr>\r\n													<td align=\"center\" class=\"mcnButtonBlockInner\" style=\"mso-line-height-rule: exactly; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%; padding-top:0; padding-right:18px; padding-bottom:18px; padding-left:18px;\" valign=\"top\">\r\n														<table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" class=\"mcnButtonContentContainer\" style=\"border-collapse: collapse; mso-table-lspace: 0; mso-table-rspace: 0; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%; border-collapse: separate !important;border-radius: 48px;\">\r\n															<tbody>\r\n																<tr>\r\n																	<td align=\"center\" class=\"mcnButtonContent\" style=\"mso-line-height-rule: exactly; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%; font-family: \'Asap\', Helvetica, sans-serif; font-size: 16px; padding-top:24px; padding-right:48px; padding-bottom:24px; padding-left:48px;\" valign=\"middle\">\r\n\r\n																		<p class=\"null\" style=\'color: #2a2a2a; font-family: \"Asap\", Helvetica, sans-serif; font-size: 24px; font-style: normal; line-height: 125%; letter-spacing: 1px; text-align: center; display: block; margin: 0; padding: 0 0 40px 0\'>\r\n																			New files have been<br> send for revision in<br> \"<i>##PROJECT_NAME##</i>\"\r\n																		</p>\r\n\r\n																		<p class=\"null\" style=\'color: #2a2a2a; font-family: \"Asap\", Helvetica, sans-serif; font-size: 20px; font-style: normal; line-height: 125%; letter-spacing: 1px; text-align: center; display: block; margin: 0; padding: 0 0 40px 0\'>\r\n																			View all reviews on your dashboard\r\n																		</p>\r\n																		<img align=\"none\" alt=\"Review Done\" src=\"##WEBSITE_URL##/website-assets/images/review-done.jpg\" style=\"-ms-interpolation-mode: bicubic; border: 0; outline: none; text-decoration: none; height: auto; width: 450px;  margin: 0px; box-shadow: 0 1px 10px 1px #5bad9a69;\"  />	\r\n\r\n																																			\r\n																	</td>\r\n																</tr>\r\n															</tbody>\r\n														</table>\r\n													</td>\r\n												</tr>\r\n											</tbody>\r\n										</table>\r\n									</td>\r\n								</tr>\r\n							</tbody>\r\n						</table>\r\n\r\n						<table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" class=\"mcnButtonBlock\" style=\"border-collapse: collapse; mso-table-lspace: 0; mso-table-rspace: 0; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%; min-width:100%;\" width=\"100%\">\r\n							<tbody class=\"mcnButtonBlockOuter\">\r\n								<tr>\r\n									<td align=\"center\" class=\"mcnButtonBlockInner\" style=\"mso-line-height-rule: exactly; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%; padding: 0 50px;\" valign=\"top\">\r\n\r\n										<table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" class=\"mcnButtonBlock\" style=\"border-collapse: collapse; mso-table-lspace: 0; mso-table-rspace: 0; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%; min-width:100%; background-color: #5bad9a;\" width=\"100%\">\r\n											<tbody class=\"mcnButtonBlockOuter\">\r\n												\r\n												<tr>\r\n													<td align=\"center\" class=\"mcnButtonContent\" style=\"mso-line-height-rule: exactly; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%; font-family: \'Asap\', Helvetica, sans-serif; font-size: 16px; padding: 24px;\" valign=\"middle\">\r\n\r\n														<a class=\"mcnButton \" href=\"##DASHBOARD_LINK##\" style=\"mso-line-height-rule: exactly; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%; display: block; text-decoration: none; letter-spacing: 1px; line-height: 100%; text-align: center; text-decoration: none; color: #ffffff; text-transform: uppercase;\" target=\"_blank\" title=\"Go To Dashboard\">Go To Dashboard</a>\r\n													</td>\r\n\r\n												</tr>\r\n											</tbody>\r\n										</table>\r\n									</td>\r\n								</tr>\r\n							</tbody>\r\n						</table>\r\n\r\n					</td>\r\n				</tr>\r\n\r\n				<tr>\r\n					<td id=\"templatePreheader\" style=\"mso-line-height-rule: exactly; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%; background-color: #ffffff; border-bottom: 0; border-top: 1px solid #76aea169; padding-top: 16px; padding-bottom: 8px\" valign=\"top\">\r\n						<table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" class=\"mcnTextBlock\" style=\"border-collapse: collapse; mso-table-lspace: 0; mso-table-rspace: 0; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%; min-width:100%;\" width=\"100%\">\r\n							<tbody class=\"mcnTextBlockOuter\">\r\n								<tr>\r\n									\r\n									<td class=\"mcnTextContent\" style=\'mso-line-height-rule: exactly; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%; word-break: break-word; color: #2a2a2a; font-family: \"Asap\", Helvetica, sans-serif; font-size: 12px; line-height: 150%; text-align: center; padding-top:2px; padding-bottom: 2px;\' valign=\"top\">\r\n\r\n										<p class=\"null\" style=\'color: #2a2a2a; font-family: \"Asap\", Helvetica, sans-serif; font-size: 12px; font-style: normal; line-height: 125%; letter-spacing: 1px; text-align: center; display: block; margin: 0; padding: 0\'>\r\n\r\n											Learn More About <a class=\"mcnButton \" href=\"#\" style=\"text-decoration: none; letter-spacing: 1px; line-height: 100%; text-align: center; color: #5bad9a; text-transform: uppercase;\" target=\"_blank\" title=\"##COPY_RIGHT##\">##COPY_RIGHT##</a>\r\n										</p>\r\n\r\n										<p class=\"null\" style=\'color: #2a2a2a; font-family: \"Asap\", Helvetica, sans-serif; font-size: 12px; font-style: normal; line-height: 125%; letter-spacing: 1px; text-align: center; display: block; margin: 0; padding: 10px 0;\'>\r\n\r\n											<a class=\"mcnButton \" href=\"#\" style=\"text-decoration: none; letter-spacing: 1px; line-height: 100%; text-align: center; color: #5bad9a; text-transform: uppercase;\" target=\"_blank\" title=\"About Piquic\">About Piquic</a><span style=\"font-weight: bolder;\"> &middot; </span><a class=\"mcnButton \" href=\"#\" style=\"text-decoration: none; letter-spacing: 1px; line-height: 100%; text-align: center; color: #5bad9a; text-transform: uppercase;\" target=\"_blank\" title=\"Help\">Help</a><span style=\"font-weight: bolder;\"> &middot; </span><a class=\"mcnButton \" href=\"#\" style=\"text-decoration: none; letter-spacing: 1px; line-height: 100%; text-align: center; color: #5bad9a; text-transform: uppercase;\" target=\"_blank\" title=\"Legal\">Legal</a><span style=\"font-weight: bolder;\"> &middot; </span><a class=\"mcnButton \" href=\"#\" style=\"text-decoration: none; letter-spacing: 1px; line-height: 100%; text-align: center; color: #5bad9a; text-transform: uppercase;\" target=\"_blank\" title=\"Report Spam\">Report Spam</a>\r\n										</p>\r\n									</td>\r\n								</tr>\r\n							</tbody>\r\n						</table>\r\n					</td>\r\n				</tr>\r\n			</table>\r\n		</td>\r\n	</tr>\r\n</table>');
INSERT INTO `wc_settings` (`setting_id`, `setting_key`, `setting_value`) VALUES
(21, 'MAIL_BODY_NOTIFY_REVISION_FILEUPLOAD', '<link href=\'https://fonts.googleapis.com/css?family=Asap:400,400italic,700,700italic\' rel=\'stylesheet\' type=\'text/css\'>\r\n\r\n<table align=\"center\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" height=\"100%\" id=\"bodyTable\" style=\"border-collapse: collapse; mso-table-lspace: 0; mso-table-rspace: 0; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%; background-color: #fff; height: 100%; margin: 0; padding: 0; width: 100%\" width=\"100%;\">\r\n	<tr>\r\n		<td align=\"center\" id=\"bodyCell\" style=\"mso-line-height-rule: exactly;-ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%; border-top: 0;height: 100%; margin: 0; padding: 0; width: 100%;\" valign=\"top\">\r\n			<table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" class=\"templateContainer\" style=\"border-collapse: collapse; mso-table-lspace: 0; mso-table-rspace: 0;  box-shadow: 8px 13px 44px 0px #000000; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%; max-width: 600px; border: 0\" width=\"100%\">\r\n				<tr>\r\n					<td id=\"templatePreheader\" style=\"mso-line-height-rule: exactly; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%; background-color: #ffffff; border-top: 0; border-bottom: 1px solid #76aea169; padding-top: 16px; padding-bottom: 8px\" valign=\"top\">\r\n						<table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" class=\"mcnTextBlock\" style=\"border-collapse: collapse; mso-table-lspace: 0; mso-table-rspace: 0; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%; min-width:100%;\" width=\"100%\">\r\n							<tbody class=\"mcnTextBlockOuter\">\r\n								<tr>\r\n									<td class=\"mcnTextBlockInner\" style=\"mso-line-height-rule: exactly; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%\" valign=\"top\">\r\n										<table align=\"left\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" class=\"mcnTextContentContainer\" style=\"border-collapse: collapse; mso-table-lspace: 0; mso-table-rspace: 0; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%; min-width:100%;\" width=\"100%\">\r\n											<tbody>\r\n												<tr>\r\n													<td class=\"mcnTextContent\" style=\'mso-line-height-rule: exactly; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%; word-break: break-word; color: #2a2a2a; font-family: \"Asap\", Helvetica, sans-serif; font-size: 12px; line-height: 150%; text-align: center; padding-top:9px; padding-right: 18px; padding-bottom: 9px; padding-left: 18px;\' valign=\"top\">\r\n\r\n														<img align=\"none\" alt=\"Piquic\" height=\"52\" src=\"##WEBSITE_URL##/website-assets/images/logo.png\" style=\"-ms-interpolation-mode: bicubic; border: 0; outline: none; text-decoration: none; height: auto; width: 200px;  margin: 0px;\"  />\r\n													</td>\r\n												</tr>\r\n											</tbody>\r\n										</table>\r\n									</td>\r\n								</tr>\r\n							</tbody>\r\n						</table>\r\n					</td>\r\n				</tr>\r\n\r\n				<tr>\r\n					<td id=\"templateBody\" style=\"mso-line-height-rule: exactly; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%; background-color: #ffffff; border-top: 0; border-bottom: 0; padding-top: 0; padding-bottom: 20px\" valign=\"top\">\r\n\r\n						<table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" class=\"mcnButtonBlock\" style=\"border-collapse: collapse; mso-table-lspace: 0; mso-table-rspace: 0; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%; min-width:100%;\" width=\"100%\">\r\n							<tbody class=\"mcnButtonBlockOuter\">\r\n								<tr>\r\n									<td align=\"center\" class=\"mcnButtonBlockInner\" style=\"mso-line-height-rule: exactly; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%; padding-top:18px; padding-right:18px; padding-bottom:18px; padding-left:18px;\" valign=\"top\">\r\n\r\n										<table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" class=\"mcnButtonBlock\" style=\"border-collapse: collapse; mso-table-lspace: 0; mso-table-rspace: 0; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%; min-width:100%;\" width=\"100%\">\r\n											<tbody class=\"mcnButtonBlockOuter\">\r\n												<tr>\r\n													<td align=\"center\" class=\"mcnButtonBlockInner\" style=\"mso-line-height-rule: exactly; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%; padding-top:0; padding-right:18px; padding-bottom:18px; padding-left:18px;\" valign=\"top\">\r\n														<table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" class=\"mcnButtonContentContainer\" style=\"border-collapse: collapse; mso-table-lspace: 0; mso-table-rspace: 0; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%; border-collapse: separate !important;border-radius: 48px;\">\r\n															<tbody>\r\n																<tr>\r\n																	<td align=\"center\" class=\"mcnButtonContent\" style=\"mso-line-height-rule: exactly; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%; font-family: \'Asap\', Helvetica, sans-serif; font-size: 16px; padding-top:24px; padding-right:48px; padding-bottom:24px; padding-left:48px;\" valign=\"middle\">\r\n\r\n																		<p class=\"null\" style=\'color: #2a2a2a; font-family: \"Asap\", Helvetica, sans-serif; font-size: 24px; font-style: normal; line-height: 125%; letter-spacing: 1px; text-align: center; display: block; margin: 0; padding: 0 0 40px 0\'>\r\n																			New files have been revised in<br> \"<i>##PROJECT_NAME##</i>\"\r\n																		</p>\r\n\r\n																		<p class=\"null\" style=\'color: #2a2a2a; font-family: \"Asap\", Helvetica, sans-serif; font-size: 20px; font-style: normal; line-height: 125%; letter-spacing: 1px; text-align: center; display: block; margin: 0; padding: 0 0 40px 0\'>\r\n																			View all reviews on your dashboard\r\n																		</p>\r\n																		<img align=\"none\" alt=\"Review Done\" src=\"##WEBSITE_URL##/website-assets/images/review-done.jpg\" style=\"-ms-interpolation-mode: bicubic; border: 0; outline: none; text-decoration: none; height: auto; width: 450px;  margin: 0px; box-shadow: 0 1px 10px 1px #5bad9a69;\"  />	\r\n\r\n																																			\r\n																	</td>\r\n																</tr>\r\n															</tbody>\r\n														</table>\r\n													</td>\r\n												</tr>\r\n											</tbody>\r\n										</table>\r\n									</td>\r\n								</tr>\r\n							</tbody>\r\n						</table>\r\n\r\n						<table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" class=\"mcnButtonBlock\" style=\"border-collapse: collapse; mso-table-lspace: 0; mso-table-rspace: 0; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%; min-width:100%;\" width=\"100%\">\r\n							<tbody class=\"mcnButtonBlockOuter\">\r\n								<tr>\r\n									<td align=\"center\" class=\"mcnButtonBlockInner\" style=\"mso-line-height-rule: exactly; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%; padding: 0 50px;\" valign=\"top\">\r\n\r\n										<table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" class=\"mcnButtonBlock\" style=\"border-collapse: collapse; mso-table-lspace: 0; mso-table-rspace: 0; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%; min-width:100%; background-color: #5bad9a;\" width=\"100%\">\r\n											<tbody class=\"mcnButtonBlockOuter\">\r\n												\r\n												<tr>\r\n													<td align=\"center\" class=\"mcnButtonContent\" style=\"mso-line-height-rule: exactly; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%; font-family: \'Asap\', Helvetica, sans-serif; font-size: 16px; padding: 24px;\" valign=\"middle\">\r\n\r\n														<a class=\"mcnButton \" href=\"##DASHBOARD_LINK##\" style=\"mso-line-height-rule: exactly; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%; display: block; text-decoration: none; letter-spacing: 1px; line-height: 100%; text-align: center; text-decoration: none; color: #ffffff; text-transform: uppercase;\" target=\"_blank\" title=\"Go To Dashboard\">Go To Dashboard</a>\r\n													</td>\r\n\r\n												</tr>\r\n											</tbody>\r\n										</table>\r\n									</td>\r\n								</tr>\r\n							</tbody>\r\n						</table>\r\n\r\n					</td>\r\n				</tr>\r\n\r\n				<tr>\r\n					<td id=\"templatePreheader\" style=\"mso-line-height-rule: exactly; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%; background-color: #ffffff; border-bottom: 0; border-top: 1px solid #76aea169; padding-top: 16px; padding-bottom: 8px\" valign=\"top\">\r\n						<table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" class=\"mcnTextBlock\" style=\"border-collapse: collapse; mso-table-lspace: 0; mso-table-rspace: 0; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%; min-width:100%;\" width=\"100%\">\r\n							<tbody class=\"mcnTextBlockOuter\">\r\n								<tr>\r\n									\r\n									<td class=\"mcnTextContent\" style=\'mso-line-height-rule: exactly; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%; word-break: break-word; color: #2a2a2a; font-family: \"Asap\", Helvetica, sans-serif; font-size: 12px; line-height: 150%; text-align: center; padding-top:2px; padding-bottom: 2px;\' valign=\"top\">\r\n\r\n										<p class=\"null\" style=\'color: #2a2a2a; font-family: \"Asap\", Helvetica, sans-serif; font-size: 12px; font-style: normal; line-height: 125%; letter-spacing: 1px; text-align: center; display: block; margin: 0; padding: 0\'>\r\n\r\n											Learn More About <a class=\"mcnButton \" href=\"#\" style=\"text-decoration: none; letter-spacing: 1px; line-height: 100%; text-align: center; color: #5bad9a; text-transform: uppercase;\" target=\"_blank\" title=\"##COPY_RIGHT##\">##COPY_RIGHT##</a>\r\n										</p>\r\n\r\n										<p class=\"null\" style=\'color: #2a2a2a; font-family: \"Asap\", Helvetica, sans-serif; font-size: 12px; font-style: normal; line-height: 125%; letter-spacing: 1px; text-align: center; display: block; margin: 0; padding: 10px 0;\'>\r\n\r\n											<a class=\"mcnButton \" href=\"#\" style=\"text-decoration: none; letter-spacing: 1px; line-height: 100%; text-align: center; color: #5bad9a; text-transform: uppercase;\" target=\"_blank\" title=\"About Piquic\">About Piquic</a><span style=\"font-weight: bolder;\"> &middot; </span><a class=\"mcnButton \" href=\"#\" style=\"text-decoration: none; letter-spacing: 1px; line-height: 100%; text-align: center; color: #5bad9a; text-transform: uppercase;\" target=\"_blank\" title=\"Help\">Help</a><span style=\"font-weight: bolder;\"> &middot; </span><a class=\"mcnButton \" href=\"#\" style=\"text-decoration: none; letter-spacing: 1px; line-height: 100%; text-align: center; color: #5bad9a; text-transform: uppercase;\" target=\"_blank\" title=\"Legal\">Legal</a><span style=\"font-weight: bolder;\"> &middot; </span><a class=\"mcnButton \" href=\"#\" style=\"text-decoration: none; letter-spacing: 1px; line-height: 100%; text-align: center; color: #5bad9a; text-transform: uppercase;\" target=\"_blank\" title=\"Report Spam\">Report Spam</a>\r\n										</p>\r\n									</td>\r\n								</tr>\r\n							</tbody>\r\n						</table>\r\n					</td>\r\n				</tr>\r\n			</table>\r\n		</td>\r\n	</tr>\r\n</table>'),
(22, 'MAIL_BODY_ADDED_BRIEF', '<link href=\'https://fonts.googleapis.com/css?family=Asap:400,400italic,700,700italic\' rel=\'stylesheet\' type=\'text/css\'>\r\n\r\n<table align=\"center\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" height=\"100%\" id=\"bodyTable\" style=\"border-collapse: collapse; mso-table-lspace: 0; mso-table-rspace: 0; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%; background-color: #fff; height: 100%; margin: 0; padding: 0; width: 100%\" width=\"100%;\">\r\n	<tr>\r\n		<td align=\"center\" id=\"bodyCell\" style=\"mso-line-height-rule: exactly;-ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%; border-top: 0;height: 100%; margin: 0; padding: 0; width: 100%;\" valign=\"top\">\r\n			<table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" class=\"templateContainer\" style=\"border-collapse: collapse; mso-table-lspace: 0; mso-table-rspace: 0;  box-shadow: 8px 13px 44px 0px #000000; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%; max-width: 600px; border: 0\" width=\"100%\">\r\n				<tr>\r\n					<td id=\"templatePreheader\" style=\"mso-line-height-rule: exactly; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%; background-color: #ffffff; border-top: 0; border-bottom: 1px solid #76aea169; padding-top: 16px; padding-bottom: 8px\" valign=\"top\">\r\n						<table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" class=\"mcnTextBlock\" style=\"border-collapse: collapse; mso-table-lspace: 0; mso-table-rspace: 0; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%; min-width:100%;\" width=\"100%\">\r\n							<tbody class=\"mcnTextBlockOuter\">\r\n								<tr>\r\n									<td class=\"mcnTextBlockInner\" style=\"mso-line-height-rule: exactly; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%\" valign=\"top\">\r\n										<table align=\"left\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" class=\"mcnTextContentContainer\" style=\"border-collapse: collapse; mso-table-lspace: 0; mso-table-rspace: 0; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%; min-width:100%;\" width=\"100%\">\r\n											<tbody>\r\n												<tr>\r\n													<td class=\"mcnTextContent\" style=\'mso-line-height-rule: exactly; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%; word-break: break-word; color: #2a2a2a; font-family: \"Asap\", Helvetica, sans-serif; font-size: 12px; line-height: 150%; text-align: center; padding-top:9px; padding-right: 18px; padding-bottom: 9px; padding-left: 18px;\' valign=\"top\">\r\n\r\n														<img align=\"none\" alt=\"Piquic\" height=\"52\" src=\"##WEBSITE_URL##/website-assets/images/logo.png\" style=\"-ms-interpolation-mode: bicubic; border: 0; outline: none; text-decoration: none; height: auto; width: 200px;  margin: 0px;\"  />\r\n													</td>\r\n												</tr>\r\n											</tbody>\r\n										</table>\r\n									</td>\r\n								</tr>\r\n							</tbody>\r\n						</table>\r\n					</td>\r\n				</tr>\r\n\r\n				<tr>\r\n					<td id=\"templateBody\" style=\"mso-line-height-rule: exactly; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%; background-color: #ffffff; border-top: 0; border-bottom: 0; padding-top: 0; padding-bottom: 20px\" valign=\"top\">\r\n\r\n						<table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" class=\"mcnButtonBlock\" style=\"border-collapse: collapse; mso-table-lspace: 0; mso-table-rspace: 0; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%; min-width:100%;\" width=\"100%\">\r\n							<tbody class=\"mcnButtonBlockOuter\">\r\n								<tr>\r\n									<td align=\"center\" class=\"mcnButtonBlockInner\" style=\"mso-line-height-rule: exactly; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%; padding-top:18px; padding-right:18px; padding-bottom:18px; padding-left:18px;\" valign=\"top\">\r\n\r\n										<table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" class=\"mcnButtonBlock\" style=\"border-collapse: collapse; mso-table-lspace: 0; mso-table-rspace: 0; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%; min-width:100%;\" width=\"100%\">\r\n											<tbody class=\"mcnButtonBlockOuter\">\r\n												<tr>\r\n													<td align=\"center\" class=\"mcnButtonBlockInner\" style=\"mso-line-height-rule: exactly; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%; padding-top:0; padding-right:18px; padding-bottom:18px; padding-left:18px;\" valign=\"top\">\r\n														<table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" class=\"mcnButtonContentContainer\" style=\"border-collapse: collapse; mso-table-lspace: 0; mso-table-rspace: 0; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%; border-collapse: separate !important;border-radius: 48px;\">\r\n															<tbody>\r\n																<tr>\r\n																	<td align=\"center\" class=\"mcnButtonContent\" style=\"mso-line-height-rule: exactly; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%; font-family: \'Asap\', Helvetica, sans-serif; font-size: 16px; padding-top:24px; padding-right:48px; padding-bottom:24px; padding-left:48px;\" valign=\"middle\">\r\n\r\n																		<p class=\"null\" style=\'color: #2a2a2a; font-family: \"Asap\", Helvetica, sans-serif; font-size: 24px; font-style: normal; line-height: 125%; letter-spacing: 1px; text-align: center; display: block; margin: 0; padding: 0 0 40px 0\'>\r\n																			A new brief has been uploaded<br>for the project<br>\"<i>##PROJECT_NAME##</i>\".\r\n																		</p>\r\n\r\n																		<!-- <p class=\"null\" style=\'color: #2a2a2a; font-family: \"Asap\", Helvetica, sans-serif; font-size: 20px; font-style: normal; line-height: 125%; letter-spacing: 1px; text-align: center; display: block; margin: 0; padding: 0 0 40px 0\'>\r\n																			Provide your feedback for this project.\r\n																		</p>\r\n\r\n																		<img align=\"none\" alt=\"Add Feedback\" src=\"add-feedback.gif\" style=\"-ms-interpolation-mode: bicubic; border: 0; outline: none; text-decoration: none; height: auto; width: 450px;  margin: 0px; box-shadow: 0 1px 10px 1px #5bad9a69;\"  /> -->																\r\n																	</td>\r\n																</tr>\r\n															</tbody>\r\n														</table>\r\n													</td>\r\n												</tr>\r\n											</tbody>\r\n										</table>\r\n									</td>\r\n								</tr>\r\n							</tbody>\r\n						</table>\r\n\r\n						<table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" class=\"mcnButtonBlock\" style=\"border-collapse: collapse; mso-table-lspace: 0; mso-table-rspace: 0; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%; min-width:100%;\" width=\"100%\">\r\n							<tbody class=\"mcnButtonBlockOuter\">\r\n								<tr>\r\n									<td align=\"center\" class=\"mcnButtonBlockInner\" style=\"mso-line-height-rule: exactly; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%; padding: 0 50px;\" valign=\"top\">\r\n\r\n										<table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" class=\"mcnButtonBlock\" style=\"border-collapse: collapse; mso-table-lspace: 0; mso-table-rspace: 0; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%; min-width:100%; background-color: #5bad9a;\" width=\"100%\">\r\n											<tbody class=\"mcnButtonBlockOuter\">\r\n												\r\n												<tr>\r\n													<td align=\"center\" class=\"mcnButtonContent\" style=\"mso-line-height-rule: exactly; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%; font-family: \'Asap\', Helvetica, sans-serif; font-size: 16px; padding: 24px;\" valign=\"middle\">\r\n\r\n														<a class=\"mcnButton \" href=\"##DASHBOARD_LINK##\" style=\"mso-line-height-rule: exactly; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%; display: block; text-decoration: none; letter-spacing: 1px; line-height: 100%; text-align: center; text-decoration: none; color: #ffffff; text-transform: uppercase;\" target=\"_blank\" title=\"Go To Dashboard\">Go To Dashboard</a>\r\n													</td>\r\n\r\n												</tr>\r\n											</tbody>\r\n										</table>\r\n									</td>\r\n								</tr>\r\n							</tbody>\r\n						</table>\r\n\r\n					</td>\r\n				</tr>\r\n\r\n				<tr>\r\n					<td id=\"templatePreheader\" style=\"mso-line-height-rule: exactly; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%; background-color: #ffffff; border-bottom: 0; border-top: 1px solid #76aea169; padding-top: 16px; padding-bottom: 8px\" valign=\"top\">\r\n						<table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" class=\"mcnTextBlock\" style=\"border-collapse: collapse; mso-table-lspace: 0; mso-table-rspace: 0; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%; min-width:100%;\" width=\"100%\">\r\n							<tbody class=\"mcnTextBlockOuter\">\r\n								<tr>\r\n									\r\n									<td class=\"mcnTextContent\" style=\'mso-line-height-rule: exactly; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%; word-break: break-word; color: #2a2a2a; font-family: \"Asap\", Helvetica, sans-serif; font-size: 12px; line-height: 150%; text-align: center; padding-top:2px; padding-bottom: 2px;\' valign=\"top\">\r\n\r\n										<p class=\"null\" style=\'color: #2a2a2a; font-family: \"Asap\", Helvetica, sans-serif; font-size: 12px; font-style: normal; line-height: 125%; letter-spacing: 1px; text-align: center; display: block; margin: 0; padding: 0\'>\r\n\r\n											Learn More About <a class=\"mcnButton \" href=\"#\" style=\"text-decoration: none; letter-spacing: 1px; line-height: 100%; text-align: center; color: #5bad9a; text-transform: uppercase;\" target=\"_blank\" title=\"##COPY_RIGHT##\">##COPY_RIGHT##</a>\r\n										</p>\r\n\r\n										<p class=\"null\" style=\'color: #2a2a2a; font-family: \"Asap\", Helvetica, sans-serif; font-size: 12px; font-style: normal; line-height: 125%; letter-spacing: 1px; text-align: center; display: block; margin: 0; padding: 10px 0;\'>\r\n\r\n											<a class=\"mcnButton \" href=\"#\" style=\"text-decoration: none; letter-spacing: 1px; line-height: 100%; text-align: center; color: #5bad9a; text-transform: uppercase;\" target=\"_blank\" title=\"About Piquic\">About Piquic</a><span style=\"font-weight: bolder;\"> &middot; </span><a class=\"mcnButton \" href=\"#\" style=\"text-decoration: none; letter-spacing: 1px; line-height: 100%; text-align: center; color: #5bad9a; text-transform: uppercase;\" target=\"_blank\" title=\"Help\">Help</a><span style=\"font-weight: bolder;\"> &middot; </span><a class=\"mcnButton \" href=\"#\" style=\"text-decoration: none; letter-spacing: 1px; line-height: 100%; text-align: center; color: #5bad9a; text-transform: uppercase;\" target=\"_blank\" title=\"Legal\">Legal</a><span style=\"font-weight: bolder;\"> &middot; </span><a class=\"mcnButton \" href=\"#\" style=\"text-decoration: none; letter-spacing: 1px; line-height: 100%; text-align: center; color: #5bad9a; text-transform: uppercase;\" target=\"_blank\" title=\"Report Spam\">Report Spam</a>\r\n										</p>\r\n									</td>\r\n								</tr>\r\n							</tbody>\r\n						</table>\r\n					</td>\r\n				</tr>\r\n			</table>\r\n		</td>\r\n	</tr>\r\n</table>');

-- --------------------------------------------------------

--
-- Table structure for table `wc_users`
--

CREATE TABLE `wc_users` (
  `user_id` int(11) NOT NULL,
  `wekan_member_id` text NOT NULL,
  `user_type_id` int(11) NOT NULL,
  `client_id` int(11) NOT NULL,
  `brand_id` text NOT NULL,
  `user_name` varchar(145) DEFAULT NULL,
  `user_company` text NOT NULL,
  `user_address` varchar(245) DEFAULT NULL,
  `user_email` varchar(145) DEFAULT NULL,
  `user_telephone` varchar(45) DEFAULT NULL,
  `user_mobile` varchar(45) DEFAULT NULL,
  `user_password` varchar(45) DEFAULT NULL,
  `user_password_text` varchar(45) NOT NULL,
  `brand_access` text NOT NULL,
  `status` enum('Active','Inactive') DEFAULT NULL,
  `deleted` enum('0','1') NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `wc_users`
--

INSERT INTO `wc_users` (`user_id`, `wekan_member_id`, `user_type_id`, `client_id`, `brand_id`, `user_name`, `user_company`, `user_address`, `user_email`, `user_telephone`, `user_mobile`, `user_password`, `user_password_text`, `brand_access`, `status`, `deleted`) VALUES
(1, 'wkwAJLwkeYb68t3ra', 2, 1, '0', 'meri_admin', '', NULL, 'meri_admin@gmail.com', NULL, '3455653886', '7c4a8d09ca3762af61e59520943dc26494f8941b', 'MTIzNDU2', '', 'Active', '0'),
(2, 'MgeLhBTCgXq9pC78o', 3, 1, '0', 'meri_pm', '', NULL, 'meri_pm@gmail.com', NULL, '3455676445', '7c4a8d09ca3762af61e59520943dc26494f8941b', 'MTIzNDU2', '', 'Active', '0'),
(3, 'hg8ihTLBW6AktcxWD', 4, 1, '1', 'meri_bm', '', NULL, 'meri_bm@gmail.com', NULL, '3455678776', '7c4a8d09ca3762af61e59520943dc26494f8941b', 'MTIzNDU2', '', 'Active', '0'),
(4, 'WjvQ8qeacxrs5Ry55', 2, 2, '0', 'admin.zara@inintra.com', '', NULL, 'admin.zara@inintra.com', NULL, '990196221991', '6bc613ef7e45e76401f835edf1a221e482b8d962', 'QWFBYTIxMjE=', '', 'Active', '0'),
(5, 'yuC2gqdidpGTnEiSA', 3, 2, '0', 'pm.zara@inintra.com', '', NULL, 'pm.zara@inintra.com', NULL, '99019622199999', '6bc613ef7e45e76401f835edf1a221e482b8d962', 'QWFBYTIxMjE=', '', 'Active', '0'),
(6, '3kqbqGHefW7QLFjFR', 4, 2, ',2,7,', 'bmaaaaaa.zara@inintra.com', '', NULL, 'bm.zara@inintra.com', NULL, '990196221999998', '6bc613ef7e45e76401f835edf1a221e482b8d962', 'QWFBYTIxMjE=', '', 'Active', '0'),
(7, '5PmcxJrywNyHiWWk9', 4, 1, ',9,11,10,12,1,14,13,5,6,', 'india harmony demo account', '', NULL, 'india.harmony@gmail.com', NULL, ' 919599598833', 'f7c3bc1d808e04732adf679965ccc34ca7ae3441', 'MTIzNDU2Nzg5', '', 'Active', '0'),
(9, '3nRSkGfb43MxNZXzB', 3, 3, '0', 'Pm demo account', '', NULL, 'serge.gianchandani@gmail.com', NULL, '9599598834', 'f7c3bc1d808e04732adf679965ccc34ca7ae3441', 'MTIzNDU2Nzg5', '', 'Active', '0'),
(11, 'r4nPD3pEuK64r6Jt6', 2, 1, '0', 'serge piquic admin marico', '', NULL, 'serge@piquic.com', NULL, ' 919599598833', 'bfe54caa6d483cc3887dce9d1b8eb91408f1ea7a', 'OTg3NjU0MzIx', '', 'Active', '0'),
(10, '9AS3mpYvi2NhuYFgj', 3, 1, '0', 'Shikha- PM Marico', '', NULL, 'shikha@mokshaproductions.in', NULL, ' 919999220427', 'bfe54caa6d483cc3887dce9d1b8eb91408f1ea7a', 'OTg3NjU0MzIx', '', 'Active', '0'),
(12, 'XKFcvn7RYZcxtQ4HW', 4, 1, '5', 'Ragini Sabnavis', '', NULL, 'ragini.sabnavis@marico.com', NULL, ' 919999999999', 'd5db3b5d69672e94d31a23ffd7554747871abb24', 'VWhqdTU2NzM=', '', 'Active', '0'),
(13, 'SDRLzpJWZPY9Kzskj', 4, 1, ',12,13,6,', 'Neha Dhanuka', '', NULL, 'neha.dhanuka@marico.com', NULL, ' 919999999998', '57ca7fff2fa484549ae11bd8f4db7ed00c40d22c', 'WWh1ajZ0eTg=', '', 'Active', '0'),
(14, 'HaSivTQ6vHJ8NN35o', 4, 1, '1', 'Akash chopra', '', NULL, 'akash.chopra@mokshaproductions.in', NULL, ' 919999904046', 'f7c3bc1d808e04732adf679965ccc34ca7ae3441', 'MTIzNDU2Nzg5', '', 'Active', '0'),
(15, 'taFZHx6krpA7vAfYp', 3, 1, '', 'meri1_pm@gmail.com', '', NULL, 'meri1_pm@gmail.com', NULL, '7655456776', '7c4a8d09ca3762af61e59520943dc26494f8941b', 'MTIzNDU2', '', 'Active', '0'),
(16, '', 4, 1, ',1,5,', 'meri1_bm', '', NULL, 'meri1_bm@gmail.com', NULL, '7655678776', '7c4a8d09ca3762af61e59520943dc26494f8941b', 'MTIzNDU2', '', 'Active', '0'),
(17, '', 4, 2, ',2,7,', 'bm2.zara@inintra.com', '', NULL, 'bm2.zara@inintra.com', NULL, '99012211422', '6bc613ef7e45e76401f835edf1a221e482b8d962', 'QWFBYTIxMjE=', '', 'Active', '0'),
(18, 'S22cxhP9EvxpNJjH4', 3, 2, '', 'pm2.zara@inintra.com', '', NULL, 'pm2.zara@inintra.com', NULL, '990196221922221', '6bc613ef7e45e76401f835edf1a221e482b8d962', 'QWFBYTIxMjE=', '', 'Active', '0'),
(19, 'skzQ6jbjFCYsdgkPE', 3, 1, '', 'Akash PM', '', NULL, 'Akash11chopra@gmail.com', NULL, ' 91987654321', 'eb07eca9b650d97c5947758773a0691a534aabdb', 'dWhlNzhVSg==', '', 'Active', '0'),
(20, 'DEo9vrbHq9MFrCSRA', 2, 4, '', 'Test Admin', '', NULL, 'testadmin@gmail.com', NULL, '9087654321', '7c4a8d09ca3762af61e59520943dc26494f8941b', 'MTIzNDU2', '', 'Active', '0'),
(21, 'Y2di4kBEqDQ2C5FuM', 3, 4, '', 'Test PM', '', NULL, 'testpm@gmail.com', NULL, '9807654321', '7c4a8d09ca3762af61e59520943dc26494f8941b', 'MTIzNDU2', '', 'Active', '0'),
(22, '', 4, 4, ',8,', 'Test BM', '', NULL, 'testbm@gmail.com', NULL, '9870654321', '7c4a8d09ca3762af61e59520943dc26494f8941b', 'MTIzNDU2', '', 'Active', '0'),
(24, 'hAp93CRGPwiFGYjGT', 2, 4, '', 'raja priya', '', NULL, 'raja.priya@mokshaproductions.in', NULL, '57567567657', '7c4a8d09ca3762af61e59520943dc26494f8941b', 'MTIzNDU2', '', 'Active', '0'),
(25, 'BiduAFousvzWoWpi3', 3, 4, '', 'poulami', '', NULL, 'poulami@mokshaproductions.in', NULL, '45565654654633', '7c4a8d09ca3762af61e59520943dc26494f8941b', 'MTIzNDU2', '', 'Active', '0'),
(26, '', 4, 4, ',8,', 'pranav', '', NULL, 'pranav@mokshaproductions.in', NULL, '3333333333333333333', '7c4a8d09ca3762af61e59520943dc26494f8941b', 'MTIzNDU2', '', 'Active', '0'),
(27, '', 4, 4, ',8,', 'Pou', '', NULL, 'poulami221.test@gmail.com', NULL, '4543533534545345', '7c4a8d09ca3762af61e59520943dc26494f8941b', 'MTIzNDU2', '', 'Active', '0'),
(28, '', 4, 4, ',8,', 'pou', '', NULL, 'poulami21.test@gmail.com', NULL, '45565654654655', '7c4a8d09ca3762af61e59520943dc26494f8941b', 'MTIzNDU2', '', 'Active', '0'),
(29, '', 4, 1, ',1,', 'user to delete', '', NULL, 'serge@mokshaproductions.in', NULL, ' 919599598833', 'bfe54caa6d483cc3887dce9d1b8eb91408f1ea7a', 'OTg3NjU0MzIx', '', 'Active', '0'),
(30, '93hmoBzMHDdmSghfR', 4, 1, ',9,11,10,', 'Vivian', '', NULL, 'vivian.gonsalves@marico.com', NULL, ' 9188796 93226', 'b9938bf1abcadf3ade8ba0c6c3f934460fc68e78', 'SW5mb0AxMjM=', '', 'Active', '0'),
(31, 'RThXQyCsm2tbyHjRC', 4, 1, ',12,13,6,', 'Priyanka Save', '', NULL, 'priyanka.save@marico.com', NULL, ' 9199673 29280', '29ae01f431f5cb70a001657f6408e00c0b2f6018', 'dXJibzAzSE4=', '', 'Active', '0'),
(32, 'qsLWEsT99zf6Aws6Q', 4, 1, ',12,13,6,', 'Varsha Patil', '', NULL, 'varsha.patil@marico.com', NULL, ' 9198337 51563', 'ae4e77e7945069e6c6e5686b23c8041c4abad9b0', 'dWpndDY1SEc=', '', 'Active', '0'),
(33, '', 4, 1, ',14,', 'Seher', '', NULL, 'seher.contractor@marico.com', NULL, ' 9196190 01208', 'eed257bf0d51441d3c5ed07f9ff4f1020e81ca0e', 'ZHVoZWQ4VUo=', '', 'Active', '0'),
(34, 'YFCbwGRYcNfgLauSr', 4, 1, ',14,', 'Sruti Gudavalli', '', NULL, 'sruti.gudavalli@marico.com', NULL, ' 9191678 37563', 'dbb8e50626bb0befefa772426cd3c36d9e243d00', 'eWd5NzZZRw==', '', 'Active', '0'),
(35, 'LssvKqJkGxMbp2uGZ', 3, 1, '', 'Sangeeta Sharma', '', NULL, 'sangeeta.sharma@mokshaproductions.in', NULL, '9999999999', '5813dfc3b7955dc5d4a396b83f5b6e4972d41df4', 'eWhkZGk4NkJO', '', 'Active', '0');

-- --------------------------------------------------------

--
-- Table structure for table `wc_users_type`
--

CREATE TABLE `wc_users_type` (
  `user_type_id` int(11) NOT NULL,
  `user_type_name` varchar(255) NOT NULL,
  `status` enum('Active','Inactive') NOT NULL,
  `deleted` enum('0','1') NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `wc_users_type`
--

INSERT INTO `wc_users_type` (`user_type_id`, `user_type_name`, `status`, `deleted`) VALUES
(1, 'Super admin', 'Active', '0'),
(2, 'Admin', 'Active', '0'),
(3, 'Project manager', 'Active', '0'),
(4, 'Brand manager', 'Active', '0'),
(5, 'test', 'Inactive', '1');

-- --------------------------------------------------------

--
-- Table structure for table `wc_wekan_board_list`
--

CREATE TABLE `wc_wekan_board_list` (
  `wekan_board_list_id` int(11) NOT NULL,
  `client_id` int(11) NOT NULL,
  `wekan_board_list` varchar(50) NOT NULL,
  `status` enum('Active','Inactive') NOT NULL,
  `deleted` enum('0','1') NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `wc_wekan_board_list`
--

INSERT INTO `wc_wekan_board_list` (`wekan_board_list_id`, `client_id`, `wekan_board_list`, `status`, `deleted`) VALUES
(1, 0, 'Brief - Review Pending', 'Active', '0'),
(2, 0, 'Rejected Brief', 'Active', '0'),
(3, 0, 'Ongoing Tasks', 'Active', '0'),
(4, 0, 'Task Completed', 'Active', '0'),
(5, 0, 'Feedbacks', 'Active', '0'),
(6, 0, 'Pending for Approval by Marico SPOC', 'Active', '0'),
(7, 0, 'Pending for Approval by CD', 'Active', '0'),
(8, 0, 'test', 'Active', '0');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `ci_cookies`
--
ALTER TABLE `ci_cookies`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `ci_sessions`
--
ALTER TABLE `ci_sessions`
  ADD PRIMARY KEY (`session_id`),
  ADD KEY `last_activity_idx` (`last_activity`);

--
-- Indexes for table `login_admin`
--
ALTER TABLE `login_admin`
  ADD PRIMARY KEY (`user_id`);

--
-- Indexes for table `wc_brands`
--
ALTER TABLE `wc_brands`
  ADD PRIMARY KEY (`brand_id`);

--
-- Indexes for table `wc_brief`
--
ALTER TABLE `wc_brief`
  ADD PRIMARY KEY (`brief_id`);

--
-- Indexes for table `wc_briefstatus`
--
ALTER TABLE `wc_briefstatus`
  ADD PRIMARY KEY (`status_id`);

--
-- Indexes for table `wc_brief_product_list`
--
ALTER TABLE `wc_brief_product_list`
  ADD PRIMARY KEY (`brief_product_id`);

--
-- Indexes for table `wc_brief_records`
--
ALTER TABLE `wc_brief_records`
  ADD PRIMARY KEY (`brief_record_id`);

--
-- Indexes for table `wc_clients`
--
ALTER TABLE `wc_clients`
  ADD PRIMARY KEY (`client_id`);

--
-- Indexes for table `wc_client_wekan_list_relation`
--
ALTER TABLE `wc_client_wekan_list_relation`
  ADD PRIMARY KEY (`client_wekan_list_relation_id`);

--
-- Indexes for table `wc_feedback`
--
ALTER TABLE `wc_feedback`
  ADD PRIMARY KEY (`feedback_id`);

--
-- Indexes for table `wc_feedback_content`
--
ALTER TABLE `wc_feedback_content`
  ADD PRIMARY KEY (`feedback_content_id`);

--
-- Indexes for table `wc_feedback_rating`
--
ALTER TABLE `wc_feedback_rating`
  ADD PRIMARY KEY (`feedback_rating_id`);

--
-- Indexes for table `wc_feedback_type`
--
ALTER TABLE `wc_feedback_type`
  ADD PRIMARY KEY (`feedback_type_id`);

--
-- Indexes for table `wc_image_annotation`
--
ALTER TABLE `wc_image_annotation`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `wc_image_upload`
--
ALTER TABLE `wc_image_upload`
  ADD PRIMARY KEY (`image_id`);

--
-- Indexes for table `wc_module`
--
ALTER TABLE `wc_module`
  ADD PRIMARY KEY (`module_id`);

--
-- Indexes for table `wc_module_permission`
--
ALTER TABLE `wc_module_permission`
  ADD PRIMARY KEY (`module_permission_id`);

--
-- Indexes for table `wc_module_permission_backup`
--
ALTER TABLE `wc_module_permission_backup`
  ADD PRIMARY KEY (`module_permission_id`);

--
-- Indexes for table `wc_page_content`
--
ALTER TABLE `wc_page_content`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `wc_product_price`
--
ALTER TABLE `wc_product_price`
  ADD PRIMARY KEY (`product_id`);

--
-- Indexes for table `wc_revision`
--
ALTER TABLE `wc_revision`
  ADD PRIMARY KEY (`revision_id`);

--
-- Indexes for table `wc_settings`
--
ALTER TABLE `wc_settings`
  ADD PRIMARY KEY (`setting_id`);

--
-- Indexes for table `wc_users`
--
ALTER TABLE `wc_users`
  ADD PRIMARY KEY (`user_id`);

--
-- Indexes for table `wc_users_type`
--
ALTER TABLE `wc_users_type`
  ADD PRIMARY KEY (`user_type_id`);

--
-- Indexes for table `wc_wekan_board_list`
--
ALTER TABLE `wc_wekan_board_list`
  ADD PRIMARY KEY (`wekan_board_list_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `wc_brands`
--
ALTER TABLE `wc_brands`
  MODIFY `brand_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;

--
-- AUTO_INCREMENT for table `wc_brief`
--
ALTER TABLE `wc_brief`
  MODIFY `brief_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=56;

--
-- AUTO_INCREMENT for table `wc_briefstatus`
--
ALTER TABLE `wc_briefstatus`
  MODIFY `status_id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `wc_brief_product_list`
--
ALTER TABLE `wc_brief_product_list`
  MODIFY `brief_product_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=79;

--
-- AUTO_INCREMENT for table `wc_brief_records`
--
ALTER TABLE `wc_brief_records`
  MODIFY `brief_record_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=66;

--
-- AUTO_INCREMENT for table `wc_clients`
--
ALTER TABLE `wc_clients`
  MODIFY `client_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `wc_client_wekan_list_relation`
--
ALTER TABLE `wc_client_wekan_list_relation`
  MODIFY `client_wekan_list_relation_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=29;

--
-- AUTO_INCREMENT for table `wc_feedback`
--
ALTER TABLE `wc_feedback`
  MODIFY `feedback_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=23;

--
-- AUTO_INCREMENT for table `wc_feedback_content`
--
ALTER TABLE `wc_feedback_content`
  MODIFY `feedback_content_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=29;

--
-- AUTO_INCREMENT for table `wc_feedback_rating`
--
ALTER TABLE `wc_feedback_rating`
  MODIFY `feedback_rating_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `wc_feedback_type`
--
ALTER TABLE `wc_feedback_type`
  MODIFY `feedback_type_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `wc_image_annotation`
--
ALTER TABLE `wc_image_annotation`
  MODIFY `id` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=180;

--
-- AUTO_INCREMENT for table `wc_image_upload`
--
ALTER TABLE `wc_image_upload`
  MODIFY `image_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=159;

--
-- AUTO_INCREMENT for table `wc_module`
--
ALTER TABLE `wc_module`
  MODIFY `module_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT for table `wc_module_permission`
--
ALTER TABLE `wc_module_permission`
  MODIFY `module_permission_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=45;

--
-- AUTO_INCREMENT for table `wc_module_permission_backup`
--
ALTER TABLE `wc_module_permission_backup`
  MODIFY `module_permission_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=45;

--
-- AUTO_INCREMENT for table `wc_page_content`
--
ALTER TABLE `wc_page_content`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;

--
-- AUTO_INCREMENT for table `wc_product_price`
--
ALTER TABLE `wc_product_price`
  MODIFY `product_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=22;

--
-- AUTO_INCREMENT for table `wc_revision`
--
ALTER TABLE `wc_revision`
  MODIFY `revision_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=129;

--
-- AUTO_INCREMENT for table `wc_settings`
--
ALTER TABLE `wc_settings`
  MODIFY `setting_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=23;

--
-- AUTO_INCREMENT for table `wc_users`
--
ALTER TABLE `wc_users`
  MODIFY `user_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=36;

--
-- AUTO_INCREMENT for table `wc_users_type`
--
ALTER TABLE `wc_users_type`
  MODIFY `user_type_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `wc_wekan_board_list`
--
ALTER TABLE `wc_wekan_board_list`
  MODIFY `wekan_board_list_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
