<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
   
class Item_model extends CI_Model
{ 
    public function __construct()
    {
        $this->load->database();
        $this->load->library('session');
    }
	
	public function item_insert($data)
	{
        $this->db->insert('items', $data);
    }
	
	
	
	
	public function item_update($itemdata, $item_id)
    {
        $this->db->where('item_id', $item_id);
        $report = $this->db->update('items', $itemdata);
        if ($report) {
            return true;
        } else {
            return false;
        }
    }
	
	public function extras_to_items_insert($data)
	{
        $this->db->insert('extras_to_items', $data);
    }
	
	
	 public function delete_extra_item($item_id)
    {
        $this->db->where('item_id', $item_id);
        $this->db->delete('extras_to_items');
    }
	
	
	
	
	public function options_to_items_insert($data)
	{
        $this->db->insert('options_to_items', $data);
    }
	
	
	 public function delete_option_item($item_id)
    {
        $this->db->where('item_id', $item_id);
        $this->db->delete('options_to_items');
    }
	
	
	
	
	public function category_to_items_insert($data)
	{
        $this->db->insert('item_category', $data);
    }
	
	public function category_to_items_update($data, $item_category_id)
    {
        $this->db->where('item_category_id', $item_category_id);
        $report = $this->db->update('item_category', $data);
        if ($report) {
            return true;
        } else {
            return false;
        }
    }
	
	
   public function delete_item_category_item($item_id)
    {
        $this->db->where('item_id', $item_id);
        $this->db->delete('item_category');
    }
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	public function item_price_insert($data)
	{
        $this->db->insert('item_pricing', $data);
    }
	
	
	
	
	public function item_price_update($itemdata, $item_id)
    {
        $this->db->where('item_id', $item_id);
        $report = $this->db->update('item_pricing', $itemdata);
        if ($report) {
            return true;
        } else {
            return false;
        }
    }
	
	
	
		public function item_segment_insert($data)
	{
        $this->db->insert('item_segment', $data);
    }
	
	
	
	
	public function item_segment_update($itemdata, $id)
    {
        $this->db->where('id', $id);
        $report = $this->db->update('item_segment', $itemdata);
        if ($report) {
            return true;
        } else {
            return false;
        }
    }
	
	
	
	
	
	
	
	
	
	
	

    public function count_items()
    {
        return $this->db->count_all('items_master');
    }

    public function get_item_list($limit, $start)
    {
        $sql = 'select * from items order by name DESC limit ' . $start . ', ' . $limit;
        $query = $this->db->query($sql);
        return $query->result();
    }

    public function delete_item($item_id)
    {
        $this->db->where('item_id', $item_id);
        $this->db->delete('items');
    }
	

	
	 public function get_singleitem($item_id)
    {
        if ($item_id != FALSE) {
            $query = $this->db->get_where('items', array('item_id' => $item_id));
            return $query->row_array();
        } else {
            return FALSE;
        }

    }
	public function itemupdate($itemdata, $item_id)
    {
        $this->db->where('item_id', $item_id);
        $report = $this->db->update('items', $itemdata);
        if ($report) 
        {
            return true;
        } 
        else 
        {
            return false;
        }
    }
	
	
	
	
	
	 public function get_singleadmin($item_id)
    {
        if ($item_id != FALSE) {
            $query = $this->db->get_where('items', array('item_id' => $item_id));
            return $query->row_array();
        } else {
            return FALSE;
        }

    }
	
	
		
	
	 public function get_singleBrand($Brand_id)
    {
        if ($Brand_id != FALSE) {
            $query = $this->db->get_where('tblbrands', array('Brand_id' => $Brand_id));
            return $query->row_array();
        } else {
            return FALSE;
        }

    }
	
	
	
	
	
	
	
	public function brand_insert($data)
	{
        $this->db->insert('tblbrands', $data);
    }

	
public function get_Brand($BrandID)
{
    if ($BrandID != FALSE) 
    {
        $query = $this->db->get_where('tblbrands', array('BrandID' => $BrandID));
        return $query->row_array();
    } 
    else 
    {
        return FALSE;
    }

}
	
	public function brand_update($data, $BrandID)
    {
        $this->db->where('BrandID', $BrandID);
        $report = $this->db->update('tblbrands', $data);
        if ($report) {
            return true;
        } else {
            return false;
        }
    }
	
	
	
		
	public function Brand_Fitting_update($data, $BFittingID)
    {
        $this->db->where('BFittingID', $BFittingID);
        $report = $this->db->update('tblbrandfittings', $data);
		
		$sql = $this->db->last_query();
        if ($report) {
            return true;
        } else {
            return false;
        }
    }
	
		public function Brand_Fitting_insert($data)
	{
        $this->db->insert('tblbrandfittings', $data);
    }
	
	
	
	
	
	
	
	
	
		public function admin_update($data, $item_id)
    {
        $this->db->where('item_id', $item_id);
        $report = $this->db->update('pj_item', $data);
        if ($report) {
            return true;
        } else {
            return false;
        }
    }
	
	public function delete_brand($BrandID)
    {
        $this->db->where('BrandID', $BrandID);
        $this->db->delete('tblbrands');
    }
	
	public function delete_brand_fitting($BFittingID)
    {
        $this->db->where('BFittingID', $BFittingID);
        $this->db->delete('tblbrandfittings');
    }
	
	public function days_insert($data1)
	{
		$date_from=$data1['date_from'];
		$date_to=$data1['date_to'];
		$owner_id=$data1['owner_id'];
		$service_id=$data1['service_id'];

		for($i=0;$i<count($date_from);$i++)
		{
			
			$this->db->insert('nonworking_days', array('date_from'=>date('Y-m-d H:i:s',strtotime($date_from[$i])),'date_to'=>date('Y-m-d H:i:s',strtotime($date_to[$i])),
			'created_on'=> date('Y-m-d H:i:s'),'owner_id'=>$owner_id,'service_id'=>$service_id));
		}   
	}

	public function days_update($data1,$id)
	{
		$date_from=$data1['date_from'];
		$date_to=$data1['date_to'];
		$owner_id=$data1['owner_id'];
		$service_id=$data1['service_id'];

		for($i=0;$i<count($date_from);$i++)
		{
			$this->db->where('id', $id);
			$this->db->update('nonworking_days', array('date_from'=>date('Y-m-d H:i:s',strtotime($date_from[$i])),'date_to'=>date('Y-m-d H:i:s',strtotime($date_to[$i])),
			'created_on'=> date('Y-m-d H:i:s'),'owner_id'=>$owner_id,'service_id'=>$service_id));
		}   
	}
	
	
	
	 public function get_setting_accordion($setting_key)
    {
        if ($setting_key != FALSE) {
            $query = $this->db->get_where('setting', array('setting_key' => $setting_key));
            return $query->row_array();
        } else {
            return FALSE;
        }

    }
	
	
	public function get_allcountries()
	{
	
		$sql = 'select * from countries order by name ASC  ';
        $query = $this->db->query($sql);
        return $query->result();
	
	
	}
	public function get_allstates()
	{
	
		$sql = 'select * from states order by name ASC  ';
        $query = $this->db->query($sql);
        return $query->result();
	
	
	}
	
	
	public function get_allcities()
	{
	
		$sql = 'select * from cities order by name ASC  ';
        $query = $this->db->query($sql);
        return $query->result();
	
	
	}
	
	public function Brand_leave_insert($data)
	{
	$this->db->insert('tblbrands_leave', $data);
	}
	
	public function get_Brand_leave($leave_id)
	{
	if ($Brand_id != FALSE) 
	{
	$query = $this->db->get_where('tblbrands_leave', array('leave_id' => $leave_id));
	return $query->row_array();
	} 
	else 
	{
	return FALSE;
	}
	
	}
	
	public function Brand_leave_update($data, $leave_id)
	{
	$this->db->where('leave_id', $leave_id);
	$report = $this->db->update('tblbrands_leave', $data);
	if ($report) {
	return true;
	} else {
	return false;
	}
	}
	
	
	
	/* public function getBrandleave_count()
    {
        return $this->db->count_all('tblbrands_leave');
    }*/
	 public function getBrandleave_count($Brand_id)
    {
         $sql = 'select * from tblbrands_leave where Brand_id="'.$Brand_id.'" ';
        $query = $this->db->query($sql);
        return $query->result();
    }

    public function getallBrandleave($Brand_id,$limit,$offset)
    {
        $sql = 'select * from tblbrands_leave where Brand_id="'.$Brand_id.'" order by leave_id DESC limit ' . $offset . ', ' . $limit;
        $query = $this->db->query($sql);
        return $query->result();
    }
	
	
	
	
	 public function delete_brand_leave($leave_id)
    {
        $this->db->where('leave_id', $leave_id);
        $this->db->delete('tblbrands_leave');
    }
	
	
	
	

	
	}

