<?php
class Plans_model extends CI_Model{



    public function __construct()
    {
        $this->load->database();
        $this->load->library('session');
    }



    public function plan_insert($data)
    {
    $this->db->insert('tbl_plan', $data);
    }


    public function plan_update($data, $planid)
    {
    $this->db->where('planid', $planid);
    $report = $this->db->update('tbl_plan', $data);
    if ($report) {
    return true;
    } else {
    return false;
    }
    }


    
function getAllPlans($plan_currency){
    /*echo "select tbl_plan.*,tbl_plantype.plntypname from  tbl_plan inner join tbl_plantype on tbl_plan.plntyid=tbl_plantype.plntyid WHERE tbl_plan.deleted='0' and tbl_plan.status='0' group by tbl_plan.plntyid ";*/

    $sql="select tbl_plan.*,tbl_plantype.plntypname from  tbl_plan inner join tbl_plantype on tbl_plan.plntyid=tbl_plantype.plntyid WHERE tbl_plan.deleted='0' and tbl_plan.status='0' and tbl_plan.plan_for='upload' ";
    if($plan_currency!='')
    {
        $sql.=" and tbl_plan.plan_currency='$plan_currency' ";
    }

    

    $sql.=" group by tbl_plan.plntyid ";

//echo $sql;
    $query=$this->db->query($sql);
    $plansDetails= $query->result_array();

    $arr=array();

        if(!empty($plansDetails))
        {
            $i=0;
        foreach($plansDetails as $key => $planrow)
        {
        $plntyid=$planrow['plntyid'];
        $plntypname=$planrow['plntypname'];
        //$plan_name=$planrow['plan_name'];
        //$plan_amount=$planrow['plan_amount'];
        //$plan_credit=$planrow['plan_credit'];
         $arr[$i]['plntyid']= $plntyid; 
         $arr[$i]['plntypname']= $plntypname; 

            $sql1="select tbl_plan.*,tbl_plantype.plntypname from  tbl_plan inner join tbl_plantype on tbl_plan.plntyid=tbl_plantype.plntyid WHERE tbl_plan.deleted='0' and tbl_plan.status='0' and tbl_plan.plntyid='$plntyid' and tbl_plan.plan_for='upload' ";

            if($plan_currency!='')
            {
            $sql1.=" and tbl_plan.plan_currency='$plan_currency' ";
            }



           

            //echo $sql1;

            $query=$this->db->query($sql1);
            $plansDetails1= $query->result_array();

            

            if(!empty($plansDetails1))
            {
            $k=0;

            foreach($plansDetails1 as $key1 => $planrow1)
            {
                $planid=$planrow1['planid'];
                $plan_name=$planrow1['plan_name'];
                $plan_amount=$planrow1['plan_amount'];
                $plan_credit=$planrow1['plan_credit'];
                $plan_currency=$planrow1['plan_currency'];
                $plan_currency_symbol=$planrow1['plan_currency_symbol'];
                $plan_per_image=$planrow1['plan_per_image'];


                $arr[$i]['plandetails'][$k]['planid']= $planid; 
                $arr[$i]['plandetails'][$k]['plan_name']= $plan_name; 
                $arr[$i]['plandetails'][$k]['plan_amount']= $plan_amount; 
                $arr[$i]['plandetails'][$k]['plan_credit']= $plan_credit;
                $arr[$i]['plandetails'][$k]['plan_currency']= $plan_currency;
                $arr[$i]['plandetails'][$k]['plan_currency_symbol']= $plan_currency_symbol;
                $arr[$i]['plandetails'][$k]['plan_per_image']= $plan_per_image; 



              

              $k++;            
            }
            }
            else
            {
                $arr[$i]['plandetails'][]="";
            }






         $i++;

        }

        }


   return $arr;

 }



function getAllbilling($user_id){

    

$query=$this->db->query("select tbl_users_billing.*,tbl_plantype.plntypname
    FROM  tbl_users_billing
    INNER JOIN  tbl_plan on tbl_users_billing.billing_plan_id=tbl_plan.planid
    INNER JOIN  tbl_plantype on tbl_plan.plntyid=tbl_plantype.plntyid
    WHERE tbl_users_billing.user_id='$user_id' and tbl_users_billing.payment_status='Paid' and tbl_users_billing.payment_form='0'");
return $query->result_array();

 }

function getAllbookbilling($user_id){

    

$query=$this->db->query("select tbl_users_billing.*,tbl_plantype.plntypname
    FROM  tbl_users_billing
    INNER JOIN  tbl_plan on tbl_users_billing.billing_plan_id=tbl_plan.planid
    INNER JOIN  tbl_plantype on tbl_plan.plntyid=tbl_plantype.plntyid
    WHERE tbl_users_billing.user_id='$user_id' and tbl_users_billing.payment_status='Paid' and tbl_users_billing.payment_form='1' ");
return $query->result_array();

 }

function getAllbookorder($user_id){

$query=$this->db->query("select * from tbl_booking where user_id='$user_id' order by book_id Desc ");

// $query=$this->db->query("select tbl_users_billing.*,tbl_plantype.plntypname
// FROM tbl_users_billing
// INNER JOIN tbl_plan on tbl_users_billing.billing_plan_id=tbl_plan.planid
// INNER JOIN tbl_plantype on tbl_plan.plntyid=tbl_plantype.plntyid
// WHERE tbl_users_billing.user_id='$user_id' and tbl_users_billing.payment_form='1'");
return $query->result_array();

}
    
    
    
function get_plansById($planid){
  /*echo " select tbl_plan.*,tbl_plantype.plntypname from  tbl_plan
         INNER JOIN  tbl_plantype on tbl_plan.plntyid=tbl_plantype.plntyid
         WHERE tbl_plan.planid='$planid' ";
*/
    $planid=preg_replace("/[^A-Za-z0-9\-\']/", '', $planid);     
    $query=$this->db->query("select tbl_plan.*,tbl_plantype.plntypname from  tbl_plan
        INNER JOIN  tbl_plantype on tbl_plan.plntyid=tbl_plantype.plntyid
        WHERE tbl_plan.planid=".$this->db->escape($planid)."");
    return $query->result_array();
 }

    
    
    public function billing_insert($data)
    {
        $this->db->insert('tbl_users_billing', $data);
    }
    
    
    
    
    public function billing_update($userdata, $billing_id)
    {
        $this->db->where('billing_id', $billing_id);
        $report = $this->db->update('tbl_users_billing', $userdata);
        if ($report) {
            return true;
        } else {
            return false;
        }
    }

      public function delete_billing($billing_id)
    {
        $this->db->where('billing_id', $billing_id);
        $this->db->delete('tbl_users_billing');
    }
    
    
    

















	public function upload_image($data){		
		$result=$this->db->insert('tbl_upload_img',$data);
		return $result;
	}





	public function upload_img_insert($data)
	{
        $this->db->insert('tbl_upload_img', $data);
    }
	
	
	
	
	public function upload_img_update($userdata, $upimg_id)
    {
        $this->db->where('upimg_id', $upimg_id);
        $report = $this->db->update('tbl_upload_img', $userdata);
        if ($report) {
            return true;
        } else {
            return false;
        }
    }


    public function upload_images_insert($data)
	{
        $this->db->insert('tbl_upload_images', $data);
    }
	
	
	
	
	public function upload_images_update($userdata, $id)
    {
        $this->db->where('id', $id);
        $report = $this->db->update('tbl_upload_images', $userdata);
        if ($report) {
            return true;
        } else {
            return false;
        }
    }
		


/////////////////////////////By papri/////////////////////////////////////

public function tbuser_billing_update($plan_credit,$plan_amount, $user_id)
{

$usrquery = "SELECT * FROM `tbl_users` where user_id='$user_id'"; 
$uquery=$this->db->query($usrquery);
$userinfo= $uquery->result_array();
$total_amount=$userinfo[0]['total_amount'];
$total_credit=$userinfo[0]['total_credit'];

$bplncredit=$plan_credit;
$bplnamt=$plan_amount;

$totalamount=$total_amount-$bplnamt;
$totalcredit=$total_credit-$bplncredit;

$data = array(  
'total_credit'=>$totalcredit,
'total_amount'=>$totalamount,   

);


$this->db->where('user_id', $user_id);
$report = $this->db->update('tbl_users', $data);
if ($report) {
return true;
} else {
return false;
}



}



    //////////////////////////////////////////////////////////////






	
}