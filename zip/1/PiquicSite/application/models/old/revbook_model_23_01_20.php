<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Revbook_model extends CI_Model
{
    public function __construct()
    {
        $this->load->database();
        $this->load->library('session');
    }
		
	public function add_booking($data)
	{
     $res= $this->db->insert('tbl_booking', $data);
     if($res)
     {
     	 return true;
     }
     else{
     	return false;
     }
  }
  public function update_booking($data,$tmb_book_id)
  {
    $this->db->where('book_id', $tmb_book_id);
    $report = $this->db->update('tbl_booking', $data);
   //  $res= $this->db->insert('tbl_booking', $data);
    if($report)
    {
      return true;
    }
    else{
      return false;
    }
  }
	public function getOwnerlist()
  {
    $sql = 'select * from nc_user where usertype="2"';
    $query = $this->db->query($sql);
    return $query->num_rows();
  }


 }
