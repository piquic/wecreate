<?php
class stylequeue_model extends CI_Model 
{	

  function queueimages($sku_key=""){

    if ($this->session->userdata('front_logged_in')) {
      $session_data = $this->session->userdata('front_logged_in');
      $user_id = $session_data['user_id'];
      $user_name = $session_data['user_name'];
    }
    else
    {
      $user_id = '';
      $user_name = '';
    }

    $sql="select * from  tbl_upload_img WHERE queue='1' AND status='0'";

    if($user_id!='')
    {
      $sql.=" and user_id='$user_id' ";
    }
    if($sku_key!='')
    {
      $sql.=" and zipfile_name='$sku_key' ";
    }

    $sql .= " order by upimg_id desc ";
    
    $query=$this->db->query($sql);
    return $query->result();
  }


  function updatemult_modskuinfo($skuno,$model){ 

    if ($this->session->userdata('front_logged_in')) {
      $session_data = $this->session->userdata('front_logged_in');
      $user_id = $session_data['user_id'];
      $user_name = $session_data['user_name'];
    }
    else
    {
      $user_id = '';
      $user_name = '';
    }


    $skuno=explode(',', $skuno); 
    foreach ($skuno as $sku) {

/////////////////////////ppppp///////////////////////////////

      $second_DB = $this->load->database('another_db', TRUE); 
      $pautoquery = "SELECT gen_id FROM `tbl_model` where m_id='$model'  ";
  // echo $pautoquery;
      $query2=$second_DB->query($pautoquery);
      $modelinfo= $query2->result_array();
      $gen_id=$modelinfo[0]['gen_id'];



      $pautoquery = "SELECT gender FROM `tbl_upload_img` where user_id='$user_id' and  zipfile_name='$sku'  ";
  // echo $pautoquery;
      $query2=$this->db->query($pautoquery);
      $modelinfo= $query2->result_array();
      $gender=$modelinfo[0]['gender'];

      if($gen_id==$gender)
      {
        $this->db->set('model_id', $model);       
    //$this->db->where('skuno', $sku);

        $array = array('skuno' => $sku, 'user_id' => $user_id);
        $this->db->where($array); 
        $result=$this->db->update('tbl_style');
      }



/////////////////////////ppppp///////////////////////////////

    }

  }

  function update_vendorsku($skuno,$vendor){ 

    if ($this->session->userdata('front_logged_in')) {
      $session_data = $this->session->userdata('front_logged_in');
      $user_id = $session_data['user_id'];
      $user_name = $session_data['user_name'];
    }
    else
    {
      $user_id = '';
      $user_name = '';
    }

    $skuno=explode(',', $skuno); 
    foreach ($skuno as $sku) {
      $this->db->set('vendor_id', $vendor);       
    //$this->db->where('zipfile_name', $sku);
      $array = array('zipfile_name' => $sku, 'user_id' => $user_id);
      $this->db->where($array); 
      $result=$this->db->update('tbl_upload_img');
    }

  }


  function update_autostylingsku($skuno,$autoval){ 


    if ($this->session->userdata('front_logged_in')) {
      $session_data = $this->session->userdata('front_logged_in');
      $user_id = $session_data['user_id'];
      $user_name = $session_data['user_name'];
    }
    else
    {
      $user_id = '';
      $user_name = '';
    }


    if($skuno==''){
     $this->db->set('autostyle', '1'); 
     $this->db->set('status', '2');
     $this->db->set('stage', 'Styling');
     $this->db->set('comments', 'Pending');
     $result=$this->db->update('tbl_style');

     $query=$this->db->query("select * from  tbl_style WHERE autostyle='1' and user_id='$user_id' AND status='0'");
     $row=$query->result();
     foreach($row as $querow){          
       $sku=$querow->skuno;
       $this->db->set('status', '0');       
   //$this->db->where('zipfile_name', $sku);
       $array = array('zipfile_name' => $sku, 'user_id' => $user_id);
       $this->db->where($array); 
       $result=$this->db->update('tbl_upload_img');


       $sku=$querow->skuno;
       $gender=$querow->gender;
       $category=$querow->category;
       $create_date=date("d-m-y-h-i-s");
       $client_name=$querow->client_name; 

       if($gender=='1'){
        $gender='male';
      }
      else if($gender=='2'){
        $gender='female';
      }
      else if($gender=='3'){
        $gender='boy';
      }
      else if($gender=='4'){
        $gender='boy78';
      }
      else if($gender=='5'){
        $gender='girl';
      }
      else if($gender=='6'){
        $gender='girl78';
      }  
   //$this->db->set('status', '1');      
   //$CI = &get_instance();
   //$this->db2 = $CI->load->database('another_db', TRUE); 
      $second_DB = $this->load->database('another_db', TRUE); 

      $data=array(
        'sku_no' => $sku,
        'img_project_name' => $sku,
        'img_name' => $sku,
        'gender' => $gender,
        'category' => $category,
        'create_date' => $create_date,
        'status' => '0',
        'client_name'=>$client_name
      );
      $second_DB->insert('tbl_trun_pic', $data);

    }
  }

  else{

   $skuno=explode(',', $skuno); 
   foreach ($skuno as $sku) {


     $this->db->set('autostyle', '1'); 
     $this->db->set('status', '2');
     $this->db->set('stage', 'Styling');
     $this->db->set('comments', 'Pending');

     $this->db->where('skuno', $sku);
     $result=$this->db->update('tbl_style');

     $this->db->set('status', '0');       
   //$this->db->where('zipfile_name', $sku);
     $array = array('zipfile_name' => $sku, 'user_id' => $user_id);
     $this->db->where($array); 
     $result=$this->db->update('tbl_upload_img');




   //echo "select * from  tbl_style WHERE autostyle='1' and user_id='$user_id' AND skuno='$sku'";

     $query=$this->db->query("select * from  tbl_style WHERE autostyle='1' and user_id='$user_id' AND skuno='$sku'");
     $rowstyle=$query->result_array();
     $gender=$rowstyle[0]['gender'];
     $category=$rowstyle[0]['category'];

     if($gender=='1'){
      $gender='male';
    }
    else if($gender=='2'){
      $gender='female';
    }
    else if($gender=='3'){
      $gender='boy';
    }
    else if($gender=='4'){
      $gender='boy78';
    }
    else if($gender=='5'){
      $gender='girl';
    }
    else if($gender=='6'){
      $gender='girl78';
    }
    /*else if($gender=='ps-male'){
    $gender='1';
    }else if($gender=='ps-female'){
    $gender='2';
  }*/


  $create_date=date("d-m-y-h-i-s");
  $client_name=$user_name;

  $second_DB = $this->load->database('another_db', TRUE); 

  $data=array(
    'sku_no' => $sku,
    'img_project_name' => $sku,
    'img_name' => $sku,
    'gender' => $gender,
    'category' => $category,
    'create_date' => $create_date,
    'status' => '0',
    'client_name'=>$client_name
  );
  $second_DB->insert('tbl_trun_pic', $data);




}
}




}



function getmodelbygender($gender_id){ 

  if ($this->session->userdata('front_logged_in')) {
    $session_data = $this->session->userdata('front_logged_in');
    $user_id = $session_data['user_id'];
    $user_name = $session_data['user_name'];
  }
  else
  {
    $user_id = '';
    $user_name = '';
  }


  $second_DB = $this->load->database('another_db', TRUE); 

  $pautoquery = "SELECT * FROM `tbl_model` where status='1' and gen_id='$gender_id' ORDER by `m_id` ";
    // echo $pautoquery;
  $query2=$second_DB->query($pautoquery);
  return $modelinfo= $query2->result_array();



}







function update_processing($skuno,$sty_id){ 

  if ($this->session->userdata('front_logged_in')) {
    $session_data = $this->session->userdata('front_logged_in');
    $user_id = $session_data['user_id'];
    $user_name = $session_data['user_name'];
  }
  else
  {
    $user_id = '';
    $user_name = '';
  }



///////////////////////////////////////////////////////////////////////////////////////////////


  $pautoquery = "SELECT * FROM `tbl_upload_img` where user_id='$user_id' and zipfile_name='$skuno'  ";
    // echo $pautoquery;
  $query2=$this->db->query($pautoquery);
  $usrinfo= $query2->result_array();
  $upimg_id=$usrinfo[0]['upimg_id'];



  $pautoquery = "SELECT count(*) as ctn_sku_image FROM `tbl_upload_images` where upimg_id='$upimg_id'  ";
    // echo $pautoquery;
  $query2=$this->db->query($pautoquery);
  $usrinfo= $query2->result_array();
  $ctn_sku_image=$usrinfo[0]['ctn_sku_image'];








  $pautoquery = "SELECT * FROM `tbl_users` where user_id='$user_id'  ";
    // echo $pautoquery;
  $query2=$this->db->query($pautoquery);
  $usrinfo= $query2->result_array();
  $total_amount=$usrinfo[0]['total_amount'];
  $total_credit=$usrinfo[0]['total_credit'];

  if($total_amount<='0')
  {
   $total_amount='';
 }
 if($total_amount=='0.00')
 {
  $total_amount='';
}

if($total_credit<='0')
{
 $total_credit='';
}
if($total_credit=='0.00')
{
  $total_credit='';
}

if($total_amount!='')
{
  $current_amount=$total_amount-(8*$ctn_sku_image);
  $amount_history=8*$ctn_sku_image;
  $payment_status="Paid";
}
else
{
  $current_amount=0-(8*$ctn_sku_image);
  $amount_history="-".(8*$ctn_sku_image);
  $payment_status="Not Paid";
}



if($total_credit!='')
{
 $current_credit=$total_credit-$ctn_sku_image;
 $credit_history=$ctn_sku_image;
 $payment_status="Paid";
}
else
{
  $current_credit=0-$ctn_sku_image;
  $credit_history="-".$ctn_sku_image;
  $payment_status="Not Paid";
}

if($current_amount<=0)
{
$payment_status="Not Paid";
}

$userdata=array(
  'total_amount' => $current_amount,
  'total_credit' => $current_credit);

$this->db->where('user_id', $user_id);
$report = $this->db->update('tbl_users', $userdata);






$data=array(
  'user_id' => $user_id,
  'billing_id' => '0',
  'sty_id' => $sty_id,
  'upimg_id' => $upimg_id,
  'amount' => $amount_history,
  'credit' => $credit_history,
  'account_history_date' => date("Y-m-d"),
  'account_history_time' => date("H:i:s"),
  'payment_status' => $payment_status,
);
$this->db->insert('tbl_account_history', $data);

    ///////////////////////////////////////////////////////////////////////////////////////////////


$this->db->set('status', '2');
$this->db->set('stage', 'Processing'); 
if($payment_status=='Not Paid')
{
  $this->db->set('status_type', 'Reject'); 
  $this->db->set('comments', 'Warning - Payment Pending'); 
}


    //$this->db->where('zipfile_name', $sku);
$array = array('sty_id' => $sty_id);
$this->db->where($array); 
$result=$this->db->update('tbl_style');







}





}

?>