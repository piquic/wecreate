<?php
class Labelimg_model extends CI_Model 
{	

function labelimages($sku_key){

    if ($this->session->userdata('front_logged_in')) {
    $session_data = $this->session->userdata('front_logged_in');
    $user_id = $session_data['user_id'];
    $user_name = $session_data['user_name'];
    }
    else
    {
    $user_id = '';
    $user_name = '';
    }


  $sql="select * from  tbl_upload_img WHERE queue='0' ";

  if($user_id!='')
  {
    $sql.=" and user_id='$user_id' ";
  }
  if($sku_key!='')
  {
    $sql.=" and zipfile_name='$sku_key' ";
  }

  $sql.=" order by upimg_id desc ";
      

  $query=$this->db->query($sql);
  return $query->result();
 }

 function update_skuinfo(){


    if ($this->session->userdata('front_logged_in')) {
    $session_data = $this->session->userdata('front_logged_in');
    $user_id = $session_data['user_id'];
    $user_name = $session_data['user_name'];
    }
    else
    {
    $user_id = '';
    $user_name = '';
    }

   $skuno=$this->input->post('skuno');
   $category=$this->input->post('category'); 
   $this->db->set('category', $category);       
   //$this->db->where('zipfile_name', $skuno);
   $array = array('zipfile_name' => $skuno, 'user_id' => $user_id);
   $this->db->where($array); 
   $result=$this->db->update('tbl_upload_img');
   return $result;
    }

 function update_genskuinfo(){
    if ($this->session->userdata('front_logged_in')) {
    $session_data = $this->session->userdata('front_logged_in');
    $user_id = $session_data['user_id'];
    $user_name = $session_data['user_name'];
    }
    else
    {
    $user_id = '';
    $user_name = '';
    }

   $skuno=$this->input->post('skuno');
   $gender=$this->input->post('gender'); 
   $this->db->set('gender', $gender);       
   //$this->db->where('zipfile_name', $skuno);
   $array = array('zipfile_name' => $skuno, 'user_id' => $user_id);
     $this->db->where($array); 
   $result=$this->db->update('tbl_upload_img');
   return $result;
    }

function updatemult_catskuinfo($skuno,$category){ 

        if ($this->session->userdata('front_logged_in')) {
    $session_data = $this->session->userdata('front_logged_in');
    $user_id = $session_data['user_id'];
    $user_name = $session_data['user_name'];
    }
    else
    {
    $user_id = '';
    $user_name = '';
    }






	$skuno=explode(',', $skuno); 
   foreach ($skuno as $sku) {
   $this->db->set('category', $category);       
   //$this->db->where('zipfile_name', $sku);
   $array = array('zipfile_name' => $sku, 'user_id' => $user_id);
     $this->db->where($array); 
   $result=$this->db->update('tbl_upload_img');
     }
   
    }

 function updatemult_genskuinfo($skuno,$gender){ 

        if ($this->session->userdata('front_logged_in')) {
    $session_data = $this->session->userdata('front_logged_in');
    $user_id = $session_data['user_id'];
    $user_name = $session_data['user_name'];
    }
    else
    {
    $user_id = '';
    $user_name = '';
    }

   $skuno=explode(',', $skuno);
   foreach ($skuno as $sku) {
   $this->db->set('gender', $gender);       
   //$this->db->where('zipfile_name', $sku);
   $array = array('zipfile_name' => $sku, 'user_id' => $user_id);
     $this->db->where($array); 
   $result=$this->db->update('tbl_upload_img');

   }



    }



function delete_sku($skuno){ 

        if ($this->session->userdata('front_logged_in')) {
    $session_data = $this->session->userdata('front_logged_in');
    $user_id = $session_data['user_id'];
    $user_name = $session_data['user_name'];
    }
    else
    {
    $user_id = '';
    $user_name = '';
    }


   $query=$this->db->query("select * from  tbl_upload_img where zipfile_name='$skuno'");   
   $row=$query->result_array();
   $del=$row[0]['zipfile_name'];
   $directory='upload';
   $imgdirname=$directory . '/' . $del .'/';
   $dirPath=$directory . '/' . $del;
  $files =glob($imgdirname."*.{jpg,JPG}", GLOB_BRACE);
  foreach($files as $file){	
    	unlink($file); //delete file
    }

   rmdir($dirPath);

   //$this->db->where('zipfile_name', $skuno);
   $array = array('zipfile_name' => $skuno, 'user_id' => $user_id);
   $this->db->where($array); 
   $result=$this->db->delete('tbl_upload_img');
   return $result;

    }

function insert_file($file_name,$skuno){ 

        if ($this->session->userdata('front_logged_in')) {
    $session_data = $this->session->userdata('front_logged_in');
    $user_id = $session_data['user_id'];
    $user_name = $session_data['user_name'];
    }
    else
    {
    $user_id = '';
    $user_name = '';
    }



  $this->db->select("upimg_id"); 
  $this->db->from('tbl_upload_img');
  //$this->db->where('zipfile_name', $skuno);





  $array = array('zipfile_name' => $skuno, 'user_id' => $user_id);
  $this->db->where($array); 
  $query = $this->db->get();
  $row=$query->result_array();
  $id=$row[0]['upimg_id'];



    $img_name_check=strtoupper($file_name);

    if(strstr($img_name_check,'FRONT'))
    {
    $img_ang='Front';
    }
    elseif(strstr($img_name_check,'BACK'))
    {
    $img_ang='Back';
    }
    elseif(strstr($img_name_check,'LEFT'))
    {
    $img_ang='LeftSide';
    }
    elseif(strstr($img_name_check,'RIGHT'))
    {
    $img_ang='RightSide';
    }
    elseif(strstr($img_name_check,'TAG'))
    {
    $img_ang='Tag';
    }
    else
    {
    $img_ang='';
    }


$data = array("img_name" => $file_name, "upimg_id" => $id,'img_ang'=>$img_ang);
  
$this->db->insert('tbl_upload_images', $data);

}

 function updateimg_anginfo($id,$selval,$img,$imgid){


        if ($this->session->userdata('front_logged_in')) {
    $session_data = $this->session->userdata('front_logged_in');
    $user_id = $session_data['user_id'];
    $user_name = $session_data['user_name'];
    }
    else
    {
    $user_id = '';
    $user_name = '';
    }

   $imgid=$imgid;
    $id=$id;
    $rand=rand(1,20);
    $tagoption=$selval;
    $img=$img;
   $query=$this->db->query("select * from  tbl_upload_img where upimg_id='$id'  and user_id='$user_id' "); 
   $row=$query->result_array();
   $sku=$row[0]['zipfile_name'];
   $path = "upload/".$user_id."/".$sku.'/'; 

  $ext=explode('.', $img);
  
  $imgext=end($ext);
  $imgext='.'.$imgext;

  $newName=$sku.'-'.$tagoption.$imgext;

  if(file_exists($path.$newName)){
$newName=$sku.'-'.$tagoption.$rand.$imgext;
}else{
  $newName=$sku.'-'.$tagoption.$imgext;
}
rename($path.$img, $path.$newName);


   $this->db->set('img_name', $newName); 
   $this->db->set('img_ang', $tagoption);          
   $this->db->where('id', $imgid);
   $result=$this->db->update('tbl_upload_images');

  
   


    }



function updatestyle_queinfo($sku,$cat,$gen){ 

    if ($this->session->userdata('front_logged_in')) {
    $session_data = $this->session->userdata('front_logged_in');
    $user_id = $session_data['user_id'];
    $user_name = $session_data['user_name'];
    }
    else
    {
    $user_id = '';
    $user_name = '';
    }


    $sku=$sku;
    $cat=$cat;
    $gen=$gen;

    $this->db->set('queue', '1');          
    //$this->db->where('zipfile_name', $sku);
    $array = array('zipfile_name' => $sku, 'user_id' => $user_id);
     $this->db->where($array); 
    $result=$this->db->update('tbl_upload_img');

    $query =$this->db->query("select * from  tbl_style Where skuno='$sku' and user_id='$user_id'");    
    // $query = $this->db->get("select * from  tbl_style Where skuno='$sku'");
    if ($query->num_rows() > 0){
    $data = array("category" => $cat, "gender" => $gen); 
    //$this->db->where('skuno', $sku); 
    //$this->db->where('user_id', $user_id);
    $array = array('skuno' => $sku, 'user_id' => $user_id);
     $this->db->where($array);  
    $this->db->update('tbl_style', $data);

    }else{
    $data = array("user_id" => $user_id,"skuno" => $sku, "category" => $cat, "gender" => $gen);  
    $this->db->insert('tbl_style', $data);

    }


}





}

?>