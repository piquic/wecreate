<?php
class stylequeue_model extends CI_Model 
{	

  function queueimages($sku_key="",$filter_val=""){

    if ($this->session->userdata('front_logged_in')) {
      $session_data = $this->session->userdata('front_logged_in');
      $user_id = $session_data['user_id'];
      $user_name = $session_data['user_name'];
    }
    else
    {
      $user_id = '';
      $user_name = '';
    }

    $sql="select tbl_upload_img.*,tbl_style.status as style_status from  tbl_upload_img left join tbl_style on tbl_upload_img.zipfile_name=tbl_style.skuno WHERE tbl_upload_img.queue='1' AND tbl_upload_img.status='0'";
  
    if($user_id!='')
    {
      $sql.=" and tbl_upload_img.user_id='$user_id' ";
    }
    if($sku_key!='')
    {
      $sql.=" and tbl_upload_img.zipfile_name like '%".$sku_key."%' ";
    }
     if($filter_val!='')
    {
      $sql.=" and tbl_style.status='$filter_val'";
    }
    $sql .= " order by style_status ASC, upimg_id DESC ";
     

     // echo $sql; exit();
    
    $query=$this->db->query($sql);
    return $query->result();
  }


  function updatemult_modskuinfo($skuno,$model){ 

    if ($this->session->userdata('front_logged_in')) {
      $session_data = $this->session->userdata('front_logged_in');
      $user_id = $session_data['user_id'];
      $user_name = $session_data['user_name'];
    }
    else
    {
      $user_id = '';
      $user_name = '';
    }


    $skuno=explode(',', $skuno); 
    foreach ($skuno as $sku) {

/////////////////////////ppppp///////////////////////////////

      $second_DB = $this->load->database('another_db', TRUE); 
      $pautoquery = "SELECT gen_id FROM `tbl_model` where m_id='$model'  ";
  // echo $pautoquery;
      $query2=$second_DB->query($pautoquery);
      $modelinfo= $query2->result_array();
      $gen_id=$modelinfo[0]['gen_id'];



      $pautoquery = "SELECT gender FROM `tbl_upload_img` where user_id='$user_id' and  zipfile_name='$sku'  ";
  // echo $pautoquery;
      $query2=$this->db->query($pautoquery);
      $modelinfo= $query2->result_array();
      $gender=$modelinfo[0]['gender'];

      if($gen_id==$gender)
      {
        $this->db->set('model_id', $model);       
    //$this->db->where('skuno', $sku);

        $array = array('skuno' => $sku, 'user_id' => $user_id);
        $this->db->where($array); 
        $result=$this->db->update('tbl_style');
      }
      else
      {
         echo "missmatch";
         exit;
      }



/////////////////////////ppppp///////////////////////////////

    }

  }

  function update_vendorsku($skuno,$vendor){ 

    if ($this->session->userdata('front_logged_in')) {
      $session_data = $this->session->userdata('front_logged_in');
      $user_id = $session_data['user_id'];
      $user_name = $session_data['user_name'];
    }
    else
    {
      $user_id = '';
      $user_name = '';
    }

    $skuno=explode(',', $skuno); 
    foreach ($skuno as $sku) {
      $this->db->set('vendor_id', $vendor);       
    //$this->db->where('zipfile_name', $sku);
      $array = array('zipfile_name' => $sku, 'user_id' => $user_id);
      $this->db->where($array); 
      $result=$this->db->update('tbl_upload_img');
    }

  }

  function update_autostylingUpdate($sku,$cat,$gen,$autoval){ 


    if ($this->session->userdata('front_logged_in')) {
      $session_data = $this->session->userdata('front_logged_in');
      $user_id = $session_data['user_id'];
      $user_name = $session_data['user_name'];
    }
    else
    {
      $user_id = '';
      $user_name = '';
    }



  


    $data = array(
    "user_id" => $user_id,
    "skuno" => $sku, 
    "category" => $cat, 
    "gender" => $gen,
    "status" => '2',
    "autostyle" => '1',
    "stage" => 'Auto Styling',
    "comments" => 'Pending',
    "client_name" => $user_name,
    );  
    $this->db->insert('tbl_style', $data);
    
    $sty_id = $this->db->insert_id();



     $this->db->set('status', '0'); 
     $this->db->set('queue', '1');       
     $array = array('zipfile_name' => $sku, 'user_id' => $user_id);
     $this->db->where($array); 
     $result=$this->db->update('tbl_upload_img');






     $gender=$gen;
     $category=$cat;

    if($gender=='1'){
      $gender='male';
    }
    else if($gender=='2'){
      $gender='female';
    }
    else if($gender=='3'){
      $gender='boy';
    }
    else if($gender=='4'){
      $gender='boy78';
    }
    else if($gender=='5'){
      $gender='girl';
    }
    else if($gender=='6'){
      $gender='girl78';
    }


  $create_date=date("d-m-y-h-i-s");
  $client_name=$user_name;

  $second_DB = $this->load->database('another_db', TRUE); 


 $pautoquery = "SELECT * FROM `tbl_trun_pic` where  sku_no='$sku' and brand='$user_name'  ";
    // echo $pautoquery;
  $query2=$second_DB->query($pautoquery);
  $usrinfo= $query2->result_array();
  if(empty($usrinfo))
  {
      $data=array(
      'sku_no' => $sku,
      'img_project_name' => $sku,
      'img_name' => $sku,
      'gender' => $gender,
      'category' => $category,
      'create_date' => $create_date,
      'status' => '0',
      'client_name'=>$client_name,
      'brand'=>$client_name
      );
      //print_r($data);
      $second_DB->insert('tbl_trun_pic', $data);
  }



  $photoshop_upload_path=PHOTOSHOP_UPLOAD_PATH;

   if (!is_dir($photoshop_upload_path."Piquic@".$client_name)) {
    mkdir($photoshop_upload_path."Piquic@".$client_name, 0777, true);
    }
    $target = $photoshop_upload_path."Piquic@".$client_name."/";

    $source= "./upload/".$user_id."/".$sku."/";


    
    $images = glob($source."*.{jpg,JPG,jpeg,JPEG,png,PNG}", GLOB_BRACE);

    //echo "<pre>";
   //print_r($images); 
    // $images = glob($imgdirname."*.JPG");
    foreach ($images as $file_path){

      $target_path=str_replace("./upload/".$user_id."/".$sku."/",$target,$file_path);
      copy($file_path, $target_path);

      //unlink($file_path);
    }    
    

    ///////////////////////////////////////////////////////////////////////////////////////////////

  $skuno=$sku;

  $pautoquery = "SELECT * FROM `tbl_upload_img` where user_id='$user_id' and zipfile_name='$skuno'  ";
    // echo $pautoquery;
  $query2=$this->db->query($pautoquery);
  $usrinfo= $query2->result_array();
  $upimg_id=$usrinfo[0]['upimg_id'];



  $pautoquery = "SELECT count(*) as ctn_sku_image FROM `tbl_upload_images` where upimg_id='$upimg_id'  ";
    // echo $pautoquery;
  $query2=$this->db->query($pautoquery);
  $usrinfo= $query2->result_array();
  $ctn_sku_image=$usrinfo[0]['ctn_sku_image'];



  $pautoquery = "SELECT * FROM `tbl_users` where user_id='$user_id'  ";
    // echo $pautoquery;
  $query2=$this->db->query($pautoquery);
  $usrinfo= $query2->result_array();
  $total_amount=$usrinfo[0]['total_amount'];
  $total_credit=$usrinfo[0]['total_credit'];
  $user_currency=$usrinfo[0]['user_currency'];

  if($user_currency=='')
  {
      $user_currency='USD';
  }

  $setting_key="PER_PRICE_".$user_currency;
  
  $query=$this->db->query("select * from  tbl_settings  WHERE setting_key='$setting_key' ");
  $userDetails= $query->result_array();
  $per_price=$userDetails[0]['setting_value'];
  
  $payment_amount=$per_price*$ctn_sku_image;
  $payment_credit=$ctn_sku_image;

  

  $current_amount=$total_amount-$payment_amount;
  $current_credit=$total_credit-$payment_credit;
  


  $userdata=array(
    'total_amount' => $current_amount,
    'total_credit' => $current_credit
    );

  $this->db->where('user_id', $user_id);
  $report = $this->db->update('tbl_users', $userdata);


  $data=array(
    'user_id' => $user_id,
    'billing_id' => '0',
    'sty_id' => $sty_id,
    'upimg_id' => $upimg_id,
    'history_type' => 'Deduction',
    'amount' => $payment_amount,
    'credit' => $payment_credit,
    'account_history_date' => date("Y-m-d"),
    'account_history_time' => date("H:i:s"),
    'payment_status' => '',
  );
  $this->db->insert('tbl_account_history', $data);

    ///////////////////////////////////////////////////////////////////////////////////////////////


  if(strstr($current_amount,"-"))
  {
  $this->db->set('status_type', 'Reject'); 
  $this->db->set('comments', 'Warning - Payment Pending');
  $this->db->set('style_payment_status', 'Not Paid');  
  
  }
  else
  {
  $this->db->set('style_payment_status', 'Paid');  
  }

  $this->db->set('style_amount', $payment_amount); 
  $this->db->set('style_credit', $payment_credit);


  if(strstr($total_amount,"-")  && strstr($total_credit,"-") )
  {
    $this->db->set('style_pending_amount', "-".$payment_amount); 
    $this->db->set('style_pending_credit', "-".$payment_credit); 
  }
  else
  {
    $this->db->set('style_pending_amount', $current_amount); 
    $this->db->set('style_pending_credit', $current_credit); 
  }
  

  //$this->db->where('zipfile_name', $sku);
  $array = array('sty_id' => $sty_id);
  $this->db->where($array); 
  $result=$this->db->update('tbl_style');
    
   
    








}

  function update_autostylingsku($skuno,$autoval){ 


    if ($this->session->userdata('front_logged_in')) {
      $session_data = $this->session->userdata('front_logged_in');
      $user_id = $session_data['user_id'];
      $user_name = $session_data['user_name'];
    }
    else
    {
      $user_id = '';
      $user_name = '';
    }


    if($skuno==''){
     $this->db->set('autostyle', '1'); 
     $this->db->set('status', '2');
     $this->db->set('stage', 'Styling');
     $this->db->set('comments', 'Pending');
     $result=$this->db->update('tbl_style');

     $query=$this->db->query("select * from  tbl_style WHERE autostyle='1' and user_id='$user_id' AND status='0'");
     $row=$query->result();
     foreach($row as $querow){          
       $sku=$querow->skuno;
       $this->db->set('status', '0');       
   //$this->db->where('zipfile_name', $sku);
       $array = array('zipfile_name' => $sku, 'user_id' => $user_id);
       $this->db->where($array); 
       $result=$this->db->update('tbl_upload_img');


       $sku=$querow->skuno;
       $gender=$querow->gender;
       $category=$querow->category;
       $create_date=date("d-m-y-h-i-s");
       $client_name=$querow->client_name; 

       if($gender=='1'){
        $gender='male';
      }
      else if($gender=='2'){
        $gender='female';
      }
      else if($gender=='3'){
        $gender='boy';
      }
      else if($gender=='4'){
        $gender='boy78';
      }
      else if($gender=='5'){
        $gender='girl';
      }
      else if($gender=='6'){
        $gender='girl78';
      }  
   //$this->db->set('status', '1');      
   //$CI = &get_instance();
   //$this->db2 = $CI->load->database('another_db', TRUE); 
      $second_DB = $this->load->database('another_db', TRUE); 

      $data=array(
        'sku_no' => $sku,
        'img_project_name' => $sku,
        'img_name' => $sku,
        'gender' => $gender,
        'category' => $category,
        'create_date' => $create_date,
        'status' => '0',
        'client_name'=>$client_name
      );
      $second_DB->insert('tbl_trun_pic', $data);

    }
  }

  else{

   $skuno=explode(',', $skuno); 
   foreach ($skuno as $sku) {


     $this->db->set('autostyle', '1'); 
     $this->db->set('status', '2');
     $this->db->set('stage', 'Styling');
     $this->db->set('comments', 'Pending');

     $this->db->where('skuno', $sku);
     $result=$this->db->update('tbl_style');

     $this->db->set('status', '0');       
   //$this->db->where('zipfile_name', $sku);
     $array = array('zipfile_name' => $sku, 'user_id' => $user_id);
     $this->db->where($array); 
     $result=$this->db->update('tbl_upload_img');




   //echo "select * from  tbl_style WHERE autostyle='1' and user_id='$user_id' AND skuno='$sku'";

     $query=$this->db->query("select * from  tbl_style WHERE autostyle='1' and user_id='$user_id' AND skuno='$sku'");
     $rowstyle=$query->result_array();
     $gender=$rowstyle[0]['gender'];
     $category=$rowstyle[0]['category'];

     if($gender=='1'){
      $gender='male';
    }
    else if($gender=='2'){
      $gender='female';
    }
    else if($gender=='3'){
      $gender='boy';
    }
    else if($gender=='4'){
      $gender='boy78';
    }
    else if($gender=='5'){
      $gender='girl';
    }
    else if($gender=='6'){
      $gender='girl78';
    }
    /*else if($gender=='ps-male'){
    $gender='1';
    }else if($gender=='ps-female'){
    $gender='2';
  }*/


  $create_date=date("d-m-y-h-i-s");
  $client_name=$user_name;

  $second_DB = $this->load->database('another_db', TRUE); 

  $data=array(
    'sku_no' => $sku,
    'img_project_name' => $sku,
    'img_name' => $sku,
    'gender' => $gender,
    'category' => $category,
    'create_date' => $create_date,
    'status' => '0',
    'client_name'=>$client_name
  );
  $second_DB->insert('tbl_trun_pic', $data);




}
}




}



function getmodelbygender($gender_id){ 

  if ($this->session->userdata('front_logged_in')) {
    $session_data = $this->session->userdata('front_logged_in');
    $user_id = $session_data['user_id'];
    $user_name = $session_data['user_name'];
  }
  else
  {
    $user_id = '';
    $user_name = '';
  }


  $second_DB = $this->load->database('another_db', TRUE); 

  $pautoquery = "SELECT * FROM `tbl_model` where status='1' and gen_id='$gender_id' ORDER by `m_id` ";
    // echo $pautoquery;
  $query2=$second_DB->query($pautoquery);
  return $modelinfo= $query2->result_array();



}







function update_processing($skuno,$sty_id){ 

  if ($this->session->userdata('front_logged_in')) {
    $session_data = $this->session->userdata('front_logged_in');
    $user_id = $session_data['user_id'];
    $user_name = $session_data['user_name'];
  }
  else
  {
    $user_id = '';
    $user_name = '';
  }



///////////////////////////////////////////////////////////////////////////////////////////////


  $pautoquery = "SELECT * FROM `tbl_upload_img` where user_id='$user_id' and zipfile_name='$skuno'  ";
    // echo $pautoquery;
  $query2=$this->db->query($pautoquery);
  $usrinfo= $query2->result_array();
  $upimg_id=$usrinfo[0]['upimg_id'];



  $pautoquery = "SELECT count(*) as ctn_sku_image FROM `tbl_upload_images` where upimg_id='$upimg_id'  ";
    // echo $pautoquery;
  $query2=$this->db->query($pautoquery);
  $usrinfo= $query2->result_array();
  $ctn_sku_image=$usrinfo[0]['ctn_sku_image'];



  $pautoquery = "SELECT * FROM `tbl_users` where user_id='$user_id'  ";
    // echo $pautoquery;
  $query2=$this->db->query($pautoquery);
  $usrinfo= $query2->result_array();
  $total_amount=$usrinfo[0]['total_amount'];
  $total_credit=$usrinfo[0]['total_credit'];
  $user_currency=$usrinfo[0]['user_currency'];

  if($user_currency=='')
  {
      $user_currency='USD';
  }

  $setting_key="PER_PRICE_".$user_currency;
  
  $query=$this->db->query("select * from  tbl_settings  WHERE setting_key='$setting_key' ");
  $userDetails= $query->result_array();
  $per_price=$userDetails[0]['setting_value'];
  
  $payment_amount=$per_price*$ctn_sku_image;
  $payment_credit=$ctn_sku_image;

  

  $current_amount=$total_amount-$payment_amount;
  $current_credit=$total_credit-$payment_credit;
  


  $userdata=array(
    'total_amount' => $current_amount,
    'total_credit' => $current_credit);

  $this->db->where('user_id', $user_id);
  $report = $this->db->update('tbl_users', $userdata);


  $data=array(
    'user_id' => $user_id,
    'billing_id' => '0',
    'sty_id' => $sty_id,
    'upimg_id' => $upimg_id,
    'history_type' => 'Deduction',
    'amount' => $payment_amount,
    'credit' => $payment_credit,
    'account_history_date' => date("Y-m-d"),
    'account_history_time' => date("H:i:s"),
    'payment_status' => '',
  );
  $this->db->insert('tbl_account_history', $data);

    ///////////////////////////////////////////////////////////////////////////////////////////////


$this->db->set('status', '2');
$this->db->set('stage', 'Processing'); 

if(strstr($current_amount,"-"))
  {
  $this->db->set('status_type', 'Reject'); 
  $this->db->set('comments', 'Warning - Payment Pending');
  $this->db->set('style_payment_status', 'Not Paid');  
  

  }
  else
  {
  $this->db->set('style_payment_status', 'Paid');  
  }



  $this->db->set('style_amount', $payment_amount); 
  $this->db->set('style_credit', $payment_credit);

  if(strstr($total_amount,"-")  && strstr($total_credit,"-") )
  {
    $this->db->set('style_pending_amount', "-".$payment_amount); 
    $this->db->set('style_pending_credit', "-".$payment_credit); 
  }
  else
  {
    $this->db->set('style_pending_amount', $current_amount); 
    $this->db->set('style_pending_credit', $current_credit); 
  }


    //$this->db->where('zipfile_name', $sku);
$array = array('sty_id' => $sty_id);
$this->db->where($array); 
$result=$this->db->update('tbl_style');







}



function delete_skuStyleque($skuno,$sty_id){ 

    if ($this->session->userdata('front_logged_in')) {
    $session_data = $this->session->userdata('front_logged_in');
    $user_id = $session_data['user_id'];
    $user_name = $session_data['user_name'];
    }
    else
    {
    $user_id = '';
    $user_name = '';
    }

 //echo "select * from  tbl_upload_img where zipfile_name='$skuno'  and user_id='$user_id' ";
   $query=$this->db->query("select * from  tbl_upload_img where zipfile_name='$skuno'  and user_id='$user_id' ");   
   $row=$query->result_array();
   $del=$row[0]['zipfile_name'];
    $upimg_id=$row[0]['upimg_id'];
   $directory='upload';
   $imgdirname=$directory . '/' . $user_id .'/'. $del .'/';
   $dirPath=$directory . '/' . $user_id .'/'. $del;
  $files =glob($imgdirname."*.{jpg,JPG,jpeg,JPEG,png,PNG}", GLOB_BRACE);
  foreach($files as $file){ 
      unlink($file); //delete file
    }

   rmdir($dirPath);


   //exit;

   $array = array('upimg_id' => $upimg_id);
   $this->db->where($array); 
   $result=$this->db->delete('tbl_upload_images');

   //$this->db->where('zipfile_name', $skuno);
   $array = array('zipfile_name' => $skuno, 'user_id' => $user_id);
   $this->db->where($array); 
   $result=$this->db->delete('tbl_upload_img');


   $array = array('sty_id' => $sty_id, 'user_id' => $user_id);
   $this->db->where($array); 
   $result=$this->db->delete('tbl_style');



   return $result;

    }





}

?>