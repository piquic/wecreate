<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Login_model extends CI_Model
{
    public function login($username, $password)
    {
        $this->db->select('*');
        $this->db->from('user');
     //   $this->db->where('u_rolecode', 1);
        $this->db->where('email', $username);
        $this->db->where('password', sha1($password));
        $this->db->limit(1);

        $query = $this->db->get();

//echo $query->num_rows(); exit();
        if ($query->num_rows() == 1) {
            return $query->result();
        } else {
            return false;
        }
    }
	
	
		
	    public function loginadmin($username, $password)
    {
        $this->db->select('*');
        $this->db->from('login_admin');
     //   $this->db->where('u_rolecode', 1);
        $this->db->where('email', $username);
        $this->db->where('password', sha1($password));
		$this->db->where('status', 'Active');
        $this->db->limit(1);

        $query = $this->db->get();

//echo $query->num_rows(); exit();
        if ($query->num_rows() == 1) {
            return $query->result();
        } else {
            return false;
        }
    }
	
	
	
	    public function loginusers($username, $password)
    {
        $this->db->select('*');
        $this->db->from('user');
     //   $this->db->where('u_rolecode', 1);
        $this->db->where('user_email', $username);
        $this->db->where('user_password', $password);
		$this->db->where('user_status', 'Enable');
        $this->db->limit(1);

        $query = $this->db->get();

//echo $query->num_rows(); exit();
        if ($query->num_rows() == 1) {
            return $query->result();
        } else {
            return false;
        }
    }
	
	
	
	
	
	
}

?>