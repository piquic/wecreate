<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
 
class UpdateUser_model extends CI_Model
{
    public function __construct()
    {
        $this->load->database();
        $this->load->library('session');
    }
	
	public function get_usr_data($user_id)
    {
        $sql = "select * from tbl_users where user_id = '$user_id'";
        $query = $this->db->query($sql);
        return $query->result_array();
    }

	public function update_user($userdata, $user_id)
    {
        $this->db->where('user_id', $user_id);

        $usrUpdate = $this->db->update('tbl_users', $userdata);

        if ($usrUpdate) {
            return true;
        } else {
            return false;
        }
    }

    public function update_pswd($userpswd, $user_id)
    {
        $this->db->where('user_id', $user_id);

        $pswdUpdate = $this->db->update('tbl_users', $userpswd);

        if ($pswdUpdate) {
            return true;
        } else {
            return false;
        }
    }
}