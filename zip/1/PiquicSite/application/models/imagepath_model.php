<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Imagepath_model extends CI_Model
{
	public function __construct()
	{
		$this->load->database();
		$this->load->library('session');
	}


	public function create_image_path($details_array)
	{
		// echo "<pre>";

		// print_r($details_array);
		$today=date('Y-m-d');
		$expiry_date=date('Y-m-d',strtotime('+30 days',strtotime($today))) . PHP_EOL;
		//print_r($expiry_date);
		//print_r($details_array['Category']);
		foreach ($details_array as $key => $value) {
    //echo $details_array[$key]->Category."<br>";
			$user_id = $details_array[$key]->UserID;
    //$zipfile_name = $details_array[$key]->OrderNo;
    //$category = $details_array[$key]->Category;
			if ($details_array[$key]->Gender == "MALE") {
				$gender = "1";
			}
			elseif ($details_array[$key]->Gender == "FEMALE") {
				$gender = "2";
			}
			if ($details_array[$key]->Category == "Men Apparels") {
				$category = "bottom";
			}
			elseif ($details_array[$key]->Category == "Womens Apparels") {
				$category = "dress";
			}
			$down_img_size = "800px X 1200px";
			$order_status = "1";
			$order_date = date("Y/m/d");
			$zipfile_name = $details_array[$key]->SKUNo;
			$vendorcode=$details_array[$key]->VendorCode;
			$sku_thumb_path = 'sku_img_thumb/'.$zipfile_name.'/';
     //print_r($sku_thumb_path);
    //print_r($sku_no);
			$this->db->select('*');
			$this->db->from('tbl_users');
			$this->db->where('user_id', $user_id);
			$query1 = $this->db->get();
			if ($query1->num_rows() >=0) {
				$result1=$query1->result_array();
				$client_name = $result1[0]['user_name'];
				
			} else {
			//return false;
			}

			$this->db->select('*');
			$this->db->from('tbl_download_img');
			$this->db->where('zipfile_name', $zipfile_name);
			$query = $this->db->get();
			if ($query->num_rows() >0) {
				$name_error = "Sorry... order_no already inserted.";
				
			} else {

				$result= $this->db->insert('tbl_download_img', array('user_id'=>$user_id,'zipfile_name'=>$zipfile_name,'category'=>$category,'gender'=>$gender,'queue'=>0,'status'=>0,'client_name'=>$client_name,'vendor_id'=>$vendorcode,'sku_img_path'=>$sku_thumb_path,'create_date'=>$today,'expiry_date'=>$expiry_date,'downloaded'=>2));
				if($result){
					$succmsg = "You Successfully inserteded the VALUES.";
				}
			}
			$ImagePath_array =  $details_array[$key]->ImagePath;
			foreach ($ImagePath_array as $ImagePath_array_key => $ImagePath_array_value) {
				
				$sku_img_path = $ImagePath_array_value->Path;
      // echo "<br>".$sku_img_path;
				$sku_img_path = str_replace("\\","/",$sku_img_path);
				$this->db->select('*');
				$this->db->from('tbl_download_img_path');
				$this->db->where('location_name', $sku_img_path);
				$query = $this->db->get();
				if ($query->num_rows() >0) {
					$name_error = "Sorry... order_no already inserted.";
					
				} else {

					$result= $this->db->insert('tbl_download_img_path', array('zipfile_name'=>$zipfile_name,'location_name'=>$sku_img_path));
					if($result){
						$succmsg = "You Successfully inserteded the VALUES.";
					}
				}

				
			}

		}
		

	}
	
}