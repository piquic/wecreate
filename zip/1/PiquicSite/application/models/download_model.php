<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Download_model extends CI_Model
{
	public function __construct()
	{
		$this->load->database();
		$this->load->library('session');
	}


	public function get_image($user_id,$rowperpage,$lastid,$ordno,$modelgen,$modelcat,$status)
	{

		//echo "szdfsd".$modelgen;
		$this->db->select('*');
		$this->db->from('tbl_download_img');
		$this->db->where('user_id', $user_id);
		$this->db->where('status_thumb', '1');
		if($lastid!='')
		{

			$this->db->where('upimg_id <', $lastid);
		}
		if(!empty($ordno))
		{
			$this->db->where_in('zipfile_name',  explode(',', $ordno));
		}
		if(!empty($modelgen))
		{
			$this->db->where_in('gender',explode(',', $modelgen));
		}
		if(!empty($modelcat))
		{
			$this->db->where_in('category',  explode(',', $modelcat));
		}
		if(!empty($status))
		{
			$this->db->where_in('downloaded',  explode(',', $status));
		}
		$this->db->order_by('upimg_id', 'DESC');
		$this->db->order_by('downloaded', 'DESC');


		$this->db->limit($rowperpage);   
		
		$query = $this->db->get();
		
		//echo $this->db->last_query();
		//exit();
		$count_row= $query->num_rows(); 
		//print_r($count_row);
		if ($query->num_rows() >=0) {
			$result=$query->result_array();
			//print_r($result);
			
			return $result;
			
		} else {
			return false;
		}
		//exit();

	}
	// // Select total records
	public function getimagerecordCount($user_id,$rowperpage,$lastid,$ordno,$modelgen,$modelcat,$status) {

		$this->db->select('count(*) as allcount');
		$this->db->from('tbl_download_img');
		$this->db->where('user_id', $user_id);
		$this->db->where('status_thumb', '1');
		if(!empty($ordno))
		{
			$this->db->where_in('zipfile_name', explode(',',  $ordno));
		}
		if(!empty($modelgen))
		{
			$this->db->where_in('gender', explode(',', $modelgen));
		}
		if(!empty($modelcat))
		{
			$this->db->where_in('category',  explode(',', $modelcat));
		}
		if($lastid!='')
		{

			$this->db->where('upimg_id <', $lastid);
		}
		if(!empty($status))
		{
			$this->db->where_in('downloaded',  explode(',', $status));
		}
		$this->db->order_by('upimg_id', 'DESC');
		$this->db->order_by('downloaded', 'DESC');


		//$this->db->limit($rowperpage); 

      // $this->db->select('count(*) as allcount');
      // $this->db->from('tbl_img_download_path');
		$query = $this->db->get();
		$result = $query->result_array();
      // echo $this->db->last_query();
		
		return $result[0]['allcount'];
	}
	public function gender_count($user_id) {

		$this->db->select('count(gender) as gender');
		$this->db->from('tbl_download_img');
		$this->db->where('user_id', $user_id);
		$this->db->where('status_thumb', '1');
		if(!empty($ordno))
		{
			$this->db->where_in('zipfile_name', explode(',',  $ordno));
		}
		if(!empty($modelgen))
		{
			$this->db->where_in('gender', explode(',', $modelgen));
		}
		if(!empty($modelcat))
		{
			$this->db->where_in('category',  explode(',', $modelcat));
		}
		if($lastid!='')
		{

			$this->db->where('upimg_id <', $lastid);
		}
		if(!empty($status))
		{
			$this->db->where_in('downloaded',  explode(',', $status));
		}
		$this->db->order_by('upimg_id', 'DESC');
		$this->db->order_by('downloaded', 'DESC');

		$query = $this->db->get();
		$result = $query->result_array();
      	// echo $this->db->last_query();
		return $result[0]['allcount'];
	}
	public function upload_img_insert($data)
	{
		$this->db->insert('tbl_download_img', $data);
	}
	public function upload_img_update($userdata, $upimg_id)
	{
		$this->db->where('upimg_id', $upimg_id);
		$report = $this->db->update('tbl_download_img', $userdata);
		if ($report) {
			return true;
		} else {
			return false;
		}
	}
 //    public function upload_images_insert($data)
	// {
 //        $this->db->insert('tbl_upload_images', $data);
 //    }
	// public function upload_images_update($userdata, $id)
 //    {
 //        $this->db->where('id', $id);
 //        $report = $this->db->update('tbl_upload_images', $userdata);
 //        if ($report) {
 //            return true;
 //        } else {
 //            return false;
 //        }
 //    }
	
	public function add_user($data)
	{
		$this->db->insert('tbl_users', $data);
	}

	public function getOwnerlist()
	{
		$sql = 'select * from nc_user where usertype="2"';
		$query = $this->db->query($sql);
		return $query->num_rows();
	}


}