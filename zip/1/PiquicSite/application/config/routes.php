<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	http://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There area two reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router what URI segments to use if those provided
| in the URL cannot be matched to a valid route.
|
*/
/*backend*/
/*$route['default_controller'] = 'login';
$route['admin_login'] = "login";
$route['logout'] = 'dashboard/logout';
$route['404_override'] = '';
$route['dashboard'] = 'dashboard';*/


////////////////////////////////ADMIN////////////////////////////////////////

$route['administrator'] = 'admin/administrator/index';
$route['administrator/check_login'] = 'admin/administrator/check_login';
$route['administrator/dashboard'] = 'admin/administrator/dashboard';

$route['administrator/manage-user'] = 'admin/user/index';
$route['administrator/add-user'] ='admin/user/add';
$route['administrator/view-user/(:any)/(:any)'] ='admin/user/add/$1/$2';
$route['administrator/edit-user/(:any)'] ='admin/user/add/$1';
$route['administrator/update-user/(:any)'] ='admin/user/update_user/$1';


$route['administrator/manage-page-content'] = 'admin/page/index';
$route['administrator/add-page'] ='admin/page/add';
$route['administrator/view-page/(:any)/(:any)'] ='admin/page/add/$1/$2';
$route['administrator/edit-page/(:any)'] ='admin/page/add/$1';
$route['administrator/update-page/(:any)'] ='admin/page/update_user/$1';


$route['administrator/image_size'] = 'admin/image_size/index';



$route['administrator/manage-plantype'] = 'admin/plantype/index';
$route['administrator/add-plantype'] ='admin/plantype/add';
$route['administrator/view-plantype/(:any)/(:any)'] ='admin/plantype/add/$1/$2';
$route['administrator/edit-plantype/(:any)'] ='admin/plantype/add/$1';
$route['administrator/update-plantype/(:any)'] ='admin/plantype/update_user/$1';



$route['administrator/manage-paymenttype'] = 'admin/paymenttype/index';
$route['administrator/add-paymenttype'] ='admin/paymenttype/add';
$route['administrator/view-paymenttype/(:any)/(:any)'] ='admin/paymenttype/add/$1/$2';
$route['administrator/edit-paymenttype/(:any)'] ='admin/paymenttype/add/$1';
$route['administrator/update-paymenttype/(:any)'] ='admin/paymenttype/update_user/$1';



$route['administrator/manage-plan'] = 'admin/plan/index';
$route['administrator/add-plan'] ='admin/plan/add';
$route['administrator/view-plan/(:any)/(:any)'] ='admin/plan/add/$1/$2';
$route['administrator/edit-plan/(:any)'] ='admin/plan/add/$1';
$route['administrator/update-plan/(:any)'] ='admin/plan/update_user/$1';


$route['administrator/manage-setting'] = 'admin/setting/index';
$route['administrator/update_setting'] = 'admin/setting/update_setting';

$route['administrator/manage-order-upload'] = 'admin/order_upload/index';
$route['administrator/update-order-upload'] = 'admin/order_upload/update_order';

$route['administrator/manage-order'] = 'admin/order/index';
$route['administrator/update_order'] = 'admin/order/update_order';

////////////////////////////////FRONT////////////////////////////////////////


$route['default_controller'] = 'front/home';

$route['home'] = 'front/home/index';
$route['login'] = 'front/home/login';
$route['logout'] = 'front/home/logout';
$route['reset_password/(:any)'] = 'front/home/reset_password/$1';
$route['expired'] = 'front/home/expired';
$route['uploadimage'] = 'front/upload/uploadimage';

$route['label'] = 'front/labelimages/index';
$route['label/(:any)'] = 'front/labelimages/index/$1';

$route['stylequeue'] = 'front/stylequeue/stylequeueview';
$route['stylequeue/(:any)'] = 'front/stylequeue/stylequeueview/$1';
$route['stylequeue/(:any)/(:any)'] = 'front/stylequeue/stylequeueview/$1/$2';

$route['style'] = 'front/style/styleview';
$route['style/(:any)'] = 'front/style/styleview/$1';

$route['styleframe/(:any)'] = 'front/style/styleframeview/$1';
$route['styleframe/(:any)/(:any)'] = 'front/style/styleframeview/styleview/$1/$2';

$route['processing'] = 'front/processing/index';
$route['processing/(:any)'] = 'front/processing/index/$1';
$route['processing/(:any)/(:any)'] = 'front/processing/index/$1/$2';

$route['plans'] = 'front/plans/index';
$route['modelselect'] = 'front/modelselect/index';

$route['download'] = 'front/download';
$route['download/(:any)'] = 'front/download/showdownload/$1';

$route['generatethumb'] = 'front/generatethumb';
$route['imagepath'] = 'front/imagepath';

$route['payments'] = 'front/payments/index';
$route['payments/(:any)'] = 'front/payments/index/$1';


$route['paymentdetails'] = 'front/payments/paymentdetails';
$route['paymentdetails/(:any)'] = 'front/payments/paymentdetails/$1';


$route['success'] = 'front/payments/paymentsuccess';
$route['failure'] = 'front/payments/paymentfailure';

$route['myaccount'] = 'front/myaccount/index';
$route['myaccount/(:any)'] ='front/myaccount/index/$1';


$route['autostyle'] = 'front/stylequeue/autostyleview';
$route['autostyle/(:any)'] ='front/stylequeue/autostyleview/$1';


$route['paysuccess'] = 'front/payments/paysuccess';
$route['payfailure'] = 'front/payments/payfailure';

$route['booking'] = 'front/booking/index';
$route['booking/(:any)'] = 'front/booking/index/$1';

// $route['revbook'] = 'front/revbook/index';
$route['revbook/(:any)'] ='front/revbook/index/$1';

$route['booking_payments'] = 'front/booking_payments/index';
$route['booking_payments/(:any)'] = 'front/booking_payments/index/$1';


$route['booking_paymentdetails'] = 'front/booking_payments/booking_paymentdetails';
$route['booking_paymentdetails/(:any)'] = 'front/booking_payments/booking_paymentdetails/$1';

$route['bookpaymentsuccess'] = 'front/booking_payments/paymentsuccess';
$route['bookpaymentfailure'] = 'front/booking_payments/bookfailure';

$route['bookpaysuccess'] = 'front/booking_payments/paysuccess';
$route['bookpayfailure'] = 'front/booking_payments/payfailure';


$route['booksuccess'] = 'front/booking_payments/booksuccess';
$route['bookfailure'] = 'front/booking_payments/bookfailure';

$route['order'] = 'front/order/index';

$route['privacy-policy'] = 'front/privacy_policy/index';


$route['activate-registration/(:any)'] = 'front/home/activateregistration/$1';
$route['activate-registration'] = 'front/home/activateregistration';










$route['manage-user'] ='user/index';
$route['add-user'] ='user/add';
$route['view-user/(:any)/(:any)'] ='user/add/$1/$2';
$route['edit-user/(:any)'] ='user/add/$1';
$route['update-user/(:any)'] ='user/update_user/$1';


$route['googleLogin'] = 'front/home/googleLogin';

