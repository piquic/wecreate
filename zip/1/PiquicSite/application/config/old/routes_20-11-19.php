<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	http://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There area two reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router what URI segments to use if those provided
| in the URL cannot be matched to a valid route.
|
*/
/*backend*/
/*$route['default_controller'] = 'login';
$route['admin_login'] = "login";
$route['logout'] = 'dashboard/logout';
$route['404_override'] = '';
$route['dashboard'] = 'dashboard';*/


////////////////////////////////ADMIN////////////////////////////////////////

$route['administrator'] = 'admin/administrator/index';
$route['administrator/check_login'] = 'admin/administrator/check_login';
$route['administrator/dashboard'] = 'admin/administrator/dashboard';

$route['administrator/manage-user'] = 'admin/user/index';
$route['administrator/add-user'] ='admin/user/add';
$route['administrator/view-user/(:any)/(:any)'] ='admin/user/add/$1/$2';
$route['administrator/edit-user/(:any)'] ='admin/user/add/$1';
$route['administrator/update-user/(:any)'] ='admin/user/update_user/$1';


$route['administrator/manage-page-content'] = 'admin/page/index';
$route['administrator/add-page'] ='admin/page/add';
$route['administrator/view-page/(:any)/(:any)'] ='admin/page/add/$1/$2';
$route['administrator/edit-page/(:any)'] ='admin/page/add/$1';
$route['administrator/update-page/(:any)'] ='admin/page/update_user/$1';


$route['administrator/image_size'] = 'admin/image_size/index';



$route['administrator/manage-plantype'] = 'admin/plantype/index';
$route['administrator/add-plantype'] ='admin/plantype/add';
$route['administrator/view-plantype/(:any)/(:any)'] ='admin/plantype/add/$1/$2';
$route['administrator/edit-plantype/(:any)'] ='admin/plantype/add/$1';
$route['administrator/update-plantype/(:any)'] ='admin/plantype/update_user/$1';



$route['administrator/manage-paymenttype'] = 'admin/paymenttype/index';
$route['administrator/add-paymenttype'] ='admin/paymenttype/add';
$route['administrator/view-paymenttype/(:any)/(:any)'] ='admin/paymenttype/add/$1/$2';
$route['administrator/edit-paymenttype/(:any)'] ='admin/paymenttype/add/$1';
$route['administrator/update-paymenttype/(:any)'] ='admin/paymenttype/update_user/$1';



$route['administrator/manage-plan'] = 'admin/plan/index';
$route['administrator/add-plan'] ='admin/plan/add';
$route['administrator/view-plan/(:any)/(:any)'] ='admin/plan/add/$1/$2';
$route['administrator/edit-plan/(:any)'] ='admin/plan/add/$1';
$route['administrator/update-plan/(:any)'] ='admin/plan/update_user/$1';



////////////////////////////////FRONT////////////////////////////////////////


$route['default_controller'] = 'front/home';

$route['home'] = 'front/home/index';
$route['login'] = 'front/home/login';
$route['logout'] = 'front/home/logout';
$route['uploadimage'] = 'front/upload/uploadimage';
$route['label'] = 'front/labelimages/index';
$route['stylequeue'] = 'front/stylequeue/stylequeueview';
$route['style'] = 'front/style/styleview';
$route['style/(:any)'] = 'front/style/styleview/$1';

$route['processing'] = 'front/processing/index';
$route['plans'] = 'front/plans/index';
$route['modelselect'] = 'front/modelselect/index';

$route['download'] = 'front/download/showdownload';
$route['download/(:any)'] = 'front/download/showdownload/$1';


$route['payments'] = 'front/payments/index';
$route['payments/(:any)'] = 'front/payments/index/$1';


$route['paymentdetails'] = 'front/payments/paymentdetails';
$route['paymentdetails/(:any)'] = 'front/payments/paymentdetails/$1';


















































$route['manage-user'] ='user/index';
$route['add-user'] ='user/add';
$route['view-user/(:any)/(:any)'] ='user/add/$1/$2';
$route['edit-user/(:any)'] ='user/add/$1';
$route['update-user/(:any)'] ='user/update_user/$1';



$route['manage-customer'] ='customer/index';
$route['add-customer'] ='customer/add';
$route['view-customer/(:any)/(:any)'] ='customer/add/$1/$2';
$route['edit-customer/(:any)'] ='customer/add/$1';
$route['update-customer/(:any)'] ='customer/update_customer/$1';


$route['manage-entity'] ='entity/index';
$route['add-entity'] ='entity/add';
$route['view-entity/(:any)/(:any)'] ='entity/add/$1/$2';
$route['edit-entity/(:any)'] ='entity/add/$1';
$route['update-entity/(:any)'] ='entity/update_entity/$1';




$route['manage-menu'] ='menu/index';
$route['add-menu'] ='menu/add';
$route['view-menu/(:any)/(:any)'] ='menu/add/$1/$2';
$route['edit-menu/(:any)'] ='menu/add/$1';
$route['update-menu/(:any)'] ='menu/update_entity/$1';



$route['manage-item-category'] ='item_category/index';
$route['add-item-category'] ='item_category/add';
$route['view-item-category/(:any)/(:any)'] ='item_category/add/$1/$2';
$route['edit-item-category/(:any)'] ='item_category/add/$1';
$route['update-item-category/(:any)'] ='item_category/update_entity/$1';


$route['manage-item'] ='item/index';
$route['add-item'] ='tem/add';
$route['view-item/(:any)/(:any)'] ='tem/add/$1/$2';
$route['edit-item/(:any)'] ='tem/add/$1';
$route['update-item/(:any)'] ='tem/update_item/$1';




$route['manage-extras'] ='extras/index';
$route['add-extras'] ='extras/add';
$route['view-extras/(:any)/(:any)'] ='extras/add/$1/$2';
$route['edit-extras/(:any)'] ='extras/add/$1';
$route['update-extras/(:any)'] ='extras/update_extras/$1';




$route['manage-options'] ='options/index';
$route['add-options'] ='options/add';
$route['view-options/(:any)/(:any)'] ='options/add/$1/$2';
$route['edit-options/(:any)'] ='options/add/$1';
$route['update-options/(:any)'] ='options/update_options/$1';



$route['manage-segments'] ='segments/index';
$route['add-segments'] ='segments/add';
$route['view-segments/(:any)/(:any)'] ='segments/add/$1/$2';
$route['edit-segments/(:any)'] ='segments/add/$1';
$route['update-segments/(:any)'] ='segments/update_segments/$1';




$route['manage-promotionpoints'] ='promotionpoints/index';
$route['add-promotionpoints'] ='promotionpoints/add';
$route['view-promotionpoints/(:any)/(:any)'] ='promotionpoints/add/$1/$2';
$route['edit-promotionpoints/(:any)'] ='promotionpoints/add/$1';
$route['update-promotionpoints/(:any)'] ='promotionpoints/update_promotionpoints/$1';


$route['manage-sharepoints'] ='sharepoints/index';
$route['add-sharepoints'] ='sharepoints/add';
$route['view-sharepoints/(:any)/(:any)'] ='sharepoints/add/$1/$2';
$route['edit-sharepoints/(:any)'] ='sharepoints/add/$1';
$route['update-sharepoints/(:any)'] ='sharepoints/update_sharepoints/$1';




$route['manage-combos'] ='combos/index';
$route['add-combos'] ='combos/add';
$route['view-combos/(:any)/(:any)'] ='combos/add/$1/$2';
$route['edit-combos/(:any)'] ='combos/add/$1';
$route['update-combos/(:any)'] ='combos/update_combos/$1';






$route['manage-order'] ='order/index';
$route['add-order'] ='order/add';
$route['view-order/(:any)/(:any)'] ='order/add/$1/$2';
$route['edit-order/(:any)'] ='order/add/$1';
$route['update-order/(:any)'] ='order/update_order/$1';




$route['manage-banner'] ='banner/index';
$route['add-banner'] ='banner/add';
$route['view-banner/(:any)/(:any)'] ='banner/add/$1/$2';
$route['edit-banner/(:any)'] ='banner/add/$1';
$route['update-banner/(:any)'] ='banner/update_banner/$1';




$route['manage-slider'] ='slider/index';
$route['add-slider'] ='slider/add';
$route['view-slider/(:any)/(:any)'] ='slider/add/$1/$2';
$route['edit-slider/(:any)'] ='slider/add/$1';
$route['update-slider/(:any)'] ='slider/update_slider/$1';
































$route['jobs'] = 'jobs';
$route['business'] = 'business';
$route['edit-business/(:any)'] ='business/create_business/$1';


$route['user_roles'] = 'user_roles';
$route['users'] = 'users';
$route['ware_houses'] = 'ware_houses';
$route['items'] = 'items';
$route['price_list']='price_list';
$route['create_price_list']='price_list/create_price_list';
$route['functions']='functions';
$route['edit-user-role/(:any)']='functions/index/$1';
$route['view-user-role/(:any)']='functions/view_user_role/$1';
$route['edit-price-list/(:any)']='price_list/create_price_list/$1';
$route['view-price-list/(:any)']='price_list/view_price_list/$1';




   
$route['purchase_orders']='purchase_orders';
$route['create_purchase_order']='purchase_orders/create_purchase_order';
$route['edit-purchase-order/(:any)']='purchase_orders/create_purchase_order/$1';
$route['view-purchase-order/(:any)']='purchase_orders/view_purchase_order/$1';

$route['purchase_receive']='purchase_receive';
$route['create_purchase_receive']='purchase_receive/create_purchase_receive';
$route['edit-purchase-receive/(:any)']='purchase_receive/create_purchase_receive/$1';
$route['view-purchase-receive/(:any)']='purchase_receive/view_purchase_receive/$1';

$route['manage-setting'] ='admin/Setting/index';
$route['manage-owner-setting'] ='admin/Business_owner/manageowner';
$route['update-manage-owner'] ='admin/Business_owner/update_manage_owner';


$route['manage-colours'] ='Colours/index';
$route['add-colours'] ='Colours/add';
$route['view-colours/(:any)/(:any)'] ='Colours/add/$1/$2';
$route['edit-colours/(:any)'] ='Colours/add/$1';
$route['update-colours/(:any)'] ='Colours/update_fitting/$1';


$route['manage-entrypointtypes'] ='Entrypointtypes/index';
$route['add-entrypointtypes'] ='Entrypointtypes/add';
$route['view-entrypointtypes/(:any)/(:any)'] ='Entrypointtypes/add/$1/$2';
$route['edit-entrypointtypes/(:any)'] ='Entrypointtypes/add/$1';
$route['update-entrypointtypes/(:any)'] ='Entrypointtypes/update_fitting/$1';


$route['manage-sources'] ='Sources/index';
$route['add-sources'] ='Sources/add';
$route['view-sources/(:any)/(:any)'] ='Sources/add/$1/$2';
$route['edit-sources/(:any)'] ='Sources/add/$1';
$route['update-sources/(:any)'] ='Sources/update_fitting/$1';



$route['manage-fitting'] ='Fitting/index';
$route['add-fitting'] ='Fitting/add';
$route['view-fitting/(:any)/(:any)'] ='Fitting/add/$1/$2';
$route['edit-fitting/(:any)'] ='Fitting/add/$1';
$route['update-fitting/(:any)'] ='Fitting/update_fitting/$1';





$route['manage-brand'] ='Brand/index';
$route['add-brand'] ='Brand/add';
$route['view-brand/(:any)/(:any)'] ='Brand/add/$1/$2';
$route['edit-brand/(:any)'] ='Brand/add/$1';
$route['update-brand/(:any)'] ='Brand/update_fitting/$1';




$route['manage-job'] ='Job/index';
$route['add-job'] ='Job/add';
$route['view-job/(:any)'] ='Job/view/$1';
$route['edit-job/(:any)'] ='Job/add/$1';
$route['update-job/(:any)'] ='Job/update_fitting/$1';


$route['manage-factory-production'] ='Factory_production/report';
$route['add-factory-production'] ='Factory_production/add';
$route['view-factory-production/(:any)/(:any)'] ='Factory_production/add/$1/$2';
$route['edit-factory-production/(:any)'] ='Factory_production/add/$1';
$route['update-factory-production/(:any)'] ='Factory_production/update_fitting/$1';



$route['manage-factory-production-cutting-variable'] ='Factory_production/report_cutting_variable';
$route['add-factory-production'] ='Factory_production/add';
$route['view-factory-production/(:any)/(:any)'] ='Factory_production/add/$1/$2';
$route['edit-factory-production/(:any)'] ='Factory_production/add/$1';
$route['update-factory-production/(:any)'] ='Factory_production/update_fitting/$1';



$route['manage-factory-production-cutting'] ='Factory_production/report_cutting';
$route['add-factory-production'] ='Factory_production/add';
$route['view-factory-production/(:any)/(:any)'] ='Factory_production/add/$1/$2';
$route['edit-factory-production/(:any)'] ='Factory_production/add/$1';
$route['update-factory-production/(:any)'] ='Factory_production/update_fitting/$1';



$route['manage-factory-production-fitting'] ='Factory_production/report_fitting';
$route['add-factory-production'] ='Factory_production/add';
$route['view-factory-production/(:any)/(:any)'] ='Factory_production/add/$1/$2';
$route['edit-factory-production/(:any)'] ='Factory_production/add/$1';
$route['update-factory-production/(:any)'] ='Factory_production/update_fitting/$1';


$route['manage-factory-production-client-quote'] ='Factory_production/report_client_quote';
$route['manage-factory-production-part-list'] ='Factory_production/report_part_list';


$route['manage-setting'] ='Setting/index';
$route['add-setting'] ='Setting/add';


