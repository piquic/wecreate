<?php
/* For Add Edit service page start*/
$lang['Add Services'] = 'Add Services';
$lang['Add Services Details'] = 'Services';

$lang['Edit Services'] = 'Edit Services';
$lang['Edit Services Details'] = 'Services';

$lang['Service Business Name']='Business Name';
$lang['Service Business Name Details']='Business Name';


$lang['Service Name']='Service Name';
$lang['Service Name Details']='Service Name';

$lang['Price']='Price';
$lang['Price Details']='For Free - put 0,otherwise xx.xx format';

$lang['Required Deposit']='Required Deposit';
$lang['Required Deposit Details']='Numbers only,eg 28';


$lang['Time Before Booking']='Time Before Booking';
$lang['Time Before Booking Details']='Number of hours entered here will prevent customer from booking any time spot within this period. Example: if right now is 12pm and you have 4 hours in this field, the earliest time spot customers will be able to book is 4pm';

$lang['Description']='Description';
$lang['Description Details']='Service details will be here ';


$lang['Service Duration']='Service Duration';
$lang['Service Duration Details']='Duration of 1 booking interval (spot)';



$lang['Payment Mode']='Select The Point of Payment';
$lang['Payment Mode Details']='If offline invoice selected - reservation will be confirmed automatically and spot will be removed from availability, it is your responsibility to collect payment from customer outside.';

$lang['Minimum Booking per services']='If offline invoice selected - reservation will be confirmed automatically,it is your responsibility to collect payment from customer';

$lang['Maximum Booking per services']='Maximum Booking per services';
$lang['Maximum Booking per services Details']='If offline invoice selected - reservation will be confirmed automatically,it is your responsibility to collect payment from customer';



$lang['Sunday']='Sunday';
$lang['Sunday Details']='Sunday';



$lang['Monday']='Monday';
$lang['Monday Details']='Monday';



$lang['Tuesday']='Tuesday';
$lang['Tuesday Details']='Tuesday';



$lang['Wednesday']='Wednesday';
$lang['Wednesday Details']='Wednesday';

$lang['Thursday']='Thursday';
$lang['Thursday Details']='Thursday';

$lang['Friday']='Friday';
$lang['Friday Details']='Friday';

$lang['Saturday']='Saturday';
$lang['Saturday Details']='Saturday';



$lang['Start Time']='Start Time';
$lang['Start Time Details']='Start Time';

$lang['End Time']='End Time';
$lang['End Time Details']='End Time';

$lang['Package option']="Package option";
$lang['Select Package option']="Select Package option";

/* For Add Edit service page start*/


/* For Add Edit Booking page start*/



$lang['Add Booking'] = 'Add Booking';
$lang['Add Booking Details'] = 'Add Booking';


$lang['Edit Booking'] = 'Edit Booking';
$lang['Edit Booking Details'] = 'Edit Booking';

$lang['Business Name'] = 'Business Name';
$lang['Business Name Details'] = 'Select the relevant Business Name';


$lang['Customer'] = 'Customer';
$lang['Customer Details'] = 'Select the Customer related to the booking, or create a New Customer';


$lang['Service'] = 'Service';
$lang['Service Details'] = 'Select the Service that your Customer intend to book from the drop-down';

$lang['Service Price'] = 'Service Price';
$lang['Service Price Details'] = 'Service Price';




$lang['Special Instructions'] = 'Special Instructions';
$lang['Special Instructions Details'] = 'Special Instructions';

$lang['Booking Date'] = 'Booking Date';
$lang['Booking Date Details'] = 'Select the date of the Booking. The time-slots available will be displayed below';

$lang['Available Time Slots'] = 'Available Time Slots';
$lang['Available Time Slots Details'] = 'Available Time Slots';




/* For Add Edit Booking page start*/




/* For Add Edit Business Owner page start*/


$lang['Cancellation Charges'] = 'Cancellation Charges';
$lang['Cancellation Charges Details'] = 'Cancellation Charges here';


$lang['Number Of Days Prior To Cancellation'] = 'Number Of Days Prior To Cancellation';
$lang['Number Of Days Prior To Cancellation Details'] = 'Number Of Days Prior To Cancellation here';



$lang['Owner Business Name'] = 'Business Name';
$lang['Owner Business Name Details'] = 'Put your Business name here';


$lang['Business Type'] = 'Business Type';
$lang['Business Type Details'] = 'Select your Business Type ';

$lang['Contact Person Name'] = 'Contact Person Name';
$lang['Contact Person Name Details'] = 'Contact Person Name of this Business will be here';

$lang['Owner Email'] = 'Email';
$lang['Owner Email Details'] = 'Business email will be here';

$lang['Owner Mobile'] = 'Mobile';
$lang['Owner Mobile Details'] = 'Business mobile will be here';


$lang['Owner Theme Color'] = 'Color';
$lang['Owner Theme Color Details'] = 'Choose your color theme from this select box';


$lang['Owner Address'] = 'Address';
$lang['Owner Address Details'] = 'Put Business address here';

$lang['Owner City'] = 'City';
$lang['Owner City Details'] = 'Put Business city';

$lang['Owner State'] = 'State';
$lang['Owner State Details'] = 'Put Business State';

$lang['Owner Country'] = 'Country';
$lang['Owner Country Details'] = 'Put country';

$lang['Owner Sales Tax'] = 'Sales Tax';
$lang['Owner Sales Tax Details'] = 'Put sale tax percentage here';

$lang['Domain'] = 'Domain';
$lang['Domain Details'] = 'Put your domain link here';

$lang['ABN No'] = 'ABN No';
$lang['ABN No Details'] = 'Put your ABN No here';

$lang['Phone No'] = 'Phone No';
$lang['Phone No Details'] = 'Put your Phone No here';

$lang['Opening Time'] = 'Opening Time';
$lang['Opening Time Details'] = 'Select opening time here';



$lang['Closing Time'] = 'Closing Time';
$lang['Closing Time Details'] = 'Select closing time here';


$lang['Upload Logo'] = 'Upload Logo';
$lang['Upload Logo Details'] = 'Upload business logo';

$lang['User Name'] = 'User Name';
$lang['User Name Details'] = 'Put business username';

$lang['Password'] = 'Password';
$lang['Password Details'] = 'Put password';

$lang['Confirm Password'] = 'Confirm password';
$lang['Confirm Password Details'] = 'Put confirm password';



$lang['Booking Reminder On'] = 'Booking Reminder On';
$lang['Booking Reminder On Details'] = 'Please check to get get mail';

$lang['Payment Method'] = 'Payment Method';
$lang['Payment Method Details'] = 'Payment Method Details';

/* For Add Edit Booking page start*/

?>

