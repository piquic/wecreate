<?php  $this->load->view('admin/includes/globalcss');?>


<style>
/*.table-bordered {
    border: 1px solid #666666;
}*/
.showdbutton {
    cursor:pointer; 
  background-color:#666666; 
  margin:10px 0px; 
  padding:10px; 
  color:#FFFFFF;
  width:100%;
}

.disabledbutton {
    pointer-events: none;
    opacity: 0.9;
  
  cursor:pointer; 
  background-color:#999999; 
  margin:10px 0px; 
  padding:10px; 
  color:#FFFFFF;
  width:100%;
}
label.error{
 color:#FF0000;
 font-size:12px;
}
.col-md-6 {
    -ms-flex: 0 0 50%;
    flex: 0 0 50%;
    max-width: 50%;
}
.pl-3, .px-3 {
    padding-left: 1rem!important;
}
.pr-3, .px-3 {
    padding-right: 1rem!important;
}
.pt-3, .py-3 {
    padding-top: 1rem!important;
}
.p-2 {
    padding: .5rem!important;
}
hr {
    margin-top: 1rem;
    margin-bottom: 1rem;
    border: 0;
    border-top: 1px solid rgba(0,0,0,.1);
}
hr {
    box-sizing: content-box;
    height: 0;
    overflow: visible;
}
.rounded {
    border-radius: .25rem!important;
}
.border {
    border: 1px solid #dee2e6!important;
}
.row {
    display: -ms-flexbox;
    display: flex;
   /* -ms-flex-wrap: wrap;
    flex-wrap: wrap;*/
    margin-right: -15px;
    margin-left: -15px;
}
.col-md-5 {
    -ms-flex: 0 0 41.666667%;
    flex: 0 0 41.666667%;
    max-width: 41.666667%;
}
.p-3 {
    padding: 1rem!important;
}
.col-md-12 {
    -ms-flex: 0 0 100%;
    flex: 0 0 100%;
    max-width: 100%;
}
.text-piquic {
    color: #76aea1 !important;
}

.justify-content-between {
    -ms-flex-pack: justify!important;
    justify-content: space-between!important;
}
.d-flex {
    display: -ms-flexbox!important;
    display: flex!important;
}
.mb-3, .my-3 {
    margin-bottom: 1rem!important;
}
.rounded-lg {
    border-radius: .3rem!important;
}
.bg-light {
    background-color: #f8f9fa!important;
}
</style>
<div>
  <!-- Section content header -->
 
  <!-- Section content -->
    <section class="content">
    <div class="col-md-12">
      <!-- start Information Box -->
      <div class="box">
      <!-- box header style start-->
        <div class="col-md-12 box-header with-border" ><h3 class="box-title">Booking Details - <?php echo $order_id; ?></h3></div>
          <!-- /.box-header -->
          <!-- form start -->
          
        
        <div class="box-body">
        <div class="addmargin10">&nbsp;</div>
          <div class="tab-content">
              <div id="home" class="tab-pane fade in active">
                <h5 class="modal-title"><?php echo $order_id;?></h5>
                  
                                      <div class="modal-body" id="bill_<?php echo $book_id; ?>">
                                        <div class="border rounded p-2">
                                          <div class="row pt-3 px-3">
                                            <div class="col-md-6">
                                              <img src="<?php echo PIQUIC_URL;?>images/logo-piquic-md.png">
                                            </div>

                                            <div class="col-md-6 text-right">
                                              <span class="font-weight-bold mb-1">Order_ID #<?php echo $order_id ?></span><br>
                                              <span class="text-piquic"> <?php echo $create_date; ?> </span>
                                            </div>
                                          </div>

                                          <hr >

                                          <div class="row p-3">
                                            <div class="col-md-5">
                                              <p class="font-weight-bold mb-4">Client Information</p>
                                              <table>
                                                <tbody>
                                                  <tr>
                                                    <td class="pr-3"><span class="font-weight-bold">Name: </span></td>
                                                    <td><?php echo $user_name; ?></td>
                                                  </tr>
                                                  <tr>
                                                    <td class="pr-3"><span class="font-weight-bold">Company Name: </span></td>
                                                    <td><?php echo $user_company; ?></td>
                                                  </tr>
                                                  <tr>
                                                    <td class="pr-3"><span class="font-weight-bold">Address: </span></td>
                                                    <td><?php echo $user_address; ?></td>
                                                  </tr>
                                                  <tr>
                                                    <td class="pr-3"><span class="font-weight-bold">Mobile: </span></td>
                                                    <td><?php echo $user_mobile; ?></td>
                                                  </tr>
                                                </tbody>
                                              </table>
                                            </div>

                                            <div class="col-md-7">

                                              <p class="font-weight-bold mb-4">Booking Details</p>

                                              <div class="row">
                                                <div class="col-md-6">
                                                  <div class="rounded-lg bg-light p-3 mb-3">
                                                    <span class="font-weight-bold">Model: </span><br>
                                                    <?php
                                                    if($m_status=="1"){
                                                      echo "Let \"Piquic Style Expert\" select my models & style." ;
                                                    } else {
                                                      echo "I want to select my models and styling once my apparels gets digitized.";
                                                    } ?>
                                                  </div>
                                                </div>

                                                <div class="col-md-6">
                                                  <div class="rounded-lg bg-light p-3 mb-3">
                                                    <span class="font-weight-bold">Delivery: </span><br>
                                                    <?php
                                                    if($d_status=="1"){
                                                      echo "Standard (6-7 days after receiving your products)." ;
                                                    } else {
                                                      echo "Express (2-3 days after receiving your products).";
                                                    } ?>
                                                  </div>
                                                </div>
                                              </div>
                                              <div class="row">
                                                <div class="col-md-6">
                                                  <div class="rounded-lg bg-light p-3 mb-3">
                                                    <span class="font-weight-bold">Payment Type: </span><br>
                                                    <?php echo $payment_type ?>
                                                  </div>
                                                </div>

                                                <div class="col-md-6">
                                                  <div class="rounded-lg bg-light p-3 mb-3">
                                                    <span class="font-weight-bold">Payment Status: </span><br>
                                                    <?php echo $payment_status ?>
                                                  </div>
                                                </div>
                                              </div>
                                              <div class="row">
                                                <div class="col-md-12">
                                                  <div class="rounded-lg bg-light p-3 mb-3">
                                                    <span class="font-weight-bold">Notes: </span><br>
                                                    <?php if($notes != '') { echo $notes; } else { echo "N/A"; } ?>
                                                  </div>
                                                </div>
                                              </div>
                                            </div>
                                          </div>

                                          <div class="row p-3">
                                            <div class="col-md-12">
                                              <table class="table">
                                                <thead>
                                                  <?php if($plntypname=="Pay as you go"){ ?>
                                                  <tr>
                                                    <th class="border-0 text-uppercase small font-weight-bold">ID</th>
                                                    <th class="border-0 text-uppercase small font-weight-bold">Gender</th>
                                                    <th class="border-0 text-uppercase small font-weight-bold">Category</th>
                                                    <th class="border-0 text-uppercase small font-weight-bold">Quantity</th>
                                                    <th class="border-0 text-uppercase small font-weight-bold">Angle</th>
                                                  </tr>

                                                  <?php } 
                                                  if($plntypname=="Pay Monthly"){ ?>

                                                  <tr>
                                                    <th class="border-0 text-uppercase small font-weight-bold">ID</th>
                                                    <th class="border-0 text-uppercase small font-weight-bold">Plan Name</th>
                                                    <th class="border-0 text-uppercase small font-weight-bold">Duration</th>
                                                    <th class="border-0 text-uppercase small font-weight-bold">Total Image</th>
                                                    <th class="border-0 text-uppercase small font-weight-bold">Total Amount</th>
                                                  </tr>

                                                  <?php } ?>
                                                </thead>

                                                <tbody>
                                                  <?php if($plntypname=="Pay as you go"){ ?>
                                                  <tr>
                                                    <td>1</td>
                                                    <td>
                                                      <?php
                                                      if($gender=="1"){ echo "Male" ; }
                                                      if($gender=="2"){ echo "Female" ; }
                                                      if($gender=="4"){ echo "Boy" ; }
                                                      if($gender=="6"){ echo "Girl" ; }
                                                      ?>
                                                    </td>

                                                    <td >
                                                      <table>
                                                        <?php foreach ($category as $key => $value ) { ?>
                                                        <tr>
                                                          <td style="border-top: none; border-bottom: 1px solid #dee2e6;">
                                                            <?php echo $value; ?>
                                                          </td>
                                                        </tr>
                                                        <?php } ?>
                                                      </table>
                                                    </td>

                                                    <td>
                                                      <table>
                                                        <?php foreach ($quty as $key1 => $value1 ) { ?>
                                                        <tr>
                                                          <td style="border-top: none; border-bottom: 1px solid #dee2e6;">
                                                            <?php echo $value1; ?>
                                                          </td>
                                                        </tr>
                                                        <?php } ?>
                                                      </table>
                                                    </td>

                                                    <td>
                                                      <table>
                                                        <?php
                                                        unset($arr_filter);
                                                        foreach ($angle as $key2 => $value2 ) {
                                                          $angle_value=explode("_",$value2);
                                                          $cat=$angle_value[0];
                                                          $arr_filter[$cat][]=ucfirst($angle_value[1]);
                                                        }

                                                        foreach ($category as $key => $value ) { ?>
                                                        <tr>
                                                          <td style="border-top: none; border-bottom: 1px solid #dee2e6;">
                                                            <?php echo implode(", ",$arr_filter[$value]); ?>
                                                          </td>
                                                        </tr>
                                                        <?php } ?>
                                                      </table>
                                                    </td>
                                                  </tr>

                                                  <?php } if($plntypname=="Pay Monthly"){ ?>
                                                  <tr>
                                                    <td>1</td>
                                                    <td><?php echo $plan_name ?></td>
                                                    <td>1 Month</td>
                                                    <td><?php echo $plan_credit ?></td>
                                                    <td><?php echo $plan_amount ?></td>
                                                  </tr>
                                                  <?php } ?>
                                                </tbody>
                                              </table>
                                            </div>
                                          </div>

                                          <div class="d-flex justify-content-between text-piquic bg-light border">
                                            <div class="py-3 px-5 text-center">
                                              <div class="mb-2">Total Image</div>
                                              <div class="lead"><?php echo $total_image; ?></div>
                                            </div>

                                            <div class="py-3 px-5 text-center">
                                              <div class="mb-2">Plan Type</div>
                                              <div class="lead"><?php echo $plntypname;?></div>
                                            </div>

                                            <div class="py-3 px-5 text-center">
                                              <div class="mb-2">Total amount</div>
                                              <div class="lead">
                                                <?php
                                                if($currency_default=="USD") { echo"&#36;"; }
                                                elseif($currency_default=="INR") { echo"&#x20B9;"; }
                                                elseif($currency_default=="EUR") { echo"&#128;"; }
                                                elseif($currency_default=="GBP") { echo"&#163;"; }
                                                elseif($currency_default=="AUD") { echo"&#36;"; }
                                                elseif($currency_default=="CNY") { echo"&#165;"; }
                                                ?>&nbsp;<?php echo $total_price ?>
                                              </div>
                                            </div>
                                          </div>
                                        </div>
                                      </div>
              
<!--                   
                                <div class="container">
                                  <div class="row">
                                    <div class="col-12">
                                      <div class="card">
                                        <div class="card-body p-3">
                                          <div class="row p-3">
                                            <div class="col-md-6">
                                              <img src="<?php echo PIQUIC_URL;?>images/logo-piquic-md.png">
                                            </div>

                                            <div class="col-md-6 text-right">
                                              <p class="font-weight-bold mb-1">Order_ID #<?php echo $order_id ?></p>
                                              <p class="text-piquic"> <?php echo $create_date; ?> </p>
                                            </div>
                                          </div>

                                          <hr >

                                          <div class="row p-3">
                                            <div class="col-md-6">
                                              <p class="font-weight-bold mb-4">Client Information</p>
                                              <p class="mb-1"><span class=" text-piquic">Name: </span>`<?php echo $user_name; ?></p>
                                              <p> <span class=" text-piquic">Company Name:</span><?php echo $user_company; ?></p>
                                              <p class="mb-1"> <span class=" text-piquic">Address: </span><?php echo $user_address; ?></p>
                                              <p class="mb-1"><span class=" text-piquic">Mobile:</span> <?php echo $user_mobile; ?></p>
                                            </div>

                                            <div class="col-md-6 text-right">
                                              <p class="font-weight-bold mb-4">Booking Details</p>
                                              <p class="mb-1"><span class="text-piquic">Model: </span> <?php 
                                              if($m_status=="1"){
                                               echo "Let \"Piquic Style Expert\" select my models & style." ; 
                                             } 
                                             else{ 
                                               echo "I want to select my models and styling once my apparels gets digitized."; 
                                             } 
                                             ?> </p>
                                             <p class="mb-1"><span class="text-piquic">Delivery : </span> <?php 
                                             if($d_status=="1"){
                                               echo "Standard (6-7 days after receiving your products)." ; 
                                             } 
                                             else{ 
                                               echo "Express (2-3 days after receiving your products)."; 
                                             } 
                                             ?> </p>
                                             <p class="mb-1"><span class="text-piquic">Payment Status: </span> <?php echo $payment_status ?></p>
                                             <p class="mb-1"><span class="text-piquic">Notes: </span> <?php echo $notes ?></p>
                                           </div>
                                         </div>

                                         <div class="row p-3">
                                          <div class="col-md-12">
                                            <table class="table">
                                              <thead>
                                               <?php if($plntypname=="Pay as you go"){
                                                ?>
                                                <tr>
                                                  <th class="border-0 text-uppercase small font-weight-bold">ID</th>
                                                  <th class="border-0 text-uppercase small font-weight-bold">Gender</th>
                                                  <th class="border-0 text-uppercase small font-weight-bold">Category</th>
                                                  <th class="border-0 text-uppercase small font-weight-bold">Quantity</th>
                                                  <th class="border-0 text-uppercase small font-weight-bold">Angle</th>

                                                </tr>
                                                <?php
                                              }
                                              if($plntypname=="Pay Monthly"){
                                                ?>
                                                <tr>
                                                  <th class="border-0 text-uppercase small font-weight-bold">ID</th>
                                                  <th class="border-0 text-uppercase small font-weight-bold">Plan Name</th>
                                                  <th class="border-0 text-uppercase small font-weight-bold">Duatation</th>
                                                  <th class="border-0 text-uppercase small font-weight-bold">Total Image</th>
                                                  <th class="border-0 text-uppercase small font-weight-bold">Total Amount</th>
                                                </tr>
                                                <?php
                                              }
                                              ?>

                                            </thead>
                                            <tbody>
                                             <?php if($plntypname=="Pay as you go"){
                                              ?>

                                              <tr>
                                                <td>1</td>
                                                <td><?php 
                                                if($gender=="1"){ echo "Male" ; } 
                                                if($gender=="2"){ echo "Female" ; } 
                                                if($gender=="4"){ echo "Boy" ; } 
                                                if($gender=="6"){ echo "Girl" ; } 
                                                ?>
                                              </td>

                                              <td >
                                               <table>

                                                 <?php foreach ($category as $key => $value ) {
                                                  ?><tr>
                                                    <td style="border-top: none; border-bottom: 1px solid #dee2e6;">
                                                     <?php echo $value; ?>
                                                   </td>
                                                 </tr>
                                                 <?php
                                               } ?>

                                             </table>
                                           </td>
                                           <td><table>

                                             <?php foreach ($quty as $key1 => $value1 ) {
                                              ?><tr>
                                                <td style="border-top: none; border-bottom: 1px solid #dee2e6;">
                                                 <?php echo $value1; ?>
                                               </td>
                                             </tr>
                                             <?php
                                           } ?>

                                         </table></td>
                                         <td><table>

                                           <?php 

                                        //echo "<pre>";
                                        //print_r($angle);
                                           unset($arr_filter);


                                           foreach ($angle as $key2 => $value2 ) {
                                          //echo $value2;
                                            $angle_value=explode("_",$value2);
                                            $cat=$angle_value[0];
                                            $arr_filter[$cat][]=ucfirst($angle_value[1]);
                                          }
                                        //echo "<pre>";
                                        //print_r($arr_filter);

                                          foreach ($category as $key => $value ) {
                                            ?><tr>
                                              <td style="border-top: none; border-bottom: 1px solid #dee2e6;">
                                               <?php echo implode(", ",$arr_filter[$value]); ?>
                                             </td>
                                           </tr>
                                           <?php
                                         } ?>

                                       </table></td>

                                     </tr>
                                     <?php
                                   }
                                   if($plntypname=="Pay Monthly"){
                                    ?>
                                    <tr>
                                      <td>1</td>
                                      <td><?php echo $plan_name ?></td>
                                      <td>1 Month</td>
                                      <td><?php echo $plan_credit ?></td>
                                      <td><?php echo $plan_amount ?></td>

                                    </tr>
                                    <?php
                                  }
                                  ?>


                                </tbody>
                              </table>
                            </div>
                          </div>

                          <div class="d-flex flex-row-reverse bg-dark text-white">
                            <div class="py-3 px-5 text-right">
                              <div class="mb-2">Total Image</div>
                              <div class="h4 font-weight-light"><?php echo $total_image; ?></div>
                            </div>

                            <div class="py-3 px-5 text-right">
                              <div class="mb-2">Plan Type</div>
                              <div class="h4 font-weight-light"><?php echo $plntypname;?></div>
                            </div>

                            <div class="py-3 px-5 text-right">
                              <div class="mb-2">Total amount</div>
                              <div class="h4 font-weight-light"><?php
                              if($currency_default=="USD") { echo"&#36;"; }
                              elseif($currency_default=="INR") { echo"&#x20B9;"; }
                              elseif($currency_default=="EUR") { echo"&#128;"; }
                              elseif($currency_default=="GBP") { echo"&#163;"; }
                              elseif($currency_default=="AUD") { echo"&#36;"; }
                              elseif($currency_default=="CNY") { echo"&#165;"; }
                              ?>&nbsp;<?php echo $total_price ?></div>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
 -->
             

              </div>
            </div>
        </div>
          
      </div>
    </div>
    
  </section>
</div>

<?php $this->load->view('admin/includes/globaljs');?>
<link type="text/css" href="<?php echo base_url();?>assets/form-select2/select2.css" rel="stylesheet"/>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootswatch/3.3.7/lumen/bootstrap.min.css">



<script type="text/javascript" src="<?php echo base_url();?>assets/form-select2/select2.min.js"></script>



<!--<script  type="text/javascript" src="<?php echo base_url();?>assets/dist/password-score.js"></script>
<script  type="text/javascript" src="<?php echo base_url();?>assets/dist/password-score-options.js"></script>
<script  type="text/javascript" src="<?php echo base_url();?>assets/dist/bootstrap-strength-meter.js"></script>-->

 <script>
  function getCurrencySymbol()
  {
    var order_currency=$('#order_currency').val();
    //alert(order_currency);
    if(order_currency=='INR')
    {
      var order_currency_symbol='&#x20B9;';
    }
    else if(order_currency=='USD')
    {
      var order_currency_symbol='&#36;';
    }
    else if(order_currency=='EUR')
    {
      var order_currency_symbol='&#128;';
    }
    else if(order_currency=='GBP')
    {
      var order_currency_symbol='&#163;';
    }
    else if(order_currency=='AUD')
    {
      var order_currency_symbol='&#36;';
    }
    else if(order_currency=='CNY')
    {
      var order_currency_symbol='&#165;';
    }

    else
    {
      var order_currency_symbol='';
    }
    var order_currency_symbol='';

    $('#order_currency_symbol').val(order_currency_symbol);
  }
    
    
  jQuery(function ($) {
     // $('.content-wrapper').css('min-height','900px');
     //$('#entity_id').select2();
    
    
    $('#order_form').validate({
    
           rules: {
    
          
    
      plntyid: {
      required: true,
      },
      order_currency: {
      required: true,
      },
      order_per_image: {
      required: true,
      },
      /*order_currency_symbol: {
      required: true,
      },*/
      order_name: {
      required: true,
      },
      order_amount: {
      required: true,
      },
      order_credit: {
      required: true,
      },
      
      status: {
      required: true,
      }

      
    },
     messages: {
     
      
      
      
        },
        submitHandler: function(form) {
    
           //alert("koko");
    
            var orderid=$('#orderid').val();
        
          var plntyid=$('#plntyid').val();
        var order_currency=$('#order_currency').val();
        var order_currency_symbol=$('#order_currency_symbol').val();
        var order_per_image=$('#order_per_image').val();
          var order_name=$('#order_name').val();
          var order_amount=$('#order_amount').val();
          var order_credit=$('#order_credit').val();
          var status=$('#status').val();
        
        //alert("<?php echo base_url();?>user/addUser?&orderid="+orderid+"&UserName="+UserName+"&UserComments="+UserComments+"&UserIsActive="+UserIsActive);  
          
        $.ajax({
        method:'post',
        url: "<?php echo base_url();?>admin/order/insert_order", 
        data:"&orderid="+orderid+"&plntyid="+plntyid+"&order_currency="+order_currency+"&order_currency_symbol="+order_currency_symbol+"&order_per_image="+order_per_image+"&order_name="+order_name+"&order_amount="+order_amount+"&order_credit="+order_credit+"&status="+status,
        success: function(result){
        if(result)
        {
          //alert(result);

          //alert("koko");

          var val=$.trim(result);
          //alert(val);

          $("#success_message").show();
          setTimeout(function(){setInterval(function(){
          //alert("ppp");
          parent.$.fancybox.close();

          }, 3000)}, 3000);
          parent.$.fancybox.close();
          parent.$('#example1').dataTable().fnStandingRedraw();
          
          
          
          
        
          
          
         
        
        }
        }
        });
          
          
          
          
          
            
              
          
             
    
    } 
     
    
    });
    

    
    
  }); 
</script>   
<script>
 $(document).ready(function() {
 //alert("popo");
    /* $('#example-getting-started-input').strengthMeter('text', {
            container: $('#example-getting-started-text')
        });
        $('#example-tooltip').strengthMeter('tooltip');*/
    /*$('#user_passowrd').strengthMeter('progressBar', {
            container: $('#example-progress-bar-container')
        });*/
    });
</script>
<script type="text/javascript" src="<?php echo base_url();?>theme/assets/source/jquery.fancybox.js?v=2.1.5"></script>
<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>theme/assets/source/jquery.fancybox.css?v=2.1.5" media="screen" />       