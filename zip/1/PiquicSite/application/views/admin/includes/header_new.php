
<?php 
	/*$ip=$_SERVER['REMOTE_ADDR'];
	$details = @str_replace("a:14:","",file_get_contents("http://ip-api.com/php/{$ip}"));
	if(strstr($details,'"timezone";'))
	{
	$arr1 = @explode('"timezone";',$details);
	$arr2 = @explode('";',$arr1[1]); // returns "d"
	$arr3=@explode(':"',$arr2[0]);
	$tzone=$arr3[1];
	}
	else
	{
	$tzone='Asia/Kolkata';
	}
 date_default_timezone_set($tzone);*/
	
	$session_data = $this->session->userdata('logged_in');
	$username = $session_data['username'];
	$firstname = $session_data['firstname'];
	$rolecode = $session_data['rolecode'];
	$color=	$session_data['color'];
	$logo=$session_data['logo']; 
?>
<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title>My Booking Solution</title>
    <link rel="shortcut icon" type="image/png" href="<?php echo base_url(); ?>/assets/img/favicon.png"/>
	<meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
	
	<?php include('globalcss_new.php');?> 

</head>
<body class="hold-transition skin-blue sidebar-mini">
	<div class="wrapper">
		<header class="main-header">
			<!-- Logo -->
			<a href="<?php echo base_url(); ?>" class="logo">
			<!-- mini logo for sidebar mini 50x50 pixels -->
			<span class="logo-mini"><b></b>MBS</span>
			<!-- logo for regular state and mobile devices -->
			<span class="logo-lg"><b>
			<?php if ($rolecode==1)
			{
			?>
			<img src="<?php echo base_url().'assets/img/logo-1.png'; ?>" alt="logo">
			<?php
			}
			else
			{
			 $path_logo='uploadlogo/'.$logo;
			?>
			<img src="<?php if(!empty($logo) && file_exists($path_logo)) { echo base_url().'uploadlogo/'.$logo;}else{ echo base_url().'assets/img/logo-1.png';} ?>" alt="logo">
			<?php
			}
			?>
			
			</b></span>
			</a>
			<!-- Header Navbar: style can be found in header.less -->
			<nav class="navbar navbar-static-top">
				<!-- Sidebar toggle button-->
				
				<div class="navbar-custom-menu">
					<ul class="nav navbar-nav">
						<!-- Messages: style can be found in dropdown.less-->
						<!-- User Account: style can be found in dropdown.less -->
						
						<!-- Control Sidebar Toggle Button -->
					</ul>
				</div>
                			
			</nav>

		</header>
<!-- body and div wrapper close in footer-->
