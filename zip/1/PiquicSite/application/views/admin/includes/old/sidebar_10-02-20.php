<!-- Last Edited: KW 25/09/16 -->
<?php
	$session_data = $this->session->userdata('logged_in');
	$rolecode = $session_data['rolecode'];
	$usertype= $session_data['rolecode'];
	$roles= $session_data['roles'];
	//$businesstype = $session_data['businesstype'];
	
	$role=trim($roles,',');
	if(strstr($role,","))
	{
	$role=explode(',',$role);
	}
	else
	{
	$role=array($role);
	}
	
							
							
		//echo "<pre>";
		//print_r($role); exit;				
	
?>

<aside class="main-sidebar">
	<section class="sidebar">
		<div class="user-panel">
			<div class="pull-left info"></div>
		</div>
   
		
		<ul class="sidebar-menu">
		<?php //echo $this->router->fetch_class()."----".$this->router->fetch_method();?>
			

				
			<li class="treeview <?php if($this->router->fetch_class()=='User' && ($this->router->fetch_method()=='index' || $this->router->fetch_method()=='index' )) { ?> active <?php } ?> ">
				<a href="<?php echo base_url('administrator/manage-user'); ?>">
					<i class="fa fa-user"></i> <span>User Management
					
				</span></a>
			</li>
					
					

					
			<li class="treeview <?php if($this->router->fetch_class()=='User' && ($this->router->fetch_method()=='index' || $this->router->fetch_method()=='index' )) { ?> active <?php } ?> ">
				<a href="<?php echo base_url('administrator/manage-page-content'); ?>">
					<i class="fa fa-user"></i> <span>Page Content Management
					
				</span></a>
			</li>
					
			
			
			<li class="treeview <?php if($this->router->fetch_class()=='User' && ($this->router->fetch_method()=='index' || $this->router->fetch_method()=='index' )) { ?> active <?php } ?> ">
				<a href="<?php echo base_url('administrator/image_size'); ?>">
					<i class="far fa-file"></i> <span>Image Size Management
					
				</span></a>
			</li>
			
			
			<li class="treeview <?php if($this->router->fetch_class()=='Paymenttype' && ($this->router->fetch_method()=='index' || $this->router->fetch_method()=='index' )) { ?> active <?php } ?> ">
				<a href="<?php echo base_url('administrator/manage-paymenttype'); ?>">
					<i class="fa fa-user"></i> <span>Payment Type Management
					
				</span></a>
			</li>
			

			<li class="treeview <?php if($this->router->fetch_class()=='Plantype' && ($this->router->fetch_method()=='index' || $this->router->fetch_method()=='index' )) { ?> active <?php } ?> ">
				<a href="<?php echo base_url('administrator/manage-plantype'); ?>">
					<i class="fa fa-user"></i> <span>Plan Type Management
					
				</span></a>
			</li>
			


			<li class="treeview <?php if($this->router->fetch_class()=='Plan' && ($this->router->fetch_method()=='index' || $this->router->fetch_method()=='index' )) { ?> active <?php } ?> ">
				<a href="<?php echo base_url('administrator/manage-plan'); ?>">
					<i class="fa fa-user"></i> <span>Plan Management
					
				</span></a>
			</li>

			<li class="treeview <?php if($this->router->fetch_class()=='Plan' && ($this->router->fetch_method()=='index' || $this->router->fetch_method()=='index' )) { ?> active <?php } ?> ">
				<a href="<?php echo base_url('administrator/manage-setting'); ?>">
					<i class="fa fa-user"></i> <span>Setting Management
					
				</span></a>
			</li>
			

			<li class="treeview <?php if($this->router->fetch_class()=='Order_upload' && ($this->router->fetch_method()=='index' || $this->router->fetch_method()=='index' )) { ?> active <?php } ?> ">
				<a href="<?php echo base_url('administrator/manage-order-upload'); ?>">
					<i class="fa fa-user"></i> <span>Order Management (Upload)

					</span></a>
				</li>

				<li class="treeview <?php if($this->router->fetch_class()=='Order' && ($this->router->fetch_method()=='index' || $this->router->fetch_method()=='index' )) { ?> active <?php } ?> ">
					<a href="<?php echo base_url('administrator/manage-order'); ?>">
						<i class="fa fa-user"></i> <span>Order Management (Book)

						</span></a>
					</li>
			
		</ul>
	
	</section>
</aside>
