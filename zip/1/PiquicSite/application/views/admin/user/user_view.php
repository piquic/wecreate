<?php  $this->load->view('admin/includes/globalcss');?>


<style>
/*.table-bordered {
    border: 1px solid #666666;
}*/
.showdbutton {
    cursor:pointer; 
	background-color:#666666; 
	margin:10px 0px; 
	padding:10px; 
	color:#FFFFFF;
	width:100%;
}

.disabledbutton {
    pointer-events: none;
    opacity: 0.9;
	
	cursor:pointer; 
	background-color:#999999; 
	margin:10px 0px; 
	padding:10px; 
	color:#FFFFFF;
	width:100%;
}
label.error{
 color:#FF0000;
 font-size:12px;
}
.only_view_css{
pointer-events: none;
border: none;
border-color: transparent;
border-style : hidden;

}
</style>
<div>
  <!-- Section content header -->
 
  <!-- Section content -->
    <section class="content">
		<div class="col-md-12">
		  <!-- start Information Box -->
			<div class="box">
			<!-- box header style start-->
				<div class="col-md-12 box-header with-border" ><h3 class="box-title"><?php if($user_id==''){ echo $lang['Create User']; }else {echo $lang['View User']; }?></h3></div>
					<!-- /.box-header -->
					<!-- form start -->
					
					<div id="success_message" style="display:none;">
					<div class="alert alert-success">
					<strong>Updated successfully!</strong>.
					</div>
				    </div>
					
				
				<div class="box-body">
				
				
				   <div class="col-md-12">
						<ul class="nav nav-tabs">
							<li id="li_home" class="active"><a data-toggle="tab" href="#home">User Information</a></li>
							<?php
							//if($rolecode=='1' || $rolecode=='2')
							if($user_id!='' )
							{
							?>
							<!--<li id="li_menu1"  ><a data-toggle="tab" href="#menu1">Change Password</a></li>-->
							<?php
							}
							?>
							
						</ul>
					</div>
				
				
				<div class="addmargin10">&nbsp;</div>
					<div class="tab-content">
					
				     <div id="home" class="tab-pane fade in active">
					 
					 		    <?php  $array = array('id'=>'User_form', 'role'=>'form', 'class'=>'form form-horizontal has-validation-callback');
				    echo form_open_multipart('User/insert_user', $array); ?>
					
					
					<input type='hidden' name='user_id' value='<?php echo $user_id;?>' id='user_id'>
					
					<div class="form-group">
					    <label for="inputEmail3"  class="col-sm-2 control-label"><?php echo $lang['user_name'];?></label>
						<div class="col-sm-8">
						    <input  type='text'  class="form-control only_view_css" id="user_name" name="user_name" value='<?php echo $user_name;?>'>
                            <?php echo form_error('user_name', '<div class="error" style="color:red;">', '</div>'); ?>
						</div>
						
					</div>
					
					
					<div class="form-group">
					    <label for="inputEmail3"  class="col-sm-2 control-label"><?php echo $lang['user_email'];?></label>
						<div class="col-sm-8">
						    <input  type='text'  class="form-control only_view_css" id="user_email" name="user_email" value='<?php echo $user_email;?>'>
                            <?php echo form_error('user_email', '<div class="error" style="color:red;">', '</div>'); ?>
						</div>
						
					</div>
					
					 <?php
				if($user_id=='')
				{
				?>
					
					<div class="form-group">
					    <label for="inputEmail3"  class="col-sm-2 control-label"><?php echo $lang['user_password'];?></label>
						<div class="col-sm-8">
						    <input  type='text'  class="form-control only_view_css" id="user_password" name="user_password" value='<?php echo $user_password;?>'>
                            <?php echo form_error('user_password', '<div class="error" style="color:red;">', '</div>'); ?>
							
						</div>
						
					</div>
					
					
					
					<!--<div class="form-group">
					<label class="col-sm-2 control-label">Password Strength</label>
					<div class="col-sm-4" id="example-progress-bar-container">
					
					</div>
					</div>-->
					
					
					
					
					
					
					<div class="form-group">
					    <label for="inputEmail3"  class="col-sm-2 control-label"><?php echo $lang['user_cpassword'];?></label>
						<div class="col-sm-8">
						    <input  type='text'  class="form-control only_view_css" id="user_cpassword" name="user_cpassword" value=''>
                            <?php echo form_error('user_cpassword', '<div class="error" style="color:red;">', '</div>'); ?>
						</div>
						
					</div>
					
					 <?php
				}
				?>
				
				<div class="form-group">
					    <label for="inputEmail3"  class="col-sm-2 control-label"><?php echo $lang['user_mobile'];?></label>
						<div class="col-sm-8">
						    <input  type='text'  class="form-control only_view_css" id="user_mobile" name="user_mobile" value='<?php echo $user_mobile;?>'>
                            <?php echo form_error('user_mobile', '<div class="error" style="color:red;">', '</div>'); ?>
						</div>
						
					</div>
					
					<div class="form-group">
					    <label for="inputEmail3"  class="col-sm-2 control-label"><?php echo $lang['user_telephone'];?></label>
						<div class="col-sm-8">
						    <input  type='text'  class="form-control only_view_css" id="user_telephone" name="user_telephone" value='<?php echo $user_telephone;?>'>
                            <?php echo form_error('user_telephone', '<div class="error" style="color:red;">', '</div>'); ?>
						</div>
						
					</div>
					
					
			
					
					
					
					<div class="form-group   ">
								<label for="inputEmail3"  class="col-sm-2 control-label"><?php echo $lang['user_address'];?></label>
								<div class="col-sm-8">
								<textarea  class="form-control only_view_css" id="user_address" name="user_address"><?php echo $user_address; ?></textarea>
									
									<?php echo form_error('user_address', '<div class="error" style="color:red;">', '</div>'); ?>
								</div>
								
								
							
							
								
							</div>
					
					
					
					
					
					
					
					
			
					
					
					<div class="form-group">
					<label for="inputEmail3"  class="col-sm-2 control-label"><?php echo $lang['user_status'];?></label>
					<div class="col-sm-8">
				
							
								<select name="user_status" id="user_status" class="form-control only_view_css" >
								 <!--<option value=''>Select</option>-->
								 <option value="Enable" <?php if($user_status=='Enable') { ?> selected="selected" <?php } ?>  >Enable</option>
								 <option value="Disable" <?php if($user_status=='Disable') { ?> selected="selected" <?php } ?>   >Disable</option>
								 </select>
					
					</div>
					</div>
					
					
					
					
					
					
					
					
					
					
					
					
					
					
					
					
					
					
					
				
					
				
					
				
					
					
					
					<!-- Contractor type fields end-->
					<!--<div class="row">
					<?php if($user_id!=''){
						$button_name='Update';
					}else {
						$button_name='Save';
					} ?>
						<div class="col-md-6"><input class="btn btn-success pull-right" type="submit" value="<?php echo $button_name;?>"></div>	   
						<div class="col-md-6"><input class="btn btn-danger pull-left" type="reset"></div>
					</div>-->
					
					
					
					
					
					
					
					
					
					
					
					
					
					
					
					<?php echo form_close();?> 
					 
					 
					 
				    </div>
					
					
					
					
					<!--Tab Password Change-->
						<div id="menu1" class="tab-pane fade">
						<!--<div id="" class=""></div>-->
							<!-- form start -->
							
                        <?php 
				$array = array('id'=>'User_password_form', 'role'=>'form', 'class'=>'form form-horizontal has-validation-callback');
				echo form_open_multipart('admin/User/update_change_password', $array); ?>
                
                <input type="hidden" class="form-control only_view_css" id="user_id" name="user_id" value="<?php echo $user_id ?>" >
                
						<div class="form-group">
							<label for="inputEmail3"  class="col-sm-2 control-label">Password</label>
							<div class="col-sm-8">
								<input type="password" class="form-control only_view_css" id="user_change_password" name="user_change_password">
								<?php echo form_error('user_change_password', '<div class="error" style="color:red;">', '</div>'); ?>
							</div>
						</div>
						<div class="form-group">
							<label for="inputEmail3"  class="col-sm-2 control-label">Confirm Password</label>
							<div class="col-sm-8">
								<input type="password" class="form-control only_view_css"  id="user_change_cpassword" name="user_change_cpassword">
								<?php echo form_error('user_change_cpassword', '<div class="error" style="color:red;">', '</div>'); ?>
							</div>
						</div>
						
						<div class="addmargin10">&nbsp;</div>
						
						<div class="row">
						<?php if($user_id!=''){
						$button_name='Update';
						}else {
						$button_name='Save';
						} ?>
						<div class="col-md-6"><input class="btn btn-success pull-right" type="submit" value="<?php echo $button_name;?>"></div>	   
						<div class="col-md-6"><input class="btn btn-danger pull-left" type="reset"></div>
						</div>
					
					
					
						<?php echo form_close(); ?>	
						</div>	
					
					
				    </div>
					
					
					
					
				
		
					
					
					
				</div>
					
			</div>
		</div>
		
	</section>
</div>

<?php $this->load->view('admin/includes/globaljs');?>
<link type="text/css" href="<?php echo base_url();?>assets/form-select2/select2.css" rel="stylesheet"/>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootswatch/3.3.7/lumen/bootstrap.min.css">



<script type="text/javascript" src="<?php echo base_url();?>assets/form-select2/select2.min.js"></script>



<!--<script  type="text/javascript" src="<?php echo base_url();?>assets/dist/password-score.js"></script>
<script  type="text/javascript" src="<?php echo base_url();?>assets/dist/password-score-options.js"></script>
<script  type="text/javascript" src="<?php echo base_url();?>assets/dist/bootstrap-strength-meter.js"></script>-->

 <script>
    
    
	jQuery(function ($) {
	   // $('.content-wrapper').css('min-height','900px');
		
		
		
	
		
		
		/*$('#user_password').strengthMeter('progressBar', {
            container: $('#example-progress-bar-container')
        });*/
		
		
		
	    /*$.validator.addMethod("regex", function(value, element, regexpr) {          
		 return regexpr.test(value);
		}, "Please enter a valid Mobile Number.");*/
		
		
		
		
		
		
		
		
		
		//alert("popo");
		
		
		
		
		
		$('#User_form').validate({
		
           rules: {
            user_name:{
				required:true
			},
			user_email:{
				required:true,
				email: true,
				<?php
				if($user_id=="")
				{
				?>
				remote: {
				url: "<?php echo base_url();?>admin/user/checkexistemail",
				type: "post"
				},
				<?php
				}
				?>	 
				 
			 },
			
			
			<?php
			if($user_id=='')
			{
			?>
			user_password:{
				required:true
			},
			user_cpassword: {
				required:true,
                equalTo: "#user_password"             
            },
			<?php
			}
			?>
		   user_mobile: {
		   required: true,
		   digits: true,
		  /* maxlength: 10,*/
		    <?php
		   if($user_id=="")
		   {
		   ?>
		    remote: {
                    url: "<?php echo base_url();?>admin/user/checkexistmobile",
                    type: "post"
                   },
				 
		   <?php
		   }
		   ?>		 
				 
		},
		
		
			user_status: {
                required: true,
			}
			
			
		},
		 messages: {
		 
		    user_email:{
		      remote:"This email already exists"
		     },
			user_mobile: {
			digits: "This field can only contain numbers.",
			/*maxlength: "this must be 10 digit number.",*/
			remote:"This mobile already exists",
			
			},
			
			 /*user_mobile:{
		      remote:"This email already exists"
		     }*/
                 <?php
				if($user_id=='')
				{
				?>
                user_cpassword: {
            	equalTo:"password not match",
			     },
			    <?php
				}
				?>   
		  
			
        },
				submitHandler: function(form) {
		
		       //alert("koko");
		
		        var user_id=$('#user_id').val();
			    var user_name=$('#user_name').val();
			    var user_address=$('#user_address').val();
				var user_email=$('#user_email').val();
				var user_telephone=$('#user_telephone').val();
				var user_mobile=$('#user_mobile').val();
				var user_password=$('#user_password').val();
				var user_group=$('#user_group').val();
				var user_status=$('#user_status').val();
				
				
				
				
					
					
				//alert("<?php echo base_url();?>user/addUser?&user_id="+user_id+"&UserName="+UserName+"&UserComments="+UserComments+"&UserIsActive="+UserIsActive);	
					
				$.ajax({
				method:'post',
				url: "<?php echo base_url();?>admin/user/insert_user", 
				data:"&user_id="+user_id+"&user_name="+user_name+"&user_address="+user_address+"&user_email="+user_email+"&user_telephone="+user_telephone+"&user_mobile="+user_mobile+"&user_password="+user_password+"&user_group="+user_group+"&user_status="+user_status,
				success: function(result){
				if(result)
				{
				    //alert(result);
					
					//alert("koko");
					
					var val=$.trim(result);
					//alert(val);
					
					$("#success_message").show();
				  setTimeout(function(){setInterval(function(){
				  //alert("ppp");
				  parent.$.fancybox.close();
				  
				  }, 3000)}, 3000);
				  
				  
					
					parent.$.fancybox.close();
					
					//alert("fofo");
					//alert("koko");
					
					parent.$('#example1').dataTable().fnStandingRedraw();
					
					
				  
					
				
				  
				  
				 
				
				}
				}
				});
					
					
					
					
					
						
							
					
					   
		
		} 
		 
		
		});
		
		
		
	$('#User_password_form').validate({
        rules: {
			user_change_password:{
				
				required:true
			},
			
			 user_change_cpassword: {
				required:true,
                equalTo: "#user_change_password"              
            }
			
        },
		 messages: {
          
			user_change_cpassword: {
            	equalTo:"password not match"
			}
			
        },
		submitHandler: function(form) {
		        var user_id=$('#user_id').val();
				var user_password=$('#user_change_password').val();
				
				
				
				
					
					
				//alert("<?php echo base_url();?>user/addUser?&user_id="+user_id+"&UserName="+UserName+"&UserComments="+UserComments+"&UserIsActive="+UserIsActive);	
					
				$.ajax({
				method:'post',
				url: "<?php echo base_url();?>admin/user/update_change_password", 
				data:"&user_id="+user_id+"&user_password="+user_password,
				success: function(result){
				if(result)
				{
				// alert(result);
					
					
					
					var val=$.trim(result);
					
					
					$("#success_message").show();
				  setTimeout(function(){setInterval(function(){
				  //alert("ppp");
				  parent.$.fancybox.close();
				  
				  }, 3000)}, 3000);
					
					//alert(val);
					parent.$.fancybox.close();
					parent.$('#example1').dataTable().fnStandingRedraw();
					
				
				  
				  
				 
				
				}
				}
				});
				
				
				
		 },
    });
	
		
		
		
		
	});	
</script>   
<script>
 $(document).ready(function() {
 //alert("popo");
	  /* $('#example-getting-started-input').strengthMeter('text', {
            container: $('#example-getting-started-text')
        });
        $('#example-tooltip').strengthMeter('tooltip');*/
		/*$('#user_passowrd').strengthMeter('progressBar', {
            container: $('#example-progress-bar-container')
        });*/
    });
</script>
<script type="text/javascript" src="<?php echo base_url();?>theme/assets/source/jquery.fancybox.js?v=2.1.5"></script>
<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>theme/assets/source/jquery.fancybox.css?v=2.1.5" media="screen" />				