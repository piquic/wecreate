<?php  $this->load->view('admin/includes/globalcss');?>


<style>
/*.table-bordered {
    border: 1px solid #666666;
}*/
.showdbutton {
    cursor:pointer; 
	background-color:#666666; 
	margin:10px 0px; 
	padding:10px; 
	color:#FFFFFF;
	width:100%;
}

.disabledbutton {
    pointer-events: none;
    opacity: 0.9;
	
	cursor:pointer; 
	background-color:#999999; 
	margin:10px 0px; 
	padding:10px; 
	color:#FFFFFF;
	width:100%;
}
label.error{
 color:#FF0000;
 font-size:12px;
}
</style>
<div>
  <!-- Section content header -->
 
  <!-- Section content -->
    <section class="content">
		<div class="col-md-12">
		  <!-- start Information Box -->
			<div class="box">
			<!-- box header style start-->
				<div class="col-md-12 box-header with-border" ><h3 class="box-title"><?php if($planid==''){ echo 'Create Plan '; }else {echo 'Update Plan '; }?></h3></div>
					<!-- /.box-header -->
					<!-- form start -->
					
					<div id="success_message" style="display:none;">
					<div class="alert alert-success">
					<strong>Updated successfully!</strong>.
					</div>
				    </div>
					
				
				<div class="box-body">
				
				
				   <div class="col-md-12">
						<ul class="nav nav-tabs">
							<li id="li_home" class="active"><a data-toggle="tab" href="#home">Plan Information</a></li>
							
							
						</ul>
					</div>
				
				
				<div class="addmargin10">&nbsp;</div>
					<div class="tab-content">
					
				     <div id="home" class="tab-pane fade in active">
					 
					 		    <?php  $array = array('id'=>'plan_form', 'role'=>'form', 'class'=>'form form-horizontal has-validation-callback');
				    echo form_open_multipart('plan/insert_plan', $array); ?>
					
					
					<input type='hidden' name='planid' value='<?php echo $planid;?>' id='planid'>
					
					
					
	              <div class="form-group">
					    <label for="inputEmail3"  class="col-sm-2 control-label"><?php echo 'Plan Type Name';?>
                        <span style="color:#F00">*</span>
                        </label>
						<div class="col-sm-8">
						    <!-- <input  type='text'  class="form-control" id="plntyid" name="plntyid" value='<?php echo $plntyid;?>'> -->


							<select name="plntyid" id="plntyid" class="form-control" >
							<option value=''>Select</option>
							<?php
							$plantype_details=$this->db->query("select * from tbl_plantype where deleted='0' and status='0' ")->result_array();
							if(!empty($plantype_details))
							{
							foreach($plantype_details as $key => $plantype)
							{
								$plntyid=$plantype['plntyid'];
								$plntypname=$plantype['plntypname'];
							?>
							<option value="<?php echo $plntyid;?>" <?php if($plntyid_sel==$plntyid) { ?> selected="selected" <?php } ?>  ><?php echo $plntypname;?></option>
							<?php

							}
							}
							?>

						



							</select>




                            <?php echo form_error('plntyid', '<div class="error" style="color:red;">', '</div>'); ?>
						</div>
						
					</div>



					<div class="form-group">
					<label for="inputEmail3"  class="col-sm-2 control-label"><?php echo 'Plan Currency';?>
					<span style="color:#F00">*</span>
					</label>
					<div class="col-sm-8">
					<select id="plan_currency" name="plan_currency" class="form-control" required="" onchange="getCurrencySymbol();">
					<option value="" >Select</option>
					<option value="INR" <?php if($plan_currency=='INR') { ?> selected <?php } ?>>INR (&#x20B9;)</option>
					<option value="USD" <?php if($plan_currency=='USD')  { ?> selected <?php } ?>>USD (&#36;)</option>
					<option value="EUR" <?php if($plan_currency=='EUR') {  ?> selected <?php } ?>>EUR (&#128;)</option>
					<option value="GBP" <?php if($plan_currency=='GBP') {  ?> selected <?php } ?>>GBP (&#163;)</option>
					<option value="AUD" <?php if($plan_currency=='AUD') {    ?> selected <?php } ?>>AUD (&#36;)</option>
					<option value="CNY" <?php if($plan_currency=='CNY') {  ?> selected <?php } ?>>CNY (&#165;)</option>
					</select>
					<?php echo form_error('plan_currency', '<div class="error" style="color:red;">', '</div>'); ?>
					<input  type='hidden'  class="form-control" id="plan_currency_symbol" name="plan_currency_symbol" value='<?php echo $plan_currency_symbol;?>' readonly>
					</div>

					</div>



					




						<div class="form-group">
					    <label for="inputEmail3"  class="col-sm-2 control-label"><?php echo 'Plan Name';?>
                        <span style="color:#F00">*</span>
                        </label>
						<div class="col-sm-8">
						    <input  type='text'  class="form-control" id="plan_name" name="plan_name" value='<?php echo $plan_name;?>'>
                            <?php echo form_error('plan_name', '<div class="error" style="color:red;">', '</div>'); ?>
						</div>
						
					</div>
					
					
					
					
					
					
					<div class="form-group">
					    <label for="inputEmail3"  class="col-sm-2 control-label"><?php echo 'Plan Amount ';?>
                        <span style="color:#F00">*</span>
                        </label>
						<div class="col-sm-8">
						    <input  type='text'  class="form-control" id="plan_amount" name="plan_amount" value='<?php echo $plan_amount;?>'>
                            <?php echo form_error('plan_amount', '<div class="error" style="color:red;">', '</div>'); ?>
						</div>
						
					</div>



					<div class="form-group">
					    <label for="inputEmail3"  class="col-sm-2 control-label"><?php echo 'Plan Credit ';?>
                        <span style="color:#F00">*</span>
                        </label>
						<div class="col-sm-8">
						    <input  type='text'  class="form-control" id="plan_credit" name="plan_credit" value='<?php echo $plan_credit;?>'>
                            <?php echo form_error('plan_credit', '<div class="error" style="color:red;">', '</div>'); ?>
						</div>
						
					</div>

					<div class="form-group">
					    <label for="inputEmail3"  class="col-sm-2 control-label"><?php echo 'Plan per Image ';?>
                        <span style="color:#F00">*</span>
                        </label>
						<div class="col-sm-8">
						    <input  type='text'  class="form-control" id="plan_per_image" name="plan_per_image" value='<?php echo $plan_per_image;?>'>
                            <?php echo form_error('plan_per_image', '<div class="error" style="color:red;">', '</div>'); ?>
						</div>
						
					</div>
					
					
				
					
					<div class="form-group">
					<label for="inputEmail3"  class="col-sm-2 control-label"><?php echo 'Status';?></label>
					<div class="col-sm-8">
				
							
								<select name="status" id="status" class="form-control" >
								 <!--<option value=''>Select</option>-->
								 <option value="0" <?php if($status=='0') { ?> selected="selected" <?php } ?>  >Enable</option>
								 <option value="1" <?php if($status=='1') { ?> selected="selected" <?php } ?>   >Disable</option>
								 </select>
					
					</div>
					</div>
					
					
					
					
					
					
				
				
					
					
					
					
					
					
					
					
					
					
					
					
					
					
					
					<?php echo form_close();?> 
					 
					 
					 
				    </div>
					
					
					
					
				
					
				    </div>
					
					
					
					
				
		
					
					
					
				</div>
					
			</div>
		</div>
		
	</section>
</div>

<?php $this->load->view('admin/includes/globaljs');?>
<link type="text/css" href="<?php echo base_url();?>assets/form-select2/select2.css" rel="stylesheet"/>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootswatch/3.3.7/lumen/bootstrap.min.css">



<script type="text/javascript" src="<?php echo base_url();?>assets/form-select2/select2.min.js"></script>



<!--<script  type="text/javascript" src="<?php echo base_url();?>assets/dist/password-score.js"></script>
<script  type="text/javascript" src="<?php echo base_url();?>assets/dist/password-score-options.js"></script>
<script  type="text/javascript" src="<?php echo base_url();?>assets/dist/bootstrap-strength-meter.js"></script>-->

 <script>
 	function getCurrencySymbol()
 	{
 		var plan_currency=$('#plan_currency').val();
 		//alert(plan_currency);
 		if(plan_currency=='INR')
 		{
 			var plan_currency_symbol='&#x20B9;';
 		}
 		else if(plan_currency=='USD')
 		{
 			var plan_currency_symbol='&#36;';
 		}
 		else if(plan_currency=='EUR')
 		{
 			var plan_currency_symbol='&#128;';
 		}
 		else if(plan_currency=='GBP')
 		{
 			var plan_currency_symbol='&#163;';
 		}
 		else if(plan_currency=='AUD')
 		{
 			var plan_currency_symbol='&#36;';
 		}
 		else if(plan_currency=='CNY')
 		{
 			var plan_currency_symbol='&#165;';
 		}

 		else
 		{
 			var plan_currency_symbol='';
 		}
		var plan_currency_symbol='';

		$('#plan_currency_symbol').val(plan_currency_symbol);
 	}
    
    
	jQuery(function ($) {
	   // $('.content-wrapper').css('min-height','900px');
	   //$('#entity_id').select2();
		
		
		$('#plan_form').validate({
		
           rules: {
		
          
		
			plntyid: {
			required: true,
			},
			plan_currency: {
			required: true,
			},
			plan_per_image: {
			required: true,
			},
			/*plan_currency_symbol: {
			required: true,
			},*/
			plan_name: {
			required: true,
			},
			plan_amount: {
			required: true,
			},
			plan_credit: {
			required: true,
			},
			
			status: {
			required: true,
			}

			
		},
		 messages: {
		 
		  
		  
			
        },
				submitHandler: function(form) {
		
		       //alert("koko");
		
		        var planid=$('#planid').val();
				
			    var plntyid=$('#plntyid').val();
				var plan_currency=$('#plan_currency').val();
				var plan_currency_symbol=$('#plan_currency_symbol').val();
				var plan_per_image=$('#plan_per_image').val();
			    var plan_name=$('#plan_name').val();
			    var plan_amount=$('#plan_amount').val();
			    var plan_credit=$('#plan_credit').val();
			    var status=$('#status').val();
				
				//alert("<?php echo base_url();?>user/addUser?&planid="+planid+"&UserName="+UserName+"&UserComments="+UserComments+"&UserIsActive="+UserIsActive);	
					
				$.ajax({
				method:'post',
				url: "<?php echo base_url();?>admin/plan/insert_plan", 
				data:"&planid="+planid+"&plntyid="+plntyid+"&plan_currency="+plan_currency+"&plan_currency_symbol="+plan_currency_symbol+"&plan_per_image="+plan_per_image+"&plan_name="+plan_name+"&plan_amount="+plan_amount+"&plan_credit="+plan_credit+"&status="+status,
				success: function(result){
				if(result)
				{
					//alert(result);

					//alert("koko");

					var val=$.trim(result);
					//alert(val);

					$("#success_message").show();
					setTimeout(function(){setInterval(function(){
					//alert("ppp");
					parent.$.fancybox.close();

					}, 3000)}, 3000);
					parent.$.fancybox.close();
					parent.$('#example1').dataTable().fnStandingRedraw();
					
					
				  
					
				
				  
				  
				 
				
				}
				}
				});
					
					
					
					
					
						
							
					
					   
		
		} 
		 
		
		});
		

		
		
	});	
</script>   
<script>
 $(document).ready(function() {
 //alert("popo");
	  /* $('#example-getting-started-input').strengthMeter('text', {
            container: $('#example-getting-started-text')
        });
        $('#example-tooltip').strengthMeter('tooltip');*/
		/*$('#user_passowrd').strengthMeter('progressBar', {
            container: $('#example-progress-bar-container')
        });*/
    });
</script>
<script type="text/javascript" src="<?php echo base_url();?>theme/assets/source/jquery.fancybox.js?v=2.1.5"></script>
<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>theme/assets/source/jquery.fancybox.css?v=2.1.5" media="screen" />				