<?php  $this->load->view('admin/includes/globalcss');?>
<!-- <script src="https://cdn.ckeditor.com/4.5.7/standard/ckeditor.js"></script>
-->
 <script src="//cdn.ckeditor.com/4.13.0/full/ckeditor.js"></script>
<!-- <script src="<?php echo base_url(); ?>assets/ckeditor/ckeditor.js"></script> -->
<style>
/*.table-bordered {
    border: 1px solid #666666;
    }*/
    .showdbutton {
    	cursor:pointer; 
    	background-color:#666666; 
    	margin:10px 0px; 
    	padding:10px; 
    	color:#FFFFFF;
    	width:100%;
    }

    .disabledbutton {
    	pointer-events: none;
    	opacity: 0.9;

    	cursor:pointer; 
    	background-color:#999999; 
    	margin:10px 0px; 
    	padding:10px; 
    	color:#FFFFFF;
    	width:100%;
    }
    label.error{
    	color:#FF0000;
    	font-size:12px;
    }
</style>
<div>
	<!-- Section content header -->

	<!-- Section content -->
	<section class="content">
		<div class="col-md-12">
			<!-- start Information Box -->
			<div class="box">
				<!-- box header style start-->
				<div class="col-md-12 box-header with-border" ><h3 class="box-title"><?php if($id==''){ echo 'Create Pages'; }else {echo 'Update Pages'; }?></h3></div>
				<!-- /.box-header -->
				<!-- form start -->

				<div id="success_message" style="display:none;">
					<div class="alert alert-success">
						<strong>Updated successfully!</strong>.
					</div>
				</div>

				
				<div class="box-body">


					<div class="col-md-12">
						<ul class="nav nav-tabs">
							<li id="li_home" class="active"><a data-toggle="tab" href="#home">Pages Information</a></li>
							
							
						</ul>
					</div>


					<div class="addmargin10">&nbsp;</div>
					<div class="tab-content">

						<div id="home" class="tab-pane fade in active">

							<?php  $array = array('id'=>'Page_form', 'role'=>'form', 'class'=>'form form-horizontal has-validation-callback');
							echo form_open_multipart('Page/insert_page', $array); ?>


							<input type='hidden' name='id' value='<?php echo $id;?>' id='id'>
							<div class="form-group">
								<label for="inputEmail3"  class="col-sm-2 control-label">Page Title
									<span style="color:#F00">*</span>
								</label>
								<div class="col-sm-10">
									<input  type='text'  class="form-control" id="page_title" name="page_title" value='<?php echo $page_title;?>'>
									<?php echo form_error('page_title', '<div class="error" style="color:red;">', '</div>'); ?>
								</div>

							</div>


							<div class="form-group">
								<label for="inputEmail3"  class="col-sm-2 control-label">Page Content
									<span style="color:#F00">*</span>
								</label>
								<div class="col-sm-10">
									<textarea id="content" class="form-control" name="content"><?php echo $content;?></textarea>

									<?php echo form_error('content', '<div class="error" style="color:red;">', '</div>'); ?>
								</div>

							</div>
							<?php if($id==''){
								?>
								<div class="form-group">
									<label for="inputEmail3"  class="col-sm-2 control-label">Page Image (Optional)
										<span style="color:#F00">*</span>
									</label>
									<div class="col-sm-10">
										<!-- <input  type='file'  class="form-control" id="pimg" name="pimg"> -->						   
										<!-- <input type="file" class="upload_files fileInput css_uploadFile" id="files1" alt='1' name="files1[]" multiple /> -->
										<input  type='hidden'  class="form-control" id="pimg" name="pimg" value=''>
										<?php echo form_error('pimg', '<div class="error" style="color:red;">', '</div>'); ?>
									</div>

								</div>
								<?php
							}else {
								?>
								<div class="form-group">
									<label for="inputEmail3"  class="col-sm-2 control-label">Page  Image   
										<span style="color:#F00">*</span>
									</label>
									<div class="col-sm-10">
										<img style="width: 50px;" src="<?php echo base_url()?>assets/images/pages-img/<?php echo $pimg ?>">
									</div>
								</div>
								<div class="form-group">
									<!-- <label for="inputEmail3"  class="col-sm-2 control-label">Page New Image (Optional)
										<span style="color:#F00">*</span>
									</label> -->
									<div class="col-sm-10">
									<!-- 	<input  type='file'  class="form-control" id="pimg" name="pimg">	 -->
										<!-- <input type="file" class="upload_files fileInput css_uploadFile" id="files1" alt='1' name="files1[]" multiple /> -->
										<input  type='hidden'  class="form-control" id="pimg" name="pimg" value="">
						<!-- <div id="show_pimg1">
						<?php
						if($pimg!='' && $pimg!='No')
						{
						?>
						<span class="pip">
						<img src="<?php echo base_url(); ?>/assets/img/pages-img/<?php echo $pimg; ?>"  height='50' width='50'><br/>
						<span class='pimg_remove1 remove remove' id="1" value='<?php echo $pimg; ?>'>Remove</span>
						</span>
						<?php
						}
						?>
						</div>
             -->            <?php echo form_error('pimg', '<div class="error" style="color:red;">', '</div>'); ?>

										
									</div>

								</div>
								<?php
							}?>
							<!-- Contractor type fields end-->
						<!-- 	<div class="row">
								<?php if($id!=''){
									$button_name='Update';
								}else {
									$button_name='Save';
								} ?>
								<div class="col-md-6"><input class="btn btn-success pull-right" type="submit" value="<?php echo $button_name;?>"></div>	   
								<div class="col-md-6"><input class="btn btn-danger pull-left" type="reset"></div>
							</div> -->

							<?php echo form_close();?> 



						</div>


					</div>
					
				</div>

			</div>
		</div>
		
	</section>
</div>

<?php $this->load->view('admin/includes/globaljs'); ?>
<link type="text/css" href="<?php echo base_url();?>assets/form-select2/select2.css" rel="stylesheet"/>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootswatch/3.3.7/lumen/bootstrap.min.css">



<script type="text/javascript" src="<?php echo base_url();?>assets/form-select2/select2.min.js"></script>



<!--<script  type="text/javascript" src="<?php echo base_url();?>assets/dist/password-score.js"></script>
<script  type="text/javascript" src="<?php echo base_url();?>assets/dist/password-score-options.js"></script>
<script  type="text/javascript" src="<?php echo base_url();?>assets/dist/bootstrap-strength-meter.js"></script>-->

<script>


	jQuery(function ($) {
	   // $('.content-wrapper').css('min-height','900px');	   $('#entity_id').select2();
	   $('#Page_form').validate({

	   	rules: {

	   		page_title:{
	   			required:true,
	   			// minlength: 2,
	   			// alphanumeric:true,
	   			// atLeastOneLetter:true,
	   		},
	   		content:{
	   			required:true,				
	   		},

        },
	   		submitHandler: function(form) {
	   			 // var formData = new FormData(form);
	   			 //  formData.append('content',encodeURIComponent(CKEDITOR.instances['content'].getData()));
		  //      //alert("koko");

		       var id=$('#id').val();

		       var page_title=$('#page_title').val();
		       var content = encodeURIComponent(CKEDITOR.instances['content'].getData());
		  //     // var content=$('#content').val();
		     	var pimg =$('#pimg').val();
		  //      alert(pimg);
		  //      alert(content);
				// //var content=$('#content').val();			
				// //var user_status=$('#user_status').val();

				// //alert("<?php echo base_url();?>user/addUser?&id="+id+"&UserName="+UserName+"&UserComments="+UserComments+"&UserIsActive="+UserIsActive);	

				$.ajax({
					method:'POST',
					url: "<?php echo base_url();?>admin/page/insert_page", 
					data:"&id="+id+"&page_title="+page_title+"&content="+content+"&pimg="+pimg,
					success: function(result){
						if(result)
						{
							//consu;le
							//console.log(result);
				    	alert(result);					
							//var val=$.trim(result);
							//alert(val);
					
					$("#success_message").show();
						setTimeout(function(){
							setInterval(function(){
					  		//alert("ppp");
					  		parent.$.fancybox.close();
							}, 
							3000)
							}, 
						3000);
						parent.$.fancybox.close();
						parent.$('#example1').dataTable().fnStandingRedraw();
				}
			}
		});
			}, 


		});	
	});	





	 $(".upload_files").on("change", function(e) {
	
//alert("goko");
	 var no = $(this).attr('alt') ;
	 
	 //alert(no);
	
      var files = e.target.files,
        filesLength = files.length;
      for (var i = 0; i < filesLength; i++) {
        var f = files[i]
        var fileReader = new FileReader();
        fileReader.onload = (function(e) {
          var file = e.target;
		  
		  //alert(e.target.result);
		  
         /* $("<span class=\"pip\">" +
            "<img class=\"imageThumb\" src=\"" + e.target.result + "\" title=\"" + file.name + "\"/>" +
            "<br/><span class=\"remove\">Remove</span>" +
            "</span>").insertAfter("#files");*/
			
			
			
			
	//alert(no);
  var input = document.getElementById("files"+no);
  file = input.files[0];
  if(file != undefined){
    formData= new FormData();

    if(!!file.type.match(/image.*/)){
      formData.append("image", file);


      //alert("aaaa");
      $.ajax({
        url: "<?php echo base_url();?>admin/page/upload_file",
        type: "POST",
        data: formData,
        processData: false,
        contentType: false,
        success: function(data){
		
		 //alert("koko");
	    // alert(data);
		 
		    $("#pimg").val($.trim(data));
			  
			 
			  
			  
			 /*$("<span class=\"pip\">" +
             show_pimg +
            "<br/><span class='remove remove"+no+"' value='"+pimg+"'>Remove</span>" +
            "</span>").insertAfter("#files");*/
			
			 /*$("<span class=\"pip\">" +
            "<img class=\"imageThumb\" src=\"" + e.target.result + "\" title=\"" + file.name + "\"/>" +
            "<br/><span class=\"remove\">Remove</span>" +
            "</span>").insertAfter("#files");*/
			
			/*var temp_pimg = $("#pimg"+no).val();
			if(temp_pimg!='')
			{
			
			 var main_pimg=temp_pimg+",,,"+pimg;
			 $("#pimg"+no).val(main_pimg);
			}
			else
			{
			 $("#pimg"+no).val(",,,"+pimg);
			}*/
			
			//  $("#show_pimg"+no).html("<span class=\"pip\">" +
   //           show_pimg +
   //          "<br/><span class='pimg_remove remove remove"+no+"' value='"+pimg+"'>Remove</span>" +
   //          "</span>");
			
			// $("#pimg"+no).val(data);
		
		
          // alert('success');
		   
		   
		  //   $(".remove"+no+"").click(function(){
		  
		  //     //alert("joko");
			  
			 //   var remove_value = $(this).attr('value') ;
			   
			 //  // alert(remove_value);
			   
			 //   var temp_pimg = $("#pimg"+no).val();
			   
			 //   var main_pimg = temp_pimg.replace(remove_value, '')
			   
			 //    $("#pimg"+no).val(main_pimg);
			  
    //            $(this).parent(".pip").remove();
			   
			   
			 //  // alert("<?php echo base_url();?>admin/Project/get_unlink_path");
			   
				// $.ajax({
				// method:'get',
				// url: "<?php echo base_url(); ?>item/get_unlink_path", 
				// data:"&image_delete_value="+remove_value,
				// success: function(result){
				// if(result)
				// {
				// //alert(result);
				// }
				// }
				// });
				
				// return false;			
    //       });
		  
		  
        }
      });
    }else{
	
	  alert("Please Upload Images");
	  return false;
	  
	  
	   formData.append("image", file);
       $.ajax({
        url: "<?php echo base_url();?>page/upload_file",
        type: "POST",
        data: formData,
        processData: false,
        contentType: false,
        success: function(data){
		
		
		  // alert(data);
				
			$("#pimg").val(data);
			
			  //  var arr=data.split("@@@");
			  // var show_pimg=arr[0];
			  // var pimg=arr[1];
		     
		 /*$("<span class=\"pip\">" +
             show_pimg +
            "<br/><span class='remove remove"+no+"' value='"+pimg+"'>Remove</span>" +
            "</span>").insertAfter("#files");*/
			
			
			/*var temp_pimg = $("#pimg"+no).val();
			if(temp_pimg!='')
			{
			
			 var main_pimg=temp_pimg+",,,"+pimg;
			 $("#pimg"+no).val(main_pimg);
			}
			else
			{
			 $("#pimg"+no).val(",,,"+pimg);
			}*/
			
			 // $("#show_pimg"+no).html("<span class=\"pip\">" +
    //          show_pimg +
    //         "<br/><span class=' pimg_remove remove remove"+no+"' value='"+pimg+"'>Remove</span>" +
    //         "</span>");
			
			
			
			
			
		 // $(".remove"+no+"").click(function(){
		  
		 //      //alert("joko");
			  
			//    var remove_value = $(this).attr('value') ;
			   
			//  //  alert(remove_value);
			   
			//    var temp_pimg = $("#pimg"+no).val();
			   
			//    var main_pimg = temp_pimg.replace(remove_value, '')
			   
			//     $("#pimg"+no).val(main_pimg);
			  
   //             $(this).parent(".pip").remove();
			   
			   
			//    //alert("<?php echo base_url();?>admin/Project/get_unlink_path");
			   
			// 	$.ajax({
			// 	method:'get',
			// 	url: "<?php echo base_url(); ?>item/get_unlink_path", 
			// 	data:"&image_delete_value="+remove_value,
			// 	success: function(result){
			// 	if(result)
			// 	{
			// 	//alert(result);
			// 	}
			// 	}
			// 	});			   
			   
			//    return false;			
   //        });	
		
		
           // alert('success');
        }
      });
	  
	  
    }
  }
  else{
    alert('Input something!');
  }

          /*$(".remove").click(function(){
		  
		  alert("joko");
            $(this).parent(".pip").remove();
          });*/
          
          // Old code here
          /*$("<img></img>", {
            class: "imageThumb",
            src: e.target.result,
            title: file.name + " | Click to remove"
          }).insertAfter("#files").click(function(){$(this).remove();});*/
          
        });
        fileReader.readAsDataURL(f);
      }
    });
	
</script>   
<script>
	$(document).ready(function() {
		CKEDITOR.replace('content', {
			allowedContent:true,
		});
 //alert("popo");
	  /* $('#example-getting-started-input').strengthMeter('text', {
            container: $('#example-getting-started-text')
        });
        $('#example-tooltip').strengthMeter('tooltip');*/
		/*$('#user_passowrd').strengthMeter('progressBar', {
            container: $('#example-progress-bar-container')
        });*/
    });
</script>
<script type="text/javascript" src="<?php echo base_url();?>theme/assets/source/jquery.fancybox.js?v=2.1.5"></script>
<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>theme/assets/source/jquery.fancybox.css?v=2.1.5" media="screen" />				
