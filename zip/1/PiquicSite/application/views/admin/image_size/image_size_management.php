<?php  $this->load->view('admin/includes/globalcss');?>
<!-- <script src="https://cdn.ckeditor.com/4.5.7/standard/ckeditor.js"></script>
-->
<!--  <script src="//cdn.ckeditor.com/4.13.0/full/ckeditor.js"></script> -->
<!-- <script src="<?php echo base_url(); ?>assets/ckeditor/ckeditor.js"></script> -->
 <script src="//cdn.ckeditor.com/4.13.0/full/ckeditor.js"></script>
<style>
/*.table-bordered {
    border: 1px solid #666666;
    }*/
    .showdbutton {
    	cursor:pointer; 
    	background-color:#666666; 
    	margin:10px 0px; 
    	padding:10px; 
    	color:#FFFFFF;
    	width:100%;
    }

    .disabledbutton {
    	pointer-events: none;
    	opacity: 0.9;

    	cursor:pointer; 
    	background-color:#999999; 
    	margin:10px 0px; 
    	padding:10px; 
    	color:#FFFFFF;
    	width:100%;
    }
    label.error{
    	color:#FF0000;
    	font-size:12px;
    }
</style>
<div>
	<!-- Section content header -->

	<!-- Section content -->
	<section class="content">
		<div class="col-md-12">
			<!-- start Information Box -->
			<div class="box">
				<!-- box header style start-->
				<div class="col-md-12 box-header with-border" ><h3 class="box-title"><?php if($id==''){ echo 'Create Image Size'; }else {echo 'Update Image Size'; }?></h3></div>
				<!-- /.box-header -->
				<!-- form start -->

				<div id="success_message" style="display:none;">
					<div class="alert alert-success">
						<strong>Updated successfully!</strong>.
					</div>
				</div>

				
				<div class="box-body">


					<div class="col-md-12">
						<ul class="nav nav-tabs">
							<li id="li_home" class="active"><a data-toggle="tab" href="#home">Image Size Information</a></li>
							
							
						</ul>
					</div>


					<div class="addmargin10">&nbsp;</div>
					<div class="tab-content">

						<div id="home" class="tab-pane fade in active">

							<?php  $array = array('id'=>'Image_size_form', 'role'=>'form', 'class'=>'form form-horizontal has-validation-callback');
							echo form_open_multipart('Image_size/insert_image_size', $array); ?>


							<input type='hidden' name='id' value='<?php echo $id;?>' id='id'>
							<div class="form-group">
								<label for="inputEmail3"  class="col-sm-2 control-label">Image Size 
									<span style="color:#F00">*</span>
								</label>
								<div class="col-sm-10">
									<!-- <input  type='text'  class="form-control" id="image_size" name="image_size" value='<?php echo $image_size;?>'> -->



									<textarea id="image_size" class="form-control" name="image_size"><?php echo $image_size;?></textarea>
									<?php echo form_error('image_size', '<div class="error" style="color:red;">', '</div>'); ?>
								</div>

							</div>

							<div class="form-group">
							<label for="inputEmail3"  class="col-sm-2 control-label"><?php echo 'Status';?></label>
							<div class="col-sm-8">


								<select name="status" id="status" class="form-control" >
									<!--<option value=''>Select</option>-->
									<option value="0" <?php if($status=='0') { ?> selected="selected" <?php } ?>  >Enable</option>
									<option value="1" <?php if($status=='1') { ?> selected="selected" <?php } ?>   >Disable</option>
								</select>

							</div>
						</div>

							<!-- Contractor type fields end-->
							<div class="row">
								<?php if($id!=''){
									$button_name='Update';
								}else {
									$button_name='Save';
								} ?>
								<div class="col-md-6"><input class="btn btn-success pull-right" type="submit" value="<?php echo $button_name;?>"></div>	   
								<div class="col-md-6"><input class="btn btn-danger pull-left" type="reset"></div>
							</div>

							<?php echo form_close();?> 



						</div>


					</div>
					
				</div>

			</div>
		</div>
		
	</section>
</div>

<?php $this->load->view('admin/includes/globaljs'); ?>
<link type="text/css" href="<?php echo base_url();?>assets/form-select2/select2.css" rel="stylesheet"/>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootswatch/3.3.7/lumen/bootstrap.min.css">



<script type="text/javascript" src="<?php echo base_url();?>assets/form-select2/select2.min.js"></script>



<!--<script  type="text/javascript" src="<?php echo base_url();?>assets/dist/password-score.js"></script>
<script  type="text/javascript" src="<?php echo base_url();?>assets/dist/password-score-options.js"></script>
<script  type="text/javascript" src="<?php echo base_url();?>assets/dist/bootstrap-strength-meter.js"></script>-->

<script>
	jQuery(function ($) {
	   $('#Image_size_form').validate({
	   	rules: {
	   		image_size:{
	   			required:true,
	   		},
	   		
        },
	   		submitHandler: function(form) {
	   			var id=$('#id').val();
 		        var image_size=encodeURIComponent(CKEDITOR.instances['image_size'].getData());
 		        var status=$('#status').val();
		        $.ajax({
					method:'POST',
					url: "<?php echo base_url();?>admin/image_size/insert_image_size", 
					data:"&id="+id+"&image_size="+image_size+"&status="+status,
					success: function(result){
						if(result)
						{
							//alert(result);
							$("#success_message").show();
							setTimeout(function(){
								setInterval(function(){
						  		//alert("ppp");
						  		parent.$.fancybox.close();
								}, 
								3000)
							}, 
						3000);
						parent.$.fancybox.close();
						parent.$('#example1').dataTable().fnStandingRedraw();
				}
			}
		});
			}, 


		});	
	});	



	
</script>   
<script>
	$(document).ready(function() {
		CKEDITOR.replace('image_size', {
			allowedContent:true,
		});
    });
</script>
<script type="text/javascript" src="<?php echo base_url();?>theme/assets/source/jquery.fancybox.js?v=2.1.5"></script>
<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>theme/assets/source/jquery.fancybox.css?v=2.1.5" media="screen" />				
