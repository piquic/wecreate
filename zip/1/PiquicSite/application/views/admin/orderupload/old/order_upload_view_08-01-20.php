<?php  $this->load->view('admin/includes/globalcss');?>


<style>
/*.table-bordered {
    border: 1px solid #666666;
}*/
.showdbutton {
    cursor:pointer; 
	background-color:#666666; 
	margin:10px 0px; 
	padding:10px; 
	color:#FFFFFF;
	width:100%;
}

.disabledbutton {
    pointer-events: none;
    opacity: 0.9;
	
	cursor:pointer; 
	background-color:#999999; 
	margin:10px 0px; 
	padding:10px; 
	color:#FFFFFF;
	width:100%;
}
label.error{
 color:#FF0000;
 font-size:12px;
}
</style>
<div>
  <!-- Section content header -->
 
  <!-- Section content -->
    <section class="content">
		<div class="col-md-12">
		  <!-- start Information Box -->
			<div class="box">
			<!-- box header style start-->
				<div class="col-md-12 box-header with-border" ><h3 class="box-title"><?php if($orderid==''){ echo 'Create order Type'; }else {echo 'Update order Type'; }?></h3></div>
					<!-- /.box-header -->
					<!-- form start -->
					
					<div id="success_message" style="display:none;">
					<div class="alert alert-success">
					<strong>Updated successfully!</strong>.
					</div>
				    </div>
					
				
				<div class="box-body">
				
				
				   <div class="col-md-12">
						<ul class="nav nav-tabs">
							<li id="li_home" class="active"><a data-toggle="tab" href="#home">Payment Type Information</a></li>
							
							
						</ul>
					</div>
				
				
				<div class="addmargin10">&nbsp;</div>
					<div class="tab-content">
					
				     <div id="home" class="tab-pane fade in active">
					 
					 		    <?php  $array = array('id'=>'order_form', 'role'=>'form', 'class'=>'form form-horizontal has-validation-callback');
				    echo form_open_multipart('order/insert_order', $array); ?>
					
					
					<input type='hidden' name='orderid' value='<?php echo $orderid;?>' id='orderid'>
					
					
					
	              <div class="form-group">
					    <label for="inputEmail3"  class="col-sm-2 control-label"><?php echo 'order Type Name';?>
                        <span style="color:#F00">*</span>
                        </label>
						<div class="col-sm-8">
						    <!-- <input  type='text'  class="form-control" id="plntyid" name="plntyid" value='<?php echo $plntyid;?>'> -->


							<select name="plntyid" id="plntyid" class="form-control" >
							<option value=''>Select</option>
							<?php
							$ordertype_details=$this->db->query("select * from tbl_ordertype where deleted='0' and status='0' ")->result_array();
							if(!empty($ordertype_details))
							{
							foreach($ordertype_details as $key => $ordertype)
							{
								$plntyid=$ordertype['plntyid'];
								$plntypname=$ordertype['plntypname'];
							?>
							<option value="<?php echo $plntyid;?>" <?php if($plntyid_sel==$plntyid) { ?> selected="selected" <?php } ?>  ><?php echo $plntypname;?></option>
							<?php

							}
							}
							?>

						



							</select>




                            <?php echo form_error('plntyid', '<div class="error" style="color:red;">', '</div>'); ?>
						</div>
						
					</div>



					<div class="form-group">
					<label for="inputEmail3"  class="col-sm-2 control-label"><?php echo 'order Currency';?>
					<span style="color:#F00">*</span>
					</label>
					<div class="col-sm-8">
					<select id="order_currency" name="order_currency" class="form-control" required="" onchange="getCurrencySymbol();">
					<option value="" >Select</option>
					<option value="INR" <?php if($order_currency=='INR') { ?> selected <?php } ?>>INR (&#x20B9;)</option>
					<option value="USD" <?php if($order_currency=='USD')  { ?> selected <?php } ?>>USD (&#36;)</option>
					<option value="EUR" <?php if($order_currency=='EUR') {  ?> selected <?php } ?>>EUR (&#128;)</option>
					<option value="GBP" <?php if($order_currency=='GBP') {  ?> selected <?php } ?>>GBP (&#163;)</option>
					<option value="AUD" <?php if($order_currency=='AUD') {    ?> selected <?php } ?>>AUD (&#36;)</option>
					<option value="CNY" <?php if($order_currency=='CNY') {  ?> selected <?php } ?>>CNY (&#165;)</option>
					</select>
					<?php echo form_error('order_currency', '<div class="error" style="color:red;">', '</div>'); ?>
					<input  type='hidden'  class="form-control" id="order_currency_symbol" name="order_currency_symbol" value='<?php echo $order_currency_symbol;?>' readonly>
					</div>

					</div>



					




						<div class="form-group">
					    <label for="inputEmail3"  class="col-sm-2 control-label"><?php echo 'order name Name';?>
                        <span style="color:#F00">*</span>
                        </label>
						<div class="col-sm-8">
						    <input  type='text'  class="form-control" id="order_name" name="order_name" value='<?php echo $order_name;?>'>
                            <?php echo form_error('order_name', '<div class="error" style="color:red;">', '</div>'); ?>
						</div>
						
					</div>
					
					
					
					
					
					
					<div class="form-group">
					    <label for="inputEmail3"  class="col-sm-2 control-label"><?php echo 'order Amount ';?>
                        <span style="color:#F00">*</span>
                        </label>
						<div class="col-sm-8">
						    <input  type='text'  class="form-control" id="order_amount" name="order_amount" value='<?php echo $order_amount;?>'>
                            <?php echo form_error('order_amount', '<div class="error" style="color:red;">', '</div>'); ?>
						</div>
						
					</div>



					<div class="form-group">
					    <label for="inputEmail3"  class="col-sm-2 control-label"><?php echo 'order Credit ';?>
                        <span style="color:#F00">*</span>
                        </label>
						<div class="col-sm-8">
						    <input  type='text'  class="form-control" id="order_credit" name="order_credit" value='<?php echo $order_credit;?>'>
                            <?php echo form_error('order_credit', '<div class="error" style="color:red;">', '</div>'); ?>
						</div>
						
					</div>

					<div class="form-group">
					    <label for="inputEmail3"  class="col-sm-2 control-label"><?php echo 'order per Image ';?>
                        <span style="color:#F00">*</span>
                        </label>
						<div class="col-sm-8">
						    <input  type='text'  class="form-control" id="order_per_image" name="order_per_image" value='<?php echo $order_per_image;?>'>
                            <?php echo form_error('order_per_image', '<div class="error" style="color:red;">', '</div>'); ?>
						</div>
						
					</div>
					
					
				
					
					<div class="form-group">
					<label for="inputEmail3"  class="col-sm-2 control-label"><?php echo 'Status';?></label>
					<div class="col-sm-8">
				
							
								<select name="status" id="status" class="form-control" >
								 <!--<option value=''>Select</option>-->
								 <option value="0" <?php if($status=='0') { ?> selected="selected" <?php } ?>  >Enable</option>
								 <option value="1" <?php if($status=='1') { ?> selected="selected" <?php } ?>   >Disable</option>
								 </select>
					
					</div>
					</div>
					
					
					
					
					
					
				
				
					
					
					
					
					
					
					
					
					
					
					
					
					
					
					
					<?php echo form_close();?> 
					 
					 
					 
				    </div>
					
					
					
					
				
					
				    </div>
					
					
					
					
				
		
					
					
					
				</div>
					
			</div>
		</div>
		
	</section>
</div>

<?php $this->load->view('admin/includes/globaljs');?>
<link type="text/css" href="<?php echo base_url();?>assets/form-select2/select2.css" rel="stylesheet"/>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootswatch/3.3.7/lumen/bootstrap.min.css">



<script type="text/javascript" src="<?php echo base_url();?>assets/form-select2/select2.min.js"></script>



<!--<script  type="text/javascript" src="<?php echo base_url();?>assets/dist/password-score.js"></script>
<script  type="text/javascript" src="<?php echo base_url();?>assets/dist/password-score-options.js"></script>
<script  type="text/javascript" src="<?php echo base_url();?>assets/dist/bootstrap-strength-meter.js"></script>-->

 <script>
 	function getCurrencySymbol()
 	{
 		var order_currency=$('#order_currency').val();
 		//alert(order_currency);
 		if(order_currency=='INR')
 		{
 			var order_currency_symbol='&#x20B9;';
 		}
 		else if(order_currency=='USD')
 		{
 			var order_currency_symbol='&#36;';
 		}
 		else if(order_currency=='EUR')
 		{
 			var order_currency_symbol='&#128;';
 		}
 		else if(order_currency=='GBP')
 		{
 			var order_currency_symbol='&#163;';
 		}
 		else if(order_currency=='AUD')
 		{
 			var order_currency_symbol='&#36;';
 		}
 		else if(order_currency=='CNY')
 		{
 			var order_currency_symbol='&#165;';
 		}

 		else
 		{
 			var order_currency_symbol='';
 		}
		var order_currency_symbol='';

		$('#order_currency_symbol').val(order_currency_symbol);
 	}
    
    
	jQuery(function ($) {
	   // $('.content-wrapper').css('min-height','900px');
	   //$('#entity_id').select2();
		
		
		$('#order_form').validate({
		
           rules: {
		
          
		
			plntyid: {
			required: true,
			},
			order_currency: {
			required: true,
			},
			order_per_image: {
			required: true,
			},
			/*order_currency_symbol: {
			required: true,
			},*/
			order_name: {
			required: true,
			},
			order_amount: {
			required: true,
			},
			order_credit: {
			required: true,
			},
			
			status: {
			required: true,
			}

			
		},
		 messages: {
		 
		  
		  
			
        },
				submitHandler: function(form) {
		
		       //alert("koko");
		
		        var orderid=$('#orderid').val();
				
			    var plntyid=$('#plntyid').val();
				var order_currency=$('#order_currency').val();
				var order_currency_symbol=$('#order_currency_symbol').val();
				var order_per_image=$('#order_per_image').val();
			    var order_name=$('#order_name').val();
			    var order_amount=$('#order_amount').val();
			    var order_credit=$('#order_credit').val();
			    var status=$('#status').val();
				
				//alert("<?php echo base_url();?>user/addUser?&orderid="+orderid+"&UserName="+UserName+"&UserComments="+UserComments+"&UserIsActive="+UserIsActive);	
					
				$.ajax({
				method:'post',
				url: "<?php echo base_url();?>admin/order/insert_order", 
				data:"&orderid="+orderid+"&plntyid="+plntyid+"&order_currency="+order_currency+"&order_currency_symbol="+order_currency_symbol+"&order_per_image="+order_per_image+"&order_name="+order_name+"&order_amount="+order_amount+"&order_credit="+order_credit+"&status="+status,
				success: function(result){
				if(result)
				{
					//alert(result);

					//alert("koko");

					var val=$.trim(result);
					//alert(val);

					$("#success_message").show();
					setTimeout(function(){setInterval(function(){
					//alert("ppp");
					parent.$.fancybox.close();

					}, 3000)}, 3000);
					parent.$.fancybox.close();
					parent.$('#example1').dataTable().fnStandingRedraw();
					
					
				  
					
				
				  
				  
				 
				
				}
				}
				});
					
					
					
					
					
						
							
					
					   
		
		} 
		 
		
		});
		

		
		
	});	
</script>   
<script>
 $(document).ready(function() {
 //alert("popo");
	  /* $('#example-getting-started-input').strengthMeter('text', {
            container: $('#example-getting-started-text')
        });
        $('#example-tooltip').strengthMeter('tooltip');*/
		/*$('#user_passowrd').strengthMeter('progressBar', {
            container: $('#example-progress-bar-container')
        });*/
    });
</script>
<script type="text/javascript" src="<?php echo base_url();?>theme/assets/source/jquery.fancybox.js?v=2.1.5"></script>
<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>theme/assets/source/jquery.fancybox.css?v=2.1.5" media="screen" />				