<?php  $this->load->view('admin/includes/globalcss');

$second_DB = $this->load->database('another_db', TRUE); 
?>
<link rel="stylesheet" href="<?php echo base_url(); ?>website-assets/css/bootstrap.min.css">

<style>
/*.table-bordered {
    border: 1px solid #666666;
}*/
.showdbutton {
    cursor:pointer; 
	background-color:#666666; 
	margin:10px 0px; 
	padding:10px; 
	color:#FFFFFF;
	width:100%;
}

.disabledbutton {
    pointer-events: none;
    opacity: 0.9;
	
	cursor:pointer; 
	background-color:#999999; 
	margin:10px 0px; 
	padding:10px; 
	color:#FFFFFF;
	width:100%;
}
label.error{
 color:#FF0000;
 font-size:12px;
}
</style>
<div>
  <!-- Section content header -->
 
  <!-- Section content -->
    <section class="content">
		<div class="col-md-12">
		  <!-- start Information Box -->
			<div class="box">
			<!-- box header style start-->
				<div class="col-md-12 box-header with-border" ><h3 class="box-title">View Upload Order</h3></div>
					<!-- /.box-header -->
					<!-- form start -->
					
					<div id="success_message" style="display:none;">
					<div class="alert alert-success">
					<strong>Updated successfully!</strong>.
					</div>
				    </div>
					
				
				<div class="box-body">
				
				
				   <div class="col-md-12">
						<ul class="nav nav-tabs">
							<li id="li_home" class="active"><a data-toggle="tab" href="#home">Upload Order Information</a></li>
							
							
						</ul>
					</div>
				
				
				<div class="addmargin10">&nbsp;</div>
					<div class="tab-content">
					
				     <div id="home" class="tab-pane fade in active">
					 
					 		    <?php  $array = array('id'=>'order_form', 'role'=>'form', 'class'=>'form form-horizontal has-validation-callback');
				    echo form_open_multipart('order/insert_order', $array); ?>
					
					
					<input type='hidden' name='orderid' value='<?php echo $orderid;?>' id='orderid'>
					
					<div class="form-group">
					    <label for="inputEmail3"  class="col-sm-2 control-label"><?php echo 'SKU No.';?>
                        </label>
						<div class="col-sm-8">
						   <?php echo $skuno;?>
						</div>
						
					</div>
					
	              <div class="form-group">
					    <label for="inputEmail3"  class="col-sm-2 control-label"><?php echo 'Gender';?>
                        
                        </label>
						<div class="col-sm-8">
						  <?php 
										if($gender=="1"){ echo "Male" ; } 
										if($gender=="2"){ echo "Female" ; } 
										if($gender=="4"){ echo "Boy" ; } 
										if($gender=="6"){ echo "Girl" ; } 
										?>
						</div>
						
					</div>

					<div class="form-group">
					    <label for="inputEmail3"  class="col-sm-2 control-label"><?php echo 'Category';?>
                        
                        </label>
						<div class="col-sm-8">
						  <?php echo $category;?>
						</div>
						
					</div>


				

					


			<div class="row">
				<div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
					<h5>Assets</h5>
				</div>
				<div class="col-6 col-sm-6 col-md-4 col-lg-2 col-xl-2">
					<div class="w-100 mb-3 p-1 text-center">
						<p class="lead">Background</p>
						<img class="w-100 d-block border" src="<?php echo PIQUIC_URL;?>images/thumbnails/bg/<?php echo $bg; ?>">
						<span><?php echo str_replace(".jpg","", $bg); ?></span>
					</div>
				</div>

				<?php


			


			$mdlqry   = "SELECT * FROM `tbl_model` WHERE `m_id` = '$model_id'";
			$query=$second_DB->query($mdlqry);
			$ymrows=$query->result_array();
			$mdlrow=$ymrows[0];
			$mod_name = $mdlrow['mod_name'];
			$mod_img  = $mdlrow['mod_img'];

			$mbqry      = "SELECT * FROM `tbl_model` WHERE `mod_name` = '$modbody'";
			$query=$second_DB->query($mdlqry);
			$ymrows=$query->result_array();
			$mbrow=$ymrows[0];
			$mbmod_img  = $mbrow['mod_img'];
				?>

				<div class="col-6 col-sm-6 col-md-4 col-lg-2 col-xl-2">
					<div class="w-100 mb-3 p-1 text-center">
						<p class="lead">Model</p>
						<img class="w-100 d-block py-3 border" src="<?php echo PIQUIC_URL;?>images/thumbnails/model_image/<?php echo $mod_img; ?>">
						<span><?php echo $mod_name; ?></span>
					</div>
				</div>

				<div class="col-6 col-sm-6 col-md-4 col-lg-2 col-xl-2">
					<div class="w-100 mb-3 p-1 text-center">
						<p class="lead">Model Body</p>
						<img class="w-100 d-block py-3 border" src="<?php echo PIQUIC_URL;?>images/thumbnails/model_image/<?php echo $mbmod_img; ?>">
						<span><?php echo $modbody; ?></span>
					</div>
				</div>

				<?php if($poseids != null){ ?>
					<div class="col-6 col-sm-6 col-md-4 col-lg-2 col-xl-2">
						<div class="w-100 mb-3 p-1 text-center">
							<p class="lead">Pose<small>(s)</small></p>
							<div id="carouselPose" class="carousel slide" data-ride="carousel">
								<div class="carousel-inner">
									<?php
									$poseqry  = "SELECT * FROM `tbl_model_pose` WHERE `mpose_id` IN ($poseids);";
									

						// $mpose_img = array();
									$pid = 1;

									$query2=$second_DB->query($poseqry);										
											$modelinfo= $query2->result_array();
											foreach($modelinfo as $poserow){

									
										$mpose_img = $poserow['mpose_img'];
							// array_push($mpose_img, $temp_nm);

										?>
										<div class="carousel-item <?php if($pid == 1) { ?> active <?php } ?>">
											<img src="<?php echo PIQUIC_URL;?>images/thumbnails/model_pose/<?php echo $mpose_img; ?>" class="d-block w-100 border" alt="<?php echo $mpose_img; ?>">
											<span><?php echo str_replace(".png","", $mpose_img); ?></span>
										</div>
										<?php $pid++; }?>
									</div>
									<a class="carousel-control-prev" href="#carouselPose" role="button" data-slide="prev">
										<span class="carousel-control-prev-icon" aria-hidden="true"></span>
										<span class="sr-only">Previous</span>
									</a>
									<a class="carousel-control-next" href="#carouselPose" role="button" data-slide="next">
										<span class="carousel-control-next-icon" aria-hidden="true"></span>
										<span class="sr-only">Next</span>
									</a>
								</div>
							</div>
						</div>
					<?php } ?>

					<?php if(!empty($top)){ ?>
						<div class="col-6 col-sm-6 col-md-4 col-lg-2 col-xl-2">
							<div class="w-100 mb-3 p-1 text-center" style="max-height: 31.4rem;">
								<p class="lead">Top</p>
								<div class="w-100 border" style="height: 31.5rem; overflow: hidden;">
									<img src="<?php echo PIQUIC_URL;?>images/thumbnails/top/<?php echo $top;?>" style="width: 20rem; margin: -4.5rem 0 0 -6.5rem;">
								</div>
								<span><?php echo str_replace(".png","", $top); ?></span>
							</div>
						</div>
					<?php } ?>

					<?php if(!empty($bottom)){ ?>
						<div class="col-6 col-sm-6 col-md-4 col-lg-2 col-xl-2">
							<div class="w-100 mb-3 p-1 text-center" style="max-height: 31.4rem;">
								<p class="lead">Bottom</p>
								<div class="w-100 border" style="height: 31.4rem; overflow: hidden;">
									<img src="<?php echo PIQUIC_URL;?>images/thumbnails/bottom/<?php echo $bottom; ?>"; style="width: 13rem; margin: -7rem 0 0 -2.9rem;">
								</div>
								<!-- <img class="w-100 d-block" src="images/bottom/<?php echo $bottom; ?>"> -->
								<span><?php echo str_replace(".png","", $bottom); ?></span>
							</div>
						</div>
					<?php } ?>

					<div class="col-6 col-sm-6 col-md-4 col-lg-2 col-xl-2">
						<div class="w-100 mb-3 p-1 text-center">
							<p class="lead">Shoes</p>
							<div class="w-100 border" style="height: 11.5rem; overflow: hidden;">
								<img src="<?php echo PIQUIC_URL;?>images/thumbnails/foot/<?php echo $shoes; ?>" style="width: 22rem; margin: -21rem 0 0 -7.5rem;">
							</div>
							<!-- <img class="w-100 d-block" src="images/foot/<?php echo $shoes; ?>"> -->
							<span><?php echo str_replace(".png","", $shoes); ?></span>
						</div>
					</div>

					<?php if($accessories != null){ ?>
						<div class="col-6 col-sm-6 col-md-4 col-lg-2 col-xl-2">
							<div class="w-100 mb-3 p-1 text-center">
								<p class="lead">Accessories</p>
								<div id="carouselAcc" class="carousel slide" data-ride="carousel">
									<div class="carousel-inner">
										<?php
										$accqry  = "SELECT * FROM `tbl_accessories` WHERE `acces_id` IN ($accessories);";

										$pid = 1;
										

											$query2=$second_DB->query($accqry);										
											$modelinfo= $query2->result_array();
											foreach($modelinfo as $accrow){

											$acces_img = $accrow['acces_img'];
											?>
											<div class="carousel-item <?php if($pid == 1) { ?> active <?php } ?>">
												<div class="w-100 border" style="height: 11.5rem; overflow: hidden;">
													<img src="<?php echo PIQUIC_URL;?>images/thumbnails/accessories/<?php echo $acces_img; ?>" style="width: 55rem; margin: 0 0 0 -47.5rem;">
												</div>
												<!-- <img src="images/accessories/<?php echo $acces_img; ?>" class="d-block w-100" alt="<?php //echo $acces_img; ?>"> -->
												<span><?php echo str_replace(".png","", $acces_img); ?></span>
											</div>
											<?php $pid++; }?>
										</div>
										<a class="carousel-control-prev" href="#carouselAcc" role="button" data-slide="prev">
											<span class="carousel-control-prev-icon" aria-hidden="true"></span>
											<span class="sr-only">Previous</span>
										</a>
										<a class="carousel-control-next" href="#carouselAcc" role="button" data-slide="next">
											<span class="carousel-control-next-icon" aria-hidden="true"></span>
											<span class="sr-only">Next</span>
										</a>
									</div>
								</div>
							</div>
						<?php } ?>
					</div>

					<div class="row">
						<?php if( ($lipcol != null && $lipefft != null) || $eyemkup != null || ( $eyeshcol != null && $eyeint != null ) || (!empty($blshcol) && !empty($blshint))) { ?>
							<div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
								<div class="w-100 mb-3 p-1 text-center" style="max-height: 16.4rem;">
									<p class="lead">Makeup</p>

									<div class="w-100 border d-flex justify-content-around" style="height: 11.5rem;">

										<?php if($lipcol != null && $lipefft != null) { ?>
											<div class="text-center pt-2">
												<p>Lipstick Color</p>
												<?php
												$lipqry = "SELECT * FROM `tbl_lipcol` WHERE `col_code` LIKE '$lipcol' GROUP BY `col_code`";

												
												$query=$second_DB->query($lipqry);
												$ymrows=$query->result_array();
												$liprow=$ymrows[0];
												$col_name = $liprow['col_name'];







												$hex = "#".$lipcol."";
												list($r, $g, $b) = sscanf($hex, "#%02x%02x%02x");
												$r += 82;
												$g += 23;
												$b += 15;
												?>
												<div class="p-1 mb-3 shadow rounded" style="width: 100%;" title="<?php echo $col_name;?>">
													<i class="fas fa-circle icon-lg" style="color:rgb(<?php echo $r.', '.$g.', '.$b; ?>);"></i><br>
													<small><?php echo substr($col_name, 0, 10);?></small>
												</div>

												<p>Lipstick Effect - 
													<?php
													if( $lipefft == 0 ) { echo "Matte"; }
													else if ( $lipefft > 0 || $lipefft < 100 ) { echo "Satin"; }
													else if ( $lipefft == 100 ) { echo "Satin"; }
													?>
												</p>
											</div>
										<?php } ?>

										<?php if( $eyemkup != null ) { ?>
											<div class="text-center pt-2">
												<p>Eye Make Up</p>

												<?php
												if($eyemkup == 0) { $emuTxt = "natural";}
												else if ($eyemkup > 0) { $emuTxt = "medium";}
												else if ($eyemkup >= 80) { $emuTxt = "smoky";}
												?>

												<img class="p-1 shadow" src="<?php echo PIQUIC_URL;?>images/makeup/eye-<?php echo $emuTxt; ?>.png" style="width: 60%;">

												<p class="pt-3"><?php echo ucfirst($emuTxt); ?></p>
											</div>
										<?php } ?>

										<?php if( $eyeshcol != null && $eyeint != null ) { ?>
											<div class="text-center pt-2">
												<p>Eye Shadow Color</p>
												<?php
												$esqry = "SELECT * FROM `tbl_eyeshadow` WHERE `eyesh_code` LIKE '$eyeshcol' GROUP BY `eyesh_code`";
												

												$query=$second_DB->query($esqry);
												$ymrows=$query->result_array();
												$esrow=$ymrows[0];
												$eyesh_name = $esrow['eyesh_name'];

												$eshex = "#".$eyeshcol."";
												list($esr, $esg, $esb) = sscanf($eshex, "#%02x%02x%02x");

												?>
												<div class="p-1 mb-3 shadow rounded" style="width: 100%;" title="<?php echo $eyesh_name;?>">
													<i class="fas fa-circle icon-lg" style="color:rgb(<?php echo $esr.', '.$esg.', '.$esb; ?>);"></i><br>
													<small><?php echo substr($eyesh_name, 0, 10);?></small>
												</div>

												<p>Eyeshadow Intensity - <?php echo $eyeint; ?></p>
											</div>
										<?php } ?>

										<?php if( $blshcol != null && $blshint != null ) { ?>
											<div class="text-center pt-2">
												<p>Blush Color</p>
												<?php
												$blshqry = "SELECT * FROM `tbl_blush` WHERE `blu_code` LIKE '$blshcol' GROUP BY `blu_code`";
												
												$query=$second_DB->query($blshqry);
												$ymrows=$query->result_array();
												$blshrow=$ymrows[0];
												$blu_name = $blshrow['blu_name'];

												$blshhex = "#".$blshcol."";
												list($blshr, $blshg, $blshb) = sscanf($blshhex, "#%02x%02x%02x");

												?>
												<div class="p-1 mb-3 shadow rounded" style="width: 100%;" title="<?php echo $blu_name;?>">
													<i class="fas fa-circle icon-lg" style="color:rgb(<?php echo $blshr.', '.$blshg.', '.$blshb; ?>);"></i><br>
													<small><?php echo substr($blu_name, 0, 10);?></small>
												</div>

												<p>Blush Intensity - <?php echo $blshint; ?></p>
											</div>
										<?php } ?>
									</div>
								</div>
							</div>
						<?php } ?>
					</div>

					<hr class="w-75">


					
					<?php echo form_close();?> 
					 
					 
					 
				    </div>
					
					
					
					
				
					
				    </div>
					
					
					
					
				
		
					
					
					
				</div>
					
			</div>
		</div>
		
	</section>
</div>

<?php $this->load->view('admin/includes/globaljs');?>
<link type="text/css" href="<?php echo base_url();?>assets/form-select2/select2.css" rel="stylesheet"/>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootswatch/3.3.7/lumen/bootstrap.min.css">



<script type="text/javascript" src="<?php echo base_url();?>assets/form-select2/select2.min.js"></script>



<!--<script  type="text/javascript" src="<?php echo base_url();?>assets/dist/password-score.js"></script>
<script  type="text/javascript" src="<?php echo base_url();?>assets/dist/password-score-options.js"></script>
<script  type="text/javascript" src="<?php echo base_url();?>assets/dist/bootstrap-strength-meter.js"></script>-->

 <script>
 	function getCurrencySymbol()
 	{
 		var order_currency=$('#order_currency').val();
 		//alert(order_currency);
 		if(order_currency=='INR')
 		{
 			var order_currency_symbol='&#x20B9;';
 		}
 		else if(order_currency=='USD')
 		{
 			var order_currency_symbol='&#36;';
 		}
 		else if(order_currency=='EUR')
 		{
 			var order_currency_symbol='&#128;';
 		}
 		else if(order_currency=='GBP')
 		{
 			var order_currency_symbol='&#163;';
 		}
 		else if(order_currency=='AUD')
 		{
 			var order_currency_symbol='&#36;';
 		}
 		else if(order_currency=='CNY')
 		{
 			var order_currency_symbol='&#165;';
 		}

 		else
 		{
 			var order_currency_symbol='';
 		}
		var order_currency_symbol='';

		$('#order_currency_symbol').val(order_currency_symbol);
 	}
    
    
	jQuery(function ($) {
	   // $('.content-wrapper').css('min-height','900px');
	   //$('#entity_id').select2();
		
		
		$('#order_form').validate({
		
           rules: {
		
          
		
			plntyid: {
			required: true,
			},
			order_currency: {
			required: true,
			},
			order_per_image: {
			required: true,
			},
			/*order_currency_symbol: {
			required: true,
			},*/
			order_name: {
			required: true,
			},
			order_amount: {
			required: true,
			},
			order_credit: {
			required: true,
			},
			
			status: {
			required: true,
			}

			
		},
		 messages: {
		 
		  
		  
			
        },
				submitHandler: function(form) {
		
		       //alert("koko");
		
		        var orderid=$('#orderid').val();
				var order_for=$('#order_for').val();
				//alert(order_for);
			    var plntyid=$('#plntyid').val();
				var order_currency=$('#order_currency').val();
				var order_currency_symbol=$('#order_currency_symbol').val();
				var order_per_image=$('#order_per_image').val();
			    var order_name=$('#order_name').val();
			    var order_amount=$('#order_amount').val();
			    var order_credit=$('#order_credit').val();
			    var status=$('#status').val();
				
				//alert("<?php echo base_url();?>user/addUser?&orderid="+orderid+"&UserName="+UserName+"&UserComments="+UserComments+"&UserIsActive="+UserIsActive);	
					
				$.ajax({
				method:'post',
				url: "<?php echo base_url();?>admin/order/insert_order", 
				data:"&orderid="+orderid+"&order_for="+order_for+"&plntyid="+plntyid+"&order_currency="+order_currency+"&order_currency_symbol="+order_currency_symbol+"&order_per_image="+order_per_image+"&order_name="+order_name+"&order_amount="+order_amount+"&order_credit="+order_credit+"&status="+status,
				success: function(result){
				if(result)
				{
					//alert(result);

					//alert("koko");

					var val=$.trim(result);
					//alert(val);

					$("#success_message").show();
					setTimeout(function(){setInterval(function(){
					//alert("ppp");
					parent.$.fancybox.close();

					}, 3000)}, 3000);
					parent.$.fancybox.close();
					parent.$('#example1').dataTable().fnStandingRedraw();
					
					
				  
					
				
				  
				  
				 
				
				}
				}
				});
					
					
					
					
					
						
							
					
					   
		
		} 
		 
		
		});
		

		
		
	});	
</script>   
<script>
 $(document).ready(function() {
 //alert("popo");
	  /* $('#example-getting-started-input').strengthMeter('text', {
            container: $('#example-getting-started-text')
        });
        $('#example-tooltip').strengthMeter('tooltip');*/
		/*$('#user_passowrd').strengthMeter('progressBar', {
            container: $('#example-progress-bar-container')
        });*/
    });
</script>
<script type="text/javascript" src="<?php echo base_url();?>theme/assets/source/jquery.fancybox.js?v=2.1.5"></script>
<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>theme/assets/source/jquery.fancybox.css?v=2.1.5" media="screen" />				