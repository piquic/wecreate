<?php  $this->load->view('admin/includes/globalcss');?>


<style>
/*.table-bordered {
    border: 1px solid #666666;
}*/
.showdbutton {
    cursor:pointer; 
	background-color:#666666; 
	margin:10px 0px; 
	padding:10px; 
	color:#FFFFFF;
	width:100%;
}

.disabledbutton {
    pointer-events: none;
    opacity: 0.9;
	
	cursor:pointer; 
	background-color:#999999; 
	margin:10px 0px; 
	padding:10px; 
	color:#FFFFFF;
	width:100%;
}
label.error{
 color:#FF0000;
 font-size:12px;
}
</style>
<div>
  <!-- Section content header -->
 
  <!-- Section content -->
    <section class="content">
		<div class="col-md-12">
		  <!-- start Information Box -->
			<div class="box">
			<!-- box header style start-->
				<div class="col-md-12 box-header with-border" ><h3 class="box-title">View Upload Order</h3></div>
					<!-- /.box-header -->
					<!-- form start -->
					
					<div id="success_message" style="display:none;">
					<div class="alert alert-success">
					<strong>Updated successfully!</strong>.
					</div>
				    </div>
					
				
				<div class="box-body">
				
				
				   <div class="col-md-12">
						<ul class="nav nav-tabs">
							<li id="li_home" class="active"><a data-toggle="tab" href="#home">Upload Order Information</a></li>
							
							
						</ul>
					</div>
				
				
				<div class="addmargin10">&nbsp;</div>
					<div class="tab-content">
					
				     <div id="home" class="tab-pane fade in active">
					 
					 		    <?php  $array = array('id'=>'order_form', 'role'=>'form', 'class'=>'form form-horizontal has-validation-callback');
				    echo form_open_multipart('order/insert_order', $array); ?>
					
					
					<input type='hidden' name='orderid' value='<?php echo $orderid;?>' id='orderid'>
					
					<div class="form-group">
					    <label for="inputEmail3"  class="col-sm-2 control-label"><?php echo 'SKU No.';?>
                        </label>
						<div class="col-sm-8">
						   <?php echo $plntypname;?>
						</div>
						
					</div>
					
	              <div class="form-group">
					    <label for="inputEmail3"  class="col-sm-2 control-label"><?php echo 'Gender';?>
                        
                        </label>
						<div class="col-sm-8">
						  <?php echo $plntypname;?>
						</div>
						
					</div>

					<div class="form-group">
					    <label for="inputEmail3"  class="col-sm-2 control-label"><?php echo 'Category';?>
                        
                        </label>
						<div class="col-sm-8">
						  <?php echo $plntypname;?>
						</div>
						
					</div>



					
					<?php echo form_close();?> 
					 
					 
					 
				    </div>
					
					
					
					
				
					
				    </div>
					
					
					
					
				
		
					
					
					
				</div>
					
			</div>
		</div>
		
	</section>
</div>

<?php $this->load->view('admin/includes/globaljs');?>
<link type="text/css" href="<?php echo base_url();?>assets/form-select2/select2.css" rel="stylesheet"/>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootswatch/3.3.7/lumen/bootstrap.min.css">



<script type="text/javascript" src="<?php echo base_url();?>assets/form-select2/select2.min.js"></script>



<!--<script  type="text/javascript" src="<?php echo base_url();?>assets/dist/password-score.js"></script>
<script  type="text/javascript" src="<?php echo base_url();?>assets/dist/password-score-options.js"></script>
<script  type="text/javascript" src="<?php echo base_url();?>assets/dist/bootstrap-strength-meter.js"></script>-->

 <script>
 	function getCurrencySymbol()
 	{
 		var order_currency=$('#order_currency').val();
 		//alert(order_currency);
 		if(order_currency=='INR')
 		{
 			var order_currency_symbol='&#x20B9;';
 		}
 		else if(order_currency=='USD')
 		{
 			var order_currency_symbol='&#36;';
 		}
 		else if(order_currency=='EUR')
 		{
 			var order_currency_symbol='&#128;';
 		}
 		else if(order_currency=='GBP')
 		{
 			var order_currency_symbol='&#163;';
 		}
 		else if(order_currency=='AUD')
 		{
 			var order_currency_symbol='&#36;';
 		}
 		else if(order_currency=='CNY')
 		{
 			var order_currency_symbol='&#165;';
 		}

 		else
 		{
 			var order_currency_symbol='';
 		}
		var order_currency_symbol='';

		$('#order_currency_symbol').val(order_currency_symbol);
 	}
    
    
	jQuery(function ($) {
	   // $('.content-wrapper').css('min-height','900px');
	   //$('#entity_id').select2();
		
		
		$('#order_form').validate({
		
           rules: {
		
          
		
			plntyid: {
			required: true,
			},
			order_currency: {
			required: true,
			},
			order_per_image: {
			required: true,
			},
			/*order_currency_symbol: {
			required: true,
			},*/
			order_name: {
			required: true,
			},
			order_amount: {
			required: true,
			},
			order_credit: {
			required: true,
			},
			
			status: {
			required: true,
			}

			
		},
		 messages: {
		 
		  
		  
			
        },
				submitHandler: function(form) {
		
		       //alert("koko");
		
		        var orderid=$('#orderid').val();
				var order_for=$('#order_for').val();
				//alert(order_for);
			    var plntyid=$('#plntyid').val();
				var order_currency=$('#order_currency').val();
				var order_currency_symbol=$('#order_currency_symbol').val();
				var order_per_image=$('#order_per_image').val();
			    var order_name=$('#order_name').val();
			    var order_amount=$('#order_amount').val();
			    var order_credit=$('#order_credit').val();
			    var status=$('#status').val();
				
				//alert("<?php echo base_url();?>user/addUser?&orderid="+orderid+"&UserName="+UserName+"&UserComments="+UserComments+"&UserIsActive="+UserIsActive);	
					
				$.ajax({
				method:'post',
				url: "<?php echo base_url();?>admin/order/insert_order", 
				data:"&orderid="+orderid+"&order_for="+order_for+"&plntyid="+plntyid+"&order_currency="+order_currency+"&order_currency_symbol="+order_currency_symbol+"&order_per_image="+order_per_image+"&order_name="+order_name+"&order_amount="+order_amount+"&order_credit="+order_credit+"&status="+status,
				success: function(result){
				if(result)
				{
					//alert(result);

					//alert("koko");

					var val=$.trim(result);
					//alert(val);

					$("#success_message").show();
					setTimeout(function(){setInterval(function(){
					//alert("ppp");
					parent.$.fancybox.close();

					}, 3000)}, 3000);
					parent.$.fancybox.close();
					parent.$('#example1').dataTable().fnStandingRedraw();
					
					
				  
					
				
				  
				  
				 
				
				}
				}
				});
					
					
					
					
					
						
							
					
					   
		
		} 
		 
		
		});
		

		
		
	});	
</script>   
<script>
 $(document).ready(function() {
 //alert("popo");
	  /* $('#example-getting-started-input').strengthMeter('text', {
            container: $('#example-getting-started-text')
        });
        $('#example-tooltip').strengthMeter('tooltip');*/
		/*$('#user_passowrd').strengthMeter('progressBar', {
            container: $('#example-progress-bar-container')
        });*/
    });
</script>
<script type="text/javascript" src="<?php echo base_url();?>theme/assets/source/jquery.fancybox.js?v=2.1.5"></script>
<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>theme/assets/source/jquery.fancybox.css?v=2.1.5" media="screen" />				