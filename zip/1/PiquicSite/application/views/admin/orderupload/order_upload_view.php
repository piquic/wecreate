<?php  $this->load->view('admin/includes/globalcss');

$second_DB = $this->load->database('another_db', TRUE); 
?>
<link rel="stylesheet" href="<?php echo base_url(); ?>website-assets/css/bootstrap.min.css">

<style>
	.showdbutton {
    	cursor:pointer; 
    	background-color:#666666; 
    	margin:10px 0px; 
    	padding:10px; 
    	color:#FFFFFF;
    	width:100%;
    }

    .disabledbutton {
    	pointer-events: none;
    	opacity: 0.9;

    	cursor:pointer; 
    	background-color:#999999; 
    	margin:10px 0px; 
    	padding:10px; 
    	color:#FFFFFF;
    	width:100%;
    }
    label.error{
    	color:#FF0000;
    	font-size:12px;
    }
</style>

<div>
 	<!-- Section content header -->

 	<!-- Section content -->
 	<section class="content">
 		<div class="col-md-12">
 			<!-- start Information Box -->
 			<div class="box">
 				<!-- box header style start-->
 				<div class="col-md-12 box-header with-border" >
 					<h3 class="box-title p-1 pb-2">View Upload Order</h3>
 				</div>
 				<!-- /.box-header -->

 				<!-- form start -->

 				<div id="success_message" style="display:none;">
 					<div class="alert alert-success">
 						<strong>Updated successfully!</strong>.
 					</div>
 				</div>

 				<div class="box-body row">

 					<div class="col-md-12">
 						<ul class="nav nav-tabs">
 							<li id="li_home" class="active"><a data-toggle="tab" href="#home">Upload Order Information</a></li>
 						</ul>
 						<!-- </div> -->

 						<div class="addmargin10">&nbsp;</div>

 						<div class="tab-content">
 							<div id="home" class="tab-pane fade in active">
 								<?php 
 								$array = array('id'=>'order_form', 'role'=>'form', 'class'=>'form form-horizontal has-validation-callback'); 
 								echo form_open_multipart('order/insert_order', $array);
 								?>

 								<input type='hidden' name='orderid' value='<?php echo $orderid;?>' id='orderid'>

 								<div class="row">
 									<div class="col-md-4">
 										<div class="form-row px-3">
 											<label class="col-12 col-sm-6">
 												<p><b><?php echo 'SKU No. :';?></b></p>
 											</label>

 											<div class="col-12 col-sm-6">
 												<p><?php echo $skuno;?></p>
 											</div>
 										</div>
 									</div>

 									<div class="col-md-4">
 										<div class="form-row px-3">
 											<label class="col-12 col-sm-6">
 												<p><b><?php echo 'Gender :';?></b></p>
 											</label>

 											<div class="col-12 col-sm-6">
 												<p><?php 
 												if($gender=="1"){ echo "Male" ; } 
 												if($gender=="2"){ echo "Female" ; } 
 												if($gender=="4"){ echo "Boy" ; } 
 												if($gender=="6"){ echo "Girl" ; } 
 												?></p>
 											</div>
 										</div>
 									</div>

 									<div class="col-md-4">
 										<div class="form-row px-3">
 											<label class="col-12 col-sm-6">
 												<p><b><?php echo 'Category :';?></b></p>
 											</label>

 											<div class="col-12 col-sm-6">
 												<p><?php echo ucfirst($category);?></p>
 											</div>
 										</div>
 									</div>
 								</div>

 								<div class="row">
 									<div class="col-12 col-sm-12 col-md-12">
 										<div class="px-3">
 											<p><b>Assets:</b></p>

 											<div class="col-12 col-sm-12 col-md-12">
 												<div class="row">
 													<div class="col-12 col-sm-12 col-md-6">
 														<p><b>Background :</b> <?php echo str_replace(".jpg","", $bg); ?></p>
 													</div>

 													<?php
 													$mdlqry   = "SELECT * FROM `tbl_model` WHERE `m_id` = '$model_id'";
 													$query=$second_DB->query($mdlqry);
 													$ymrows=$query->result_array();
 													//print_r($ymrows);
 													if(!empty($ymrows)) {
	 													$mdlrow=$ymrows[0];
	 													$mod_name = $mdlrow['mod_name'];
	 													$mod_img  = $mdlrow['mod_img'];
 													}
 													else{
 														$mod_name ='';
	 													$mod_img  = '';
 													}
 													$mbqry      = "SELECT * FROM `tbl_model` WHERE `mod_name` = '$modbody'";
 													$query=$second_DB->query($mdlqry);
 													$ymrows=$query->result_array();
 													if(!empty($ymrows)) {
	 													$mbrow=$ymrows[0];
 														$mbmod_img  = $mbrow['mod_img'];
 													}
 													else{
 														$mbmod_img ='';
	 													
 													}
 													
 													?>
 													<div class="col-12 col-sm-12 col-md-6">
 														<p><b>Model Name :</b> <?php echo $mod_name; ?></p>
 													</div>

 													<div class="col-12 col-sm-12 col-md-6">
 														<p><b>Model Body :</b> <?php echo $modbody; ?></p>
 													</div>

 													<?php if($poseids != null){ ?>
 														<div class="col-12 col-sm-12 col-md-6">
 															<p>
 																<b>Pose(s) :</b>
 																<?php
 																$poseqry  = "SELECT * FROM `tbl_model_pose` WHERE `mpose_id` IN ($poseids);";
 																$pid = 1;
 																$query2=$second_DB->query($poseqry);
 																$modelinfo= $query2->result_array();
 																foreach($modelinfo as $poserow){
 																	$mpose_img = $poserow['mpose_img'];
 																	if($pid > 1){
 																		echo ", " . str_replace(".png","", $mpose_img);
 																	} else {
 																		echo str_replace(".png","", $mpose_img);
 																	}

 																	$pid++;
 																}?>
 															</p>
 														</div>
 													<?php } ?>

 													<?php if($top != 'null'){ ?>
 														<div class="col-12 col-sm-12 col-md-6">
 															<p><b>Top :</b> <?php echo str_replace(".png","", $top); ?></p>
 														</div>
 													<?php } ?>

 													<?php if($bottom != 'null'){ ?>
 														<div class="col-12 col-sm-12 col-md-6">
 															<p><b>Bottom :</b> <?php echo str_replace(".png","", $bottom); ?></p>
 														</div>
 													<?php } ?>

 													<div class="col-12 col-sm-12 col-md-6">
 														<p><b>Shoes :</b> <?php echo str_replace(".png","", $shoes); ?></p>
 													</div>

 													<?php if($accessories != ''){ ?>
 														<div class="col-12 col-sm-12 col-md-6">
 															<p>
 																<b>Accessories :</b> 
 																<?php
 																if(!empty($accessories)){
 																$accqry  = "SELECT * FROM `tbl_accessories` WHERE `acces_id` IN ($accessories);";

 																$pid = 1;
 																$query2=$second_DB->query($accqry);
 																$modelinfo= $query2->result_array();

 																foreach($modelinfo as $accrow){
 																	$acces_img = $accrow['acces_img'];
 																	if($pid > 1){
 																		echo ", " . str_replace(".png","", $acces_img);
 																	} else {
 																		echo str_replace(".png","", $acces_img);
 																	}
 																	$pid++;
 																}
 															}?>
 															</p>
 														</div>
 													<?php } ?>	
 												</div>

 												<div class="row">
 													<?php if( ($lipcol != null && $lipefft != null) || $eyemkup != null || ( $eyeshcol != null && $eyeint != null ) || (!empty($blshcol) && !empty($blshint))) { ?>
 														<div class="col-12 col-sm-12 col-md-12">
 															<p><b>Makeup :</b></p>
 														</div>

 														<?php
 														if($lipcol != null && $lipefft != null) { 
 															$lipqry = "SELECT * FROM `tbl_lipcol` WHERE `col_code` LIKE '$lipcol' GROUP BY `col_code`";

 															$query=$second_DB->query($lipqry);
 															$ymrows=$query->result_array();
 															$liprow=$ymrows[0];
 															$col_name = $liprow['col_name'];

 															$hex = "#".$lipcol."";
 															list($r, $g, $b) = sscanf($hex, "#%02x%02x%02x");
 															$r += 82;
 															$g += 23;
 															$b += 15;
 															?>
 															<div class="col-12 col-sm-12 col-md-6">
 																<p><b>Lipstick Color :</b> <i class="fas fa-circle" style="color:rgb(<?php echo $r.', '.$g.', '.$b; ?>);"></i>&nbsp;<?php echo substr($col_name, 0, 10);?></p>
 															</div>

 															<div class="col-12 col-sm-12 col-md-6">
 																<p>
 																	<b>Lipstick Effect :</b> 
 																	<?php
 																	if( $lipefft == 0 ) { echo "Matte"; }
 																	else if ( $lipefft > 0 || $lipefft < 100 ) { echo "Satin"; }
 																	else if ( $lipefft == 100 ) { echo "Glossy"; }
 																	?>
 																</p>
 															</div>
 														<?php } ?>

 														<?php
 														if( $eyeshcol != null && $eyeint != null ) {

 															$esqry = "SELECT * FROM `tbl_eyeshadow` WHERE `eyesh_code` LIKE '$eyeshcol' GROUP BY `eyesh_code`";

 															$query=$second_DB->query($esqry);
 															$ymrows=$query->result_array();
 															$esrow=$ymrows[0];
 															$eyesh_name = $esrow['eyesh_name'];

 															$eshex = "#".$eyeshcol."";
 															list($esr, $esg, $esb) = sscanf($eshex, "#%02x%02x%02x");
 															?>

 															<div class="col-12 col-sm-12 col-md-6">
 																<p><b>Eye Shadow Color :</b> 
 																	<i class="fas fa-circle icon-lg" style="color:rgb(<?php echo $esr.', '.$esg.', '.$esb; ?>);"></i>&nbsp;<?php echo substr($eyesh_name, 0, 10);?>
 																</p>
 															</div>

 															<div class="col-12 col-sm-12 col-md-6">
 																<p><b>Eyeshadow Intensity :</b> <?php echo $eyeint; ?></p>
 															</div>
 														<?php } ?>

 														<?php if( $eyemkup != null ) { ?>
 															<div class="col-12 col-sm-12 col-md-6">
 																<p><b>Eye Make Up :</b> 
 																	<?php
 																	if($eyemkup == 0) { echo "Natural";}
 																	else if ($eyemkup > 0) { echo "Medium";}
 																	else if ($eyemkup >= 80) { echo "Smoky";}
 																	?>
 																</p>
 															</div>
 															<div class="col-12 col-sm-12 col-md-6">&nbsp;</div>
 														<?php } ?>

 														<?php
 														if( $blshcol != null && $blshint != null ) {

 															$blshqry = "SELECT * FROM `tbl_blush` WHERE `blu_code` LIKE '$blshcol' GROUP BY `blu_code`";

 															$query=$second_DB->query($blshqry);
 															$ymrows=$query->result_array();
 															$blshrow=$ymrows[0];
 															$blu_name = $blshrow['blu_name'];

 															$blshhex = "#".$blshcol."";
 															list($blshr, $blshg, $blshb) = sscanf($blshhex, "#%02x%02x%02x");
 															?>
 															<div class="col-12 col-sm-12 col-md-6">
 																<p>
 																	<b>Blush Color :</b> 
 																	<i class="fas fa-circle icon-lg" style="color:rgb(<?php echo $blshr.', '.$blshg.', '.$blshb; ?>);"></i>&nbsp;<?php echo substr($blu_name, 0, 10);?>
 																</p>
 															</div>

 															<div class="col-12 col-sm-12 col-md-6">
 																<p><b>Blush Intensity :</b> <?php echo $blshint; ?></p>
 															</div>
 														<?php } ?>
 													<?php } ?>
 												</div>
 											</div>
 										</div>
 									</div>
 								</div>

 								<hr class="w-75 text-center">
 								<div class="row">
 									<div class="col-12 col-sm-12">
 										<div class="px-3">
 											<p><b>Preview :</b></p>
 										</div>
 									</div>

 									<style type="text/css">
 										.bmw { width: 72.66%; }
 										.pmw { width: 100%; }

 										@media (min-width: 1024px) {
 											.accpbg { width: 4em; height: 5.5em; top: 0em; right: 7.7em; background-repeat: no-repeat !important; background-position: top right !important; background-size: 24em !important; }
 										}
 										@media (min-width: 768px) and (max-width: 1023px) {
 											.accpbg { width: 5em; height: 6em; top: 0em; right: 7.7em; background-repeat: no-repeat !important; background-position: top right !important; background-size: 23.1em !important; }
 										}

 										#bg, #shdw, #pose, #head, #top, #bottom, #dress, #access, #foot { position: absolute; }
 										#bg { z-index: 995; }
 										#shdw { z-index: 996; }
 										#pose { z-index: 997; }
 										#top { z-index: 1007; }
 										#bottom { z-index: 1006; }
 										#dress { z-index: 1007; }
 										#access { z-index: 1005; }
 										#foot { z-index: 998; }

 										#lipstick, #eyeshd, #eyemkp, #blush { position: absolute; }
 										#lipstick { z-index: 998; }
 										#eyeshd { z-index: 998; }
 										#eyemkp { z-index: 998; }
 										#blush { z-index: 998; }

 										#accbr, #accer, #accnc, #accrn { position: absolute; }
 										#accbr { z-index: 1001; }
 										#accer { z-index: 1002; }
 										#accnc { z-index: 1003; }
 										#accrn { z-index: 1004; }
 									</style>

 									<?php
 									// $mdlqry = "SELECT * FROM `tbl_model` WHERE `m_id` = '$model_id'";
 									// $query=$second_DB->query($mdlqry);
 									// $ymrows=$query->result_array();
 									// $mdlrow=$ymrows[0];
 									// $mod_name = $mdlrow['mod_name'];
 									// $mod_img = $mdlrow['mod_img'];

 									$mpqry = "SELECT * FROM `tbl_model_pose` WHERE `m_id` = '$model_id' AND `mpose_img` LIKE '%MSTR%'";
 									$mpres = $second_DB->query($mpqry);
 									$mprow = $mpres->result_array();
 									if(!empty($mprow)) {
										$mpose_img = $mprow[0]['mpose_img'];
									}
									else{
										$mpose_img ='';
										
									}
 								
 									if(!empty($accessories)){

 									$acqry   = "SELECT * FROM `tbl_accessories` WHERE `acces_id` IN ($accessories)";
 									$acres   = $second_DB->query($acqry);
 									$ac_cnt  = $acres->num_rows;
 									$ac_info = $acres->result_array();

 									$acc[] = '';
 									if ($ac_cnt > 0){
 										$pid = 1;
 										foreach($ac_info as $acrow){
											$acces_img = $acrow['acces_img'];
											if($pid > 1){
												// echo ", " . str_replace(".png","", $acces_img);
												$acc[] = $acces_img;
											} else {
												// echo str_replace(".png","", $acces_img);
												$acc[] = $acces_img;
											}
											$pid++;
										}
 									}

 									$bracelet = '';
 									$earring = '';
 									$necklace = '';
 									$ring = '';

 									foreach ($acc as $val) {
 										if(strpos($val, "-ACCBR")) {
 											$bracelet = $val;
 										}
 										if(strpos($val, "-ACCER")) {
 											$earring = $val;
 										}
 										if(strpos($val, "-ACCNC")) {
 											$necklace = $val;
 										}
 										if(strpos($val, "-ACCRNG")) {
 											$ring = $val;
 										}
 									}
 								}
 									if($lipcol != '' || $lipefft != '') {
 										if($lipefft >= '0' && $lipefft < '35') {
 											$lipefft = "MATTE";
 										} else if($lipefft >= '35' && $lipefft <= '65') {
 											$lipefft = "SATIN";
 										} else if($lipefft > '65' && $lipefft <= '100') {
 											$lipefft = "GLOSSY";
 										}
 										$lipstkimg   = $mod_name."-MSTR-".$lipcol."-".$lipefft.".png";
 									}

 									if($eyeshcol != ''){
 										$eyeshdimg   = $mod_name."-MSTR-".$eyeshcol.".png";
 									}

 									if($eyemkup != '') {
 										if($eyemkup >= '0') {
 											$eyemkup = "NATURAL";
 										} else if($eyemkup >= '40') {
 											$eyemkup = "MEDIUM";
 										} else if($eyemkup >= '80') {
 											$eyemkup = "SMOKY";
 										}
 										$eyemkpimg   = $mod_name."-MSTR-".$eyemkup.".png";
 									}

 									if($blshcol != ''){
 										$blushimg   = $mod_name."-MSTR-".$blshcol.".png";
 									}
 									//print_r($cutout_manu."---".$cutout_flag);
 									//print_r($bottom);
 									if($cutout_manu == '1'){
	 									$pcqry       = "SELECT * FROM `tbl_upload_img` WHERE `zipfile_name` = '$skuno'";
	 									$pcres       = $this->db->query($pcqry);
	 									$pcrow       = $pcres->result_array();

	 									$imgdirname = PATH_CUTOUTPNG . $skuno."\\";
	 									$images = glob($imgdirname."*.{png,PNG}",GLOB_BRACE);
	 									// print_r($images);

	 									foreach ($images as $image) {
	 										// echo $modbody;
	 										if(strpos($image, "-".strtoupper($modbody)."-")){
	 											$src_img = $image;
	 										}
	 									}

	 									// echo $src_img;
	 									$arr=explode("/",$src_img);
	 									$arrval= end($arr);

	 									$src_img = file_get_contents($arrval);
	 									$image_codes = base64_encode($src_img);
	 								}
 									?>

 									<div class="col-12 col-sm-12 col-md-3">&nbsp;</div>

 									<div class="col-12 col-sm-12 col-md-8">
 										<div class="px-3" style="height: 38em;">
 											<div id="bg" class="bmw">
 												<img id="bg-big" class="pmw <?php if($bg=="bg-white.jpg"){ echo 'shadow'; } ?>" src="<?php echo PIQUIC_URL;?>images/bg/<?php echo $bg;?>">
 											</div>

 											<div id="pose" class="bmw text-center">
 												<img id="pose-big" class="pmw" src="<?php echo PIQUIC_URL;?>images/model_pose/<?php echo $mpose_img;?>">
 											</div>

 											<div id="top" class="bmw">
 												<?php if($category == "bottom" && ($top != "cutoutimg.png" || $top != null)) { ?>
 													<img class="pmw" src="<?php echo PIQUIC_URL;?>images/top/<?php echo $top;?>">
 												<?php } else if($category == "top" && $cutout_flag == 1 && $cutout_manu == 0) { ?>
 													<div style="background-image: url(<?php echo base_url();?>upload/<?php echo $user_id;?>/<?php echo $skuno;?>/cutoutimg.png); height: 20em; background-repeat: no-repeat; background-size: calc(80px + <?php echo $source_w; ?>px) calc(121px + <?php echo $source_h; ?>px); background-position: calc(1px + <?php echo $source_x;?>px) calc(9px + <?php echo $source_y;?>px); transform: rotate(calc(2deg + <?php echo $source_angle;?>deg));">&nbsp;</div>
 												<?php } else if($category == "top" && $cutout_manu == 1) { ?>
 													<img id="top-big" class="pmw" src="data:image/jpg;charset=utf-8;base64,<?php echo $image_codes;?>" >
 												<?php } ?>
 											</div>

 											<div id="bottom" class="bmw">
 												<?php if($category == "top" && ($bottom != "cutoutimg.png" || $bottom != null)) { ?>
 													<img class="pmw" src="<?php echo PIQUIC_URL;?>images/bottom/<?php echo $bottom;?>">
 												<?php } else if($category == "bottom" && $cutout_flag == 1 && $cutout_manu == 0) { ?>
 													<div style="background-image: url(<?php echo PIQUIC_SITE_URL;?>upload/<?php echo $user_id;?>/<?php echo $skuno;?>/cutoutimg.png); height: 35em; background-repeat: no-repeat; background-size: calc(72px + <?php echo $source_w; ?>px) calc(70px + <?php echo $source_h; ?>px); background-position: calc(10*<?php echo $source_x;?>px) calc(5*<?php echo $source_y;?>px); transform: rotate(<?php echo $source_angle;?>deg);">&nbsp;</div>
 												<?php } else if($category == "bottom" && $cutout_manu == 1) { ?>
 													<img id="bottom-big" class="pmw" src="data:image/jpg;charset=utf-8;base64,<?php echo $image_codes;?>">
 												<?php } ?>
 											</div>

 											<?php if($category == "dress") { ?>
 												<div id="dress" class="bmw">
 													<?php if($cutout_flag == 1 && $cutout_manu == 0){ ?>
 														<img id="dress-big" class="pmw" src="upload/<?php echo $user_id;?>/<?php echo $skuno;?>/cutoutimg.png">
 													<?php } else if($cutout_manu == 1){ ?>
 														<img id="dress-big" class="pmw" src="data:image/jpg;charset=utf-8;base64,<?php echo $image_codes;?>">
 													<?php } ?>
 												</div>
 											<?php } ?>

 											<?php if($accessories) { ?>
 												<?php if($ring || $earring || $necklace || $bracelet) { ?>
 													<div id="access" class="accpbg" style="background: url(<?php echo PIQUIC_URL;?>images/bg/<?php echo $bg;?>);"></div>
 												<?php } ?>

 												<?php if($ring) { ?>
 													<div id="accrn" class="bmw">
 														<img id="accrn-big" class="pmw" src="<?php echo PIQUIC_URL;?>images/accessories/<?php echo $ring;?>">
 													</div>
 												<?php } ?>

 												<?php if($earring) { ?>
 													<div id="accer" class="bmw">
 														<img id="accer-big" class="pmw" src="<?php echo PIQUIC_URL;?>images/accessories/<?php echo $earring;?>">
 													</div>
 												<?php } ?>

 												<?php if($necklace) { ?>
 													<div id="accnc" class="bmw">
 														<img id="accnc-big" class="pmw" src="<?php echo PIQUIC_URL;?>images/accessories/<?php echo $necklace;?>">
 													</div>
 												<?php } ?>

 												<?php if($bracelet) { ?>
 													<div id="accbr" class="bmw">
 														<img id="accbr-big" class="pmw" src="<?php echo PIQUIC_URL;?>images/accessories/<?php echo $bracelet;?>">
 													</div>
 												<?php } ?>
 											<?php } ?>
 											<?php if(!empty($shoes)){
 												?>
	 											<div id="foot" class="bmw">
	 												<img id="foot-big" class="pmw" src="<?php echo PIQUIC_URL;?>images/foot/<?php echo $shoes;?>">
	 											</div>
	 											<?php
 											} ?>
 											<?php if($lipcol != '' || $lipefft != '') { ?>
 												<div id="lipstick" class="bmw">
 													<img id="lipstick-big" class="pmw" src="<?php echo PIQUIC_URL;?>images/lipstick/<?php echo $lipstkimg;?>">
 												</div>
 											<?php } ?>

 											<?php if($eyeshcol != ''){ ?>
 												<div id="eyeshd" class="bmw">
 													<img id="eyeshd-big" class="pmw" style="opacity: <?php echo $eyeint?>%;" src="<?php echo PIQUIC_URL;?>images/eyeshadow/<?php echo $eyeshdimg;?>">
 												</div>
 											<?php } ?>

 											<?php if($eyemkup != '') { ?>
 												<div id="eyemkp" class="bmw">
 													<img id="eyemkp-big" class="pmw" src="<?php echo PIQUIC_URL;?>images/eyemakeup/<?php echo $eyemkpimg;?>">
 												</div>
 											<?php } ?>

 											<?php if($blshcol != ''){ ?>
 												<div id="blush" class="bmw">
 													<img id="blush-big" class="pmw" style="opacity: <?php echo $blshint?>%;" src="<?php echo PIQUIC_URL;?>images/blush/<?php echo $blushimg;?>">
 												</div>
 											<?php } ?>
 										</div>
 									</div>

 								</div>
 								<?php echo form_close();?> 
 							</div>
 						</div>
 					</div>
 				</div>
 			</div>
 		</div>
 	</section>
</div>

 <?php $this->load->view('admin/includes/globaljs');?>
 <link type="text/css" href="<?php echo base_url();?>assets/form-select2/select2.css" rel="stylesheet"/>
 <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootswatch/3.3.7/lumen/bootstrap.min.css">



 <script type="text/javascript" src="<?php echo base_url();?>assets/form-select2/select2.min.js"></script>



<!--<script  type="text/javascript" src="<?php echo base_url();?>assets/dist/password-score.js"></script>
<script  type="text/javascript" src="<?php echo base_url();?>assets/dist/password-score-options.js"></script>
<script  type="text/javascript" src="<?php echo base_url();?>assets/dist/bootstrap-strength-meter.js"></script>-->

<script>
	function getCurrencySymbol()
	{
		var order_currency=$('#order_currency').val();
 		//alert(order_currency);
 		if(order_currency=='INR')
 		{
 			var order_currency_symbol='&#x20B9;';
 		}
 		else if(order_currency=='USD')
 		{
 			var order_currency_symbol='&#36;';
 		}
 		else if(order_currency=='EUR')
 		{
 			var order_currency_symbol='&#128;';
 		}
 		else if(order_currency=='GBP')
 		{
 			var order_currency_symbol='&#163;';
 		}
 		else if(order_currency=='AUD')
 		{
 			var order_currency_symbol='&#36;';
 		}
 		else if(order_currency=='CNY')
 		{
 			var order_currency_symbol='&#165;';
 		}

 		else
 		{
 			var order_currency_symbol='';
 		}
 		var order_currency_symbol='';

 		$('#order_currency_symbol').val(order_currency_symbol);
 	}


 	jQuery(function ($) {
	   // $('.content-wrapper').css('min-height','900px');
	   //$('#entity_id').select2();


	   $('#order_form').validate({

	   	rules: {



	   		plntyid: {
	   			required: true,
	   		},
	   		order_currency: {
	   			required: true,
	   		},
	   		order_per_image: {
	   			required: true,
	   		},
			/*order_currency_symbol: {
			required: true,
		},*/
		order_name: {
			required: true,
		},
		order_amount: {
			required: true,
		},
		order_credit: {
			required: true,
		},

		status: {
			required: true,
		}


	},
	messages: {




	},
	submitHandler: function(form) {
		
		       //alert("koko");

		       var orderid = $('#orderid').val();
		       var order_for=$('#order_for').val();
				//alert(order_for);
				var plntyid=$('#plntyid').val();
				var order_currency=$('#order_currency').val();
				var order_currency_symbol=$('#order_currency_symbol').val();
				var order_per_image=$('#order_per_image').val();
				var order_name=$('#order_name').val();
				var order_amount=$('#order_amount').val();
				var order_credit=$('#order_credit').val();
				var status=$('#status').val();
				
				//alert("<?php echo base_url();?>user/addUser?&orderid="+orderid+"&UserName="+UserName+"&UserComments="+UserComments+"&UserIsActive="+UserIsActive);	

				$.ajax({
					method:'post',
					url: "<?php echo base_url();?>admin/order/insert_order", 
					data:"&orderid="+orderid+"&order_for="+order_for+"&plntyid="+plntyid+"&order_currency="+order_currency+"&order_currency_symbol="+order_currency_symbol+"&order_per_image="+order_per_image+"&order_name="+order_name+"&order_amount="+order_amount+"&order_credit="+order_credit+"&status="+status,
					success: function(result){
						if(result)
						{
					//alert(result);

					//alert("koko");

					var val=$.trim(result);
					//alert(val);

					$("#success_message").show();
					setTimeout(function(){setInterval(function(){
					//alert("ppp");
					parent.$.fancybox.close();

				}, 3000)}, 3000);
					parent.$.fancybox.close();
					parent.$('#example1').dataTable().fnStandingRedraw();
					
					

					





				}
			}
		});










			} 


		});




	});	
</script>   
<script>
	$(document).ready(function() {
 //alert("popo");
	  /* $('#example-getting-started-input').strengthMeter('text', {
            container: $('#example-getting-started-text')
        });
        $('#example-tooltip').strengthMeter('tooltip');*/
		/*$('#user_passowrd').strengthMeter('progressBar', {
            container: $('#example-progress-bar-container')
         });*/
      });
   </script>
   <script type="text/javascript" src="<?php echo base_url();?>theme/assets/source/jquery.fancybox.js?v=2.1.5"></script>
   <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>theme/assets/source/jquery.fancybox.css?v=2.1.5" media="screen" />				