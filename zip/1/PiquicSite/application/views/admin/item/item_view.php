<?php  $this->load->view('admin/includes/globalcss');



?>


<style>
/*.table-bordered {
    border: 1px solid #666666;
}*/
.showdbutton {
    cursor:pointer; 
	background-color:#666666; 
	margin:10px 0px; 
	padding:10px; 
	color:#FFFFFF;
	width:100%;
}

.disabledbutton {
    pointer-events: none;
    opacity: 0.9;
	
	cursor:pointer; 
	background-color:#999999; 
	margin:10px 0px; 
	padding:10px; 
	color:#FFFFFF;
	width:100%;
}
label.error{
 color:#FF0000;
 font-size:12px;
}
</style>
<style>
/*.table-bordered {
    border: 1px solid #666666;
}*/
.upload_files {
height:20px;
z-index:999;

}
.disabledbutton {
    pointer-events: none;
    opacity: 0.9;
}


.fileContainer {
    overflow: hidden;
    position: relative;
}

.fileContainer [type=file] {
    cursor: inherit;
    display: block;
   /* font-size: 999px;*/
    filter: alpha(opacity=0);
    min-height: 100%;
    min-width: 100%;
    opacity: 0;
    /*position: absolute;*/
    right: 0;
    text-align: right;
    top: 0;
	height: 8px;
    margin-bottom: 0px;
}

/* Example stylistic flourishes */

.fileContainer span{
    /*background:#CDD1D1;*/
    border-radius: .5em;
    float: left;
    padding: .5em;
	/*color:#333333;*/
}

.fileContainer_span{
    background:#CDD1D1;
    border-radius: .5em;
    float: left;
    padding: .5em;
	color:#333333;
	cursor:pointer;
}

.fileContainer [type=file] {
    cursor: pointer;
}
.item_image_remove{
cursor:pointer;

}
.remove {
cursor:pointer;

}
.only_view_css{
pointer-events: none;
border: none;
border-color: transparent;
border-style : hidden;

}
</style>
<div>
  <!-- Section content header -->
 
  <!-- Section content -->
    <section class="content">
		<div class="col-md-12">
		  <!-- start Information Box -->
			<div class="box">
			
			<div id="success_message" style="display:none;">
					<div class="alert alert-success">
					<strong>Updated successfully!.</strong>
					</div>
				    </div>
			<!-- box header style start-->
				<div class="col-md-12 box-header with-border" ><h3 class="box-title"><?php if($item_id==''){ echo $lang['create_item']; }else {echo $lang['view_item']; }?></h3></div>
					<!-- /.box-header -->
					<!-- form start -->
					
					
					
				
				<div class="box-body">
				
				
				   <div class="col-md-12">
						<ul class="nav nav-tabs">
							<li id="li_home" class="active"><a data-toggle="tab" href="#home">Item Information</a></li>
							
							
						</ul>
					</div>
				
				
				<div class="addmargin10">&nbsp;</div>
					<div class="tab-content">
					
				     <div id="home" class="tab-pane fade in active">
					 
					<?php  $array = array('id'=>'item_form', 'role'=>'form', 'class'=>'form form-horizontal has-validation-callback');
				    echo form_open_multipart('item/insert_item', $array); ?>
					
					
					<input type='hidden' name='item_id' value='<?php echo $item_id;?>' id='item_id'>
					
					
					
					
					
					<div class="form-group">
					    <label for="inputEmail3"  class="col-sm-2 control-label"><?php echo $lang['item_menu'];?></label>
						<div class="col-sm-8">
						  
							<select  class="form-control only_view_css" id="menu_id" name="menu_id">
									<option value="" >Select Menu</option>
							<?php   $sql = "select * from `menu`  where 1=1 ";
									$mainquery = $this->db->query($sql);
									$arrayquery = $mainquery->result();
									foreach ($arrayquery as $row) {
									$sel_menu_id = $row->menu_id;
									$menu_name = $row->menu_name;
									$selected="";
									
										if($menu_id==$sel_menu_id)
										{
										$selected="selected";
										}
										else
										{
										$selected="";
										}
												
									
									?>
									<option value="<?php echo $sel_menu_id;?>" <?php echo $selected;?>><?php echo $menu_name;?></option>
									<?php
										}
									?>
								</select>
							 <?php echo form_error('menu_id', '<div class="error" style="color:red;">', '</div>'); ?>
							
							
							
							
							
						</div>
						
					</div>
					
					
					<!--<div class="form-group">
					    <label for="inputEmail3"  class="col-sm-2 control-label"><?php echo $lang['item_category'];?></label>
						<div class="col-sm-8">
						  
							<select  class="form-control only_view_css" id="category_id" name="category_id">
									<option value="" >Select Category</option>
							<?php   $sql = "select * from `category`  where 1=1 ";
									$mainquery = $this->db->query($sql);
									$arrayquery = $mainquery->result();
									foreach ($arrayquery as $row) {
									$sel_category_id = $row->category_id;
									$category_level3 = $row->category_level3;
									$selected="";
									
										if($category_id==$sel_category_id)
										{
										$selected="selected";
										}
										else
										{
										$selected="";
										}
												
									
									?>
									<option value="<?php echo $sel_category_id;?>" <?php echo $selected;?>><?php echo $category_level3;?></option>
									<?php
										}
									?>
								</select>
							 <?php echo form_error('category_id', '<div class="error" style="color:red;">', '</div>'); ?>
							
							
							
							
							
						</div>
						
					</div>-->
					
					
					
					<div class="form-group">
					    <label for="inputEmail3"  class="col-sm-2 control-label"><?php echo $lang['item_extra'];?></label>
						<div class="col-sm-8  only_view_css">
						
						<select name="extra_id[]" id="extra_id" class="populate  only_view_css"  style="width:100% !important" multiple="multiple">
						  
						
									
							<?php   $sql = "select * from `extras`  where 1=1 and status='1' and deleted='0'";
									$mainquery = $this->db->query($sql);
									$arrayquery = $mainquery->result();
									foreach ($arrayquery as $row) {
									$extra_id = $row->extra_id;
									$extra_name = $row->extra_name;
									$selected="";
									
										if(in_array($extra_id,$extra_id_sel))
                                        {
											$selected="selected";
										} else { $selected=""; }
												
									
									?>
									<option value="<?php echo $extra_id;?>" <?php echo $selected;?>><?php echo $extra_name;?></option>
									<?php
										}
									?>
								</select>
							 <?php echo form_error('extra_id', '<div class="error" style="color:red;">', '</div>'); ?>
							
							
							
							
							
						</div>
						
					</div>
					
					
					
					
					
					<div class="form-group">
					    <label for="inputEmail3"  class="col-sm-2 control-label"><?php echo $lang['item_option'];?></label>
						<div class="col-sm-8  only_view_css">
						
						<select name="option_id[]" id="option_id" class="populate  only_view_css"  style="width:100% !important" multiple="multiple">
						  
						
									
							<?php   $sql = "select * from `options`  where 1=1 and status='1' and deleted='0' ";
									$mainquery = $this->db->query($sql);
									$arrayquery = $mainquery->result();
									foreach ($arrayquery as $row) {
									$option_id = $row->option_id;
									$option_name = $row->option_name;
									$selected="";
									
										if(in_array($option_id,$option_id_sel))
                                        {
											$selected="selected";
										} else { $selected=""; }
												
									
									?>
									<option value="<?php echo $option_id;?>" <?php echo $selected;?>><?php echo $option_name;?></option>
									<?php
										}
									?>
								</select>
							 <?php echo form_error('option_id', '<div class="error" style="color:red;">', '</div>'); ?>
							
							
							
							
							
						</div>
						
					</div>
					
					
					
					<div class="form-group">
					    <label for="inputEmail3"  class="col-sm-2 control-label"><?php echo $lang['item_name'];?></label>
						<div class="col-sm-8">
						    <input  type='text'  class="form-control only_view_css" id="item_name" name="item_name" value='<?php echo $item_name;?>'>
                            <?php echo form_error('item_name', '<div class="error" style="color:red;">', '</div>'); ?>
						</div>
						
					</div>
					
					
			
					
					
					<div class="form-group">
								<label for="inputEmail3"  class="col-sm-2 control-label"><?php echo $lang['item_description'];?></label>
								<div class="col-sm-8">
								<textarea  class="form-control only_view_css" id="item_description" name="item_description"><?php echo $item_description; ?></textarea>
									
									<?php echo form_error('item_description', '<div class="error" style="color:red;">', '</div>'); ?>
								</div>
					</div>
					
					
					
					
					
					
					
					<div class="form-group">
					    <label for="inputEmail3"  class="col-sm-2 control-label"><?php echo $lang['item_price'];?></label>
						<div class="col-sm-8">
						    <input  type='text'  class="form-control only_view_css" id="item_price" name="item_price" value='<?php echo $item_price;?>'>
                            <?php echo form_error('item_price', '<div class="error" style="color:red;">', '</div>'); ?>
						</div>
						
					</div>
					
					<div class="form-group">
					    <label for="inputEmail3"  class="col-sm-2 control-label">Discount(%)</label>
						<div class="col-sm-8">
						    <input  type='text'  class="form-control only_view_css" id="item_price" name="discount_amount" value='<?php echo $discount_amount;?>'>
                            <?php echo form_error('discount_amount', '<div class="error" style="color:red;">', '</div>'); ?>
						</div>
						
					</div>
					<div class="form-group">
					    <label for="inputEmail3"  class="col-sm-2 control-label">Discount Start Date</label>
						<div class="col-sm-8">
						    <input  type='text'  class="form-control only_view_css" id="start_date" name="start_date" value='<?php echo $start_date;?>'>
                            <?php echo form_error('start_date', '<div class="error" style="color:red;">', '</div>'); ?>
						</div>
						
					</div>
					<div class="form-group">
					    <label for="inputEmail3"  class="col-sm-2 control-label">Discount End Date</label>
						<div class="col-sm-8">
						    <input  type='text'  class="form-control only_view_css" id="end_date" name="end_date" value='<?php echo $end_date;?>'>
                            <?php echo form_error('end_date', '<div class="error" style="color:red;">', '</div>'); ?>
						</div>
						
					</div>
					
					
					<div class="form-group">
					    <label for="inputEmail3"  class="col-sm-2 control-label"><?php echo $lang['item_segment'];?></label>
						<div class="col-sm-8">
						
						<select name="segment_id" id="segment_id" class="form-control only_view_css" >
						 <option value=''>Select Segment</option>
						  <?php   $sql = "select * from `segments`  where 1=1 ";
									$mainquery = $this->db->query($sql);
									$arrayquery = $mainquery->result();
									foreach ($arrayquery as $row) {
									$segment_id = $row->segment_id;
									$segment_name = $row->segment_name;
									$selected="";
									
										if($segment_id==$segment_id_sel)
                                        {
											$selected="selected";
										} else { $selected=""; }
												
									
									?>
									<option value="<?php echo $segment_id;?>" <?php echo $selected;?>><?php echo $segment_name;?></option>
									<?php
										}
									?>
						</select>
							 <?php echo form_error('segment_id', '<div class="error" style="color:red;">', '</div>'); ?>
							
							
							
							
							
						</div>
						
					</div>
					
					
					
					
					
					
					
					
				<div id="segmant_detail" >
				
				<input  type='hidden'  class="form-control only_view_css" id="level1segment_price" name="level1segment_price" value='0'>
				<input  type='hidden'  class="form-control only_view_css" id="level2segment_price" name="level2segment_price" value='0'>
				<input  type='hidden'  class="form-control only_view_css" id="level3segment_price" name="level3segment_price" value='0'>
				
				</div>
				
					
					
					
					<div class="form-group">
					<label for="inputEmail3"  class="col-sm-2 control-label"><?php echo $lang['item_status'];?></label>
					<div class="col-sm-8">
				
				
					<select name="item_status" id="item_status" class="form-control only_view_css" >
					 <option value=''>Select Status</option>
					 <option value="Active" <?php if($item_status=='Active') { ?> selected="selected" <?php } ?>  >Active</option>
					 <option value="Temporary Unavailable" <?php if($item_status=='Temporary Unavailable') { ?> selected="selected" <?php } ?>   >Temporary Unavailable</option>
					 </select>
					  <?php echo form_error('item_status', '<div class="error" style="color:red;">', '</div>'); ?>
					</div>
					</div>
					
					
					
					
					
					
				
					
					
					<div class="form-group">
					    <label for="inputEmail3"  class="col-sm-2 control-label"><?php echo $lang['item_image1'];?></label>
						<div class="col-sm-8">
						
						
						<!--<label class="fileContainer">
						<span class="fileContainer_span">Browse</span>
						<input type="file" class="upload_files fileInput css_uploadFile" alt="1"  id="files1" name="files[]" multiple />
						</label>-->
					    
						<input  type='hidden'  class="form-control only_view_css" id="item_image1" name="item_image1" value='<?php echo $item_image1;?>'>
						<div id="show_item_image1">
						<?php
						if($item_image1!='' && $item_image1!='No')
						{
						?>
						<span class="pip">
						<img src="<?php echo base_url(); ?>/upload_item_image/<?php echo $item_image1; ?>.png"  height='50' width='50'><br/>
						<!--<span class='item_image_remove1 remove remove' id="1" value='<?php echo $item_image1; ?>'>Remove</span>-->
						</span>
						<?php
						}
						?>
						</div>
                        <?php echo form_error('item_image1', '<div class="error" style="color:red;">', '</div>'); ?>
						</div>
						
					 </div>
					
					
					
					
					<!--<div class="form-group">
					    <label for="inputEmail3"  class="col-sm-2 control-label"><?php echo $lang['item_image2'];?></label>
						<div class="col-sm-8">
						
						
						<label class="fileContainer">
						<span class="fileContainer_span">Browse</span>
						<input type="file" class="upload_files fileInput css_uploadFile" alt="2"  id="files2" name="files[]" multiple />
						</label>
					    
						<input  type='hidden'  class="form-control only_view_css" id="item_image2" name="item_image2" value='<?php echo $item_image2;?>'>
						<div id="show_item_image2">
						<?php
						if($item_image2!='')
						{
						?>
						<span class="pip">
						<img src="<?php echo base_url(); ?>/upload_item_image/<?php echo $item_image2; ?>"  height='50' width='50'><br/>
						<span class='item_image_remove2 remove remove' id="2" value='<?php echo $item_image2; ?>'>Remove</span>
						</span>
						<?php
						}
						?>
						</div>
                        <?php echo form_error('item_image2', '<div class="error" style="color:red;">', '</div>'); ?>
						</div>
						
					</div>-->
					
					
					<!--<div class="form-group">
					    <label for="inputEmail3"  class="col-sm-2 control-label"><?php echo $lang['item_image3'];?></label>
						<div class="col-sm-8">
						
						
						<label class="fileContainer">
						<span class="fileContainer_span">Browse</span>
						<input type="file" class="upload_files fileInput css_uploadFile" alt="3"  id="files3" name="files[]" multiple />
						</label>
					    
						<input  type='hidden'  class="form-control only_view_css" id="item_image3" name="item_image3" value='<?php echo $item_image3;?>'>
						<div id="show_item_image3">
						<?php
						if($item_image3!='')
						{
						?>
						<span class="pip">
						<img src="<?php echo base_url(); ?>/upload_item_image/<?php echo $item_image3; ?>"  height='50' width='50'><br/>
						<span class='item_image_remove3 remove remove' id="3" value='<?php echo $item_image3; ?>'>Remove</span>
						</span>
						<?php
						}
						?>
						</div>
                        <?php echo form_error('item_image3', '<div class="error" style="color:red;">', '</div>'); ?>
						</div>
						
					</div>-->
					
					<!--<div class="form-group">
					    <label for="inputEmail3"  class="col-sm-2 control-label"><?php echo $lang['item_image4'];?></label>
						<div class="col-sm-8">
						
						
						<label class="fileContainer">
						<span class="fileContainer_span">Browse</span>
						<input type="file" class="upload_files fileInput css_uploadFile" alt="4"  id="files4" name="files[]" multiple />
						</label>
					    
						<input  type='hidden'  class="form-control only_view_css" id="item_image4" name="item_image4" value='<?php echo $item_image4;?>'>
						<div id="show_item_image4">
						<?php
						if($item_image4!='')
						{
						?>
						<span class="pip">
						<img src="<?php echo base_url(); ?>/upload_item_image/<?php echo $item_image4; ?>"  height='50' width='50'><br/>
						<span class='item_image_remove4 remove remove' id="4" value='<?php echo $item_image4; ?>'>Remove</span>
						</span>
						<?php
						}
						?>
						</div>
                        <?php echo form_error('item_image4', '<div class="error" style="color:red;">', '</div>'); ?>
						</div>
						
					</div>-->
					
					
					
					
					
					<!--<div class="form-group">
					    <label for="inputEmail3"  class="col-sm-2 control-label"><?php echo $lang['item_image5'];?></label>
						<div class="col-sm-8">
						
						
						<label class="fileContainer">
						<span class="fileContainer_span">Browse</span>
						<input type="file" class="upload_files fileInput css_uploadFile" alt="5"  id="files5" name="files[]" multiple />
						</label>
					    
						<input  type='hidden'  class="form-control only_view_css" id="item_image5" name="item_image5" value='<?php echo $item_image5;?>'>
						<div id="show_item_image5">
						<?php
						if($item_image5!='')
						{
						?>
						<span class="pip">
						<img src="<?php echo base_url(); ?>/upload_item_image/<?php echo $item_image5; ?>"  height='50' width='50'><br/>
						<span class='item_image_remove5 remove remove' id="5" value='<?php echo $item_image5; ?>'>Remove</span>
						</span>
						<?php
						}
						?>
						</div>
                        <?php echo form_error('item_image', '<div class="error" style="color:red;">', '</div>'); ?>
						</div>
						
					</div>-->
					
					<div >
					
					<input  type='hidden'  class="form-control only_view_css" id="item_image2" name="item_image2" value='<?php echo $item_image2;?>'>
					<input  type='hidden'  class="form-control only_view_css" id="item_image3" name="item_image3" value='<?php echo $item_image3;?>'>
					<input  type='hidden'  class="form-control only_view_css" id="item_image4" name="item_image4" value='<?php echo $item_image4;?>'>
					<input  type='hidden'  class="form-control only_view_css" id="item_image5" name="item_image5" value='<?php echo $item_image5;?>'>
					
					
					</div>
					
					
					
					
					
					
					
					
					
					
					
					
					
					
					
					
					
					
					
					
					
					
					
					
					
					
					<div class="form-group">
					    <label for="inputEmail3"  class="col-sm-2 control-label"><?php echo $lang['notes'];?></label>
						<div class="col-sm-8">
						    <div class="container">
							
							<label class="radio-inline  only_view_css">
							<input type="radio" name="notes" id="notes" value="1" <?php if($notes=='1') { ?> checked="checked" <?php } ?> >Allow Comments
							</label>
							<label class="radio-inline  only_view_css">
							<input type="radio" name="notes"  id="notes" value="0" <?php if($notes=='0') { ?> checked="checked" <?php } ?>>Not Allow Comments
							</label>
							</div>
						</div>
						
					</div>
					
					
					
					
					
					
						
			
					
					
					<div class="form-group">
					<label for="inputEmail3"  class="col-sm-2 control-label"><?php echo $lang['status'];?></label>
					<div class="col-sm-8">
				
							
								<select name="status" id="status" class="form-control only_view_css" >
								 <!--<option value=''>Select</option>-->
								 <option value="1" <?php if($status=='1') { ?> selected="selected" <?php } ?>  >Enable</option>
								 <option value="0" <?php if($status=='0') { ?> selected="selected" <?php } ?>   >Disable</option>
								 </select>
					
					</div>
					</div>
					
					
					
					
					
					
					
					
					
					
					
					
					
					
					
					
					
					
					
				
					
				
					
				
					
					
					
					<!-- Contractor type fields end-->
					<!--<div class="row">
					<?php if($item_id!=''){
						$button_name='Update';
					}else {
						$button_name='Save';
					} ?>
						<div class="col-md-6"><input class="btn btn-success pull-right" type="submit" value="<?php echo $button_name;?>"></div>	   
						<div class="col-md-6"><input class="btn btn-danger pull-left" type="reset"></div>
					</div>-->
					
					
					
					
					
					
					
					
					
					
					
					
					
					
					
					<?php echo form_close();?> 
					 
					 
					 
				    </div>
					
					
					
					
					
							
					
					
				    </div>
					
					
					
					
				
		
					
					
					
				</div>
					
			</div>
		</div>
		
	</section>
</div>

<?php $this->load->view('admin/includes/globaljs');?>
<link type="text/css" href="<?php echo base_url();?>assets/form-select2/select2.css" rel="stylesheet"/>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootswatch/3.3.7/lumen/bootstrap.min.css">



<script type="text/javascript" src="<?php echo base_url();?>assets/form-select2/select2.min.js"></script>



<!--<script  type="text/javascript" src="<?php echo base_url();?>assets/dist/password-score.js"></script>
<script  type="text/javascript" src="<?php echo base_url();?>assets/dist/password-score-options.js"></script>
<script  type="text/javascript" src="<?php echo base_url();?>assets/dist/bootstrap-strength-meter.js"></script>-->

 <script>
    
    
	jQuery(function ($) {
	
	
	$('#extra_id').select2();
	$('#option_id').select2();
	
	
	
	
			  
		 $('#segment_id').change(function(){
		 
		 
			// var str = $(this).attr("id");
			var item_id = $('#item_id').val();
			var segment_id = $('#segment_id').val();
			
			//alert(segment_id);
			$.ajax({
				method:'post',
				url: "<?php echo base_url(); ?>item/get_segment", 
				data:"&item_id="+item_id+"&segment_id="+segment_id,
				success: function(result){
				if(result)
				{
				  // alert(result);
					
					//alert("koko");
					
					var main_html=$.trim(result);
					//alert(val);
					$("#segmant_detail").show();
					$("#segmant_detail").html(main_html);
				  
				 
				 
				
				}
				}
				});
					
			 
			 
	    });
	
	
	<?php
	if($segment_id_sel!='')
	{
	?>
	
	
	
		 
		 
			// var str = $(this).attr("id");
			var item_id = '<?php echo $item_id; ?>';
			var segment_id = '<?php echo $segment_id_sel; ?>';
			//alert(item_id);
			//alert(segment_id);
			$.ajax({
				method:'post',
				url: "<?php echo base_url(); ?>item/get_segment", 
				data:"&item_id="+item_id+"&segment_id="+segment_id,
				success: function(result){
				if(result)
				{
				  //alert(result);
					
					//alert("koko");
					
					var main_html=$.trim(result);
					//alert(val);
					$("#segmant_detail").show();
					$("#segmant_detail").html(main_html);
				  
				 
				 
				
				}
				}
				});
					
			 
			 
	    
	
	
	
	<?php
	}
	?>
	
	
	
	
	
	//alert("popo");
	
	
	 $(".upload_files").on("change", function(e) {
	
	//alert("goko");
	 var no = $(this).attr('alt') ;
	 
	 //alert(no);
	
      var files = e.target.files,
        filesLength = files.length;
      for (var i = 0; i < filesLength; i++) {
        var f = files[i]
        var fileReader = new FileReader();
        fileReader.onload = (function(e) {
          var file = e.target;
		  
		 // alert(e.target.result);
		  
         /* $("<span class=\"pip\">" +
            "<img class=\"imageThumb\" src=\"" + e.target.result + "\" title=\"" + file.name + "\"/>" +
            "<br/><span class=\"remove\">Remove</span>" +
            "</span>").insertAfter("#files");*/
			
			
			
			
		//alert(no);
  var input = document.getElementById("files"+no);
  file = input.files[0];
  if(file != undefined){
    formData= new FormData();
    if(!!file.type.match(/image.*/)){
      formData.append("image", file);
      $.ajax({
        url: "<?php echo base_url();?>upload_file.php?folder_name=upload_item_image",
        type: "POST",
        data: formData,
        processData: false,
        contentType: false,
        success: function(data){
		
		 //alert("koko");
	     //alert(data);
		 
		     var arr=data.split("@@@");
			  var show_item_image=arr[0];
			  var item_image=arr[1];
			  
			 
			  
			  
			 /*$("<span class=\"pip\">" +
             show_item_image +
            "<br/><span class='remove remove"+no+"' value='"+item_image+"'>Remove</span>" +
            "</span>").insertAfter("#files");*/
			
			 /*$("<span class=\"pip\">" +
            "<img class=\"imageThumb\" src=\"" + e.target.result + "\" title=\"" + file.name + "\"/>" +
            "<br/><span class=\"remove\">Remove</span>" +
            "</span>").insertAfter("#files");*/
			

			/*var temp_item_image = $("#item_image"+no).val();
			if(temp_item_image!='')
			{
			
			 var main_item_image=temp_item_image+",,,"+item_image;
			 $("#item_image"+no).val(main_item_image);
			}
			else
			{
			 $("#item_image"+no).val(",,,"+item_image);
			}*/
			
			 $("#show_item_image"+no).html("<span class=\"pip\">" +
             show_item_image +
            "<br/><span class='item_image_remove remove remove"+no+"' value='"+item_image+"'>Remove</span>" +
            "</span>");
			
			$("#item_image"+no).val(item_image);
		
		
          // alert('success');
		   
		   
		    $(".remove"+no+"").click(function(){
		  
		      //alert("joko");
			  
			   var remove_value = $(this).attr('value') ;
			   
			  // alert(remove_value);
			   
			   var temp_item_image = $("#item_image"+no).val();
			   
			   var main_item_image = temp_item_image.replace(remove_value, '')
			   
			    $("#item_image"+no).val(main_item_image);
			  
               $(this).parent(".pip").remove();
			   
			   
			  // alert("<?php echo base_url();?>admin/Project/get_unlink_path");
			   
				$.ajax({
				method:'get',
				url: "<?php echo base_url(); ?>item/get_unlink_path", 
				data:"&image_delete_value="+remove_value,
				success: function(result){
				if(result)
				{
				//alert(result);
				}
				}
				});
				
				return false;			
          });
		  
		  
        }
      });
    }else{
	
	  alert("Please Upload Images");
	  return false;
	  
	  
	   formData.append("image", file);
       $.ajax({
        url: "<?php echo base_url();?>upload_file.php?folder_name=upload_item_image",
        type: "POST",
        data: formData,
        processData: false,
        contentType: false,
        success: function(data){
		
		
		  // alert(data);
			
			
			   var arr=data.split("@@@");
			  var show_item_image=arr[0];
			  var item_image=arr[1];
		     
		 /*$("<span class=\"pip\">" +
             show_item_image +
            "<br/><span class='remove remove"+no+"' value='"+item_image+"'>Remove</span>" +
            "</span>").insertAfter("#files");*/
			
			
			/*var temp_item_image = $("#item_image"+no).val();
			if(temp_item_image!='')
			{
			
			 var main_item_image=temp_item_image+",,,"+item_image;
			 $("#item_image"+no).val(main_item_image);
			}
			else
			{
			 $("#item_image"+no).val(",,,"+item_image);
			}*/
			
			 $("#show_item_image"+no).html("<span class=\"pip\">" +
             show_item_image +
            "<br/><span class=' item_image_remove remove remove"+no+"' value='"+item_image+"'>Remove</span>" +
            "</span>");
			
			$("#item_image"+no).val(item_image);
			
			
			
			
		 $(".remove"+no+"").click(function(){
		  
		      //alert("joko");
			  
			   var remove_value = $(this).attr('value') ;
			   
			 //  alert(remove_value);
			   
			   var temp_item_image = $("#item_image"+no).val();
			   
			   var main_item_image = temp_item_image.replace(remove_value, '')
			   
			    $("#item_image"+no).val(main_item_image);
			  
               $(this).parent(".pip").remove();
			   
			   
			   //alert("<?php echo base_url();?>admin/Project/get_unlink_path");
			   
				$.ajax({
				method:'get',
				url: "<?php echo base_url(); ?>item/get_unlink_path", 
				data:"&image_delete_value="+remove_value,
				success: function(result){
				if(result)
				{
				//alert(result);
				}
				}
				});			   
			   
			   return false;			
          });	
		
		
           // alert('success');
        }
      });
	  
	  
    }
  }else{
    alert('Input something!');
  }

			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
          /*$(".remove").click(function(){
		  
		  alert("joko");
            $(this).parent(".pip").remove();
          });*/
          
          // Old code here
          /*$("<img></img>", {
            class: "imageThumb",
            src: e.target.result,
            title: file.name + " | Click to remove"
          }).insertAfter("#files").click(function(){$(this).remove();});*/
          
        });
        fileReader.readAsDataURL(f);
      }
    });
	
	
	
	
	
	
	     $(".remove").click(function(){
		  
		      //alert("joko");
			  
			   var no = $(this).attr('id') ;
			   var remove_value = $(this).attr('value') ;
			   
			   //alert(remove_value);
			   
			   var temp_item_image = $("#item_image"+no).val();
			   
			   var main_item_image = temp_item_image.replace(remove_value, '')
			   
			    $("#item_image"+no).val(main_item_image);
			  
               $(this).parent(".pip").remove();
			   
			   
			  // alert("<?php echo base_url();?>admin/Project/get_unlink_path");
			   
				$.ajax({
				method:'get',
				url: "<?php echo base_url(); ?>item/get_unlink_path", 
				data:"&image_delete_value="+remove_value,
				success: function(result){
				if(result)
				{
				//alert(result);
				}
				}
				});
				
				return false;			
          });
		  
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	   // $('.content-wrapper').css('min-height','900px');
		
		
		
	
		
		
		/*$('#item_password').strengthMeter('progressBar', {
            container: $('#example-progress-bar-container')
        });*/
		
		
		
	    /*$.validator.addMethod("regex", function(value, element, regexpr) {          
		 return regexpr.test(value);
		}, "Please enter a valid Mobile Number.");*/
		
		
		
		
		
		
		
		
		
		//alert("popo");
		
		
		
		
		
		$('#item_form').validate({
		
           rules: {
		     menu_id:{
				required:true
			 },
             item_name:{
				required:true
			 },
			 item_description:{
				required:true
			 },
			 item_price:{
				required:true
			 },
			 level1segment_price:{
				required:true
			 },
			 level2segment_price:{
				required:true
			 },
			 level3segment_price:{
				required:true
			 },
			 item_status:{
				required:true
			 },
			 item_image1:{
				required:true
			 },
			 notes:{
				required:true
			},
			status:{
				required:true
			},
			
			
			
			
			
			
		},
		 messages: {
		 
		    
			
		  
			
        },
				submitHandler: function(form) {
		
		       //alert("koko");
		
		        var item_id=$('#item_id').val();
				var menu_id=$('#menu_id').val();
				
				
				if($('#extra_id').val()!='')
				{
					var extra_id=',';
					$('#extra_id option:selected').each(function() {
					//alert($(this).val());
					extra_id+=$(this).val()+',';
					});
				}
				else
				{
				   var extra_id='';
				}
				
				
				
				if($('#option_id').val()!='')
				{
					var option_id=',';
					$('#option_id option:selected').each(function() {
					//alert($(this).val());
					option_id+=$(this).val()+',';
					});
				}
				else
				{
				   var option_id='';
				}
				
				
				
				
				
			    var item_name=$('#item_name').val();
			    var item_description=$('#item_description').val();
				var item_price=$('#item_price').val();
				
				var segment_id=$('#segment_id').val();
				var level1segment_price=$('#level1segment_price').val();
				var level2segment_price=$('#level2segment_price').val();
				var level3segment_price=$('#level3segment_price').val();
				
				
				
				var item_status=$('#item_status').val();
				var item_image1=$('#item_image1').val();
				var item_image2=$('#item_image2').val();
				var item_image3=$('#item_image3').val();
				var item_image4=$('#item_image4').val();
				var item_image5=$('#item_image5').val();
				var notes = $("input[name='notes']:checked").val();
				var status=$('#status').val();
				
				
				
					
				//alert("<?php echo base_url(); ?>item/insert_item?&item_id="+item_id+"&menu_id="+menu_id+"&extra_id="+extra_id+"&option_id="+option_id+"&item_name="+item_name+"&item_description="+item_description+"&item_price="+item_price+"&item_status="+item_status+"&item_image1="+item_image1+"&item_image2="+item_image2+"&item_image3="+item_image3+"&item_image4="+item_image4+"&item_image5="+item_image5+"&notes="+notes+"&status="+status+"&segment_id="+segment_id+"&level1segment_price="+level1segment_price+"&level2segment_price="+level2segment_price+"&level3segment_price="+level3segment_price);
					
				$.ajax({
				method:'post',
				url: "<?php echo base_url(); ?>item/insert_item", 
				data:"&item_id="+item_id+"&menu_id="+menu_id+"&extra_id="+extra_id+"&option_id="+option_id+"&item_name="+item_name+"&item_description="+item_description+"&item_price="+item_price+"&item_status="+item_status+"&item_image1="+item_image1+"&item_image2="+item_image2+"&item_image3="+item_image3+"&item_image4="+item_image4+"&item_image5="+item_image5+"&notes="+notes+"&status="+status+"&segment_id="+segment_id+"&level1segment_price="+level1segment_price+"&level2segment_price="+level2segment_price+"&level3segment_price="+level3segment_price,
				success: function(result){
				if(result)
				{
				    //alert(result);
					
					//alert("koko");
					
					var val=$.trim(result);
					//alert(val);
					
					$("#success_message").show();
				  setTimeout(function(){setInterval(function(){
				  //alert("ppp");
				  parent.$.fancybox.close();
				  
				  }, 3000)}, 3000);
				  
				  
					
					parent.$.fancybox.close();
					
					//alert("fofo");
					//alert("koko");
					
					parent.$('#example1').dataTable().fnStandingRedraw();
					
					
				  
					
				
				  
				  
				 
				
				}
				}
				});
					
					
					
					
					
						
							
					
					   
		
		} 
		 
		
		});
		
		
		
	
	
		 
		
		
		
	});	
</script>   
<script>
 $(document).ready(function() {
 //alert("popo");
	  /* $('#example-getting-started-input').strengthMeter('text', {
            container: $('#example-getting-started-text')
        });
        $('#example-tooltip').strengthMeter('tooltip');*/
		/*$('#user_passowrd').strengthMeter('progressBar', {
            container: $('#example-progress-bar-container')
        });*/
    });
</script>
<script type="text/javascript" src="<?php echo base_url();?>theme/assets/source/jquery.fancybox.js?v=2.1.5"></script>
<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>theme/assets/source/jquery.fancybox.css?v=2.1.5" media="screen" />				