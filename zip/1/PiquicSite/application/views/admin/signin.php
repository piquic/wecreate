<?php
/*$password="admin";
echo sha1($password);
echo "aaaaaaaa";exit;*/
?>
<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Piquic| Log In</title>
   <link rel="shortcut icon" type="image/png" href="<?php echo base_url(); ?>assets/img/logo.png"/>
  
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.6 -->
  <link rel="stylesheet" href="<?php echo base_url('theme');?>/assets/bootstrap/css/bootstrap.min.css">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="<?php echo base_url('theme');?>/assets/dist/css/AdminLTE.min.css">
  <!-- iCheck -->
  <link rel="stylesheet" href="<?php echo base_url('theme');?>/assets/plugins/iCheck/square/blue.css">

</head>
<body class="hold-transition login-page">
<div class="login-box">
  <div class="login-logo">
    <a href="#"><b><img src="<?php echo base_url(); ?>assets/img/logo.png" style="margin-top:5px;" alt="logo" ></a>
  </div>
  <!-- /.login-logo -->
  <div class="login-box-body" id='login'>
    <p class="login-box-msg">Sign in </p>

   
    <?php echo $this->session->flashdata('check_database', 'invalid username and password');  
					$class = array("class"=>"login-box-body", "id"=>"login_form"); 
					echo form_open('administrator/check_login', $class);
					?>
					<?php echo validation_errors(); ?>
      <div class="form-group has-feedback">
        <input class="form-control" type="email" placeholder="Email" id="email" name="email" value="<?php echo set_value('email'); ?>" />
        <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
      </div>
      <div class="form-group has-feedback">
       <input class="form-control" type="password" placeholder="Password" name="password" value="<?php echo set_value('password'); ?>">
        <span class="glyphicon glyphicon-lock form-control-feedback"></span>
      </div>
	  
	   <!--  <div class="g-recaptcha" data-sitekey="6Ld32gcUAAAAAJksyxLaPv4d0KaMqyevDLAVAhaU" style="transform:scale(0.92);-webkit-transform:scale(0.92);transform-origin:0 0;-webkit-transform-origin:0 0;"></div>-->
		 
      <div class="row">
        <div class="col-xs-8">
          <div class="checkbox icheck">
            <label>
              <input type="checkbox"> Remember Me
            </label>
          </div>
        </div>
        <!-- /.col -->
        <div class="col-xs-4">
        <input class="btn btn-primary" type="submit" value="Sign In" name="signin" />
        </div>
        <!-- /.col -->
      </div>
	   <div class="col-xs-10">
		    <br>
			<a href="#" id="forget_password">I forgot my password</a><br>
		</div><br/>
	     
    </form>
   		
  </div>
   <!--Written by sravanthi-->
	<div  id="customer_forget_password"  style="display:none;"   >
		<div class=" login-box" >
			<div class="login-box-body">
			    <div  id="success_forget_password"  style="display:none;"   >
			        <div class="alert alert-success" style="border:1px #5ACE64 solid;">
			            <a class="close" data-dismiss="alert">x</a>
			            <strong>
			            <font style="color:#ffff">Successfully mail send. Check your mail</font>
			            </strong>
			        </div>
			    </div>
				<div  id="error_forget_password"  style="display:none;"   >
				    <div class="alert alert-error" style="border:1px #f00 solid;">
				        <a class="close" data-dismiss="alert">x</a>
				        <strong>
				        <font style="color:#ffff">Invalid Email</font>
				        </strong>
				    </div>
				</div>
				<p class="login-box-msg">Forget Password</p>
				<?php echo $this->session->flashdata('check_database_owner', 'invalid username and password');  
				$class = array("class"=>"login-box-body", "id"=>"forget_password_customer"); 
				echo form_open('administrator/check_forget_password', $class);
				?>
				<?php echo validation_errors(); ?>
				<div class="form-group has-feedback">
				<input class="form-control" type="email" placeholder="Email" id="forget_email" name="forget_email" value="<?php echo set_value('email'); ?>" />
				<span class="glyphicon glyphicon-envelope form-control-feedback"></span>
				</div>
				
				
				<div class="g-recaptcha" data-sitekey="6Ld32gcUAAAAAJksyxLaPv4d0KaMqyevDLAVAhaU" style="transform:scale(0.92);-webkit-transform:scale(0.92);transform-origin:0 0;-webkit-transform-origin:0 0;"></div>
				
				<div class="row">
				    <div class="col-xs-4">
					   <button type="submit" class="btn btn-primary btn-block btn-flat">Submit</button>
					</div>
					<div class="col-xs-4">
					   <button type="button" class="btn btn-primary btn-block btn-flat" onClick="location.href='<?php echo base_url('login');?>'">Back</button>
					</div>
				</div>
				</form>
			</div>
		</div>
	</div>	
  <!-- /.login-box-body -->
</div>
<!-- /.login-box -->
<?php echo form_close();?>
<!-- jQuery 2.2.3 -->
<script src="<?php echo base_url('theme');?>/assets/plugins/jQuery/jquery-2.2.3.min.js"></script>
<!-- Bootstrap 3.3.6 -->
<script src="<?php echo base_url('theme');?>/assets/bootstrap/js/bootstrap.min.js"></script>
<!-- iCheck -->
<script src="<?php echo base_url('theme');?>/assets/plugins/iCheck/icheck.min.js"></script>
<script src="https://www.google.com/recaptcha/api.js" async defer></script>
<script src="<?php echo base_url(); ?>assets/js/jquery.validate.min.js"></script>


<script>
  $(function () {
    $('input').iCheck({
      checkboxClass: 'icheckbox_square-blue',
      radioClass: 'iradio_square-blue',
      increaseArea: '20%' // optional
    });
  });
  </script>
  <script>
 $(function () {

        // Setup form validation on the #register-form element
        $("#login_form").validate({

            // Specify the validation rules
            rules: {
                email: {
                    required: true,
                    email: true
                },
                password: "required"
            },

            // Specify the validation error messages
            messages: {

                email: {
                    required: "Please Enter Email",
                    email: "Please Specify Valid Email"
                },
                password: "Please Enter Password"
            },

            submitHandler: function (form) {
                form.submit();
            }
        });

    });
    //written by sravanthi
	$('#forget_password').click(function(){
    $("#customer_forget_password").show(1000);
	$("#login").hide(1000);

   
    });
	$("#forget_password_customer").validate({

            // Specify the validation rules
            rules: {
                forget_email: {
                    required: true,
                    email: true
                }
            },

            // Specify the validation error messages
            messages: {

                forget_email: {
                    required: "Please Enter Email",
                    email: "Please Specify Valid Email"
                }
            },

            submitHandler: function (form) {
                //form.submit();
				
				var email=$('#forget_email').val();
			    // alert(email);
					$.ajax({
					type: "POST",
					url: "<?php echo base_url();?>login/check_forget_password", 
				    data:"email="+email,
					success: function (result) {
				    //alert(result);
					if(result==0)
					{
					 //alert('error');
					 $("#success_forget_password").hide(1000);
					 $("#error_forget_password").show(1000);
					}
					else
					{
					 // alert('success');
					  $("#error_forget_password").hide(1000);
					  $("#success_forget_password").show(1000);
					}
					
					
					}
					});
					return false;
					}
        });
		
</script> 
<style>
 #login_form label.error, .output {
        color: #FB3A3A;
    }
	
	
	.alert .alert-error { color:#ffffff !important;}
	
	
	@media (max-width: 500px) {
.g-recaptcha {transform:scale(0.85)!important;-webkit-transform:scale(0.85)!important;transform-origin:0 0;-webkit-transform-origin:0 0;}
}
@media (max-width: 414px) {
.g-recaptcha {transform:scale(0.96)!important;-webkit-transform:scale(0.96)!important;transform-origin:0 0;-webkit-transform-origin:0 0;}
}
@media (max-width: 375px) {
.g-recaptcha {transform:scale(0.85)!important;-webkit-transform:scale(0.85)!important;transform-origin:0 0;-webkit-transform-origin:0 0;}
}
@media (max-width: 320px) {
.g-recaptcha {transform:scale(0.69)!important;-webkit-transform:scale(0.69)!important;transform-origin:0 0;-webkit-transform-origin:0 0;}
}

</style>
</body>
</html>
