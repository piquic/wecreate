<?php
session_start();
error_reporting(0);
$data['page_title'] = "master autopose";
$this->load->view('front/includes/header',$data);
$this->load->view('front/includes/nav');
$second_DB = $this->load->database('another_db', TRUE); 
$session_data = $this->session->userdata('front_logged_in');
$user_id = $session_data['user_id'];
$user_name = $session_data['user_name'];

$autoquery=$this->db->query("select * from  tbl_autopose WHERE client_name='$user_name'");
$mstautoinfo=$autoquery->result_array();

if(!empty($mstautoinfo))
{
$autom_id=$mstautoinfo[0]['model_id'];
$mod_pose_id=$mstautoinfo[0]['mod_pose_id'];
$mod_skin_tone=$mstautoinfo[0]['mod_skin_tone'];
}
else
{
$autom_id='';
$mod_pose_id='';
$mod_skin_tone='';
}


$Sekcje = explode( ',',$autom_id );
$poseSekcje = explode(',',$mod_pose_id );
$poseSekcje=explode(",", ("pose_".implode(",pose_", $poseSekcje)));
//$poseSekcje=explode(",", (implode(",pose_", $poseSekcje)));
$poseSekcje = implode(",",$poseSekcje);
$mst = explode( ',',$mod_skin_tone );

if($poseSekcje=='pose_')
{
$poseSekcje='';
}
if($poseSekcje!='')
{
$poseSekcje=','.$poseSekcje.',';
}

?>

<div class="container-fluid">
	<div class="row mb-4">

		<!-- <?php //$this->load->view('front/includes/sidebar'); ?>

		<div class="col-12 col-sm-12 col-md-9 col-lg-9 col-xl-9 border"> -->
		<div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12 border">

			<div class="container">
				<div class="row mt-4">
					<div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
						<h2 class="text-dark">Model Selection</h2>
					</div>
				</div>
			</div>

			<div class="container pt-3">
				<div class="row">
					<div class="col-12 col-sm-12 col-md-4 col-lg-4 col-xl-4">
						<p class="lead">Client Name:</p>

					</div>
					<div class="col-12 col-sm-12 col-md-6 col-lg-6 col-xl-6">
						<i><?php echo $user_name;?></i>
					</div>
				</div>

				<div class="row">
					<div class="col-12 col-sm-12 col-md-4 col-lg-4 col-xl-4">
						<p class="lead">Model Usage:</p>
					</div>

					<div class="col-12 col-sm-12 col-md-6 col-lg-4 col-xl-4">
						<select class="form-control" id="usedate" name="usedate">
							<option value="0">Select</option>
							<option value="1">Today</option>
							<option value="2">Yesterday</option>
							<option value="3">Last 7 Days</option>
							<option value="4">Last 30 Days</option>
							<option value="5">This Year</option>
						</select>
					</div>
				</div>
			</div>

			<hr>

			<div class="container">
				<div class="row" id="modelfilter">

					<input type="hidden" name="model[]" id="model" value="<?php echo $autom_id; ?>"/>
					<input type="hidden" name="modelpose[]" id="modelpose" value="<?php echo $poseSekcje; ?>"/>
					<input type="hidden" id="skintoneval" name="skintoneval[]" value="<?php echo $mod_skin_tone; ?>">

					<div class="col-12 col-sm-12 col-md-2 col-lg-2 col-xl-2">
						<p class="lead">Models:</p>
					</div>

					<div class="col-12 col-sm-12 col-md-10 col-lg-10 col-xl-10">
						<div class="row">

						<?php 	
						$CI = &get_instance();
						$this->db2 = $CI->load->database('another_db', TRUE);					
						$modquery = $this->db2->query("SELECT * FROM `tbl_model` where status='1'");
						$modresult=$modquery->result();					
							foreach($modresult as $modrow){	
							$m_id= $modrow->m_id;
							$mod_name=$modrow->mod_name;
							$mod_img=$modrow->mod_img;
							$skintone=$modrow->skintone;

							$arr=explode(",",$skintone);
			
		    	$arrjpg = array();

			    foreach ($arr as $value) {
				     if(strpos($value, '.jpg')){
					    array_push($arrjpg, $value);
				    }
			      }

      $mkquery = $this->db2->query("SELECT SUM(`count`) AS value_sum  FROM `tbl_model_count` where  model_id='$m_id'");
								$mprow=$mkquery->result_array();
							$count=$mprow[0]['value_sum'];

							if($count!=''){
								$count=$count;
							}else{
								$count='0';
							}

							?>

							<div class="col-12 col-sm-12 col-md-2 col-lg-2 col-xl-2 text-center">
								<div class="mcbox">

									<?php if($autom_id!=''){ ?>
        <input type="checkbox" class="m-chkbox <?php if(in_array($m_id,$Sekcje)) { ?> activecat <?php }?>" id="<?php echo $m_id; ?>" name="<?php echo $m_id; ?>" onclick="modselFunction(<?php echo $m_id; ?>)" <?php if (in_array($m_id, $Sekcje)) { ?> checked <?php }?> >
								
							<?php } else{ ?>

        <input type="checkbox" class="m-chkbox activecat" id="<?php echo $m_id; ?>" name="<?php echo $m_id; ?>" onclick="modselFunction(<?php echo $m_id; ?>)" checked >

							<?php } ?>

								</div> 

								<div class="mlbox">
									<label class="m-chklbl" for="<?php echo $m_id; ?>">&nbsp;</label>
								</div>

								<div class="border rounded-circle mb-2 crop">
									<img class="w-100 rounded-circle" src="<?php echo PIQUIC_URL;?>images/model_image/<?php echo $mod_img; ?>" /> 
								</div>
								<p class="lead text-uppercase"><img style="width: 1.5rem;" src="<?php echo PIQUIC_URL;?>images/icon/sktn.png" data-toggle="collapse" data-target="#m_<?php echo $m_id; ?>"><?php echo $mod_name; ?><br><small>(<?php echo $count;?>)</small>
								</p>
							</div>
      <?php } ?>

							<!-- collapse Start -->		

							<?php
						
				$modquery = $this->db2->query("SELECT * FROM `tbl_model` where status='1'");
				$modresult=$modquery->result();		
				foreach($modresult as $moderow){
				$mo_id=$moderow->m_id;
				$model_name=$moderow->mod_name;
				$skintone=$moderow->skintone;
				$arr=explode(",",$skintone);
				$arrjpg = array();
				foreach ($arr as $value) {
					if(strpos($value, '.jpg')){
						array_push($arrjpg, $value);
					}
				}
				?>				
							<div class="collapse fade bg-white shadow position-fixed" id="m_<?php echo $mo_id; ?>" tabindex="-1" role="dialog" aria-labelledby="m_<?php echo $mo_id; ?>Title" aria-hidden="true" data-parent="#modelfilter" style="top: 40%; left: 24%; z-index: 111111; padding: 0; width: 63%;" >
								<div class="row p-3">
									<div class="col-md-10">
										<h5 class="text-uppercase" id="m_<?php echo $mo_id; ?>Title">Skin Tone for <?php echo $model_name; ?></h5>
									</div>
									<div class="col-md-2">
										<button type="button" class="close" data-toggle="collapse" data-target="#m_<?php echo $mo_id; ?>" aria-label="Close">
											<span aria-hidden="true">&times;</span>
										</button>
									</div>
								</div>

								<div class="row p-3 rdst-group">
									<input type="hidden" id="rdst-value" name="rdst-value">

									<?php 	
						$i='1';
						foreach($arrjpg as $image){								
							?>
									<div class="col-md-2 text-center mb-3 rdst" id="div_<?php echo $m_id.$i; ?>" data-value="<?php echo $image?>" >
										<div class="shadow rounded p-1 <?php if (in_array("$image",$mst)) { ?> border <?php } ?>" id="<?php echo $image?>">
											<img class="w-100" src="<?php echo PIQUIC_URL;?>images/skin_tone/<?php echo $image?>" >
											<small><?php echo $image?></small>
										</div>
									</div>
      <?php $i++; } ?>


								</div>
							</div>
<?php } ?>

						</div>
					</div>
				</div>
			</div>

			<hr>

			<div class="container">
				<div class="row pt-2 pl-2" >
					<div class="col-12 col-sm-12 col-md-4 col-lg-4 col-xl-4">
						<p class="lead">Gender:</p>
					</div>
					<div class="col-12 col-sm-12 col-md-6 col-lg-4 col-xl-4">
						<select class="form-control" id="gen_id" name="gen_id">

							<option value="0">Select Gender</option>
						<?php 	
						$genquery=$this->db->query("select * from  tbl_gender WHERE status='1'");
						$geninfo=$genquery->result();
						foreach($geninfo as $gnresrow){  
							$genname=$gnresrow->gen_name; 
							$genid=$gnresrow->gen_id; 
							?>
							<option value="<?php echo $genid;?>"><?php echo $genname;?></option>
							
							<?php } ?>
						</select>
					</div>
				</div>

				<div class="row pt-2 pl-2" >
					<div class="col-12 col-sm-12 col-md-4 col-lg-4 col-xl-4">
						<p class="lead">Model:</p>
					</div>

					<div class="col-12 col-sm-12 col-md-6 col-lg-4 col-xl-4">
						<div id="gen_model_id_div">
							<select class="form-control" id="gen_model_id" name="gen_model_id" onchange="showPoseModel();">
							<option value="0">Select Model</option>
							<?php 	

							

							 $pautoquery = "SELECT * FROM `tbl_model` WHERE 1=1 ";
							 if($autom_id!='')
							 {
							 	$pautoquery.= " and m_id in($autom_id) ";
							 }
							 
							 $pautoquery.= "  ORDER by `m_id`  ";                                      
										$query2=$second_DB->query($pautoquery);										
										$modelinfo= $query2->result_array();
										foreach($modelinfo as $mrow){
										$mid = $mrow['m_id'];
										$mod_name = $mrow['mod_name'];
										$mod_name =strtoupper($mod_name);

								?>  
							?>
							<option value="<?php echo $mid;?>"><?php echo $mod_name;?></option>

							<?php } ?>
			</select>
						</div>
					</div>
				</div>
			</div>
			<hr>
			<div class="container">
				<!-- <div class="row pt-2 pl-2" id="poselist" style="display: none;"> -->
				<div class="row pt-2 pl-2" id="poselist" style="display: none;">

				</div> 
			</div>
		</div>
	</div>
</div>

	<div class="row pt-2 pl-2 fixed-bottom text-right">
			<div class="col-md-12 mb-2 pr-4">
				<button type="button" id="saveauto" class="btn btn-piquic btn-lg">SAVE</button>
			</div>
		</div>

<?php $this->load->view('front/includes/footer'); ?>

<script type="text/javascript">

	function modselFunction(m) {
 
  var lfckv = document.getElementById(m).checked;
if(lfckv==true){
//alert("1");	
			$('#'+m+'').addClass('activecat').removeClass('inactivecat');
		}else{	
		//alert("2");	
			$('#'+m+'').addClass('inactivecat').removeClass('activecat');
		}
//alert("3");

		var ids = $('.activecat').map(function(i) {
			console.log("ppp");
			console.log(this.id);
			return this.id;
		});
		console.log("kkkkkkk");
		console.log(ids);
		$('#model').val(ids.get().join(','));




		var gen_id=$("#gen_id").val();
		var model=$("#model").val();

		$.ajax({
		type:'POST',
		url:'<?php echo PIQUIC_AJAX_URL ;?>/selectmodel.php',
		data: {'gen_id':gen_id,'model':model},
		success: function(response){
		// alert(response);
		$("#gen_model_id").html(response);
		$("#poselist").hide();

		}
		});	





   }


  


		function showPoseModel()	{
				var gen_model_id=$("#gen_model_id").val();
				var mi =$('#model').val();
				var poid=$('#modelpose').val();
				var cli_name= '<?php echo $user_name;?>';
				var usedate=$("select[name=usedate]").val();
				if(usedate!='0' ){
				var status='2';
				}else{
				var status='1';
				}
				$.ajax({
				type:'POST',
				url:'<?php echo PIQUIC_AJAX_URL ;?>/posecheck_masterauto.php',
				data:{'gen_model_id':gen_model_id,'poid':poid,'cli_name':cli_name,'usedate':usedate,'status':status},
				success: function(checkresults) {	
					// alert(checkresults);
				$("#poselist").html(checkresults);
				$("#poselist").show();
				}
				});

		}


	$(document).ready(function() { 

// $("#gen_id").on("change", function() {

// $('#gen_model_id').prop('selectedIndex',0);

// });

	$("#gen_id").on("change", function() {   
		var gen_id=$("#gen_id").val();
		var model=$("#model").val();
	
		$.ajax({
		type:'POST',
		url:'<?php echo PIQUIC_AJAX_URL ;?>/selectmodel.php',
		data: {'gen_id':gen_id,'model':model},
		success: function(response){
   // alert(response);
		$("#gen_model_id").html(response);
		$("#poselist").hide();

		}
		});					
		});

$("#usedate").on("change", function() {	
			var usedate=$("select[name=usedate]").val();
			var client_name='<?php echo $user_name;?>';
			var status='2';	
			$.ajax({
				type:'POST',
				url:'<?php echo PIQUIC_AJAX_URL ;?>/modelcheck_masterauto.php',
				data:{'client_name':client_name,'usedate':usedate,'status':status},
				success: function(modcheckres){
					// alert(modcheckres);
					$("#modelfilter").html(modcheckres);
				}
			});
		});

		$("#saveauto").on("click", function() {	
			var mi =$('#model').val();
			var mpi =$('#modelpose').val();
			var gen_model_id =$('#gen_model_id').val();
			var cname='<?php echo $user_name;?>';
			var stval =$('#skintoneval').val();				
			if(cname!='0' && cname!=''){
				$.ajax({
					type:'POST',
					url:'<?php echo PIQUIC_AJAX_URL ;?>/saveposecheck_masterauto.php',
					data:{'mi':mi,'mpi':mpi,'cname':cname,'stval':stval,'gen_model_id':gen_model_id},
					success: function(saveres) { 
						// alert('Model & Pose added for this Client');
						$('#modal_msg').modal('show');
						$('#txtMsg').html('Model & Pose added for this Client.');
						window.location.href="<?php echo base_url();?>stylequeue";
						/*setTimeout(function(){
							//location.reload(); 
						}, 5000);*/
					}
				});
			}else{
				// alert('Please select a brand name');
				$('#modal_msg').modal('show');
				$('#txtMsg').html('Please select a brand name.');
			}
		});



 $('.rdst-group .rdst').click(function(){
   	$(this).parent().find('div').removeClass('border');
   	$(this).children('div').addClass('border');
   	var val = $(this).attr('data-value');

   	$(this).parent().find('input').val(val);

   	var ids = $('.border').map(function(i) {
   		return this.id;
   	});
   	$('#skintoneval').val(ids.get().join(',')); 
   });


	});



</script>