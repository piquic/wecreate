<?php
session_start();
error_reporting(0);
$data['page_title'] = "my account";

$this->load->view('front/includes/header',$data);
// $this->load->view('front/includes/menu');
$this->load->view('front/includes/nav');



if ($this->session->userdata('front_logged_in')) {
	$session_data = $this->session->userdata('front_logged_in');
	$user_id = $session_data['user_id'];
	$user_name = $session_data['user_name'];
	
	
	
} else {
	
	$session_data = $this->session->userdata('front_logged_in');
	$user_id = $session_data['user_id'];
	$user_name = $session_data['user_name'];
	
	
}

// print_r($user_details['0']['user_id']);
$success_msg = '';
$error_msg = '';

if(isset($_POST['btnUsrUpdate'])){
	
	$txtUserId 	= $_POST['txtUserId'];
	$txtUser    = $_POST['txtUser'];
	$txtCompany = $_POST['txtCompany'];
	$txtEmail 	= $_POST['txtEmail'];
	$txtPhone 	= $_POST['txtPhone'];
	$txtAddress = $_POST['txtAddress'];

}

if(isset($_POST['btnCngPswd'])){
	
	// if($_POST['txtPswd'] == $_POST['txtRePswd']) {
	$txtUserIdPswd = $_POST['txtUserIdPswd'];
	$txtOldPswd 	= $_POST['txtOldPswd'];
	$txtPswd       = $_POST['txtPswd'];
	$txtRePswd 	   = $_POST['txtRePswd'];
	// } else {
	// 	$error_msg = "New Password and Retype Password is not same.";
	// }

}


if($param == 'success'){
	$success_msg = 'Data updated successfully.';
} else if($param == 'error') {
	$error_msg = 'Error! while updating data.';
} else if($param == ''){
	$success_msg = '';
	$error_msg = '';
}



$query=$this->db->query("select * from  tbl_users  WHERE user_id='$user_id' ");
$userDetails= $query->result_array();
$autostyle_check=$userDetails[0]['autostyle'];

?>

<div class="container-fluid">
	<div class="row">

		<?php //$this->load->view('front/includes/sidebar'); ?>

		<!-- <div class="col-12 col-sm-12 col-md-9 col-lg-9 col-xl-9 border"> -->
			<div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12 mainscroll">
				<div class="container pt-3">
					<div class="row">
						<div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">

							<?php if($success_msg != '') {?>
								<div class="alert alert-success text-center" role="alert">
									<?php echo $success_msg?>
								</div>
							<?php }?>

							<?php if($error_msg != '') {?>
								<div class="alert alert-danger text-center" role="alert">
									<?php echo $error_msg?>
								</div>
							<?php }?>

							<div class="border rounded pt-4 px-4">

								<ul class="nav nav-tabs" id="usrTab" role="tablist">
									<li class="nav-item">
										<a class="nav-link lead active" id="profile-tab" data-toggle="tab" href="#profile" role="tab" aria-controls="profile" aria-selected="true">Profile</a>
									</li>

									<?php if($login_type == '0') {?>
									<li class="nav-item">
										<a class="nav-link lead" id="changePswd-tab" data-toggle="tab" href="#changePswd" role="tab" aria-controls="changePswd" aria-selected="false">Change Password</a>
									</li>
									<?php } ?>

									<li class="nav-item">
										<a class="nav-link lead bg-light" href="<?php echo base_url();?>plans" role="tab" aria-selected="false">Add Credit</a>
									</li>
								</ul>
								<div class="tab-content" id="usrTabContent">
									<div class="tab-pane fade show active" id="profile" role="tabpanel" aria-labelledby="profile-tab">
										<div class="pt-4 px-4">
											<form action="<?php echo base_url();?>front/myaccount/usrUpdate" method="post">
												<input type="hidden" id="txtUserId" name="txtUserId" value="<?php echo $user_details['0']['user_id']; ?>">
												<div class="form-group row pt-3">

													<div class="col-12 col-sm-12 col-md-8 col-lg-8 col-xl-8">

														<div class="form-group row">
															<label for="txtUser" class="col-sm-4 lead">User Name: </label>
															<div class="col-sm-8">
																<input type="text" class="form-control" id="txtUser" name="txtUser" value="<?php echo $user_details['0']['user_name']; ?>">
															</div>
														</div>

														<div class="form-group row">
															<label for="txtCompany" class="col-sm-4 lead">Company: </label>
															<div class="col-sm-8">
																<input type="text" class="form-control" id="txtCompany" name="txtCompany" value="<?php echo $user_details['0']['user_company']; ?>">
															</div>
														</div>

														<div class="form-group row">
															<label for="txtEmail" class="col-sm-4 lead">Email: </label>
															<div class="col-sm-8">
																<input type="email" class="form-control" id="txtEmail" name="txtEmail" value="<?php echo $user_details['0']['user_email']; ?>">
															</div>
														</div>

														<div class="form-group row">
															<label for="txtPhone" class="col-sm-4 lead">Mobile: </label>
															<div class="col-sm-8">
																<input type="text" class="form-control" id="txtPhone" name="txtPhone" value="<?php echo $user_details['0']['user_mobile']; ?>">
															</div>
														</div>

														<div class="form-group row">
															<label for="txtAddress" class="col-sm-4 lead">Address: </label>
															<div class="col-sm-8">
																<textarea class="form-control" id="txtAddress" name="txtAddress"><?php echo $user_details['0']['user_address']; ?></textarea>
															</div>
														</div>

														<div class="form-group row">
															<label for="currency" class="col-sm-4 lead">Set Currency: </label>
															<div class="col-sm-8">
																<select id="currency" name="currency" class="form-control" required="" onchange="changePlan(this.value);">
																		<option value="INR" <?php if($currency_default=='INR') { ?> selected <?php } ?>>INR</option>
																		<option value="USD" <?php if($currency_default=='USD')  { ?> selected <?php } ?>>USD</option>
																		<option value="EUR" <?php if($currency_default=='EUR') {  ?> selected <?php } ?>>EUR</option>
																		<option value="GBP" <?php if($currency_default=='GBP') {  ?> selected <?php } ?>>GBP</option>
																		<option value="AUD" <?php if($currency_default=='AUD') {    ?> selected <?php } ?>>AUD</option>
																		<option value="CNY" <?php if($currency_default=='CNY') {  ?> selected <?php } ?>>CNY</option>
																	</select>
															</div>
														</div>

														<div class=" form-group row pt-3">
															<div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12 text-right">
																<button type="submit" class="btn btn-piquic" id="btnUsrUpdate" name="btnUsrUpdate">Update</button>
															</div>
														</div>
													</div>

													<div class="col-12 col-sm-12 col-md-4 col-lg-4 col-xl-4 text-md-right">
														<h5>Balance: </h5>

														<hr>

														<div class="row">
															<div class="col-12 col-sm-12 col-md-6 col-lg-6 col-xl-6">
																<p class="lead">Image Credits: </p>
															</div>

															<div class="col-12 col-sm-12 col-md-6 col-lg-6 col-xl-6">
																<p class="lead"><?php echo $user_details['0']['total_credit']; ?> images</p>
															</div>
														</div>

														<div class="row">
															<div class="col-12 col-sm-12 col-md-6 col-lg-6 col-xl-6">
																<p class="lead">Amount Paid: </p>
															</div>

															<div class="col-12 col-sm-12 col-md-6 col-lg-6 col-xl-6">
																<p class="lead"><?php echo $user_details['0']['total_amount']; ?>&nbsp;<?php echo $user_details['0']['user_currency']; ?></p>
															</div>
														</div>

														<hr>

														<div class="row">
															<div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
																<div class="custom-control custom-switch">
																	<input type="checkbox" class="custom-control-input" id="switchStyle" <?php if($autostyle_check=='1') { ?>checked <?php } ?> >
																	<label class="custom-control-label chkToggle" for="switchStyle">
																		<span class="lead pl-2"><b>AUTO STYLING</b></span><br>
																		<small class="pl-2">LET PIQUIC STYLE MY PRODUCTS</small>
																	</label>
																</div>
															</div>
														</div>
														
													</div>
												</div>
											</form>
										</div>
									</div>

									<?php if($login_type == '0') {?>
									<div class="tab-pane fade" id="changePswd" role="tabpanel" aria-labelledby="changePswd-tab">
										<div class="pt-4 px-4">
											<form action="<?php echo base_url();?>front/myaccount/pswdUpdate" id="frm_changepass" name="frm_changepass"  method="post">
												<input type="hidden" id="txtUserIdPswd" name="txtUserIdPswd" value="<?php echo $user_details['0']['user_id']; ?>">

												<div class="form-group row pt-3">

													<div class="col-12 col-sm-12 col-md-8 col-lg-8 col-xl-8">

														<div class="form-group row">
															<label for="txtOldPswd" class="col-sm-4 lead">Old Password: </label>
															<div class="col-sm-8">
																<input type="password" class="form-control" id="txtOldPswd" name="txtOldPswd">
															</div>
														</div>

														<div class="form-group row">
															<label for="txtPswd" class="col-sm-4 lead">New Password: </label>
															<div class="col-sm-8">
																<input type="password" class="form-control" id="txtPswd" name="txtPswd">
															</div>
														</div>

														<div class="form-group row">
															<label for="txtRePswd" class="col-sm-4 lead">Retype Password: </label>
															<div class="col-sm-8">
																<input type="password" class="form-control" id="txtRePswd" name="txtRePswd">
															</div>
														</div>

														<div class=" form-group row pt-3">
															<div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12 text-right">
																<button type="submit" class="btn btn-piquic" id="btnCngPswd" name="btnCngPswd">Change Password</button>
															</div>
														</div>
													</div>
												</div>
											</form>
										</div>
									</div>
									<?php } ?>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>

	<?php $this->load->view('front/includes/footer'); ?>

	<script type="text/javascript">
		
		<?php
		$login_type= $this->uri->segment(2);
		if($login_type=='1' && $user_details['0']['user_company']=='')
		{
		?>
		$('#modal_msg').modal('show');
		$('#txtMsg').html('Please fill Company name.');
		<?php
		}
		?>




		$('#switchStyle').on('click', function(){

		//alert("pp");

		// $('switchStyle').prop('checked', true);

		var chked = document.getElementById('switchStyle').checked;

		//alert(chked);

		if( chked == true){

			//window.location = "auto_style.php";
			var autostyle=1;

		} else {


			//window.location = "style.php";
			var autostyle=0;

		}



		$.ajax({
			type:'POST',
			url:'<?php echo PIQUIC_AJAX_URL ;?>/updateautostyle.php', 
			data: {'autostyle':autostyle},
			success: function(accessres){ 
				//alert(accessres);
			//alert("<?php echo base_url() ;?>label");
			//window.location.href="<?php echo base_url() ;?>label";
			
		}
	});



	});




  	$(function () {

		jQuery.validator.addMethod("notEqual", function(value, element, param) {
		return this.optional(element) || value != $(param).val();
		}, "This has to be different...");


		$("#frm_changepass").validate({
			rules: {
				txtOldPswd:{
					required:true,
					remote: {
								url: "<?php echo base_url();?>front/myaccount/checkexistPass",
								type: "post"
							}
						
					},
                txtPswd:{
						required:true,
						notEqual: "#txtOldPswd" 
						
					},
				txtRePswd: {
						required:true,
						equalTo: "#txtPswd" ,
						
					},
						
					},
				messages:
						{
							txtOldPswd:
							{
							   remote:"No such password exist exists."
							},
							
						},
						submitHandler: function (form) {
							document.frm_changepass.submit();
						}
					});
	});












</script>