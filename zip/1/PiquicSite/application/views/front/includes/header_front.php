<!-- Last Edited: KW 25/09/16 -->
<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title>My Booking Solution</title>
    <link rel="shortcut icon" type="image/png" href="<?php echo base_url();; ?>/assets/img/favicon.png"/>
	<meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
	<?php $this->load->view('admin/includes/globalcss');?> 
	<?php 
			/*$ip=$_SERVER['REMOTE_ADDR'];
			$json_timezone = @file_get_contents('http://ip-api.com/'.$ip);
			$json_timezone=json_decode($json_timezone);
			$tzone='Asia/Kolkata';
			if(isset($json_timezone)){
			if($json_timezone->{'status'}=="success")
			{
			$tzone=$json_timezone->{'timezone'}; exit();
			}
			}
			date_default_timezone_set($tzone);*/
			
			
			
	function getTimeZoneFromIpAddress(){
		$clientsIpAddress = get_client_ip();
		//$clientsIpAddress = '124.43.75.14';
		$clientInformation = @unserialize(file_get_contents('http://www.geoplugin.net/php.gp?ip='.$clientsIpAddress));

		$clientsLatitude = $clientInformation['geoplugin_latitude'];
		$clientsLongitude = $clientInformation['geoplugin_longitude'];
		$clientsCountryCode = $clientInformation['geoplugin_countryCode'];

		$timeZone = get_nearest_timezone($clientsLatitude, $clientsLongitude, $clientsCountryCode) ;

		return $timeZone;

	}

	function get_client_ip() {
		$ipaddress = '';
		if (getenv('HTTP_CLIENT_IP'))
			$ipaddress = getenv('HTTP_CLIENT_IP');
		else if(getenv('HTTP_X_FORWARDED_FOR'))
			$ipaddress = getenv('HTTP_X_FORWARDED_FOR');
		else if(getenv('HTTP_X_FORWARDED'))
			$ipaddress = getenv('HTTP_X_FORWARDED');
		else if(getenv('HTTP_FORWARDED_FOR'))
			$ipaddress = getenv('HTTP_FORWARDED_FOR');
		else if(getenv('HTTP_FORWARDED'))
			$ipaddress = getenv('HTTP_FORWARDED');
		else if(getenv('REMOTE_ADDR'))
			$ipaddress = getenv('REMOTE_ADDR');
		else
			$ipaddress = 'UNKNOWN';
		return $ipaddress;
	}

	function get_nearest_timezone($cur_lat, $cur_long, $country_code = '') {
		$timezone_ids = ($country_code) ? DateTimeZone::listIdentifiers(DateTimeZone::PER_COUNTRY, $country_code)
			: DateTimeZone::listIdentifiers();

		if($timezone_ids && is_array($timezone_ids) && isset($timezone_ids[0])) {

			$time_zone = '';
			$tz_distance = 0;

			//only one identifier?
			if (count($timezone_ids) == 1) {
				$time_zone = $timezone_ids[0];
			} else {

				foreach($timezone_ids as $timezone_id) {
					$timezone = new DateTimeZone($timezone_id);
					$location = $timezone->getLocation();
					$tz_lat   = $location['latitude'];
					$tz_long  = $location['longitude'];

					$theta    = $cur_long - $tz_long;
					$distance = (sin(deg2rad($cur_lat)) * sin(deg2rad($tz_lat)))
						+ (cos(deg2rad($cur_lat)) * cos(deg2rad($tz_lat)) * cos(deg2rad($theta)));
					$distance = acos($distance);
					$distance = abs(rad2deg($distance));
					// echo '<br />'.$timezone_id.' '.$distance;

					if (!$time_zone || $tz_distance > $distance) {
						$time_zone   = $timezone_id;
						$tz_distance = $distance;
					}

				}
			}
			return  $time_zone;
		}
		return 'unknown';
	}
			
			// $ip=$_SERVER['REMOTE_ADDR'];
			// $details = (str_replace("a:14:","",@file_get_contents("http://ip-api.com/php/{$ip}")));
			// if(strstr($details,'"timezone";'))
			// {
			// $arr1 = @explode('"timezone";',$details);
			// $arr2 = @explode('";',$arr1[1]); // returns "d"
			// $arr3=@explode(':"',$arr2[0]);
			// $tzone=$arr3[1];
			// }
			// else
			// {
			// $tzone='Asia/Kolkata';
			// }
			
			$tzone = getTimeZoneFromIpAddress();
			 date_default_timezone_set($tzone);
			
			
			
			
	?>
</head>
<body class="layout-top-nav">
	<div class="wrapper">

<!-- body and div wrapper close in footer-->