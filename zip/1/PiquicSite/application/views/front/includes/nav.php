<?php
$class_name=$this->router->fetch_class();
$method_name=$this->router->fetch_method();


if ($this->session->userdata('front_logged_in')) {
	$session_data = $this->session->userdata('front_logged_in');
	$user_id = $session_data['user_id'];
	$user_name = $session_data['user_name'];
	$type1=$this->session->userdata('type1');
}
else
{
	$user_id = '';
	$user_name = '';
}


$sql="select * from  tbl_upload_img WHERE queue='0' ";

if($user_id!='')
{
	$sql.=" and user_id='$user_id' ";
}


$query=$this->db->query($sql);
$labelimagesinfo=$query->result();


$sql="select * from  tbl_upload_img WHERE queue='1' AND status='0'";

if($user_id!='')
{
	$sql.=" and user_id='$user_id' ";
}


$query=$this->db->query($sql);
$stylequeinfo=$query->result();



$sql="select * from  tbl_style WHERE 1=1 AND status='2'";

if($user_id!='')
{
	$sql.=" and user_id='$user_id' ";
}


$query=$this->db->query($sql);
$stylenavinfo=$query->result();


$query=$this->db->query("select * from  tbl_users  WHERE user_id='$user_id' ");
$userDetails= $query->result_array();
$autostyle_check=$userDetails[0]['autostyle'];

?>
<nav class="navbar navbar-expand-lg navbar-light bg-white border">
	<!-- <a class="navbar-brand" href="#">&nbsp;</a> -->
	<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
		<span class="navbar-toggler-icon"></span>
	</button>
	<div class="collapse navbar-collapse" id="navbarNav">
		<ul class="navbar-nav" style="margin-top: -0.5rem; margin-bottom: -0.5rem;">
			<?php if($type1 == "upload"){ ?>
				<li class="nav-item pr-4 <?php if($class_name == 'upload' && $method_name == 'uploadimage'){ echo "active"; } ?>">
					<a class="nav-link text-uppercase" href="<?php echo base_url();?>uploadimage">Upload Images</a>
				</li>
			<?php } 
			if($type1 == "book"){ ?>
				<li class="nav-item pr-4 <?php if($class_name == 'booking'){ echo "active"; } ?>">
					<a class="nav-link text-uppercase" href="<?php echo base_url();?>booking">Calculate Price</a>
				</li>
			<?php } 

			if($type1 == "upload"){ ?>
				<li class="nav-item pr-4 <?php if($class_name == 'labelimages' && $method_name == 'index'){ echo "active"; } ?>  ">
					<a class="nav-link text-uppercase <?php if(empty($labelimagesinfo)){ echo "disabled"; } ?>  " href="<?php echo base_url();?>label">Label</a>
				</li>
			<?php } 
			if($type1 == "upload" || $type1 == "book"){ 
				if($autostyle_check=='1')
				{
					$style_url = base_url()."autostyle";
				}
				else
				{
					$style_url = base_url()."stylequeue";
				}
				?>

				<li class="nav-item pr-4 <?php if($class_name == 'stylequeue' || $class_name == 'stylequeueview' || $class_name == 'style' || $class_name == 'styleview' ){ echo "active"; } ?>">
					<a class="nav-link text-uppercase <?php if(empty($stylequeinfo)){ echo "disabled"; } ?> " href="<?php echo $style_url;?>">Style</a>
				</li>
			<?php }  
			if($type1 == "upload" || $type1 == "book"){ ?>
				<li class="nav-item pr-4 <?php if($class_name == 'processing' && $method_name == 'index'){ echo "active"; } ?>">
					<?php
					$proc_qry = $this->db->query("SELECT * FROM `tbl_style` WHERE `status` = '2' AND user_id='$user_id'");
					// echo "SELECT * FROM `tbl_style` WHERE `status` = '2' AND user_id='$user_id'";
					$proc_res = $proc_qry->result_array();
					$row_cnt  = count($proc_res);
					?>
					<a class="nav-link text-uppercase <?php if(empty($stylenavinfo)){ echo "disabled"; } ?> " href="<?php echo base_url();?>processing">Processing <span class="badge badge-piquic"><?php echo $row_cnt;?></span></a>
				</li>
			<?php } 
			if($type1 == "upload" || $type1 == "book"){ ?>
				<li class="nav-item pr-4 <?php if($class_name == 'download'){ echo "active"; } ?>">
					<a class="nav-link text-uppercase" href="<?php echo base_url();?>download">Download</a>
				</li>
			<?php } ?>
		</ul>
	</div>
</nav>