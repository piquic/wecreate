<!DOCTYPE html>
<html lang="en">
	<head>
		<!-- Required meta tags -->
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
		<!-- Bootstrap CSS -->
		<link rel="stylesheet" href="<?php echo base_url(); ?>website-assets/css/bootstrap.min.css">

		<!-- <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.1/css/all.css" integrity="sha384-50oBUHEmvpQ+1lW4y57PTFmhCaXp0ML5d60M1M7uH2+nqUivzIebhndOJK28anvf" crossorigin="anonymous"> -->
		<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>website-assets/fontawsome/css/all.css">

		<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>website-assets/css/custom.css">

		<title><?php echo ucwords($page_title); ?> - Piquic</title>

		<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>

	</head>

	<body id="<?php echo str_replace(" ","-",$page_title); ?>">
		<?php $this->load->view('front/includes/menu'); ?>