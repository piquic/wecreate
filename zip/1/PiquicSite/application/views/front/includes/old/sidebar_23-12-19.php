<?php
$second_DB = $this->load->database('another_db', TRUE); 
$class_name=$this->router->fetch_class();
$method_name=$this->router->fetch_method();

$TITLE=$this->router->fetch_method();

if ($this->session->userdata('front_logged_in')) {
	$session_data = $this->session->userdata('front_logged_in');
	$user_id = $session_data['user_id'];
	$user_name = $session_data['user_name'];

}
else
{
	redirect('home');
}



$query16= "SELECT * FROM tbl_users WHERE user_id = '$user_id'";
$query=$this->db->query($query16);
$capvalue16=$query->result_array();
$background_id_select = $capvalue16[0]['background_id'];
$image_size_id_select = $capvalue16[0]['image_size_id'];


if($background_id_select!='' && $background_id_select!='0')
{
	$query16= "SELECT * FROM tbl_background WHERE back_id = '$background_id_select'";
	$query=$second_DB->query($query16);
	$capvalue16=$query->result_array();
	$back_id = $capvalue16[0]['back_id'];
	$back_img = $capvalue16[0]['back_img'];
}
else
{
	$back_id = '';
	$back_img ='';	
}

?>


<!--
<div class="col-12 col-sm-12 col-md-3 col-lg-3 col-xl-3 border">
	<?php //if($script == 'uploadimage') {?>
	<div class="w-100 pt-3 pl-3">
		All Images
	</div>
	<?php //} ?>
</div>
-->


<div class="col-12 col-sm-12 col-md-3 col-lg-3 col-xl-3 border sidescroll">

	<!-- Upload page Sidebar -->
	<?php if($class_name == 'upload') {?>
	<div class="w-100 pt-3 pb-3">
		<div class="nav flex-column nav-pills" id="v-pills-tab" role="tablist" aria-orientation="vertical">
			<a class="nav-link active" id="v-pills-zip-tab" data-toggle="pill" href="#v-pills-zip" role="tab" aria-controls="v-pills-zip" aria-selected="true">
				<span class="lead"><b>Upload multiple SKUs (ZIP file)</b></span><br>
				<small>Upload a zip file containing multiple SKUs.</small>
			</a>

			<a class="nav-link" id="v-pills-skuimg-tab" data-toggle="pill" href="#v-pills-skuimg" role="tab" aria-controls="v-pills-skuimg" aria-selected="false">				
				<span class="lead"><b>Upload a single SKU</b></span><br>
				<small>Enter SKU no. and upload images for the SKU.</small>
			</a>


			<a class="nav-link" id="v-pills-skufolder-tab" data-toggle="pill" href="#v-pills-skufolder" role="tab" aria-controls="v-pills-skufolder" aria-selected="false">				
			<span class="lead"><b>Upload SKU Folder</b></span><br>
			<small>Upload a folder containing multiple SKUs.</small>
			</a>

		</div>
	</div>
	
	<hr class="w-75">




	<?php } ?>



	
	<?php /*if($class_name == 'stylequeue' || $class_name == 'uploadimage') {?>
		<!-- <div class="w-100 pt-3 pl-3">
			All Images
		</div> -->


		<div class="w-100 pt-3 pb-3 pl-3">
			<div class="row">
				<div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
					<div class="custom-control custom-switch">
						<input type="checkbox" class="custom-control-input" id="switchStyle" <?php if($TITLE == 'auto style') {?> checked <?php }?>>
						<label class="custom-control-label chkToggle" for="switchStyle">
							<span class="lead pl-2"><b>AUTO STYLING</b></span><br>
							<small class="pl-2">LET PIQUIC STYLE MY PRODUCTS</small>
						</label>
					</div>
				</div>
			</div>
		</div>
		<hr class="w-75">
	<?php } */ ?>

	<!-- Style page Sidebar -->
	<?php if($class_name == 'style' || $class_name == 'auto style') {

		$skuurl = $skuno;
		$query = $this->db->get_where('tbl_upload_img', array('zipfile_name' => $skuno));
		//$query = $this->db->get("tbl_upload_img");
		$imgresult=$query->result_array();
		//echo "<pre>";
		//print_r($query->result_array());
		$category=$imgresult[0]['category'];
		$gender=$imgresult[0]['gender'];
		$brandid =$imgresult[0]['vendor_id'];
		$client_name =$imgresult[0]['client_name'];
		$second_DB = $this->load->database('another_db', TRUE); 

		$genid=$gender;
		$skuclient_name=$client_name;

		?>


		

		<div class="w-100 pt-3 pl-2">
			<div class="row">
				<div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
					<div class="overflow-hidden thumb_scroll">
						<h5>Backgrounds&nbsp;<small id="bkg-info" data-placement="right" data-content="Select background to add into your style."><i class="far fa-question-circle"></i></small></h5>
						<?php
						$second_DB = $this->load->database('another_db', TRUE); 
						/*$pautoquery = "SELECT * FROM `tbl_vendor_background` INNER JOIN tbl_background ON tbl_vendor_background.backimgid=tbl_background.back_id  WHERE 1=1 AND genid='$genid' AND vendor_id='$brandid'  ";*/

						$pautoquery = "SELECT * FROM `tbl_background`   WHERE 1=1 AND status='1' AND psd_status='1'  ";
						//echo $pautoquery;
						$query2=$second_DB->query($pautoquery);
						$vendor_backgroundinfo= $query2->result_array();

						if(empty($vendor_backgroundinfo))
						{
							?>
							<small class="text-danger"><i class="fas fa-exclamation-circle"></i>&nbsp;Missing</small>
							<?php
						}
						?>
						

						<div class="row">


							<?php
							
							foreach($vendor_backgroundinfo as $mrow)
							{

								$back_id = $mrow['back_id'];
								$back_img = $mrow['back_img'];

							?>
							<div class="col-12 col-sm-12 col-md-4 col-lg-4 col-xl-4 pb-3">
								<img id="bg_image_<?php echo $back_id;?>" onclick="setBackground('<?php echo $user_id;?>','<?php echo $back_id;?>');" class="bg_image_class img-fluid p-1 border rounded-lg <?php if($background_id_select == $back_id) { ?> border-piquic <?php }  ?>" src="<?php echo PIQUIC_URL."/images/thumbnails/bg/".$back_img;?>">

								<span id="chk_bg_<?php echo $back_id;?>" class="chk_bg_class text-piquic slct 
										<?php if($background_id_select != $back_id) { ?> d-none <?php }  ?> "><i class="fas fa-check"></i></span>
							</div>

							<?php	} ?>
						</div>
					</div>
				</div>
			</div>
		</div>

		<hr class="w-75">

		<div class="w-100 pb-3 pl-2">
			<div class="row">
				<div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
					<div class="overflow-hidden thumb_scroll">
						<h5>Image Size&nbsp;<small id="img-size-info" data-placement="right" data-content="Select image size to recieve final images of that size."><i class="far fa-question-circle"></i></small></h5>

						<div class="row">

							<?php
							$pautoquery = "SELECT * FROM `tbl_image_size` where deleted='0' ORDER BY `id` ASC ";
							// echo $pautoquery;
							$query2=$this->db->query($pautoquery);
							$imageinfo= $query2->result_array();

							foreach($imageinfo as $mrow)
							{


								$image_size_id = $mrow['id'];
								$image_size = $mrow['image_size'];

							?>
							<div class="col-12 col-sm-12 col-md-6 col-lg-6 col-xl-6 pb-3" onclick="setImageSize('<?php echo $user_id;?>','<?php echo $image_size_id;?>');" style="cursor:pointer;">

								<div  id="imgsize_<?php echo $image_size_id;?>"  class="imgsize_class w-100 text-center mb-3 pt-3 pb-2 border rounded-lg <?php if($image_size_id_select == $image_size_id) { ?> border-piquic <?php } ?>">
										<?php echo $image_size;?>
								</div>
								
								<span id="chk_imgsize_<?php echo $image_size_id;?>" class="chk_imgsize_class text-piquic slct 
										<?php if($image_size_id_select!=$image_size_id) { ?> d-none <?php }  ?>  "><i class="fas fa-check"></i></span>
							</div>

							<?php } ?>
							</div>
						</div>
					</div>
				</div>
			</div>
			<hr class="w-75">
			<?php } ?>

			<?php if($class_name == 'stylequeue' || $class_name == 'style' || $class_name == 'auto style') {?>
			

			<div class="w-100 pt-3 pl-2">
				<div class="row">
					<div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
						<!-- <div class="overflow-hidden thumb_scroll">
							<h5>Model Selection&nbsp;<small id="model-info" data-placement="right" data-content="Check the SKU(s) and then select model for the styling."><i class="far fa-question-circle"></i></small></h5>

							<small class="text-danger"><i class="fas fa-exclamation-circle"></i>&nbsp;No model selected</small>
						</div> -->

						<!-- <div class="overflow-hidden"> -->
						<h5>Model Selection&nbsp;<small id="model-info" data-placement="right" data-content="Check the SKU(s) and then select model for the styling."><i class="far fa-question-circle"></i></small></h5>
						
						<?php    
						//echo     "SELECT * FROM `tbl_autopose` where client_name='$user_name'";       
						$modselinfoquery =$this->db->query("SELECT * FROM `tbl_autopose` where client_name='$user_name'"); 							
						if ($modselinfoquery->num_rows()==0){						
						?>
						
						<small class="text-danger"><i class="fas fa-exclamation-circle"></i>&nbsp;No model selected&nbsp;<i><a class="text-info" href="<?php echo base_url();?>modelselect" >Click Here...</a></i></small>
						
						<?php }
						else{

							?>
						
<small class="text-danger"><i><a class="text-info" href="<?php echo base_url();?>modelselect" >Click Here For Changes....</a></i></small>
					<div class="mod_scroll">
						<div class="row pt-2">


<?php										
							$pautoquery = "SELECT * FROM `tbl_autopose` WHERE client_name='$user_name' ";
									$query=$this->db->query($pautoquery);
									$pautorow=$query->result_array();
									if(!empty($pautorow))
									{
									$model_id = $pautorow[0]['model_id'];
									}
									else
									{
									$model_id = '';
									}

									$where_arr="";
									if($model_id!=''){
									$where_arr=' AND  m_id in('.$model_id.')   ';
									}
									
									$pautoquery = "SELECT * FROM `tbl_model` WHERE 1=1 ".$where_arr."  AND status='1' ORDER by `m_id` ";                                      
										$query2=$second_DB->query($pautoquery);										
										$modelinfo= $query2->result_array();
										foreach($modelinfo as $mrow){
										$mid = $mrow['m_id'];
										$mod_name = $mrow['mod_name'];
										$mod_name =strtoupper($mod_name);

										$yposequery = "SELECT * FROM `tbl_model_pose` where m_id='$mid' AND pose_status='1' AND  status='1' ;";								
										$query=$second_DB->query($yposequery);
										$ymrows=$query->result_array();
										$ymrow=$ymrows[0];	
										?>

						<div class="col-12 col-sm-12 col-md-4 col-lg-4 col-xl-4 pb-3">
						<img id="model_sel_<?php echo $mid;?>" class="w-100 border rounded model_sel_class" src="<?php echo PIQUIC_URL;?>/images/thumbnails/model_image/<?php echo $mrow['mod_img'];?>" onclick="modlmultskuSidebar('<?php echo $mid;?>');">

						<small><?php echo $mod_name;?></small>

						<span id="span_model_sel_<?php echo $mid;?>" class="text-piquic slct d-none"><i class="fas fa-check"></i></span>
						</div>

						<?php
											 }

											?>


						<!-- <div class="col-12 col-sm-12 col-md-4 col-lg-4 col-xl-4 pb-3">
						<img id="" class="w-100 border rounded border-piquic" src="../piquic/images/thumbnails/model_image/Kri.jpg">

						<span class="text-piquic slct"><i class="fas fa-check"></i></span>
						</div> -->



						</div>
					</div>
							<?php
						} ?>
						<!-- </div> -->



					</div>
				</div>
			</div>
			<hr class="w-75">
			<?php } ?>



<!-- Download Page Sidebar -->
<?php if($class_name == 'download') {?>
	<div class="w-100 pt-3 pl-3">
		<div class="row">
			<div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
				<h3>Display</h3>

				<div class="pl-3 pb-3">
					<div class="custom-control custom-checkbox">
						<input type="checkbox" class="custom-control-input " id="chkStatusAll" name="chkStatusAll"onclick='statusFunc("all");'>
						<label class="custom-control-label chkBox" for="chkStatusAll">All</label>
					</div>

					<div class="custom-control custom-checkbox">
						<input type="checkbox" class="custom-control-input checkBoxStatusClass" value="1" id="status_1" name="1" onclick='statusFunc("1");'>
						<label class="custom-control-label chkBox" for="status_1">
							Downloaded <!-- <span class="badge badge-white">(237)</span> -->
						</label>
					</div>

					<div class="custom-control custom-checkbox">
						<input type="checkbox" class="custom-control-input checkBoxStatusClass" value="2" id="status_2" name="2" onclick='statusFunc("2");'>
						<label class="custom-control-label chkBox" for="status_2">
							Not Downloaded <!-- <span class="badge badge-white">(23)</span> -->
						</label>
					</div>

						<!-- <div class="custom-control custom-checkbox">
							<input type="checkbox" class="custom-control-input checkBoxStatusClass" value="3" id="status_3" name="2" onclick='statusFunc("3");'>
							<label class="custom-control-label chkBox" for="status_3">
								In Review <span class="badge badge-white">(0)</span>
							</label>
						</div> -->
					</div>
				</div>

				<div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
					<h3>Category</h3>

					<div class="pl-3 pb-3">
						<div class="custom-control custom-checkbox">
							<input type="checkbox" class="custom-control-input" id="chkCGAll" name="chkCGAll" onclick='categoryFunc("all");'>
							<label class="custom-control-label chkBox" for="chkCGAll">All</label>
						</div>

						<div class="custom-control custom-checkbox">
							<input type="checkbox" class="custom-control-input checkBoxCategoryClass" id="bottom" name="bottom" value="bottom" onclick='categoryFunc("bottom");'>
							<label class="custom-control-label chkBox" for="bottom">
								Bottom <!-- <span class="badge badge-white">(23)</span> -->
							</label>
						</div>

						<div class="custom-control custom-checkbox">
							<input type="checkbox" class="custom-control-input checkBoxCategoryClass" id="dress" name="dress" onclick='categoryFunc("dress");' value="dress">
							<label class="custom-control-label chkBox" for="dress">
								Dress <!-- <span class="badge badge-white">(237)</span> -->
							</label>
						</div>
						
						<div class="custom-control custom-checkbox">
							<input type="checkbox" class="custom-control-input checkBoxCategoryClass" id="top" name="top" onclick='categoryFunc("top");'>
							<label class="custom-control-label chkBox" for="top">
								Top <!-- <span class="badge badge-white">(0)</span> -->
							</label>
						</div>
					</div>
				</div>

				<div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
					<h3>Gender</h3>

					<div class="pl-3 pb-3">
						<div class="custom-control custom-checkbox">
							<input type="checkbox" class="custom-control-input" id="chkGDAll" name="chkGDAll" onclick="genderFunc('all')">
							<label class="custom-control-label chkBox" for="chkGDAll">All</label>
						</div>

						<div class="custom-control custom-checkbox">
							<input type="checkbox" class="custom-control-input checkBoxGenderClass" id="1" value="1" onclick="genderFunc('1')" >
							<label class="custom-control-label chkBox" for="1">
								Male <!-- <span class="badge badge-white">(23)</span> -->
							</label>
						</div>

						<div class="custom-control custom-checkbox">
							<input type="checkbox" class="custom-control-input checkBoxGenderClass" id="2" value="2" onclick="genderFunc('2')">
							<label class="custom-control-label chkBox" for="2">
								Female <!-- <span class="badge badge-white">(237)</span> -->
							</label>
						</div>
						
						<div class="custom-control custom-checkbox">
							<input type="checkbox" class="custom-control-input checkBoxGenderClass" id="3" value="3" onclick="genderFunc('3')">
							<label class="custom-control-label chkBox" for="3">
								Boy <!-- <span class="badge badge-white">(0)</span> -->
							</label>
						</div>
						
						<div class="custom-control custom-checkbox">
							<input type="checkbox" class="custom-control-input checkBoxGenderClass" id="4" value="4" onclick="genderFunc('3')">
							<label class="custom-control-label chkBox" for="4">
								Girl <!-- <span class="badge badge-white">(9)</span> -->
							</label>
						</div>
					</div>
				</div>
			</div>
		</div>
		<hr class="w-75">
	<?php } ?>

<!-- Create New SKU -->
	<?php if($class_name == 'labelimages') {?>
				<div class="w-100 pt-3 pl-3">
			<div class="row">
				<div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
					<p class="text-piquic"><i class="fas fa-plus-square"></i>&emsp;<span class="h5">Create New SKU</span></p>

					<div class="pb-3 pr-3">
						<form id="sidebar_sku_form" name="sidebar_sku_form" action="">
							<div class="form-row">
								<div class="form-group">
									<label for="txtSKUNew" class="lblOver text-piquic">&nbsp;SKU #&nbsp;</label>
									<input type="text" class="form-control" id="txtSKUNew" name="txtSKUNew">
								</div>
							</div>

							<div class="form-row">
								<div id="upldImg" class="col-12">
									<span class="w-100">UPLOAD IMAGES</span><br>
									<small class="text-muted">
										Supports only JPG. Max. 10 files per upload.
									</small>
									<!-- <label for="btnUpldImg" class="btn btn-piquic btn-block" style="letter-spacing: .09rem;">
										UPLOAD IMAGES
									</label>

									<input type="file" id="btnUpldImg" name="btnUpldImg" class="form-control d-none"> -->

									<label for="sku_files" class="btn btn-piquic btn-block" >
										UPLOAD IMAGES
									</label>

									<input type="file" class="form-control d-none upload_sku_files fileInput css_uploadFile" id="sku_files"  name="sku_files[]" multiple  />
								</div>

								<div id="skuImageUploadDiv" class="col-12 pt-1">
									<div class="d-none" id="div_ctn_img">
										<small><b><span id='ctn_img'></span> IMAGES UPLOADED</b></small>
									</div>
								</div>

								<input  type='hidden'  class="form-control" id="pimg" name="pimg" value=''>

								<button type="submit" class="btn btn-piquic btn-block d-none" id="btnCretSKU" name="btnCretSKU">CREATE SKU</button>
								<button type="button" class="btn btn-outline-piquic btn-block" id="btnUpldCancel" name="btnUpldCancel">CANCEL</button>
							</div>
						</form>
					</div>
				</div>
			</div>
		</div>
		<hr class="w-75">
		<?php } ?>	
		<!-- Booking page Sidebar -->
		<?php if($class_name == 'booking') { 
			if(!empty($book_id)){
				$order_id=$booking_data[0]['order_id'];
				$gender=$booking_data[0]['gender'];
				$category=$booking_data[0]['category'];

				if(!empty($category))
				{
					$category_arr=array_filter(explode(",", $category));
				}
				$quty=$booking_data[0]['quty'];
				if(!empty($quty))
				{
					$total_quty=array_sum(array_filter(explode(",", $quty)));
				}
				$angle=$booking_data[0]['angle'];
				$m_status=$booking_data[0]['m_status'];
				$d_status=$booking_data[0]['d_status'];
				$notes=$booking_data[0]['notes'];
				$total_price=$booking_data[0]['total_price'];
			}
			else{
				$order_id="";
				$gender="";
				$category="";
				$quty="";
				$total_quty="";
				$angle="";
				$m_status="";
				$d_status="";
				$notes="";
				$total_price="";
			}
			?>
			<div class="w-100 pt-3 pb-3">
				<div class="row">
					<div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
						<div class="card">
							<div class="card-body">
								<h5 class="card-title">Booking Details</h5>
								<hr>
								<p class="card-text">
									<i class="fas fa-check text-piquic"></i>&emsp;<b>Gender:</b>&nbsp;
									<lable id="lbl_gender">
										<?php 
										if($gender=="1"){ echo "Male" ; } 
										if($gender=="2"){ echo "Female" ; } 
										if($gender=="4"){ echo "Boy" ; } 
										if($gender=="6"){ echo "Girl" ; } 
										?>

									</lable><br><br>
									<i class="fas fa-check text-piquic"></i>&emsp;<b>Category:</b>&nbsp;
									<?php
									if(!empty($category_arr)){
										$cat_count=count($category_arr);
										foreach ($category_arr as $key => $value) {
											if($key=="1")
											{
												?>
												<lable id="lbl_cat1"><?php echo $value.", "; ?></lable>
												<?php
											}
											if($key=="2")
											{
												?>
												<lable id="lbl_cat2"><?php echo $value.", "; ?></lable>
												<?php
											}
											if($key=="3")
											{
												?>
												<lable id="lbl_cat3"><?php echo $value.", "; ?></lable>
												<?php
											}

										}
										if($cat_count=="1")
										{
											?>
											<lable id="lbl_cat2"></lable>
											<lable id="lbl_cat3"></lable>
											<?php
										}
										if($cat_count=="2")
										{
											?>
											<lable id="lbl_cat3"></lable>
											<?php
										}
									}
									else{
										?>
										<lable id="lbl_cat1"></lable>
										<lable id="lbl_cat2"></lable>
										<lable id="lbl_cat3"></lable>
										<?php
									}
									?>

									<br><br>
									<i class="fas fa-check text-piquic"></i>&emsp;<b>Qty:</b>&nbsp;
									<lable id="lbl_qty" >
										<?php 
										if(!empty($total_quty)){ 
											echo $total_quty; 
										} 
										else{ 
											echo "0.00"; 
										} 
										?>
									</lable><br><br>
									<i class="fas fa-check text-piquic"></i>&emsp;<b>Delivery:</b>&nbsp;
									<lable id="lbl_d_status">
										<?php 
										if($d_status=="1"){
											echo "Standard (6-7 days after receiving your products)." ; 
										} 
										else{ 
											echo "Express (2-3 days after receiving your products)."; 
										} 
										?>	
									</lable><br><br>
									<span class="d-flex justify-content-between">
										<span class="h5">Total:</span>

										<span class="h5">
											<?php
											$query = $this->db->get_where('tbl_users', array('user_id'=>$user_id));
											
											$result=$query->result_array(); 
											$curr=$result[0]['user_currency'];
											if($curr=="USD")
											{
												echo"&#36;";
											}
											elseif($curr=="INR")
											{
												echo"&#x20B9;";	
											}
											elseif($curr=="EUR")
											{
												echo"&#128;";
											}
											elseif($curr=="GBP")
											{
												echo"&#163;";
											}
											elseif($curr=="AUD")
											{
												echo"&#36;";
											}
											elseif($curr=="CNY")
											{
												echo"&#165;";
											}
											?>

											<lable id="lbl_total" ><?php if(!empty($total_price)){ echo $total_price; } else{ echo "0.00"; } ?></lable></span>
										</span>
									</p>	
								</div>
							</div>
							<button class="btn btn-piquic w-100 mt-3" type="submit" id="btnBook" form="booking_form" name="btnBook">CONFIRM BOOKING</button>
						</div>	
					</div>
				</div>

				<hr class="w-75">
			<?php } ?>
	</div>

	<script type="text/javascript">

	// 	$('#switchStyle').on('click', function(){

	// 	// $('switchStyle').prop('checked', true);

	// 	var chked = document.getElementById('switchStyle').checked;

	// 	if( chked == true){
	// 		window.location = "auto_style.php";
	// 	} else {
	// 		window.location = "style.php";
	// 	}
	// });

		$('#btnUpldCancel').on('click', function(){
			location.reload();
		});

		$('#bkg-info').on('mouseover',function() {
			$('#bkg-info').popover('show');
		});

		$('#bkg-info').on('mouseout',function() {
			$('#bkg-info').popover('hide');
		});

		$('#img-size-info').on('mouseover',function() {
			$('#img-size-info').popover('show');
		});

		$('#img-size-info').on('mouseout',function() {
			$('#img-size-info').popover('hide');
		});

		$('#model-info').on('mouseover',function() {
			$('#model-info').popover('show');
		});

		$('#model-info').on('mouseout',function() {
			$('#model-info').popover('hide');
		});















 $(".upload_sku_files").on("change", function(e) {
	
//alert("goko");

     $("#loader").show();
	 
	
      var files = e.target.files,
        filesLength = files.length;
        var f = files[0];
        var fileReader = new FileReader();
        fileReader.onload = (function(e) {
          var file = e.target;
		  
		  console.log(e.target.result);
		  
         /* $("<span class=\"pip\">" +
            "<img class=\"imageThumb\" src=\"" + e.target.result + "\" title=\"" + file.name + "\"/>" +
            "<br/><span class=\"remove\">Remove</span>" +
            "</span>").insertAfter("#files");*/
			
			
			
			
	//alert(no);
  var input = document.getElementById("sku_files");
  var inputArr=input.files;

    for(var k=0;k<inputArr.length;k++)
    {
       var file=inputArr[k];

       //alert(file);
        console.log(file);
        //console.log("lllllllll");
		if(file != undefined  && (file.type=='image/png' || file.type=='image/jpg'   || file.type=='image/jpeg') ){
	formData= new FormData();
        	formData.append("image", file);


        	var data = $.ajax({
			url: "<?php echo base_url();?>front/labelimages/upload_files_fromsku",
        	type: "POST",
        	data: formData,
        	processData: false,
        	contentType: false,
			async: false
			}).responseText;



				 //alert("koko");
	     //alert(data);

	     var pimgVal=$.trim($("#pimg").val());
	     if(pimgVal!='')
	     {
	     	var item_chek=","+$.trim(data)+",";
	     	var check=(pimgVal.indexOf(item_chek));
	     	  //alert(check);
	     	  var pimgValItem=pimgVal+$.trim(data)+",";



	     	}
	     	else
	     	{
	     		var pimgValItem=","+$.trim(data)+",";

	     	}
	     	
	     	$("#pimg").val(pimgValItem);

	     	var pimgValCheckCount=$.trim($("#pimg").val());
	     	var pimgValCheckCount = pimgValCheckCount.replace(/^,|,$/g, '');

	     	var checkArr=pimgValCheckCount.split(",");
	     	var checkLength=checkArr.length;

		      

		      if(checkLength<=10)
		      {

                //alert(show_ctn_img);

                var show_pimg="<div class='col-6 col-sm-6 col-md-3 col-lg-3 col-xl-3'><img class='w-100 border rounded mb-2' src='temp_upload/<?php echo $user_id;?>/"+"thumb_"+data+"' ></div>";
                $("#skuImageUploadDiv").append(show_pimg);

                $("#div_ctn_img").removeClass("d-none");
                $("#ctn_img").html(checkLength);


             }
             else
             {
             	alert("You exceed your limit. Only 10 images you can upload at a time");
             }


             
             var lastCount=parseInt(inputArr.length)-1;

            // alert(k+"---"+lastCount);
		      if(k==lastCount)
		      {
		      	// $("#loader").hide();

                   var ctn=0;
					$("img").on('load', function(){

						//console.log(this.length);
					   //console.log(this);
					//alert("pp");

					if (this.complete) {
					// this image already loaded
					// do whatever you would do when it was loaded
					console.log("completed");
					//alert("completed");
					//alert(ctn);
					if(ctn==lastCount)
					{
						$("#loader").hide();
					}
					else
					{
						ctn++;
					}
					
					} else {

					$(this).on('load', function() {
					// image now loaded
					});

					}

					});

					
		      }




		}
		else
		{
               $("#loader").hide();
		    	alert('Uploaded file is not an image!');
		    	//return false;
		    	window.location.href="<?php echo base_url();?>label";
		}




    }


          
        });
        fileReader.readAsDataURL(f);

        $('#upldImg').hide();
        $('#btnCretSKU').removeClass('d-none');
      
    });







jQuery(function ($) {
	   // $('.content-wrapper').css('min-height','900px');	   $('#entity_id').select2();
	   $('#sidebar_sku_form').validate({

	   	rules: {

	   		txtSKUNew:{
	   			required:true,
	   			minlength: 3,
	   			remote: {
				url: "<?php echo base_url();?>front/upload/checkexistsku",
				type: "post"
				},
	   		},
	   		

           },

            messages: {
				txtSKUNew:{
				remote:"This sku already exists."
				},
            },
	   		submitHandler: function(form) {

	   			 $("#loader").show();
	   			 // var formData = new FormData(form);
	   			var user_id='<?php echo $user_id;?>';
	   			var txtSKUNew=$('#txtSKUNew').val();
	   			var pimg=$('#pimg').val();

				$.ajax({
				method:'POST',
				url: "<?php echo base_url();?>front/upload/insertSKUFromSidebar", 
				data:"&user_id="+user_id+"&txtSKUNew="+txtSKUNew+"&pimg="+pimg,
				success: function(result){
				if(result)
				{
					$("#loader").hide();
				//consu;le
				//console.log(result);
				//alert(result);					
				//var val=$.trim(result);
				//alert(val);
				window.location.href="<?php echo base_url();?>label";

				
				}
				}
				});
	

	
			}, 


		});	
	});	






	</script>