<?php
if ($this->session->userdata('front_logged_in')) {
            $session_data = $this->session->userdata('front_logged_in');
            $user_id = $session_data['user_id'];
            $user_name = $session_data['user_name'];
            $type1=$this->session->userdata('type1');
            }
            else
            {
            $user_id = '';
            $user_name = '';
            }
?>
<header class="bg-white border-bottom">
	<div class="d-flex justify-content-between">
		<div class="p-2">
			<a href="<?php echo base_url();?>"><img class="img-fluid" src="<?php echo PIQUIC_URL;?>images/logo-piquic-md.png"></a>
		</div>

		<?php if(!empty($user_id)){ ?>
		<div class="dropdown pt-3 pr-1 text-center">
			<img id="img-user" class="login-img float-right" src="<?php echo PIQUIC_URL;?>images/user.jpg"/>

			<a class="btn dropdown-toggle" href="#" role="button" id="dropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
				<?php echo $user_name;?>
			</a>
			 
			<div class="dropdown-menu dd-piquic" aria-labelledby="dropdownMenuLink">
				<?php if( $type1=='upload' || $type1=='book') { ?>
				<a class="dropdown-item" href="<?php echo base_url();?>myaccount">My Account</a>
				<?php } ?>

				<?php if( $type1=='upload') { ?>
					<a class="dropdown-item" href="<?php echo base_url();?>plans">My Plans</a>
				<?php } else if($type1=='book') { ?>
					<a class="dropdown-item" href="<?php echo base_url();?>order">My Order</a>
				<?php } ?>
				
				<!-- <div class="dropdown-divider"></div> -->
				<a class="dropdown-item" href="<?php echo base_url();?>logout">Logout</a>
			</div>			
		</div>
		<?php } ?>
	</div>
	<!-- <nav class="navbar navbar-expand-lg navbar-light bg-white border-bottom">
		<a class="navbar-brand" href="<?php echo base_url();?>"><img class="img-fluid" src="<?php echo PIQUIC_URL;?>images/logo-piquic-md.png"></a>
		<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarCollapse" aria-controls="navbarCollapse" aria-expanded="false" aria-label="Toggle navigation">
			<span class="navbar-toggler-icon"></span>
		</button>

		<div class="collapse navbar-collapse" id="navbarCollapse">
			<ul class="navbar-nav mr-auto ml-auto">
				<li class="nav-item pl-2 pr-2">
					<a class="nav-link" href="#">Explore</a>
				</li>
				<li class="nav-item pl-2 pr-2">
					<a class="nav-link" href="#">Team</a>
				</li>
				<li class="nav-item pl-2 pr-2">
					<a class="nav-link" href="#">Pricing</a>
				</li>
				<li class="nav-item pl-2 pr-2">
					<a class="nav-link" href="#">Book Now</a>
				</li>
			</ul>
			<?php if(!empty($user_id)){
				?>
             <ul class="navbar-nav ml-auto">
				<li class="nav-item dropdown">
					<img id="img-user" class="login-img float-md-left" src="<?php echo PIQUIC_URL;?>images/user.jpg"/>

					<a class="nav-link float-md-left dropdown-toggle" href="#" id="navbarDropdownMenuLink" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><?php echo $user_name;?></a>

					<div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
						<a class="dropdown-item" href="<?php echo base_url();?>myaccount">My Account</a>
						<a class="dropdown-item" href="<?php echo base_url();?>plans">My Plans</a>
						<a class="dropdown-item" href="<?php echo base_url();?>logout">Logout</a>
					</div>
				</li>
			</ul>
				<?php

			}
			?>
		</div>
	</nav> -->
</header>