<?php
session_start();
error_reporting(0);
$data['page_title'] = "review booking";

$this->load->view('front/includes/header',$data);
  if ($this->session->userdata('front_logged_in')) {
    $session_data = $this->session->userdata('front_logged_in');
     $user_id = $session_data['user_id'];
     $user_name = $session_data['user_name'];
     $_SESSION['user_id']=$user_id;
     $_SESSION['user_name']=$user_name;
    }
    else
    {
    $user_id = '';
    $user_name = ''; 
    $_SESSION['user_id']=$user_id;
     $_SESSION['user_name']=$user_name;
    }
$query = $this->db->get_where('tbl_users', array('user_id'=>$user_id));
		//$query = $this->db->get("tbl_upload_img");
$result=$query->result_array(); 
$currency=$result[0]['user_currency'];
if($currency=="USD")
{
	$currency_sym= "&#36;";
}
elseif($currency=="INR")
{
	$currency_sym="&#x20B9;";	
}
elseif($currency=="EUR")
{
	$currency_sym="&#128;";
}
elseif($currency=="GBP")
{
	$currency_sym="&#163;";
}
elseif($currency=="AUD")
{
	$currency_sym="&#36;";
}
elseif($currency=="CNY")
{
	$currency_sym="&#165;";
}
// $this->load->view('front/includes/menu');
$this->load->view('front/includes/nav');


$order_id=$booking_data[0]['order_id'];
$gender=$booking_data[0]['gender'];
$category=$booking_data[0]['category'];
$quty=$booking_data[0]['quty'];
$total_quty="";
if(!empty($quty))
{
	$total_quty=array_sum(array_filter(explode(",", $quty)));
}

$angle=$booking_data[0]['angle'];
$m_status=$booking_data[0]['m_status'];
$d_status=$booking_data[0]['d_status'];
$notes=$booking_data[0]['notes'];
$total_price=$booking_data[0]['total_price'];
$total_image=$booking_data[0]['total_image'];
$plan_id=$booking_data[0]['plan_type'];
$plan_left_amt=$booking_data[0]['plan_left_amt'];
$plan_left_credit=$booking_data[0]['plan_left_credit'];
$plan_used_amt=$booking_data[0]['plan_used_amt'];
$plan_used_credit=$booking_data[0]['plan_used_credit'];
$p_status=$booking_data[0]['p_status'];
?>

<div class="container-fluid">
	<div class="row">

		<?php //$this->load->view('front/includes/sidebar'); ?>

		<!-- <div class="col-12 col-sm-12 col-md-9 col-lg-9 col-xl-9 border mainscroll"> -->
		<div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12 border mainscroll">
			<div class="container pt-3">
				<div class="row mb-4">
					<div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
						<h1>Review your booking</h1>
					</div>
				</div>
			</div>

			<div class="container pt-3">
				<div class="row mb-4">
					<div class="col-12 col-sm-12 col-md-8 col-lg-8 col-xl-8">
						
						<div class="card mb-3">
							<div class="card-body">
								<h5 class="card-title">
									<div class="d-flex justify-content-between">
										<span>Booking Details</span>
										<a class="btn btn-piquic btn-sm" href="<?php echo base_url();?>booking/<?php echo $book_id ?>">&emsp;Edit&emsp;</a>
								</h5>
								<hr>
								<p class="card-text">
									<i class="fas fa-check text-piquic"></i>&emsp;<b>Gender:</b>
									<?php 
									if($gender=="1"){
										echo "Male";
									}
									if($gender=="2"){
										echo "Female";
									}
									if($gender=="4"){
										echo "Boy";
									}
									if($gender=="6"){
										echo "Girl";
									}

									?>
									<br><br><i class="fas fa-check text-piquic"></i>&emsp;<b>Category:</b>&nbsp;<?php echo trim($category,',');?>
									<br><br><i class="fas fa-check text-piquic"></i>&emsp;<b>Qty:</b>&nbsp;<?php echo $total_quty; ?>
									<br><br><i class="fas fa-check text-piquic"></i>&emsp;<b>Delivery:</b>&nbsp;<?php 
									if($d_status=="1"){
										echo "Standard (6-7 days after receiving your products).";
									}
									if($d_status=="2"){
										echo "Express (2-3 days after receiving your products).";
									}
									
									?>&nbsp;
									<!-- <a href="<?php echo base_url();?>booking/<?php echo $book_id ?>">Change</a> -->
								</p>
							</div>
						</div>

						<div class="card mb-3">
							<div class="card-body">
								<h5 class="card-title">Delivery Address</h5>
										<hr>
										<p class="card-text">
											Deliver your products samples to the following address:
										</p>
										<div class="card-text" id="address">
											
											<div style="font-weight: 400; font-size: 2em; color: #76aea1;">Moksha Designs - Piquic Studios</div>
											<div style="width: 75%;">
												<hr style="color: #76aea1; ">
											</div>
											<div style="font-size: 1.25rem; font-weight: 400;">
												Sultan Sadan 1, Westend Marg Lane 3,
												<br>Saidulajab, Delhi - 110030
												<br>India
											</div><br><br>

											<div style="width: 10%; float: left; font-size: 1rem; font-weight: 300; text-align: right; padding-right: 1em;">
												FROM
											</div>
											<div style="width: 90%;  float: left; font-size: 1.25rem; font-weight: 300; border-left: 1px solid #acacac; padding-left: 1em;">
												<div><?php echo $user_name; ?></div>
												<div style="text-transform: uppercase;"><?php echo $user_company; ?></div>
												<div style="width: 60%;"><?php echo $user_address; ?></div>
												<div>Ph: <?php echo $user_mobile; ?></div>
												<div style="font-weight: 400;">Order ID - <?php echo $order_id; ?></div>
											</div>
										</div>

										<input class="btn btn-outline-secondary mt-3 px-5" id="btnPrintLabel" type="button" value="PRINT DELIVERY LABEL" onclick="printDiv()">  
							
							</div>
						</div>
					</div>
					
					<div class="col-12 col-sm-12 col-md-4 col-lg-4 col-xl-4">
						<div id="success_message" style="display:none;">
				        <div class="alert alert-success">
				          	<strong>Your Booking has beed Placed</strong>.
				        </div>
				    </div>
			        <div id="error_message" style="display:none;">
				        <div class="alert alert-danger">
				        	<strong>Something wrong in your Booking</strong>.
				        </div>
			        </div>
						<div class="card mb-3">
							<div class="card-body">
								<p class="card-text">
									<span class="d-flex justify-content-between">
										<span class="h5">Total:</span>

										<span class="h5"><?php echo $currency_sym ?><?php echo $total_price ?> </span>
									</span>
								</p>
								<button class="btn btn-piquic w-100 mt-3" id="btnPlaceOrder">PLACE YOUR ORDER</button>
								<button class="btn btn-piquic w-100 mt-3" id="btnCancelOrder">CANCEL YOUR ORDER</button>
							</div>
						</div>						
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<?php $this->load->view('front/includes/footer'); ?>

<script type="text/javascript">
$(document).ready(function() {
	$("#btnPlaceOrder").click(function () {
		var tmb_book_id="<?php echo $book_id; ?>";
		var order_id="<?php echo $order_id; ?>";
		var user_id="<?php echo $user_id; ?>";
		var gender="<?php echo $gender; ?>";
		var category="<?php echo $category; ?>";
		var quty="<?php echo $quty; ?>";
		var angle="<?php echo $angle; ?>";
		var m_status="<?php echo $m_status; ?>";
		var d_status="<?php echo $d_status; ?>";
		var notes="<?php echo $notes; ?>";
		var total_price='<?php echo $total_price;?>';
		var total_image='<?php echo $total_image;?>';
		var plan_id='<?php echo $plan_id;?>';
		var currency='<?php echo $currency;?>';
		var plan_left_amt="<?php echo $plan_left_amt; ?>";
		var plan_left_credit="<?php echo $plan_left_credit; ?>";
		var plan_used_amt="<?php echo $plan_used_amt; ?>";
		var plan_used_credit="<?php echo $plan_used_credit; ?>";
		var p_status="<?php echo $p_status; ?>";

		/*var dataa = {};
		dataa.tmb_book_id = "<?php echo $book_id; ?>";
		dataa.order_id = "<?php echo $order_id; ?>";
		dataa.user_id = "<?php echo $user_id; ?>";
		dataa.gender = "<?php echo $gender; ?>";
		dataa.category = "<?php echo $category; ?>";
		dataa.quty = "<?php echo $quty; ?>";
		dataa.angle = "<?php echo $angle; ?>";
		dataa.m_status = "<?php echo $m_status; ?>";
		dataa.d_status = "<?php echo $d_status; ?>";
		dataa.notes = "<?php echo $notes; ?>";
		dataa.total_price = "<?php echo $total_price; ?>";
		dataa.total_image = "<?php echo $total_image; ?>";
		dataa.plan_id = "<?php echo $plan_id; ?>";
		dataa.currency = "<?php echo $currency; ?>";
		dataa.plan_left_amt = "<?php echo $plan_left_amt; ?>";
		dataa.plan_left_credit = "<?php echo $plan_left_credit; ?>";
		dataa.plan_used_amt = "<?php echo $plan_used_amt; ?>";
		dataa.plan_used_credit = "<?php echo $plan_used_credit; ?>";
		dataa.p_status = "<?php echo $p_status; ?>";
		

		var myJSONText = JSON.stringify(dataa);*/

		$("#loader").show();
		$.ajax({
          method:'post',
          url: "<?php echo base_url();?>front/revbook/add_book", 
          data:"&tmb_book_id="+tmb_book_id+"&user_id="+user_id+"&order_id="+order_id+"&gender="+gender+"&category="+category+"&quty="+quty+"&angle="+angle+"&m_status="+m_status+"&d_status="+d_status+"&notes="+notes+"&total_price="+total_price+"&total_image="+total_image+"&plan_id="+plan_id+"&currency="+currency+"&plan_left_amt="+plan_left_amt+"&plan_left_credit="+plan_left_credit+"&plan_used_amt="+plan_used_amt+"&plan_used_credit="+plan_used_credit+"&p_status="+p_status,
          /*data:{data:myJSONText},*/
          // contentType: false,
          // processData: false,
          // data:formData,
          success: function(result){
          	//alert(result);
          	if(result!=""){
          		$("#loader").hide();
          		if(result=="Paid")
          		{
          			window.location = "<?php echo base_url();?>booksuccess/";
          		}
          		else{
          			window.location = "<?php echo base_url();?>booking_paymentdetails/"+result ;
          		}
	//
          		// $("#success_message").show();
          		// $("#error_message").hide();
          	}
          	else if(result=="0"){
          		alert("Wrong plan type!.");
          	}
  			else{
  				//window.location = "<?php echo base_url();?>bookfailure" ;
  				location.reload(true);
  			}	       
          }
          // error:function(result){
          // 	exit();
          // }
        });
	});

		$("#btnCancelOrder").click(function () {
		var tmb_book_id="<?php echo $book_id; ?>";
		$("#loader").show();
		$.ajax({
          method:'post',
          url: "<?php echo base_url();?>front/revbook/delete_book", 
          data:"&tmb_book_id="+tmb_book_id,
          // contentType: false,
          // processData: false,
          // data:formData,
          success: function(result){
          	//alert(result);
          	if(result!=""){
          		$("#loader").hide();
          		window.location = "<?php echo base_url();?>booking";

          		// $("#success_message").show();
          		// $("#error_message").hide();
          	}
  			else{
  				//window.location = "<?php echo base_url();?>bookfailure" ;
  				location.reload(true);
  			}	       
          }
          // error:function(result){
          // 	exit();
          // }
        });
	});
});

 function printDiv() { 
            var divContents = document.getElementById("address").innerHTML; 
            var a = window.open('Delivery Label', '', 'height=500, width=500'); 
            a.document.write('<html>'); 
            a.document.write('<body>');  
            a.document.write(divContents); 
            a.document.write('<br><br>'); 
            // a.document.write(divContents); 
            // a.document.write('<br><br>'); 
            // a.document.write(divContents); 
            // a.document.write('<br><br>'); 
            // a.document.write(divContents); 
            a.document.write('</body></html>'); 
            a.document.close(); 
            a.print(); 
        } 

</script>