<?php

$data['page_title'] = "plans";



if ($this->session->userdata('front_logged_in')) {
            $session_data = $this->session->userdata('front_logged_in');
            $user_id = $session_data['user_id'];
            $user_name = $session_data['user_name'];
            
			
         
        } else {
        	
            $session_data = $this->session->userdata('front_logged_in');
            $user_id = $session_data['user_id'];
            $user_name = $session_data['user_name'];
            
		
        }

        //echo $this->session->userdata('sess_plan_id')."kkk";
if(!empty($this->session->userdata('sess_plan_id')))
{
$sess_currency = $this->session->userdata('sess_currency');
$sess_plan_id = $this->session->userdata('sess_plan_id');
$plansDetails = $this->plans_model->get_plansById($sess_plan_id);
$hid_plantype_id=$plansDetails[0]['plntyid'];
$hid_plan_id=$plansDetails[0]['planid'];
$hid_plan_amount=$plansDetails[0]['plan_amount'];
$hid_plan_credit=$plansDetails[0]['plan_credit'];

}
else
{
$sess_currency = '';
$sess_plan_id = '';
$hid_plantype_id= '';
$hid_plan_id= '';
$hid_plan_amount= '';
$hid_plan_credit= '';
}

if($hid_plan_credit=='')
{
$hid_plan_credit='0';
}

?>


								<div class="tab-pane fade show active" id="plans-slct" role="tabpanel" aria-labelledby="plans-slct-tab">
									<div class="row p-2">

                   <?php
                   //echo "<pre>";
                   //print_r($plansDetails);
                   if(!empty($plansTypeDetails))
                   {
                   	foreach($plansTypeDetails as $key => $planrow)
                   	{
                   		$plntyid=$planrow['plntyid'];
                   		$plntypname=$planrow['plntypname'];
                   		$plandetails=$planrow['plandetails'];
                   		
					?>
                      <div class="col-12 col-sm-12 col-md-6 col-lg-6 col-xl-6">
											<div class="p-4">
												<p class="lead"><?php echo $plntypname;?></p>
												<small class="text-muted">
													Designed for infrequent sending, you buy images ( as opposed to recurring charges) as necessary, based on volume.
												</small>

												<div class="row pt-3 <?php if($plntyid=='1'){ echo 'clickbone-group'; } if($plntyid=='2'){ echo 'clickbmon-group'; } ?>">
													<div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12 pb-3">
														<small><b>How many images would you like to purchase?</b></small>
													</div>

 <?php
                   //echo "<pre>";
                   //print_r($plandetails);
                   if(!empty($plandetails))
                   {
                   	foreach($plandetails as $key1 => $planrow1)
                   	{
														$planid=$planrow1['planid'];
														$plan_name=$planrow1['plan_name'];
														$plan_amount=$planrow1['plan_amount'];
														$plan_credit=$planrow1['plan_credit'];
														$plan_currency=$planrow1['plan_currency'];
														$plan_currency_symbol=$planrow1['plan_currency_symbol'];
														$plan_per_image=$planrow1['plan_per_image'];

														
                   		
										?>

				<div class="col-12 col-sm-12 col-md-6 col-lg-6 col-xl-6 <?php if($plntyid=='1'){ echo 'clickb'; } if($plntyid=='2'){ echo 'clickbmon'; } ?> " style="cursor: pointer;" onclick="setAmount('<?php echo $plntyid;?>','<?php echo $planid;?>','<?php echo $plan_amount;?>','<?php echo $plan_credit;?>');">
				<div id="planSelectbox<?php echo $planid;?>" class="planSelectbox w-100 text-center mb-3 py-3 border <?php if($sess_plan_id==$planid) { ?> border-piquic <?php } ?> ">
				<?php echo $plan_credit;?> Images<br>
				<b><?php echo $plan_currency_symbol;?><?php echo $plan_amount;?><?php if($plntyid == '2') { echo "/month"; } ?></b>
				<br>
				<small class="text-muted">(<?php echo $plan_currency_symbol;?><?php echo $plan_per_image;?> per image)</small>
				</div>
				<span id="planSelectboxTick<?php echo $planid;?>" class="planSelectboxTick text-piquic slct <?php if($sess_plan_id!=$planid) { ?> d-none <?php } ?> " id="ticko1" ><i class="fas fa-check"></i></span>
				</div>

													

										<?php
                   	}

                   }
                   ?>		




												</div>
											</div>
										</div>


										<?php
                   	}

                   }
                   ?>

							


									</div>
								</div>

								
<script type="text/javascript">

	function changePlan(currency)
	{
//alert(currency);
		$.ajax({
				type:'POST',					
				url : "<?php echo base_url();?>front/plans/getPlanDetailByCurrency", 
				data:{'currency':currency},
				success: function(response) {	
				 //alert(response);
				//location.reload();
				$("#plans-slct").html($.trim(response));
				}
				});
	}
</script>