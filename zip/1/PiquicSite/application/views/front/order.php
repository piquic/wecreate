<?php

session_start();
error_reporting(0); 
$data['page_title'] = "Order";

$this->load->view('front/includes/header',$data);
// $this->load->view('front/includes/menu');
$this->load->view('front/includes/nav');

if ($this->session->userdata('front_logged_in')) {
	$session_data = $this->session->userdata('front_logged_in');
	$user_id = $session_data['user_id'];
	$user_name = $session_data['user_name'];
	
	
	
} else {
	
	$session_data = $this->session->userdata('front_logged_in');
	$user_id = $session_data['user_id'];
	$user_name = $session_data['user_name'];
	
	
}

?>
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/bs4/dt-1.10.18/datatables.min.css"/>

<div class="container-fluid">
	<div class="row">

		<?php //$this->load->view('front/includes/sidebar'); ?>

		<!-- <div class="col-12 col-sm-12 col-md-9 col-lg-9 col-xl-9 border mainscroll"> -->
			<div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12 mainscroll">

				<div class="container pt-3">
					<div class="row mb-4">
						<div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
							<h1>My Order </h1>
						</div>
					</div>

					<!--  -->

					<div class="row my-4">					
						<div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
							<div class="full-w">
								<ul class="nav nav-tabs" id="myTab" role="tablist">
									<li class="nav-item px-3 text-uppercase">
										<a class="nav-link active" id="plans-slct-tab" data-toggle="tab" href="#plans-slct" role="tab" aria-controls="plans-slct" aria-selected="true">My Order</a>
									</li>
									<!-- <li class="nav-item px-3 text-uppercase">
										<a class="nav-link" id="billing-tab" data-toggle="tab" href="#billing" role="tab" aria-controls="billing" aria-selected="false">billing history</a>
									</li> -->
									<!-- <li class="nav-item px-3 text-uppercase">
										<a class="nav-link" id="xxxxxx-tab" data-toggle="tab" href="#xxxxxx" role="tab" aria-controls="xxxxxx" aria-selected="false">xxxxxx</a>
									</li> -->
								</ul>


								<div class="tab-content" id="myTabContent">



									<div class="tab-pane fade show active" id="plans-slct" role="tabpanel" aria-labelledby="plans-slct-tab">
										<div class="p-3">
											<?php
										 // echo "<pre>";
										 // print_r($orderDetails);
//exit();
											//echo "<pre>";
											//print_r($plandetails);
											if(!empty($orderDetails)) {
												// echo "order";
												foreach($orderDetails as $keybill => $bookorder) {
													$order_id =$bookorder['order_id'];

													$total_price=$bookorder['total_price'];
													$total_image=$bookorder['total_image'];
													$plan_type=$bookorder['plan_type'];
													$payment_status=$bookorder['p_status'];
													$create_date=$bookorder['create_date'];

													$category=array_filter(explode(",", $bookorder['category']));
													$gender=$bookorder['gender'];
													$quty=array_filter(explode(",", $bookorder['quty']));
													$m_status=$bookorder['m_status'];
													$angle=array_filter(explode(",", $bookorder['angle']));
													$d_status=$bookorder['d_status'];
													$notes=$bookorder['notes'];
													$currency=$bookorder['currency'];

													$book_date_time=date("D, M d ,Y H:i A",strtotime($create_date));
													$book_id=$bookorder['book_id'];
													$query1=$this->db->query("select * FROM tbl_plan INNER JOIN tbl_plantype on tbl_plan.plntyid=tbl_plantype.plntyid WHERE tbl_plan.planid=$plan_type");

													$bookDetails1= $query1->result_array();
													//print_r($bookDetails1);
													$plntypname=$bookDetails1[0]['plntypname'];
													$plan_name=$bookDetails1[0]['plan_name'];
													$plan_credit=$bookDetails1[0]['plan_credit'];
													$plan_amount=$bookDetails1[0]['plan_amount'];

													$user_query1=$this->db->query("select * FROM tbl_users where user_id=$user_id");

													$userDetails1= $user_query1->result_array();
													//print_r($userDetails1);
													$user_name=$userDetails1[0]['user_name'];
													$user_company=$userDetails1[0]['user_company'];
													$user_address=$userDetails1[0]['user_address'];
													$user_mobile=$userDetails1[0]['user_mobile'];

													$userbill_query1=$this->db->query("select * FROM tbl_users_billing where book_id=$book_id");

														$userbillDetails1= $userbill_query1->result_array();
														$payment_type_id=$userbillDetails1[0]['payment_type'];
														$payment_type_query1=$this->db->query("select * FROM tbl_paymenttype where paytyid='$payment_type_id'");

														// echo "select * FROM tbl_paymenttype where paytyid=$payment_type_id";

														$payment_typeDetails1= $payment_type_query1->result_array();
														$payment_type=$payment_typeDetails1[0]['paytypname'];
																											
													// $paym_query1=$this->db->query("select * FROM tbl_paymenttype where paytyid=$payment_type");

													// $paymDetails1= $paym_query1->result_array();
													// $paytypname=$paymDetails1[0]['paytypname'];

													?>
													<div class="border-bottom p-3 row text-center text-sm-left">

														<div class="col-12 col-sm-12 col-md-8 col-lg-8 col-xl-8">
															<span class="lead text-piquic"><?php echo $order_id;?></span><br>
															<small>
																<?php echo $plntypname;?> ( <?php echo $total_image;?> images. )<br>
																<?php echo $book_date_time;?>
															</small>
														</div>

														<div class="col-12 col-sm-12 col-md-2 col-lg-2 col-xl-2">
															<div class="lead pt-3"><?php
															if($currency_default=="USD") { echo"&#36;"; }
															elseif($currency_default=="INR") { echo"&#x20B9;"; }
															elseif($currency_default=="EUR") { echo"&#128;"; }
															elseif($currency_default=="GBP") { echo"&#163;"; }
															elseif($currency_default=="AUD") { echo"&#36;"; }
															elseif($currency_default=="CNY") { echo"&#165;"; }
															?>&nbsp;<?php echo round($total_price, 2);?></div>
														</div>

														<div class="col-12 col-sm-12 col-md-2 col-lg-2 col-xl-2">
															<?php if($payment_status=="Notpaid"||$payment_status=="Not paid" ||$payment_status=="Not Paid"){ ?>

																<div class="pt-3">
																	<a href="<?php echo base_url(); ?>booking_paymentdetails/<?php echo $book_id; ?>"  class="btn btn-block btn-piquic mb-2">Pay Now</a>
																</div>

															<?php } else { ?>

																<div class="pt-3">
																	<button type="submit" class="btn btn-block btn-outline-piquic mb-2" data-toggle="modal" data-target="#modal_<?php echo $book_id; ?>">View</button>
																</div>

																<div class="modal" id="modal_<?php echo $book_id; ?>" tabindex="-1" role="dialog">
																	<div class="modal-dialog" role="document" style="max-width: 75em !important;">
																		<div class="modal-content">
																			<div class="modal-header">
																				<h5 class="modal-title"><?php echo $order_id;?></h5>
																				<button type="button" class="close text-danger" data-dismiss="modal" aria-label="Close">
																					<span aria-hidden="true">&times;</span>
																				</button>
																			</div>

																			<div class="modal-body" id="bill_<?php echo $book_id; ?>">
																				<div class="border rounded p-2">
																					<div class="row pt-3 px-3">
																						<div class="col-md-6">
																							<img src="<?php echo PIQUIC_URL;?>images/logo-piquic-md.png">
																						</div>

																						<div class="col-md-6 text-right">
																							<span class="font-weight-bold mb-1">Order_ID #<?php echo $order_id ?></span><br>
																							<span class="text-piquic"> <?php echo $book_date_time; ?> </span>
																						</div>
																					</div>

																					<hr >

																					<div class="row p-3">
																						<div class="col-md-5">
																							<p class="font-weight-bold mb-4">Client Information</p>
																							<table>
																								<tbody>
																									<tr>
																										<td class="pr-3"><span class="font-weight-bold">Name: </span></td>
																										<td><?php echo $user_name; ?></td>
																									</tr>
																									<tr>
																										<td class="pr-3"><span class="font-weight-bold">Company Name: </span></td>
																										<td><?php echo $user_company; ?></td>
																									</tr>
																									<tr>
																										<td class="pr-3"><span class="font-weight-bold">Address: </span></td>
																										<td><?php echo $user_address; ?></td>
																									</tr>
																									<tr>
																										<td class="pr-3"><span class="font-weight-bold">Mobile: </span></td>
																										<td><?php echo $user_mobile; ?></td>
																									</tr>
																								</tbody>
																							</table>
																						</div>

																						<div class="col-md-7">

																							<p class="font-weight-bold mb-4">Booking Details</p>

																							<div class="row">
																								<div class="col-md-6">
																									<div class="rounded-lg bg-light p-3 mb-3">
																										<span class="font-weight-bold">Model: </span><br>
																										<?php
																										if($m_status=="1"){
																											echo "Let \"Piquic Style Expert\" select my models & style." ;
																										} else {
																											echo "I want to select my models and styling once my apparels gets digitized.";
																										} ?>
																									</div>
																								</div>

																								<div class="col-md-6">
																									<div class="rounded-lg bg-light p-3 mb-3">
																										<span class="font-weight-bold">Delivery: </span><br>
																										<?php
																										if($d_status=="1"){
																											echo "Standard (6-7 days after receiving your products)." ;
																										} else {
																											echo "Express (2-3 days after receiving your products).";
																										} ?>
																									</div>
																								</div>
																							</div>
																							<div class="row">
																								<div class="col-md-6">
																									<div class="rounded-lg bg-light p-3 mb-3">
																										<span class="font-weight-bold">Payment Type: </span><br>
																										<?php echo $payment_type ?>
																									</div>
																								</div>

																								<div class="col-md-6">
																									<div class="rounded-lg bg-light p-3 mb-3">
																										<span class="font-weight-bold">Payment Status: </span><br>
																										<?php echo $payment_status ?>
																									</div>
																								</div>
																							</div>
																							<div class="row">
																								<div class="col-md-12">
																									<div class="rounded-lg bg-light p-3 mb-3">
																										<span class="font-weight-bold">Notes: </span><br>
																										<?php if($notes != '') { echo $notes; } else { echo "N/A"; } ?>
																									</div>
																								</div>
																							</div>
																						</div>
																					</div>

																					<div class="row p-3">
																						<div class="col-md-12">
																							<table class="table">
																								<thead>
																									<?php if($plntypname=="Pay as you go"){ ?>
																									<tr>
																										<th class="border-0 text-uppercase small font-weight-bold">ID</th>
																										<th class="border-0 text-uppercase small font-weight-bold">Gender</th>
																										<th class="border-0 text-uppercase small font-weight-bold">Category</th>
																										<th class="border-0 text-uppercase small font-weight-bold">Quantity</th>
																										<th class="border-0 text-uppercase small font-weight-bold">Angle</th>
																									</tr>

																									<?php } 
																									if($plntypname=="Pay Monthly"){ ?>

																									<tr>
																										<th class="border-0 text-uppercase small font-weight-bold">ID</th>
																										<th class="border-0 text-uppercase small font-weight-bold">Plan Name</th>
																										<th class="border-0 text-uppercase small font-weight-bold">Duration</th>
																										<th class="border-0 text-uppercase small font-weight-bold">Total Image</th>
																										<th class="border-0 text-uppercase small font-weight-bold">Total Amount</th>
																									</tr>

																									<?php } ?>
																								</thead>

																								<tbody>
																									<?php if($plntypname=="Pay as you go"){ ?>
																									<tr>
																										<td>1</td>
																										<td>
																											<?php
																											if($gender=="1"){ echo "Male" ; }
																											if($gender=="2"){ echo "Female" ; }
																											if($gender=="4"){ echo "Boy" ; }
																											if($gender=="6"){ echo "Girl" ; }
																											?>
																										</td>

																										<td >
																											<table>
																												<?php foreach ($category as $key => $value ) { ?>
																												<tr>
																													<td style="border-top: none; border-bottom: 1px solid #dee2e6;">
																														<?php echo $value; ?>
																													</td>
																												</tr>
																												<?php } ?>
																											</table>
																										</td>

																										<td>
																											<table>
																												<?php foreach ($quty as $key1 => $value1 ) { ?>
																												<tr>
																													<td style="border-top: none; border-bottom: 1px solid #dee2e6;">
																														<?php echo $value1; ?>
																													</td>
																												</tr>
																												<?php } ?>
																											</table>
																										</td>

																										<td>
																											<table>
																												<?php
																												unset($arr_filter);
																												foreach ($angle as $key2 => $value2 ) {
																													$angle_value=explode("_",$value2);
																													$cat=$angle_value[0];
																													$arr_filter[$cat][]=ucfirst($angle_value[1]);
																												}

																												foreach ($category as $key => $value ) { ?>
																												<tr>
																													<td style="border-top: none; border-bottom: 1px solid #dee2e6;">
																														<?php echo implode(", ",$arr_filter[$value]); ?>
																													</td>
																												</tr>
																												<?php } ?>
																											</table>
																										</td>
																									</tr>

																									<?php } if($plntypname=="Pay Monthly"){ ?>
																									<tr>
																										<td>1</td>
																										<td><?php echo $plan_name ?></td>
																										<td>1 Month</td>
																										<td><?php echo $plan_credit ?></td>
																										<td><?php echo $plan_amount ?></td>
																									</tr>
																									<?php } ?>
																								</tbody>
																							</table>
																						</div>
																					</div>

																					<div class="d-flex justify-content-between text-piquic bg-light border">
																						<div class="py-3 px-5 text-center">
																							<div class="mb-2">Total Image</div>
																							<div class="lead"><?php echo $total_image; ?></div>
																						</div>

																						<div class="py-3 px-5 text-center">
																							<div class="mb-2">Plan Type</div>
																							<div class="lead"><?php echo $plntypname;?></div>
																						</div>

																						<div class="py-3 px-5 text-center">
																							<div class="mb-2">Total amount</div>
																							<div class="lead">
																								<?php
																								if($currency_default=="USD") { echo"&#36;"; }
																								elseif($currency_default=="INR") { echo"&#x20B9;"; }
																								elseif($currency_default=="EUR") { echo"&#128;"; }
																								elseif($currency_default=="GBP") { echo"&#163;"; }
																								elseif($currency_default=="AUD") { echo"&#36;"; }
																								elseif($currency_default=="CNY") { echo"&#165;"; }
																								?>&nbsp;<?php echo $total_price ?>
																							</div>
																						</div>
																					</div>
																				</div>
																			</div>

																			<div class="modal-footer d-flex justify-content-between">
																				<button type="button" class="btn btn-outline-piquic px-5" id="btnPrint" val="<?php echo $book_id?>">Print</button>

																				<button type="button" class="btn btn-outline-danger px-5" data-dismiss="modal">Close</button>
																			</div>
																		</div>
																	</div>
																</div>

															<?php } ?>
													</div>
												</div>

												<?php 

											} } else { ?>

												<div class="border-bottom p-3">
													<div class="d-flex justify-content-between">
														<span class="lead text-piquic">No History</span>
													</div>
												</div>
											<?php } ?>
										</div>

									</div>


									<div class="tab-pane fade" id="billing" role="tabpanel" aria-labelledby="billing-tab">
										<div class="p-3">




											<?php
                   //echo "<pre>";
                   //print_r($plandetails);
											if(!empty($billingDetails))
											{
												foreach($billingDetails as $keybill => $bookbill)
												{
													$billing_id=$bookbill['billing_id'];
													$billing_unique_id=$bookbill['billing_unique_id'];
													$billing_plan_id=$bookbill['billing_plan_id'];
													$billing_plan_credit=$bookbill['billing_plan_credit'];
													$billing_plan_amount=$bookbill['billing_plan_amount'];
													$billing_date_time=$bookbill['billing_date_time'];
													$billing_status=$bookbill['billing_status'];
													$payment_status=$bookbill['payment_status'];

													$plntypname=$bookbill['plntypname'];

													$billing_date_time=date("D, M d ,Y H:i A",strtotime($billing_date_time));

													?>


													<div id="plan_1" class="border-bottom p-3">
														<div class="d-flex justify-content-between">
															<div>
																<span class="lead text-piquic"><?php echo $billing_unique_id;?></span><br>
																<small>
																	<?php echo $plntypname;?> ( <?php echo $billing_plan_credit;?> images. )<br>

																	<?php echo $billing_date_time;?>
																</small>
															</div>

															<div class="pt-3">
																<span class="lead">
																	<?php
																	if($currency_default=="USD")
																	{
																		echo"&#36;";
																	}
																	elseif($currency_default=="INR")
																	{
																		echo"&#x20B9;";	
																	}
																	elseif($currency_default=="EUR")
																	{
																		echo"&#128;";
																	}
																	elseif($currency_default=="GBP")
																	{
																		echo"&#163;";
																	}
																	elseif($currency_default=="AUD")
																	{
																		echo"&#36;";
																	}
																	elseif($currency_default=="CNY")
																	{
																		echo"&#165;";
																	}
																	?>&nbsp;<?php echo $billing_plan_amount; ?></span>
																</div>

											<!-- 	<div class="pt-3">
													<button type="submit" class="btn btn-outline-piquic mb-2">View</button>
												</div> -->
											</div>
										</div>

										<?php
									}

								}

								else
								{
									?>
									<div id="plan_1" class="border-bottom p-3">
										<div class="d-flex justify-content-between">
											<div>
												<span class="lead text-piquic">No History</span>
											</div>
										</div>
										
										<?php
									}
									?>


								</div>



							</div>

							<div class="tab-pane fade" id="xxxxxx" role="tabpanel" aria-labelledby="xxxxxx-tab">...</div>
						</div>
					</div>
				</div>

			</div>
		</div>
	</div>
</div>
</div>

<?php
$this->load->view('front/includes/footer');
?>

<script type="text/javascript" src="https://cdn.datatables.net/v/bs4/dt-1.10.18/datatables.min.js"></script>

<script type="text/javascript">

	jQuery.fn.extend({
		printElem: function() {
			var cloned = this.clone();
			var printSection = $('#printSection');
			if (printSection.length == 0) {
				printSection = $('<div id="printSection"></div>')
				$('body').append(printSection);
			}
			printSection.append(cloned);
			var toggleBody = $('body *:visible');
			toggleBody.hide();
			$('#printSection, #printSection *').show();
			window.print();
			printSection.remove();
			toggleBody.show();
		}
	});


	$(document).ready(function(){
		$(document).on('click', '#btnPrint', function(){
			var book_id=$(this).attr('val');
			//alert(book_id);
			$('#bill_'+book_id).printElem();
		});
	});


	$(document).ready(function() {

		//changePlan('<?php //echo $currency_default;?>');

		$(".clickbone-group .clickb ").on("click", function(e) {

			$('.clickbmon div').removeClass('border-piquic');
			$('.clickbmon span').addClass('d-none');
			$(this).parent().find('.clickb div').removeClass('border-piquic');
			$(this).parent().find('.clickb span').addClass('d-none');

			$(this).children('div').addClass('border-piquic');
			$(this).children('span').removeClass('d-none');			       
		});

		$(".clickbmon-group .clickbmon ").on("click", function(e) {
			$('.clickb div').removeClass('border-piquic');
			$('.clickb span').addClass('d-none');

			$(this).parent().find('.clickbmon div').removeClass('border-piquic');
			$(this).parent().find('.clickbmon span').addClass('d-none');
			$(this).children('div').addClass('border-piquic');
			$(this).children('span').removeClass('d-none');			       
		});


// $("#clreyemk").on("click", function() {
	
// 			$('.rdeyemk-group').find('.rdeyemk div img').removeClass('border');
// 		});
})

	

</script>
<!-- 
<script type="text/javascript">
	

	function changePlan(currency)
	{
//alert(currency);
$.ajax({
	type:'POST',					
	url : "<?php //echo base_url();?>front/plans/getPlanDetailByCurrency", 
	data:{'currency':currency},
	success: function(response) {	
				 //alert(response);
				//location.reload();
				$("#plans-slct").html($.trim(response));
			}
		});
}


$('#set-currency').on('change', function(){
	var selected = $(this).val();
        // alert(selected);
        
        $.post('<?php //echo site_url( $this->uri->segment(1) . '/setcurrency'); ?>', {currency: selected}, function() {
        	location.reload();
        });
     });

function setAmount(hid_plantype_id,planid,plan_amount,plan_credit)
{

	$("#hid_plantype_id").val(hid_plantype_id);
	$("#hid_plan_id").val(planid);
	$("#hid_plan_amount").val(plan_amount);
	$("#hid_plan_credit").val(plan_credit);
	$("#inputAmount").val(plan_amount);

	$(".planSelectbox").removeClass("border-piquic");
	$(".planSelectboxTick").addClass("d-none");

	$("#planSelectbox"+planid).addClass("border-piquic");
	$("#planSelectboxTick"+planid).removeClass("d-none");
}

function submitPlanFrm()
{
	var inputAmount = $("#inputAmount").val();
	var currency = $("#currency").val();
	//alert(inputAmount);
	//alert(currency);
	if($.trim(inputAmount)!='' )
	{
		document.frm_plan.submit();
	}
	else
	{
		alert("Select Plan");
		return false;
	}

}

</script>
-->
