<div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">			
<?php

if(!empty($download_image)) {
							//print_r($download_image);
								//	print_r("ALl num rows".$allNumRows);
	if(!empty($allNumRows) > 0){
									//print_r("ALl num rows".$allNumRows);
							//print_r($download_image);
							//$sno = $row+1;

		foreach ($download_image as $key) {
			$upimg_id=$key['upimg_id'];
			$last_upimg_id=$key['upimg_id'];
											//print_r($last_upimg_id);
			$sku_thumb=$key['sku_img_path'];
											//print_r($sku_thumb);
			$sku_no=$key['zipfile_name'];
			if(is_dir($sku_thumb))
			{
				
				$result=scandir($sku_thumb);
				$files = array_diff($result, array('.', '..'));
         		if(count($files) != 0){
         		//	echo "the folder is NOT empty";
				

?>

<div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
	<div class="row">
		<div class="col-6 col-sm-6 col-md-6  col-lg-6 col-xl-6 pb-6">
			<div class="custom-control custom-checkbox">
				<input type="checkbox" class="custom-control-input checkBoxDownloadClass" id="<?php  echo($sku_no); ?>" name="images[]" class="select" value="<?php echo($sku_no); ?>" onclick="imgdownFunc('<?php echo($sku_no); ?>')">
				<label class="custom-control-label roundCheck lead" for="<?php  echo($sku_no); ?>">
					SKU: <?php  echo($sku_no); ?>
				</label>
				<span id="image_validate"></span>
			</div>
		</div>
	</div>
	<div id="SKU_<?php  echo($sku_no); ?>" class="pb-5">
		<div class="row pt-3 pl-md-4">
			<div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
				<div class="pl-3">
					<div class="row">
						<?php
						$images = glob($sku_thumb."*.{png,PNG,jpg,JPG}",GLOB_BRACE);
						//print_r($images);
						// arsort($images);

						$i = 0; 

						foreach($images as $image){ 
							$image_name1=explode("/",$image);
							$image_name=explode(".",$image_name1[2]);
							//print_r($image);
							// $image_path="http://172.25.0.71:82/".$sku_no."/".$image_name_org.".jpg";
							// $image2 = file_get_contents($image_path);
							// $image_codes2 = base64_encode($image2);
							$image = file_get_contents($image);
							$image_codes = base64_encode($image);
							$filecount = count($images); 
						?>
						<div class="col-6 col-sm-6 col-md-3 col-lg-3 col-xl-3 pb-3">
							<div class="imgCnt">
								<img class="w-100 border" src="data:image/jpg;charset=utf-8;base64, <?php echo $image_codes; ?>">

								<div class="container">
									<div class="row zmcr">
										<div class="text-center col-6" data-toggle="modal" data-target="#zoom_img_id_<?php echo $last_upimg_id."_".$i ?> "  onclick="initZoom('<?php print($image_name[0]); ?>');">
											<i class="fas fa-expand-arrows-alt"></i><br>ZOOM
										</div>

										<div class="text-center col-6" data-toggle="modal" data-target="#crop_img_id_<?php echo $last_upimg_id."_".$i ?>" onclick="initCropper('<?php print($image_name[0]); ?>',<?php echo $i ?>,<?php echo $last_upimg_id; ?>,<?php echo  $filecount; ?>,'<?php  print_r($sku_no); ?>' );">
											<i class="fas fa-crop-alt"></i><br>CROP
										</div>

									</div>
								</div>
							</div>

							<!-- Zoom Modal -->
							<div class="modal fade" id="zoom_img_id_<?php echo $last_upimg_id."_".$i ?>" tabindex="-1" role="dialog" aria-labelledby="zoom_img_id_Label" aria-hidden="true">
								<div class="modal-dialog" role="document">
									<div class="modal-content">
										<div class="modal-header">
											<h5 class="modal-title" id="zoom_img_id_Label"><?php print($image_name[0]); ?></h5>
											<button type="button" class="close" data-dismiss="modal" aria-label="Close">
												<span aria-hidden="true">&times;</span>
											</button>
										</div>
										<div class="modal-body">
											<?php
											$image_name_org=str_replace("_thumb","",$image_name[0]);
											?>
											<script type="text/javascript">
                                            $(window).on('load', function(){
                                              var img = document.getElementById('img_<?php print($image_name[0]); ?>');
                                              var width = img.naturalWidth;
                                              var height = img.naturalHeight;
                                              if (width < height){
                                                $('#img_<?php print($image_name[0]); ?>' ).removeClass('img').addClass('img-fluid');
                                              }
                                            });
                                          </script>
                                          <?php
											$base_url=base_url();
											$ip_url='http://203.122.46.54:8080/';
											if(strpos($base_url,$ip_url) !== false){
												 $image_path="http://203.122.46.51:82/".$sku_no."/".$image_name_org.".jpg";
											   
											} else{
											   $image_path="http://172.25.0.71:82/".$sku_no."/".$image_name_org.".jpg";
											}
																						
											
											?>
											<div class="text-center zoom" id="zoom_<?php print($image_name[0]); ?>">
												<span class='zoom' id='<?php print($image_name[0]); ?>2'>
													<img class="w-100 d-block" id="img_<?php print($image_name[0]); ?>" src="<?php echo $image_path ?>" />
												</span>
											</div>

										</div>
									</div>
								</div>
							</div>

							<!-- Crop Modal -->
							<div class="modal fade" id="crop_img_id_<?php echo $last_upimg_id."_".$i ?>" tabindex="-1" role="dialog" aria-labelledby="crop_img_id_Label" aria-hidden="true">
								<div class="modal-dialog" role="document">
									<div class="modal-content">
										<div class="modal-header">
											<h5 class="modal-title" id="crop_img_id_Label"><?php print($image_name[0]); ?></h5>
											<button type="button" class="close" data-dismiss="modal" aria-label="Close">
												<span aria-hidden="true">&times;</span>
											</button>
										</div>
										<div class="modal-body">
											<?php

											$image_name_org=str_replace("_thumb","",$image_name[0]);
// 											$base_url=base_url();
// 											$ip_url='http://203.122.46.51:82/';


// 											$ch = curl_init();
							if(strpos($base_url,$ip_url) !== false){
											    $image_path="http://172.25.0.71:82/".$sku_no."/".$image_name_org.".jpg";
											} else{
											  $image_path="http://203.122.46.51:82/".$sku_no."/".$image_name_org.".jpg";
											}
												
											$image_path="http://172.25.0.71:82/".$sku_no."/".$image_name_org.".jpg";
											$image2 = file_get_contents($image_path);

// // 											curl_setopt($ch, CURLOPT_URL, sprintf($image_path));
// // curl_setopt($ch, CURLOPT_HEADER, 0);
// // curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
// // curl_setopt($ch, CURLOPT_BINARYTRANSFER, 1);
// // $image2 = curl_exec($ch);
// // curl_close($ch);
// // header('Content-type: image/jpeg');
// // echo $image2;				
										$image_codes2 = base64_encode($image2);
											?>
											<div class="text-center">
												<img id="crop_<?php print($image_name[0]); ?>" class="w-100 " src="data:image/jpg;charset=utf-8;base64, <?php echo $image_codes2; ?>">
											</div>
										</div>
										<div class="modal-footer">
											<div class="text-center" id="crop_div_<?php echo $last_upimg_id."_".$i ?>" onclick="initCropper('<?php print($image_name[0]); ?>',<?php echo $i ?>, <?php echo $last_upimg_id ?>,<?php echo  $filecount; ?>,'<?php  print_r($sku_no); ?>');">
												<button id="crop_button_<?php echo $last_upimg_id."_".$i ?>" class="btn btn-piquic" data-dismiss="modal">Crop</button>
											</div>
											<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
										</div>
									</div>
								</div>
							</div>
						</div>

						<?php
						$i++; }

						$crop_img=$sku_thumb."/crop/";

						if(file_exists($crop_img))
						{
							$crop_images = glob($crop_img."*.{png,PNG,jpg,JPG}",GLOB_BRACE);
							//print_r($crop_images);
							// arsort($crop_images);

							$i = 0; 

							foreach($crop_images as $crop_image){ 

								$crop_image = file_get_contents($crop_image);
								$crop_image_codes = base64_encode($crop_image);
							  	//$filecount = count($images); 
						?>

						<div class="col-6 col-sm-6 col-md-3 col-lg-3 col-xl-3 pb-3">
							<div class="imgCnt">
								<img class="w-100 d-block border rounded" src="data:image/jpg;charset=utf-8;base64, <?php echo $crop_image_codes; ?>">
							</div>
						</div>

						<?php } } ?>
					</div>

					<!-- Cropped Images -->
					<div class="row d-none" id="cropped_result_<?php echo $last_upimg_id; ?>">

						<div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12 pb-3">
							<h5>Cropped Images</h5>
						</div>

					</div>
				</div>
			</div>                  
		</div>
	</div>
</div>
<?php 
		
				}
				//else{
				// 	echo "the folder is empty"; 
				// }
			}
			// else{
			// 	echo "dir not exite";
			// }

} 


?>


<?php if($allNumRows > $showLimit){ ?>
	<div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
		<div class="load-more text-center" lastID="<?php echo $last_upimg_id; ?>"  >
			<i class="fas fa-spinner fa-3x fa-spin text-piquic"></i>
		</div>
	</div>
<?php } else { ?>
	<div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">

		 <div class="load-more more pb-1" lastID="0">
		 	<p class="p--offset"> No More Data To Load !!  </p>
			<!-- <div class="alert alert-success" role="alert">
				<h4 class="alert-heading">That's All!!</h4>
			</div> -->
		</div> 
		
		
	
	</div>
<?php } 
} else { ?>
	<div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
		<div class="load-more pb-1" lastID="0">
		 	<p class="p--offset" > No More Data To Load !!  </p>
			<!-- <div class="alert alert-success" role="alert">
				<h4 class="alert-heading">That's All!!</h4>
			</div> -->
		</div> 

</div>
<?php } } else { ?>
	 <div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
		
		<div id="notfound" class="load-more" lastID="0">
		<div class="notfound">
			<div class="notfound-404">
				<h1><span></span></h1>
			</div>
			<h2>Oops! Data Not Found</h2>
			<p>Sorry! based on your search there is no data found.</p>
		</div>
	</div>
	</div> 
	
<?php } ?>

