<?php
session_start();
error_reporting(0);
$data['page_title'] = "download";
$this->load->view('front/includes/header',$data);
// $this->load->view('front/includes/menu');
$this->load->view('front/includes/nav');
?>
<link href="<?php echo base_url();?>website-assets/crop/css/cropper.css" rel="stylesheet">
<script src="<?php echo base_url();?>website-assets/crop/js/cropper.js"></script>
<script src="<?php echo base_url();?>website-assets/zoom/js/jquery.zoom.js"></script>
<!-- <script src="js/main.js"></script> -->
<div class="container-fluid">
	<div class="row">

		<?php $this->load->view('front/includes/sidebar'); ?>

		<div class="col-12 col-sm-12 col-md-9 col-lg-9 col-xl-9 border">
		<!-- <div class="col-12 col-sm-12 col-md-9 col-lg-9 col-xl-9 border mainscroll"> -->
			<div class="bg-piquic full-w my-2" style="min-height: 3rem;">&nbsp;</div>
			<div class="container pt-3">		

				<div class="row mb-4">
					<div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
						<div class="bg-white">
							<div class="row mb-3">
								<div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
									<div class="row">
										<div class="col-6 col-sm-6 col-md-6  col-lg-6 col-xl-6 pb-6">
											<div class="custom-control custom-checkbox">
												<!-- <input type="checkbox" class="custom-control-input" id="chbkDownloadSlctAll">
													<label class="custom-control-label roundCheck" for="chbkDownloadSlctAll">Select All SKUs</label> -->
												</div>
											</div>
										<!-- <div class="col-6 col-sm-6 col-md-6  col-lg-6 col-xl-6 pb-6">
											<div class="download">
												&emsp;<span class="text-piquic lead" title="Download All" id="downimg"><i class="fas fa-download"></i></span>
											</div>
										</div> -->
									</div>
								</div>
							</div>
							
							<div class="row">



								<input type="hidden" name="ordno" id="ordno"/>
								<input type="hidden" name="modelgen" id="modelgen"/>
								<input type="hidden" name="modelcat" id="modelcat"/>
								<input type="hidden" name="status" id="status"/>
								<input type="hidden" name="downloadimgid" id="downloadimgid"  size="40"/>
								<!-- <form method="post" action="<?php echo base_url();?>front/download/download_sku"> -->
									<form method="post" id="download_sku">
										<div id="download_list" class="w-100">

											<?php $this->load->view('front/download_loader', $download_image);   ?>


										</div>
										<br />
										<div class="col-12 text-right fixed-bottom">
											<input type="submit" name="download"  id= 'downimg' class="btn btn-piquic mb-3" value="Download" disabled />
										</div>
									</form>
								</div>
								<!-- <div id="down_loader" class="col-md-12 text-center d-none"><h4 class="h4">Please wait &hellip; Don't refresh the page until your download will start. &nbsp;&nbsp;</h4><img src="download_loader.gif" style="width: 75px;height: 75%;"> 
									<div class="loader"></div>


								</div> -->
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>

		<?php
		$this->load->view('front/includes/footer');
		?>


		<script type="text/javascript" defer>
			$(document).ready(function(){

				var modelgen =$('#modelgen').val();
				var ordval =$('#ordno').val();
				var modelcat =$('#modelcat').val();
				var status=$('#status').val();
				var max_limit=5;
				$(window).scroll(function(){
					var modelgen =$('#modelgen').val();
					var ordval =$('#ordno').val();
					var modelcat =$('#modelcat').val();
					var status=$('#status').val();
					var lastID = $('.load-more').attr('lastID');

//console.log(lastID);
if(($(window).scrollTop() == $(document).height() - $(window).height()) && (lastID != 0)){

	$.ajax({
		type:'POST',
		url:'<?php echo base_url();?>front/download/showdownloader',
		data:'lastID='+lastID+'&modelgen='+modelgen+'&ordval='+ordval+'&modelcat='+modelcat+'&status='+status,
		beforeSend:function(){
			$('.load-more').show();
		},
		success:function(html){

			$('.load-more').remove();
			$('#download_list').append(html);

		}
	});
}

$(".checkBoxDownloadClass:input:checkbox").change(function (){
	if ($(".checkBoxDownloadClass:input:checkbox:checked").length>="1"){
		$("#downimg").prop("disabled", false);
	}
	else{
		$("#downimg").prop("disabled", true);
	}
	if ($(".checkBoxDownloadClass:input:checkbox:checked").length > max_limit){
		this.checked = false;
	}
});

});
				$(".checkBoxDownloadClass:input:checkbox").each(function (index){
					this.checked = (".checkBoxDownloadClass:input:checkbox" < max_limit);
				}).change(function (){
					if ($(".checkBoxDownloadClass:input:checkbox:checked").length>="1"){
						$("#downimg").prop("disabled", false);
					}
					else{
						$("#downimg").prop("disabled", true);
					}
					if ($(".checkBoxDownloadClass:input:checkbox:checked").length > max_limit){
						this.checked = false;
					}
				});

				$("#chbkDownloadSlctAll").click(function () {

					var total_check = $(".checkBoxDownloadClass").length;
					if(total_check>max_limit){

					}
					var check_value=$(".checkBoxDownloadClass").prop('checked', $(this).prop('checked'));
					$(".checkBoxDownloadClass").each(function(index){
						var id = $(this).attr("id");
						var class_name = $(this).attr("class");
						//console.log(class_name);
						var check=$("#" + class_name).attr('checked', true);
						//console.log("#" + class_name+"----"+check);
						if(check == true)
						{
							//console.log("Checkbox is checked.");

						}
						else if(check == false){
    		  //	$(this).prop("checked", true);
    		 // console.log("Checkbox is unchecked.");
    		}
    		if((".checkBoxDownloadClass:input:checkbox").length > max_limit){
	    		//console.log("hi");
           // this.checked = false;
           $(".checkBoxDownloadClass").prop('checked', false);
        }
        else{
        	//this.checked = true;
        	$(".checkBoxDownloadClass").prop('checked', true);
        }
    		// var check_value = ($(".checkBoxDownloadClass").prop('checked', $(this).prop('checked')) < max_limit);

    	});

   // alert(check_value);
    	// if (check_value.length>="1"){
     //        $("#downimg").prop("disabled", false);
     //  	}
	    // else{
	    //      $("#downimg").prop("disabled", true);
	    // }
	    //alert($(".checkBoxDownloadClass").length+" >"+ max_limit);
	    if ($(".checkBoxDownloadClass").length > max_limit){
	    		//console.log("hi");
           // this.checked = false;
           $(".checkBoxDownloadClass").prop('checked', false);
        }
        else{
        	//this.checked = true;
        	$(".checkBoxDownloadClass").prop('checked', true);
        }
    // 	if($(".checkBoxDownloadClass").prop('checked', $(this).prop('checked')).length<max_limit)
    // 	{
    // 		alert("if");
    // 		$(".checkBoxDownloadClass").prop('checked', $(this).prop('checked'));
    // 	}
    // 	$(".checkBoxDownloadClass").each(function(index){
    // 		//console.log("hi");
    // 		  this.checked = (".checkBoxDownloadClass:input:checkbox" < max_limit);

    //   }).change(function (){
    // 	if ($(".checkBoxDownloadClass:input:checkbox:checked").length>="1"){
    //         $("#downimg").prop("disabled", false);
    //   	}
	   //  else{
	   //       $("#downimg").prop("disabled", true);
	   //  }
    //     if ($(".checkBoxDownloadClass:input:checkbox:checked").length > max_limit){
    //         this.checked = false;
    //     }
    // });

    // 	$(".checkBoxDownloadClass:input:checkbox").each(function (index){
    //     this.checked = (".checkBoxDownloadClass:input:checkbox" < max_limit);
    // }).change(function (){
    // 	if ($(".checkBoxDownloadClass:input:checkbox:checked").length>="1"){
    //         $("#downimg").prop("disabled", false);
    //   	}
	   //  else{
	   //       $("#downimg").prop("disabled", true);
	   //  }
    //     if ($(".checkBoxDownloadClass:input:checkbox:checked").length > max_limit){
    //         this.checked = false;
    //     }
    // });

      // 	$(".checkBoxDownloadClass").each(function(){
      // 		if ($(".checkBoxDownloadClass").prop('checked', $(this).prop('checked')).length >= max_limit){
      //       this.checked = false;
      //   }
      //   if (!$(this).prop("checked")){

      //    	$("#downimg").prop("disabled", true);
      // 	}
      // 	else{

      //    	$("#downimg").prop("disabled", false);
      // 	}
      // });

   });





			});
k=0;
function initCropper(image,i,last_upimg_id,filecount,sku_no){
			//alert(sku_no);
		//$("#crop_div_"+i).removeClass('d-none');
		//  $("#crop_"+i).addClass("d-none")
			//$('.box-2').show();
			var image = document.getElementById("crop_"+image);
			///console.log(image);
			 // var image=this.id;
			 var cropper = new Cropper(image, {
			 	aspectRatio: 1 / 1,
			 	crop: function(e) {
			 		console.log(e.detail.x);
			 		console.log(e.detail.y);
			 	}
			 });

				// On crop button clicked
		//alert('crop_button_'+last_upimg_id+"_"+i);
		document.getElementById('crop_button_'+last_upimg_id+"_"+i).addEventListener('click', function(){
				//  alert("crop_button");
				
				$("#cropped_result_"+last_upimg_id).removeClass('d-none');
				var imgurl =  cropper.getCroppedCanvas().toDataURL();
						// var img = document.createElement("img");
						//  img.className ="w-100 d-block border rounded";
						// img.id="image_"+k;
						// img.src = imgurl;
						var d = new Date();
						var n = d.getTime();
						id="image_"+k;
						className ="w-100 d-block border rounded";
						// src = imgurl;
						// document.getElementById("cropped_result_"+last_upimg_id).appendChild(img);
						var image_name=sku_no+filecount+"_crop"+n+".jpg";
						var image_name1="\'"+sku_no+filecount+"_crop"+n+"\'";
						var sku_noo="\'"+sku_no+"\'";
						//console.log(image_name1);
						$("#cropped_result_"+last_upimg_id).append('<div class="col-6 col-sm-6 col-md-3 col-lg-3 col-xl-3 pb-3"><div class="imgCnt"><img class="'+className  +'" src="'+ imgurl+'"   id="'+id+'"><div class="d-flex justify-content-around"><div class="text-center" onclick="delete_crop('+image_name1+","+sku_noo+');"><i class="far fa-trash-alt"></i><br>Delete</div><div class="text-center" onclick="save_crop('+image_name1+');"><i class="far fa-save"></i><br>Save</div></div></div></div>');
												 // var input = document.createElement("input");
						// input.id="img_val"+k;
						// input.type = "text";
						// input.value="asdf";
						// document.getElementById("cropped_result_"+i).appendChild(input);
						
						/* ---------------- SEND IMAGE TO THE SERVER-------------------------*/
						
						cropper.getCroppedCanvas().toBlob(function (blob) {
							//console.log(blob);
							var formData = new FormData();
							formData.append('croppedImage', blob, image_name);
							formData.append('sku_no',sku_no);
							//console.log(formData);
											// Use `jQuery.ajax` method
											$.ajax({
												url: "<?php echo base_url();?>front/download/upload_file",
												method: "POST",
												data: formData,
												processData: false,
												contentType: false,
												success: function (file) {
													//console.log('Upload success - '+file);
													//window.location.href = "<?php// echo base_url();?>download";
												},
												error: function () {
													//console.log('Upload error');
												}
											});
										});
						/*----------------------------------------------------*/
						k=k+1;
					})
	}
	function delete_crop(image_name,sku_no){
		$.ajax({
			url: "<?php echo base_url();?>front/download/upload_file_delete",
			method: "POST",
			data: "&image_name="+image_name+"&sku_no="+sku_no,

			success: function (file) {
				if(file=="Deleted")
				{
					console.log('Deleted success - '+file);
					window.location.href = "<?php echo base_url();?>download";
				}
			},
			error: function () {
				console.log('Upload error');
			}
		});
	}
	function save_crop(image_name){
		window.location.href = "<?php echo base_url();?>download";
	}
	function initZoom(image){
	//alert("zoom");
	$('#zoom_'+image).zoom();
}
function genderFunc(gen) {
	var lastID ="" ;
	if(gen=="all")
	{	
		$(".checkBoxGenderClass").prop('checked', $('#chkGDAll').prop('checked'))
		$(".checkBoxGenderClass").each(function(){
			if (!$(this).prop("checked")){
				$(this).removeClass('activegen');
			}
			else{
				$(this).addClass('activegen');
			}
		});
	}
	else{
		var lfckv = document.getElementById(gen).checked;
		if($(".checkBoxGenderClass").length == $(".checkBoxGenderClass:checked").length) {
			$("#chkGDAll").prop("checked",true);
		}
		else{
			$("#chkGDAll").prop("checked",false);
		}
		if(lfckv==true){
			$('#'+gen+'').addClass('activegen');
			
		}else{
			$('#'+gen+'').removeClass('activegen');
		}
	}
	var ids = $('.activegen').map(function(i) {
		return this.id;
	});
	$('#modelgen').val(ids.get().join(',')); 
	var modelgen =$('#modelgen').val();
	var ordval =$('#ordno').val();
	var modelcat =$('#modelcat').val();
	var status=$('#status').val();
	$.ajax({
		type:'POST',
		url:'<?php echo base_url();?>front/download/showdownloader',
		data:{'ordno':ordval,'modelgen':modelgen,'modelcat':modelcat,'status':status,'lastID':lastID},
		success: function(checkresults) {
			$("#download_list").html(checkresults);
		}
	});
}
function categoryFunc(cat) {
	var lastID ="" ;
	if(cat=="all")
	{	
		$(".checkBoxCategoryClass").prop('checked', $('#chkCGAll').prop('checked'))
		$(".checkBoxCategoryClass").each(function(){
			if (!$(this).prop("checked")){
				$(this).removeClass('activecat');
			}
			else{
				$(this).addClass('activecat');
			}
		});
	}
	else{
		var lfckv = document.getElementById(cat).checked;
		if($(".checkBoxCategoryClass").length == $(".checkBoxCategoryClass:checked").length) {
			$("#chkCGAll").prop("checked",true);
		}
		else{
			$("#chkCGAll").prop("checked",false);
		}
		if(lfckv==true){
			$('#'+cat+'').addClass('activecat');
			
		}else{
			$('#'+cat+'').removeClass('activecat');
		}
	}
	var ids = $('.activecat').map(function(i) {
		return this.id;
	});
	$('#modelcat').val(ids.get().join(',')); 
	var modelgen =$('#modelgen').val();
	var ordval =$('#ordno').val();
	var modelcat =$('#modelcat').val();
	var status=$('#status').val();
	$.ajax({
		type:'POST',
		url:'<?php echo base_url();?>front/download/showdownloader',
		data:{'ordno':ordval,'modelgen':modelgen,'modelcat':modelcat,'status':status,'lastID':lastID},
		success: function(checkresults) {
			$("#download_list").html(checkresults);
		}
	});
}
function statusFunc(status) {
	//alert(status);
	var lastID ="" ;
	if(status=="all")
	{	
		$(".checkBoxStatusClass").prop('checked', $('#chkStatusAll').prop('checked'))
		$(".checkBoxStatusClass").each(function(){
			if (!$(this).prop("checked")){
				$(this).removeClass('activestatus');
			}
			else{
				$(this).addClass('activestatus');
			}
		});
	}
	else{
		var lfckv = document.getElementById("status_"+status).checked;
		if($(".checkBoxStatusClass").length == $(".checkBoxStatusClass:checked").length) {
			$("#chkStatusAll").prop("checked",true);
		}
		else{
			$("#chkStatusAll").prop("checked",false);
		}
		if(lfckv==true){
			$('#status_'+status+'').addClass('activestatus');
			
		}else{
			$('#status_'+status+'').removeClass('activestatus');
		}
	}
	var ids = $('.activestatus').map(function(i) {
		return this.value;
	});
	$('#status').val(ids.get().join(',')); 
	var modelgen =$('#modelgen').val();
	var ordval =$('#ordno').val();
	var modelcat =$('#modelcat').val();
	var status=$('#status').val();
	//alert(status);
	$.ajax({
		type:'POST',
		url:'<?php echo base_url();?>front/download/showdownloader',
		data:{'ordno':ordval,'modelgen':modelgen,'modelcat':modelcat,'status':status,'lastID':lastID},
		success: function(checkresults) {
			$("#download_list").html(checkresults);
		}
	});
}
//   function imgdownFunc(imgid) {

//    var lfckvimg = document.getElementById(imgid).checked;
//    if (lfckvimg==true){
//           //alert("chbkDownloadSlctAll if");
//          $("#downimg").prop("disabled", false);
//       }
//       else{
//          $("#downimg").prop("disabled", true);
//       }



// 			alert($("input[name=images]:checked").length);
//    //var chbkDownloadSlctAll=document.getElementById('chbkDownloadSlctAll').checked;
//   $(".checkBoxDownloadClass").prop('checked', $(lfckvimg).prop('checked'));
//       $(".checkBoxDownloadClass").each(function(){
//        //alert($("input[name=checkBoxDownloadClass]:checked").length);
//         // if($("input[name=checkBoxDownloadClass]:checked").length!=0 ){
//        if (lfckvimg==true){
//           //alert("chbkDownloadSlctAll if");
//          $("#downimg").prop("disabled", false);
//       }
//       else{
//          $("#downimg").prop("disabled", true);
//       }

//       });
//       $(".checkBoxDownloadClass").each(function(){
//         if($("input[class=checkBoxDownloadClass]:checked").length>=8 ){
//           this.checked = false;
//         }        
//       });
//       // if($(".checkBoxDownloadClass").length == $(".checkBoxDownloadClass:checked").length) {
//       //       $("#chbkDownloadSlctAll").prop("checked",true);
//       //   }
//       //   else{
//       //       $("#chbkDownloadSlctAll").prop("checked",false);
//       //   }
//    // alert(chbkDownloadSlctAll);
//   //  if(lfckvimg==true){
//   //   // alert("true");
//   //   $('#'+imgid+'').addClass('activeimgid');

//   // }else{
//   //     // alert("false");
//   //   $('#'+imgid+'').removeClass('activeimgid');

//   // }
//   // var ids = $('.activeimgid').map(function(i) {
//   //   return this.value;
//   // });
//   // $('#downloadimgid').val(ids.get().join(',')); 


// }

// $(document).ready(function () {

//     // $(".checkBoxDownloadClass").change(function(){     
//     // 	alert("change");
//     // if($("input[name=checkBoxDownloadClass]:checked").length>8 ){
//     //       this.checked = false;
//     //     }  
//     // });

//   });
//   // $(".checkBoxDownloadClass").change(function(){     
//   //   	//alert("change");
//   //   	if("input[name=checkBoxDownloadClass]:checked")
//   //   	{
//   //   		 $("#downimg").prop("disabled", false);
//   //   	}
//   //   	 else{
//   //       	$("#downimg").prop("disabled", true);
//   //       }	

//   //   });

$(function () {
	$("#download_sku").validate({
		rules: {
			'images[]':"required",
		},
		messages:
		{
			'images[]':{
				required:"Please Select any Category on this list"         
			},
		},
		errorPlacement: function(error, element) {
			if (element.attr("name") == "images[]") {
				error.insertAfter("#images_validate");
			}
			else { 
				error.insertAfter( element );
			}

		},
		submitHandler: function (form) {
    	//alert("<?php echo base_url();?>front/download/download_sku");
    	var formData = new FormData(form);
    	$("#loader").show();
    	$.ajax({
    		method:'post',
    		url:'<?php echo base_url();?>front/download/download_sku',
    		contentType: false,
    		processData: false,
    		data:formData,
    		success: function(result){
    			
    			if(result!=""){
    				if(window.location = result)
    			 {
    			 	$("#loader").hide();
    			 	//alert("success");
    			 	var zip_file=result;
	    			// console.log(zip_file);

	    		// 	  $.ajax({
			    // 		method:'post',
			    // 		url:'<?php //echo base_url();?>front/download/sku_zip_unlink',
			    // 		data:{'zip_file':zip_file},
			    // 		success: function(result){
			    			
			    // 			 if(result=="success")
			    // 			 {
			    // 			 	$("#loader").hide();
			    // 			 }  	
					  // 	}
					  // });
    			 }
    			 else{
    			 	// alert("error");
    			 	$('#modal_msg').modal('show');
					$('#txtMsg').html('Error');
    			 }
    		}

    	}
    });
    	

    }

 });
});
</script>

<style>
	/* styles unrelated to zoom */
	/** { border:0; margin:0; padding:0; }*/
	.more	p { top: 3px;
		right: 28px;
		text-align: center;
		font-size: 30px; }

		/* these styles are for the demo, but are not required for the plugin */
		.zoom {
			display:inline-block;
			position: relative;
		}

		/* magnifying glass icon */
		.zoom:after {
			content:'';
			display:block; 
			width:33px; 
			height:33px; 
			position:absolute; 
			top:0;
			right:0;
			background:url(icon.png);
		}

		.zoom img {
			display: block;
		}

		.zoom img::selection { background-color: transparent; }

		#ex2 img:hover { cursor: url(grab.cur), default; }
		#ex2 img:active { cursor: url(grabbed.cur), default; }
		.loader {
			color: #76aea1;
			font-size: 20px;
			margin: 100px auto;
			width: 1em;
			height: 1em;
			border-radius: 50%;
			position: relative;
			text-indent: -9999em;
			-webkit-animation: load4 1.3s infinite linear;
			animation: load4 1.3s infinite linear;
			-webkit-transform: translateZ(0);
			-ms-transform: translateZ(0);
			transform: translateZ(0);
		}
	</style>