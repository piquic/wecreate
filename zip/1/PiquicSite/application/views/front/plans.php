<?php
session_start();
error_reporting(0);
$data['page_title'] = "plans";

$this->load->view('front/includes/header',$data);
// $this->load->view('front/includes/menu');
$this->load->view('front/includes/nav');

if ($this->session->userdata('front_logged_in')) {
	$session_data = $this->session->userdata('front_logged_in');
	$user_id = $session_data['user_id'];
	$user_name = $session_data['user_name'];
	
	
	
} else {
	
	$session_data = $this->session->userdata('front_logged_in');
	$user_id = $session_data['user_id'];
	$user_name = $session_data['user_name'];
	
	
}

?>
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/bs4/dt-1.10.18/datatables.min.css"/>

<div class="container-fluid">
	<div class="row">

		<?php //$this->load->view('front/includes/sidebar'); ?>

		<!-- <div class="col-12 col-sm-12 col-md-9 col-lg-9 col-xl-9 border mainscroll"> -->
			<div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12 mainscroll">

				<div class="container pt-3">
					<div class="row mb-4">
						<div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
							<h1>Account Overview</h1>
						</div>
					</div>

					<?php
					$query=$this->db->query("select * from  tbl_users  WHERE user_id='$user_id' ");
					$userDetails= $query->result_array();
					$total_amount=$userDetails[0]['total_amount'];

//echo "select sum(billing_plan_amount) as sum_billing_plan_amount from  tbl_users_billing  WHERE user_id='$user_id' ";
					$query=$this->db->query("select sum(billing_plan_amount) as sum_billing_plan_amount from  tbl_users_billing  WHERE user_id='$user_id' ");
					$userDetails= $query->result_array();
					if(!empty($userDetails) && $userDetails[0]['sum_billing_plan_amount']!=NULL)
					{
						$sum_billing_plan_amount=$userDetails[0]['sum_billing_plan_amount'];

						$query=$this->db->query("select * from  tbl_users  WHERE user_id='$user_id' ");
						$userDetails= $query->result_array();
						$total_amount=$userDetails[0]['total_amount'];
					}
					else
					{
						$sum_billing_plan_amount='0';
					}



//echo $this->session->userdata('sess_plan_id')."kkk";
					if(!empty($this->session->userdata('sess_plan_id')))
					{
						$sess_currency = $this->session->userdata('sess_currency');
						$sess_plan_id = $this->session->userdata('sess_plan_id');
						$plansDetails = $this->plans_model->get_plansById($sess_plan_id);
						$hid_plantype_id=$plansDetails[0]['plntyid'];
						$hid_plan_id=$plansDetails[0]['planid'];
						$hid_plan_amount=$plansDetails[0]['plan_amount'];
						$hid_plan_credit=$plansDetails[0]['plan_credit'];

						$query = $this->db->query("select * from tbl_users WHERE user_id='$user_id' ");
						$userDetails = $query->result_array();
						$show_total_amount = $userDetails[0]['total_amount'];
						$show_total_credit = $userDetails[0]['total_credit'];

					}
					else
					{
						if ($this->session->userdata('front_logged_in')) {
							$session_data = $this->session->userdata('front_logged_in');
							$user_id = $session_data['user_id'];
							$user_name = $session_data['user_name'];

							$query=$this->db->query("select * from  tbl_users  WHERE user_id='$user_id' ");
							$userDetails= $query->result_array();
							$show_total_amount=$userDetails[0]['total_amount'];
							$show_total_credit=$userDetails[0]['total_credit'];



							$sess_currency = '';
							$sess_plan_id = '';
							$hid_plantype_id= '';
							$hid_plan_id= '';
							$hid_plan_amount= '';
							$hid_plan_credit= '';





						} else {

							$sess_currency = '';
							$sess_plan_id = '';
							$hid_plantype_id= '';
							$hid_plan_id= '';
							$hid_plan_amount= '';
							$hid_plan_credit= '';


						}


					}

					if($hid_plan_credit=='')
					{
						$hid_plan_credit='0';
					}

					?>

					<input type="hidden" id="currency" name="currency" value="<?php echo $currency_default;?>">

					<input type="hidden" name="hid_plantype_id" id="hid_plantype_id" value="<?php echo $hid_plantype_id?>">

					<input type="hidden" name="hid_plan_amount" id="hid_plan_amount" value="<?php echo $hid_plan_amount?>">
					<input type="hidden" name="hid_plan_credit" id="hid_plan_credit" value="<?php echo $hid_plan_credit?>">

					<form id="frm_plan" name="frm_plan" method="post" action="<?php echo base_url();?>payments" onSubmit="submitPlanFrm();" >
						<div class="row mb-4">
							<div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
								<div class="card">
									<div class="card-body">
										<div class="row">
											<div class="col-12 col-sm-12 col-md-3 col-lg-3 col-xl-3">
												<h2><!-- <i class="fas fa-rupee-sign"></i>&nbsp; -->
													<?php echo $show_total_credit;?> Images</h2>


													<input type="hidden" name="hid_plan_id" id="hid_plan_id" value="<?php echo $hid_plan_id?>">




													<small>Your Balance</small>
												</div>

												<div class="col-12 col-sm-12 col-md-9 col-lg-9 col-xl-9">
													<div class="text-center pt-3">

														<div class="row">
															<!-- <div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-2 pb-2"><?php echo $currency_default;?></div> -->
															<div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-8 pb-2">
																<div class="input-group mb-2">
																	<div class="input-group-prepend">
																		<div class="input-group-text"><?php echo $currency_default;?></div>
																	</div>
																	<input readonly="" type="text" class="form-control" name="inputAmount" id="inputAmount" placeholder="Enter amount to be added" value="<?php echo $hid_plan_amount?>" required="">
																</div>
																<!-- <input type="text" class="form-control" name="inputAmount" id="inputAmount" placeholder="Enter amount to be added" value="<?php echo $hid_plan_amount?>" required=""> -->
															</div>
															<div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-4 pb-2">
																<button id="add_money_btn" type="submit" class="btn btn-piquic mb-2   <?php if($hid_plan_amount=='') { ?> d-none <?php } ?>">Add Money</button>
																
															</div>
														</div>

													</div>
												</div>


											<!-- <div class="col-12 col-sm-12 col-md-3 col-lg-3 col-xl-3">
												<div class="pt-3">
													<select id="currency" name="currency" class="form-control" required="" onchange="changePlan(this.value);" disabled>
														<option value="INR" <?php if($currency_default=='INR') { ?> selected <?php } ?>>INR</option>
														<option value="USD" <?php if($currency_default=='USD')  { ?> selected <?php } ?>>USD</option>
														<option value="EUR" <?php if($currency_default=='EUR') {  ?> selected <?php } ?>>EUR</option>
														<option value="GBP" <?php if($currency_default=='GBP') {  ?> selected <?php } ?>>GBP</option>
														<option value="AUD" <?php if($currency_default=='AUD') {    ?> selected <?php } ?>>AUD</option>
														<option value="CNY" <?php if($currency_default=='CNY') {  ?> selected <?php } ?>>CNY</option>
													</select>
													<small>Set the currency at My Account Page</small>
												</div>
											</div> -->
										</div>
									</div>
								</div>
							</div>
						</div>

					</form>

					<div class="row my-4">					
						<div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
							<div class="full-w">
								<ul class="nav nav-tabs" id="myTab" role="tablist">
									<li class="nav-item px-3 text-uppercase">
										<a class="nav-link active" id="plans-slct-tab" data-toggle="tab" href="#plans-slct" role="tab" aria-controls="plans-slct" aria-selected="true">plans</a>
									</li>
									<li class="nav-item px-3 text-uppercase">
										<a class="nav-link" id="billing-tab" data-toggle="tab" href="#billing" role="tab" aria-controls="billing" aria-selected="false">billing history</a>
									</li>
									<!-- <li class="nav-item px-3 text-uppercase">
										<a class="nav-link" id="xxxxxx-tab" data-toggle="tab" href="#xxxxxx" role="tab" aria-controls="xxxxxx" aria-selected="false">xxxxxx</a>
									</li> -->
								</ul>


								<div class="tab-content" id="myTabContent">



									<div class="tab-pane fade show active" id="plans-slct" role="tabpanel" aria-labelledby="plans-slct-tab">
										<div class="row p-2">

											<?php
                   //echo "<pre>";
                   //print_r($plansDetails);
											if(!empty($plansTypeDetails))
											{
												foreach($plansTypeDetails as $key => $planrow)
												{
													$plntyid=$planrow['plntyid'];
													$plntypname=$planrow['plntypname'];
													$plandetails=$planrow['plandetails'];
													
													?>
													<div class="col-12 col-sm-12 col-md-6 col-lg-6 col-xl-6">
														<div class="p-4">
															<p class="lead"><?php echo $plntypname;?></p>
															<small class="text-muted">
																Designed for infrequent sending, you buy images ( as opposed to recurring charges) as necessary, based on volume.
															</small>

															<div class="row pt-3 <?php if($plntyid=='1'){ echo 'clickbone-group'; } if($plntyid=='2'){ echo 'clickbmon-group'; } ?>">
																<div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12 pb-3">
																	<small><b>How many images would you like to purchase?</b></small>
																</div>

																<?php
                   //echo "<pre>";
                   //print_r($plandetails);
																if(!empty($plandetails))
																{
																	foreach($plandetails as $key1 => $planrow1)
																	{
																		$planid=$planrow1['planid'];
																		$plan_name=$planrow1['plan_name'];
																		$plan_amount=$planrow1['plan_amount'];
																		$plan_credit=$planrow1['plan_credit'];
																		$plan_currency=$planrow1['plan_currency'];
																		$plan_currency_symbol=$planrow1['plan_currency_symbol'];
																		$plan_per_image=$planrow1['plan_per_image'];																
																		
																		?>

																		<div class="col-12 col-sm-12 col-md-6 col-lg-6 col-xl-6 <?php if($plntyid=='1'){ echo 'clickb'; } if($plntyid=='2'){ echo 'clickbmon'; } ?> " style="cursor: pointer;" onclick="setAmount('<?php echo $plntyid;?>','<?php echo $planid;?>','<?php echo $plan_amount;?>','<?php echo $plan_credit;?>');">
																			<div id="planSelectbox<?php echo $planid;?>" class="planSelectbox w-100 text-center mb-3 py-3 border <?php if($sess_plan_id==$planid) { ?> border-piquic <?php } ?> ">
																				<?php echo $plan_credit;?> Images<br>
																				<b><?php echo $plan_currency_symbol;?><?php echo $plan_amount;?><?php if($plntyid == '2') { echo "/month"; } ?></b>
																				<br>
																				<small class="text-muted">(<?php echo $plan_currency_symbol;?><?php echo $plan_per_image;?> per image)</small>
																			</div>
																			<span id="planSelectboxTick<?php echo $planid;?>" class="planSelectboxTick text-piquic slct <?php if($sess_plan_id!=$planid) { ?> d-none <?php } ?> " id="ticko1" ><i class="fas fa-check"></i></span>
																		</div>



																		

																		<?php
																	}

																}

																else
																{
																	?>
																	<div class="col-12 col-sm-12 col-md-6 col-lg-6 col-xl-6">
																		<div class="p-4">
																			<p class="lead">No Plans</p>
																		</div>
																	</div>
																	<?php	
																}
																?>		




															</div>
														</div>
													</div>


													<?php
												}

											}
											else
											{
												?>
												<div class="col-12 col-sm-12 col-md-6 col-lg-6 col-xl-6">
													<div class="p-4">
														<p class="lead">No Plans</p>
													</div>
												</div>
												<?php
											}
											?>

											


										</div>
									</div>

									<div class="tab-pane fade" id="billing" role="tabpanel" aria-labelledby="billing-tab">
										<div class="p-3">




											<?php
                   //echo "<pre>";
                   //print_r($plandetails);
											if(!empty($billingDetails))
											{
												foreach($billingDetails as $keybill => $planrowbill)
												{
													$billing_id=$planrowbill['billing_id'];
													$billing_unique_id=$planrowbill['billing_unique_id'];
													$billing_plan_id=$planrowbill['billing_plan_id'];
													$billing_plan_credit=$planrowbill['billing_plan_credit'];
													$billing_plan_amount=$planrowbill['billing_plan_amount'];
													$billing_date_time=$planrowbill['billing_date_time'];
													$billing_status=$planrowbill['billing_status'];
													$payment_status=$planrowbill['payment_status'];

													$plntypname=$planrowbill['plntypname'];

													$payment_type=$planrowbill['payment_type'];

													$billing_date_time=date("D, M d ,Y H:i A",strtotime($billing_date_time));

													$billing_date=date("D, M d Y",strtotime($billing_date_time));
													$billing_time=date(" H:i A",strtotime($billing_date_time));



													$user_query1=$this->db->query("select * FROM tbl_users where user_id=$user_id");

													$userDetails1= $user_query1->result_array();
													//print_r($userDetails1);
													$user_name=$userDetails1[0]['user_name'];
													$user_company=$userDetails1[0]['user_company'];
													$user_address=$userDetails1[0]['user_address'];
													$user_mobile=$userDetails1[0]['user_mobile'];



													$paym_query1=$this->db->query("select * FROM tbl_paymenttype where paytyid=$payment_type");

													$paymDetails1= $paym_query1->result_array();
													//print_r($userDetails1);
													$paytypname=$paymDetails1[0]['paytypname'];


													
													?>


													<div class="border-bottom p-3 row text-center text-sm-left">
														<div class="col-12 col-sm-12 col-md-8 col-lg-8 col-xl-8">
															<span class="lead text-piquic"><?php echo $billing_unique_id;?></span><br>
															<small>
																<?php echo $plntypname;?> ( <?php echo $total_image;?> images. )<br>
																<?php echo $book_date_time;?>
															</small>
														</div>

														<div class="col-12 col-sm-12 col-md-2 col-lg-2 col-xl-2">
															<div class="lead pt-3"><?php
															if($currency_default=="USD") { echo"&#36;"; }
															elseif($currency_default=="INR") { echo"&#x20B9;"; }
															elseif($currency_default=="EUR") { echo"&#128;"; }
															elseif($currency_default=="GBP") { echo"&#163;"; }
															elseif($currency_default=="AUD") { echo"&#36;"; }
															elseif($currency_default=="CNY") { echo"&#165;"; }
															?>&nbsp;<?php echo $billing_plan_amount;?></div>
														</div>

														<div class="col-12 col-sm-12 col-md-2 col-lg-2 col-xl-2">
															<?php if($payment_status=="Notpaid"||$payment_status=="Not paid" ||$payment_status=="Not Paid"){ ?>
																<a href="<?php echo base_url(); ?>paymentdetails/<?php echo $billing_id; ?>"  class="btn btn-block btn-piquic mb-2">Pay Now</a>
															<?php } else { ?>
															<div class="pt-3">
																<button type="submit" class="btn btn-block btn-outline-piquic mb-2" data-toggle="modal" data-target="#modal_<?php echo $billing_id; ?>">View</button>
															</div>

															<div class="modal" id="modal_<?php echo $billing_id; ?>" tabindex="-1" role="dialog">
																<div class="modal-dialog" role="document" style="max-width: 976px !important;">
																	<div class="modal-content">
																		<div class="modal-body" id="printableArea<?php echo $billing_id;?>">
																				<div class="border rounded p-2">
																					<div class="row pt-3 px-3">
																						<div class="col-md-6">
																							<img src="<?php echo PIQUIC_URL;?>images/logo-piquic-md.png">
																						</div>

																						<div class="col-md-6 text-right">
																							<span class="font-weight-bold mb-1">Billing ID #<?php echo $billing_unique_id ?></span><br>
																							<span class="text-piquic"><?php echo $billing_date_time; ?></span>
																						</div>
																					</div>

																					<hr >

																					<div class="row p-3">
																						<div class="col-md-6">
																							<p class="font-weight-bold mb-4">Client Information</p>
																							<table>
																								<tbody>
																									<tr>
																										<td class="pr-3"><span class="font-weight-bold">Name: </span></td>
																										<td><?php echo $user_name; ?></td>
																									</tr>
																									<tr>
																										<td class="pr-3"><span class="font-weight-bold">Company Name: </span></td>
																										<td><?php echo $user_company; ?></td>
																									</tr>
																									<tr>
																										<td class="pr-3"><span class="font-weight-bold">Address: </span></td>
																										<td><?php echo $user_address; ?></td>
																									</tr>
																									<tr>
																										<td class="pr-3"><span class="font-weight-bold">Mobile: </span></td>
																										<td><?php echo $user_mobile; ?></td>
																									</tr>
																								</tbody>
																							</table>
																						</div>

																						<div class="col-md-6">

																							<p class="font-weight-bold mb-4">Booking Details</p>

																							<div class="row">
																								<div class="col-md-6">
																									<div class="rounded-lg bg-light p-3 mb-3">
																										<span class="font-weight-bold">Payment Type: </span><br>
																										<?php echo $paytypname;?>
																									</div>
																								</div>

																								<div class="col-md-6">
																									<div class="rounded-lg bg-light p-3 mb-3">
																										<span class="font-weight-bold">Payment Status: </span><br>
																										<?php echo $payment_status;?>
																									</div>
																								</div>

																								<div class="col-md-6">
																									<div class="rounded-lg bg-light p-3 mb-3">
																										<span class="font-weight-bold">Date: </span><br>
																										<?php echo $billing_date;?>
																									</div>
																								</div>

																								<div class="col-md-6">
																									<div class="rounded-lg bg-light p-3 mb-3">
																										<span class="font-weight-bold">Time: </span><br>
																										<?php echo $billing_time;?>
																									</div>
																								</div>
																							</div>
																						</div>
																					</div>

																					<div class="d-flex justify-content-between text-piquic bg-light border">
																						<div class="py-3 px-5 text-center">
																							<div class="mb-2">Plan Type</div>
																							<div class="lead"><?php echo $plntypname;?></div>
																						</div>

																						<div class="py-3 px-5 text-center">
																							<div class="mb-2">Total Credit</div>
																							<div class="lead"><?php echo $billing_plan_credit; ?> Images</div>
																						</div>

																						<div class="py-3 px-5 text-center">
																							<div class="mb-2">Total amount</div>
																							<div class="lead">
																								<?php
																								if($currency_default=="USD") { echo"&#36;"; }
																								elseif($currency_default=="INR") { echo"&#x20B9;"; }
																								elseif($currency_default=="EUR") { echo"&#128;"; }
																								elseif($currency_default=="GBP") { echo"&#163;"; }
																								elseif($currency_default=="AUD") { echo"&#36;"; }
																								elseif($currency_default=="CNY") { echo"&#165;"; }
																								?>&nbsp;<?php echo $billing_plan_amount ?>
																							</div>
																						</div>
																					</div>
																				</div>
																			</div>

																			<div class="modal-footer d-flex justify-content-between">
																				<button type="button"  id="btnPrint" val="<?php echo $billing_id;?>" class="btn btn-outline-piquic px-5" >Print</button>
																				<button type="button" class="btn btn-outline-danger px-5" data-dismiss="modal">Close</button>
																			</div>
																		</div>
																	</div>
																</div>

															<?php } ?>
														</div>

														
										</div>

										<?php
									}

								}

								else
								{
									?>
									<div class="border-bottom p-3">
										<div class="d-flex justify-content-between">
											<div>
												<span class="lead text-piquic">No History</span>
											</div>
										</div>
										<?php
									}
									?>





								</div>
							</div>

							<div class="tab-pane fade" id="xxxxxx" role="tabpanel" aria-labelledby="xxxxxx-tab">...</div>
						</div>
					</div>
				</div>

			</div>
		</div>
	</div>
</div>
</div>

<?php
$this->load->view('front/includes/footer');
?>

<script type="text/javascript" src="https://cdn.datatables.net/v/bs4/dt-1.10.18/datatables.min.js"></script>

<script type="text/javascript">



	jQuery.fn.extend({
		printElem: function() {
			var cloned = this.clone();
			var printSection = $('#printSection');
			if (printSection.length == 0) {
				printSection = $('<div id="printSection"></div>')
				$('body').append(printSection);
			}
			printSection.append(cloned);
			var toggleBody = $('body *:visible');
			toggleBody.hide();
			$('#printSection, #printSection *').show();
			window.print();
			printSection.remove();
			toggleBody.show();
		}
	});



	$(document).ready(function(){
		$(document).on('click', '#btnPrint', function(){

			var billing_id=$(this).attr('val');
		//alert(billing_id);
		$('#printableArea'+billing_id).printElem();
	});
	});


	$(document).ready(function() {

		changePlan('<?php echo $currency_default;?>');

		$(".clickbone-group .clickb ").on("click", function(e) {

			$('.clickbmon div').removeClass('border-piquic');
			$('.clickbmon span').addClass('d-none');
			$(this).parent().find('.clickb div').removeClass('border-piquic');
			$(this).parent().find('.clickb span').addClass('d-none');



			$(this).children('div').addClass('border-piquic');
			$(this).children('span').removeClass('d-none');			       
		});

		$(".clickbmon-group .clickbmon ").on("click", function(e) {
			$('.clickb div').removeClass('border-piquic');
			$('.clickb span').addClass('d-none');

			$(this).parent().find('.clickbmon div').removeClass('border-piquic');
			$(this).parent().find('.clickbmon span').addClass('d-none');
			$(this).children('div').addClass('border-piquic');
			$(this).children('span').removeClass('d-none');			       
		});


// $("#clreyemk").on("click", function() {
	
// 			$('.rdeyemk-group').find('.rdeyemk div img').removeClass('border');
// 		});


})


</script>

<script type="text/javascript">
	

	function changePlan(currency)
	{
//alert(currency);
$.ajax({
	type:'POST',					
	url : "<?php echo base_url();?>front/plans/getPlanDetailByCurrency", 
	data:{'currency':currency},
	success: function(response) {	
				 //alert(response);
				//location.reload();
				$("#plans-slct").html($.trim(response));
			}
		});
}


$('#set-currency').on('change', function(){
	var selected = $(this).val();
        // alert(selected);
        
        $.post('<?php echo site_url( $this->uri->segment(1) . '/setcurrency'); ?>', {currency: selected}, function() {
        	location.reload();
        });
      });

function setAmount(hid_plantype_id,planid,plan_amount,plan_credit)
{

	$("#hid_plantype_id").val(hid_plantype_id);
	$("#hid_plan_id").val(planid);
	$("#hid_plan_amount").val(plan_amount);
	$("#hid_plan_credit").val(plan_credit);
	$("#inputAmount").val(plan_amount);

	$(".planSelectbox").removeClass("border-piquic");
	$(".planSelectboxTick").addClass("d-none");

	$("#planSelectbox"+planid).addClass("border-piquic");
	$("#planSelectboxTick"+planid).removeClass("d-none");

	$("#add_money_btn").removeClass("d-none");
}

function submitPlanFrm()
{
	var inputAmount = $("#inputAmount").val();
	var currency = $("#currency").val();
	//alert(inputAmount);
	//alert(currency);
	if($.trim(inputAmount)!='' )
	{
		document.frm_plan.submit();
	}
	else
	{
		// alert("Select Plan");
		$('#modal_msg').modal('show');
		$('#txtMsg').html('Select a plan first.');
		//window.location.href="<?php echo base_url();?>plans";
		return false;
	}

}

</script>

