<?php
session_start();
error_reporting(0);
$data['page_title'] = "process list";

$this->load->view('front/includes/header',$data);
// $this->load->view('front/includes/menu');
$this->load->view('front/includes/nav');



$session_data=$this->session->userdata('front_logged_in');
//print_r("session ".$session_data);
$userid = $session_data['user_id'];

$i = 1;
$query=$this->db->query("SELECT * FROM tbl_style WHERE user_id='$userid' AND status='2' ");   
foreach ($query->result() as $resrow)
{ 

	if($resrow->skuno==$sku)
	{
		$page_key=$i; 
		break;
	}
	$i++;
}

$perPageRecord='2';

//echo $page_key;





?>

<style type="text/css">
	span.details-control {
		background: url('website-assets/details_open.png') no-repeat center center;
		cursor: pointer;
	}
	tr.details span.details-control {
		background: url('website-assets/details_close.png') no-repeat center center;
	}


</style>
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/bs4/dt-1.10.18/datatables.min.css"/>

<script src="<?php echo base_url();?>website-assets/dragResizeRotatableJs/jquery.ui.rotatable.js"></script>
<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>website-assets/dragResizeRotatableJs/jquery-ui.css"/> 
<link rel="stylesheet" href="<?php echo base_url();?>website-assets/dragResizeRotatableJs/jquery.ui.rotatable.css">
<div class="container-fluid">
	<div class="row mb-4">



		<div class="container-fluid">
			<div class="row mb-3">

				<?php
				//$this->load->view('front/includes/sidebar');

				?>

                <!-- <div class="col-12 col-sm-12 col-md-9 col-lg-9 col-xl-9 border"> -->
				<div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
				<!-- <div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12 mainscroll"> -->

					<div class="container pt-3">
						<div class="row mb-4">
							<div class="col-6 col-sm-6 col-md-6 col-lg-6 col-xl-6">
								<h1>Processing List</h1>
							</div>

					<!-- <div class="col-6 col-sm-6 col-md-6 col-lg-6 col-xl-6">
						<div class="text-right w-100">
							<span class="text-black-50 pr-2" data-toggle="collapse" data-target="#filter-it"><i class="fas fa-sliders-h fa-2x"></i></span>
						</div>
					</div> -->
				</div>

				<div class="row my-4">					
					<div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
						<!-- <div class="table-responsive"> -->
							<table id="pro-cess" class="table border-bottom table-bordered">
								<thead class="thead-light">
									<tr class="text-success">
										<!-- <th></th> -->
										<th scope="col" data-orderable="false"></th>
										<th scope="col">SKU</th>
										<th scope="col">Category</th>
										<th scope="col">Stage</th>
										<th scope="col">Status</th>
										<th scope="col">Payment Status</th>
										<th scope="col">Action</th>
									</tr>
								</thead>

								<tbody>
									
								</tbody>
							</table>
						<!-- </div> -->
					</div>

				</div>
			</div>
		</div>
	</div>
</div>

<!-- <div class="modal fade" id="modal_preview" tabindex="-1" role="dialog" aria-hidden="true">
	<div class="modal-dialog" role="document" style="max-width: 23.5em !important;">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title text-dark"></h5>
				<button type="button" class="close text-danger" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true" >&times;</span>
				</button>
			</div>
			<div class="modal-body">
				<div id="preview-data"></div>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
			</div>
		</div>
	</div>
</div> -->

<?php
$this->load->view('front/includes/footer');
?>



<script type="text/javascript" src="https://cdn.datatables.net/v/bs4/dt-1.10.18/datatables.min.js"></script>

<script type="text/javascript">
	
	var table  = $('#pro-cess').DataTable( {
        //"pageLength": '<?php echo $perPageRecord;?>',
        "pageLength": '10',
        <?php
        if($style_id!='' && $sku!='')
        {
        	?>
        	"search": {
        		"search": "<?php echo $sku;?>"
        	},
       // table.columns(1).search( '<?php echo $sku;?>').draw();
       <?php
    }
    ?>
		//"order": [[ 1, "desc" ]],
		"lengthMenu": [ [10,25, 50, 100], [10,25, 50, 100] ],
		"oLanguage": {
			"sSearch": "Search SKU: "
		},
		"columnDefs": [ {
			"targets": 0,
			"className": 'text-center lead'
		}, {
			"targets": 1,
			"orderable": true,
			"className": 'lead'
		}, {
			"targets": 2,
			"orderable": true,
			"className": 'lead'
		}, {
			"targets": 3,
			"orderable": true,
			"className": 'lead'
		}, {
			"targets": 4,
			"orderable": true,
			"className": 'lead'
		}, {
			"targets": 5,
			"orderable": true,
			"className": 'lead'
		} ],
		"ajax": {
			"url": "<?php echo base_url();?>front/processing/processing_list",
			"type": "GET"
		},
		rowCallback: function (row, data) {

			var firstColumnCheck=$.trim(data[0]);
             //console.log(firstColumnCheck);
             if(firstColumnCheck=='<span class="text-danger"><i class="fas fa-exclamation-circle"></i></span>')
             {
             	$(row).addClass('table-danger text-danger');
             }
             else
             {
             	$(row).removeClass('table-danger text-danger');
             }

             $(row).addClass(' details-control');


        /*if ( data.cashflow.manual > 0 || data.cashflow.additional_repayment > 0 ) {
            $(row).addClass('fontThick');
        }
        //if it is not the summation row the row should be selectable
        if ( data.cashflow.position !== 'L' ) {
            $(row).addClass('selectRow');
         }*/
      }





   } );

	<?php
   /* if($style_id!='' && $sku!='')
    {
    ?>
   table.columns(1).search( '<?php echo $sku;?>').draw();
    <?php
 }*/
 ?>



/*
function format ( d ) {
    return 'Full name: '+d.first_name+' '+d.last_name+'<br>'+
        'Salary: '+d.salary+'<br>'+
        'The child row can contain any data you wish, including links, images, inner tables etc.';
}

 // Array to track the ids of the details displayed rows
    var detailRows = [];
 
    $('#pro-cess tbody').on( 'click', 'tr td.details-control', function () {
        var tr = $(this).closest('tr');
        var row = dt.row( tr );
        var idx = $.inArray( tr.attr('id'), detailRows );
 
        if ( row.child.isShown() ) {
            tr.removeClass( 'details' );
            row.child.hide();
 
            // Remove from the 'open' array
            detailRows.splice( idx, 1 );
        }
        else {
            tr.addClass( 'details' );
            row.child( format( row.data() ) ).show();
 
            // Add to the 'open' array
            if ( idx === -1 ) {
                detailRows.push( tr.attr('id') );
            }
        }
    } );
 
    // On each draw, loop over the `detailRows` array and show any child rows
    dt.on( 'draw', function () {
        $.each( detailRows, function ( i, id ) {
            $('#'+id+' td.details-control').trigger( 'click' );
        } );
    } );

    */


    /* Formatting function for row details - modify as you need */
    function format ( d ) {
    // `d` is the original data object for the row
/*    return '<table cellpadding="5" cellspacing="0" border="0">'+
        '<tr>'+
            '<td>Full name:</td>'+
            '<td>'+d.name+'</td>'+
        '</tr>'+
        '<tr>'+
            '<td>Extension number:</td>'+
            '<td>'+d.extn+'</td>'+
        '</tr>'+
        '<tr>'+
            '<td>Extra info:</td>'+
            '<td>And any further details here (images etc)...</td>'+
        '</tr>'+
        '</table>';*/
        console.log(d);

        var style_str=d[1];
        var style_arr=style_str.split("@@@@@@");
        var style_id=$.trim(style_arr[1]);

     //alert(style_id);
     var remote = $.ajax({
     	type:'POST',
     	url:'<?php echo PIQUIC_AJAX_URL ;?>/process-details.php',
     	data: {'style_id':style_id},
     	async: false
     }).responseText;

     // var pre_view = $.ajax({
     // 	type:'POST',
     // 	url:'<?php echo PIQUIC_AJAX_URL ;?>/pre_view.php',
     // 	data: {'style_id':style_id},
     // 	async: false
     // }).responseText;

     var comments=d[6];
     comments=comments.replace( /<.*?>/g, '' );


     var tab_str= '<table cellpadding="5" cellspacing="0" border="0">'+
     '<tr>'+
     '<td>Stage:</td>'+
     '<td>'+d[3]+'</td>'+
     '</tr>'+
     '<tr>'+
     '<td>Status:</td>'+
     '<td>'+d[4]+'</td>'+
     '</tr>'+
     '<tr>'+
     '<td>Comments:</td>'+
     '<td>'+comments+'</td>'+
     '</tr>'+
     '<tr><td colspan=4>'+remote+'</td></tr>';

     // tab_str += '<tr><td colspan=4 class="text-center"><span class="btn btn-piquic" data-toggle="modal" data-target="#modal_preview">Final Preview</span></td></tr>';


        //if(d[4].indexOf("Payment Pending")!='-1')
        if(d[5].indexOf("Not Paid")!='-1')
        {
        	tab_str+='<tr>'+
        	'<td >Payment Pending</td>'+
        	'<td ><a href="<?php echo base_url();?>plans">Add credits</a> to process your images</td>'+
        	'</tr>';
        }

        // $('#preview-data').html(pre_view);

        tab_str+='</table>';

        return tab_str;
     }


  // Add event listener for opening and closing details
  $('#pro-cess tbody').on('click', 'tr.details-control', function () {

    	//console.log(this);
    	var tr = $(this).closest('tr');
    	var row = table.row( tr );

        //console.log(tr);
        console.log(row);

        var d=row.data();
        console.log(d);
        var style_str=d[1];
        var style_arr=style_str.split("@@@@@@");
        var style_id=$.trim(style_arr[1]);

		//alert(style_id);

		if ( row.child.isShown() ) {
        	//alert("close");
        	$("#div_detail"+style_id).html('<i class="fa fa-fw fa-plus"></i>');
            // This row is already open - close it
            row.child.hide();
            tr.removeClass('shown');
         }
         else {
        	//alert("open");
        	$("#div_detail"+style_id).html('<i class="fa fa-fw fa-minus"></i>');
            // Open this row

            //console.log(row.data());
            row.child( format(row.data()) ).show();
            tr.addClass('shown');
         }
      } );


  <?php if($style_id!='' && $sku!='')
  {
  	?>

  	$( "tr.details-control" ).each(function() {
  		console.log("aaaaaa");
  	});
        //$( "#book" ).load(function() {
          //  $('#pro-cess tbody').on('load', 'tr.details-control', function () {

          	$( "tr.details-control" ).load(function() {
        // Handler for .load() called.
        // alert("zzzzzz");
        var tr = $("tr.details-control").closest('tr');
        var row = table.row( tr );

       //console.log(tr);
       console.log(row);

       var d=row.data();
       console.log(d);
       var style_str=d[1];
       var style_arr=style_str.split("@@@@@@");
       var style_id=$.trim(style_arr[1]);

        // alert(style_id);

        if ( row.child.isShown() ) {
            //alert("close");
            $("#div_detail<?php echo $style_id;?>").html('<i class="fa fa-fw fa-plus"></i>');
            // This row is already open - close it
            row.child.hide();
            tr.removeClass('shown');
         }
         else {
            //alert("open");
            $("#div_detail<?php echo $style_id;?>").html('<i class="fa fa-fw fa-minus"></i>');
            // Open this row

            //console.log(row.data());
            row.child( format(row.data()) ).show();
            tr.addClass('shown');
         }

      });
          	<?php
          }
          ?>




       </script>