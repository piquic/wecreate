<?php
$data['page_title'] = "plans";

$this->load->view('front/includes/header',$data);
// $this->load->view('front/includes/menu');
$this->load->view('front/includes/nav');

if ($this->session->userdata('front_logged_in')) {
	$session_data = $this->session->userdata('front_logged_in');
	$user_id = $session_data['user_id'];
	$user_name = $session_data['user_name'];
	
	
	
} else {
	
	$session_data = $this->session->userdata('front_logged_in');
	$user_id = $session_data['user_id'];
	$user_name = $session_data['user_name'];
	
	
}

?>
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/bs4/dt-1.10.18/datatables.min.css"/>

<div class="container-fluid">
	<div class="row">

		<?php //$this->load->view('front/includes/sidebar'); ?>

		<!-- <div class="col-12 col-sm-12 col-md-9 col-lg-9 col-xl-9 border mainscroll"> -->
		<div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12 mainscroll">

			<div class="container pt-3">
				<div class="row mb-4">
					<div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
						<h1>My Order </h1>
					</div>
				</div>

	<!--  -->

					<div class="row my-4">					
						<div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
							<div class="full-w">
								<ul class="nav nav-tabs" id="myTab" role="tablist">
									<li class="nav-item px-3 text-uppercase">
										<a class="nav-link active" id="plans-slct-tab" data-toggle="tab" href="#plans-slct" role="tab" aria-controls="plans-slct" aria-selected="true">My Order</a>
									</li>
									<!-- <li class="nav-item px-3 text-uppercase">
										<a class="nav-link" id="billing-tab" data-toggle="tab" href="#billing" role="tab" aria-controls="billing" aria-selected="false">billing history</a>
									</li> -->
									<!-- <li class="nav-item px-3 text-uppercase">
										<a class="nav-link" id="xxxxxx-tab" data-toggle="tab" href="#xxxxxx" role="tab" aria-controls="xxxxxx" aria-selected="false">xxxxxx</a>
									</li> -->
								</ul>


								<div class="tab-content" id="myTabContent">



									<div class="tab-pane fade show active" id="plans-slct" role="tabpanel" aria-labelledby="plans-slct-tab">
										<div class="p-3">

											<?php
                   // echo "<pre>";
                   // print_r($orderDetails);
               
											
                   //echo "<pre>";
                   //print_r($plandetails);
											if(!empty($orderDetails))
											{
											//	echo "order";
												foreach($orderDetails as $keybill => $bookorder)
												{
													$order_id =$bookorder['order_id'];

													$total_price=$bookorder['total_price'];
													$total_image=$bookorder['total_image'];
													$plan_type=$bookorder['plan_type'];
													$payment_status=$bookorder['p_status'];
													$create_date=$bookorder['create_date'];
													
													$book_date_time=date("D, M d ,Y H:i A",strtotime($create_date));
													$book_id=$bookorder['book_id'];
													$query1=$this->db->query("select * FROM tbl_plan INNER JOIN tbl_plantype on tbl_plan.plntyid=tbl_plantype.plntyid WHERE tbl_plan.planid='$plan_type'");

													$bookDetails1= $query1->result_array();
													
													$plntypname=$bookDetails1[0]['plntypname'];
													?>


													<div id="plan_1" class="border-bottom p-3">
														<div class="d-flex justify-content-between">
															<div>
																<span class="lead text-piquic"><?php echo $order_id;?></span><br>
																<small>
																	<?php echo $plntypname;?> ( <?php echo $total_image;?> images. )<br>
																	
																	<?php echo $book_date_time;?>
																</small>
															</div>

															<div class="pt-3">
																<span class="lead">
																	<?php
																if($currency_default=="USD")
																{
																	echo"&#36;";
																}
																elseif($currency_default=="INR")
																{
																	echo"&#x20B9;";	
																}
																elseif($currency_default=="EUR")
																{
																	echo"&#128;";
																}
																elseif($currency_default=="GBP")
																{
																	echo"&#163;";
																}
																elseif($currency_default=="AUD")
																{
																	echo"&#36;";
																}
																elseif($currency_default=="CNY")
																{
																	echo"&#165;";
																}
																?>&nbsp;<?php echo $total_price;?></span>
															</div>

															<?php
															if($payment_status=="Notpaid"||$payment_status=="Not paid"){
																?>
																<div class="pt-3">
																	<a href="<?php echo base_url(); ?>booking_paymentdetails/<?php echo $book_id; ?>"  class="btn btn-outline-piquic mb-2">Pay Now</a>
																	<!-- <button type="submit" class="btn btn-outline-piquic mb-2">Pay Now</button> -->
																</div>
																<?php
															}else{
																
																?>
																<div class="pt-3">
																	<button type="submit" class="btn btn-outline-piquic mb-2">View</button>
																</div>
																<?php
															} ?>
											<!-- 	<div class="pt-3">
													<button type="submit" class="btn btn-outline-piquic mb-2">View</button>
												</div> -->
											</div>
										</div>

										<?php
									}

								}

								else
								{
									?>
									<div id="plan_1" class="border-bottom p-3">
										<div class="d-flex justify-content-between">
											<div>
												<span class="lead text-piquic">No History</span>
											</div>
										</div>
									</div>
										<?php
									}
									?>





							
							</div>
											


										</div>
									

									<div class="tab-pane fade" id="billing" role="tabpanel" aria-labelledby="billing-tab">
										<div class="p-3">




											<?php
                   //echo "<pre>";
                   //print_r($plandetails);
											if(!empty($billingDetails))
											{
												foreach($billingDetails as $keybill => $bookbill)
												{
													$billing_id=$bookbill['billing_id'];
													$billing_unique_id=$bookbill['billing_unique_id'];
													$billing_plan_id=$bookbill['billing_plan_id'];
													$billing_plan_credit=$bookbill['billing_plan_credit'];
													$billing_plan_amount=$bookbill['billing_plan_amount'];
													$billing_date_time=$bookbill['billing_date_time'];
													$billing_status=$bookbill['billing_status'];
													$payment_status=$bookbill['payment_status'];

													$plntypname=$bookbill['plntypname'];

													$billing_date_time=date("D, M d ,Y H:i A",strtotime($billing_date_time));
													
													?>


													<div id="plan_1" class="border-bottom p-3">
														<div class="d-flex justify-content-between">
															<div>
																<span class="lead text-piquic"><?php echo $billing_unique_id;?></span><br>
																<small>
																	<?php echo $plntypname;?> ( <?php echo $billing_plan_credit;?> images. )<br>
																	
																	<?php echo $billing_date_time;?>
																</small>
															</div>

															<div class="pt-3">
																<span class="lead">
																	<?php
																if($currency_default=="USD")
																{
																	echo"&#36;";
																}
																elseif($currency_default=="INR")
																{
																	echo"&#x20B9;";	
																}
																elseif($currency_default=="EUR")
																{
																	echo"&#128;";
																}
																elseif($currency_default=="GBP")
																{
																	echo"&#163;";
																}
																elseif($currency_default=="AUD")
																{
																	echo"&#36;";
																}
																elseif($currency_default=="CNY")
																{
																	echo"&#165;";
																}
																?>&nbsp;<?php echo $billing_plan_amount; ?></span>
															</div>

											<!-- 	<div class="pt-3">
													<button type="submit" class="btn btn-outline-piquic mb-2">View</button>
												</div> -->
											</div>
										</div>

										<?php
									}

								}

								else
								{
									?>
									<div id="plan_1" class="border-bottom p-3">
										<div class="d-flex justify-content-between">
											<div>
												<span class="lead text-piquic">No History</span>
											</div>
										</div>
										
										<?php
									}
									?>


	</div>


							
							</div>

							<div class="tab-pane fade" id="xxxxxx" role="tabpanel" aria-labelledby="xxxxxx-tab">...</div>
						</div>
					</div>
				</div>

			</div>
		</div>
	</div>
</div>
</div>

<?php
$this->load->view('front/includes/footer');
?>

<script type="text/javascript" src="https://cdn.datatables.net/v/bs4/dt-1.10.18/datatables.min.js"></script>

<script type="text/javascript">

	$(document).ready(function() {

		changePlan('<?php echo $currency_default;?>');

		$(".clickbone-group .clickb ").on("click", function(e) {

			$('.clickbmon div').removeClass('border-piquic');
			$('.clickbmon span').addClass('d-none');
			$(this).parent().find('.clickb div').removeClass('border-piquic');
			$(this).parent().find('.clickb span').addClass('d-none');



			$(this).children('div').addClass('border-piquic');
			$(this).children('span').removeClass('d-none');			       
		});

		$(".clickbmon-group .clickbmon ").on("click", function(e) {
			$('.clickb div').removeClass('border-piquic');
			$('.clickb span').addClass('d-none');

			$(this).parent().find('.clickbmon div').removeClass('border-piquic');
			$(this).parent().find('.clickbmon span').addClass('d-none');
			$(this).children('div').addClass('border-piquic');
			$(this).children('span').removeClass('d-none');			       
		});


// $("#clreyemk").on("click", function() {
	
// 			$('.rdeyemk-group').find('.rdeyemk div img').removeClass('border');
// 		});


})


</script>
<!-- 
<script type="text/javascript">
	

	function changePlan(currency)
	{
//alert(currency);
$.ajax({
	type:'POST',					
	url : "<?php echo base_url();?>front/plans/getPlanDetailByCurrency", 
	data:{'currency':currency},
	success: function(response) {	
				 //alert(response);
				//location.reload();
				$("#plans-slct").html($.trim(response));
			}
		});
}


$('#set-currency').on('change', function(){
	var selected = $(this).val();
        // alert(selected);
        
        $.post('<?php echo site_url( $this->uri->segment(1) . '/setcurrency'); ?>', {currency: selected}, function() {
        	location.reload();
        });
     });

function setAmount(hid_plantype_id,planid,plan_amount,plan_credit)
{

	$("#hid_plantype_id").val(hid_plantype_id);
	$("#hid_plan_id").val(planid);
	$("#hid_plan_amount").val(plan_amount);
	$("#hid_plan_credit").val(plan_credit);
	$("#inputAmount").val(plan_amount);

	$(".planSelectbox").removeClass("border-piquic");
	$(".planSelectboxTick").addClass("d-none");

	$("#planSelectbox"+planid).addClass("border-piquic");
	$("#planSelectboxTick"+planid).removeClass("d-none");
}

function submitPlanFrm()
{
	var inputAmount = $("#inputAmount").val();
	var currency = $("#currency").val();
	//alert(inputAmount);
	//alert(currency);
	if($.trim(inputAmount)!='' )
	{
		document.frm_plan.submit();
	}
	else
	{
		alert("Select Plan");
		return false;
	}

}

</script>
 -->
