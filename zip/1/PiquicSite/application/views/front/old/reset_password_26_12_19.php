<?php
session_start();


$data['page_title'] = "reset password";

$this->load->view('front/includes/header',$data);
// $this->load->view('front/includes/menu');

  if ($this->session->userdata('front_logged_in')) {
    $session_data = $this->session->userdata('front_logged_in');
     $user_id = $session_data['user_id'];
     $user_name = $session_data['user_name'];
     $_SESSION['user_id']=$user_id;
     $_SESSION['user_name']=$user_name;
    }
    else
    {
    $user_id = '';
    $user_name = ''; 
    $_SESSION['user_id']=$user_id;
     $_SESSION['user_name']=$user_name;
    }

 ?>
  

<div class="container">
  <div class="row my-4">
    <div class="col-12 col-sm-12 col-md-6 col-lg-6 col-xl-6">
      <h1>Reset Your Password:</h1>
    </div>
  </div>
    <div class="col-12 col-sm-12 col-md-12 col-lg-6 col-xl-6 pb-4">
      <div class="shadow">
        <form class="form form-horizontal has-validation-callback"  id="resetpassword_form" method="post">
                 <div id="success_message" style="display:none;">
          <div class="alert alert-success">
          <strong>Your Password has been successfully updated!</strong>.
          </div>
            </div>
          <table class="table table-borderless">

            <tr>
              <td class="pt-3 text-right"><i class="fas fa-lock"></i></td>
              <input type="hidden" class="form-control mb-2" id="uid" name="uid" value="<?php echo $uid; ?>">
              <td><input type="password" class="form-control mb-2" id="upassword" name="upassword" placeholder="Set a password" required>
                <?php echo form_error('upassword', '<div class="error" style="color:red;">', '</div>'); ?>

              </td>
            </tr>
            <tr>
              <td class="pt-3 text-right"><i class="fas fa-lock"></i></td>
              <td><input type="password" required class="form-control mb-2" name="ure_password" id="ure_password" placeholder="Re-enter the password">
                <?php echo form_error('ure_password', '<div class="error" style="color:red;">', '</div>'); ?>
                <span id='message' class="error"></span>
              </td>
            </tr>
            
            <tr>
              <td colspan="2" class="text-center">
                <button name="regSubmit" type="submit" class="btn btn-piquic ">Reset Password <i class="fas fa-key"></i></button>
                <span id="success" class="successMsg" style="display:none;"></span>
                <!-- <span id="loding"><i class="fas fa-circle-notch text-success fa-spin"></i></span> -->
              </td>
            </tr>
          </table>
        </form>
       
      </div>
    </div>

  
</div>

<?php
$this->load->view('front/includes/footer');
?>


<script type="text/javascript">

$(function () {
      $("#resetpassword_form").validate({
       rules: {
        
        <?php
        if($user_name=='')
        { ?>
          upassword:{
            required:true,
            minlength:5,
          },
          ure_password: {
            required:true,
            equalTo: "#upassword" ,
            minlength:5,
          },
          <?php  }  ?>
   
    },
    submitHandler: function (form) {
    
      var uid=$('#uid').val();
      var upassword=$('#upassword').val();
        $.ajax({
          method:'post',
          url: "<?php echo base_url();?>front/home/reset_password_update", 
          data:"&uid="+uid+"&upassword="+upassword,
          success: function(result){
            if(result)
            {
              $("#success_message").show();
              window.setTimeout(function() {
                  window.location = "<?php echo base_url();?>";
              }, 1000);
             
          
            }
          }
        });
    }
  });
});

</script>


