<?php
$data['page_title'] = "process list";

$this->load->view('front/includes/header',$data);
// $this->load->view('front/includes/menu');
$this->load->view('front/includes/nav');
?>
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/bs4/dt-1.10.18/datatables.min.css"/>
<div class="container-fluid">
	<div class="row mb-4">



<div class="container-fluid">
	<div class="row mb-4">

		<?php
$this->load->view('front/includes/sidebar');

?>

		<div class="col-12 col-sm-12 col-md-9 col-lg-9 col-xl-9 border">

			<div class="container pt-3">
				<div class="row mb-4">
					<div class="col-6 col-sm-6 col-md-6 col-lg-6 col-xl-6">
						<h1>Process List</h1>
					</div>

					<!-- <div class="col-6 col-sm-6 col-md-6 col-lg-6 col-xl-6">
						<div class="text-right w-100">
							<span class="text-black-50 pr-2" data-toggle="collapse" data-target="#filter-it"><i class="fas fa-sliders-h fa-2x"></i></span>
						</div>
					</div> -->
				</div>

				<!-- <div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
					<div class="bg-piquic full-w" style="min-height: .5px;">
						<div id="filter-it" class="row text-white collapse">
							<div class="col-12 col-sm-12 col-md-11 col-lg-11 col-xl-11 pl-4">
								<div class="row">
									<div class="col-12 col-sm-12 col-md-3 col-lg-3 col-xl-3">
										<select class="custom-select navSlct mt-2 text-white" id="slct_model">
											<option class="text-piquic" selected>Model</option>
											<option class="text-piquic" value="0">Not Selected</option>
											<option class="text-piquic" value="1">Selected</option>
										</select>
									</div>

									<div class="col-12 col-sm-12 col-md-3 col-lg-3 col-xl-3">
										<select class="custom-select navSlct mt-2 text-white" id="slct_pose">
											<option class="text-piquic" selected>Pose</option>
											<option class="text-piquic" value="0">Not Selected</option>
											<option class="text-piquic" value="1">Selected</option>
										</select>
									</div>

									<div class="col-12 col-sm-12 col-md-3 col-lg-3 col-xl-3">
										<select class="custom-select navSlct mt-2 text-white" id="slct_top">
											<option class="text-piquic" selected>Top</option>
											<option class="text-piquic" value="0">Not Selected</option>
											<option class="text-piquic" value="1">Selected</option>
										</select>
									</div>

									<div class="col-12 col-sm-12 col-md-3 col-lg-3 col-xl-3">
										<select class="custom-select navSlct mt-2 text-white" id="slct_btm">
											<option class="text-piquic" selected>Bottom</option>
											<option class="text-piquic" value="0">Not Selected</option>
											<option class="text-piquic" value="1">Selected</option>
										</select>
									</div>

									<div class="col-12 col-sm-12 col-md-3 col-lg-3 col-xl-3">
										<select class="custom-select navSlct mt-2 text-white" id="slct_shoe">
											<option class="text-piquic" selected>Shoes</option>
											<option class="text-piquic" value="0">Not Selected</option>
											<option class="text-piquic" value="1">Selected</option>
										</select>
									</div>

									<div class="col-12 col-sm-12 col-md-3 col-lg-3 col-xl-3">
										<select class="custom-select navSlct mt-2 text-white" id="slct_acc">
											<option class="text-piquic" selected>Accessories</option>
											<option class="text-piquic" value="0">Not Selected</option>
											<option class="text-piquic" value="1">Selected</option>
										</select>
									</div>

									<div class="col-12 col-sm-12 col-md-3 col-lg-3 col-xl-3">
										<select class="custom-select navSlct mt-2 text-white" id="slct_mkup">
											<option class="text-piquic" selected>Makup</option>
											<option class="text-piquic" value="0">Not Selected</option>
											<option class="text-piquic" value="1">Selected</option>
										</select>
									</div>
								</div>
							</div>

							<div class="col-12 col-sm-12 col-md-1 col-lg-1 col-xl-1 pt-3 pl-4">
								<span data-toggle="collapse" data-target="#filter-it"><i class="fas fa-times"></i></span>
							</div>
						</div>
					</div>
				</div> -->

				<div class="row my-4">					
					<div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
						<div class="table-responsive">
							<table id="pro-cess" class="table table-hover border-bottom table-bordered">
								<thead class="thead-light">
									<tr class="text-success">
										<th scope="col" data-orderable="false"></th>
										<th scope="col">SKU</th>
										<th scope="col">Category</th>
										<th scope="col">Stage</th>
										<th scope="col">Status</th>
									</tr>
								</thead>

								<tbody>
									<!-- <tr>
										<th scope="row"><span class="text-info"><i class="fas fa-sync-alt"></i></span></th>
										<td>112398JK389</td>
										<td>Shirt</td>
										<td>Style</td>
										<td>Styling Pending</td>
									</tr>
									<tr>
										<th scope="row"><span class="text-danger"><i class="fas fa-exclamation-circle"></i></span></th>
										<td>56897GT8998</td>
										<td>T-Shirt</td>
										<td>Proccessing</td>
										<td class="text-danger">Warning - 2 images rejected</td>
									</tr>
									<tr>
										<th scope="row"><span class="text-danger"><i class="fas fa-exclamation-circle"></i></span></th>
										<td>56897GT8998</td>
										<td>T-Shirt</td>
										<td>Proccessing</td>
										<td class="text-danger">Warning - Payment Pending</td>
									</tr>

									<tr class="table-danger text-danger">
										<th scope="row"><i class="fas fa-exclamation-circle"></i></th>
										<td>56897GT8998</td>
										<td>T-Shirt</td>
										<td>Proccessing</td>
										<td>Warning - 2 images rejected</td>
									</tr>
									<tr class="table-danger text-danger">
										<th scope="row"><i class="fas fa-exclamation-circle"></i></th>
										<td>56897GT8998</td>
										<td>T-Shirt</td>
										<td>Proccessing</td>
										<td>Warning - Payment Pending</td>
									</tr> -->
								</tbody>
							</table>
						</div>
					</div>

				</div>
			</div>
		</div>
	</div>
</div>
<?php
$this->load->view('front/includes/footer');
?>



<script type="text/javascript" src="https://cdn.datatables.net/v/bs4/dt-1.10.18/datatables.min.js"></script>

<script type="text/javascript">
	$('#pro-cess').DataTable( {
        "pageLength": 10,
		"order": [[ 1, "desc" ]],
		"lengthMenu": [ [10,25, 50, 100], [10,25, 50, 100] ],
		"oLanguage": {
			"sSearch": "Search SKU: "
		},
		"columnDefs": [ {
			"targets": 0,
			"className": 'text-center lead'
		}, {
			"targets": 1,
			"orderable": true,
			"className": 'lead'
		}, {
			"targets": 2,
			"orderable": true,
			"className": 'lead'
		}, {
			"targets": 3,
			"orderable": true,
			"className": 'lead'
		}, {
			"targets": 4,
			"orderable": true,
			"className": 'lead'
		} ],
		"ajax": {
		"url": "<?php echo base_url();?>front/processing/processing_list",
		"type": "GET"
	    },
	     rowCallback: function (row, data) {
	     	
             var firstColumnCheck=$.trim(data[0]);
             console.log(firstColumnCheck);
             if(firstColumnCheck=='<span class="text-danger"><i class="fas fa-exclamation-circle"></i></span>')
             {
             	$(row).addClass('table-danger text-danger');
             }
             else
             {
             	$(row).removeClass('table-danger text-danger');
             }
	     	 

        /*if ( data.cashflow.manual > 0 || data.cashflow.additional_repayment > 0 ) {
            $(row).addClass('fontThick');
        }
        //if it is not the summation row the row should be selectable
        if ( data.cashflow.position !== 'L' ) {
            $(row).addClass('selectRow');
        }*/
    }




    
	} );
</script>