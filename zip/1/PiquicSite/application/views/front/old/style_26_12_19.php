<?php
if ($this->session->userdata('front_logged_in')) {
	$session_data = $this->session->userdata('front_logged_in');
	$user_id = $session_data['user_id'];
	$user_name = $session_data['user_name'];
}
else
{
	$user_id = '';
	$user_name = '';
}

$skuurl = $skuno;
$query = $this->db->get_where('tbl_upload_img', array('zipfile_name' => $skuno,'user_id' => $user_id));
//$query = $this->db->get("tbl_upload_img");
$imgresult=$query->result_array();
//echo "<pre>";
//print_r($query->result_array());
$category=$imgresult[0]['category'];
$gender=$imgresult[0]['gender'];
$brandid =$imgresult[0]['vendor_id'];
$client_name =$imgresult[0]['client_name'];
$second_DB = $this->load->database('another_db', TRUE); 
$skuclient_name=$client_name;

//$image = '';
$brand=$brandid;
$image_codes = '#'; 


// $imgdirname = PATH_CUTOUTPNG. $skuurl .'\\'; 

// $images = glob($imgdirname."*.{png,PNG}",GLOB_BRACE);
// $image = array_shift(array_values($images));
// $arr = explode("\\",$image);
// $arrval = end($arr);
// // exit();
// $image = file_get_contents($imgdirname.$arrval);
// $image_codes = base64_encode($image); 

$image_codes = "upload/".$user_id."/".$skuurl."/cutoutimg.png"; 
if(!file_exists($image_codes))
{
	$image_codes = "";
}
else
{
	$image_codes = base_url()."upload/".$user_id."/".$skuurl."/cutoutimg.png"; 
}




$stylequery = "SELECT * FROM `tbl_style` where skuno ='$skuno' AND user_id='$user_id' ";
$styleres=$this->db->query($stylequery);
$stylerow=$styleres->result_array();
$sty_id = $stylerow[0]['sty_id'];	



/*echo  $skuclquery = "SELECT * FROM `tbl_trun_pic` WHERE  sku_no='$skuurl' ;";
//$skuclresult=mysqli_query($mysqli, $skuclquery);	
//$skuclrow= mysqli_fetch_assoc($skuclresult);

$query=$second_DB->query($skuclquery);
$pautorow=$query->result_array();
$skuclrow = $pautorow[0];*/


//$skuclient_name = $skuclrow['client_name'];
//$brand = $skuclrow['brand'];
//$note = $skuclrow['note'];

// $skuclient_name = $brand;
//$gender = $skuclrow['gender'];
$sku_category = $category;


if($gender=='male'){
	$gender='1';
}else if($gender=='female'){
	$gender='2';
}else if($gender=='boy'){
	$gender='3';
}else if($gender=='girl'){
	$gender='5';
}else if($gender=='ps-male'){
	$gender='1';
}else if($gender=='ps-female'){
	$gender='2';
}else if($gender=='boy78'){
	$gender='4';
}else if($gender=='girl78'){
	$gender='6';
}




$category=strtoupper($category);

if($category=='TOP') {  
	$imgpos='T';
	$clsShow = '';
	
} else if($category=='BOTTOM') { 
	$imgpos='B';
	$clsShow = '';
	
} else if($category=='DRESS') { 
	$imgpos='D';
	$clsShow = '';
	
}else if($category=='BUNG') { 
	$imgpos='BUG';
	$clsShow = '';
	
}else if($category=='TUNG') { 
	$imgpos='TUG';
	$clsShow = '';
	
} else {
	$imgpos = '';
	$clsShow = 'show';
	
}
//echo $imgpos;
//echo $clsShow;

?>

<?php
$data['page_title'] = "style";
$this->load->view('front/includes/header', $data);
// $this->load->view('front/includes/menu');		
$this->load->view('front/includes/nav');
?>

<div class="container-fluid">
	<div class="row">

		<?php
		$this->load->view('front/includes/sidebar');
		?>


		<div class="col-12 col-sm-12 col-md-9 col-lg-9 col-xl-9 border mainscroll">

			<div class="container pt-3">
				<div class="row mb-4">
					<div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
						<h1>Style</h1>
					</div>
				</div>

				<div class="row mb-4">
					
					<div class="col-12 col-sm-12 col-md-6 col-lg-6 col-xl-6">
						<div class="border" style="min-height: 41.5rem;">
							<div class="row">
								<div class="col-4 col-sm-4 col-md-4 col-lg-4 col-xl-4">
									<div class="overflow-auto thumb_scroll" style="max-height: 41.2rem;">
										<div class="card">
											<ul class="list-group list-group-flush skupanel"><!-- Cutout Image comes dynamically via JS-->
												<li class="list-group-item">
													<div class="row">
														<div class="col-9">

															<?php
															$uploaded_image=$imagesinfo[0]['img_name'];
															$cutout_uploaded_image=$imagesinfo[0]['img_name'];
															$zipfile_name=$imagesinfo[0]['zipfile_name'];

															$cutout_uploaded_image="cutoutimg.png";
															?>
															<?php if($image_codes!=''){?>
															<img class="img-thumbnail" src="<?php echo $image_codes;?>">
															<?php } else { ?>
															<img class="img-thumbnail" src="<?php echo PIQUIC_URL;?>images/cutout.png">
															<?php } ?>
															<!-- <img class="img-thumbnail" src="<?php echo base_url();?>temp/cutout-fbtm.png"> -->
															<!-- <img class="img-thumbnail" src="<?php echo base_url();?>temp/cutout-fdrs.png"> -->

															<?php //if($image_codes!=''){?>
																<!-- <img class="img-thumbnail" src="data:image/jpg;charset=utf-8;base64,<?php //echo $image_codes; ?>"> -->
															<?php //} ?>
														</div>
													</div>
												</li>
											</ul>
										</div>

										<div class="card">
											<ul class="list-group list-group-flush posepanel">
												
											</ul>
										</div>

										<div class="card">
											<ul class="list-group list-group-flush toppanel">
												<!-- Top List items comes dynamically via JS-->
											</ul>
										</div>

										<div class="card">
											<ul class="list-group list-group-flush bottompanel">
												<!-- Bottom List items comes dynamically via JS-->
											</ul>
										</div>
										

										<div class="card">
											<ul class="list-group list-group-flush accesspanel">
												
											</ul>
										</div>

										<div class="card">
											<ul class="list-group list-group-flush footpanel">
												<!--Foot List items comes dynamically via JS-->
											</ul>
										</div>
									</div>
								</div>

								<div class="col-8 col-sm-8 col-md-8 col-lg-8 col-xl-8 pt-2 pr-4 pb-2 pl-1">
									<div class="card border-0">

										<?php 




										$session_data = $this->session->userdata('front_logged_in');
										$user_id = $session_data['user_id'];



										$query16= "SELECT * FROM tbl_users WHERE user_id = '$user_id'";
										$query=$this->db->query($query16);
										$capvalue16=$query->result_array();
										$background_id_select = $capvalue16[0]['background_id'];
										$image_size_id_select = $capvalue16[0]['image_size_id'];


										if($background_id_select!='' && $background_id_select!='0')
										{
											$query16= "SELECT * FROM tbl_background WHERE back_id = '$background_id_select'";
											$query=$second_DB->query($query16);
											$capvalue16=$query->result_array();
											$back_id = $capvalue16[0]['back_id'];
											$back_img = $capvalue16[0]['back_img'];
											$backimg = $back_img;	
										}
										else
										{
											$back_id = '';
											$back_img = '';
											$backimg = 'bg-white.jpg';	
										}


										?>



										<div id="bg" class="row">
											<div class="col-md-12">
												<!-- <img id="bg-big" class="img-fluid" src="#>"> -->
												<!-- <img id="bg-big" class="img-fluid" src="<?php echo base_url();?>website-assets/images/bg/1399244370.jpg"> -->

												<img id="bg-big" class="img-fluid" src="<?php echo PIQUIC_URL;?>/images/bg/<?php echo $backimg;?>">
											</div>
										</div>

										<div id="shdw" class="row">
											
										</div>

										<div id="pose" class="row">
											<div class="col-md-12">
												<img id="pose-big" class="collapse img-fluid" src="#">
												<!-- <img id="pose-big" class="img-fluid" src="<?php echo base_url();?>website-assets/images/model_pose/KLAD-MSTR.png"> -->
											</div>
										</div>

										<?php /*<div id="top" class="row">
											<div class="col-md-12"> 
												<?php if($imgpos=='T' || $imgpos=='TUG'){
													if($image_codes!=''){?>
														<img class="img-fluid" src="data:image/jpg;charset=utf-8;base64,<?php echo $image_codes; ?>">
													<?php }

												}else{?>                        
													<img id="top-big" class="collapse <?php echo $clsShow; ?> img-fluid" src="images/cutout.png">
												<?php }  ?>

											</div>
										</div>

										<div id="bottom" class="row">
											<div class="col-md-12">
												<?php if($imgpos=='B' || $imgpos=='BUG'){
													if($image_codes!=''){?>
														<img class="img-fluid" src="data:image/jpg;charset=utf-8;base64,<?php echo $image_codes; ?>">
													<?php }

												}else{?>
													<img id="bottom-big" class="collapse <?php echo $clsShow; ?> img-fluid" src="images/cutout.png">

												<?php }  ?>

											</div>
										</div>

										<div id="dress" class="row">
											<div class="col-md-12">
												<?php if($imgpos=='D'){
													if($image_codes!=''){?>
														<img class="img-fluid" src="data:image/jpg;charset=utf-8;base64,<?php echo $image_codes; ?>">
													<?php }

												}else{?>
													<img id="dress-big" class="collapse <?php echo $clsShow; ?> img-fluid" src="images/cutout.png">

												<?php }  ?>

											</div>
										</div> */?>


										<div id="top" class="row">
											<div class="col-md-12"> 
												<?php if($imgpos=='T' || $imgpos=='TUG'){
													if($image_codes!=''){?>
														<!-- <div class="demo" id="resizable"><img class="img-fluid"  src="<?php echo $image_codes; ?>"></div> -->


													<div id="reswrapper">
													<div class="demo" id="resizable"><img class="img-fluid"  src="<?php echo $image_codes; ?>"></div>
													</div>
													<?php }
													else {?>                        
													<img id="top-big" class="img-fluid" src="<?php echo PIQUIC_URL;?>images/cutout.png">
												<?php }

												}else{?>                        
													<img id="top-big" class="collapse <?php echo $clsShow; ?> img-fluid" src="<?php echo PIQUIC_URL;?>images/cutout.png">
												<?php }  ?>

											</div>
										</div>

										<div id="bottom" class="row">
											<div class="col-md-12">
												<?php if($imgpos=='B' || $imgpos=='BUG'){
													if($image_codes!=''){?>
														<div id="reswrapper">
														<div class="demo" id="resizable"><img class="img-fluid" src="<?php echo $image_codes; ?>"></div>
													</div>
													<?php }
													else {?>                        
													<img id="bottom-big" class="img-fluid" src="<?php echo PIQUIC_URL;?>images/cutout.png">
												<?php }

												}else{?>
													<img id="bottom-big" class="collapse <?php echo $clsShow; ?> img-fluid" src="<?php echo PIQUIC_URL;?>images/cutout.png">

												<?php }  ?>

											</div>
										</div>

										<div id="dress" class="row">
											<div class="col-md-12">
												<?php if($imgpos=='D'){
													if($image_codes!=''){?>
														<div id="reswrapper">
														<div class="demo" id="resizable"><img class="img-fluid" src="<?php echo $image_codes; ?>"></div>
													</div>
													<?php }
													else {?>                        
													<img id="dress-big" class="img-fluid" src="<?php echo PIQUIC_URL;?>images/cutout.png">
												<?php }

												}else{?>
													<img id="dress-big" class="collapse <?php echo $clsShow; ?> img-fluid" src="<?php echo PIQUIC_URL;?>images/cutout.png">

												<?php }  ?>

											</div>
										</div>

										
										<!-- <?php if($image_codes!=''){?> -->
										<div id="access"  class="row accbg d-none" style="background: url(<?php echo PIQUIC_URL;?>/images/bg/<?php echo $backimg;?>);"></div>
										<!-- <?php } ?> -->

										<div id="accrn" class="row">
											<img id="accrn-big" class="collapse img-fluid" src="#">
										</div>

										<div id="accer" class="row">
											<div class="col-md-12">
												<img id="accer-big" class="collapse img-fluid" src="#">
											</div>
										</div>

										<div id="accnc" class="row">
											<div class="col-md-12">
												<img id="accnc-big" class="collapse img-fluid " src="#">
											</div>
										</div>

										<div id="accbr" class="row">
											<div class="col-md-12">
												<img id="accbr-big" class="collapse img-fluid " src="#">
											</div>
										</div>

										<div id="foot" class="row">
											<div class="col-md-12">
												<img id="foot-big" class="collapse img-fluid " src="#">
											</div>
										</div>

									</div>
									<?php
									if($image_codes!='')
									{
									?>

									<div class="img_tool_box">


									<input type="text" name="source_x" id="source_x" value="" />
									<input type="text" name="source_y" id="source_y" value="" />

									<input type="text" name="source_w" id="source_w" value="" />
									<input type="text" name="source_h" id="source_h" value="" />

									<input type="text" name="source_angle" id="source_angle" value="" />

									

										<div class="d-flex justify-content-around">
											<span class="btn btn-piquic" onclick="openImgRot('click')" id="openImgRot" alt='0'><i class="fas fa-undo"></i><br>Rotate</span>
											<input type="text" name="openImgRot_ctn" id="openImgRot_ctn" value="0" />


											<span class="btn btn-piquic"   onclick="openImgDrg('click')"  id="openImgDrg"  alt='0'><i class="fas fa-expand-alt" style="transform: rotate(90deg);"></i><br>Move</span>
    										<input type="text" name="openImgDrg_ctn" id="openImgDrg_ctn" value="0" />

    										<span class="btn btn-piquic"   onclick="openImgRes('click')"  id="openImgRes"  alt='0'><i class="fas fa-expand-alt" style="transform: rotate(90deg);"></i><br>Resize</span>
    										<input type="text" name="openImgRes_ctn" id="openImgRes_ctn" value="0" />
										</div>
									</div>

									

									<?php
									}
									?>

								</div>
							</div>
						</div>
					</div>

					<div class="col-12 col-sm-12 col-md-6 col-lg-6 col-xl-6">
						<div class="border" style="min-height: 35.5rem;">
							<nav id="select-style">
								<div class="nav nav-tabs nav-justified" id="nav-tab" role="tablist">

									<a class="nav-item nav-link active" id="nav-model-tab" data-toggle="tab" href="#nav-model" role="tab" aria-controls="nav-model" aria-selected="false">Model</a>

									<a class="nav-item nav-link disabled" id="nav-pose-tab" data-toggle="tab" href="#nav-pose" role="tab" aria-controls="nav-pose" aria-selected="false">Pose</a>

									<a class="nav-item nav-link disabled <?php if($imgpos=='T' || $imgpos=='D' || $imgpos=='TUG'){ ?>d-none<?php } ?>" id="nav-top-tab" data-toggle="tab" href="#nav-top" role="tab" aria-controls="nav-top" aria-selected="false">Top</a>

									<a class="nav-item nav-link disabled <?php if($imgpos=='B' || $imgpos=='D' || $imgpos=='BUG'){ ?>d-none<?php } ?>" id="nav-bottom-tab" data-toggle="tab" href="#nav-bottom" role="tab" aria-controls="nav-bottom" aria-selected="false">Bottom</a>

									<a class="nav-item nav-link disabled" id="nav-shoes-tab" data-toggle="tab" href="#nav-shoes" role="tab" aria-controls="nav-shoes" aria-selected="false">Shoes</a>

									<a class="nav-item nav-link disabled" id="nav-accessories-tab" data-toggle="tab" href="#nav-accessories" role="tab" aria-controls="nav-accessories" aria-selected="false">Accessories</a>

									<a class="nav-item nav-link disabled <?php if($gender=='1' || $gender=='3'|| $gender=='4' || $imgpos=='TUG' || $imgpos=='BUG'){ ?>d-none<?php } ?>" id="nav-makeup-tab" data-toggle="tab" href="#nav-makeup" role="tab" aria-controls="nav-makeup" aria-selected="false">MakeUp</a>

								</div>
							</nav>

							<div class="tab-content" id="nav-tabContent">
								<div class="tab-pane fade show active" id="nav-model" role="tabpanel" aria-labelledby="nav-model-tab"><!--  model start  -->
									<div class="container-fluid scroll-box">
										<div class="row pt-3 pb-3">
											<div class="col-12 col-sm-12 col-md-6 col-lg-6 col-xl-6">
												<div class="form-row">
													<div class="form-group col-3 col-md-3 ">
														<img id="on-model" class="w-100 d-none" src="<?php echo PIQUIC_URL;?>images/icon/toggle-on.png" >
														<img id="off-model" class="w-100" src="<?php echo PIQUIC_URL;?>images/icon/toggle-off.png">
													</div>
													<div class="form-group col-9 col-md-9 pt-2">
														<label id="auto-pose" >
															Auto Select Model  
														</label>
													</div>
												</div>
											</div>
											<div class="col-12 col-sm-12 col-md-6 col-lg-6 col-xl-6">
												<select name="gender" id="generID" class="form-control" <?php if($gender!=''){echo 'disabled';}?>>
													<option value="0" >Select Gender</option>
													<?php  
													$cquery1 = "SELECT * FROM `tbl_gender` where status='1' ORDER by `gen_id`;";
													$query=$this->db->query($cquery1);
													$result=$query->result_array();
													foreach($result as $key => $row){
														$gen_id=$row['gen_id'];
														?>
														<option value="<?php echo $row['gen_id'];?>" <?php if($gender==$gen_id){ echo'selected';}?>><?php echo $row['gen_name'];?></option>
													<?php } ?>
												</select>
											</div>
										</div>

										<div class="row" id="modelandangles">
											<?php										
											$pautoquery = "SELECT * FROM `tbl_autopose` WHERE client_name='$client_name' ";
											$query=$this->db->query($pautoquery);
											$pautorow=$query->result_array();
											if(!empty($pautorow))
											{
												$model_id = $pautorow[0]['model_id'];
											}
											else
											{
												$model_id = '';
											}


											if($model_id!=''){
												$where_arr=' AND  m_id in('.$model_id.') AND gen_id in ('.$gender.')   ';
											}
											else
											{
												$where_arr=' AND gen_id in ('.$gender.')  ';
											}
											$pautoquery = "SELECT * FROM `tbl_model` WHERE 1=1 ".$where_arr."  AND status='1' ORDER by `m_id` ";                                      
											$query2=$second_DB->query($pautoquery);										
											$modelinfo= $query2->result_array();
											foreach($modelinfo as $mrow){
												$mid = $mrow['m_id'];
												$mod_name = $mrow['mod_name'];
												$mod_name =strtoupper($mod_name);

												$yposequery = "SELECT * FROM `tbl_model_pose` where m_id='$mid' AND pose_status='1' AND  status='1' ;";								
												$query=$second_DB->query($yposequery);
												$ymrows=$query->result_array();
												$ymrow=$ymrows[0];	
												?>

												<div class="col-6 col-sm-6 col-md-4 col-lg-4 col-xl-4 mb-3" onclick="poseimg(<?php echo $ymrow['mpose_id'];?>,'<?php echo PIQUIC_URL;?>/images/model_pose/<?php echo $ymrow['mpose_img'];?>',<?php echo $mrow['m_id'];?>);changeimg('<?php echo $skuurl;?>','<?php echo $mod_name;?>'); shadowimg('<?php echo $mrow['m_id'];?>','<?php echo $gender;?>','<?php echo  $brandid;?>');">
													<div class="border" style="background: url(<?php echo PIQUIC_URL;?>/images/model_image/<?php echo $mrow['mod_img'];?>) no-repeat center center; background-size: contain; height: 10rem;"></div>

													<span style="font-size: .74rem; text-transform: uppercase;"><?php echo $mrow['mod_name'];?></span>

													<div class="custom-control custom-radio c-box">
														<input type="radio" class="custom-control-input" name="model" value="<?php echo $mrow['m_id'];?>" id="modelID<?php echo $mrow['m_id'];?>" onclick="poseimg(<?php echo $ymrow['mpose_id'];?>,'<?php echo PIQUIC_URL;?>/images/model_pose/<?php echo $ymrow['mpose_img'];?>',<?php echo $mrow['m_id'];?>); changeimg('<?php echo $skuurl;?>','<?php echo $mod_name;?>'); shadowimg('<?php echo $mrow['m_id'];?>','<?php echo $gender;?>','<?php echo  $brandid;?>');">

														<label class="custom-control-label" for="modelID<?php echo $mrow['m_id'];?>" >&nbsp;</label>
													</div>
												</div>
												<?php
											}

											?>
										</div>
									</div>
								</div><!--  model end  -->

								<div class="tab-pane fade" id="nav-pose" role="tabpanel" aria-labelledby="nav-pose-tab"><!--  pose display start  -->
									<input class="w-100" type="hidden"  id="my_pose_id"  name="my_pose_id[]"/>
									<div class="container-fluid scroll-box" id="modelposeandangles">

									</div>
								</div><!--  pose end  -->

								<div class="tab-pane fade" id="nav-top" role="tabpanel" aria-labelledby="nav-top-tab"><!--  Top display start  -->
									<div class="container-fluid scroll-box" id="topfilter">
										<!-- <div class="row pt-3 pb-3">
											<div class="col-6 col-md-6">
												<div class="custom-control custom-checkbox">
													<input type="checkbox" class="custom-control-input" id="showExtraTop" checked disabled>
													<label class="custom-control-label" for="showExtraTop">Show Extras</label>
												</div>
											</div>

											<div class="col-6 col-md-6">
												<select name="topWear" id="topWear" class="form-control" disabled>
													<option value="0" selected >All Top</option>
												</select>
											</div>
										</div>
										<div class="row" id="extratop"></div> -->
									</div>
								</div><!--  Top end  -->

								<!-- Modal -->
								<!-- <div class="modal fade" id="modelTop" tabindex="-1" role="dialog" aria-labelledby="modelTopTitle" aria-hidden="true">
									<div class="modal-dialog modal-dialog-centered" role="document">
										<div class="modal-content">
											<div class="modal-header" style="background-color: #76aea1 !important;">
												<h5 class="modal-title text-white" id="modelTopTitle">Warning</h5>
											</div>

											<div class="modal-body">
												<p class="lead">Using <b>"Show Extras"</b> option will display bottoms which have a limited choice of poses and model.</p>
											</div>

											<div class="modal-footer" style="justify-content: space-between!important">
												<div class="custom-control custom-checkbox">
													<input type="checkbox" class="custom-control-input" name="chkTop" id="chkTop" >
													<label class="custom-control-label" for="chkTop">Don't show again</label>
												</div>
												<button type="button" data-dismiss="modal" aria-label="Close" class="btn btn-sm btn-piquic" id="gotitTop">Got it</button>
											</div>
										</div>
									</div>
								</div> -->

								<div class="modal fade" id="err_msg_sty" tabindex="-1" role="dialog" aria-hidden="true">
									<div class="modal-dialog" role="document">
										<div class="modal-content">
											<div class="modal-header">
												<h5 class="modal-title text-danger" id="errLabelSty"></h5>
												<button type="button" class="close text-danger" data-dismiss="modal" aria-label="Close">
													<span aria-hidden="true" >&times;</span>
												</button>
											</div>
										</div>
									</div>
								</div>

								<div class="tab-pane fade" id="nav-bottom" role="tabpanel" aria-labelledby="nav-bottom-tab"><!--  bottom display start  -->
									<div class="container-fluid scroll-box" id="bottomfilter">
										<!-- <div class="row pt-3 pb-3">
											<div class="col-6 col-md-6">
												<div class="custom-control custom-checkbox">
													<input type="checkbox" class="custom-control-input" id="showExtraBottom" checked disabled>
													<label class="custom-control-label" for="showExtraBottom">Show Extras</label>
												</div>
											</div>

											<div class="col-6 col-md-6">
												<select name="bottomWear" id="bottomWear" class="form-control" disabled>
													<option value="0" selected>All Bottom</option>
												</select>
											</div>
										</div>
										<div class="row" id="extrabtm"></div> -->
									</div>
								</div><!--  bottom end  -->

								<!-- Modal -->
								<div class="modal fade" id="modelBtm" tabindex="-1" role="dialog" aria-labelledby="modelBtmTitle" aria-hidden="true">
									<div class="modal-dialog modal-dialog-centered" role="document">
										<div class="modal-content">
											<div class="modal-header" style="background-color: #76aea1 !important;">
												<h5 class="modal-title text-white" id="modelBtmTitle">Warning</h5>
											</div>

											<div class="modal-body">
												<p class="lead">Using <b>"Show Extras"</b> option will display bottoms which have a limited choice of poses and model.</p>
											</div>

											<div class="modal-footer" style="justify-content: space-between!important">
												<div class="custom-control custom-checkbox">
													<input type="checkbox" class="custom-control-input" name="chkBtm" id="chkBtm" >
													<label class="custom-control-label" for="chkBtm">Don't show again</label>
												</div>
												<button type="button" data-dismiss="modal" aria-label="Close" class="btn btn-sm btn-piquic" id="gotitBtm">Got it</button>
											</div>
										</div>
									</div>
								</div>

								<div class="tab-pane fade" id="nav-shoes" role="tabpanel" aria-labelledby="nav-shoes-tab"><!--  shoe display start  -->
									<div class="container-fluid scroll-box" id="footfilter">
									</div>
								</div><!--  shoe end  -->

								<div class="tab-pane fade" id="nav-accessories" role="tabpanel" aria-labelledby="nav-accessories-tab"><!--  accessories display start  -->
									<input class="w-100" type="hidden"  id="my_access_id"  name="my_access_id[]"/>
									<div class="container-fluid scroll-box" id="accessoriesfilter">
									</div>
								</div><!--  accessories end  -->

								<div class="tab-pane fade" id="nav-makeup" role="tabpanel" aria-labelledby="nav-makeup-tab"><!--  make-up display Start  -->
									<?php //include('style_make_up.php'); ?>
								</div><!--  make-up end  -->
							</div>
						</div>
					</div>
				</div>

				<div class="row mb-3">
					<div class="col-6 col-sm-6 col-md-3 col-lg-3 col-xl-3">&nbsp;</div>				

					<div class="col-6 col-sm-6 col-md-3 col-lg-3 col-xl-3">
						<button class="btn btn-piquic btn-block m-1" id="savePsd">Save</button>
					</div>

					<div class="col-6 col-sm-6 col-md-3 col-lg-3 col-xl-3">
						<button class="btn btn-outline-piquic btn-block m-1" id="cancel">Cancel</button>
					</div>

					<div class="col-6 col-sm-6 col-md-3 col-lg-3 col-xl-3">&nbsp;</div>

				</div>

				<?php 
				$sorted_tag='';


		 /*$query=$this->db->query("select * from  tbl_upload_images where upimg_id='$id'");   
             foreach ($query->result() as $resrow)
             { 
             $img=$resrow->img_name;
             $imgid=$resrow->id;*/

             $company = "SELECT * FROM `tbl_upload_img` WHERE `zipfile_name` ='".$skuurl."' and user_id='$user_id' ";
		 //$query=$this->db->query("select * from  tbl_upload_images where upimg_id='$id'");   
		/*$comp_res=mysqli_query($mysqli, $company);
		$cname= mysqli_fetch_assoc($comp_res);
		$client_name= $cname['client_name'];*/

		$query=$this->db->query($company);
		$ymrows=$query->result_array();
		$cname=$ymrows[0];
		$client_name= $cname['client_name'];

		$temp_client = explode(" ", $client_name);
		$client_name = $temp_client[0];

		//$imgdirname = '\\\\172.25.0.86\\e\\Piquic Dropbox\\Piquic@'. $client_name .'\\'. $skuurl .'\\'; 
		//$imgdirname = 'D:\\172.25.0.86\\Piquic Dropbox\\Piquic@'. $client_name .'\\'. $skuurl .'\\'; 
		//$imgdirname = 'E:\\xampp\\htdocs\\piquic_client_new\\upload\\'. $user_id .'\\'. $skuurl .'\\';
		$imgdirname = 'upload/'. $user_id .'/'. $skuurl .'/';  
		$images = glob($imgdirname."*.{jpg,JPG}", GLOB_BRACE);

		//echo "<pre>";
		//print_r($images); 
		// $images = glob($imgdirname."*.JPG");
		foreach ($images as $file){
			if(strpos($file,'Tag') !== false){							  
				$sorted_tag=$file;
			} 
		}		 

		?>
		<!-- <p id="tagImgBtn" class="btn btn-success tagbtn lead" data-toggle="modal" data-target="#tagModal" onclick="loadTagImage('<?php echo $skuurl; ?>')">Tag Image</p>

			<div class="modal fade" id="tagModal" tabindex="-1" role="dialog" aria-labelledby="tagModalLabel" aria-hidden="true">
				<div class="modal-dialog" role="document" style="max-width: 333px !important">
					<div class="modal-content">
						<div class="modal-header">
							<h5 class="modal-title" id="tagModalLabel">Tag Image</h5>
							<button type="button" class="close" data-dismiss="modal" aria-label="Close">
								<span aria-hidden="true">&times;</span>
							</button>
						</div>
						<div class="modal-body">
							<div class="row" id="imgDiv">
							</div>
						</div>
					</div>
				</div>
			</div> -->

			<input  type="hidden" name="imgtag" id="imgtag" value="<?php echo $sorted_tag;?>"/>
			<input  type="hidden" name="skuid" id="skuid" value="<?php echo $skuurl;?>"/>

		</div>
	</div>
</div>
</div>

<?php
$this->load->view('front/includes/footer');
?>

<link rel="stylesheet" type="text/css" href="//ajax.googleapis.com/ajax/libs/jqueryui/1.7.1/themes/base/jquery-ui.css"/>

<script src="<?php echo base_url();?>website-assets/dragResizeRotatableJs/jquery.ui.rotatable.js"></script>
<link rel="stylesheet" href="<?php echo base_url();?>website-assets/dragResizeRotatableJs/jquery.ui.rotatable.css">

<script type="text/javascript">
//$(document).ready(function() {
//alert("xxx");


  function openImgRot(type){

  	if($("#source_angle").val()!='')
		{
		  var degrees=$("#source_angle").val(); 
		}
		else
		{
		 var degrees= false;
		}

  	var openImgRot_ctn=$("#openImgRot_ctn").val();

  	if(type=='load')
	   {
	   	if(openImgRot_ctn=='0')
	   	{
	   		openImgRot_ctn='1';
	   	}
	   	else
	   	{
	   		openImgRot_ctn='0';
	   	}
	   }

  	//alert(openImgRot_ctn);

    if(openImgRot_ctn=='0')
    {
		$('#resizable').rotatable({

		degrees: degrees,   
		stop: function(event, ui){
		if(ui.angle.current<0){
		var given_angle=ui.angle.current+2*Math.PI;
		}
		else{ var given_angle=ui.angle.current;  }
		var new_angle=Math.round(given_angle*180/ Math.PI)+"deg";
		$("#source_angle").val(Math.round(given_angle*180/ Math.PI));
		$("#resizable").css("transform","rotate("+new_angle+")");
		} 


		});
	  
	   if(type=='click')
	   {
	   	$("#openImgRot_ctn").val('1');
	   }
	    
       
    }
    else
    {
         
        if(type=='click')
	   { 
	   	$("#resizable").rotatable('destroy').removeClass("rotate"); 
 		 $("#openImgRot_ctn").val('0');
 		}
    }

   
  }


    function openImgDrg(type){


    if($("#source_x").val()!='' && $("#source_y").val()!='' )
		{
		  var sourceX = $("#source_x").val();
	      var sourceY = $("#source_y").val(); 
		}
		else
		{
		  var sourceX = 0;
	      var sourceY = 0; 
		}


  	var openImgDrg_ctn=$("#openImgDrg_ctn").val();

  	  	if(type=='load')
	   {
	   	if(openImgDrg_ctn=='0')
	   	{
	   		openImgDrg_ctn='1';
	   	}
	   	else
	   	{
	   		openImgDrg_ctn='0';
	   	}
	   }

  	//alert(alt);

    if(openImgDrg_ctn=='0')
    {
    	$("#resizable").draggable({
		create: function() {
		if(sourceX!='0' || sourceY!='0')
		{
		$("#resizable").css("left", sourceX+'px');
		$("#resizable").css("top", sourceY+'px');
		}
		
		},
		stop:function(event,ui) {
		var wrapper = $("#reswrapper").offset();
		var borderLeft = parseInt($("#reswrapper").css("border-left-width"),10);
		var borderTop = parseInt($("#reswrapper").css("border-top-width"),10);
		var pos = ui.helper.offset();
		$("#source_x").val(pos.left - wrapper.left - borderLeft);
		$("#source_y").val(pos.top - wrapper.top - borderTop);
		//alert($("#source_x").val() + "," + $("#source_y").val());

		//var top = $("#resizable").position().top;
		//var left = $("#resizable").position().left;

		//$("#source_x").val(top);
		//$("#source_y").val(left);
		}
		});




        if(type=='click')
	   {
        $("#openImgDrg_ctn").val('1');
       }
    }
    else
    {

    	

    	if(type=='click')
	   {
	   	$("#resizable").draggable('destroy').removeClass("drag");
    	$("#openImgDrg_ctn").val('0');
       }
    }

  }




  function openImgRes(type){

  	if($("#source_w").val()!='' && $("#source_h").val()!='' )
		{
		  var startW = $("#source_w").val();
	      var startH = $("#source_h").val(); 
		}
		else
		{
		  var startW = 0;
	      var startH = 0; 
		}
    

  	var openImgRes_ctn=$("#openImgRes_ctn").val();

  	  	if(type=='load')
	   {
	   	if(openImgRes_ctn=='0')
	   	{
	   		openImgRes_ctn='1';
	   	}
	   	else
	   	{
	   		openImgRes_ctn='0';
	   	}
	   }

  	//alert(alt);

    if(openImgRes_ctn=='0')
    {



		$("#resizable").resizable({
   	 
		create: function() {

		if(startW!='0' || startH!='0')
		{
		$("#resizable").css("width", startW+'px');
		$("#resizable").css("height", startH+'px');
		}

		
		},  
    //Other options
	    start : function(event,ui) {
	    	if($("#source_w").val()!='' && $("#source_h").val()!='' )
	    	{
	    	startW = $("#source_w").val();
	        startH = $("#source_h").val();
	    	}
	    	else
	    	{
	    	startW = $("#reswrapper").outerWidth();
	        startH = $("#reswrapper").outerHeight();
	    	}
	        
	    },
	    stop : function(event,ui) {
	        endW = $("#reswrapper").outerWidth();
	        endH = $("#reswrapper").outerHeight();

			/*var width = $(event.target).width();
			var height = $(event.target).height();*/

	        $("#source_w").val(endW);
			$("#source_h").val(endH);
	        //alert("width changed:"+ (endW-startW) + " -- Height changed:" + (endH-endW));
	    }
	});


			if(type=='click')
			{
			$("#openImgRes_ctn").val('1');
			}
    }
    else
    {

    	
    	
		if(type=='click')
		{
		$("#resizable").resizable('destroy').removeClass("resize");
		$("#openImgRes_ctn").val('0');
		}
    }

  }






//alert("aaaaa");

  //$('#resizable').draggable().resizable().rotatable();
   /*function openImgRot()
   {
   	$('#resizable').rotatable();
   }
  
   function openImgResDrg()
   {
   	 $('#resizable').draggable().resizable();
   }*/
 /*.rotatable({
    //Other options
	    start : function(event,ui) {
	    	
	        console.log("start");
	        console.log(this);
	    },
	    stop : function(event,ui) {
	    	console.log("stop");
	        console.log(this);
	        
	    }
	})*/
	

/*	  if($("#source_angle").val()!='')
		{
		  var degrees=$("#source_angle").val(); 
		}
		else
		{
		 var degrees= false;
		}


		if($("#source_w").val()!='' && $("#source_h").val()!='' )
		{
		  var startW = $("#source_w").val();
	      var startH = $("#source_h").val(); 
		}
		else
		{
		  var startW = 0;
	      var startH = 0; 
		}

		alert(startW);

   $('#resizable')
   .rotatable({
		
		degrees: degrees,   
		stop: function(event, ui){
             if(ui.angle.current<0){
                   var given_angle=ui.angle.current+2*Math.PI;
                }
                else{ var given_angle=ui.angle.current;  }
                var new_angle=Math.round(given_angle*180/ Math.PI)+"deg";
                $("#source_angle").val(Math.round(given_angle*180/ Math.PI));
                $("#resizable").css("transform","rotate("+new_angle+")");
        } 

	 
	})
   .resizable({
   	 
		
    //Other options
	    start : function(event,ui) {
	    	if($("#source_w").val()!='' && $("#source_h").val()!='' )
	    	{
	    	startW = $("#source_w").val();
	        startH = $("#source_h").val();
	    	}
	    	else
	    	{
	    	startW = $(this).outerWidth();
	        startH = $(this).outerHeight();
	    	}
	        
	    },
	    stop : function(event,ui) {
	        endW = $(this).outerWidth();
	        endH = $(this).outerHeight();
	        $("#source_w").val((endW-startW));
			$("#source_h").val((endH-endW));
	        //alert("width changed:"+ (endW-startW) + " -- Height changed:" + (endH-endW));
	    }
	})
   .draggable({
   	     
		stop:function(event,ui) {
		var wrapper = $("#reswrapper").offset();
		var borderLeft = parseInt($("#reswrapper").css("border-left-width"),10);
		var borderTop = parseInt($("#reswrapper").css("border-top-width"),10);
		var pos = ui.helper.offset();
		$("#source_x").val(pos.left - wrapper.left - borderLeft);
		$("#source_y").val(pos.top - wrapper.top - borderTop);
		//alert($("#source_x").val() + "," + $("#source_y").val());
		}
		})
   ;
*/
     //$('#resizable').resizable();
 


//});
</script>


<script type="text/javascript">
	window.load=loadFunction();

	function loadFunction()
	{

		<?php

		$query = $this->db->get_where('tbl_style', array('skuno' => $skuno,'user_id' => $user_id));
		$imgresult=$query->result_array();
		$model_id=$imgresult[0]['model_id'];

		if($model_id!='' && $model_id!='0')
		{


			$pautoquery = "SELECT * FROM `tbl_model` WHERE 1=1  AND m_id='$model_id'  ";                                      
			$query2=$second_DB->query($pautoquery);										
			$modelinfo= $query2->result_array();
			$mrow=$modelinfo[0];
			$mid = $mrow['m_id'];
			$mod_name = $mrow['mod_name'];
			$mod_name =strtoupper($mod_name);

			$yposequery = "SELECT * FROM `tbl_model_pose` where m_id='$mid' AND pose_status='1' AND  status='1' ;";								
			$query=$second_DB->query($yposequery);
			$ymrows=$query->result_array();
			$ymrow=$ymrows[0];


			?>
               //alert('aaaaaaa');
               poseimg(<?php echo $ymrow['mpose_id'];?>,'<?php echo PIQUIC_URL;?>/images/model_pose/<?php echo $ymrow['mpose_img'];?>',<?php echo $mrow['m_id'];?>);
               changeimg('<?php echo $skuurl;?>','<?php echo $mod_name;?>'); 
               shadowimg('<?php echo $mrow['m_id'];?>','<?php echo $gender;?>','<?php echo  $brandid;?>');
               <?php

            }
            ?>

         }


         function setBackground(user_id,back_id) {

         	$.ajax({
         		type:'POST',
         		url:'<?php echo PIQUIC_AJAX_URL ;?>/set_background.php',
         		data:{'user_id':user_id,'back_id':back_id},
         		success: function(shdwcheckresults) {
				//alert(shdwcheckresults);

				$(".chk_bg_class").addClass("d-none");
				$("#chk_bg_"+back_id).removeClass("d-none");


				$(".bg_image_class").removeClass("border-piquic");
				$("#bg_image_"+back_id).addClass("border-piquic");


				$('#bg-big').attr('src',$.trim(shdwcheckresults));
    			//$('#access').attr('src',$.trim(shdwcheckresults));
    			$('#access').attr("style","background:url("+shdwcheckresults+");");


				 //$("#shdw").html(shdwcheckresults);
				}
			});
         }




         function setImageSize(user_id,image_size_id) {
         	$.ajax({
         		type:'POST',
         		url:'<?php echo PIQUIC_AJAX_URL ;?>/set_imagesize.php',
         		data:{'user_id':user_id,'image_size_id':image_size_id},
         		success: function(shdwcheckresults) {
				//alert(shdwcheckresults);
				$(".chk_imgsize_class").addClass("d-none");
				$("#chk_imgsize_"+image_size_id).removeClass("d-none");
				// $("#shdw").html(shdwcheckresults);

				$(".imgsize_class").removeClass("border-piquic");
				$("#imgsize_"+image_size_id).addClass("border-piquic");

			}
		});
         }




         function poseimg(n,img,m) {
         	$("#savePsd").attr("disabled", true);                
         	//$("div#divLoading").addClass('show');

         	$("#loader").show();

         	if ($('#pose-big').hasClass("show")){
         		$('#pose-big').attr('src',''+ img +'');
         		$('#pose_thumb_'+ n +'').attr('data-target', '');
         	} else {
         		$('#pose-big').addClass("show");
         		$('#pose-big').attr('src',''+ img +'');
         		$('#pose_thumb_'+ n +'').attr('data-target', '');
         	}

         	$('.posepanel').empty();
         	$('.toppanel').empty();
         	$('.bottompanel').empty();
         	$('.footpanel').empty();

         	$('#top-big').attr('src','');
         	$('#dress-big').attr('src','');
         	$('#bottom-big').attr('src','');
         	$('#foot-big').attr('src','');
         	$('#modelID'+ m + '').prop('checked', true);	
         	var lfckv = document.getElementById('modelID'+ m + '').checked;
         	if(lfckv==true){	
         		var mid=m;
         		var skuid =$('#skuid').val();

     // alert(mid);
     //   alert(skuid);
     $("#nav-pose-tab").removeClass('disabled');
				// $("#nav-makeup-tab").removeClass('disabled');

				$("#nav-top-tab").addClass('disabled');
				$("#nav-bottom-tab").addClass('disabled');
				$("#nav-shoes-tab").addClass('disabled');
				$("#nav-accessories-tab").addClass('disabled');



				$.ajax({
					type:'POST',
					url:'<?php echo PIQUIC_AJAX_URL ;?>/styleajaxpose.php', 
					data: {'mid':mid,'skuid':skuid},
					success: function(poseres){	
						// alert(poseres);
						
						$("#modelposeandangles").html(poseres);

						//$("div#divLoading").removeClass('show');
						$("#loader").hide(); 
						var noOfImages = $("img").length;
						console.log(noOfImages);
						var noLoaded = 0;

						$("img").on('load', function(){ 
							console.log(this);
							//$(this).css('background','none');
							$(this).removeClass('loading');
						});
						

					}
				});

				<?php  if($gender=='2' || $gender=='5'|| $gender=='6'){ ?>
					var client_name = '<?php echo $skuclient_name; ?>';

					$.ajax({
						type:'POST',			
						url:'<?php echo PIQUIC_AJAX_URL ;?>/style_make_up.php', 
						data: {'mid':mid,'client_name':client_name},
						success: function(makesecres){							
							if(makesecres!=0)
							{
								$("#nav-makeup-tab").removeClass('disabled');
								$("#nav-makeup").html(makesecres);
							}
							if(makesecres==0)
							{
								$("#nav-makeup-tab").addClass('disabled');
							}

							$.ajax({
								type:'POST',
								url:'<?php echo PIQUIC_AJAX_URL ;?>/styleajaxmakeup.php', 
								data: {'mid':mid},
								success: function(makeres){						
									// console.log(makeres);					
									$('.rdLip div').removeClass('border');

									// $("#make_up").html(makeres);
									var arr = makeres.split('@@@@@@');
									var img_src = $.trim(arr[0]);
									var hidField = $.trim(arr[1]);

									// console.log(arr);
									$("#make_up").css('background','url('+img_src+') no-repeat center top');
									$("#make_up").html(hidField);

									$("#lipstikcol").html('');
								}
							});
						}
					});
				<?php  } ?>
			}else{
				$('#modelsi'+ m +'').remove(); 
			}

		}


		function posecl(cl,mcl,ang) {
			var cl=cl;
			var mcl=mcl;
			$('#pose'+ cl +'').remove(); //removing thumbnails

			$("input[name=pose_"+ ang + "]").removeAttr('disabled'); //enabling selection

			var v = 'poseID_'+ cl +''; //adding to the array

			var l = list.length;

			for( var i = 0; i < l; i++){ 
				if ( list[i] === v ) {

					list.splice(i, 1);
					$('#my_pose_id').val(list);

				} else if ( list[l] === v ) {

					list.pop();
					$('#my_pose_id').val(list);

				} else if ( list.length == 0 && list[0] === v ) {

					list.shift();
					$('#my_pose_id').val(list);

				}
			}

			$('#poseID_'+ cl +'').prop('checked',false);
			$('#modelID'+ mcl +'').prop('checked',false);




			var checkedPose = [];
			$.each($("input[name='pose_"+ ang + "']:checked"), function(){          
				checkedPose.push($(this).val());
			});
											//alert(checkedPose);

											if(checkedPose=='')
											{

												$("#nav-top-tab").addClass('disabled');
												$("#nav-bottom-tab").addClass('disabled');
												$("#nav-shoes-tab").addClass('disabled');
												$("#nav-accessories-tab").addClass('disabled');

											}

											var bgbig = $('#bg img').attr('src');
											var posebig =$('#my_pose_id').val(); 
											var imgType = '<?php echo $imgpos; ?>';
											var topbig=$('#top img').attr('src');
											var bottombig=$('#bottom img').attr('src');
											var footbig=$('#foot img').attr('src');	

											console.log(" Big : "+bgbig+" ------Pose : "+posebig+" ------Image Type : "+imgType+" -------Top : "+topbig+" -----Bottom : "+bottombig+" ---------Foot : "+footbig);


											switch (imgType) {
												case 'D':


												if(bgbig!='' && posebig!='#' && posebig!='' && footbig!='#'  && footbig!=''){

													$('#savePsd').removeAttr("disabled");

												}
												else
												{
													$("#savePsd").attr("disabled", true);
												}


												break;
												case 'B':
			//alert("aaaaaaaaaa");
			if(bgbig!='' && posebig!='#' && posebig!='' && topbig!='#' && topbig!=''  && footbig!='#'  && footbig!=''){

				$('#savePsd').removeAttr("disabled");

			}
			else
			{
				$("#savePsd").attr("disabled", true);
			}



			break;
			case 'T':
			
			if(bgbig!='' && posebig!='#' && posebig!='' && bottombig!='#' && bottombig!=''  && footbig!='#'  && footbig!=''){

				$('#savePsd').removeAttr("disabled");

			}
			else
			{
				$("#savePsd").attr("disabled", true);
			}


			break;
			default:


			if(bgbig!='' && posebig!='#'  && posebig!='' && topbig!='#' && bottombig!='#' && footbig!='#' && posebig!='' && topbig!='' && bottombig!='' && footbig!=''){

				$('#savePsd').removeAttr("disabled");

			}
			else
			{
				$("#savePsd").attr("disabled", true);
			}        


		}


		var selectedValue = $("#my_pose_id").val();

		$.ajax({
			type:'POST',
			url:'<?php echo PIQUIC_AJAX_URL ;?>/posecheckboxselection.php',
			data:{'selectedValue':selectedValue},
			success: function(checkresults) {
			}
		});
	}



// Pose Selection End


function shadowimg(mid,genid,brndid) {
	$.ajax({
		type:'POST',
		url:'<?php echo PIQUIC_AJAX_URL ;?>/poseshadowselection.php',
		data:{'mid':mid,'genid':genid,'brndid':brndid},
		success: function(shdwcheckresults) {
				//alert(shdwcheckresults);
				$("#shdw").html(shdwcheckresults);
			}
		});
}


function changeimg(sku,img) {	
	var img = img;
	var skuno =sku;
	$.ajax({
		type:'POST',
		url:'<?php echo PIQUIC_AJAX_URL ;?>/style_top_ajax_img_chg.php', 
		data:{'img':img,'skuno':skuno},
		success: function(imgchngres) {	
			$("#top").html(imgchngres);
			//$('#resizable').draggable().resizable().rotatable();
			//$("#openImgRot_ctn").val('0');
			//$("#openImgResDrg_ctn").val('0');
			openImgRot('load');
			openImgDrg('load');
			openImgRes('load');
			
				



				



		}
	});

	$.ajax({
		type:'POST',
		url:'<?php echo PIQUIC_AJAX_URL ;?>/style_btm_ajax_img_chg.php', 
		data:{'img':img,'skuno':skuno},
		success: function(imgbtmchngres) {	
			$("#bottom").html(imgbtmchngres);
			//$('#resizable').draggable().resizable().rotatable();
			//$("#openImgRot_ctn").val('0');
			//$("#openImgResDrg_ctn").val('0');
			openImgRot('load');
			openImgDrg('load');
			openImgRes('load');

			 
			



		}
	});	

	$.ajax({
		type:'POST',
		url:'<?php echo PIQUIC_AJAX_URL ;?>/style_drs_ajax_img_chg.php', 
		data:{'img':img,'skuno':skuno},
		success: function(imgbtmchngres) {	
				//alert(imgbtmchngres);
				$("#dress").html(imgbtmchngres);
				//$('#resizable').draggable().resizable().rotatable();
				//$("#openImgRot_ctn").val('0');
			    //$("#openImgResDrg_ctn").val('0');
				openImgRot('load');
				openImgDrg('load');
				openImgRes('load');

              



				



			}
		});	




}


function loadTagImage(sku){
	var sku = sku;

	$.ajax({
		type: "POST",
		url: "<?php echo PIQUIC_AJAX_URL ;?>tag_img.php",
		data: {skuno:sku},
		success: function(skudata){
					// alert(skudata);
					$('#imgDiv').html(skudata);
				}
			});
}


function modelheadcl(cl) {
	var cl=cl;
	$('#modelsi'+ cl +'').remove();
	$('#modelID'+ cl +'').prop('checked',false);
}



		// Top Selection Start

		function topclick(m,i) {


			$('#topID'+ m + '').prop('checked', true);

			var lfckv = document.getElementById('topID'+ m + '').checked;
			if(lfckv==true){
				var imgbox='<li class="list-group-item"  id="top'+ m +'" ><div class="row"> <div class="col-9"> <img id="top_thumb_'+ m +'" class="img-thumbnail" src="'+ i + '" data-toggle="collapse" data-target="#top" ></div> <div class="col-3 iconimg" ><img id="top_close_'+ m +'" class="w-100 icont" src="<?php echo PIQUIC_URL;?>images/icon/times.png" onclick="topcl('+ m +')"><br /> <img class="w-100 iconlo" src="<?php echo PIQUIC_URL;?>images/icon/layer-over.png" onclick="topover('+ m +')"><br /> <img class="w-100 iconlu" src="<?php echo PIQUIC_URL;?>images/icon/layer-under.png" onclick="topunder('+ m +')"> </div> </div></li>';
			// $(imgbox).appendTo($(".sidepanel"));
			$('.toppanel').empty().append(imgbox);
		} else {
			$('#top'+ m +'').remove();
		}
	}

	function topimg(n,img) {
		if ($('#top-big').hasClass("show")){
			$('#top-big').attr('src',''+ img +'');
			$('#top_thumb_'+ n +'').attr('data-target', '');
		} else {
			$('#top-big').addClass("show");
			$('#top-big').attr('src',''+ img +'');
			$('#top_thumb_'+ n +'').attr('data-target', '');
		}


		/***************Poulami***************/

		var bgbig = $('#bg img').attr('src');
		var posebig =$('#my_pose_id').val(); 
		var imgType = '<?php echo $imgpos; ?>';
		var topbig=$('#top img').attr('src');
		var bottombig=$('#bottom img').attr('src');
		var footbig=$('#foot img').attr('src');	

		console.log(" Big : "+bgbig+" ------Pose : "+posebig+" ------Image Type : "+imgType+" -------Top : "+topbig+" -----Bottom : "+bottombig+" ---------Foot : "+footbig);


		switch (imgType) {
			case 'D':


			if(bgbig!='' && posebig!='#' && posebig!='' && footbig!='#'  && footbig!=''){

				$('#savePsd').removeAttr("disabled");

			}
			else
			{
				$("#savePsd").attr("disabled", true);
			}


			break;
			case 'B':
			//alert("aaaaaaaaaa");
			if(bgbig!='' && posebig!='#' && posebig!='' && topbig!='#' && topbig!=''  && footbig!='#'  && footbig!=''){

				$('#savePsd').removeAttr("disabled");

			}
			else
			{
				$("#savePsd").attr("disabled", true);
			}



			break;
			case 'T':
			
			if(bgbig!='' && posebig!='#' && posebig!='' && bottombig!='#' && bottombig!=''  && footbig!='#'  && footbig!=''){

				$('#savePsd').removeAttr("disabled");

			}
			else
			{
				$("#savePsd").attr("disabled", true);
			}


			break;
			default:


			if(bgbig!='' && posebig!='#'  && posebig!='' && topbig!='#' && bottombig!='#' && footbig!='#' && posebig!='' && topbig!='' && bottombig!='' && footbig!=''){

				$('#savePsd').removeAttr("disabled");

			}
			else
			{
				$("#savePsd").attr("disabled", true);
			}        


		}





		/******************************/

	}

	function topcl(cl) {
		var cl=cl;
		$('#top-big').attr('src','');
		$('#top'+ cl +'').remove();
		$('#topID'+ cl +'').prop('checked',false);
		$('#'+ cl +'').prop('checked',false);
		$('#skuno2nd').val('');


		/***************Poulami***************/

		var bgbig = $('#bg img').attr('src');
		var posebig =$('#my_pose_id').val(); 
		var imgType = '<?php echo $imgpos; ?>';
		var topbig=$('#top img').attr('src');
		var bottombig=$('#bottom img').attr('src');
		var footbig=$('#foot img').attr('src');	

		console.log(" Big : "+bgbig+" ------Pose : "+posebig+" ------Image Type : "+imgType+" -------Top : "+topbig+" -----Bottom : "+bottombig+" ---------Foot : "+footbig);


		switch (imgType) {
			case 'D':


			if(bgbig!='' && posebig!='#' && posebig!='' && footbig!='#'  && footbig!=''){

				$('#savePsd').removeAttr("disabled");

			}
			else
			{
				$("#savePsd").attr("disabled", true);
			}


			break;
			case 'B':
			//alert("aaaaaaaaaa");
			if(bgbig!='' && posebig!='#' && posebig!='' && topbig!='#' && topbig!=''  && footbig!='#'  && footbig!=''){

				$('#savePsd').removeAttr("disabled");

			}
			else
			{
				$("#savePsd").attr("disabled", true);
			}



			break;
			case 'T':
			
			if(bgbig!='' && posebig!='#' && posebig!='' && bottombig!='#' && bottombig!=''  && footbig!='#'  && footbig!=''){

				$('#savePsd').removeAttr("disabled");

			}
			else
			{
				$("#savePsd").attr("disabled", true);
			}


			break;
			default:


			if(bgbig!='' && posebig!='#'  && posebig!='' && topbig!='#' && bottombig!='#' && footbig!='#' && posebig!='' && topbig!='' && bottombig!='' && footbig!=''){

				$('#savePsd').removeAttr("disabled");

			}
			else
			{
				$("#savePsd").attr("disabled", true);
			}        


		}





		/******************************/
	}

	function topover(ov) {	
		var ov=ov;
		$("#top").css("z-index", "999");	
	}

	function topunder(un) {	
		var un=un;
		$("#top").css("z-index", "997");	
	}

	// Top Selection End

	// Bottom Selection Start

	function bottomclick(m,i) {
		//alert('bottom');
		$('#bottomID'+ m + '').prop('checked', true);		
		var lfckv = document.getElementById('bottomID'+ m + '').checked;
		if(lfckv==true){
			$('#skuno2nd').val('');
			var imgbox='<li class="list-group-item"  id="bottom'+ m +'" ><div class="row"> <div class="col-9"> <img id="bottom_thumb_'+ m +'" class="img-thumbnail" src="'+ i + '" data-toggle="collapse" data-target="#bottom" onclick="bottomimg('+ m +',\''+ i +'\')"></div> <div class="col-3 iconimg" ><img id="bottom_close_'+ m +'" class="w-100 icont" src="<?php echo PIQUIC_URL;?>images/icon/times.png" onclick="bottomcl('+ m +')"><br /> <img class="w-100 iconlo" src="<?php echo PIQUIC_URL;?>images/icon/layer-over.png" onclick="bottover('+ m +')"><br /> <img class="w-100 iconlu" src="<?php echo PIQUIC_URL;?>images/icon/layer-under.png" onclick="bottunder('+ m +')"> </div> </div></li>';
			//$(imgbox).appendTo($(".sidepanel"));
			$('.bottompanel').empty().append(imgbox);
		} else {
			$('#bottom'+ m +'').remove();
		}
	}

	function bottomimg(n,img) {

		if ($('#bottom-big').hasClass("show")){
			$('#bottom-big').attr('src',''+ img +'');
			$('#bottom_thumb_'+ n +'').attr('data-target', '');
		} else {
			$('#bottom-big').addClass("show");
			$('#bottom-big').attr('src',''+ img +'');
			$('#bottom_thumb_'+ n +'').attr('data-target', '');
		}


		/***************Poulami***************/

		var bgbig = $('#bg img').attr('src');
		var posebig =$('#my_pose_id').val(); 
		var imgType = '<?php echo $imgpos; ?>';
		var topbig=$('#top img').attr('src');
		var bottombig=$('#bottom img').attr('src');
		var footbig=$('#foot img').attr('src');	

		console.log(" Big : "+bgbig+" ------Pose : "+posebig+" ------Image Type : "+imgType+" -------Top : "+topbig+" -----Bottom : "+bottombig+" ---------Foot : "+footbig);


		switch (imgType) {
			case 'D':


			if(bgbig!='' && posebig!='#' && posebig!='' && footbig!='#'  && footbig!=''){

				$('#savePsd').removeAttr("disabled");

			}
			else
			{
				$("#savePsd").attr("disabled", true);
			}


			break;
			case 'B':
			//alert("aaaaaaaaaa");
			if(bgbig!='' && posebig!='#' && posebig!='' && topbig!='#' && topbig!=''  && footbig!='#'  && footbig!=''){

				$('#savePsd').removeAttr("disabled");

			}
			else
			{
				$("#savePsd").attr("disabled", true);
			}



			break;
			case 'T':
			
			if(bgbig!='' && posebig!='#' && posebig!='' && bottombig!='#' && bottombig!=''  && footbig!='#'  && footbig!=''){

				$('#savePsd').removeAttr("disabled");

			}
			else
			{
				$("#savePsd").attr("disabled", true);
			}


			break;
			default:


			if(bgbig!='' && posebig!='#'  && posebig!='' && topbig!='#' && bottombig!='#' && footbig!='#' && posebig!='' && topbig!='' && bottombig!='' && footbig!=''){

				$('#savePsd').removeAttr("disabled");

			}
			else
			{
				$("#savePsd").attr("disabled", true);
			}        


		}





		/******************************/
	}

	function bottomcl(cl) {
		var cl=cl;
		$('#bottom-big').attr('src','');
		$('#bottom'+ cl +'').remove();
		$('#bottomID'+ cl +'').prop('checked',false);
		$('#'+ cl +'').prop('checked',false);
		$('#skuno2nd').val('');

		/***************Poulami***************/

		var bgbig = $('#bg img').attr('src');
		var posebig =$('#my_pose_id').val(); 
		var imgType = '<?php echo $imgpos; ?>';
		var topbig=$('#top img').attr('src');
		var bottombig=$('#bottom img').attr('src');
		var footbig=$('#foot img').attr('src');	

		console.log(" Big : "+bgbig+" ------Pose : "+posebig+" ------Image Type : "+imgType+" -------Top : "+topbig+" -----Bottom : "+bottombig+" ---------Foot : "+footbig);


		switch (imgType) {
			case 'D':


			if(bgbig!='' && posebig!='#' && posebig!='' && footbig!='#'  && footbig!=''){

				$('#savePsd').removeAttr("disabled");

			}
			else
			{
				$("#savePsd").attr("disabled", true);
			}


			break;
			case 'B':
			//alert("aaaaaaaaaa");
			if(bgbig!='' && posebig!='#' && posebig!='' && topbig!='#' && topbig!=''  && footbig!='#'  && footbig!=''){

				$('#savePsd').removeAttr("disabled");

			}
			else
			{
				$("#savePsd").attr("disabled", true);
			}



			break;
			case 'T':
			
			if(bgbig!='' && posebig!='#' && posebig!='' && bottombig!='#' && bottombig!=''  && footbig!='#'  && footbig!=''){

				$('#savePsd').removeAttr("disabled");

			}
			else
			{
				$("#savePsd").attr("disabled", true);
			}


			break;
			default:


			if(bgbig!='' && posebig!='#'  && posebig!='' && topbig!='#' && bottombig!='#' && footbig!='#' && posebig!='' && topbig!='' && bottombig!='' && footbig!=''){

				$('#savePsd').removeAttr("disabled");

			}
			else
			{
				$("#savePsd").attr("disabled", true);
			}        


		}





		/******************************/
	}



	function bottover(ov) {	
		var ov=ov;
		$("#top").css("z-index", "998");
		$("#bottom").css("z-index", "999");	
	}

	function bottunder(un) {	
		var un=un;
		$("#top").css("z-index", "999");
		$("#bottom").css("z-index", "998");  
	}
	// Bottom Selection End

	// Foot Selection Start

	function footclick(m,i) {	

		$('#footID'+ m + '').prop('checked', true);

		var lfckv = document.getElementById('footID'+ m + '').checked;
		if(lfckv==true){
			
			var imgbox='<li class="list-group-item"  id="foot'+ m +'" ><div class="row"> <div class="col-9"> <img id="foot_thumb_'+ m +'" class="img-thumbnail" src="'+ i + '" data-toggle="collapse" data-target="#foot" onclick="footimg('+ m +',\''+ i +'\')"></div> <div class="col-3 iconimg" ><img id="foot_close_'+ m +'" class="w-100 icont" src="<?php echo PIQUIC_URL;?>images/icon/times.png" onclick="footcl('+ m +')"><br /> <img class="w-100 iconlo" src="<?php echo PIQUIC_URL;?>images/icon/layer-over.png" onclick="footover('+ m +')"><br /> <img class="w-100 iconlu" src="<?php echo PIQUIC_URL;?>images/icon/layer-under.png" onclick="footunder('+ m +')"> </div> </div></li>';
			// $(imgbox).appendTo($(".sidepanel"));
			$('.footpanel').empty().append(imgbox);

		} else {
			$('#foot'+ m +'').remove();
		}
	}

	function footimg(n,img) {
		if ($('#foot-big').hasClass("show")){
			$('#foot-big').attr('src',''+ img +'');
			$('#foot_thumb_'+ n +'').attr('data-target', '');
		} else {
			$('#foot-big').addClass("show");
			$('#foot-big').attr('src',''+ img +'');
			$('#foot_thumb_'+ n +'').attr('data-target', '');
		}





		/***************Poulami***************/

		var bgbig = $('#bg img').attr('src');
		var posebig =$('#my_pose_id').val(); 
		var imgType = '<?php echo $imgpos; ?>';
		var topbig=$('#top img').attr('src');
		var bottombig=$('#bottom img').attr('src');
		var footbig=$('#foot img').attr('src');	

		console.log(" Big : "+bgbig+" ------Pose : "+posebig+" ------Image Type : "+imgType+" -------Top : "+topbig+" -----Bottom : "+bottombig+" ---------Foot : "+footbig);


		switch (imgType) {
			case 'D':


			if(bgbig!='' && posebig!='#' && posebig!='' && footbig!='#'  && footbig!=''){

				$('#savePsd').removeAttr("disabled");

			}
			else
			{
				$("#savePsd").attr("disabled", true);
			}


			break;
			case 'B':
           // alert("aaaaaaa");
           console.log(" Big : "+bgbig+" ------Pose : "+posebig+" -------Top : "+topbig+"---------Foot : "+footbig);
           if(bgbig!='' && posebig!='#' && posebig!='' && topbig!='#' && topbig!=''  && footbig!='#'  && footbig!=''){

           	$('#savePsd').removeAttr("disabled");
				//alert("bbbbbbbbb");

			}
			else
			{
				$("#savePsd").attr("disabled", true);
				//alert("ccccccccc");
			}



			break;
			case 'T':
			
			if(bgbig!='' && posebig!='#' && posebig!='' && bottombig!='#' && bottombig!=''  && footbig!='#'  && footbig!=''){

				$('#savePsd').removeAttr("disabled");

			}
			else
			{
				$("#savePsd").attr("disabled", true);
			}


			break;
			default:


			if(bgbig!='' && posebig!='#'  && posebig!='' && topbig!='#' && bottombig!='#' && footbig!='#' && posebig!='' && topbig!='' && bottombig!='' && footbig!=''){

				$('#savePsd').removeAttr("disabled");

			}
			else
			{
				$('#savePsd').addClass("disabled");
			}        


		}





		/******************************/




	}

	function footcl(cl) {
		var cl=cl;
		$('#foot-big').attr('src','');
		$('#foot'+ cl +'').remove();
		$('#footID'+ cl +'').prop('checked',false);


		/***************Poulami***************/

		var bgbig = $('#bg img').attr('src');
		var posebig =$('#my_pose_id').val(); 
		var imgType = '<?php echo $imgpos; ?>';
		var topbig=$('#top img').attr('src');
		var bottombig=$('#bottom img').attr('src');
		var footbig=$('#foot img').attr('src');	

		console.log(" Big : "+bgbig+" ------Pose : "+posebig+" ------Image Type : "+imgType+" -------Top : "+topbig+" -----Bottom : "+bottombig+" ---------Foot : "+footbig);


		switch (imgType) {
			case 'D':


			if(bgbig!='' && posebig!='#' && posebig!='' && footbig!='#'  && footbig!=''){

				$('#savePsd').removeAttr("disabled");

			}
			else
			{
				$("#savePsd").attr("disabled", true);
			}


			break;
			case 'B':
			//alert("aaaaaaaaaa");
			if(bgbig!='' && posebig!='#' && posebig!='' && topbig!='#' && topbig!=''  && footbig!='#'  && footbig!=''){

				$('#savePsd').removeAttr("disabled");

			}
			else
			{
				$("#savePsd").attr("disabled", true);
			}



			break;
			case 'T':
			
			if(bgbig!='' && posebig!='#' && posebig!='' && bottombig!='#' && bottombig!=''  && footbig!='#'  && footbig!=''){

				$('#savePsd').removeAttr("disabled");

			}
			else
			{
				$("#savePsd").attr("disabled", true);
			}


			break;
			default:


			if(bgbig!='' && posebig!='#'  && posebig!='' && topbig!='#' && bottombig!='#' && footbig!='#' && posebig!='' && topbig!='' && bottombig!='' && footbig!=''){

				$('#savePsd').removeAttr("disabled");

			}
			else
			{
				$("#savePsd").attr("disabled", true);
			}        


		}





		/******************************/
	}


	function footover(ov) {	
		var ov=ov;
		$("#bottom").css("z-index", "998");	
	}

	function footunder(un) {	
		var un=un;
		$("#bottom").css("z-index", "997");	
	}
	// Foot Selection End

	function accessimg(n, img, subcat) {

		if($('#accessID_'+ n + '').is(":checked")){

			var subcat = subcat;
			<?php


			$query16= "SELECT * FROM `tbl_subcategory` where enabled='1' ORDER by `id_subcat`";
			$query=$second_DB->query($query16);
			$addaccessresult=$query->result_array();

			foreach($addaccessresult as $key => $addaccesssubrow)
			{
				$addid_subcat=$addaccesssubrow['id_subcat']; 
				if($addid_subcat==1){
					$addsubstr='rn';
				} 
				if($addid_subcat==2){
					$addsubstr='er';
				}
				if($addid_subcat==3){
					$addsubstr='nc';
				} 
				if($addid_subcat==4){
					$addsubstr='br';
				}
				?>

				if(subcat==<?php echo $addid_subcat;?>){

					if ($('#acc<?php echo $addsubstr;?>-big').hasClass("show")){
						$('#acc<?php echo $addsubstr;?>-big').attr('src',''+ img +'');
						$('#access_thumb_'+ n +'').attr('data-target', '');
					} else {
						$('#acc<?php echo $addsubstr;?>-big').addClass("show");
						$('#acc<?php echo $addsubstr;?>-big').attr('src',''+ img +'');
						$('#access_thumb_'+ n +'').attr('data-target', '');
					}
				}
				<?php 

			} 

			?>

		} else {
			<?php

			$query16= "SELECT * FROM `tbl_subcategory` where enabled='1' ORDER by `id_subcat`";
			$query=$second_DB->query($query16);
			$addaccessresult=$query->result_array();

			foreach($addaccessresult as $key => $accesssubrow)
			{ 
				$rmid_subcat=$accesssubrow['id_subcat']; 
				if($rmid_subcat==1){
					$rmsubstr='rn';
				} 
				if($rmid_subcat==2){
					$rmsubstr='er';
				}
				if($rmid_subcat==3){
					$rmsubstr='nc';
				} 
				if($rmid_subcat==4){
					$rmsubstr='br';
				}
				?>
				if(subcat==<?php echo $rmid_subcat;?>){
					$('#acc<?php echo $rmsubstr;?>-big').removeClass("show");
					$('#acc<?php echo $rmsubstr;?>-big').attr('src','');
				}
			<?php } ?>
		}
	}

	function accesscl(acccl,subcat) {
		var acccl=acccl;
		var subcat=subcat;

		<?php


		$query16= "SELECT * FROM `tbl_subcategory` where enabled='1' ORDER by `id_subcat`";
		$query=$second_DB->query($query16);
		$addaccessresult=$query->result_array();

		foreach($addaccessresult as $key => $accesssubrow)
		{ 


			$clid_subcat=$accesssubrow['id_subcat']; 
			if($clid_subcat==1){
				$clsubstr='rn';
			} 
			if($clid_subcat==2){
				$clsubstr='er';
			}
			if($clid_subcat==3){
				$clsubstr='nc';
			} 
			if($clid_subcat==4){
				$clsubstr='br';
			}
			?>
			if(subcat==<?php echo $clid_subcat;?>){
				$('#acc<?php echo $clsubstr;?>-big').removeClass("show");
				$('#acc<?php echo $clsubstr;?>-big').attr('src','');
			}
		<?php } ?>

		$('#access'+ acccl +'').remove();

		$("input[name=access_"+ subcat + "]").removeAttr('disabled'); //enabling selection

		var acs = 'accessID_'+ acccl +'';

		var l = accelist.length;

		for( var i = 0; i < l; i++){ 
			if ( accelist[i] === acs ) {

				accelist.splice(i, 1);
				$('#my_access_id').val(accelist);

			} else if ( accelist[l] === acs ) {

				accelist.pop();
				$('#my_access_id').val(accelist);

			} else if ( accelist.length == 0 && accelist[0] === acs ) {

				accelist.shift();
				$('#my_access_id').val(accelist);

			}
		}

		$('#accessID_'+ acccl +'').prop('checked',false);

		var accselectedValue = $("#my_access_id").val();

		$.ajax({
			type:'POST',
			url:'<?php echo PIQUIC_AJAX_URL ;?>accesscheckboxselection.php',
			data:{'accselectedValue':accselectedValue},
			success: function(accesscheckresults) { 
				// alert(accesscheckresults);
			}
		});

	}


	//  Accessories selection End

		// Validation Start

		function topvalidation(top_image) {
			var pose_img = $('#my_pose_id').val();
			var sku_no = $('#skuid').val();
			// $.ajax({
			// 	type:'POST',
			// 	url:'<?php echo PIQUIC_AJAX_URL ;?>/top_psd_exist.php', 
			// 	data:{'pose_img':pose_img,'top_image':top_image,'sku_no':sku_no},
			// 	success: function(toperrlogdata) {	
			// 		//alert(toperrlogdata);	
			// 		if(toperrlogdata!=1){					
			// 			//alert(toperrlogdata);
			// 		}
			// 	}
			// });
		}

		function footvalidation(foot_image){
			var pose_img = $('#my_pose_id').val();
			var sku_no = $('#skuid').val();
	    	//alert(pose_img);
	  //   	$.ajax({
	  //   		type:'POST',
	  //   		url:'<?php echo PIQUIC_AJAX_URL ;?>/foot_psd_exist.php', 
	  //   		data:{'pose_img':pose_img,'foot_image':foot_image,'sku_no':sku_no},
	  //   		success: function(footerrlogdata) {
	  //   			if(footerrlogdata!=1){
	  //   				//alert(footerrlogdata);
	  //   			}
			//     //console(data);
			//  }
			// });
		}

		function bottomvalidation(bottom_image) {
			var pose_img = $('#my_pose_id').val();
			var sku_no = $('#skuid').val();
		    //alert(bottom_image);
		   //  $.ajax({
		   //  	type:'POST',
		   //  	url:'<?php echo PIQUIC_AJAX_URL ;?>/bottom_psd_exist.php',
		   //  	data:{'pose_img':pose_img,'bottom_image':bottom_image,'sku_no':sku_no},
		   //  	success: function(boterrlogdata) {
			  //       //console(data);
			  //       if(boterrlogdata!=1){
			  //       	//alert(boterrlogdata);
			  //       }
			  //    }
			  // });
			}


			function accessvalidation(access_image){
				var pose_img = $('#my_pose_id').val();
				var sku_no = $('#skuid').val();

		 //    $.ajax({
		 //    	type:'POST',
		 //    	url:'<?php echo PIQUIC_AJAX_URL ;?>/access_psd_exist.php', 
		 //    	data:{'pose_img':pose_img,'access_image':access_image,'sku_no':sku_no},
		 //    	success: function(accesserrlogdata) {
		 //    		if(accesserrlogdata!=1){
		 //    			alert(accesserrlogdata);
		 //    		}	     
			//  }
			// });
		}

		// Validation End
	</script>


	<script>
		var error_msg;

		$(document).ready(function() { 
                //alert("aa");


                var clntid ='<?php echo $brand ;?>'; 
                var skuno='<?php echo $skuurl;?>';
                var genid =$('#generID').val(); 

                $('#off-model').on("click", function(){

				//alert("aaaaaa");
				var status= 'on';

				$('#off-model').removeClass('d-block').addClass('d-none');
				$('#on-model').removeClass('d-none').addClass('d-block');  

				$.ajax({
					type:'POST',
					url:'<?php echo PIQUIC_AJAX_URL ;?>/style_automodel_ajax.php',
					data:{'clntid':clntid,'skuid':skuno,'status':status,'genid':genid},
					success: function(results) {  
			// alert(results);

			$('#pose-big').attr('src','');
			$("#modelandangles").html(results);
		}
	}); 


			}); 

                $('#on-model').on("click", function(){ 

                	var status= 'off';
                	$('#on-model').removeClass('d-block').addClass('d-none');
                	$('#off-model').removeClass('d-none').addClass('d-block');

                	$.ajax({
                		type:'POST',
                		url:'<?php echo PIQUIC_AJAX_URL ;?>/style_automodel_ajax.php',
                		data:{'clntid':clntid,'skuid':skuno,'status':status ,'genid':genid},
                		success: function(results) {  
			// alert(results);     
			$('#pose-big').attr('src','');
			$("#modelandangles").html(results);
		}
	}); 


                });


                $("#generID").on("change", function() {
                	var genid =$('#generID').val(); 
                	$.ajax({
                		type:'POST',
                		url:'<?php echo PIQUIC_AJAX_URL ;?>/styleajaxmodel.php', 
                		data: {'genid':genid},
                		success: function(results) {	
                			$("#modelandangles").html(results);
                		}
                	});
                });

                $("#shotAngle").on("change", function(){


                	$("div#divLoading").addClass('show');
                	$("#loader").show();


                	var poseid =$('#shotAngle').val(); 
                	var pmidn =$('#pmidn').val(); 
                	$.ajax({
                		type:'POST',
                		url:'<?php echo PIQUIC_AJAX_URL ;?>/styleajaxpose_filter.php', 
                		data:{'poseid':poseid,'mid':pmidn},
                		success: function(results) {	

                            //console.log(results);
                			$("#modelposeandangles").html(results);
                			$("div#divLoading").removeClass('show'); 

                			var noOfImages = $("img").length;
                			console.log(noOfImages);
                			var noLoaded = 0;

                			$("img").on('load', function(){ 
                				console.log(this);
							//$(this).css('background','none');
							$(this).removeClass('loading');
						});


                		}
                	});	
                });







                $("#savePsd").on("click", function() { 

                	/***************Poulami***************/

				//alert("ccccccccc");

				/******************************/



				// alert('saving');
				var bgbig = $('#bg img').attr('src');
				var topbig=$('#top img').attr('src');
				var bottombig=$('#bottom img').attr('src');
				var footbig=$('#foot img').attr('src');

				var accessbig =$('#my_access_id').val();

				// alert(accessbig);

				var posebig =$('#my_pose_id').val(); 
				var imgtag =$('#imgtag').val(); 
				var modradioVal = $("input[name='model']:checked").val();

				var skuno = '<?php echo $skuurl; ?>';
				var imgType = '<?php echo $imgpos; ?>';
				var client_name = '<?php echo $skuclient_name; ?>';

				var lipstickcolcode =$('#radioLip-value').val();
				var lipselectEffect=$("select[name=selectEffect]").val(); 
				var eyeshdowcolcode =$('#rdsh-value').val();
				var eyemkup =$('#rdeyemk-value').val();
				var eyeintensity =$('#s-inVal').text();
				var blushcolcode =$('#rdBlsh-value').val();	
				var blushintensity =$('#b-inVal').text();
				var gender = '<?php echo $gender; ?>';
				var shadowval =$('#shdwimg').val();

				//alert(shadowval);

				// alert(lipstickcolcode);
				// alert(lipselectEffect);
				// alert(eyemkup);

				if(lipstickcolcode==''){
					var lipstickcolcode ='';
					var lipselectEffect=''; 
				}else{
					var lipstickcolcode=lipstickcolcode;
					var lipselectEffect=lipselectEffect; 
				}
				// alert(lipselectEffect);
				if(eyeshdowcolcode==''){
					var eyeshdowcolcode ='';
					var eyeintensity=''; 
				}else{

					var eyeshdowcolcode = eyeshdowcolcode;
					var eyeintensity= eyeintensity; 
				}

				if(blushcolcode==''){
					var blushcolcode ='';
					var blushintensity=''; 
				}else{

					var blushcolcode = blushcolcode;
					var blushintensity= blushintensity; 
				}

				if(imgtag!=''){

					//alert(imgType);
					if(imgType=='TUG' || imgType=='BUG'){ 
						if(gender=='2' || gender=='5' || gender=='6' ){ 
							if(bgbig!='' && posebig!='#' && posebig!='' && topbig!='#' && bottombig!='#'&& topbig!='' && bottombig!=''){ 
								<?php //include 'stylesaveajaxscript.php';?>

								<?php
								$this->load->view('front/stylesaveajaxscript');
								?>



							}else{
								error_msg = 'Please select assets for PSD.';
								$('#errLabelSty').text(error_msg);
								$('#err_msg_sty').modal('show');
							}
						}else{
							if(bgbig!='' && posebig!='#' && posebig!=''){ 
								if(topbig=='#'){topbig='';}
								if(bottombig=='#'){bottombig='';}
								<?php //include 'stylesaveajaxscript.php';?>
								<?php
								$this->load->view('front/stylesaveajaxscript');
								?>





							}else{
								error_msg = 'Please select Pose for PSD.';
								$('#errLabelSty').text(error_msg);
								$('#err_msg_sty').modal('show');
							}
						}
					} 
					else if(imgType=='D' ){ 

						/*alert("poulami");
                        alert(bgbig);
						alert(posebig);
						alert(footbig);*/
						

						if(bgbig!='' && posebig!='#' && footbig!='#' && posebig!='' && footbig!=''){	
							<?php //include 'stylesaveajaxscript.php';?>	

							<?php
							$this->load->view('front/stylesaveajaxscript');
							?>







						}	else {
							error_msg = 'Please select all assets for PSD.';
							$('#errLabelSty').text(error_msg);
							$('#err_msg_sty').modal('show');
						}	
					} else{

						

						if(imgType=="T")
						{
							topbig='<?php echo $cutout_uploaded_image;?>';
						}
						else if(imgType=="B")
						{
							bottombig='<?php echo $cutout_uploaded_image;?>';
						}

						//alert(bgbig);
						//alert(posebig);
						//alert(topbig);
						//alert(bottombig);
						//alert(footbig);


						if(bgbig!='' && posebig!='#' && topbig!='#' && bottombig!='#' && footbig!='#' && posebig!='' && topbig!='' && bottombig!='' && footbig!=''){
                           //alert("ppppp");

                           <?php //include 'stylesaveajaxscript.php';?>	
                           <?php
                           $this->load->view('front/stylesaveajaxscript');
                           ?>



                        }	else {
                        	error_msg = 'Please select all assets for PSD.';
                        	$('#errLabelSty').text(error_msg);
                        	$('#err_msg_sty').modal('show');
                        }	
                     }
                  }else{

					//alert("aaaaaaaaa");
					error_msg = 'Please upload Tag image, before styling. Redirecting to Labelpage.';
					//$('#errLabelSty').text(error_msg);
					//$('#err_msg_sty').modal('show');
					alert(error_msg);

					$.ajax({
						type:'POST',
						url:'<?php echo PIQUIC_AJAX_URL ;?>/updatelabelstatus.php', 
						data: {'skuid':skuno},
						success: function(accessres){ 
					//alert(accessres);
					//alert("<?php echo base_url() ;?>label");
					window.location.href="<?php echo base_url() ;?>label";

				}
			});



				}
			});
























});








</script>


