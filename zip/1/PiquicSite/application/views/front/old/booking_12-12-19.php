<?php
$data['page_title'] = "booking";

$this->load->view('front/includes/header',$data);
// $this->load->view('front/includes/menu');
$this->load->view('front/includes/nav');
if(!empty($book_id)){
	$order_id=$booking_data[0]['order_id'];
	$gender=$booking_data[0]['gender'];
	$category=$booking_data[0]['category'];
	if(!empty($category))
	{
		$category_arr=array_filter(explode(",", $category));
	}
	$quty=$booking_data[0]['quty'];
	if(!empty($quty))
	{
		$quty_arr=array_filter(explode(",", $quty));
	}
	$total_quty=$booking_data[0]['quty'];
	$angle=$booking_data[0]['angle'];

	// if(!empty($angle))
	// {
	// 	$angle_arr=array_filter(explode(",", $angle));
	// }
	$m_status=$booking_data[0]['m_status'];
	$d_status=$booking_data[0]['d_status'];
	$notes=$booking_data[0]['notes'];
	$total_price=$booking_data[0]['total_price'];
}
else{
	$booking_data="";
	$book_id="";
	$order_id="";
	$gender="";
	$category="";
	$category_arr="";
	$quty_arr="";
	$quty="";
	$total_quty="";
	$angle="";

	$m_status="";
	$d_status="";
	$notes="";
	$total_price="";
}
?>

<div class="container-fluid">
	<div class="row">

		<?php $this->load->view('front/includes/sidebar',$booking_data,$book_id); ?>

		<div class="col-12 col-sm-12 col-md-9 col-lg-9 col-xl-9 border mainscroll">
			<div class="container pt-3">
				<div class="row mb-4">
					<div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
						<h1>Book your Piquic shoot</h1>
					</div>
				</div>
			</div>

			<div class="container pt-3">
				<div class="row mb-4">
					<div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
						<form class="form form-horizontal has-validation-callback"  id="booking_form" method="post">  
						<input type="hidden" name="user_id" id="user_id" value="<?php echo $user_id ?>">
						<input type="hidden" name="order_id" id="order_id" value="<?php if(!empty($order_id)){ echo $order_id;}else{ echo "Piquic_".time();} ?>">	
						<?php if(!empty($angle)){
							?>
							<input type="hidden" name="angle_text" id="angle_text" value="<?php echo($angle);?>">
							<?php 
						}
						?>
						<div id="divGender" class="pl-3 py-4 divGender">
							<h3>GENDER</h3>
						<div class="custom-control custom-radio custom-control-inline">
								<input type="radio" id="male" name="gender" class="custom-control-input gender" value="1" <?php if($gender=="1"){ echo "checked" ; } ?> >
								<label class="custom-control-label" for="male">Male</label>
							</div>
							<div class="custom-control custom-radio custom-control-inline">
								<input type="radio" id="female" name="gender" class="custom-control-input gender" <?php if($gender=="2"){ echo "checked" ; } ?> value="2" >
								<label class="custom-control-label" for="female">Female</label>
							</div>
							<div class="custom-control custom-radio custom-control-inline">
								<input type="radio" id="boy" name="gender" class="custom-control-input gender" <?php if($gender=="4"){ echo "checked" ; } ?> value="4" >
								<label class="custom-control-label" for="boy">Boy</label>
							</div>
							<div class="custom-control custom-radio custom-control-inline">
								<input type="radio" id="girl" name="gender" class="custom-control-input gender" <?php if($gender=="6"){ echo "checked" ; } ?> value="6" >
								<label class="custom-control-label" for="girl">Girl</label>
							</div>
							<span id="gender_validate"></span>
					</div>

						<div id="divCategory" class="pl-3 py-4">
							<h3>CATEGORY</h3>

							<div id="addCategory">

								
									
								

									<?php
								if(!empty($category_arr)){
									$cat_count=count($category_arr);
									$i=1;
									foreach (array_combine($category_arr, $quty_arr) as $category_arr => $quty_arr ) {
										?>
										<div id="category_<?php echo $i; ?>" class="form-row">
											<div class="form-group col-4">
												<label for="cat_<?php echo $i; ?>">Category</label>
												<select class="custom-select cat_select" id="cat_<?php echo $i; ?>" name="cat[]" >
													<option value="">Select your Category</option>
													<option value="Top" <?php if($category_arr=="Top"){ echo "selected"; } ?>>Top</option>
													<option value="Bottom" <?php if($category_arr=="Bottom"){ echo "selected"; } ?>>Bottom</option>
													<option value="Dress" <?php if($category_arr=="Dress"){ echo "selected"; } ?>>Dress</option>
												</select>
											</div>
											<div class="form-group col-4">
												<label for="quty_<?php echo $i; ?>">Quantity</label>
												<input type="text" class="form-control txt_quty" id="quty_<?php echo $i; ?>" name="quty[]" placeholder="Enter your Quantity" value="<?php if(!empty($quty_arr)){ echo $quty_arr;	} ?>" >
											</div>
											<?php if($i>=2)
											{
												?>
												<span class="cate_remv pt-4 pl-2"><i class="far fa-times-circle lead text-danger pt-2"></i></span> 
												<?php
											}
											?>
									</div>
									<?php
									$i++;
									}
									
								}
								else{
									?>
									<div id="category_1" class="form-row">
											<div class="form-group col-4">
												<label for="cat_1">Category</label>
												<select class="custom-select cat_select" id="cat_1" name="cat[]" >
													<option value="">Select your Category</option>
													<option value="Top">Top</option>
													<option value="Bottom">Bottom</option>
													<option value="Dress">Dress</option>
												</select>
											</div>
											<div class="form-group col-4">
												<label for="quty_1">Quantity</label>
												<input type="text" class="form-control txt_quty" id="quty_1" name="quty[]" placeholder="Enter your Quantity">
											</div>
									</div>
									<?php
								}
								?>
									
							</div>

							<div id="btnAddCategory">
								<i class="fas fa-plus-square"></i>&emsp;Add Category
							</div>

							<div id="maxMsg" class="d-none text-center">
								<div class="alert alert-warning" role="alert">
									You have selected the maximum no. of category.
								</div>								
							</div>
						</div>

						<div id="divModel" class="pl-3 py-4">
							<h3>MODEL</h3>

							<div class="form-row pb-4">
								<div class="col-12 col-md-4">
									<div class="custom-control custom-radio">
										<input type="radio" id="rdPSE" name="m_status" class="custom-control-input" value="1" <?php if($m_status=="1"){ echo "checked" ; } ?> >
										<label class="custom-control-label" for="rdPSE">Let "Piquic Style Expert" select my models & style.</label>
									</div>
								</div>
								<div class="col-12 col-md-1">&nbsp;</div>
								<div class="col-12 col-md-4">
									<div class="custom-control custom-radio">
										<input type="radio" id="rdSelf" name="m_status" class="custom-control-input" value="2" <?php if($m_status=="2"){ echo "checked" ; } ?>>
										<label class="custom-control-label" for="rdSelf">I want to select my models and styling once my apparels gets digitized.</label>
									</div>
								</div>
								<span id="m_status_validate"></span>
							</div>

							<h6 data-toggle="collapse" data-target="#mod_el"><u>Preveiw models</u></h6>
							<div id="mod_el" class="form-row pb-4 collapse">
								<div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
									<div id="model">
									</div>
								</div>
							</div>
						</div>

						<div id="angles_size" class="pl-3 py-4">
							<h3>IMAGE ANGLES & SIZE</h3>

						</div>

						<div id="delivery" class="pl-3 py-4">
							<h3>DELIVERY</h3>

							<div class="form-row pb-4">
								<div class="col-12 col-md-4">
									<div class="custom-control custom-radio">
										<input type="radio" id="rdSD" name="d_status" class="custom-control-input d_status" value="1" <?php if($d_status=="2"){ echo "checked" ; }?>>
										<label class="custom-control-label" for="rdSD">Standard (6-7 days after receiving your products).</label>
									</div>
								</div>
								<div class="col-12 col-md-1">&nbsp;</div>
								<div class="col-12 col-md-4">
									<div class="custom-control custom-radio">
										<input type="radio" id="rdED" name="d_status" class="custom-control-input d_status" value="2" <?php if($d_status=="2"){ echo "checked" ; } else{ echo "checked"; } ?>>
										<label class="custom-control-label" for="rdED">Express (2-3 days after receiving your products).</label>
									</div>
								</div>
							</div>
						</div>

						<div id="divNotes" class="pl-3 py-4">
							<div class="form-row">
								<div class="form-group col-8">
									<label for="notes" class="lblNotes">&nbsp;NOTES&nbsp; (Optional)</label>
									<textarea class="form-control" id="notes" name="notes" rows="3" ><?php if(!empty($notes)){ echo $notes; } ?></textarea>
								</div>
							</div>
						</div>
						
						<div class="pl-3 py-4">
							<input type="hidden" class="form-control txt_quty" id="total_price" name="total_price" value="<?php if(!empty($total_price)){ echo $total_price; } ?>">
							<button class="btn btn-piquic w-75" type="submit" id="btnBook" name="btnBook">CONFIRM BOOKING</button>
						</div>
	
					</form>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<?php 

	$total_price=0;
	$query = $this->db->get_where('tbl_users', array('user_id'=>$user_id));
	//$query = $this->db->get("tbl_upload_img");
	$result=$query->result_array(); 
	$curr=$result[0]['user_currency'];
	if($curr=="USD")
	{
		$query1 = $this->db->get_where('tbl_settings', array('setting_key'=>'PER_PRICE_USD'));
		//$query = $this->db->get("tbl_upload_img");
		$result1=$query1->result_array(); 
		$price=$result1[0]['setting_value'];
	}
	elseif($curr=="INR")
	{
		$query1 = $this->db->get_where('tbl_settings', array('setting_key'=>'PER_PRICE_INR'));
		//$query = $this->db->get("tbl_upload_img");
		$result1=$query1->result_array(); 
		$price=$result1[0]['setting_value'];
	}
	elseif($curr=="EUR")
	{
		$query1 = $this->db->get_where('tbl_settings', array('setting_key'=>'PER_PRICE_EUR'));
		//$query = $this->db->get("tbl_upload_img");
		$result1=$query1->result_array(); 
		$price=$result1[0]['setting_value'];
	}
	elseif($curr=="GBP")
	{
		$query1 = $this->db->get_where('tbl_settings', array('setting_key'=>'PER_PRICE_GBP'));
		//$query = $this->db->get("tbl_upload_img");
		$result1=$query1->result_array(); 
		$price=$result1[0]['setting_value'];
	}
	elseif($curr=="AUD")
	{
		$query1 = $this->db->get_where('tbl_settings', array('setting_key'=>'PER_PRICE_AUD'));
		//$query = $this->db->get("tbl_upload_img");
		$result1=$query1->result_array(); 
		$price=$result1[0]['setting_value'];
	}
	elseif($curr=="CNY")
	{
		$query1 = $this->db->get_where('tbl_settings', array('setting_key'=>'PER_PRICE_CNY'));
		//$query = $this->db->get("tbl_upload_img");
		$result1=$query1->result_array(); 
		$price=$result1[0]['setting_value'];
	}
?>
<?php $this->load->view('front/includes/footer'); ?>

<script type="text/javascript">
	function select_disable()
	{
		usedOptions = [];
		$('option').attr('disabled', false);
		$('select').each(function()
		{
			var value = $(this).val();
			usedOptions[$(this).attr('id')] = value;
		});
		var value = '';
		for (key in usedOptions)
		{
			//console.log(value+"="+usedOptions+"["+key+"]");
			//console.log('#' + key + ' option[value=' + value + ']');
			value = usedOptions[key];
			$('option[value="' + value + '"]').attr('disabled', true);
			$('#' + key + ' option[value="' + value + '"]').attr('disabled', false);
		}
		//console.log(usedOptions);
	}
function total_quantity(){
	var top_total_sum = 0;
	var bottom_total_sum = 0;
	var dress_total_sum = 0;
	var calculated_total_sum=0;
	var top_textbox_value=0;
	var bottom_textbox_value=0;
	var dress_textbox_value=0;
	var top_total_sum1 = 0;
	var bottom_total_sum1 = 0;
	var dress_total_sum1 = 0;
	var angle_len = $("[name='angle[]']:checked").length;

		var cat_id_arr = $(".cat_select").map(function () {
			return this.value;
		});
		cat_id_arr=cat_id_arr.get();
		console.log(cat_id_arr);
		var price="<?php echo $price ?>";
		var i=0;
		var j=1;
	$("#addCategory .txt_quty").each(function () {
		var get_textbox_value1 = $(this).val();
						if ($.isNumeric(get_textbox_value1)) {
							calculated_total_sum += parseFloat(get_textbox_value1);
						}
		
		// if ($.isNumeric(get_textbox_value)) {
		
				if(cat_id_arr[i]=="Top")
				{	
					var top_textbox_value = $("#quty_"+j).val();
					var angle_Top_len = $(".Top:checked").length;

					top_total_sum = (parseFloat(top_textbox_value)*angle_Top_len)*price;
					top_total_sum1 = (parseFloat(top_textbox_value))*price;
				}
				if(cat_id_arr[i]=="Bottom")
				{	
					var bottom_textbox_value = $("#quty_"+j).val();
					var angle_bottom_len = $(".Bottom:checked").length;
					bottom_total_sum = (parseFloat(bottom_textbox_value)*angle_bottom_len)*price;
					bottom_total_sum1 = (parseFloat(bottom_textbox_value))*price;
				}
				if(cat_id_arr[i]=="Dress")
				{	
					var dress_textbox_value = $("#quty_"+j).val();
					var angle_dress_len = $(".Dress:checked").length;
					dress_total_sum = (parseFloat(dress_textbox_value)*angle_dress_len)*price;
					dress_total_sum1 = (parseFloat(dress_textbox_value))*price;
				}
			i=i+1;
			
		//}                  
	});
	$("#lbl_qty").html(calculated_total_sum);


	//alert((calculated_total_sum+"*"+angle_len)+"*"+price);
	if(angle_len==0)
	{
		$("#lbl_total").html(top_total_sum1+bottom_total_sum1+dress_total_sum1);
		$("#total_price").val(top_total_sum1+bottom_total_sum1+dress_total_sum1);
	}
	else{
		$("#lbl_total").html(top_total_sum+bottom_total_sum+dress_total_sum);
		$("#total_price").val(top_total_sum+bottom_total_sum+dress_total_sum);
	}
}
	$(document).ready(function() {
		var max_category = 3;
		var wrap         = $('#addCategory');
		var add_category = $('#btnAddCategory');
		<?php 	
		if(!empty($category_arr)){
			$x_value=$cat_count;
			?>

			var formData = $('form').serialize();
			//console.log(formData);
			select_disable();
			$.ajax({
					type:'POST',
					url:'<?php echo PIQUIC_AJAX_URL ?>ajaxmodel.php', 
					data:formData, 
					//data: {'genid':gender_val},
					success: function(results) {
						$("#model").html(results);
					}
				});
				$.ajax({
					type:'POST',
					url:'<?php echo PIQUIC_AJAX_URL ?>ajax_angles_size.php', 
					data:formData, 
					//data: {'cat_val':cat_val,'gender_id':gender_id},
					success: function(results) {
						$("#angles_size").html(results);
						total_quantity();
					}
				});


			<?php
		}else{
			$x_value=1;
		}?>
		var x = "<?php echo  $x_value; ?>"; y = "<?php echo  $x_value; ?>";
		//alert(x);
		var usedOptions = [];
		var gender_id = $("input[name='gender']:checked"). val();
		var formData = $('form').serialize();
		//select_disable();
		$(document).on('click', 'select', function()
		{
			//alert("click");
			$(this).find('option[value="' + $(this).val() + '"]').attr('disabled', false);
		});
		$("#cat_"+y).on('change', function(){
			var gender_id = $("input[name='gender']:checked"). val();
			//var gender_id=$("#gender").val();
			var formData = $('form').serialize();
			//console.log(formData);
			if($(this).val()!="")
			{
			 	var cat_val=$(this).val();
				//alert(cat_val);
				if(cat_val=="Top")
				{	
					$("#lbl_cat"+y).text("Top, ");
				}
				if(cat_val=="Bottom")
				{	
					$("#lbl_cat"+y).text("Bottom, ");
				}
				if(cat_val=="Dress")
				{	
					$("#lbl_cat"+y).text("Dress, ");
				}
				$.ajax({
					type:'POST',
					url:'<?php echo PIQUIC_AJAX_URL ?>ajax_angles_size.php',
					// contentType: false,
			    // processData: false,
			    data:formData, 
					//data: {'cat_val':cat_val,'gender_id':gender_id},
					success: function(results) {
						$("#angles_size").html(results);
					}
				});
			}
			var thisVal = $(this).val();
			select_disable();
			$(this).find('option[value="' + thisVal + '"]').attr('disabled', false);
		});

		$(add_category).on('click', function(){
			// e.preventDefaults();
			var selectedValue = $("#cat_"+x).val();
			//alert(selectedValue);
			if(x < max_category) {
				x++;
				//console.log(x);
				$(wrap).append('<div id="category_' + x + '" class="form-row"> <div class="form-group col-4"> <label for="cat' + x + '">Category</label> <select class="custom-select cat_select" id="cat_' + x + '" name="cat[]"> <option value="">Select Category</option> <option value="Top">Top</option> <option value="Bottom">Bottom</option> <option value="Dress">Dress</option> </select> </div> <div class="form-group col-4"> <label for="quty' + x + '">Quantity</label> <input type="text" class="form-control txt_quty" id="quty_' + x + '" name="quty[]" placeholder="Enter Quantity" > </div> <span class="cate_remv pt-4 pl-2"><i class="far fa-times-circle lead text-danger pt-2"></i></span> </div>');
				var thisVal = $(this).val();
				select_disable();
				$(this).find('option[value="' + thisVal + '"]').attr('disabled', false);

				$("#cat_"+x).on('change', function(){
					//alert("#cat_"+x);
					var formData = $('form').serialize();
			  		//console.log(formData);
						//var gender_id = $("input[name='gender']:checked"). val();
						if($(this).val()!="")
						{
							var cat_val=$(this).val();
							//alert(cat_val);
							if(cat_val=="Top")
							{	
								$("#lbl_cat"+x).text("Top, ");
							}
							if(cat_val=="Bottom")
							{	
								$("#lbl_cat"+x).text("Bottom, ");
							}
							if(cat_val=="Dress")
							{	
								$("#lbl_cat"+x).text("Dress, ");
							}
							$.ajax({
								type:'POST',
								url:'<?php echo PIQUIC_AJAX_URL ?>ajax_angles_size.php', 
								data:formData, 
								//data: {'cat_val':cat_val,'gender_id':gender_id},
								success: function(results) {
									$("#angles_size").html(results);
								}
							});
						}
						var thisVal = $(this).val();
						select_disable();
						$(this).find('option[value="' + thisVal + '"]').attr('disabled', false);

					});
			} else {
				// $(add_category).addClass('d-none');
				$('#maxMsg').removeClass('d-none');
			}
		});


		$(wrap).on('click',".cate_remv", function(e){
			e.preventDefault();
			var cat_id_arr=[];
			var cat_id_arr1=[];
			var cat_id_arr = $(".cat_select").map(function () {
				return this.id;
			});
			cat_id_arr=cat_id_arr.get();
			
			$(this).parent('div').remove(); x--;

			var cat_id_arr1 = $(".cat_select").map(function () {
				return this.id;
			});

			cat_id_arr1=cat_id_arr1.get();
			var cat_id = cat_id_arr.filter(function(obj) { return cat_id_arr1.indexOf(obj) == -1; });
			
			for (i = 0; i < cat_id_arr.length; i++)
			{
				var select_id=cat_id_arr[i].slice(4);
				if(cat_id_arr[i]==cat_id[0])
				{	
					$("#lbl_cat"+select_id).text("");
				}
			}
			var formData = $('form').serialize();
			$.ajax({
				type:'POST',
				url:'<?php echo PIQUIC_AJAX_URL ?>ajax_angles_size.php',
				// contentType: false,
				// processData: false,
				data:formData, 
				//data: {'cat_val':cat_val,'gender_id':gender_id},
				success: function(results) {
					$("#angles_size").html(results);
					total_quantity();
				}
			});
			
		});

		$('.gender').on('change', function(){
			if($(this).val()!="")
			{
				var gender_val=$(this).val();
				var formData = $('form').serialize();
				//alert(gender_val);
				if(gender_val=="1")
				{	
					$("#lbl_gender").text("Male");
				}
				if(gender_val=="2")
				{	
					$("#lbl_gender").text("Female");
				}
				if(gender_val=="4")
				{	
					$("#lbl_gender").text("Boy");
				}
				if(gender_val=="6")
				{	
					$("#lbl_gender").text("Girl");
				}
				$.ajax({
					type:'POST',
					url:'<?php echo PIQUIC_AJAX_URL ?>ajaxmodel.php', 
					data:formData, 
					//data: {'genid':gender_val},
					success: function(results) {
						$("#model").html(results);
					}
				});
				$.ajax({
					type:'POST',
					url:'<?php echo PIQUIC_AJAX_URL ?>ajax_angles_size.php', 
					data:formData, 
					//data: {'cat_val':cat_val,'gender_id':gender_id},
					success: function(results) {
						$("#angles_size").html(results);
						total_quantity();
					}
				});
			}

		});
		$("#addCategory").on('input', '.txt_quty', function () {
			total_quantity();
		});

		$("#angles_size").on('click', function () {
			total_quantity();
		});
		$('.d_status').on('change', function(){
			if($(this).val()!="")
			{
				var d_status_val=$(this).val();
					//alert(d_status_val);
					if(d_status_val=="1")
					{	
						$("#lbl_d_status").text("Standard (6-7 days after receiving your products).");
						<?php
						$query1 = $this->db->get_where('tbl_settings', array('setting_key'=>'STANDARD_DELIVERY_RATE'));
						//$query = $this->db->get("tbl_upload_img");
						$result1=$query1->result_array(); 
						$d_price=$result1[0]['setting_value'];
						?>
						var d_price="<?php echo $d_price ?>";

					}
					if(d_status_val=="2")
					{	
						$("#lbl_d_status").text("Express (2-3 days after receiving your products).");
						<?php
						$query1 = $this->db->get_where('tbl_settings', array('setting_key'=>'EXPRESS_DELIVERY_RATE'));
						//$query = $this->db->get("tbl_upload_img");
						$result1=$query1->result_array(); 
						$d_price=$result1[0]['setting_value'];
						?>
						var d_price="<?php echo $d_price ?>";
					}
					var calculated_total_sum = 0;
					var angle_len = $("[name='angle[]']:checked").length;
					var price="<?php echo $price ?>";
					$("#addCategory .txt_quty").each(function () {
						var get_textbox_value = $(this).val();
						if ($.isNumeric(get_textbox_value)) {
							calculated_total_sum += parseFloat(get_textbox_value);
						}                  
					});
					if(d_price==0)
					{
						total_quantity();
					}
					else{
						$("#lbl_total").html(((calculated_total_sum*angle_len)*d_price)*price);
						$("#total_price").val(((calculated_total_sum*angle_len)*d_price)*price);
					}

				}
			});
	});

$(function () {
	//$("#booking_form").validate();
	//$("input[id*=cat_]").rules("add", "required");
	$("#booking_form").validate({
		rules: {
			gender:{
				required:true,
			},
			m_status:{
				required:true,
			},
			'cat[]':"required", 
			'quty[]':"required",
			'angle[]': { 
				required: true,
				minlength: 1
			},

		},
		messages:
		{
			gender:
			{
				required:"Please select a gender"
			},
			m_status:
			{
					required:"Please select a model"
			},
			'cat[]':{
				required:"Please Select any Category on this list"         
		 	},
			'quty[]':{
				required:"Enter some Quantity for your category"
			},
			'angle[]':
			{
				required:"Please select a angle "
			},
		},
		errorPlacement: function(error, element) {
			if (element.attr("name") == "gender") {
				error.insertAfter("#gender_validate");
			}
			else if (element.attr("name") == "m_status") {
				error.insertAfter("#m_status_validate");
			}
			else if (element.attr("name") == "angle[]") {
				error.insertAfter("#angle_validate");
			}
			else { 
				error.insertAfter( element );
			}

		},
		submitHandler: function (form) {
    	//document.booking_form.submit();
    	//alert("<?php echo base_url();?>front/booking/add_book");
    	var tmb_book_id="<?php echo $book_id ;?>";
    	if(tmb_book_id=="")
    	{
    		var formData = new FormData(form);
    		$.ajax({
    		method:'post',
    		url: "<?php echo base_url();?>front/booking/add_book", 
    		contentType: false,
    		processData: false,
    		data:formData,
    		success: function(result){
    			if(result!=""){
    				// alert(result);
    				window.location.href = "<?php echo base_url();?>revbook/"+result;
    				//document.booking_form.submit();
          		//$("#success_message").show();
          }
          else{
  				// $("#error_message").show();
  				}	       
  			}
      });
    	}
    	else{
    		var formData = new FormData(form);
    	$.ajax({
    		method:'post',
    		url: "<?php echo base_url();?>front/booking/add_book/"+tmb_book_id, 
    		contentType: false,
    		processData: false,
    		data:formData,
    		success: function(result){
    			if(result!=""){
    				// alert(result);
    				window.location.href = "<?php echo base_url();?>revbook/"+result;
    				//document.booking_form.submit();
          		//$("#success_message").show();
          }
          else{
  				// $("#error_message").show();
  				}	       
  			}
      });
    	}
    
    }

  });
	});
</script>

