<?php
session_start();
error_reporting(0);
$data['page_title'] = "process list";

$this->load->view('front/includes/header',$data);
// $this->load->view('front/includes/menu');
$this->load->view('front/includes/nav');
?>

<style type="text/css">
	span.details-control {
    background: url('website-assets/details_open.png') no-repeat center center;
    cursor: pointer;
}
tr.details span.details-control {
    background: url('website-assets/details_close.png') no-repeat center center;
}


</style>
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/bs4/dt-1.10.18/datatables.min.css"/>
<div class="container-fluid">
	<div class="row mb-4">



<div class="container-fluid">
	<div class="row mb-4">

		<?php //$this->load->view('front/includes/sidebar'); ?>

		<!-- <div class="col-12 col-sm-12 col-md-9 col-lg-9 col-xl-9 border"> -->
		<div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12 border mainscroll">

			<div class="container pt-3">
				<div class="row mb-4">
					<div class="col-6 col-sm-6 col-md-6 col-lg-6 col-xl-6">
						<h1>Processing List</h1>
					</div>

					<!-- <div class="col-6 col-sm-6 col-md-6 col-lg-6 col-xl-6">
						<div class="text-right w-100">
							<span class="text-black-50 pr-2" data-toggle="collapse" data-target="#filter-it"><i class="fas fa-sliders-h fa-2x"></i></span>
						</div>
					</div> -->
				</div>

				<!-- <div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
					<div class="bg-piquic full-w" style="min-height: .5px;">
						<div id="filter-it" class="row text-white collapse">
							<div class="col-12 col-sm-12 col-md-11 col-lg-11 col-xl-11 pl-4">
								<div class="row">
									<div class="col-12 col-sm-12 col-md-3 col-lg-3 col-xl-3">
										<select class="custom-select navSlct mt-2 text-white" id="slct_model">
											<option class="text-piquic" selected>Model</option>
											<option class="text-piquic" value="0">Not Selected</option>
											<option class="text-piquic" value="1">Selected</option>
										</select>
									</div>

									<div class="col-12 col-sm-12 col-md-3 col-lg-3 col-xl-3">
										<select class="custom-select navSlct mt-2 text-white" id="slct_pose">
											<option class="text-piquic" selected>Pose</option>
											<option class="text-piquic" value="0">Not Selected</option>
											<option class="text-piquic" value="1">Selected</option>
										</select>
									</div>

									<div class="col-12 col-sm-12 col-md-3 col-lg-3 col-xl-3">
										<select class="custom-select navSlct mt-2 text-white" id="slct_top">
											<option class="text-piquic" selected>Top</option>
											<option class="text-piquic" value="0">Not Selected</option>
											<option class="text-piquic" value="1">Selected</option>
										</select>
									</div>

									<div class="col-12 col-sm-12 col-md-3 col-lg-3 col-xl-3">
										<select class="custom-select navSlct mt-2 text-white" id="slct_btm">
											<option class="text-piquic" selected>Bottom</option>
											<option class="text-piquic" value="0">Not Selected</option>
											<option class="text-piquic" value="1">Selected</option>
										</select>
									</div>

									<div class="col-12 col-sm-12 col-md-3 col-lg-3 col-xl-3">
										<select class="custom-select navSlct mt-2 text-white" id="slct_shoe">
											<option class="text-piquic" selected>Shoes</option>
											<option class="text-piquic" value="0">Not Selected</option>
											<option class="text-piquic" value="1">Selected</option>
										</select>
									</div>

									<div class="col-12 col-sm-12 col-md-3 col-lg-3 col-xl-3">
										<select class="custom-select navSlct mt-2 text-white" id="slct_acc">
											<option class="text-piquic" selected>Accessories</option>
											<option class="text-piquic" value="0">Not Selected</option>
											<option class="text-piquic" value="1">Selected</option>
										</select>
									</div>

									<div class="col-12 col-sm-12 col-md-3 col-lg-3 col-xl-3">
										<select class="custom-select navSlct mt-2 text-white" id="slct_mkup">
											<option class="text-piquic" selected>Makup</option>
											<option class="text-piquic" value="0">Not Selected</option>
											<option class="text-piquic" value="1">Selected</option>
										</select>
									</div>
								</div>
							</div>

							<div class="col-12 col-sm-12 col-md-1 col-lg-1 col-xl-1 pt-3 pl-4">
								<span data-toggle="collapse" data-target="#filter-it"><i class="fas fa-times"></i></span>
							</div>
						</div>
					</div>
				</div> -->

				<div class="row my-4">					
					<div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
						<div class="table-responsive">
							<table id="pro-cess" class="table table-hover border-bottom table-bordered">
								<thead class="thead-light">
									<tr class="text-success">
										<!-- <th></th> -->
										<th scope="col" data-orderable="false"></th>
										<th scope="col">SKU</th>
										<th scope="col">Category</th>
										<th scope="col">Stage</th>
										<th scope="col">Status</th>
									</tr>
								</thead>

								<tbody>
									<!-- <tr>
										<th scope="row"><span class="text-info"><i class="fas fa-sync-alt"></i></span></th>
										<td>112398JK389</td>
										<td>Shirt</td>
										<td>Style</td>
										<td>Styling Pending</td>
									</tr>
									<tr>
										<th scope="row"><span class="text-danger"><i class="fas fa-exclamation-circle"></i></span></th>
										<td>56897GT8998</td>
										<td>T-Shirt</td>
										<td>Proccessing</td>
										<td class="text-danger">Warning - 2 images rejected</td>
									</tr>
									<tr>
										<th scope="row"><span class="text-danger"><i class="fas fa-exclamation-circle"></i></span></th>
										<td>56897GT8998</td>
										<td>T-Shirt</td>
										<td>Proccessing</td>
										<td class="text-danger">Warning - Payment Pending</td>
									</tr>

									<tr class="table-danger text-danger">
										<th scope="row"><i class="fas fa-exclamation-circle"></i></th>
										<td>56897GT8998</td>
										<td>T-Shirt</td>
										<td>Proccessing</td>
										<td>Warning - 2 images rejected</td>
									</tr>
									<tr class="table-danger text-danger">
										<th scope="row"><i class="fas fa-exclamation-circle"></i></th>
										<td>56897GT8998</td>
										<td>T-Shirt</td>
										<td>Proccessing</td>
										<td>Warning - Payment Pending</td>
									</tr> -->
								</tbody>
							</table>
						</div>
					</div>

				</div>
			</div>
		</div>
	</div>
</div>
<?php
$this->load->view('front/includes/footer');
?>



<script type="text/javascript" src="https://cdn.datatables.net/v/bs4/dt-1.10.18/datatables.min.js"></script>

<script type="text/javascript">
	var table  = $('#pro-cess').DataTable( {
        "pageLength": 10,
		"order": [[ 1, "desc" ]],
		"lengthMenu": [ [10,25, 50, 100], [10,25, 50, 100] ],
		"oLanguage": {
			"sSearch": "Search SKU: "
		},
		"columnDefs": [ {
			"targets": 0,
			"className": 'text-center lead'
		}, {
			"targets": 1,
			"orderable": true,
			"className": 'lead'
		}, {
			"targets": 2,
			"orderable": true,
			"className": 'lead'
		}, {
			"targets": 3,
			"orderable": true,
			"className": 'lead'
		}, {
			"targets": 4,
			"orderable": true,
			"className": 'lead'
		} ],
		"ajax": {
		"url": "<?php echo base_url();?>front/processing/processing_list",
		"type": "GET"
	    },
	     rowCallback: function (row, data) {
	     	
             var firstColumnCheck=$.trim(data[0]);
             console.log(firstColumnCheck);
             if(firstColumnCheck=='<span class="text-danger"><i class="fas fa-exclamation-circle"></i></span>')
             {
             	$(row).addClass('table-danger text-danger');
             }
             else
             {
             	$(row).removeClass('table-danger text-danger');
             }

             $(row).addClass(' details-control');
	     	 

        /*if ( data.cashflow.manual > 0 || data.cashflow.additional_repayment > 0 ) {
            $(row).addClass('fontThick');
        }
        //if it is not the summation row the row should be selectable
        if ( data.cashflow.position !== 'L' ) {
            $(row).addClass('selectRow');
        }*/
    }




    
	} );




/*
function format ( d ) {
    return 'Full name: '+d.first_name+' '+d.last_name+'<br>'+
        'Salary: '+d.salary+'<br>'+
        'The child row can contain any data you wish, including links, images, inner tables etc.';
}

 // Array to track the ids of the details displayed rows
    var detailRows = [];
 
    $('#pro-cess tbody').on( 'click', 'tr td.details-control', function () {
        var tr = $(this).closest('tr');
        var row = dt.row( tr );
        var idx = $.inArray( tr.attr('id'), detailRows );
 
        if ( row.child.isShown() ) {
            tr.removeClass( 'details' );
            row.child.hide();
 
            // Remove from the 'open' array
            detailRows.splice( idx, 1 );
        }
        else {
            tr.addClass( 'details' );
            row.child( format( row.data() ) ).show();
 
            // Add to the 'open' array
            if ( idx === -1 ) {
                detailRows.push( tr.attr('id') );
            }
        }
    } );
 
    // On each draw, loop over the `detailRows` array and show any child rows
    dt.on( 'draw', function () {
        $.each( detailRows, function ( i, id ) {
            $('#'+id+' td.details-control').trigger( 'click' );
        } );
    } );

*/


/* Formatting function for row details - modify as you need */
function format ( d ) {
    // `d` is the original data object for the row
/*    return '<table cellpadding="5" cellspacing="0" border="0" style="padding-left:50px;">'+
        '<tr>'+
            '<td>Full name:</td>'+
            '<td>'+d.name+'</td>'+
        '</tr>'+
        '<tr>'+
            '<td>Extension number:</td>'+
            '<td>'+d.extn+'</td>'+
        '</tr>'+
        '<tr>'+
            '<td>Extra info:</td>'+
            '<td>And any further details here (images etc)...</td>'+
        '</tr>'+
    '</table>';*/
    console.log(d);
     
     var style_str=d[1];
     var style_arr=style_str.split("@@@@@@");
     var style_id=$.trim(style_arr[1]);

     //alert(style_id);
    var remote = $.ajax({
			type:'POST',
			url:'<?php echo PIQUIC_AJAX_URL ;?>/process-details.php',
			data: {'style_id':style_id},
			async: false
			}).responseText;	

    


            var tab_str= '<table cellpadding="5" cellspacing="0" border="0" style="padding-left:50px;">'+
        '<tr>'+
            '<td>Stage:</td>'+
            '<td>'+d[3]+'</td>'+
        '</tr>'+
        '<tr>'+
            '<td>Comments:</td>'+
            '<td>'+d[4]+'</td>'+
        '</tr>'+
        '<tr><td colspan=4>'+remote+'</td></tr>';


        if(d[4].indexOf("Payment Pending")!='-1')
        {
        	   tab_str+='<tr>'+
            '<td colspan="2">Payment Pending</td>'+
        '</tr>'+
        '<tr>'+
            '<td colspan="2"><a href="<?php echo base_url();?>plans">Add credits</a> to process your images</td>'+
        '</tr>';
        }



   tab_str+='</table>';

    return tab_str;
}


  // Add event listener for opening and closing details
    $('#pro-cess tbody').on('click', 'tr.details-control', function () {

    	
        var tr = $(this).closest('tr');
        var row = table.row( tr );
 
        if ( row.child.isShown() ) {
            // This row is already open - close it
            row.child.hide();
            tr.removeClass('shown');
        }
        else {
            // Open this row

            //console.log(row.data());
            row.child( format(row.data()) ).show();
            tr.addClass('shown');
        }
    } );



</script>