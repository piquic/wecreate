<?php
$data['page_title'] = "review booking";

$this->load->view('front/includes/header',$data);
if ($this->session->userdata('front_logged_in')) {
	$session_data = $this->session->userdata('front_logged_in');
	$user_id = $session_data['user_id'];
	$user_name = $session_data['user_name'];
	$_SESSION['user_id']=$user_id;
	$_SESSION['user_name']=$user_name;
}
else
{
	$user_id = '';
	$user_name = ''; 
	$_SESSION['user_id']=$user_id;
	$_SESSION['user_name']=$user_name;
}

// $this->load->view('front/includes/menu');
$this->load->view('front/includes/nav');


$order_id=$booking_data[0]['order_id'];
$gender=$booking_data[0]['gender'];
$category=$booking_data[0]['category'];
$quty=$booking_data[0]['quty'];
$total_quty="";
if(!empty($quty))
{
	$total_quty=array_sum(array_filter(explode(",", $quty)));
}

$angle=$booking_data[0]['angle'];
$m_status=$booking_data[0]['m_status'];
$d_status=$booking_data[0]['d_status'];
$notes=$booking_data[0]['notes'];
$total_price=$booking_data[0]['total_price'];
?>

<div class="container-fluid">
	<div class="row">

		<?php //$this->load->view('front/includes/sidebar'); ?>

		<!-- <div class="col-12 col-sm-12 col-md-9 col-lg-9 col-xl-9 border mainscroll"> -->
			<div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12 border mainscroll">
				<div class="container pt-3">
					<div class="row mb-4">
						<div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
							<h1>Review your booking</h1>
						</div>
					</div>
				</div>

				<div class="container pt-3">
					<div class="row mb-4">
						<div class="col-12 col-sm-12 col-md-8 col-lg-8 col-xl-8">
							
							<div class="card mb-3">
								<div class="card-body">
									<h5 class="card-title">
										<div class="d-flex justify-content-between">
											<span>Booking Details</span>
											<a class="btn btn-piquic btn-sm" href="<?php echo base_url();?>booking/<?php echo $book_id ?>">&emsp;Edit&emsp;</a>
										</h5>
										<hr>
										<p class="card-text">
											<i class="fas fa-check text-piquic"></i>&emsp;<b>Gender:</b>
											<?php 
											if($gender=="1"){
												echo "Male";
											}
											if($gender=="2"){
												echo "Female";
											}
											if($gender=="4"){
												echo "Boy";
											}
											if($gender=="6"){
												echo "Girl";
											}

											?>
											<br><br><i class="fas fa-check text-piquic"></i>&emsp;<b>Category:</b>&nbsp;<?php echo trim($category,',');?>
											<br><br><i class="fas fa-check text-piquic"></i>&emsp;<b>Qty:</b>&nbsp;<?php echo $total_quty; ?>
											<br><br><i class="fas fa-check text-piquic"></i>&emsp;<b>Delivery:</b>&nbsp;<?php 
											if($d_status=="1"){
												echo "Standard (6-7 days after receiving your products).";
											}
											if($d_status=="2"){
												echo "Express (2-3 days after receiving your products).";
											}
											
											?>&nbsp;
											<!-- <a href="<?php echo base_url();?>booking/<?php echo $book_id ?>">Change</a> -->
										</p>
									</div>
								</div>

								<div class="card mb-3">
									<div class="card-body">
										<h5 class="card-title">Delivery Address</h5>
										<hr>
										<p class="card-text">
											Deliver your products samples to the following address:
										</p>
										<p class="card-text" id="address">
											<b>Order ID - <?php echo $order_id; ?></b><br><br>
											<b>Moksha Designs - Piquic Studios</b>
											<br>Sultan Sadan 1, Westend Marg Lane 3,
											<br>Saidulajab, Delhi - 110030
											<br>India
										</p>
										<input class="btn btn-outline-secondary mt-3 px-5" id="btnPrintLabel" type="button" value="PRINT DELIVERY LABEL" onclick="printDiv()">  
										
									</div>
								</div>
							</div>
							
							<div class="col-12 col-sm-12 col-md-4 col-lg-4 col-xl-4">
								<div id="success_message" style="display:none;">
									<div class="alert alert-success">
										<strong>Your Booking has beed Placed</strong>.
									</div>
								</div>
								<div id="error_message" style="display:none;">
									<div class="alert alert-danger">
										<strong>Something wrong in your Booking</strong>.
									</div>
								</div>
								<div class="card mb-3">
									<div class="card-body">
										<p class="card-text">
											<span class="d-flex justify-content-between">
												<span class="h5">Total:</span>

												<span class="h5">$ <?php echo $total_price ?> </span>
											</span>
										</p>
										<button class="btn btn-piquic w-100 mt-3" id="btnPlaceOrder">PLACE YOUR ORDER</button>
									</div>
								</div>						
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>

		<?php $this->load->view('front/includes/footer'); ?>

		<script type="text/javascript">
			$(document).ready(function() {
				$("#btnPlaceOrder").click(function () {
					var tmb_book_id="<?php echo $book_id; ?>";
					var order_id="<?php echo $order_id; ?>";
					var user_id="<?php echo $user_id; ?>";
					var gender="<?php echo $gender; ?>";
					var category="<?php echo $category; ?>";
					var quty="<?php echo $quty; ?>";
					var angle="<?php echo $angle; ?>";
					var m_status="<?php echo $m_status; ?>";
					var d_status="<?php echo $d_status; ?>";
					var notes="<?php echo $notes; ?>";
					var total_price='<?php echo $total_price;?>';
					$("#loader").show();
					$.ajax({
						method:'post',
						url: "<?php echo base_url();?>front/revbook/add_book", 
						data:"&tmb_book_id="+tmb_book_id+"&user_id="+user_id+"&order_id="+order_id+"&gender="+gender+"&category="+category+"&quty="+quty+"&angle="+angle+"&m_status="+m_status+"&d_status="+d_status+"&notes="+notes+"&total_price="+total_price,
          // contentType: false,
          // processData: false,
          // data:formData,
          success: function(result){
          	if(result=="1"){
          		$("#loader").hide();
          		window.location = "<?php echo base_url();?>booksuccess" ;

          		// $("#success_message").show();
          		// $("#error_message").hide();
          	}
          	else{
          		window.location = "<?php echo base_url();?>bookfailure" ;
          	}	       
          }
          // error:function(result){
          // 	exit();
          // }
       });
				});
			});

			function printDiv() { 
				var divContents = document.getElementById("address").innerHTML; 
				var a = window.open('Delivery Label', '', 'height=500, width=500'); 
				a.document.write('<html>'); 
				a.document.write('<body>');  
				a.document.write(divContents); 
				a.document.write('<br><br>'); 
				a.document.write(divContents); 
				a.document.write('<br><br>'); 
				a.document.write(divContents); 
				a.document.write('<br><br>'); 
				a.document.write(divContents); 
				a.document.write('</body></html>'); 
				a.document.close(); 
				a.print(); 
			} 

		</script>