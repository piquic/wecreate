<?php
$data['page_title'] = "booking";

$this->load->view('front/includes/header',$data);
// $this->load->view('front/includes/menu');
$this->load->view('front/includes/nav');

?>

<div class="container-fluid">
	<div class="row mb-4">

		<?php $this->load->view('front/includes/sidebar'); ?>

		<div class="col-12 col-sm-12 col-md-9 col-lg-9 col-xl-9 border">
			<div class="container pt-3">
				<div class="row mb-4">
					<div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
						<h1>Book your Piquic shoot</h1>
					</div>
				</div>
			</div>

			<div class="container pt-3">
				<div class="row mb-4">
					<div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
						<form class="form form-horizontal has-validation-callback"  id="booking_form" method="post" action="revbook">  
						<input type="hidden" name="user_id" id="user_id" value="<?php echo $user_id ?>">	
						<div id="divGender" class="pl-3 pb-5 divGender">
							<h3>GENDER</h3>
							<div class="custom-control custom-radio custom-control-inline">
								<input type="radio" id="male" name="gender" class="custom-control-input gender" value="1" >
								<label class="custom-control-label" for="male">Male</label>
							</div>
							<div class="custom-control custom-radio custom-control-inline">
								<input type="radio" id="female" name="gender" class="custom-control-input gender" value="2" >
								<label class="custom-control-label" for="female">Female</label>
							</div>
							<div class="custom-control custom-radio custom-control-inline">
								<input type="radio" id="boy" name="gender" class="custom-control-input gender" value="3" >
								<label class="custom-control-label" for="boy">Boy</label>
							</div>
							<div class="custom-control custom-radio custom-control-inline">
								<input type="radio" id="girl" name="gender" class="custom-control-input gender" value="4" >
								<label class="custom-control-label" for="girl">Girl</label>
							</div>
							<span id="gender_validate"></span>
						</div>

						<div id="divCategory" class="pl-3 pb-5">
							<h3>CATEGORY</h3>

							<div id="addCategory">

								<div id="category_1" class="form-row">
									<div class="form-group col-4">
										<label for="cat_1">Category</label>
										<select class="custom-select cat_select" id="cat_1" name="cat[]" >
											<option value="">Select your Category</option>
											<option value="Top">Top</option>
											<option value="Bottom">Bottom</option>
											<option value="Dress">Dress</option>
										</select>
									</div>

									<div class="form-group col-4">
										<label for="quty_1">Quantity</label>
										<input type="text" class="form-control txt_quty" id="quty_1" name="quty[]" placeholder="Enter your Quantity" >
									</div>
								</div>

							</div>

							<div id="btnAddCategory">
								<i class="fas fa-plus-square"></i>&emsp;Add Category
							</div>

							<div id="maxMsg" class="d-none text-center">
								<div class="alert alert-warning" role="alert">
									You have selected the maximum no. of category.
								</div>								
							</div>
						</div>

						<div id="divModel" class="pl-3 pb-5">
							<h3>MODEL</h3>

							<div class="form-row pb-4">
								<div class="col-12 col-md-4">
									<div class="custom-control custom-radio">
										<input type="radio" id="rdPSE" name="m_status" class="custom-control-input" value="1">
										<label class="custom-control-label" for="rdPSE">Let "Piquic Style Expert" select my models & style.</label>
									</div>
								</div>
								<div class="col-12 col-md-1">&nbsp;</div>
								<div class="col-12 col-md-4">
									<div class="custom-control custom-radio">
										<input type="radio" id="rdSelf" name="m_status" class="custom-control-input" value="2">
										<label class="custom-control-label" for="rdSelf">I want to select my models and styling once my apparels gets digitized.</label>
									</div>
								</div>
								<span id="m_status_validate"></span>
							</div>

							<h6><u>Preveiw models</u></h6>

													<div class="form-row pb-4">
								<div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
									<div class="row">
												<?php 	
						$CI = &get_instance();
						$this->db2 = $CI->load->database('another_db', TRUE);					
						$modquery = $this->db2->query("SELECT * FROM `tbl_model` where status='1'");
						$modresult=$modquery->result();					
							foreach($modresult as $modrow){	
							$m_id= $modrow->m_id;
							$mod_name=$modrow->mod_name;
							$mod_img=$modrow->mod_img;
							$skintone=$modrow->skintone;

							$arr=explode(",",$skintone);
			
		    	$arrjpg = array();

			    foreach ($arr as $value) {
				     if(strpos($value, '.jpg')){
					    array_push($arrjpg, $value);
				    }
			      }


							?>
							<div class="col-md-1 pb-1 pl-1 pr-1 pt-1">
								<img class="w-100 border" src="<?php echo PIQUIC_URL;?>images/model_image/<?php echo $mod_img; ?>">
							</div>
					
      <?php } ?>
								</div>
								</div>
							</div>
						</div>

						<div id="divImgAng" class="pl-3 pb-5">
							<h3>IMAGE ANGLES & SIZE</h3>

							<div class="form-row pb-4">
								<div class="col-3 col-md-2 mb-3">
									<div id="imgFrontAng" class="text-center">
										<img class="d-block w-100 border rounded" src="<?php echo PIQUIC_URL;?>/images/booking_thumb/front.jpg">

										<p>FRONT</p>
									</div>

									<div class="c-box">
										<div class="custom-control custom-checkbox">
											<input type="checkbox" class="custom-control-input" id="chkFrontAng" name="angle[]" value="front">
											<label class="custom-control-label" for="chkFrontAng">&nbsp;</label>
										</div>
									</div>
								</div>

								<div class="col-3 col-md-2 mb-3">
									<div id="imgBackAng" class="text-center">
										<img class="d-block w-100 border rounded" src="<?php echo PIQUIC_URL;?>/images/booking_thumb/back.jpg">

										<p>BACK</p>
									</div>

									<div class="c-box">
										<div class="custom-control custom-checkbox">
											<input type="checkbox" class="custom-control-input" id="chkBackAng" name="angle[]" value="back">
											<label class="custom-control-label" for="chkBackAng">&nbsp;</label>
										</div>
									</div>
								</div>

								<div class="col-3 col-md-2 mb-3">
									<div id="imgLeftAng" class="text-center">
										<img class="d-block w-100 border rounded" src="<?php echo PIQUIC_URL;?>/images/booking_thumb/left.jpg" value="left">

										<p>LEFT</p>
									</div>

									<div class="c-box">
										<div class="custom-control custom-checkbox">
											<input type="checkbox" class="custom-control-input" id="chkLeftAng" name="angle[]" value="left">
											<label class="custom-control-label" for="chkLeftAng">&nbsp;</label>
										</div>
									</div>
								</div>

								<div class="col-3 col-md-2 mb-3">
									<div id="imgRightAng" class="text-center">
										<img class="d-block w-100 border rounded" src="<?php echo PIQUIC_URL;?>/images/booking_thumb/right.jpg">

										<p>RIGHT</p>
									</div>

									<div class="c-box">
										<div class="custom-control custom-checkbox">
											<input type="checkbox" class="custom-control-input" id="chkRightAng" name="angle[]" value="right">
											<label class="custom-control-label" for="chkRightAng">&nbsp;</label>
										</div>
									</div>
								</div>
								<span id="angle_validate"></span>
							</div>

						</div>

						<div id="divDelivery" class="pl-3 pb-5">
							<h3>DELIVERY</h3>

							<div class="form-row pb-4">
								<div class="col-12 col-md-4">
									<div class="custom-control custom-radio">
										<input type="radio" id="rdSD" name="d_status" class="custom-control-input d_status" value="1">
										<label class="custom-control-label" for="rdSD">Standard (6-7 days after receiving your products).</label>
									</div>
								</div>
								<div class="col-12 col-md-1">&nbsp;</div>
								<div class="col-12 col-md-4">
									<div class="custom-control custom-radio">
										<input type="radio" id="rdED" name="d_status" class="custom-control-input d_status" value="2" checked>
										<label class="custom-control-label" for="rdED">Express (2-3 days after receiving your products).</label>
									</div>
								</div>
							</div>
						</div>

						<div id="divNotes" class="pl-3 pb-5">
							<div class="form-row">
								<div class="form-group col-8">
									<label for="notes" class="lblNotes">&nbsp;NOTES&nbsp; (Optional)</label>
									<textarea class="form-control" id="notes" name="notes" rows="3" ></textarea>
								</div>
							</div>
						</div>
						
						<div class="pl-3 pb-5">
							<button class="btn btn-piquic w-75" type="submit" id="btnBook" name="btnBook">CONFIRM BOOKING</button>
						</div>
	
					</form>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
 <?php 
									$total_quty=0;
										$query = $this->db->get_where('tbl_settings', array('setting_key'=>'PER_PRICE_USD'));
//$query = $this->db->get("tbl_upload_img");
$result=$query->result_array(); 
$price=$result[0]['setting_value'];
?>
<?php $this->load->view('front/includes/footer'); ?>

<script type="text/javascript">

	$(document).ready(function() {

		var max_category = 3;
		var wrap         = $('#addCategory');
		var add_category = $('#btnAddCategory');
		var x = 1; y = 1;
		$("#cat_"+y).on('change', function(){
						if($(this).val()!="")
						{
							var cat_val=$(this).val();
							//alert(cat_val);
							if(cat_val=="Top")
							{	
								$("#lbl_cat"+y).text("Top,");
							}
				        	if(cat_val=="Bottom")
							{	
								$("#lbl_cat"+y).text("Bottom,");
							}
							if(cat_val=="Dress")
							{	
								$("#lbl_cat"+y).text("Dress,");
							}
							// $(this).children('option').each(function() {
						 //        if ( $(this).val() === cat_val ) {
						 //            $(this).attr('disabled', true).siblings().removeAttr('disabled');   
						 //        }
						 //    });
						}
				    	
				    });

		$(add_category).on('click', function(){
			// e.preventDefaults();
			var selectedValue = $("#cat_"+x).val();
			//alert(selectedValue);
			if(x < max_category) {
				x++;
				$(wrap).append('<div id="category_' + x + '" class="form-row"> <div class="form-group col-4"> <label for="cat' + x + '">Category</label> <select class="custom-select cat_select" id="cat_' + x + '" name="cat[]"> <option value="">Select Category</option> <option value="Top">Top</option> <option value="Bottom">Bottom</option> <option value="Dress">Dress</option> </select> </div> <div class="form-group col-4"> <label for="quty' + x + '">Quantity</label> <input type="text" class="form-control txt_quty" id="quty' + x + '" name="quty[]" placeholder="Enter Quantity" > </div> </div>');
					$("#cat_"+x).on('change', function(){
						if($(this).val()!="")
						{
							var cat_val=$(this).val();
							//alert(cat_val);
							if(cat_val=="Top")
							{	
								$("#lbl_cat"+x).text("Top,");
							}
				        	if(cat_val=="Bottom")
							{	
								$("#lbl_cat"+x).text("Bottom,");
							}
							if(cat_val=="Dress")
							{	
								$("#lbl_cat"+x).text("Dress,");
							}
							// $(this).children('option').each(function() {
						 //        if ( $(this).val() === cat_val ) {
						 //            $(this).attr('disabled', true).siblings().removeAttr('disabled');   
						 //        }
						 //    });
				    	}
				    	
				    });
			} else {
				// $(add_category).addClass('d-none');
				$('#maxMsg').removeClass('d-none');
			}
			
		});
	
// var select = $("select");
// select.on("change", function() {
//     var selected = [];  
//     $.each(select, function(index, select) {           
//         if (select.value !== "") { selected.push(select.value); }
//     });         
//    $("option").prop("disabled", false);         
//    for (var index in selected) { $('option[value="'+selected[index]+'"]').prop("disabled", true); }
// });
		$('#imgFrontAng').on('click', function(){

			var checkMe = $('#chkFrontAng').is(":checked");

			if(checkMe == true){
				$('#chkFrontAng').prop('checked', false);
			} else {
				$('#chkFrontAng').prop('checked', true);
			}
		});

		$('#imgBackAng').on('click', function(){

			var checkMe = $('#chkBackAng').is(":checked");

			if(checkMe == true){
				$('#chkBackAng').prop('checked', false);
			} else {
				$('#chkBackAng').prop('checked', true);
			}
		});

		$('#imgLeftAng').on('click', function(){

			var checkMe = $('#chkLeftAng').is(":checked");

			if(checkMe == true){
				$('#chkLeftAng').prop('checked', false);
			} else {
				$('#chkLeftAng').prop('checked', true);
			}
		});

		$('#imgRightAng').on('click', function(){

			var checkMe = $('#chkRightAng').is(":checked");

			if(checkMe == true){
				$('#chkRightAng').prop('checked', false);
			} else {
				$('#chkRightAng').prop('checked', true);
			}
		});

		$('.gender').on('change', function(){
			if($(this).val()!="")
			{
				var gender_val=$(this).val();
				//alert(gender_val);
				if(gender_val=="1")
				{	
					$("#lbl_gender").text("Male");
				}
	        	if(gender_val=="2")
				{	
					$("#lbl_gender").text("Female");
				}
				if(gender_val=="3")
				{	
					$("#lbl_gender").text("Boy");
				}
				if(gender_val=="4")
				{	
					$("#lbl_gender").text("Girl");
				}
	    	}
	    });
		$("#addCategory").on('input', '.txt_quty', function () {
       		var calculated_total_sum = 0;
     		var price="<?php echo $price ?>";
       		$("#addCategory .txt_quty").each(function () {
	           var get_textbox_value = $(this).val();
	           if ($.isNumeric(get_textbox_value)) {
	              calculated_total_sum += parseFloat(get_textbox_value);
	              }                  
            });
              $("#lbl_qty").html(calculated_total_sum);
              $("#lbl_total").html(calculated_total_sum*price);
        });
		$('.d_status').on('change', function(){
			if($(this).val()!="")
			{
				var d_status_val=$(this).val();
				//alert(d_status_val);
				if(d_status_val=="1")
				{	
					$("#lbl_d_status").text("Standard (6-7 days after receiving your products).");
				}
	        	if(d_status_val=="2")
				{	
					$("#lbl_d_status").text("Express (2-3 days after receiving your products).");
				}
				
	    	}
	    });


	});


$(function () {
      $("#booking_form").validate({
       rules: {
        gender:{
          required:true,
        },
        m_status:{
        	required:true,
        },
        'cat[]':{
          required:true,         
        },
        'quty[]':{
        	required:true,
        },
        'angle[]': { 
           required: true,
           minlength: 1
        },
    },
    messages:
    {
        gender:
        {
            required:"Please select a gender"
        },
        m_status:
        {
            required:"Please select a model"
        },
        'cat[]':{
          required:"Please Select any Category on this list"         
        },
        'quty[]':{
        	required:"Enter some Quantity for your category"
        },
        'angle[]':
        {
            required:"Please select a angle "
        },
    },
    errorPlacement: function(error, element) {
    	 if (element.attr("name") == "gender") {
             error.insertAfter("#gender_validate");
        }
        else if (element.attr("name") == "m_status") {
             error.insertAfter("#m_status_validate");
        }
        else if (element.attr("name") == "angle[]") {
             error.insertAfter("#angle_validate");
        }
        else { 
            error.insertAfter( element );
        }
   		
  	},
    submitHandler: function (form) {
    	document.booking_form.submit();
    	//alert("<?php echo base_url();?>front/booking/add_book");

    	// var formData = new FormData(form);
    	// $.ajax({
     //      method:'post',
     //      url: "<?php echo base_url();?>front/booking/add_book", 
     //      contentType: false,
     //      processData: false,
     //      data:formData,
     //      success: function(result){
     //      	if(result=="1"){
     //      		$("#success_message").show();
     //      	}
  			// else{
  			// 	 $("#error_message").show();
  			// }	       
     //      }
     //      // error:function(result){
     //      // 	exit();
     //      // }
     //  });
   
    }

  });
});
</script>

