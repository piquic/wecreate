<?php
session_start();
$data['page_title'] = "upload image";

$this->load->view('front/includes/header',$data);
// $this->load->view('front/includes/menu');
$this->load->view('front/includes/nav');


if ($this->session->userdata('front_logged_in')) {
	$session_data = $this->session->userdata('front_logged_in');
	$user_id = $session_data['user_id'];
	$user_name = $session_data['user_name'];
	$_SESSION['user_id']=$user_id;
	$_SESSION['user_name']=$user_name;
}
else
{
	$user_id = '';
	$user_name = ''; 
	$_SESSION['user_id']=$user_id;
	$_SESSION['user_name']=$user_name;
}
?>
<!-----------poulami start-------------->
	<!-- <link rel="stylesheet" href="<?php echo base_url();?>/website-assets/folderTree/css/filetree.css" type="text/css" >
	-->
	<!-----------poulami end-------------->

	<div class="container-fluid">
		<div class="row">

			<?php
			$this->load->view('front/includes/sidebar');

			?>
			<div class="col-12 col-sm-12 col-md-9 col-lg-9 col-xl-9 border mainscroll" style="min-height: 29rem">

				<div class="tab-content" id="v-pills-tabContent">


					<div class="tab-pane fade show active" id="v-pills-zip" role="tabpanel" aria-labelledby="v-pills-zip-tab">
						<div class="container pt-3">
							<div class="row mb-4">
								<div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">

									<h1><?php echo $pagecontents[0]['page_title'] ?></h1>
									<p><?php echo $pagecontents[0]['content'] ?></p>

								</div>
							</div>

							<div class="row mb-4">

								<div class="col-12 col-sm-12 col-md-6 col-lg-6 col-xl-6">
									<div class="bg-light mb-3" style="min-height: 22rem;">
										<form action="" method="post" enctype="multipart/form-data" id="zipForm" name="zipForm">
											<div class="custom-file">
												<input type="file" class="custom-file-input" accept="zip/*" id="upld_zip" name="upld_zip" onchange="uploadFile()">
												<label class="custom-file-label text-center" for="upld_zip"><i class="fas fa-upload fa-3x"></i><br><br>Drop the ZIP file to upload or <i><u>browse</u></i>.</label>
											</div>
										</form>
									</div>
								</div>

								<div class="col-12 col-sm-12 col-md-6 col-lg-6 col-xl-6">
									<div class="w-100 mb-3">							
										<div class="row d-none" id="upmgs" >
											<div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
												<div class="bg-light d-flex justify-content-start pt-1">
													<span class="lead w-75" id="file_nm"></span>
													<input type="hidden" name="file_nm_hid" id="file_nm_hid" value="">
													<!-- <button type="button" class="btn"><i class="fas fa-times"></i></button> -->
												</div>
												<div class="progress" style="height: 2px;">
													<div class="progress-bar bg-piquic" role="progressbar" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100" id="file_pc"></div>
												</div>
											</div>
										</div>
									</div>

									<div class="w-100 mb-3">

										<div class="row pt-3">
											<div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
												<div class="alert alert-success text-center lead d-none" id="zipSuccess" role="alert">
													<i class="far fa-thumbs-up fa-2x"></i><br>
													<span id="txtSuccess"></span>
												</div>

												<div class="alert alert-danger text-center lead d-none" id="zipDanger" role="alert">
													<i class="far fa-thumbs-down fa-2x"></i><br>
													<span id="txtDanger"></span>
												</div>
											</div>
										</div>

										<div class="row pt-3  d-none" id="enlblimg">
											<div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
												<span class="btn btn-outline-secondary btn-lg w-100 " id="lblimg" onclick="labelimg();" >Label your images</span>
												<!-- <button type="button" class="btn btn-outline-secondary btn-lg w-100">Label your images</button> -->
											</div>
										</div>



									</div>
								</div>
							</div>
						</div>

					</div>


					<div class="tab-pane fade" id="v-pills-skuimg" role="tabpanel" aria-labelledby="v-pills-skuimg-tab">
						<div class="container pt-3">
							<div class="row">
								<div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
									<h1 class="w-100">Upload SKU</h1>
									<p class="text-muted">
										Supports only JPG. Max. 10 files per upload.
									</p>
								</div>
							</div>

							<div class="row">
								<div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
									<div class="pb-3 pr-3 mt-4">
										<form id="single_sku_form" name="single_sku_form" action="">
											<div class="form-row">
												<div class="form-group">
													<label for="txtSKUNew" class="lblOver text-piquic">&nbsp;Enter SKU #&nbsp;</label>
													<input type="text" class="form-control" id="txtSKUNew" name="txtSKUNew">
												</div>
											</div>


											<div class="form-row">
												<!-- <div id="upldImg" class="col-12"> -->
													<div class="col-12">

												<!-- <label for="sku_files" class="btn btn-piquic btn-block" >
													UPLOAD IMAGES
												</label>

												<input type="file" class="form-control d-none upload_sku_files fileInput css_uploadFile" id="sku_files"  name="sku_files[]" multiple  /> -->
												<div id="oneImg" class="bg-light mb-3" style="min-height: 10rem;">
													<div class="custom-file">
														<input type="file" class="custom-file-input upload_sku_files fileInput css_uploadFile" id="sku_files"  name="sku_files[]" multiple>
														<label class="custom-file-label text-center" for="sku_files"><i class="fas fa-upload fa-3x"></i><br><br>Drop the image file to upload or <i><u>browse</u></i>.</label>
													</div>
												</div>
											</div>

											<div id="skuImageUploadDiv" class="row pt-3">
												<div class='col-6 col-sm-6 col-md-12 col-lg-12 col-xl-12 d-none' id="div_ctn_img"><small><b><span id='ctn_img'></span> IMAGES UPLOADED</b></small></div>
											</div>

											<input  type='hidden'  class="form-control" id="pimg" name="pimg" value=''>

											<button type="submit" class="btn btn-piquic btn-block d-none" id="btnCretSKU" name="btnCretSKU">SAVE SKU</button>

											<!-- <button type="submit" class="btn btn-outline-piquic btn-block" id="btnUpldCancel" name="btnUpldCancel">CANCEL</button> -->
										</div>

									</form>
								</div>
							</div>
						</div>
					</div>
				</div>



				<div class="tab-pane fade show " id="v-pills-skufolder" role="tabpanel" aria-labelledby="v-pills-skufolder-tab">
					<div class="container pt-3">
						<div class="row mb-4">
							<div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">

								<h1 class="w-100">Drag SKU Folder</h1>
								<!-- <p class="text-muted">
									Supports only JPG. Max. 10 files per upload.
								</p> -->
								
							</div>
						</div>

						<div class="row mb-4">
							
							<div class="col-12 col-sm-12 col-md-6 col-lg-6 col-xl-6">
								<div class="bg-light mb-3" style="min-height: 22rem;">
									<form action="" method="post" enctype="multipart/form-data" id="zipForm" name="zipForm">
										<div class="custom-file dropzone">
											<input type="file" class="custom-file-input" accept="zip/*" id="skufolderupld_zip" name="skufolderupld_zip" onchange="uploadFile()">
											<label class="custom-file-label text-center" for="skufolderupld_zip"><i class="fas fa-upload fa-3x"></i><br><br>Drop the Folder to upload.</label>
										</div>
									</form>
								</div>
							</div>


							


							<div class="col-12 col-sm-12 col-md-6 col-lg-6 col-xl-6">
								<div class="w-100 mb-3">	

									<div class="row d-none" id="skufolderupmgs">

										<div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
											<div class="bg-light d-flex justify-content-start pt-1">
												<span class="lead w-75" id="skufolderfile_nm"></span>

												<input type="hidden" name="skufolderfile_nm_field" id="skufolderfile_nm_field" value="">
												<!-- <button type="button" class="btn"><i class="fas fa-times"></i></button> -->
											</div>
											<div class="progress" style="height: 2px;">
												<div class="progress-bar bg-piquic" role="progressbar" aria-valuenow="" aria-valuemin="0" aria-valuemax="100" id="skufolderfile_pc"></div>
											</div>
										</div>
									</div>

									<div class="row">
										<div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">

											<div  id="folderList">

												<!-- <div class="row" id="Uploaded Folder name" data-toggle="collapse" href="#Uploaded_Folder_images">
													<div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
														<span>&emsp;<i class="fas fa-folder text-warning"></i>&emsp;Uploaded Folder name</span>
													</div>
												</div>

												<div id="Uploaded_Folder_images" class="row collapse">
													<div class="col-12 col-sm-12 col-md-4 col-lg-4 col-xl-4">
														<img class="w-100 mb-3" src="temp_folder_upload/4/8907769095402/8907769095402-Back-Angles0011.JPG">
													</div>
												</div> -->

											</div>

										</div>
									</div>




								</div>


								<!-- <div class="row " >
									<div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
										<div id="foldertree_container"> </div>
										<div id="selected_file"></div>
									</div>
								</div> -->










								<div class="w-100 mb-3">

									<div class="row pt-3">
										<div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
											<div class="alert alert-success text-center lead d-none" id="skufolderzipSuccess" role="alert">
												<i class="far fa-thumbs-up fa-2x"></i><br>
												<span id="skufoldertxtSuccess"></span>
											</div>

											<div class="alert alert-danger text-center lead d-none" id="skufolderzipDanger" role="alert">
												<i class="far fa-thumbs-down fa-2x"></i><br>
												<span id="skufoldertxtDanger"></span>
											</div>
										</div>
									</div>

									<div class="row pt-3  d-none" id="skufolderenlblimg">
										<div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
											<span class="btn btn-outline-secondary btn-lg w-100 " id="skufolderlblimg" onclick="skufolderlabelimg();" >Label your images</span>
											<!-- <button type="button" class="btn btn-outline-secondary btn-lg w-100">Label your images</button> -->
										</div>
									</div>



								</div>
							</div>
						</div>
					</div>

				</div>








			</div>
		</div>
	</div>
</div>

<?php
$this->load->view('front/includes/footer');
?>


<!-- <script type="text/javascript" >



	
	function getfilelist( cont, root,skufolderfile_nm_field ) {

		//alert("getfilelist");
	
		$( cont ).addClass( 'wait' );

		
			
		$.post( '<?php echo base_url();?>front/upload/Foldertree', { dir: root, skufolderfile_nm_field: skufolderfile_nm_field }, function( data ) {
	
			$( cont ).find( '.start' ).html( '' );
			$( cont ).removeClass( 'wait' ).append( data );
			if( 'Sample' == root ) 
				$( cont ).find('UL:hidden').show();
			else 
				$( cont ).find('UL:hidden').slideDown({ duration: 500, easing: null });
			
		});
	}
	
	$( '#foldertree_container' ).on('click', 'LI A', function() {
		var entry = $(this).parent();
		
		if( entry.hasClass('folder') ) {
			if( entry.hasClass('collapsed') ) {
						
				entry.find('UL').remove();
				getfilelist( entry, escape( $(this).attr('rel') ));
				entry.removeClass('collapsed').addClass('expanded');
			}
			else {
				
				entry.find('UL').slideUp({ duration: 500, easing: null });
				entry.removeClass('expanded').addClass('collapsed');
			}
		} else {
			//$( '#selected_file' ).text( "File:  " + $(this).attr( 'rel' ));
			$( '#selected_file' ).html( "<img src='"+ $(this).attr( 'rel' )+"' height='60' width='60'>");
		}
	return false;
	});
	

</script> -->


<script src='<?php echo base_url();?>/website-assets/js/dragdropfolder.js'></script>

<script type="text/javascript">
	// Folder Upload

	$(document).ready(function() {
	    $("#skufolderupld_zip").click(function(e) {
	        e.preventDefault();
	    });
	});

	const dropzone = document.querySelector('.dropzone')
	dropzone.addEventListener('dragover', evt => evt.preventDefault())
	dropzone.addEventListener('drop', async evt => {
		evt.preventDefault()
		const files = await window.getFilesFromDataTransferItems(evt.dataTransfer.items)
        //console.log('files', files)
        files.forEach((file, i) => {
          //console.log(i, file.filepath);


          var file=file;

/*          formData= new FormData();
          formData.append("image", file);


          var data = $.ajax({
          url: "http://localhost/PiquicSite/testdragdrop_upload.php",
          type: "POST",
          data: formData,
          processData: false,
          contentType: false,
          async: false
          }).responseText;

          console.log(data);*/







          var folderPathName=file.filepath;
          var arrPath=folderPathName.split("/");
          var folderName=$.trim(arrPath[0]);


           //$('#skufolderfile_nm_field').val(folderName);

           var folderNamePath='&emsp;<i class="fas fa-folder text-warning"></i>&emsp;'+folderName;
           $('#skufolderfile_nm').html(folderNamePath);


           $('#skufolderupmgs').removeClass('d-none');


           var formdata = new FormData();
           formdata.append("upld_skufolder", file);
           formdata.append("upld_skufolder_name", folderPathName);
           var ajax = new XMLHttpRequest();
           ajax.upload.addEventListener("progress", progressHandlerFolder, false);
           ajax.addEventListener("load", completeHandlerFolder, false);
           ajax.addEventListener("error", errorHandlerFolder, false);
           ajax.addEventListener("abort", abortHandlerFolder, false);
           ajax.open("POST", "<?php echo base_url();?>front/upload/uploadSKUFolder");
			//ajax.open("POST", "http://localhost/PiquicSite/testdragdrop_upload.php");
			ajax.send(formdata);


		})
      })

	function progressHandlerFolder(event){
		var percent = (event.loaded / event.total) * 100;
		//console.log(percent);
		$("#skufolderfile_pc").attr("aria-valuenow", Math.round(percent));
		$("#skufolderfile_pc").css("width", Math.round(percent) + "%");
	}
	function completeHandlerFolder(event){
		$("#skufolderzipSuccess").removeClass("d-none");
		$('#skufolderenlblimg').removeClass('d-none');

         console.log(event.target.responseText);
         var response=event.target.responseText;

         
         if($.trim(response)=='not_image')
         {
         	//alert($.trim(response));
         	alert("Ignored non Image file.");
         	var res_text='Not an image file exist.';
         }
         else
         {

         var responseArr=response.split("@@@@@");
         var sku_folder=$.trim(responseArr[0]);
         var res_text=$.trim(responseArr[1]);

         var folderName =$('#skufolderfile_nm_field').val();
         if(folderName!='')
         {


         	if(folderName.search(","+sku_folder+",")=='-1')
         	{
         		var folderNameCurrent=folderName+sku_folder+",";
         		$('#skufolderfile_nm_field').val(folderNameCurrent);

         		/*var folderNameAllDiv='<div class="row" id="'+sku_folder+'" data-toggle="collapse" href="#'+sku_folder+'_images" ><div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12"><span>&emsp;<i class="fas fa-folder text-warning"></i>&emsp;'+sku_folder+'</span></div></div><div id="'+sku_folder+'_images" class="row collapse"><div class="col-12 col-sm-12 col-md-4 col-lg-4 col-xl-4"><img class="w-100 mb-3" src="temp_folder_upload/4/8907769095402/8907769095402-Back-Angles0011.JPG"></div></div>';*/
         		var folderNameAllDiv='<div class="row" onclick="openSKUFolder(\''+sku_folder+'\')" id="'+sku_folder+'" data-toggle="collapse" href="#'+sku_folder+'_images"><div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12"><span>&emsp;<i class="fas fa-folder text-warning"></i>&emsp;'+sku_folder+'</span></div></div><div id="'+sku_folder+'_images" class="row "></div>';

         		$('#folderList').append(folderNameAllDiv);


         		
         	}
         	
         }
         else
         {
         	var folderNameCurrent=","+sku_folder+",";
         	$('#skufolderfile_nm_field').val(folderNameCurrent);

         	/*	var folderNameAllDiv='<div class="row" id="'+sku_folder+'" data-toggle="collapse" href="#'+sku_folder+'_images" ><div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12"><span>&emsp;<i class="fas fa-folder text-warning"></i>&emsp;'+sku_folder+'</span></div></div><div id="'+sku_folder+'_images" class="row collapse"><div class="col-12 col-sm-12 col-md-4 col-lg-4 col-xl-4"><img class="w-100 mb-3" src="temp_folder_upload/4/8907769095402/8907769095402-Back-Angles0011.JPG"></div>';*/

         	var folderNameAllDiv='<div class="row" onclick="openSKUFolder(\''+sku_folder+'\')" id="'+sku_folder+'" data-toggle="collapse" href="#'+sku_folder+'_images"><div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12"><span>&emsp;<i class="fas fa-folder text-warning"></i>&emsp;'+sku_folder+'</span></div></div><div id="'+sku_folder+'_images" class="row "></div>';

         	$('#folderList').append(folderNameAllDiv);
         }

     }
    
        // getfilelist( $('#foldertree_container') , 'temp_folder_upload/<?php echo $user_id;?>', $('#skufolderfile_nm_field').val() );

        _("skufoldertxtSuccess").innerHTML = res_text;



		//_("skufoldertxtSuccess").innerHTML = "Upload Successfully";
	}
	function errorHandlerFolder(event){
		$("#skufolderzipDanger").removeClass("d-none");
		_("skufoldertxtDanger").innerHTML = "Upload Failed";
	}
	function abortHandlerFolder(event){
		$("#skufolderzipDanger").removeClass("d-none");
		_("skufoldertxtDanger").innerHTML = "Upload Aborted";
	}



	function openSKUFolder(foldername){
	//$("#loader").show();

	var foldernamePath='temp_folder_upload/<?php echo $user_id;?>';
	var skufolderfile_nm=$('#skufolderfile_nm_field').val();

	$.ajax({
	method:"post",
	url: "<?php echo base_url();?>front/upload/openSKUFolder",
	data:'&foldername='+foldername+'&path='+foldernamePath+'&skufolderfile_nm='+skufolderfile_nm,
	success: function(result){

	$("#"+foldername+"_images").html(result);
	$("#"+foldername+"_images").toggle();


	//$("#loader").hide();

	}
	});

	}



function skufolderlabelimg(){

	var skufolderfile_nm=$.trim($('#skufolderfile_nm_field').val());
	 //alert(skufolderfile_nm);

	 $("#loader").show();


	 var responseText = $.ajax({
	 	type:'POST',
	 	url:"<?php echo base_url();?>front/upload/checkSKULengthExistOrNotFolder",
	 	data: {'skufolderfile_nm':skufolderfile_nm},
	 	async: false
	 }).responseText;

	//alert(responseText);




		if($.trim(responseText)=='sessionout')
		{
		$("#loader").hide();
		alert("Session out please login first.");
		window.location.href="<?php echo base_url();?>";
		return false;
		}
		else if($.trim(responseText)=='not_image')
		{
		$("#loader").hide();
		alert("Files are not image.");
		window.location.href="<?php echo base_url();?>uploadimage";
		return false;
		}
		else if($.trim(responseText)=='less_strlen')
		{
			$("#loader").hide();
			alert("SKU length should be minimum 3.");
			window.location.href="<?php echo base_url();?>uploadimage";
			return false;
		}
		else if($.trim(responseText)=='exist')
		{
			var x = confirm("Same SKU exist.Are you sure you want to proceed?");
			if (x)
			{




				var responseText1 = $.ajax({
					type:'POST',
					url:"<?php echo base_url();?>front/upload/generateSKUForExistFolder",
					data: {'skufolderfile_nm':skufolderfile_nm},
					async: false
				}).responseText;

									//alert(responseText1);

									if(responseText1!='')
									{
										$('#skufolderfile_nm_field').val(responseText1);
										var skufolderfile_nm1=$.trim($('#skufolderfile_nm_field').val());


										$.ajax({
											method:"post",
											url: "<?php echo base_url();?>front/upload/moveSkuFolderUpload",
											data:'skufolderfile_nm='+skufolderfile_nm1,
											success: function(result){

												$("#loader").hide();

														//alert(result);
														//oTable.ajax.reload();
														//$('#example1').dataTable().fnStandingRedraw();
														window.location.href="<?php echo base_url();?>label";
													}
												});



									}


								}
								else
								{

									//alert("cancel");

									var responseText1 = $.ajax({
										type:'POST',
										url:"<?php echo base_url();?>front/upload/deleteCancelSKUForExistFolder",
										data: {'skufolderfile_nm':skufolderfile_nm},
										async: false
									}).responseText;

									//alert(responseText1);

									if(responseText1!='')
									{
										window.location.href="<?php echo base_url();?>uploadimage";
										$("#loader").hide();	
										return false;
									}


									
								}
							}
							else
							{

				//alert("ss");

				$.ajax({
					method:"post",
					url: "<?php echo base_url();?>front/upload/moveSkuFolderUpload",
					data:'skufolderfile_nm='+skufolderfile_nm,
					success: function(result){

						$("#loader").hide();

							//alert(result);
							//oTable.ajax.reload();
							//$('#example1').dataTable().fnStandingRedraw();
							window.location.href="<?php echo base_url();?>label";
						}
					});
			}








			/* */








		}

// Zip Upload
function _(el){
	return document.getElementById(el);
}

function uploadFile(){
	var file = _("upld_zip").files[0];
	$('#upmgs').removeClass('d-none'); 



	var zipfilepath='&emsp;<i class="fas fa-file-archive text-warning"></i>&emsp;'+file.name;
	$('#file_nm').html(zipfilepath);
	$('#file_nm_hid').val(file.name);

	var formdata = new FormData();
	formdata.append("upld_zip", file);
	var ajax = new XMLHttpRequest();
	ajax.upload.addEventListener("progress", progressHandler, false);
	ajax.addEventListener("load", completeHandler, false);
	ajax.addEventListener("error", errorHandler, false);
	ajax.addEventListener("abort", abortHandler, false);
	ajax.open("POST", "<?php echo base_url();?>front/upload/uploadfile");
	ajax.send(formdata);
}

function progressHandler(event){
	var percent = (event.loaded / event.total) * 100;
	console.log(percent);
	$("#file_pc").attr("aria-valuenow", Math.round(percent));
	$("#file_pc").css("width", Math.round(percent) + "%");
}
function completeHandler(event){
	$("#zipSuccess").removeClass("d-none");
	$('#enlblimg').removeClass('d-none');
	_("txtSuccess").innerHTML = event.target.responseText;
}
function errorHandler(event){
	$("#zipDanger").removeClass("d-none");
	_("txtDanger").innerHTML = "Upload Failed";
}
function abortHandler(event){
	$("#zipDanger").removeClass("d-none");
	_("txtDanger").innerHTML = "Upload Aborted";
}


function labelimg(){

	//var file_nm=$('#file_nm').text();
	var file_nm=$('#file_nm_hid').val();
	//alert(file_nm);

	$("#loader").show();

	var responseText = $.ajax({
		type:'POST',
		url:"<?php echo base_url();?>front/upload/checkSKULenExistMoveunzipfile",
		data: {'file_nm':file_nm},
		async: false
	}).responseText;

	//alert(responseText);




	if($.trim(responseText)=='sessionout')
	{
		$("#loader").hide();
		alert("Session out please login first.");
		window.location.href="<?php echo base_url();?>";
		return false;
	}
	else if($.trim(responseText)=='not_image')
	{
		$("#loader").hide();
		alert("Files are not image.");
		window.location.href="<?php echo base_url();?>uploadimage";
		return false;
	}
	else if($.trim(responseText)=='less_strlen')
	{
		$("#loader").hide();
		alert("SKU length should be minimum 3.");
		window.location.href="<?php echo base_url();?>uploadimage";
		return false;
	}
	else if($.trim(responseText)=='exist')
	{
		var x = confirm("Same SKU exist.Are you sure you want to proceed?");
		if (x)
		{




			var responseText1 = $.ajax({
				type:'POST',
				url:"<?php echo base_url();?>front/upload/generateSKUForExistZip",
				data: {'file_nm':file_nm},
				async: false
			}).responseText;

									//alert(responseText1);

									if(responseText1!='')
									{
										


										$.ajax({
											method:"post",
											url: "<?php echo base_url();?>front/upload/moveunzipfile",
											data:'file_nm='+file_nm,
											success: function(result){

												$("#loader").hide();
												window.location.href="<?php echo base_url();?>label";
											}
										});



									}


								}
								else
								{


									var responseText1 = $.ajax({
										type:'POST',
										url:"<?php echo base_url();?>front/upload/deleteCancelSKUForExistZip",
										data: {'file_nm':file_nm},
										async: false
									}).responseText;

									if(responseText1!='')
									{
										window.location.href="<?php echo base_url();?>uploadimage";
										$("#loader").hide();	
									}
																		
									return false;
								}
							}
							else
							{

				    //alert("ss");

				    $.ajax({
				    	method:"post",
				    	url: "<?php echo base_url();?>front/upload/moveunzipfile",
				    	data:'file_nm='+file_nm,
				    	success: function(result){

				    		$("#loader").hide();
				    		window.location.href="<?php echo base_url();?>label";
				    	}
				    });

				  }






/*				$.ajax({
				method:"post",
				url: "<?php echo base_url();?>front/upload/moveunzipfile",
				data:'file_nm='+file_nm,
				success: function(result){

		      $("#loader").hide();
          window.location.href="<?php echo base_url();?>label";
				}
			});*/










		}

		$(".upload_sku_files").on("change", function(e) {

//alert("goko");
$("#loader").show();

var files = e.target.files,
filesLength = files.length;
var f = files[0];
var fileReader = new FileReader();
fileReader.onload = (function(e) {
	var file = e.target;

	console.log(e.target.result);

         /* $("<span class=\"pip\">" +
            "<img class=\"imageThumb\" src=\"" + e.target.result + "\" title=\"" + file.name + "\"/>" +
            "<br/><span class=\"remove\">Remove</span>" +
            "</span>").insertAfter("#files");*/




	//alert(no);
	var input = document.getElementById("sku_files");
	var inputArr=input.files;
	var check_complete='';
	for(var k=0;k<inputArr.length;k++)
	{
		var file=inputArr[k];

       //alert(file);
       console.log("lllllllll");
        console.log(file);
        console.log(file.type);
        console.log("lllllllll");
        if(file != undefined && (file.type=='image/png' || file.type=='image/jpg'   || file.type=='image/jpeg') ){
        	formData= new FormData();
        	formData.append("image", file);


        	var data = $.ajax({
        		url: "<?php echo base_url();?>front/labelimages/upload_files_fromsku",
        		type: "POST",
        		data: formData,
        		processData: false,
        		contentType: false,
        		async: false
        	}).responseText;



		 if($.trim(data)=='sessionout')
		{
			$("#loader").hide();
			alert("Session out. Please login first!.");
			window.location.href="<?php echo base_url();?>home";
			return false;
		}
		else
		{

				 //alert("koko");
	     //alert(data);

	     var pimgVal=$.trim($("#pimg").val());
	     if(pimgVal!='')
	     {
	     	var item_chek=","+$.trim(data)+",";
	     	var check=(pimgVal.indexOf(item_chek));
	     	  //alert(check);
	     	  var pimgValItem=pimgVal+$.trim(data)+",";



	     	}
	     	else
	     	{
	     		var pimgValItem=","+$.trim(data)+",";

	     	}
	     	
	     	$("#pimg").val(pimgValItem);

	     	var pimgValCheckCount=$.trim($("#pimg").val());
	     	var pimgValCheckCount = pimgValCheckCount.replace(/^,|,$/g, '');

	     	var checkArr=pimgValCheckCount.split(",");
	     	var checkLength=checkArr.length;



	     	if(checkLength<=10)
	     	{

                //alert(show_ctn_img);

                var show_pimg="<div class='col-6 col-sm-6 col-md-3 col-lg-3 col-xl-3'><img class='w-100 border rounded mb-2' src='temp_upload/<?php echo $user_id;?>/"+"thumb_"+data+"' ></div>";
                $("#skuImageUploadDiv").append(show_pimg);

                $("#div_ctn_img").removeClass("d-none");
                $("#ctn_img").html(checkLength);


              }
              else
              {
              	alert("You exceed your limit. Only 10 images you can upload at a time");
              }



              var lastCount=parseInt(inputArr.length)-1;

            // alert(k+"---"+lastCount);
            if(k==lastCount)
            {
		      	// $("#loader").hide();

		      	var ctn=0;
		      	$("img").on('load', function(){

						//console.log(this.length);
					   //console.log(this);
					//alert("pp");

					if (this.complete) {
					// this image already loaded
					// do whatever you would do when it was loaded
					console.log("completed");
					//alert("completed");
					//alert(ctn);
					if(ctn==lastCount)
					{
						$("#loader").hide();
					}
					else
					{
						ctn++;
					}
					
				} else {

					$(this).on('load', function() {
					// image now loaded
				});

				}

			});


		      }


              
              }





		    }
		    else
		    {
		    	 $("#loader").hide();
		    	alert('Uploaded file is not an image!');
		    	//return false;
		    	window.location.href="<?php echo base_url();?>uploadimage";
		    }




		  }



		});
fileReader.readAsDataURL(f);

$('#upldImg').hide();
$('#btnCretSKU').removeClass('d-none');

});

	jQuery.validator.addMethod("alphanumeric", function(value, element) {
	return this.optional(element) || /^[a-z0-9\_]+$/i.test(value);
	}, "Enter only Letters, numbers, and underscore for SKU!.");

		jQuery(function ($) {
	   // $('.content-wrapper').css('min-height','900px');	   $('#entity_id').select2();
	   $('#single_sku_form').validate({

	   	rules: {

	   		txtSKUNew:{
	   			required:true,
	   			minlength: 3,
				alphanumeric: true,
				remote: {
	   				url: "<?php echo base_url();?>front/upload/checkexistsku",
	   				type: "post"
	   			},
	   		},
	   		

	   	},

	   	messages: {
	   		txtSKUNew:{
	   			remote:"This sku already exists.",
	   			alphanumeric: 'Letters, numbers, and underscore only please'
	   		},
	   		
	   	},
	   	submitHandler: function(form) {
	   		$("#loader").show();
	   			 // var formData = new FormData(form);
	   			 var user_id='<?php echo $user_id;?>';
	   			 var txtSKUNew=$('#txtSKUNew').val();
	   			 var pimg=$('#pimg').val();



	   			 $.ajax({
	   			 	method:'POST',
	   			 	url: "<?php echo base_url();?>front/upload/insertSKUFromSidebar", 
	   			 	data:"&user_id="+user_id+"&txtSKUNew="+txtSKUNew+"&pimg="+pimg,
	   			 	success: function(result){
	   			 		if(result)
	   			 		{
	   			 			$("#loader").hide();
							//consu;le
							//console.log(result);
							//alert(result);					
							//var val=$.trim(result);
							//alert(val);
							window.location.href="<?php echo base_url();?>label";


						}
					}
				});



	   			}, 


	   		});	
	 });

	</script>