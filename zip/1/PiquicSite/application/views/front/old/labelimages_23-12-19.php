<?php
$data['page_title'] = "label image";

$this->load->view('front/includes/header',$data);
// $this->load->view('front/includes/menu');
$this->load->view('front/includes/nav');

if ($this->session->userdata('front_logged_in')) {
	$session_data = $this->session->userdata('front_logged_in');
	$user_id = $session_data['user_id'];
	$user_name = $session_data['user_name'];
	
	
	
} else {
	
	$session_data = $this->session->userdata('front_logged_in');
	$user_id = $session_data['user_id'];
	$user_name = $session_data['user_name'];
	
	
}

$query=$this->db->query("select * from  tbl_users  WHERE user_id='$user_id' ");
$userDetails= $query->result_array();
$autostyle_check=$userDetails[0]['autostyle'];
?>

<div class="container-fluid">
	<div class="row">

		<?php
		$this->load->view('front/includes/sidebar');

		?>
		
		<div class="col-12 col-sm-12 col-md-9 col-lg-9 col-xl-9 border mainscroll">
			<div class="container pt-3">
				<div class="row mb-4">
					<div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
						<div class="row">
							<div class="col-12 col-sm-12 col-md-4 col-lg-4 col-xl-4">&nbsp;</div>
							<div class="col-12 col-sm-12 col-md-4 col-lg-4 col-xl-4">&nbsp;</div>
							<div class="col-12 col-sm-12 col-md-4 col-lg-4 col-xl-4">
								<div class="d-flex justify-content-around">
									<!-- <span class="text-piquic pr-2" data-toggle="collapse" data-target="#filter-it"><i class="fas fa-sliders-h fa-2x"></i></span> -->

									<!-- <span class="text-piquic pr-2"><i class="fas fa-sliders-h fa-2x"></i></span> -->

									<div class="form-inline flex-grow-1">
										<label class="sr-only" for="searchSKU">Search SKU</label>
										<input type="Search SKU" class="form-control" id="searchSKU" placeholder="Search SKU"  value="<?php echo $sku_key;?>">

										<button class="btn btn-piquic " type="submit"><i class="fas fa-search " onclick="searchSKU();"></i></button>
									</div>
									


								</div>
							</div>
						</div>
					</div>
				</div>
			</div>

			<div class="bg-piquic full-w" style="min-height: .5px;">
				<div class="container">
					<div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
						<div id="filter-it" class="row text-white d-none">
							<div class="col-12 col-sm-12 col-md-3 col-lg-3 col-xl-3 pl-4">
								<p class="pt-3" id="itemcount"> 0 item selected</p>
							</div>

							<div class="col-12 col-sm-12 col-md-2 col-lg-2 col-xl-2 pl-4">
								<select class="custom-select navSlct mt-2 text-white" name="slct_cate" id="slct_cate"  onchange="lblmultcat(this);">
									<option class="text-piquic" value=""  >Category</option>
									<option class="text-piquic" value="top">Top</option>
									<option class="text-piquic" value="bottom">Bottom</option>
									<option class="text-piquic" value="dress">Dress</option>
									<option class="text-piquic" value="topUg">Top Undergarment</option>
									<option class="text-piquic" value="bottomUg">Bottom Undergarment</option>
									<option class="text-piquic" value="dressUg">Top/Bottom Undergarment</option>
								</select>
							</div>

							<div class="col-12 col-sm-12 col-md-2 col-lg-2 col-xl-2 pl-4">
								<select class="custom-select navSlct mt-2 text-white" name="slct_gend" id="slct_gend" onchange="lblmultgend(this);"> 
									<option class="text-piquic" value="" >Gender</option>
									<option class="text-piquic" value="1">Male</option>
									<option class="text-piquic" value="2">Female</option>
									<option class="text-piquic" value="3">Boys (0-2 years)</option>
									<option class="text-piquic" value="4">Boys (2-9 years)</option>
									<option class="text-piquic" value="5">Girls (0-2 years)</option>
									<option class="text-piquic" value="6">Girl (2-9 years) </option>
								</select>
							</div>

							<div class="col-12 col-sm-12 col-md-4 col-lg-4 col-xl-4 pt-3 pl-4">&nbsp;</div>

							<!-- <div class="col-12 col-sm-12 col-md-1 col-lg-1 col-xl-1 pt-3 pl-4">
								<span data-toggle="collapse" data-target="#filter-it"><i class="fas fa-times"></i></span>
							</div> -->
						</div>
					</div>
				</div>
			</div>

			<?php
			if(!empty($labelimagesinfo))
			{
				?>

				<div class="container pt-3">
					<div class="row mb-4">
						<div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
							<div class="bg-white">
								<div class="row mb-3">
									<div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
										<div class="custom-control custom-checkbox">
											<input type="checkbox" class="custom-control-input" id="chbkSlctAll">
											<label class="custom-control-label roundCheck lead" for="chbkSlctAll">Select All SKUs</label>
										</div>
									</div>
								</div>

								<div class="row" id="SKU_ALL">
									<div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">

										<?php foreach($labelimagesinfo as $row){ 
											$category=$row->category;
											$gender=$row->gender;
											$id=$row->upimg_id;
											$sku=$row->zipfile_name;


											?>

											<div id="SKU_<?php  echo $row->upimg_id; ?>" class="pb-5">
												<div class="d-flex justify-content-start">
													<div class="custom-control custom-checkbox">
														<input type="checkbox" class="custom-control-input checkSingle" id="chbkSKU_<?php  echo $row->upimg_id; ?>" value="<?php  echo $row->zipfile_name; ?>">
														<label class="custom-control-label roundCheck lead" for="chbkSKU_<?php  echo $row->upimg_id; ?>">
															<div class="row">
																<div class="col-12 d-inline">
																	SKU:<span id="divSKUEdit<?php  echo $row->upimg_id; ?>" class="pl-1"><?php  echo $row->zipfile_name; ?></span>
																</div>
															</div>
														</label>
													</div>

													<span id="divSKUSave<?php  echo $row->upimg_id; ?>" class="d-none text-right pl-3"><input type="text" class="form-control pl-3" id="txtSKUEdit<?php  echo $row->upimg_id; ?>" name="txtSKUEdit<?php  echo $row->upimg_id; ?>" value="<?php  echo $row->zipfile_name; ?>"></span>

													<span id="spanSKUEdit<?php  echo $row->upimg_id; ?>" class="text-secondary lead pl-3"  onclick="editsku('<?php  echo $row->upimg_id; ?>','<?php  echo $row->zipfile_name; ?>');">	<i class="far fa-edit"></i>
													</span>

													<span id="spanSKUSave<?php  echo $row->upimg_id; ?>" class="text-secondary lead d-none pl-3" onclick="savesku('<?php  echo $row->upimg_id; ?>','<?php  echo $row->zipfile_name; ?>');">
														<i class="fas fa-check"></i>
													</span>

													<span id="spanSKUDelete<?php echo $row->upimg_id; ?>" class="text-secondary lead pl-3" onclick="delsku('<?php  echo $row->upimg_id; ?>','<?php  echo $row->zipfile_name; ?>');">
														<i class="far fa-times-circle"></i>
													</span>
												</div>




												<div class="row pt-3 pl-md-4">

													<div class="col-12 col-sm-12 col-md-4 col-lg-4 col-xl-4">
														<select class="custom-select" id="cate_SKU_<?php  echo $row->upimg_id; ?>" name="cate_SKU_<?php  echo $row->upimg_id; ?>" onchange="lblsingcat(this,'<?php  echo $row->zipfile_name; ?> ','<?php  echo $row->upimg_id; ?>');">
															<option value="" >Select Category</option>
															<option value="top" <?php if($category=='top'){echo 'selected';}?>>Top</option>
															<option value="bottom" <?php if($category=='bottom'){echo 'selected';}?>>Bottom</option>
															<option value="dress" <?php if($category=='dress'){echo 'selected';}?>>Dress</option>
															<option value="topUg" <?php if($category=='topUg'){echo 'selected';}?>>Top Undergarment</option>
															<option value="bottomUg" <?php if($category=='bottomUg'){echo 'selected';}?>>Bottom Undergarment</option>
															<option value="dressUg" <?php if($category=='dressUg'){echo 'selected';}?>>Top/Bottom Undergarment</option>
														</select>
													</div>

													<div class="col-12 col-sm-12 col-md-4 col-lg-4 col-xl-4">
														<select class="custom-select" id="gend_SKU_<?php  echo $row->upimg_id; ?>" onchange="lblsingend(this,'<?php  echo $row->zipfile_name; ?> ','<?php  echo $row->upimg_id; ?>');">
															<option value="" >Select Gender</option>
															<option value="1" <?php if($gender=='1'){echo 'selected';}?>>Male</option>
															<option value="2" <?php if($gender=='2'){echo 'selected';}?>>Female</option>
															<option value="3" <?php if($gender=='3'){echo 'selected';}?>>Boys (0-2 years)</option>
															<option value="4" <?php if($gender=='4'){echo 'selected';}?> >Boys (2-9 years)</option>
															<option value="5" <?php if($gender=='5'){echo 'selected';}?>>Girls (0-2 years)</option>
															<option value="6" <?php if($gender=='6'){echo 'selected';}?>>Girl (2-9 years) </option>
														</select>
													</div>
												</div>


												<?php
												$selectedAllAngle[]='';
												$selectedAngle[]='';

												unset($selectedAllAngle);
												unset($selectedAngle);
			//echo "select * from  tbl_upload_images where upimg_id='$id'";
												$query=$this->db->query("select * from  tbl_upload_images where upimg_id='$id'");   
												foreach ($query->result() as $resrow)
												{ 
													$img=$resrow->img_name;
													$imgid=$resrow->id;
													$img_ang=$resrow->img_ang;
													$selectedAngle[]=$img_ang;

													if($img_ang=='')
													{
														$selectedAllAngle[]='No';
													}
													else
													{
														$selectedAllAngle[]='Yes';
													}

												}


             //echo "<pre>";
             //print_r($selectedAllAngle);
												if(isset($selectedAngle))
												{
													if(in_array("Tag", $selectedAngle))
													{
														$tag_present='1';
													}
													else
													{
														$tag_present='0';
													}
												}
												$tag_present='1';


												?>							

												<div class="row pt-3 pl-md-4 <?php if($gender!='' && $category!=''
												&& !in_array('No',$selectedAllAngle) ){?> d-none <?php } ?>">
												<div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
													<div class="alert alert-danger" role="alert">
														<i class="fas fa-exclamation-circle"></i>&nbsp;Select the category, gender and angles for each images to activate styling.
													</div>
												</div>
											</div>

											<div class="row pt-3 pl-md-4 <?php if( $tag_present=='1' ){?> d-none <?php } ?>">
												<div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
													<div class="alert alert-danger" role="alert">
														<i class="fas fa-exclamation-circle"></i>&nbsp;Please upload Tag image.
													</div>
												</div>
											</div>

											<div class="row pt-3 pl-md-4">
												<?php
												$query=$this->db->query("select * from  tbl_upload_images where upimg_id='$id' order by id asc");   
												foreach ($query->result() as $resrow)
												{ 
													$img=$resrow->img_name;
													$imgid=$resrow->id;
													?>
													<script type="text/javascript">

														$(window).on('load', function(){

															var img = document.getElementById('img_<?php echo $imgid; ?>');

															var width = img.naturalWidth;
															var height = img.naturalHeight;
															console.log(width);
             			// console.log(height);

             			if (width > height){
             				$('#rtImg_<?php echo $imgid; ?>').removeClass('d-none');
             			} else {
             				$('#orgImg_<?php echo $imgid; ?>').removeClass('d-none');
             			}
             		});

             	</script>

             	<div class="col-12 col-sm-6 col-md-3 col-lg-3 col-xl-3 pb-2">

             		<div id="orgImg_<?php echo $imgid; ?>" class="d-none w-100">
             			<img id="img_<?php echo $imgid; ?>" class="w-100" src="<?php echo base_url().'/upload/'.$user_id.'/'.$sku.'/thumb_'.$img ;?>" alt="...">
             		</div>

             		<div id="rtImg_<?php echo $imgid; ?>" class="img-box d-none">
             			<img id="img_<?php echo $imgid; ?>" class="img" src="<?php echo base_url().'/upload/'.$user_id.'/'.$sku.'/thumb_'.$img ;?>" alt="...">
             		</div>

             		<div class="d-flex justify-content-around my-4">
             			<select class="custom-select" id="ang_SKU_<?php  echo $row->upimg_id; ?>_c"  onchange="changangle(this,'<?php  echo $id; ?>','<?php  echo $img; ?>','<?php  echo $imgid; ?>');">
             				<option value="" >Select Angle</option>
             				<option value="Front" <?php  if(strpos( $img,'Front') !== false) {echo 'selected'; } ?>>FRONT</option>
             				<option value="Back" <?php  if(strpos( $img,'Back') !== false) {echo 'selected'; } ?>>BACK</option>
             				<option value="LeftSide" <?php  if(strpos( $img,'LeftSide') !== false) {echo 'selected'; } ?>>LEFT</option>
             				<option value="RightSide"  <?php  if(strpos( $img,'RightSide') !== false) {echo 'selected'; } ?>>RIGHT</option>
             				<option value="Tag"  <?php  if(strpos( $img,'Tag') !== false) {echo 'selected'; } ?>>TAG</option>

             			</select>


             			<div class="dropdown">
             				<a class="btn" href="#" role="button" id="dropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><i class="fas fa-ellipsis-v"></i></a>
             				<div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
             					<a class="dropdown-item" href="#" onclick="deleteUploadImages('<?php echo $imgid;?>','<?php echo $sku;?>','<?php echo $id;?>')">Delete</a>
															<!-- <a class="dropdown-item" href="#">Another action</a>
																<a class="dropdown-item" href="#">Something else here</a> -->
															</div>
														</div>


													</div>
												</div>
											<?php } ?>

											<div class="col-12 col-sm-6 col-md-3 col-lg-3 col-xl-3 pb-2">
												<form id="upld_img_<?php echo $id; ?>" name="upld_img_<?php echo $id; ?>" method="post" enctype="multipart/form-data">	
													<div class="bg-light border-dotted py-5">
														<label class="w-100 text-center py-5" for="upld_img_SKU_<?php  echo $row->upimg_id; ?>" >
															<p class="py-1">
																<br><i class="far fa-file-image fa-2x"></i>
																<br>Upload Image<br>
															</p>
														</label>

														<input class="d-none" type="file" id="upld_img_SKU_<?php  echo $id; ?>" name="upld_img_SKU">
													</div>
													<input type="hidden" name="upld_img_data" id="upld_img_data<?php echo $id; ?>" value="Upload" style="display:none;"/>
													<input type="hidden" name="sku_data" id="sku_data" value="<?php  echo $row->zipfile_name; ?>" style="display:none;"/>
													<input type="hidden" name="user_id" id="user_id<?php echo $id; ?>" value="<?php  echo $user_id; ?>" />
												</form>

												<script type="text/javascript">
													$("#upld_img_SKU_<?php echo $id; ?>").change(function() {
														$('#upld_img_data<?php echo $id; ?>').click();
														$('#upld_img_data<?php echo $id; ?>').submit();
														filename = this.files[0].name;

													});

													$("form#upld_img_<?php echo $id; ?>").submit(function(e) {
														e.preventDefault();

														console.log(e);
														var formData = new FormData(this);
														$("#loader").show();
														$.ajax({
															url: '<?php echo base_url(); ?>front/labelimages/oneupload',
															type: 'POST',
															data: formData,
															success: function (data) {			
																
																$("#loader").hide();
													//location.reload();

													var upimg_id=$.trim(data);
													//alert(upimg_id);
													if(upimg_id!='')
													{

			////////////////////////////////////////////////////////////////////////////////////////

			

			var responseText = $.ajax({
				type:'POST',
				url:"<?php echo base_url();?>front/labelimages/label_ajax",
				data: {'upimg_id':upimg_id},
				async: false,
			}).responseText;

										//alert(responseText);

										$("#SKU_"+upimg_id).html(responseText);

			////////////////////////////////////////////////////////////////////////////////////////	
		}
		
													// if(data == 1){
													// 	location.reload();
													// }
												},
												cache: false,
												contentType: false,
												processData: false
											});
													});
												</script>
											</div>
										</div>


										<div class="row pt-3 pl-md-4">
											<div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
												<?php 
												if($autostyle_check=='1')
												{
													?>

													<button type="submit" class="btn btn-outline-dark btn-block <?php if($category=='' || $gender==''){ echo 'disabled'; }?>" <?php if($category!='' && $gender!='' && !in_array('No',$selectedAllAngle) && $tag_present=='1' ){ ?> onclick="sendautostyle('<?php  echo $row->zipfile_name; ?>','<?php echo $category; ?>','<?php echo $gender;?>','<?php echo $tag_present;?>');" <?php }?>id="btn_auto_style_<?php  echo $row->zipfile_name; ?>">Send For Auto Styling</button>
													<?php
												}
												else
												{
													?>
													<button type="submit" class="btn btn-outline-dark btn-block <?php if($category=='' || $gender==''){ echo 'disabled'; }?>" <?php if($category!='' && $gender!='' && !in_array('No',$selectedAllAngle) && $tag_present=='1' ){ ?> onclick="sendstyle('<?php  echo $row->zipfile_name; ?>','<?php echo $category; ?>','<?php echo $gender;?>');" <?php }?>id="btn_style_<?php  echo $row->zipfile_name; ?>">Send For Styling</button>
													<?php
												}
												?>







											</div>
										</div>



									</div>
								<?php  } 



								?>

								<input type="hidden" name="arrval" id="arrval">
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>


		<?php  
	}
	else
	{
		?>
		<div class="text-center text-danger lead pt-3" style="min-height: 60vh;">
			<span>No SKU Available.</span>
		</div>

		<?php

	}
	?>




</div>
</div>
</div>
<?php
$this->load->view('front/includes/footer');
?>

<script type="text/javascript">

	function searchSKU(){
		var sku =$('#searchSKU').val();
		window.location.href = "<?php echo base_url();?>label/"+sku ;

	}

	function editsku(imgid,sku){
		//alert(imgid);
		$('#spanSKUEdit'+imgid).addClass('d-none');
		$('#spanSKUSave'+imgid).removeClass('d-none');

		$('#spanSKUDelete'+imgid).addClass('d-none');

		$.ajax({
			type:'POST',					
			url : "<?php echo base_url();?>front/labelimages/getSKUForUpdate", 
			data:{'imgid':imgid,'sku':sku},
			success: function(result) {	
		//alert(result);
		$('#txtSKUEdit'+imgid).val($.trim(result));

		$('#divSKUEdit'+imgid).addClass('d-none');
		$('#divSKUSave'+imgid).removeClass('d-none');				

	}
});





	}

	function savesku(imgid,sku){
		$('#spanSKUEdit'+imgid).removeClass('d-none');
		$('#spanSKUSave'+imgid).addClass('d-none');

		$('#spanSKUDelete'+imgid).removeClass('d-none');

		var sku_edit=$('#txtSKUEdit'+imgid).val();	

			//alert(sku);	

			$.ajax({
				type:'POST',					
				url : "<?php echo base_url();?>front/labelimages/saveSKUForUpdate", 
				data:{'imgid':imgid,'sku':sku,'sku_edit':sku_edit},
				success: function(result) {	
			//alert(result);
			if($.trim(result)=='0')
			{
				alert("That SKU aleady exist!.");
			//$('#txtSKUEdit'+imgid).val('');
		}
		else
		{
			$('#divSKUEdit'+imgid).html($.trim(result));

			$('#divSKUEdit'+imgid).removeClass('d-none');
			$('#divSKUSave'+imgid).addClass('d-none');

			//window.location.href = "<?php echo base_url();?>label/" ;

			////////////////////////////////////////////////////////////////////////////////////////

			var upimg_id=imgid;
			var responseText = $.ajax({
				type:'POST',
				url:"<?php echo base_url();?>front/labelimages/label_ajax",
				data: {'upimg_id':upimg_id},
				async: false
			}).responseText;

		//alert(responseText);

		$("#SKU_"+upimg_id).html(responseText);

 ////////////////////////////////////////////////////////////////////////////////////////

}


}
});




		}



		function deleteUploadImages(imgid,sku,upimg_id){


			var x = confirm("Are you sure you want to delete?");
			if (x)
			{

				var sku=sku;
				var imgid =imgid;


				var sndstres = $.ajax({
					type:'POST',
					url : "<?php echo base_url();?>front/labelimages/deleteUploadImages", 
					data:{'imgid':imgid,'sku':sku},
					async: false
				}).responseText;


				if($.trim(sndstres)=='success')
				{
					////////////////////////////////////////////////////////////////////////////////////////


					var responseText = $.ajax({
						type:'POST',
						url:"<?php echo base_url();?>front/labelimages/label_ajax",
						data: {'upimg_id':upimg_id},
						async: false
					}).responseText;

					//alert(responseText);

					$("#SKU_"+upimg_id).html(responseText);
					

					////////////////////////////////////////////////////////////////////////////////////////
				}

				return true;
			}
			else
			{
				return false;
			}










		}



		function sendstyle(sku,cat,gen){
			var sku =sku;
			var cat =cat;
			var gen =gen;

			$.ajax({
				type:'POST',					
				url : "<?php echo base_url();?>front/labelimages/sendforstyle", 
				data:{'sku':sku,'cat':cat,'gen':gen},
				success: function(sndstres) {					
					window.location = "<?php echo base_url();?>stylequeue" ;
				}
			});

		}

		function sendautostyle(sku,cat,gen,tag_present){

			if(tag_present=='0')
			{
				alert("Please upload Tag image to proceed.");
				return false;
			}
			else
			{
				var sku =sku;
				var cat =cat;
				var gen =gen;
				var autoval = '1';
		//alert(autoval);

		if(autoval!='' && autoval!='0')
		{
			$.ajax({
				type:'POST',					
				url : "<?php echo base_url();?>front/stylequeue/autostylingUpdate", 
				data:{'sku':sku,'cat':cat,'gen':gen,'autoval':autoval},
				success: function(autostyres) {	
		//alert(autostyres);
		location.reload();
	}
});
		}

	}


}




function changangle(selval,id,img,imgid){
	var selval = selval.value;
	var id = id;
	var img = img;
	var imgid = imgid;

// alert(selval);

$.ajax({
	type:'POST',					
	url : "<?php echo base_url();?>front/labelimages/imgangchng", 
	data:{'id':id,'imgid':imgid,'selval':selval,'img':img},
	success: function(chngimgres) {	
				// alert(mulcatres);
				//location.reload();

            ////////////////////////////////////////////////////////////////////////////////////////

            var upimg_id=id;
            var responseText = $.ajax({
            	type:'POST',
            	url:"<?php echo base_url();?>front/labelimages/label_ajax",
            	data: {'upimg_id':upimg_id},
            	async: false
            }).responseText;

		//alert(responseText);

		$("#SKU_"+upimg_id).html(responseText);

 ////////////////////////////////////////////////////////////////////////////////////////

}
});

}


function lblmultcat(selectObject){
	var mulval = selectObject.value;
	var sku =$('#arrval').val();
	$.ajax({
		type:'POST',					
		url : "<?php echo base_url();?>front/labelimages/mulcatupdate", 
		data:{'skuno':sku,'category':mulval},
		success: function(mulcatres) {	
				// alert(mulcatres);
				//location.reload();

 ////////////////////////////////////////////////////////////////////////////////////////

            var sku_select=sku;
            var responseText = $.ajax({
            	type:'POST',
            	url:"<?php echo base_url();?>front/labelimages/label_select_ajax",
            	data: {'sku_select':sku_select},
            	async: false
            }).responseText;

		//alert(responseText);

		$("#SKU_ALL").html(responseText);

 ////////////////////////////////////////////////////////////////////////////////////////
			}
		});


}	

function lblmultgend(selectObject){
	var mulval = selectObject.value;
	var sku =$('#arrval').val();
	$.ajax({
		type:'POST',			
		url : "<?php echo base_url();?>front/labelimages/mulgenupdate", 		 
		data:{'skuno':sku,'gender':mulval},
		success: function(mulgenres) {	
			//location.reload();

			 ////////////////////////////////////////////////////////////////////////////////////////

            var sku_select=sku;
            var responseText = $.ajax({
            	type:'POST',
            	url:"<?php echo base_url();?>front/labelimages/label_select_ajax",
            	data: {'sku_select':sku_select},
            	async: false
            }).responseText;

		//alert(responseText);

		$("#SKU_ALL").html(responseText);

 ////////////////////////////////////////////////////////////////////////////////////////
		}
	});

}	


function delsku(upimg_id,sku){

	var x = confirm("Are you sure you want to delete?");
	if (x)
	{

		var sku=sku;
		$.ajax({
			type:'POST',			
			url : "<?php echo base_url();?>front/labelimages/delete", 
			data:{'skuno':sku},
			success: function(delres) {	
		// alert(delres);	
		//location.reload();

	////////////////////////////////////////////////////////////////////////////////////////


	var responseText = $.ajax({
		type:'POST',
		url:"<?php echo base_url();?>front/labelimages/label_ajax",
		data: {'upimg_id':upimg_id},
		async: false
	}).responseText;

		//alert(responseText);

		$("#SKU_"+upimg_id).html(responseText);
		$("#SKU_"+upimg_id).remove();

 ////////////////////////////////////////////////////////////////////////////////////////		
}
});
		return true;
	}
	else
	{
		return false;
	}






}


function lblsingcat(selectObject,sku,upimg_id){
	var value = selectObject.value;
	var sku =sku;
	$.ajax({
		type:'POST',			 
		url : "<?php echo base_url();?>front/labelimages/update",
		dataType : "JSON",
		data:{'category':value,'skuno':sku},
		success: function(statuschngres) {			
			//location.reload();

 ////////////////////////////////////////////////////////////////////////////////////////


 var responseText = $.ajax({
 	type:'POST',
 	url:"<?php echo base_url();?>front/labelimages/label_ajax",
 	data: {'upimg_id':upimg_id},
 	async: false
 }).responseText;

		//alert(responseText);

		$("#SKU_"+upimg_id).html(responseText);

 ////////////////////////////////////////////////////////////////////////////////////////


}
});
}

function lblsingend(selectObject,sku,upimg_id){
	var value = selectObject.value;
	var sku =sku;

	$.ajax({
		type:'POST',			  	
		url : "<?php echo base_url();?>front/labelimages/genupdate",
		dataType : "JSON",
		data:{'gender':value,'skuno':sku},
		success: function(statuschngres) {	
			//location.reload();


			////////////////////////////////////////////////////////////////////////////////////////


			var responseText = $.ajax({
				type:'POST',
				url:"<?php echo base_url();?>front/labelimages/label_ajax",
				data: {'upimg_id':upimg_id},
				async: false
			}).responseText;

		//alert(responseText);

		$("#SKU_"+upimg_id).html(responseText);

 ////////////////////////////////////////////////////////////////////////////////////////			  		
}
});
}

$(document).ready(function() {


	var chksku = []; 
	$("#chbkSlctAll").change(function(){
		if(this.checked){
			$("#filter-it").removeClass('d-none');
			$(".checkSingle").each(function(){
				this.checked=true; 
				var count=$('.checkSingle').filter(':checked').length;
				$('#itemcount').html(count+' item selected');
				chksku = $(".checkSingle:checked").map(function (i) {
					return $(this).val();
				}).get();
				$('#arrval').val(chksku);
			}) 
		}else{
			$("#filter-it").addClass('d-none');
			$(".checkSingle").each(function(){
				$('#itemcount').html(' 0 item selected');
				this.checked=false;
				chksku = $(".checkSingle:checked").map(function (i) {
					return $(this).val();
				}).get();
				$('#arrval').val(chksku);
			}) 
		}
	});

	$(".checkSingle").click(function () {
		var count=$('.checkSingle').filter(':checked').length;
		$('#itemcount').html(count+' item selected'); 
		chksku = $(".checkSingle:checked").map(function (i) {
			return $(this).val();
		}).get();
		$('#arrval').val(chksku);
		if(count>0){
			$("#filter-it").removeClass('d-none');
		}else{
			$("#filter-it").addClass('d-none');
		}

		if ($(this).is(":checked")){

			var isAllChecked = 0; 
			$(".checkSingle").each(function(){
				if(!this.checked){
					isAllChecked = 1;
				}
			}) 
			if(isAllChecked == 0){ $("#chbkSlctAll").prop("checked", true); } 
		}else {

			$("#chbkSlctAll").prop("checked", false);
		}
	});
});

</script>