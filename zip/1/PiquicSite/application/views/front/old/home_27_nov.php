<?php
session_start();


$data['page_title'] = "home";

$this->load->view('front/includes/header',$data);
// $this->load->view('front/includes/menu');

  if ($this->session->userdata('front_logged_in')) {
    $session_data = $this->session->userdata('front_logged_in');
     $user_id = $session_data['user_id'];
     $user_name = $session_data['user_name'];
     $_SESSION['user_id']=$user_id;
     $_SESSION['user_name']=$user_name;
    }
    else
    {
    $user_id = '';
    $user_name = ''; 
    $_SESSION['user_id']=$user_id;
     $_SESSION['user_name']=$user_name;
    }
 ?>
  

<div class="container">
  <div class="row my-4">
    <div class="col-12 col-sm-12 col-md-6 col-lg-6 col-xl-6">
      <h1>Choose your option:</h1>
    </div>
  </div>

  <div class="row mb-4">
    
    <div class="col-12 col-sm-12 col-md-12 col-lg-6 col-xl-6 pb-4">
      <div class="shadow">

     
        <div class="row">
          <div class="col-12 col-sm-5 col-md-5 col-lg-5 col-xl-5">
            <div class="overflow-hidden w-100 text-center">
              <img src="<?php echo site_url();?>assets/images/pages-img/<?php echo $pagecontents_first[0]['pimg']; ?> " alt="..." style="height: 26.25rem;">
            </div>
          </div>

          <div class="col-12 col-sm-7 col-md-7 col-lg-7 col-xl-7">
            <div class="pt-5 px-3 w-100">
              <h2 class="h2"><?php  echo $pagecontents_first[0]['page_title']; ?></h2>
              <p class="lead"><?php  echo $pagecontents_first[0]['content']; ?></p>
            </div>
          </div>
        </div>

        <div class="row">
          <div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
            <div class="w-100 border pt-3 pl-3 btn-cursor" id="btn_send_prod">
              <p class="lead"><i class="fas fa-arrow-right"></i>&nbsp;Get Started</p>
            </div>
          </div>
        </div>
       
      </div>
    </div>

    <div class="col-12 col-sm-12 col-md-12 col-lg-6 col-xl-6 pb-4">
      <div class="shadow">
       
   
        <div class="row">
          <div class="col-12 col-sm-5 col-md-5 col-lg-5 col-xl-5">
            <div class="overflow-hidden w-100 text-center">
              <img src="<?php echo site_url();?>assets/images/pages-img/<?php echo $pagecontents_second[0]['pimg']; ?>" alt="..." style="height: 26.25rem;">
            </div>
          </div>

          <div class="col-12 col-sm-7 col-md-7 col-lg-7 col-xl-7">
            <div class="pt-5 px-3 w-100">
             <h2 class="h2"><?php  echo $pagecontents_second[0]['page_title']; ?></h2>
              <p class="lead"><?php  echo $pagecontents_second[0]['content']; ?></p>
            </div>
          </div>
        </div>

        <div class="row">
          <div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
          <?php
          if($user_id!='')
          {
          ?>
          <div class="w-100 border pt-3 pl-3 btn-cursor" id="btn_upld_img_nextpage">
          <p class="lead"><i class="fas fa-arrow-right"></i>&nbsp;Get Started</p>
          </div>
          <?php
          }
          else
          {


          ?>
          <div class="w-100 border pt-3 pl-3 btn-cursor" id="btn_upld_img">
          <p class="lead"><i class="fas fa-arrow-right"></i>&nbsp;Get Started</p>
          </div>
          <?php
          }
          ?>

             

          </div>
        </div>
     
      </div>
    </div>

  </div>
</div>






<!-- <a href="#code" data-toggle="modal" data-target="#myModal" class="btn code-dialog">Display code</a> -->
<!-- <button type="button" class="btn btn-info btn-lg" data-toggle="modal" data-target="#myModal">Open Modal</button> -->

<!-- Modal -->
<!--   <div class="modal fade" id="myModal" role="dialog">
    <div class="modal-dialog">
    
      
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Modal Header</h4>
        </div>
        <div class="modal-body">
          <p>Some text in the modal.</p>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
      </div>
      
    </div>
  </div> -->

  <div id="myModal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="loginModalLabel" aria-hidden="true">
      <div class="modal-dialog" role="document">
    <div class="modal-content">
      <!-- <div class="text-center mt-3 mb-3"></div> -->
      <div class="modal-header">
        <!-- <h5 class="modal-title" id="loginModalLabel">Modal title</h5> -->
        <img class="img-fluid" src="assets/img/logo-piquic-sm.png">
       <!--  <button type="button" class="close" data-dismiss="modal" aria-label="Close" id="reg_close">
          <i class="far fa-times-circle"></i>
        </button>   -->
      </div>
      <div id="modal-tab" class="modal-header">
        <ul class="nav nav-pills mb-3" id="pills-tab" role="tablist">
          <li class="nav-item">
            <a class="nav-link active" id="pills-login-tab" data-toggle="pill" href="#pills-login" role="tab" aria-controls="pills-login" aria-selected="true"><i class="fas fa-user"></i> LOGIN</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" id="pills-register-tab" data-toggle="pill" href="#pills-register" role="tab" aria-controls="pills-register" aria-selected="false"><i class="fas fa-user-plus"></i> REGISTER</a>
          </li>
        </ul>
      </div>
      <div class="modal-body">
        <div class="tab-content" id="pills-tabContent">
          <div class="tab-pane fade show active" id="pills-login" role="tabpanel" aria-labelledby="pills-login-tab">
            <form class="login_form"  id="login_form" method="post">  

          <div id="error_message" style="display:none;">
          <div class="alert alert-danger">
          <strong>Invalid username or password!</strong>.
          </div>
            </div>
             <div id="success_message" style="display:none;">
          <div class="alert alert-success">
          <strong>Register successfully!</strong>.
          </div>
            </div>
              <table class="table table-borderless pr-4">
                <tr>
                  <td class="pt-3 text-right"><i class="fas fa-envelope"></i></td>            
                  <td><input type="email" class="form-control mb-2" name="email" id="email" placeholder="Enter your email">
                    <?php echo form_error('email', '<div class="error" style="color:red;">', '</div>'); ?>

                  </td>
                </tr>
                <tr>
                  <td class="pt-3 text-right"><i class="fas fa-lock"></i></td>
                  <td><input type="password" class="form-control mb-2" name="password" id="password" placeholder="Password">
                     <?php echo form_error('password', '<div class="error" style="color:red;">', '</div>'); ?>

                  </td>
                </tr>
                <!-- Section ID -->
                <input type="hidden" name="section-id" id="section-id">
                <tr>
                  <td colspan="2" class="text-center">
                    <button id="signin" type="submit" class="btn btn-piquic" name="signin">Login <i class="fas fa-sign-in-alt"></i></button>
                  </td>
                </tr>
              </table>
              </form>
              <span id="lsuccess" class="error" style="display:none;"></span>
            <?php// echo form_close(); ?>
          </div>
        <div class="tab-pane fade" id="pills-register" role="tabpanel" aria-labelledby="pills-register-tab">
           <form class="form form-horizontal has-validation-callback"  id="register_form" method="post">  
        
          
          <table class="table table-borderless">
            <tr>
              <td class="pt-3 text-right"><i class="fas fa-user"></i></td>
              <td><input type="text" name="uname" id="uname" class="form-control mb-2" placeholder="Enter your name" required minlength="6"  maxlength="50">
               <?php echo form_error('uname', '<div class="error" style="color:red;">', '</div>'); ?>

              </td>
            </tr>
            <tr>
              <td class="pt-3 text-right"><i class="fas fa-user"></i></td>
              <td><input type="text" id="cname" name="cname" class="form-control mb-2"  placeholder="Enter your Company name" required maxlength="100">
             <?php echo form_error('cname', '<div class="error" style="color:red;">', '</div>'); ?>

              </td>
            </tr>
            <tr>
              <td class="pt-3 text-right"><i class="fas fa-envelope"></i></td>
              <td><input type="email" name="uemail" id="uemail" class="form-control mb-2" placeholder="Enter your email" required>
                <?php echo form_error('uemail', '<div class="error" style="color:red;">', '</div>'); ?>

              </td>
            </tr>
            <tr>
              <td class="pt-3 text-right"><i class="fas fa-lock"></i></td>
              <td><input type="password" class="form-control mb-2" id="upassword" name="upassword" placeholder="Set a password" required>
                <?php echo form_error('upassword', '<div class="error" style="color:red;">', '</div>'); ?>

              </td>
            </tr>
            <tr>
              <td class="pt-3 text-right"><i class="fas fa-lock"></i></td>
              <td><input type="password" required class="form-control mb-2" name="ure_password" id="ure_password" placeholder="Re-enter the password">
                <?php echo form_error('ure_password', '<div class="error" style="color:red;">', '</div>'); ?>
                <span id='message' class="error"></span>
              </td>
            </tr>
            <tr>
              <td class="pt-3 text-right"><i class="fas fa-phone"></i></td>
              <td><input type="text" class="form-control mb-2" name="umobile" id="umobile"  placeholder="Enter your mobile" required>
               <?php echo form_error('umobile', '<div class="error" style="color:red;">', '</div>'); ?>

              </td>
            </tr>
            <tr>
              <td colspan="2" class="text-center">
                <button name="regSubmit" type="submit" class="btn btn-piquic ">Register <i class="fas fa-sign-in-alt"></i></button>
                <span id="success" class="successMsg" style="display:none;"></span>
                <!-- <span id="loding"><i class="fas fa-circle-notch text-success fa-spin"></i></span> -->
              </td>
            </tr>
          </table>
        </form>
<?php// echo form_close(); ?>
        </div>
      </div>
    </div>
  </div>
  </div>

</div>
<!-- Modal -->
<?php
$this->load->view('front/includes/footer');
?>


<script type="text/javascript">
$(function () {



   $('#btn_upld_img_nextpage').on('click', function(){
        <?php
          if($user_id!='')
          {
          ?>
          window.location.href = "<?php echo base_url();?>uploadimage";
          <?php
          }
          ?>

     
    });


      $("#login_form").validate({
        rules: {
          email:{
            required:true,
            email: true,
          },
          password: "required"
        },
    submitHandler: function (form) {
     /// alert("form")
     // form.submit();
      var email=$('#email').val();
      var password=$('#password').val();
        $.ajax({
          method:'post',
          url: "<?php echo base_url();?>front/home/check_login", 
          data:"&email="+email+"&password="+password,
          success: function(result){

            //alert($.trim(result));
            if($.trim(result)!=0)
            {
              //alert(1);
             // window.location.href = "<?php echo base_url();?>/home";
              window.location.href = "<?php echo base_url();?>uploadimage";
            }
            else{
              alert(2);
              $("#error_message").show();
            }
          }
        });
    }
  });
});
$(function () {
      $("#register_form").validate({
       rules: {
        uname:{
          required:true,
        },
         cname:{
          required:true,         
        },
        uemail:{
          required:true,
          email: true,
          <?php
          if($user_id=="")
          { ?>
            remote: {
              url: "<?php echo base_url();?>front/home/checkexistemail",
              type: "post"
            },
            <?php }  ?>   
        },
        <?php
        if($user_name=='')
        { ?>
          upassword:{
            required:true,
            minlength:5,
          },
          ure_password: {
            required:true,
            equalTo: "#upassword" ,
            minlength:5,
          },
          <?php  }  ?>
        umobile: {
         required: true,
         minlength:10,
         <?php
         if($user_name=="")
         { ?>
           remote: {
            url: "<?php echo base_url();?>front/home/checkexistmobile",
            type: "post"
          },
          <?php   }  ?>   
      },
    },
    submitHandler: function (form) {
      var user_id='';
      var uname=$('#uname').val();
      var cname=$('#cname').val();
      var uemail=$('#uemail').val();
      var umobile=$('#umobile').val();
      var upassword=$('#upassword').val();
        $.ajax({
          method:'post',
          url: "<?php echo base_url();?>front/home/create_user", 
          data:"&user_id="+user_id+"&uname="+uname+"&cname="+cname+"&uemail="+uemail+"&umobile="+umobile+"&upassword="+upassword,
          success: function(result){
            if(result)
            {
              $("#success_message").show();
              $('#uname').val("");
              $('#cname').val("");
              $('#uemail').val("");
              $('#umobile').val("");
              $('#upassword').val("");
              $('#ure_password').val("");
              $('#pills-login').addClass('active  show');
              $('#pills-login-tab').addClass('active');
              $('#pills-register').removeClass('active  show');
              $('#pills-register-tab').removeClass('active');
          
            }
          }
        });
    }
  });
});

  $(document).ready(function() {
      $('#btn_upld_img').on('click', function(){
        <?php
          if($user_id=='')
          {
          ?>
          //$('#myModal').modal('show');
          $('#myModal').modal({
            backdrop: 'static',
            keyboard: false
        })
          <?php
          }
          ?>

     
    });

  });

/* $(window).load(function(){        
   $('#myModal').modal('show');
    }); */

</script>


