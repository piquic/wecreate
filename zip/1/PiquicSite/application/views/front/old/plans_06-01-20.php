<?php
session_start();
error_reporting(0);
$data['page_title'] = "plans";

$this->load->view('front/includes/header',$data);
// $this->load->view('front/includes/menu');
$this->load->view('front/includes/nav');

if ($this->session->userdata('front_logged_in')) {
	$session_data = $this->session->userdata('front_logged_in');
	$user_id = $session_data['user_id'];
	$user_name = $session_data['user_name'];
	
	
	
} else {
	
	$session_data = $this->session->userdata('front_logged_in');
	$user_id = $session_data['user_id'];
	$user_name = $session_data['user_name'];
	
	
}

?>
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/bs4/dt-1.10.18/datatables.min.css"/>

<div class="container-fluid">
	<div class="row">

		<?php //$this->load->view('front/includes/sidebar'); ?>

		<!-- <div class="col-12 col-sm-12 col-md-9 col-lg-9 col-xl-9 border mainscroll"> -->
		<div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12 mainscroll">

			<div class="container pt-3">
				<div class="row mb-4">
					<div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
						<h1>Account Overview</h1>
					</div>
				</div>

				<?php
				$query=$this->db->query("select * from  tbl_users  WHERE user_id='$user_id' ");
				$userDetails= $query->result_array();
				$total_amount=$userDetails[0]['total_amount'];

//echo "select sum(billing_plan_amount) as sum_billing_plan_amount from  tbl_users_billing  WHERE user_id='$user_id' ";
				$query=$this->db->query("select sum(billing_plan_amount) as sum_billing_plan_amount from  tbl_users_billing  WHERE user_id='$user_id' ");
				$userDetails= $query->result_array();
				if(!empty($userDetails) && $userDetails[0]['sum_billing_plan_amount']!=NULL)
				{
					$sum_billing_plan_amount=$userDetails[0]['sum_billing_plan_amount'];

					$query=$this->db->query("select * from  tbl_users  WHERE user_id='$user_id' ");
					$userDetails= $query->result_array();
					$total_amount=$userDetails[0]['total_amount'];
				}
				else
				{
					$sum_billing_plan_amount='0';
				}



//echo $this->session->userdata('sess_plan_id')."kkk";
				if(!empty($this->session->userdata('sess_plan_id')))
				{
					$sess_currency = $this->session->userdata('sess_currency');
					$sess_plan_id = $this->session->userdata('sess_plan_id');
					$plansDetails = $this->plans_model->get_plansById($sess_plan_id);
					$hid_plantype_id=$plansDetails[0]['plntyid'];
					$hid_plan_id=$plansDetails[0]['planid'];
					$hid_plan_amount=$plansDetails[0]['plan_amount'];
					$hid_plan_credit=$plansDetails[0]['plan_credit'];

					$query = $this->db->query("select * from tbl_users WHERE user_id='$user_id' ");
					$userDetails = $query->result_array();
					$show_total_amount = $userDetails[0]['total_amount'];
					$show_total_credit = $userDetails[0]['total_credit'];

				}
				else
				{
					if ($this->session->userdata('front_logged_in')) {
						$session_data = $this->session->userdata('front_logged_in');
						$user_id = $session_data['user_id'];
						$user_name = $session_data['user_name'];

						$query=$this->db->query("select * from  tbl_users  WHERE user_id='$user_id' ");
						$userDetails= $query->result_array();
						$show_total_amount=$userDetails[0]['total_amount'];
						$show_total_credit=$userDetails[0]['total_credit'];


						
						$sess_currency = '';
						$sess_plan_id = '';
						$hid_plantype_id= '';
						$hid_plan_id= '';
						$hid_plan_amount= '';
						$hid_plan_credit= '';


						
						
						
					} else {
						
						$sess_currency = '';
						$sess_plan_id = '';
						$hid_plantype_id= '';
						$hid_plan_id= '';
						$hid_plan_amount= '';
						$hid_plan_credit= '';
						
						
					}


				}

				if($hid_plan_credit=='')
				{
					$hid_plan_credit='0';
				}

				?>

<input type="hidden" id="currency" name="currency" value="<?php echo $currency_default;?>">

<input type="hidden" name="hid_plantype_id" id="hid_plantype_id" value="<?php echo $hid_plantype_id?>">

<input type="hidden" name="hid_plan_amount" id="hid_plan_amount" value="<?php echo $hid_plan_amount?>">
<input type="hidden" name="hid_plan_credit" id="hid_plan_credit" value="<?php echo $hid_plan_credit?>">

				<form id="frm_plan" name="frm_plan" method="post" action="<?php echo base_url();?>payments" onSubmit="submitPlanFrm();" >
					<div class="row mb-4">
						<div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
							<div class="card">
								<div class="card-body">
									<div class="row">
										<div class="col-12 col-sm-12 col-md-3 col-lg-3 col-xl-3">
											<h2><!-- <i class="fas fa-rupee-sign"></i>&nbsp; -->
												<?php echo $show_total_credit;?> Images</h2>
												

												<input type="hidden" name="hid_plan_id" id="hid_plan_id" value="<?php echo $hid_plan_id?>">




												<small>Your Balance</small>
											</div>

											<div class="col-12 col-sm-12 col-md-9 col-lg-9 col-xl-9">
												<div class="text-center pt-3">
													
													<div class="row">
														<!-- <div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-2 pb-2"><?php echo $currency_default;?></div> -->
														<div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-8 pb-2">
															<div class="input-group mb-2">
																<div class="input-group-prepend">
																	<div class="input-group-text"><?php echo $currency_default;?></div>
																</div>
																<input type="text" class="form-control" name="inputAmount" id="inputAmount" placeholder="Enter amount to be added" value="<?php echo $hid_plan_amount?>" required="">
															</div>
															<!-- <input type="text" class="form-control" name="inputAmount" id="inputAmount" placeholder="Enter amount to be added" value="<?php echo $hid_plan_amount?>" required=""> -->
														</div>
														<div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-4 pb-2">
															<button type="submit" class="btn btn-piquic mb-2">Add Money</button>
														</div>
													</div>
													
												</div>
											</div>

											
											<!-- <div class="col-12 col-sm-12 col-md-3 col-lg-3 col-xl-3">
												<div class="pt-3">
													<select id="currency" name="currency" class="form-control" required="" onchange="changePlan(this.value);" disabled>
														<option value="INR" <?php if($currency_default=='INR') { ?> selected <?php } ?>>INR</option>
														<option value="USD" <?php if($currency_default=='USD')  { ?> selected <?php } ?>>USD</option>
														<option value="EUR" <?php if($currency_default=='EUR') {  ?> selected <?php } ?>>EUR</option>
														<option value="GBP" <?php if($currency_default=='GBP') {  ?> selected <?php } ?>>GBP</option>
														<option value="AUD" <?php if($currency_default=='AUD') {    ?> selected <?php } ?>>AUD</option>
														<option value="CNY" <?php if($currency_default=='CNY') {  ?> selected <?php } ?>>CNY</option>
													</select>
													<small>Set the currency at My Account Page</small>
												</div>
											</div> -->
										</div>
									</div>
								</div>
							</div>
						</div>

					</form>

					<div class="row my-4">					
						<div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
							<div class="full-w">
								<ul class="nav nav-tabs" id="myTab" role="tablist">
									<li class="nav-item px-3 text-uppercase">
										<a class="nav-link active" id="plans-slct-tab" data-toggle="tab" href="#plans-slct" role="tab" aria-controls="plans-slct" aria-selected="true">plans</a>
									</li>
									<li class="nav-item px-3 text-uppercase">
										<a class="nav-link" id="billing-tab" data-toggle="tab" href="#billing" role="tab" aria-controls="billing" aria-selected="false">billing history</a>
									</li>
									<!-- <li class="nav-item px-3 text-uppercase">
										<a class="nav-link" id="xxxxxx-tab" data-toggle="tab" href="#xxxxxx" role="tab" aria-controls="xxxxxx" aria-selected="false">xxxxxx</a>
									</li> -->
								</ul>


								<div class="tab-content" id="myTabContent">



									<div class="tab-pane fade show active" id="plans-slct" role="tabpanel" aria-labelledby="plans-slct-tab">
										<div class="row p-2">

											<?php
                   //echo "<pre>";
                   //print_r($plansDetails);
											if(!empty($plansTypeDetails))
											{
												foreach($plansTypeDetails as $key => $planrow)
												{
													$plntyid=$planrow['plntyid'];
													$plntypname=$planrow['plntypname'];
													$plandetails=$planrow['plandetails'];
													
													?>
													<div class="col-12 col-sm-12 col-md-6 col-lg-6 col-xl-6">
														<div class="p-4">
															<p class="lead"><?php echo $plntypname;?></p>
															<small class="text-muted">
																Designed for infrequent sending, you buy images ( as opposed to recurring charges) as necessary, based on volume.
															</small>

															<div class="row pt-3 <?php if($plntyid=='1'){ echo 'clickbone-group'; } if($plntyid=='2'){ echo 'clickbmon-group'; } ?>">
																<div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12 pb-3">
																	<small><b>How many images would you like to purchase?</b></small>
																</div>

																<?php
                   //echo "<pre>";
                   //print_r($plandetails);
																if(!empty($plandetails))
																{
																	foreach($plandetails as $key1 => $planrow1)
																	{
																		$planid=$planrow1['planid'];
																		$plan_name=$planrow1['plan_name'];
																		$plan_amount=$planrow1['plan_amount'];
																		$plan_credit=$planrow1['plan_credit'];
																		$plan_currency=$planrow1['plan_currency'];
																		$plan_currency_symbol=$planrow1['plan_currency_symbol'];
																		$plan_per_image=$planrow1['plan_per_image'];																
																		
																		?>

																		<div class="col-12 col-sm-12 col-md-6 col-lg-6 col-xl-6 <?php if($plntyid=='1'){ echo 'clickb'; } if($plntyid=='2'){ echo 'clickbmon'; } ?> " style="cursor: pointer;" onclick="setAmount('<?php echo $plntyid;?>','<?php echo $planid;?>','<?php echo $plan_amount;?>','<?php echo $plan_credit;?>');">
																			<div id="planSelectbox<?php echo $planid;?>" class="planSelectbox w-100 text-center mb-3 py-3 border <?php if($sess_plan_id==$planid) { ?> border-piquic <?php } ?> ">
																				<?php echo $plan_credit;?> Images<br>
																				<b><?php echo $plan_currency_symbol;?><?php echo $plan_amount;?><?php if($plntyid == '2') { echo "/month"; } ?></b>
																				<br>
																				<small class="text-muted">(<?php echo $plan_currency_symbol;?><?php echo $plan_per_image;?> per image)</small>
																			</div>
																			<span id="planSelectboxTick<?php echo $planid;?>" class="planSelectboxTick text-piquic slct <?php if($sess_plan_id!=$planid) { ?> d-none <?php } ?> " id="ticko1" ><i class="fas fa-check"></i></span>
																		</div>

																		

																		<?php
																	}

																}

																else
																{
																	?>
																	<div class="col-12 col-sm-12 col-md-6 col-lg-6 col-xl-6">
																		<div class="p-4">
																			<p class="lead">No Plans</p>
																		</div>
																	</div>
																	<?php	
																}
																?>		




															</div>
														</div>
													</div>


													<?php
												}

											}
											else
											{
												?>
												<div class="col-12 col-sm-12 col-md-6 col-lg-6 col-xl-6">
													<div class="p-4">
														<p class="lead">No Plans</p>
													</div>
												</div>
												<?php
											}
											?>

											


										</div>
									</div>

									<div class="tab-pane fade" id="billing" role="tabpanel" aria-labelledby="billing-tab">
										<div class="p-3">




											<?php
                   //echo "<pre>";
                   //print_r($plandetails);
											if(!empty($billingDetails))
											{
												foreach($billingDetails as $keybill => $planrowbill)
												{
													$billing_id=$planrowbill['billing_id'];
													$billing_unique_id=$planrowbill['billing_unique_id'];
													$billing_plan_id=$planrowbill['billing_plan_id'];
													$billing_plan_credit=$planrowbill['billing_plan_credit'];
													$billing_plan_amount=$planrowbill['billing_plan_amount'];
													$billing_date_time=$planrowbill['billing_date_time'];
													$billing_status=$planrowbill['billing_status'];
													$payment_status=$planrowbill['payment_status'];

													$plntypname=$planrowbill['plntypname'];

													$billing_date_time=date("D, M d ,Y H:i A",strtotime($billing_date_time));
													
													?>


													<div id="plan_1" class="border-bottom p-3">
														<div class="d-flex justify-content-between">
															<div>
																<span class="lead text-piquic"><?php echo $billing_unique_id;?></span><br>
																<small>
																	<?php echo $plntypname;?> ( <?php echo $billing_plan_credit;?> images. )<br>
																	
																	<?php echo $billing_date_time;?>
																</small>
															</div>

															<div class="pt-3">
																<span class="lead">
																	<?php
																if($currency_default=="USD")
																{
																	echo"&#36;";
																}
																elseif($currency_default=="INR")
																{
																	echo"&#x20B9;";	
																}
																elseif($currency_default=="EUR")
																{
																	echo"&#128;";
																}
																elseif($currency_default=="GBP")
																{
																	echo"&#163;";
																}
																elseif($currency_default=="AUD")
																{
																	echo"&#36;";
																}
																elseif($currency_default=="CNY")
																{
																	echo"&#165;";
																}
																?>&nbsp;<?php echo $billing_plan_amount;?></span>
															</div>

											<!-- 	<div class="pt-3">
													<button type="submit" class="btn btn-outline-piquic mb-2">View</button>
												</div> -->
											</div>
										</div>

										<?php
									}

								}

								else
								{
									?>
									<div id="plan_1" class="border-bottom p-3">
										<div class="d-flex justify-content-between">
											<div>
												<span class="lead text-piquic">No History</span>
											</div>
										</div>
										<?php
									}
									?>





								</div>
							</div>

							<div class="tab-pane fade" id="xxxxxx" role="tabpanel" aria-labelledby="xxxxxx-tab">...</div>
						</div>
					</div>
				</div>

			</div>
		</div>
	</div>
</div>
</div>

<?php
$this->load->view('front/includes/footer');
?>

<script type="text/javascript" src="https://cdn.datatables.net/v/bs4/dt-1.10.18/datatables.min.js"></script>

<script type="text/javascript">

	$(document).ready(function() {

		changePlan('<?php echo $currency_default;?>');

		$(".clickbone-group .clickb ").on("click", function(e) {

			$('.clickbmon div').removeClass('border-piquic');
			$('.clickbmon span').addClass('d-none');
			$(this).parent().find('.clickb div').removeClass('border-piquic');
			$(this).parent().find('.clickb span').addClass('d-none');



			$(this).children('div').addClass('border-piquic');
			$(this).children('span').removeClass('d-none');			       
		});

		$(".clickbmon-group .clickbmon ").on("click", function(e) {
			$('.clickb div').removeClass('border-piquic');
			$('.clickb span').addClass('d-none');

			$(this).parent().find('.clickbmon div').removeClass('border-piquic');
			$(this).parent().find('.clickbmon span').addClass('d-none');
			$(this).children('div').addClass('border-piquic');
			$(this).children('span').removeClass('d-none');			       
		});


// $("#clreyemk").on("click", function() {
	
// 			$('.rdeyemk-group').find('.rdeyemk div img').removeClass('border');
// 		});


})


</script>

<script type="text/javascript">
	

	function changePlan(currency)
	{
//alert(currency);
$.ajax({
	type:'POST',					
	url : "<?php echo base_url();?>front/plans/getPlanDetailByCurrency", 
	data:{'currency':currency},
	success: function(response) {	
				 //alert(response);
				//location.reload();
				$("#plans-slct").html($.trim(response));
			}
		});
}


$('#set-currency').on('change', function(){
	var selected = $(this).val();
        // alert(selected);
        
        $.post('<?php echo site_url( $this->uri->segment(1) . '/setcurrency'); ?>', {currency: selected}, function() {
        	location.reload();
        });
     });

function setAmount(hid_plantype_id,planid,plan_amount,plan_credit)
{

	$("#hid_plantype_id").val(hid_plantype_id);
	$("#hid_plan_id").val(planid);
	$("#hid_plan_amount").val(plan_amount);
	$("#hid_plan_credit").val(plan_credit);
	$("#inputAmount").val(plan_amount);

	$(".planSelectbox").removeClass("border-piquic");
	$(".planSelectboxTick").addClass("d-none");

	$("#planSelectbox"+planid).addClass("border-piquic");
	$("#planSelectboxTick"+planid).removeClass("d-none");
}

function submitPlanFrm()
{
	var inputAmount = $("#inputAmount").val();
	var currency = $("#currency").val();
	//alert(inputAmount);
	//alert(currency);
	if($.trim(inputAmount)!='' )
	{
		document.frm_plan.submit();
	}
	else
	{
		alert("Select Plan");
		return false;
	}

}

</script>

