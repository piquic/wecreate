<?php
$data['page_title'] = "style queue";

$this->load->view('front/includes/header',$data);
// $this->load->view('front/includes/menu');		
$this->load->view('front/includes/nav');


$second_DB = $this->load->database('another_db', TRUE); 
?>


<div class="container-fluid">
	<div class="row mb-4">
		<?php $this->load->view('front/includes/sidebar'); ?>
		

		<div class="col-12 col-sm-12 col-md-9 col-lg-9 col-xl-9 border">

			<div class="container pt-3">
				<div class="row mb-4">
					<div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
						<div class="row">
							<div class="col-12 col-sm-12 col-md-4 col-lg-4 col-xl-4">&nbsp;</div>
							<div class="col-12 col-sm-12 col-md-4 col-lg-4 col-xl-4">&nbsp;</div>
							<div class="col-12 col-sm-12 col-md-4 col-lg-4 col-xl-4">
								<div class="d-flex justify-content-around">
									<!-- <span class="text-piquic pr-2" data-toggle="collapse" data-target="#filter-it"><i class="fas fa-sliders-h fa-2x"></i></span> -->

									<!-- <span class="text-piquic pr-2"><i class="fas fa-sliders-h fa-2x"></i></span> -->

									<div class="form-inline flex-grow-1">
										<label class="sr-only" for="searchSKU">Search SKU</label>
										<input type="Search SKU" class="form-control" id="searchSKU" placeholder="Search SKU"  value="<?php echo $sku_key;?>">

										<button class="btn btn-piquic " type="submit"><i class="fas fa-search " onclick="searchSKU();"></i></button>
									</div>
									
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>

			<div class="bg-piquic full-w" style="min-height: .5px;">
				<div class="container">
					<div class="row mb-4">
						<div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
							<div id="filter-it" class="row text-white d-none">
								<div class="col-12 col-sm-12 col-md-2 col-lg-2 col-xl-2 pl-4">
									<p class="pt-3" id="itemcount">0 item<small>(s)</small> selected</p>
								</div>

								<div class="col-12 col-sm-12 col-md-9 col-lg-9 col-xl-9 pl-4">
									<div class="row">



                               <div class="col-12 col-sm-12 col-md-3 col-lg-3 col-xl-3">

								<select class="custom-select navSlct mt-2 text-white" id="gend_SKU" onchange="getModelByGender(this);">
								<option class="text-piquic"  >Select Gender</option>
								<option  class="text-piquic" value="1" >Male</option>
								<option class="text-piquic"  value="2" >Female</option>
								<option class="text-piquic"  value="3" >Boys (0-2 years)</option>
								<option class="text-piquic"  value="4" >Boys (2-9 years)</option>
								<option class="text-piquic"  value="5" >Girls (0-2 years)</option>
								<option class="text-piquic"  value="6" >Girl (2-9 years) </option>
								</select>
										</div>


										<div class="col-12 col-sm-12 col-md-3 col-lg-3 col-xl-3">
											<div id="model_search_div">
											<select class="custom-select navSlct mt-2 text-white" name="slct_model" id="slct_model" onchange="modlmultsku(this);">
												<option class="text-piquic" value="0" >Model</option>
												<?php 


												/*$mquery = "SELECT * FROM `tbl_model` where status='1' ORDER by `m_id`;";
												$mresult=$conn->query($mquery);
												while($mrow=$mresult->fetch_assoc()){*/

													$pautoquery = "SELECT * FROM `tbl_model` where status='1' ORDER by `m_id` ";
											// echo $pautoquery;
													$query2=$second_DB->query($pautoquery);
													$modelinfo= $query2->result_array();

													foreach($modelinfo as $mrow)
													{


														$mid = $mrow['m_id'];
														$mod_name = $mrow['mod_name'];
														$mod_name =strtoupper($mod_name);
														?>
														<option class="text-piquic" value="<?php echo $mid;?>"><?php echo $mod_name;?></option>
													<?php } ?>
												</select>
											</div>
											</div>

											<div class="col-12 col-sm-12 col-md-3 col-lg-3 col-xl-3">
												<select class="custom-select navSlct mt-2 text-white" name="autostyle" id="autostyle" onchange="autostylingsku(this);">											
													<option class="text-piquic" value="0">Auto Styling</option>
													<option class="text-piquic" value="1">Yes</option>
												</select>
											</div>

									  <!-- <div class="col-12 col-sm-12 col-md-3 col-lg-3 col-xl-3">
											<select class="custom-select navSlct mt-2 text-white" name="vendorsel" id="vendorsel" onchange="vendorselsku(this);">		
												<option class="text-piquic" value="0">Select Vendor</option>

										<?php 
										$CI = &get_instance();
										$this->db2 = $CI->load->database('another_db', TRUE);
										$query = $this->db2->query("select * from  tbl_vendor WHERE client_name='SaketStudio1'");
										$vendor=$query->result();
										foreach($vendor as $venrow){											 
										$vname=$venrow->vendor_name;            
										$id=$venrow->id;              
										?>
							            <option class="text-piquic" value="<?php echo $id;?>"><?php echo $vname;?></option>

										<?php } ?>



											</select>
										</div> -->

									</div>
								</div>
<!-- 
								<div class="col-12 col-sm-12 col-md-1 col-lg-1 col-xl-1 pt-3 pl-4">
									<span data-toggle="collapse" data-target="#filter-it"><i class="fas fa-times"></i></span>
								</div> -->
							</div>
						</div>
					</div>
				</div>
			</div>

			<div class="container pt-3">
				<div class="row mb-4">
					<div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
						<div class="bg-white">
							<div class="row mb-3">
								<div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
									<div class="custom-control custom-checkbox">
										<input type="checkbox" class="custom-control-input" id="chbkSlctAll" >
										<label class="custom-control-label roundCheck lead" for="chbkSlctAll">Select All SKUs</label>
									</div>
								</div>
							</div>

							<div class="row">
								<div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">

									<?php foreach($stylequeinfo as $querow){        
										$id=$querow->upimg_id;
										$sku=$querow->zipfile_name;
										$category=$querow->category;
										$gender=$querow->gender;

										if($gender=='1')
										{
										$gender_name='Male';
										}
										else if($gender=='2')
										{
										$gender_name='Female';
										}
										else if($gender=='3')
										{
										$gender_name='Boys (0-2 years)';
										}
										else if($gender=='4')
										{
										$gender_name='Boys (2-9 years)';
										}
										else if($gender=='5')
										{
										$gender_name='Girls (0-2 years)';
										}
										else if($gender=='6')
										{
										$gender_name='Girl (2-9 years)';
										}
										else
										{
											$gender_name='';
										}


										?>

										<div id="SKU_<?php echo $id; ?>" class="pb-5">
											<div class="custom-control custom-checkbox">
												<input type="checkbox" class="custom-control-input checkSingle" id="chbkSKU_<?php echo $id; ?>" value="<?php  echo $sku; ?>">
												<label class="custom-control-label roundCheck lead" for="chbkSKU_<?php echo $id; ?>">
													SKU: <?php echo $sku;?> 
												</label>
												<!-- <span class="text-danger lead"><i class="far fa-times-circle"></i></span> -->
											</div>
											<div class="row pt-3 pl-md-4">
												<div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
													<p class="lead">Gender: <?php echo ucfirst($gender_name);?> | Category: <?php echo ucfirst($category);?></p>
												</div>
											</div>

											<div class="row pt-3 pl-md-4">
												<div class="col-12 col-sm-12 col-md-3 col-lg-3 col-xl-3">

													<div id="carousel_SKU<?php echo $sku;?>" class="carousel slide" data-ride="carousel">
														<div class="carousel-inner">
															<?php
															$i = 0;
															$query=$this->db->query("select * from  tbl_upload_images where upimg_id='$id'");   
															foreach ($query->result() as $resrow)
															{ 
																$img=$resrow->img_name;
																$imgid=$resrow->id;
																
															?>
															<script type="text/javascript">

																$(window).on('load', function(){

																	var img = document.getElementById('img_<?php echo $imgid; ?>');

																	var width = img.naturalWidth;
																	var height = img.naturalHeight;

																	if (width < height){
																		$('#img_<?php echo $imgid; ?>' ).removeClass('img').addClass('img-fluid');
																	}
																});

															</script>
															<div class="carousel-item <?php if( $i == 0 ){ ?> active <?php } ?>">						<div class="img-box">
																	<img id="img_<?php echo $imgid; ?>" src="<?php echo base_url().'upload/'.$user_id.'/'.$sku.'/'.$img ;?>" class="img" alt="...">
																</div>

															</div>

															<?php $i++; } ?>														
														</div>

														<a class="carousel-control-prev" href="#carousel_SKU<?php echo $sku;?>" role="button" data-slide="prev">
															<span class="carousel-control-prev-icon" aria-hidden="true"></span>
															<span class="sr-only">Previous</span>
														</a>
														<a class="carousel-control-next" href="#carousel_SKU<?php echo $sku;?>" role="button" data-slide="next">
															<span class="carousel-control-next-icon" aria-hidden="true"></span>
															<span class="sr-only">Next</span>
														</a>
													</div>
												</div>

												<?php 
          //echo "select * from  tbl_style where skuno='$sku' and user_id='$user_id' ";
												$query=$this->db->query("select * from  tbl_style where skuno='$sku' and user_id='$user_id' ");
												$row2 = $query->row();
												$sty_id=$row2->sty_id;
												$genname=$row2->gender;
												$catname=$row2->category;
												$model=$row2->model_id;
												$poseids=$row2->poseids;
												$top=$row2->top;
												$bottom=$row2->bottom;
         //$dress=$row2->dress;
												$shoes=$row2->shoes;
												$accessories=$row2->accessories;
												$makeup=$row2->makeup;
												$status=$row2->status;
												$autostyle=$row2->autostyle;

												?>

												<div class="col-12 col-sm-12 col-md-9 col-lg-9 col-xl-9">
													<div class="row pb-3">
														<div class="col-12 col-sm-12 col-md-3 col-lg-3 col-xl-3 pb-4">
															<div class="d-flex justify-content-between">
																<span class="lead">
																	Model&nbsp;
																	<span class="text-piquic <?php if($model==''){ echo 'd-none';} ?>" ><i class="fas fa-check-circle"></i></span>
																</span>

															</div>

															<small class="text-danger <?php if($model!=''){ echo 'd-none';} ?>"><i class="fas fa-exclamation-circle"></i>&nbsp;Missing</small>

														</div>

														<div class="col-12 col-sm-12 col-md-3 col-lg-3 col-xl-3 pb-4">
															<div class="d-flex justify-content-between">
																<span class="lead">
																	Poses&nbsp;<span class="text-piquic <?php if($poseids==''){ echo 'd-none';} ?>"><i class="fas fa-check-circle"></i></span>
																</span>
															</div>
															<small class="text-danger <?php if($poseids!=''){ echo 'd-none';} ?>"><i class="fas fa-exclamation-circle"></i>&nbsp;Missing</small>
														</div>

														<?php if($catname=='bottom'){?>
															<div class="col-12 col-sm-12 col-md-3 col-lg-3 col-xl-3 pb-4">
																<div class="d-flex justify-content-between">
																	<span class="lead">
																		Top&nbsp;<span class="text-piquic <?php if($top==''){ echo 'd-none';} ?>"><i class="fas fa-check-circle"></i></span>
																	</span>
																</div>
																<small class="text-danger <?php if($top!=''){ echo 'd-none';} ?>"><i class="fas fa-exclamation-circle"></i>&nbsp;Missing</small>
															</div>
														<?php } ?>

														<?php if($catname=='top'){?>
															<div class="col-12 col-sm-12 col-md-3 col-lg-3 col-xl-3 pb-4">
																<div class="d-flex justify-content-between">
																	<span class="lead">
																		Bottom&nbsp;<span class="text-piquic <?php if($bottom==''){ echo 'd-none';} ?>"><i class="fas fa-check-circle"></i></span>
																	</span>
																</div>
																<small class="text-danger <?php if($bottom!=''){ echo 'd-none';} ?>"><i class="fas fa-exclamation-circle"></i>&nbsp;Missing</small>
															</div>
														<?php } ?>

														<div class="col-12 col-sm-12 col-md-3 col-lg-3 col-xl-3 pb-4">
															<div class="d-flex justify-content-between">
																<span class="lead">
																	Shoes&nbsp;<span class="text-piquic <?php if($shoes==''){ echo 'd-none';} ?>"><i class="fas fa-check-circle"></i></span>
																</span>
															</div>
															<small class="text-danger <?php if($shoes!=''){ echo 'd-none';} ?>"><i class="fas fa-exclamation-circle"></i>&nbsp;Missing</small>
														</div>

														<div class="col-12 col-sm-12 col-md-3 col-lg-3 col-xl-3 pb-4">
															<div class="d-flex justify-content-between">
																<span class="lead">
																	Accessories&nbsp;<span class="text-piquic  <?php if($accessories==''){ echo 'd-none';} ?>"><i class="fas fa-check-circle"></i></span>
																</span>
															</div>
															<small class="text-warning <?php if($accessories!=''){ echo 'd-none';} ?>"><i class="fas fa-exclamation-circle"></i>&nbsp;Optional</small>
														</div>

														<?php if($genname=='2' || $genname=='5' || $genname=='6' ){?>
															<div class="col-12 col-sm-12 col-md-3 col-lg-3 col-xl-3 pb-4">
																<div class="d-flex justify-content-between">
																	<span class="lead">
																		Makeup&nbsp;<span class="text-piquic <?php if($makeup==''){ echo 'd-none';} ?> "><i class="fas fa-check-circle"></i></span>
																	</span>
																</div>
																<small class="text-warning  <?php if($makeup!=''){ echo 'd-none';} ?>"><i class="fas fa-exclamation-circle"></i>&nbsp;Optional</small>
															</div>
														<?php } ?>


													</div>



							<div class="row pb-3"> 

								<?php if($catname=='dress'){?> 

									<?php if($shoes=='' || $model=='' || $poseids=='' || $top==''){ ?>
						<?php
						if($autostyle=='1')
						{
						?>
						<button type="submit" class="btn btn-outline-secondary btn-block disabled" id="btn_style_1223334444">Send for Processing</button>
						<?php
						}
						else
						{
						?>

						<button type="button" class="btn btn-outline-dark btn-block" id="btn_style" onclick="sendmainstyle('<?php echo $sku; ?>');" >Complete Styling</button>

						<?php } ?>

						<?php } ?>

									<?php if($shoes!='' && $model!='' && $poseids!=''&& $top!=''){ ?>


<?php
if($status=='2')
{
?>
    <button type="submit" class="btn btn-outline-dark btn-block disabled" id="btn_style_1223334444">Sent&nbsp;<i class="fas fa-check"></i></button>
<?php
}
else
{
	?>
    <button type="button" class="btn btn-outline-dark btn-block" id="btn_style" onclick="sendprocessing('<?php echo $sty_id;?>','<?php echo $sku; ?>');">Send for Processing</button>
	<?php
}
?>

										





									<?php } }?>


									<?php if($catname=='bottom'){?> 

										<?php if($shoes=='' || $model=='' || $poseids=='' || $top==''){ ?>
<?php
if($autostyle=='1')
{
?>
    <button type="submit" class="btn btn-outline-secondary btn-block disabled" id="btn_style_1223334444">Send for Processing</button>
<?php
}
else
{
	?>
    <button type="button" class="btn btn-outline-dark btn-block" id="btn_style" onclick="sendmainstyle('<?php echo $sku; ?>');" >Complete Styling</button>
	<?php
}
?>

		




										<?php } ?>

										<?php if($shoes!='' && $model!='' && $poseids!=''&& $top!=''){ ?>

											<?php
if($status=='2')
{
?>
    <button type="submit" class="btn btn-outline-dark btn-block disabled" id="btn_style_1223334444">Sent&nbsp;<i class="fas fa-check"></i></button>
<?php
}
else
{
	?>
											<button type="button" class="btn btn-outline-dark btn-block" id="btn_style" onclick="sendprocessing('<?php echo $sty_id;?>','<?php echo $sku; ?>');">Send for Processing</button>

											<?php
										}
										?>

										<?php } }?>

										<?php if($catname=='top'){?> 

											<?php if($shoes=='' || $model=='' || $poseids=='' || $bottom==''){ ?>
<?php
if($autostyle=='1')
{
?>
    <button type="submit" class="btn btn-outline-secondary btn-block disabled" id="btn_style_1223334444">Send for Processing</button>
<?php
}
else
{
	?>


												<button type="button" class="btn btn-outline-dark btn-block" id="btn_style" onclick="sendmainstyle('<?php echo $sku; ?>');">Complete Styling</button>

												<?php
											}
											?>
											<?php } ?>

											<?php if($shoes!='' && $model!='' && $poseids!=''&& $bottom!=''){ ?>


												<?php
if($status=='2')
{
?>
    <button type="submit" class="btn btn-outline-dark btn-block disabled" id="btn_style_1223334444">Sent&nbsp;<i class="fas fa-check"></i></button>
<?php
}
else
{
	?>

												<button type="button" class="btn btn-outline-dark btn-block" id="btn_style" onclick="sendprocessing('<?php echo $sty_id;?>','<?php echo $sku; ?>');">Send for Processing</button>


												<?php
											}
												?>

											<?php } }?>



										</div>
									</div>
								</div>
							</div>

												<?php } ?>

												<input type="hidden" name="arrval" id="arrval">
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>

					</div>
				</div>
			</div>

			<?php
			$this->load->view('front/includes/footer');
			?>

			<script type="text/javascript">

			function searchSKU(){
			var sku =$('#searchSKU').val();
			window.location.href = "<?php echo base_url();?>stylequeue/"+sku ;

			}



				function getModelByGender(selectObject)
				{
					var gender_id=selectObject.value;

					$.ajax({
					type:'POST',					
					url : "<?php echo base_url();?>front/stylequeue/getModelByGender", 
					data:{'gender_id':gender_id},
					success: function(resultdiv) {	
					//alert(resultdiv);
					$('#model_search_div').html(resultdiv);
					}
					});
				}



				function vendorselsku(selectObject){
				var vendor = selectObject.value;
				var sku =$('#arrval').val();
				$.ajax({
				type:'POST',					
				url : "<?php echo base_url();?>front/stylequeue/vendorsku", 
				data:{'skuno':sku,'vendor':vendor},
				success: function(mulcatres) {	
				// alert(mulcatres);
				// location.reload();
				}
				});
				}






				function autostylingsku(selectObject){
				var autoval = selectObject.value;
				//alert(autoval);
				var sku =$('#arrval').val();
				if(autoval!='' && autoval!='0')
				{
					$.ajax({
					type:'POST',					
					url : "<?php echo base_url();?>front/stylequeue/autostylingskufuc", 
					data:{'skuno':sku,'autoval':autoval},
					success: function(autostyres) {	
					// alert(autostyres);
					location.reload();
					}
					});
				}
				


				}



				function modlmultskuSidebar(mid){
				var modval = mid;
				var sku =$('#arrval').val();

                


                

				if(sku!='')
				{
					$("#model_sel_"+modval).addClass("border-piquic");
    			    $("#span_model_sel_"+modval).removeClass("d-none");

					$.ajax({
					type:'POST',					
					url : "<?php echo base_url();?>front/stylequeue/mulmodelupdate", 
					data:{'skuno':sku,'model':modval},
					success: function(mulcatres) {	
					// alert(mulcatres);
					location.reload();
					}
					});	
				}
				else
				{
					alert("Select at least one SKU.");
				}
				



				}









				function modlmultsku(selectObject){
				var modval = selectObject.value;
				var sku =$('#arrval').val();
				$.ajax({
				type:'POST',					
				url : "<?php echo base_url();?>front/stylequeue/mulmodelupdate", 
				data:{'skuno':sku,'model':modval},
				success: function(mulcatres) {	
				// alert(mulcatres);
				location.reload();
				}
				});
				}	

				function sendmainstyle(sku){
					var sku=sku;
					window.location = "<?php echo base_url();?>style/"+sku;
				}

				function sendprocessing(sty_id,sku){

					//alert(sty_id);
					//alert(sku);

				//window.location = "<?php //echo base_url();?>style" ;

				$.ajax({
				type:'POST',					
				url : "<?php echo base_url();?>front/stylequeue/updateprocessing", 
				data:{'skuno':sku,'sty_id':sty_id},
				success: function(mulcatres) {	
				//alert(mulcatres);
				console.log(mulcatres);
				//window.location = "<?php //echo base_url();?>processing" ;
				location.reload();
				}
				});

				}

$(document).ready(function() {

	
	var chksku = []; 
	$("#chbkSlctAll").change(function(){
		if(this.checked){
			$("#filter-it").removeClass('d-none');
			$(".checkSingle").each(function(){
				this.checked=true; 
				var count=$('.checkSingle').filter(':checked').length;
				$('#itemcount').html(count+' item selected');
				chksku = $(".checkSingle:checked").map(function (i) {
					return $(this).val();
				}).get();
				$('#arrval').val(chksku);
			}) 
		}else{
			$("#filter-it").addClass('d-none');
			$(".checkSingle").each(function(){
				$('#itemcount').html(' 0 item selected');
				this.checked=false;
				chksku = $(".checkSingle:checked").map(function (i) {
					return $(this).val();
				}).get();
				$('#arrval').val(chksku);
			}) 
		}
	});

	$(".checkSingle").click(function () {
		var count=$('.checkSingle').filter(':checked').length;
		$('#itemcount').html(count+' item selected'); 
		chksku = $(".checkSingle:checked").map(function (i) {
			return $(this).val();
		}).get();
		$('#arrval').val(chksku);
		if(count>0){
			$("#filter-it").removeClass('d-none');
		}else{
			$("#filter-it").addClass('d-none');
		}

		if ($(this).is(":checked")){

			var isAllChecked = 0; 
			$(".checkSingle").each(function(){
				if(!this.checked){
					isAllChecked = 1;
				}
			}) 
			if(isAllChecked == 0){ $("#chbkSlctAll").prop("checked", true); } 
		}else {

			$("#chbkSlctAll").prop("checked", false);
		}
	});
});


</script>