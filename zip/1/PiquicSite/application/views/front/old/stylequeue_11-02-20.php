<?php
session_start();
error_reporting(0);
$data['page_title'] = "style queue";

$this->load->view('front/includes/header',$data);
// $this->load->view('front/includes/menu');		
$this->load->view('front/includes/nav');


$second_DB = $this->load->database('another_db', TRUE); 


?>


<div class="container-fluid">
	<div class="row">
		<?php $this->load->view('front/includes/sidebar'); ?>
		

		<!-- <div class="col-12 col-sm-12 col-md-9 col-lg-9 col-xl-9 border mainscroll"> -->
			<div class="col-12 col-sm-12 col-md-9 col-lg-9 col-xl-9 border ">

			<div class="container pt-3">
				<div class="row mb-4">
					<div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
						<div class="row">
							<div class="col-12 col-sm-12 col-md-4 col-lg-4 col-xl-4">&nbsp;</div>
							<div class="col-12 col-sm-12 col-md-4 col-lg-4 col-xl-4">&nbsp;</div>
							<div class="col-12 col-sm-12 col-md-4 col-lg-4 col-xl-4">
								<div class="d-flex justify-content-around">
									<!-- <span class="text-piquic pr-2" data-toggle="collapse" data-target="#filter-it"><i class="fas fa-sliders-h fa-2x"></i></span> -->

									<!-- <span class="text-piquic pr-2"><i class="fas fa-sliders-h fa-2x"></i></span> -->

									<div class="form-inline flex-grow-1">
										<label class="sr-only" for="searchSKU">Search SKU</label>
										<form id="formsearchSKU">	
										<input type="Search SKU" class="form-control" id="searchSKU" placeholder="Search SKU"  value="<?php echo $sku_key;?>">

										<button class="btn btn-piquic " type="submit" ><i class="fas fa-search " ></i></button>

											 <!-- <input type="submit" class="btn btn-piquic "/> -->
									    </form>

										
									</div>
									
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>

			<div class="bg-piquic full-w" style="min-height: .5px;">
				<div class="container">
					<div class="row mb-4">
						<div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
							<div id="filter-it" class="row text-white d-none">
								<div class="col-12 col-sm-12 col-md-2 col-lg-2 col-xl-2 pl-4">
									<p class="pt-3" id="itemcount">0 item<small>(s)</small> selected</p>
								</div>

								<!-- <div class="col-12 col-sm-12 col-md-9 col-lg-9 col-xl-9 pl-4">
									<div class="row">

										<div class="col-12 col-sm-12 col-md-3 col-lg-3 col-xl-3">

											<select class="custom-select navSlct mt-2 text-white" id="gend_SKU" onchange="getModelByGender(this);">
												<option class="text-piquic"  >Select Gender</option>
												<option  class="text-piquic" value="1" >Male</option>
												<option class="text-piquic"  value="2" >Female</option>
												<option class="text-piquic"  value="3" >Boys (0-2 years)</option>
												<option class="text-piquic"  value="4" >Boys (2-9 years)</option>
												<option class="text-piquic"  value="5" >Girls (0-2 years)</option>
												<option class="text-piquic"  value="6" >Girl (2-9 years) </option>
											</select>
										</div>


										<div class="col-12 col-sm-12 col-md-3 col-lg-3 col-xl-3">
											<div id="model_search_div">
												<select class="custom-select navSlct mt-2 text-white" name="slct_model" id="slct_model" onchange="modlmultsku(this);">
													<option class="text-piquic" value="0" >Model</option>
													<?php 

													$pautoquery = "SELECT * FROM `tbl_model` where status='1' ORDER by `m_id` ";
													$query2=$second_DB->query($pautoquery);
													$modelinfo= $query2->result_array();

													foreach($modelinfo as $mrow)
													{


														$mid = $mrow['m_id'];
														$mod_name = $mrow['mod_name'];
														$mod_name =strtoupper($mod_name);
														?>
														<option class="text-piquic" value="<?php echo $mid;?>"><?php echo $mod_name;?></option>
													<?php } ?>
												</select>
											</div>
										</div>

										<div class="col-12 col-sm-12 col-md-3 col-lg-3 col-xl-3">
											<select class="custom-select navSlct mt-2 text-white" name="autostyle" id="autostyle" onchange="autostylingsku(this);">											
												<option class="text-piquic" value="0">Auto Styling</option>
												<option class="text-piquic" value="1">Yes</option>
											</select>
										</div>

									</div>
								</div> -->
							</div>
						</div>
					</div>
				</div>
			</div>

			<div class="container pt-3">
				<div class="row mb-4">
					<div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
						<div class="bg-white">
							<div class="row mb-3">
								<div class="col-12 col-sm-12 col-md-4 col-lg-4 col-xl-4 order-md-3">
									<div class="text-md-right pb-3">
										<select class="custom-select" id="filter_style">
											<option value="">Select</option>
											<!-- <option <?php if($filter_val=="0" || $filter_val=="" ) echo "selected"; ?> value="0">Complete Styling</option>
											<option <?php if($filter_val=="2") echo "selected"; ?> value="2">Sent for processing</option>
											<option <?php if($filter_val=="3") echo "selected"; ?> value="3">Send for processing</option> -->


											<option <?php if($filter_val=="0" || $filter_val=="" ) echo "selected"; ?> value="0">Styling Pending</option>
											<option <?php if($filter_val=="3") echo "selected"; ?> value="3">Styling Completed</option>
											<option <?php if($filter_val=="2") echo "selected"; ?> value="2">Sent for processing</option>
											
										</select>
									</div>
								</div>

								<div class="col-0 col-sm-0 col-md-4 col-lg-4 col-xl-4 order-md-2"></div>

								<div class="col-12 col-sm-12 col-md-4 col-lg-4 col-xl-4 order-md-1">
									<div class="custom-control custom-checkbox">
										<input type="checkbox" class="custom-control-input" id="chbkSlctAll" >
										<label class="custom-control-label roundCheck lead" for="chbkSlctAll">Select All SKUs</label>
									</div>
								</div>
							</div>

							<div class="row">
								<div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">

									<?php 
									if(!empty($stylequeinfo))
									{


									 foreach($stylequeinfo as $querow){

										$id=$querow->upimg_id;
										$sku=$querow->zipfile_name;
										$category=$querow->category;
										$gender=$querow->gender;



										$query=$this->db->query("select * from  tbl_style where skuno='$sku' and user_id='$user_id'  ");
										$row2 = $query->row();
										$autostyle=$row2->autostyle;
										$sty_id=$row2->sty_id;
										$status=$row2->status;

										//$sku_status

										if($autostyle!='1')
										{



											if($gender=='1')
											{
												$gender_name='Male';
											}
											else if($gender=='2')
											{
												$gender_name='Female';
											}
											else if($gender=='3')
											{
												$gender_name='Boys (0-2 years)';
											}
											else if($gender=='4')
											{
												$gender_name='Boys (2-9 years)';
											}
											else if($gender=='5')
											{
												$gender_name='Girls (0-2 years)';
											}
											else if($gender=='6')
											{
												$gender_name='Girl (2-9 years)';
											}
											else
											{
												$gender_name='';
											}


											?>

											<div id="SKU_<?php echo $id; ?>" class="pb-5 ">
												<div class="custom-control custom-checkbox">
													<input type="checkbox" class="custom-control-input checkSingle" id="chbkSKU_<?php echo $id; ?>" value="<?php  echo $sku; ?>" <?php if($status=='2')
													{ ?> disabled <?php } ?>>
													<label  class="custom-control-label roundCheck lead" for="chbkSKU_<?php echo $id; ?>">
														<a id="pointer-<?php  echo $sku; ?>"></a>
														SKU: <?php echo $sku;?> 
													</label>
													<!-- <span class="text-danger lead"><i class="far fa-times-circle"></i></span> -->

													<!-- <span id="spanSKUDelete<?php //echo $id; ?>" class="text-secondary lead pl-3" onclick="delsku('<?php  //echo $sku; ?>','<?php  //echo $sty_id; ?>','<?php  //echo $status; ?>');">
														<i class="far fa-times-circle"></i>
													</span> -->

													<span id="spanSKUDelete<?php echo $id; ?>" class="text-secondary lead pl-3"  onclick="delsku('<?php  echo $sku; ?>','<?php  echo $sty_id; ?>','<?php  echo $status; ?>');">
														<i class="far fa-times-circle"></i>
													</span>

													<div class="modal fade" id="modal_alert_sku_<?php  echo $sku; ?>" tabindex="-1" role="dialog" aria-hidden="true">
														<div class="modal-dialog" role="document">
															<div class="modal-content">
																<div class="modal-header">
																	<h5 class="modal-title text-dark" id="modMsg"></h5>
																	<button type="button" class="close text-danger" data-dismiss="modal" aria-label="Close">
																		<span aria-hidden="true" >&times;</span>
																	</button>
																</div>
																<div class="modal-footer">
																	<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
																	<button type="button" id="hideDel" class="btn btn-danger" onclick="sku_del('<?php  echo $sku; ?>','<?php  echo $sty_id; ?>','<?php  echo $status; ?>');">Delete</button>
																</div>
															</div>
														</div>
													</div>

													<?php /*if($querow->cutout_flag==0 && $querow->cutout_manual==0) { ?>
													<span class="pl-3 "><i class="fas fa-spinner fa-spin text-piquic"></i>&nbsp;&nbsp;Generating Cutout. Please wait for sometime to do styling </span>
												  <?php } */?>
												</div>
												<div class="row pt-3 pl-md-4">
													<div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
														<p class="lead">
															Gender: <?php echo ucfirst($gender_name);?> | Category: <?php echo ucfirst($category);?>  
															<!-- <?php// if($querow->cutout_flag==0 && $querow->cutout_manual==0)
												//	{
													?>
													<div class="progress border" style="height: 1.5rem !important;">
																<div class="progress-bar bg-piquic progress-bar-striped progress-bar-animated" role="progressbar" style="width: 75%; " aria-valuenow="75" aria-valuemin="0" aria-valuemax="100">Generating Cutout</div>
															</div>
													<?php// } ?> -->
														
													</p>
													</div>
												</div>

												<div class="row pt-3 pl-md-4">
													<div class="col-12 col-sm-12 col-md-3 col-lg-3 col-xl-3">

														<div id="carousel_SKU<?php echo $sku;?>" class="carousel slide" data-ride="carousel">
															<div class="carousel-inner">
																<?php
																$i = 0;
																$query=$this->db->query("select * from  tbl_upload_images where upimg_id='$id'");   
																foreach ($query->result() as $resrow)
																{ 
																	$img=$resrow->img_name;
																	$imgid=$resrow->id;

																	?>
																	<script type="text/javascript">

																		$(window).on('load', function(){

																			var img = document.getElementById('img_<?php echo $imgid; ?>');

																			var width = img.naturalWidth;
																			var height = img.naturalHeight;

																			if (width > height){
																				$('#rtImg_<?php echo $imgid; ?>').removeClass('d-none');
																			} else {
																				$('#orgImg_<?php echo $imgid; ?>').removeClass('d-none');
																			}
																		});

																	</script>
																	<div class="carousel-item <?php if( $i == 0 ){ ?> active <?php } ?>">						<div id="orgImg_<?php echo $imgid; ?>" class="d-none w-100">
																		<img id="img_<?php echo $imgid; ?>" class="w-100" src="<?php echo base_url().'/upload/'.$user_id.'/'.$sku.'/thumb_'.$img ;?>" alt="...">
																	</div>

																	<div id="rtImg_<?php echo $imgid; ?>" class="img-box d-none">
																		<img id="img_<?php echo $imgid; ?>" class="img" src="<?php echo base_url().'/upload/'.$user_id.'/'.$sku.'/thumb_'.$img ;?>" alt="...">
																	</div>

																</div>

																<?php $i++; } ?>														
															</div>

															<a class="carousel-control-prev" href="#carousel_SKU<?php echo $sku;?>" role="button" data-slide="prev">
																<span class="carousel-control-prev-icon" aria-hidden="true"></span>
																<span class="sr-only">Previous</span>
															</a>
															<a class="carousel-control-next" href="#carousel_SKU<?php echo $sku;?>" role="button" data-slide="next">
																<span class="carousel-control-next-icon" aria-hidden="true"></span>
																<span class="sr-only">Next</span>
															</a>
														</div>
													</div>

													<?php 
          //echo "select * from  tbl_style where skuno='$sku' and user_id='$user_id' ";
													$query=$this->db->query("select * from  tbl_style where skuno='$sku' and user_id='$user_id'  ");
													$row2 = $query->row();
													$sty_id=$row2->sty_id;
													$genname=$row2->gender;
													$catname=$row2->category;
													$model=$row2->model_id;
													$poseids=trim($row2->poseids);
													$top=$row2->top;
													$bottom=$row2->bottom;
         //$dress=$row2->dress;
													$shoes=$row2->shoes;
													$accessories=$row2->accessories;

													$lipstick=$row2->lipstick;
													$lipstick_effrt=$row2->lipstick_effrt;
													$eyemkup=$row2->eyemkup;
													$eyeshadowcol=$row2->eyeshadowcol;
													$eyeintensity=$row2->eyeintensity;
													$blushcolcde=$row2->blushcolcde;
													$blushintensity=$row2->blushintensity;




													$makeup=$row2->makeup;
													$status=$row2->status;
													$autostyle=$row2->autostyle;

													?>

													<div class="col-12 col-sm-12 col-md-9 col-lg-9 col-xl-9">
														<div class="row pb-3">
															<?php if($querow->cutout_flag==0 && $querow->cutout_manual==0) { ?>
															<div class="col-12 col-sm-12 col-md-6 col-lg-6 col-xl-6 pb-4">
																<div class="d-flex justify-content-between">
																	<span class="lead">
																		Cutout&nbsp;
																		<span class="text-piquic <?php if($querow->cutout_flag==0 && $querow->cutout_manual==0){ echo 'd-none';} ?>" ><i class="fas fa-check-circle"></i></span>
																	</span>
																</div>

																<small class="text-piquic">
																		<i class="fas fa-spinner fa-spin text-piquic"></i>&nbsp;&nbsp;Generating Cutout...Meanwhile select the Model, Pose, Top/Bottom, Shoes, etc...
																</small>
															</div>
															<?php } else { ?>
															<div class="col-12 col-sm-12 col-md-3 col-lg-3 col-xl-3 pb-4">
																<div class="d-flex justify-content-between">
																	<span class="lead">
																		Cutout&nbsp;
																		<span class="text-piquic <?php if($querow->cutout_flag==0 && $querow->cutout_manual==0){ echo 'd-none';} ?>" ><i class="fas fa-check-circle"></i></span>
																	</span>
																</div>

																<small class="text-piquic">
																	<i class="fas fa-info-circle"></i>&nbsp;&nbsp;Generated			
																</small>
															</div>
															<?php } ?>

															<div class="col-12 col-sm-12 col-md-3 col-lg-3 col-xl-3 pb-4">
																<div class="d-flex justify-content-between <?php if($model==''){ echo 'cur_sor';} ?>">
																	<span class="lead">
																		Model&nbsp;
																		<span class="text-piquic <?php if($model==''){ echo 'd-none';} ?>" ><i class="fas fa-check-circle"></i></span>
																	</span>

																</div>

																<small class="text-danger <?php if($model!=''){ echo 'd-none';} ?>"><i class="fas fa-exclamation-circle"></i>&nbsp;Missing</small>

															</div>

															<div class="col-12 col-sm-12 col-md-3 col-lg-3 col-xl-3 pb-4">
																<div <?php if($model!='' && $poseids==''){  ?>   data-toggle="modal" data-target="#modalstyle" onclick="setIframe('<?php echo $sku;?>','pose');"  <?php } ?> >
																<div class="d-flex justify-content-between">
																	<span class="lead">
																		Poses&nbsp;<span class="text-piquic <?php if($poseids==''){ echo 'd-none';} ?>"><i class="fas fa-check-circle"></i></span>
																	</span>
																</div>
																<small class="text-danger <?php if($poseids!=''){ echo 'd-none';} ?>"><i class="fas fa-exclamation-circle"></i>&nbsp;Missing</small>
															</div>
															</div>

															<?php if($catname=='bottom'){ ?>
																<div class="col-12 col-sm-12 col-md-3 col-lg-3 col-xl-3 pb-4">
																	<div <?php if($top=='' && $poseids!=''){  ?>   data-toggle="modal" data-target="#modalstyle" onclick="setIframe('<?php echo $sku;?>','top');" <?php } ?> >
																	<div class="d-flex justify-content-between">
																		<span class="lead">
																			Top&nbsp;<span class="text-piquic <?php if($top==''){ echo 'd-none';} ?>"><i class="fas fa-check-circle"></i></span>
																		</span>
																	</div>
																	<small class="text-danger <?php if($top!=''){ echo 'd-none';} ?>"><i class="fas fa-exclamation-circle"></i>&nbsp;Missing</small>

                                  </div>
																</div>
															<?php } ?>

															<?php if($catname=='top'){?>
																<div class="col-12 col-sm-12 col-md-3 col-lg-3 col-xl-3 pb-4">
																	<div <?php if($bottom=='' && $poseids!='' ){  ?>    data-toggle="modal" data-target="#modalstyle" onclick="setIframe('<?php echo $sku;?>','bottom');" <?php } ?> >
																	<div class="d-flex justify-content-between">
																		<span class="lead">
																			Bottom&nbsp;<span class="text-piquic <?php if($bottom==''){ echo 'd-none';} ?>"><i class="fas fa-check-circle"></i></span>
																		</span>
																	</div>
																	<small class="text-danger <?php if($bottom!=''){ echo 'd-none';} ?>"><i class="fas fa-exclamation-circle"></i>&nbsp;Missing</small>
																</div>
																</div>
															<?php } ?>

															<div class="col-12 col-sm-12 col-md-3 col-lg-3 col-xl-3 pb-4">
																<div  <?php  if($shoes=='' && $poseids!=''){ /*if(($catname=='top' && $bottom!='') || ($catname=='bottom' && $top!='') || ($catname=='dress')  ) { */ ?>   data-toggle="modal" data-target="#modalstyle" onclick="setIframe('<?php echo $sku;?>','shoes');" <?php 
																//} 
																 } ?> >
																<div class="d-flex justify-content-between">
																	<span class="lead">
																		Shoes&nbsp;<span class="text-piquic <?php if($shoes==''){ echo 'd-none';} ?>"><i class="fas fa-check-circle"></i></span>
																	</span>
																</div>
																<small class="text-danger <?php if($shoes!=''){ echo 'd-none';} ?>"><i class="fas fa-exclamation-circle"></i>&nbsp;Missing</small>
															</div>
															</div>

															<div class="col-12 col-sm-12 col-md-3 col-lg-3 col-xl-3 pb-4">
																<div  <?php if($accessories=='' && $poseids!='' ){  ?>   data-toggle="modal" data-target="#modalstyle" onclick="setIframe('<?php echo $sku;?>','accessories');" <?php } ?> >
																<div class="d-flex justify-content-between">
																	<span class="lead">
																		Accessories&nbsp;<span class="text-piquic  <?php if($accessories==''){ echo 'd-none';} ?>"><i class="fas fa-check-circle"></i></span>
																	</span>
																</div>
																<small class="text-warning <?php if($accessories!=''){ echo 'd-none';} ?>"><i class="fas fa-exclamation-circle"></i>&nbsp;Optional</small>
															</div>
															</div>
<!-- ($lipstick!='' && $lipstick_effrt!=''  && $eyemkup!='' && $eyeshadowcol!='' && $eyeintensity!='' && $blushcolcde!='' && $blushintensity!=''  -->
															<?php if($genname=='2' || $genname=='5' || $genname=='6' ){?>
																<div class="col-12 col-sm-12 col-md-3 col-lg-3 col-xl-3 pb-4">
																	<div  <?php if($poseids!='' ){ ?>   data-toggle="modal" data-target="#modalstyle" onclick="setIframe('<?php echo $sku;?>','makeup');" <?php } ?> >
																	<div class="d-flex justify-content-between">
																		<span class="lead">
																			Makeup&nbsp;<span class="text-piquic <?php if($lipstick!='' || $lipstick_effrt!=''  || $eyemkup!='' || $eyeshadowcol!='' || $eyeintensity!='' || $blushcolcde!='' || $blushintensity!='' ){ } else{ echo 'd-none'; } ?> "><i class="fas fa-check-circle"></i></span>
																		</span>
																	</div>
																	<small class="text-warning  
																	<?php if($lipstick=='' && $lipstick_effrt==''   && $eyemkup==''  && $eyeshadowcol==''  && $eyeintensity==''  && $blushcolcde==''  && $blushintensity=='' ){ } else{ echo 'd-none'; } ?>"><i class="fas fa-exclamation-circle"></i>&nbsp;Optional</small>
																</div>
																</div>
															<?php } ?>


														</div>



														<div class="row pb-3"> 

															<?php if($catname=='dress'){?> 

																<?php if($shoes=='' || $model=='' || $poseids=='' || $top==''){ ?>
																	<?php
																	if($autostyle=='1')
																	{
																		?>
																		<button type="submit" class="btn btn-outline-secondary btn-block disabled" id="btn_style_1223334444" onclick="view_process('<?php echo $sty_id;?>','<?php echo $sku; ?>');">Sent for Autostyle</button>
																		<?php
																	}
																	else
																	{
																		?>
																		

																		<button type="button" class="btn btn-outline-dark btn-block " id="btn_style" <?php if($querow->cutout_flag==0 && $querow->cutout_manual==0  ) { ?> disabled <?php } else if($poseids!='') { ?> disabled <?php }  ?> onclick="sendmainstyle('<?php echo $sku; ?>');" >Complete Styling</button>

																	<?php } ?>

																<?php } ?>

																<?php if($shoes!='' && $model!='' && $poseids!=''&& $top!=''){ ?>


																	<?php
																	if($status=='2')
																	{
																		?>
																		<button type="submit" class="btn btn-outline-dark btn-block disabled" id="btn_style_1223334444" onclick="view_process('<?php echo $sty_id;?>','<?php echo $sku; ?>');">Sent for Processing&nbsp;<i class="fas fa-check"></i></button>
																		<?php
																	}
																	else
																	{
																		?>
																		<button type="button" class="btn btn-outline-dark btn-block" id="btn_style" onclick="sendprocessing('<?php echo $sty_id;?>','<?php echo $sku; ?>');">Send for Processing</button>
																		<?php
																	}
																	?>







																<?php } }?>


																<?php if($catname=='bottom'){?> 

																	<?php if($shoes=='' || $model=='' || $poseids=='' || $top==''){ ?>
																		<?php
																		if($autostyle=='1')
																		{
																			?>
																			<button type="submit" class="btn btn-outline-secondary btn-block disabled" id="btn_style_1223334444">Send for Processing</button>
																			<?php
																		}
																		else
																		{
																			?>
																			
																			<button type="button" class="btn btn-outline-dark btn-block " id="btn_style" <?php if($querow->cutout_flag==0 && $querow->cutout_manual==0  ) { ?> disabled <?php } else if($poseids!='') { ?> disabled <?php }  ?>  onclick="sendmainstyle('<?php echo $sku; ?>');" >Complete Styling</button>
																			<?php
																		}
																		?>






																	<?php } ?>

																	<?php if($shoes!='' && $model!='' && $poseids!=''&& $top!=''){ ?>

																		<?php
																		if($status=='2')
																		{
																			?>
																			<button type="submit" class="btn btn-outline-dark btn-block disabled" id="btn_style_1223334444" onclick="view_process('<?php echo $sty_id;?>','<?php echo $sku; ?>');">Sent for Processing&nbsp;<i class="fas fa-check"></i></button>
																			<?php
																		}
																		else
																		{
																			?>
																			<button type="button" class="btn btn-outline-dark btn-block" id="btn_style" onclick="sendprocessing('<?php echo $sty_id;?>','<?php echo $sku; ?>');">Send for Processing</button>

																			<?php
																		}
																		?>

																	<?php } }?>

																	<?php if($catname=='top'){?> 

																		<?php if($shoes=='' || $model=='' || $poseids=='' || $bottom==''){ ?>
																			<?php
																			if($autostyle=='1')
																			{
																				?>
																				<button type="submit" class="btn btn-outline-secondary btn-block disabled" id="btn_style_1223334444">Send for Processing</button>
																				<?php
																			}
																			else
																			{
																				
																				?>
																				
																				
																				<button type="button"  class="btn btn-outline-dark btn-block " <?php if($querow->cutout_flag==0 && $querow->cutout_manual==0  ) { ?> disabled <?php } else if($poseids!='') { ?> disabled <?php }  ?>  id="btn_style" onclick="sendmainstyle('<?php echo $sku; ?>');">Complete Styling</button>

																				<?php
																			}
																			?>
																		<?php } ?>

																		<?php if($shoes!='' && $model!='' && $poseids!=''&& $bottom!=''){ ?>


																			<?php
																			if($status=='2')
																			{
																				?>
																				<button type="submit" class="btn btn-outline-dark btn-block disabled" id="btn_style_1223334444" onclick="view_process('<?php echo $sty_id;?>','<?php echo $sku; ?>');">Sent for Processing&nbsp;<i class="fas fa-check"></i></button>
																				<?php
																			}
																			else
																			{
																				?>

																				<button type="button" class="btn btn-outline-dark btn-block" id="btn_style" onclick="sendprocessing('<?php echo $sty_id;?>','<?php echo $sku; ?>');">Send for Processing</button>


																				<?php
																			}
																			?>

																		<?php } }?>



																	</div>
																</div>
															</div>
														</div>

													<?php } 

												}
												}
												else
														{
															?>
															<div class="text-center text-danger lead pt-3" style="min-height: 60vh;">
			<span>No Data Found.</span>
		</div>
<?php
														}

												 ?>

												<input type="hidden" name="arrval" id="arrval">
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>

					</div>
				</div>
			</div>











											<div class="modal fade" id="modalstyle" tabindex="-1" role="dialog" aria-labelledby="modalstyleLabel" aria-hidden="true">
								<div class="modal-dialog" role="document" style="max-width: 65em;">
								<div class="modal-content">


								<div class="modal-header">
								<h5 class="modal-title" id="modalstyleLabel">Style Preview</h5>
								<button type="button" class="close" data-dismiss="modal" aria-label="Close">
								<span aria-hidden="true">&times;</span>
								</button>
								</div>



								<div class="modal-body">
								<div id="pose_row" class="row">

								<iframe src="" id="iframe_info" class="iframe" name="info" seamless="" height="600px" width="100%"></iframe>
								</div>
								</div>


								<div class="modal-footer">
								<!-- <button type="button" class="btn btn-secondary px-2" data-dismiss="modal">Close</button>
								<button type="button" class="btn btn-primary px-2">Save</button> -->
								</div>


								</div>
								</div>
								</div>

			<?php
			$this->load->view('front/includes/footer');
			?>

			<script type="text/javascript">





 
          var selectedCheckId= localStorage.getItem("selectedCheckId");
          //alert(selectedCheckId);
          if(selectedCheckId!='' && selectedCheckId!=null)
          {
          	if (document.getElementById(selectedCheckId) !=null) 
          	{
          		document.getElementById(selectedCheckId).checked = true;
          	}
          	
          }
          



			function setIframe(sku,selecttype)
			{
				//alert(sku);
			var url='<?php echo base_url();?>styleframe/'+sku+'/'+selecttype;
			$('#iframe_info').attr('src', url)

			}
				function view_process(style_id,sku){
					window.location = "<?php echo base_url();?>processing/"+style_id+"/"+sku;
				}

			function sku_del(sku,sty_id,status){
				// var x = confirm("Are you sure you want to delete?");
					var x = true;
					if (x)
					{

					var sku=sku;
					var sty_id=sty_id;
					$.ajax({
					type:'POST',			
					url : "<?php echo base_url();?>front/stylequeue/deleteStyleque", 
					data:{'skuno':sku,'sty_id':sty_id},
					success: function(delres) {	
					// alert(delres);	
					location.reload();		
					}
					});
					return true;
					}
					else
					{
					return false;
					}
			}

			function delsku(sku,sty_id,status){
      // alert(status);
      // return false;
      if(status=='2')
      {
      	$('#modal_alert_sku_' + sku).modal('show');
				$('#modMsg').html('Already sent for processing. You can\'t delete.');
				$('#hideDel').addClass('d-none');
      	// alert("Already sent for processing. You can't delete.");
      	return false;
      }
      else
      {
      	$('#modal_alert_sku_' + sku).modal('show');
				$('#modMsg').html('Are you sure you want to delete?');

					// // var x = confirm("Are you sure you want to delete?");
					// var x = true;
					// if (x)
					// {

					// var sku=sku;
					// var sty_id=sty_id;
					// $.ajax({
					// type:'POST',			
					// url : "<?php // echo base_url();?>front/stylequeue/deleteStyleque", 
					// data:{'skuno':sku,'sty_id':sty_id},
					// success: function(delres) {	
					// // alert(delres);	
					// location.reload();		
					// }
					// });
					// return true;
					// }
					// else
					// {
					// return false;
					// }
      }

			}

/*				function searchSKUOld(){
					var sku =$('#searchSKU').val();
					window.location.href = "<?php echo base_url();?>stylequeue/"+sku ;

				}
*/
			document.querySelector("#formsearchSKU").addEventListener("submit", function(e){
				if($('#searchSKU').val()=="")
				{
					var sku="-";
				}
				else{
						var sku =$('#searchSKU').val();
				}
				if($('#filter_style').val()=="")
				{
					var filter_val="-";
				}
				else{
						var filter_val =$('#filter_style').val();
				}
				
				//alert(filter_val);
				window.location.href = "<?php echo base_url();?>stylequeue/"+sku+"/"+filter_val;
				e.preventDefault();


			});


			$("#filter_style").on('change', function(){

				if($('#searchSKU').val()=="")
				{
					var sku="-";
				}
				else{
					var sku =$('#searchSKU').val();
				}
				if($(this).val()=="")
				{
					var filter_val ="-";
				}
				else{
					var filter_val =$(this).val();
				}

				//alert(filter_val);
				window.location.href = "<?php echo base_url();?>stylequeue/"+sku+"/"+filter_val;
				e.preventDefault();


			});


				function getModelByGender(selectObject)
				{
					var gender_id=selectObject.value;

					$.ajax({
						type:'POST',					
						url : "<?php echo base_url();?>front/stylequeue/getModelByGender", 
						data:{'gender_id':gender_id},
						success: function(resultdiv) {	
					//alert(resultdiv);
					$('#model_search_div').html(resultdiv);
				}
			});
				}



				function vendorselsku(selectObject){
					var vendor = selectObject.value;
					var sku =$('#arrval').val();
					$.ajax({
						type:'POST',					
						url : "<?php echo base_url();?>front/stylequeue/vendorsku", 
						data:{'skuno':sku,'vendor':vendor},
						success: function(mulcatres) {	
				// alert(mulcatres);
				// location.reload();
			}
		});
				}






				function autostylingsku(selectObject){
					var autoval = selectObject.value;
				//alert(autoval);
				var sku =$('#arrval').val();
				if(autoval!='' && autoval!='0')
				{
					$.ajax({
						type:'POST',					
						url : "<?php echo base_url();?>front/stylequeue/autostylingskufuc", 
						data:{'skuno':sku,'autoval':autoval},
						success: function(autostyres) {	
					// alert(autostyres);
					location.reload();
				}
			});
				}
				


			}



			function modlmultskuSidebar(mid){
				var modval = mid;
				var sku =$('#arrval').val();






				if(sku!='')
				{
					$("#model_sel_"+modval).addClass("border-piquic");
					$("#span_model_sel_"+modval).removeClass("d-none");

					$.ajax({
					type:'POST',					
					url : "<?php echo base_url();?>front/stylequeue/mulmodelupdate", 
					data:{'skuno':sku,'model':modval},
					success: function(mulcatres) {	
					// alert(mulcatres);
					if($.trim(mulcatres)=='missmatch')
					{
					$('#modal_msg').modal('show');
					$('#txtMsg').html('Selected model\'s gender is not matching with sku gender!. Select another model according to gender.');
					// alert("Selected model's gender is not matching with sku gender!. Select another model according to gender.");

					$("#model_sel_"+modval).removeClass("border-piquic");
					$("#span_model_sel_"+modval).addClass("d-none");


					}
					else
					{
					//location.reload();
					//alert("#pointer-"+sku+"");
					//window.location.href = "<?php echo base_url();?>stylequeue/#pointer-"+sku;
					//location.reload(true);

					
					window.location.hash = "#pointer-"+sku+"";
					location.reload(true);

						/*$('html, body').animate({
						scrollTop: $("#pointer-"+sku+"").offset().top
						}, 2000);

						location.reload();*/


					}

					}
					});	
				}
				else
				{
					$('#modal_msg').modal('show');
					$('#txtMsg').html('Select at least one SKU.');
					// alert("Select at least one SKU.");
				}
				



			}









			function modlmultsku(selectObject){
				var modval = selectObject.value;
				var sku =$('#arrval').val();
				$.ajax({
					type:'POST',					
					url : "<?php echo base_url();?>front/stylequeue/mulmodelupdate", 
					data:{'skuno':sku,'model':modval},
					success: function(mulcatres) {	
				// alert(mulcatres);
				location.reload();
			}
		});
			}	

			function sendmainstyle(sku){
				var sku=sku;
				window.location = "<?php echo base_url();?>style/"+sku;
			}

			function sendprocessing(sty_id,sku){

					//alert(sty_id);
					//alert(sku);

				//window.location = "<?php //echo base_url();?>style" ;

				$.ajax({
					type:'POST',					
					url : "<?php echo base_url();?>front/stylequeue/updateprocessing", 
					data:{'skuno':sku,'sty_id':sty_id},
					success: function(mulcatres) {	
				//alert(mulcatres);
				console.log(mulcatres);
				window.location = "<?php //echo base_url();?>processing" ;
				//location.reload();
			}
		});

			}

			$(document).ready(function() {


				var chksku = []; 
				$("#chbkSlctAll").change(function(){
					if(this.checked){
						$("#filter-it").removeClass('d-none');
						$(".checkSingle").each(function(){


							

							if ($(this).is(':disabled')) {
							//return false;
							this.checked=false; 
							}
							else
							{
							this.checked=true; 
							}
			
							var count=$('.checkSingle').filter(':checked').length;
							$('#itemcount').html(count+' item selected');
							chksku = $(".checkSingle:checked").map(function (i) {
								return $(this).val();
							}).get();
							$('#arrval').val(chksku);
						}) 
					}else{
						$("#filter-it").addClass('d-none');
						$(".checkSingle").each(function(){
							$('#itemcount').html(' 0 item selected');
							this.checked=false;
							chksku = $(".checkSingle:checked").map(function (i) {
								return $(this).val();
							}).get();
							$('#arrval').val(chksku);
						}) 
					}
				});




				$(".checkSingle").click(function () {

					var count=$('.checkSingle').filter(':checked').length;
					$('#itemcount').html(count+' item selected'); 
					chksku = $(".checkSingle:checked").map(function (i) {
						return $(this).val();
					}).get();
					$('#arrval').val(chksku);
					if(count>0){
						$("#filter-it").removeClass('d-none');
					}else{
						$("#filter-it").addClass('d-none');
					}

					if ($(this).is(":checked")){


						var checkSingleId=$(this).attr("id");
                        localStorage.setItem("selectedCheckId", checkSingleId);

						var isAllChecked = 0; 
						$(".checkSingle").each(function(){
							if(!this.checked){
								isAllChecked = 1;
							}
						}) 
						if(isAllChecked == 0){ $("#chbkSlctAll").prop("checked", true); } 
					}else {

						$("#chbkSlctAll").prop("checked", false);
						localStorage.setItem("selectedCheckId", '');
					}
				});




///////////////////////////////////Cutout Api generate//////////////////////////////////////////

<?php

    $sql="select tbl_upload_img.*,tbl_style.status as style_status from  tbl_upload_img left join tbl_style on tbl_upload_img.zipfile_name=tbl_style.skuno WHERE tbl_upload_img.queue='1' AND tbl_upload_img.status='0'  AND tbl_upload_img.cutout_flag='0' AND tbl_upload_img.cutout_manual='0' AND zipfile_name NOT IN(SELECT DISTINCT skuno  FROM tbl_style WHERE   user_id='$user_id' AND autostyle ='1' ) ";
  
    if($user_id!='')
    {
      $sql.=" and tbl_upload_img.user_id='$user_id' ";
    }
    if($sku_key!='')
    {
      $sql.=" and tbl_upload_img.zipfile_name like '%".$sku_key."%' ";
    }

    $sql .= " order by style_status ASC, upimg_id DESC limit 1 ";
     

     // echo $sql; exit();
    
    $query=$this->db->query($sql);
    $stylequeinfo= $query->result();



						foreach($stylequeinfo as $querow){

						$upimg_id=$querow->upimg_id;
						$sku=$querow->zipfile_name;
						$category=$querow->category;
						$gender=$querow->gender;

						$query=$this->db->query("select * from  tbl_style where skuno='$sku' and user_id='$user_id'  ");
										$row2 = $query->row();
										$autostyle=$row2->autostyle;
										$sty_id=$row2->sty_id;
										$status=$row2->status;

										//$sku_status

				?>
 //alert('<?php echo $autostyle;?>');
				<?php					

						if($autostyle!=1 && $querow->cutout_flag==0 && $querow->cutout_manual==0) { 
?>
                               // alert('<?php echo $sku;?>');

							//alert("<?php echo base_url();?>stylequeue");
															
								$.ajax({
								type:'POST',			
								url : "<?php echo base_url();?>front/stylequeue/genCutoutFrmApi", 
								data:{'upimg_id':'<?php echo $upimg_id;?>','skuno':'<?php echo $sku;?>','user_id':'<?php echo $user_id;?>'},
								success: function(responseText) {	
								//alert(responseText);	
									if($.trim(responseText)=='Success')
									{
										window.location.href="<?php echo base_url();?>stylequeue";	
									}
								}
								});

<?php
							 }

							  

							}
?>

//////////////////////////////////////////////////////////////////////////










			});

           localStorage.setItem("selectedCheckId", '');
		</script>