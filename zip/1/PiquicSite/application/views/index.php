
<!DOCTYPE html>

<html>

<head>

  <meta charset="utf-8">

  <meta http-equiv="X-UA-Compatible" content="IE=edge">

  <title>BookingSolution| Log in</title>

  

  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">

  <!-- Bootstrap 3.3.6 -->

  <link rel="stylesheet" href="<?php echo base_url('theme');?>/assets/bootstrap/css/bootstrap.min.css">

  <!-- Font Awesome -->

  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css">

  <!-- Ionicons -->

  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css">

  <!-- Theme style -->

  <link rel="stylesheet" href="<?php echo base_url('theme');?>/assets/dist/css/AdminLTE.min.css">

  <!-- iCheck -->

  <link rel="stylesheet" href="<?php echo base_url('theme');?>/assets/plugins/iCheck/square/blue.css">



</head>

<body class="hold-transition login-page">

<div class="login-box">

  <div class="login-logo">

    <a href="#"><b>My Booking Solution</a>

  </div>

  <!-- /.login-logo -->

  <div class="login-box-body">

    <p class="login-box-msg">Sign in </p>



    <?php echo $this->session->flashdata('check_database');  

					$class = array("class"=>"login-box-body", "id"=>"login_form"); 

					echo form_open('check_login', $class);

					?>

      <div class="form-group has-feedback">

        <input class="form-control" type="email" placeholder="Email" name="email" value="<?php echo set_value('email'); ?>" />

        <span class="glyphicon glyphicon-envelope form-control-feedback"></span>

      </div>

      <div class="form-group has-feedback">

       <input class="form-control" type="password" placeholder="Password" name="password" value="<?php echo set_value('password'); ?>">

        <span class="glyphicon glyphicon-lock form-control-feedback"></span>

      </div>

      <div class="row">

        <div class="col-xs-8">

          <div class="checkbox icheck">

            <label>

              <input type="checkbox"> Remember Me

            </label>

          </div>

        </div>

        <!-- /.col -->

        <div class="col-xs-4">

        <input class="btn btn-primary" type="submit" value="Sign In" name="signin" />

        </div>

        <!-- /.col -->

      </div>

    </form>



   



  </div>

  <!-- /.login-box-body -->

</div>

<!-- /.login-box -->

<?php echo form_close();?>

<!-- jQuery 2.2.3 -->

<script src="<?php echo base_url('theme');?>/assets/plugins/jQuery/jquery-2.2.3.min.js"></script>

<!-- Bootstrap 3.3.6 -->

<script src="<?php echo base_url('theme');?>/assets/bootstrap/js/bootstrap.min.js"></script>

<!-- iCheck -->

<script src="<?php echo base_url('theme');?>/assets/plugins/iCheck/icheck.min.js"></script>

<script>

  $(function () {

    $('input').iCheck({

      checkboxClass: 'icheckbox_square-blue',

      radioClass: 'iradio_square-blue',

      increaseArea: '20%' // optional

    });

  });

</script>

</body>

</html>

