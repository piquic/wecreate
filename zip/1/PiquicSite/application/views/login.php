<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Login extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
		$this->load->library('userlib');
		$this->load->library('email');
		$this->load->library('Form_validation');
    }

    public function index()
    {
        if ($this->session->userdata('logged_in')) 
        {
            redirect('dashboard', 'refresh');
        } 
        else 
        {
            $this->load->helper(array('form'));
            $this->load->view('signin');
			
        }
    }
	//written by sravanthi
	public function check_forget_password()
    {
		
		$email=$this->input->post('email');
		$sql = "SELECT email FROM nc_user WHERE email = '$email'";
        // echo "SELECT email FROM bussiness_owner WHERE email = '$email'";
		$query = $this->db->query($sql);
		
        if( $query->num_rows() > 0 ){
			//echo "aaaa";
			$res_owner=$this->db->query("select * from nc_user where email='$email'")->result_array();
			$name=$res_owner[0]['business_name'];
			$mobile=$res_owner[0]['mobile'];
			$oemail=$res_owner[0]['email'];
			$owner_id=$res_owner[0]['owner_id'];
			$business_logo=$res_owner[0]['logo'];
			$base_owner_id=base64_encode($owner_id);
			$forget_password_link=base_url()."login/forget_password/".$base_owner_id;
			$config=$this->userlib->emailconfig(); 
			
			$message='<!DOCTYPE html>
			        <html lang="en-US" class="no-js">
					<head>
						<meta charset="UTF-8">
						<meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=1, maximum-scale=1">
						<meta name="viewport" content="width=device-width">
						<title>My Booking Solution</title>
					</head>
					<body>
					<center><table align="center" border="0" cellpadding="0" cellspacing="0" height="100%" width="100%" style="background-color:#eee9ed;background-image:none;background-repeat:repeat;background-position:top left;border-collapse:collapse!important; font-family: &quot;Open Sans&quot;, sans-serif;">
        <tbody><tr>
            <td align="center" valign="top" style="border-collapse:collapse!important">
			<table border="0" cellpadding="0" cellspacing="0" width="600" style="background-color:#ffffff;background-image:none;background-repeat:repeat;background-position:top left;border-collapse:collapse!important; margin:55px 0;font-family: &quot;Open Sans&quot;, sans-serif;">';
		    $path_logo='uploadlogo/'.$business_logo;
			if($business_logo!='' && @file_exists($path_logo)) {
				$message.='<tr>
                        <td align="center" valign="top" style="padding-top:20px;padding-bottom:20px;padding-left:16px">
                            
                            <table border="0" cellpadding="0" cellspacing="0" width="100%" style="border-collapse:collapse!important">
                                <tbody><tr>
                                    <td align="center" valign="top">
                                        <a href="'.$business_link.'" target="_blank">
										<img src="'.base_url().'uploadlogo/'.$business_logo.'"  alt="" style="max-width:600px;display:block;border-width:0;min-height:auto;line-height:100%;outline-style:none;text-decoration:none"></a>
                                    </td>
                                </tr>
                            </tbody></table>
                            
                        </td>
                    </tr>';
			}
					$message.='<tr>
                        <td align="center" valign="top">
                            
                          <table border="0" cellpadding="0" cellspacing="0" width="100%" style="background-color:#323449;background-image:url(http://mybookingsolution.com/email/images/bg.jpg);background-repeat:repeat;background-position:top left;color:#ffffff;border-collapse:collapse!important;">
                                <tbody><tr>
                                    <td valign="top" align="center" style="padding-top:40px;padding-bottom:40px;padding-left:25px;padding-right:25px;color:#ffffff;font-size:20px;font-family:&quot;Open Sans&quot;,sans-serif;text-align:center;text-transform:uppercase; font-weight:600;">
                                        Forget Password
                                    </td>
                                </tr>
                            </tbody></table>
                            
                        </td>
                    </tr>
                    <tr>
                        <td align="center" valign="top" style="padding-top:15px;padding-bottom:15px">
                            
                            <table border="0" cellpadding="0" cellspacing="0" width="100%" style="border-collapse:collapse!important;font-family: &quot;Open Sans&quot;, sans-serif;font-size:14px;">
                                <tbody><tr>
                                    <td align="left" valign="top" style="padding-top:30px;padding-bottom:30px;padding-left:15px;padding-right:15px">
                                        <table border="0" cellpadding="0" cellspacing="0" width="100%" style="border-collapse:collapse!important">
                                            <tbody><tr>
                                                <td valign="top" align="left" style="padding-bottom:25px;font-family:&quot;Open Sans&quot;,sans-serif;font-size:14px;line-height:1.5">
                                                    		<p style="font-family:&quot;Open Sans&quot;,sans-serif;color:#120a05; font-size:14px;padding-top:0px;padding-bottom:14px;padding-right:0;padding-left:0">
															
		  <span>Hi '.ucwords($name).',</span></p>';
		  $message.=' <p style="padding-bottom:10px">This is a system-generated mail that is being sent out to you with regard to your account at mybookingsolution.com.</p>
			 <p style="padding-bottom:10px">Click on the following link to create a new password for your account.</p>
			 
			<p style="padding-bottom:10px">'.$forget_password_link.'</p>
			
			 <p style="padding-bottom:10px">Please ensure that your account security has not been compromised and is being accessed by an authorised person from your company.</p>
			';
			
		
	
		
		$message.='  <p style="padding-bottom:10px">
		For any further queries about log-in details, please write to us at support@mybookingsolution.com. </p>
			
			


			
			
							
		<p style="">Regards,<br>
<strong>My Booking Solution</strong></p>
		
                                                </td>
                                            </tr>
                                        </tbody></table>
                                    </td>
                                </tr>
                            </tbody></table>
                            
                        </td>
                    </tr>';
                   
					
                 $message.=' </table>
            </td>
        </tr>
    </tbody></table>
</center>

</body>
</html>
';
	
	//echo $message;exit;		
			 $this->load->library('email', $config);
			 $this->email->set_newline("\r\n");
			 $this->email->from($email,$name);
			 $elist = array($email);
		  $this->email->to($elist);// change it to yours
		  $this->email->cc($oemail);
		  $setting=$this->db->query("select * from setting where setting_key='admin_email'")->result_array();
		$admin_email=stripslashes($setting['0']['setting_value']);
		$this->email->bcc($admin_email);
		   // $this->email->to($own_email);// change it to yours
		  $this->email->subject('Password Change');
		  $this->email->message($message);
		  if($this->email->send())
		  {
			$s=true;
			
		  }
		  else
		  {
			$s=false;
			
		  }
			
			
		 
            echo '1';
			//echo "To : ".$email."<br>CC :".$own_email."<br>"."From :".$business_name."<br>".$message;
        } 
		/* staff forget password code start here*/
		elseif($query->num_rows() == 0 ){
			//echo "i am staff";
			$staff_owner=$this->db->query("select * from staff_master where email='$email'")->result_array();
			if($staff_owner) {
			$base_owner_id=base64_encode($staff_owner[0]['customer_id']);
			$name=$staff_owner[0]['name'];
			$mobile=$staff_owner[0]['mobile'];
			//$email=$staff_owner[0]['email'];
			$owner_id=$staff_owner[0]['owner_id'];
			$owner=$this->db->query("select * from bussiness_owner where owner_id='$owner_id'")->result_array();
			$business_logo=$owner[0]['logo'];
			$oemail=$owner[0]['email'];
			//$base_owner_id=base64_encode($owner_id);
			$forget_password_link=base_url()."login/forget_password/".$base_owner_id;
			$config=$this->userlib->emailconfig(); 
			
			$message='<!DOCTYPE html>
			        <html lang="en-US" class="no-js">
					<head>
						<meta charset="UTF-8">
						<meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=1, maximum-scale=1">
						<meta name="viewport" content="width=device-width">
						<title>My Booking Solution</title>
					</head>
					<body>
					<center><table align="center" border="0" cellpadding="0" cellspacing="0" height="100%" width="100%" style="background-color:#eee9ed;background-image:none;background-repeat:repeat;background-position:top left;border-collapse:collapse!important; font-family: &quot;Open Sans&quot;, sans-serif;">
        <tbody><tr>
            <td align="center" valign="top" style="border-collapse:collapse!important">
			<table border="0" cellpadding="0" cellspacing="0" width="600" style="background-color:#ffffff;background-image:none;background-repeat:repeat;background-position:top left;border-collapse:collapse!important; margin:55px 0;font-family: &quot;Open Sans&quot;, sans-serif;">';
		    $path_logo='uploadlogo/'.$business_logo;
			if($business_logo!='' && @file_exists($path_logo)) {
				$message.='<tr>
                        <td align="center" valign="top" style="padding-top:20px;padding-bottom:20px;padding-left:16px">
                            
                            <table border="0" cellpadding="0" cellspacing="0" width="100%" style="border-collapse:collapse!important">
                                <tbody><tr>
                                    <td align="center" valign="top">
                                        <a href="'.$business_link.'" target="_blank">
										<img src="'.base_url().'uploadlogo/'.$business_logo.'"  alt="" style="max-width:600px;display:block;border-width:0;min-height:auto;line-height:100%;outline-style:none;text-decoration:none"></a>
                                    </td>
                                </tr>
                            </tbody></table>
                            
                        </td>
                    </tr>';
			}
					$message.='<tr>
                        <td align="center" valign="top">
                            
                          <table border="0" cellpadding="0" cellspacing="0" width="100%" style="background-color:#323449;background-image:url(http://mybookingsolution.com/email/images/bg.jpg);background-repeat:repeat;background-position:top left;color:#ffffff;border-collapse:collapse!important;">
                                <tbody><tr>
                                    <td valign="top" align="center" style="padding-top:40px;padding-bottom:40px;padding-left:25px;padding-right:25px;color:#ffffff;font-size:20px;font-family:&quot;Open Sans&quot;,sans-serif;text-align:center;text-transform:uppercase; font-weight:600;">
                                        Forget Password
                                    </td>
                                </tr>
                            </tbody></table>
                            
                        </td>
                    </tr>
                    <tr>
                        <td align="center" valign="top" style="padding-top:15px;padding-bottom:15px">
                            
                            <table border="0" cellpadding="0" cellspacing="0" width="100%" style="border-collapse:collapse!important;font-family: &quot;Open Sans&quot;, sans-serif;font-size:14px;">
                                <tbody><tr>
                                    <td align="left" valign="top" style="padding-top:30px;padding-bottom:30px;padding-left:15px;padding-right:15px">
                                        <table border="0" cellpadding="0" cellspacing="0" width="100%" style="border-collapse:collapse!important">
                                            <tbody><tr>
                                                <td valign="top" align="left" style="padding-bottom:25px;font-family:&quot;Open Sans&quot;,sans-serif;font-size:14px;line-height:1.5">
                                                    		<p style="font-family:&quot;Open Sans&quot;,sans-serif;color:#120a05; font-size:14px;padding-top:0px;padding-bottom:14px;padding-right:0;padding-left:0">
															
		  <span>Hi '.ucwords($name).',</span></p>';
		  $message.=' <p style="padding-bottom:10px">This is a system-generated mail that is being sent out to you with regard to your account at mybookingsolution.com.</p>
			 <p style="padding-bottom:10px">Click on the following link to create a new password for your account.</p>
			 
			<p style="padding-bottom:10px">'.$forget_password_link.'</p>
			
			 <p style="padding-bottom:10px">Please ensure that your account security has not been compromised and is being accessed by an authorised person from your company.</p>
			';
			
		
	
		
		$message.='  <p style="padding-bottom:10px">
		For any further queries about log-in details, please write to us at support@mybookingsolution.com. </p>
			
			


			
			
							
		<p style="">Regards,<br>
<strong>My Booking Solution</strong></p>
		
                                                </td>
                                            </tr>
                                        </tbody></table>
                                    </td>
                                </tr>
                            </tbody></table>
                            
                        </td>
                    </tr>';
                   
					
                 $message.=' </table>
            </td>
        </tr>
    </tbody></table>
</center>

</body>
</html>
';
	
	//echo $message;exit;		
			 $this->load->library('email', $config);
			 $this->email->set_newline("\r\n");
			 $this->email->from($email,$name);
			 $elist = array($email);
		     $this->email->to($elist);// change it to yours
		     $this->email->cc($oemail);
		  $setting=$this->db->query("select * from setting where setting_key='admin_email'")->result_array();
		$admin_email=stripslashes($setting['0']['setting_value']);
		$this->email->bcc($admin_email);
		   // $this->email->to($own_email);// change it to yours
		  $this->email->subject('Password Change');
		  $this->email->message($message);
		  if($this->email->send())
		  {
			$s=true;
			
		  }
		  else
		  {
			$s=false;
			
		  }
			
			
		 
            echo '1';
			}
			else {
				 echo '0';
			}
		}
		
		else {
			 echo '0';
		}
		
	    
		
	}
	
	public function forget_password($base_owner_id)
    {
	
	    $oowner_id=base64_decode($base_owner_id);
	    $res_owner=$this->db->query("select * from bussiness_owner where owner_id='$oowner_id'")->result_array();
		//echo "select * from bussiness_owner where owner_id='$owner_id'";
		if($res_owner){
			$owner_id=$res_owner[0]['owner_id'];
			$data['owner_id']=  $owner_id;
		}
		else {
			$ress_owner=$this->db->query("select * from staff_master where customer_id='$oowner_id'")->result_array();
			
			//echo "select * from staff_master where customer_id='$oowner_id'";
			$owner_id=$ress_owner[0]['customer_id'];
			$data['owner_id']=  $owner_id;
		}
	   
		$this->load->view('manual_booking_change_password', $data);
	}
	
	
	
	 public function check_confirm_password()
    {
	
		$owner_id=$this->input->post('owner_id');
		$password=$this->input->post('password');
		$confirm_password=$this->input->post('confirm_password');
		
		 
		
			$check=$this->db->query("SELECT owner_id FROM bussiness_owner WHERE owner_id = '$owner_id'")->result_array();
			echo "SELECT owner_id FROM bussiness_owner WHERE owner_id = '$owner_id'";exit();
			if(!empty($check) && $password==$confirm_password){
			 $password=sha1($password);
			 $this->db->query("update bussiness_owner set password ='$password' where owner_id ='$owner_id'");
			 echo "1";
			
			}
			else if(empty($check) && $password==$confirm_password){
				$password=sha1($password);
				$this->db->query("update staff_master set password ='$password' where customer_id ='$owner_id'");
				echo "1";
				 
			}
			else{
			 echo "0";
			}
	}
	
	 public function check_database_owner()
    {
        if ($this->session->userdata('logged_in_owner')) 
        {
           // redirect('dashboard', 'refresh');
        } 
        else 
        {
            $username = $this->input->post('email');
			$password = $this->input->post('password');
			
			$result =  $this->Manualbooking_model->login_owner($username, $password);
            if ($result) 
            {
                $sess_array = array();
                foreach ($result as $row) 
                {
				
				    $sess_array = array(
                                        'owner_id' => $row->customer_id,
                                        'owner_email' => $row->email,
                                        'owner_username' => $row->email
                                         );
					
					
					
					//print_r($sess_array); exit();
					
                    $this->session->set_userdata('logged_in_owner', $sess_array);
                }
				
				
				
				/*if ($this->session->userdata('logged_in_customer')) 
				{
				
					$session_data_customer = $this->session->userdata('logged_in_customer');
					$customer_id = $session_data_customer['customer_id'];
					$customer_email = $session_data_customer['customer_email'];
					
					$data['login_customer_id'] = $customer_id; 
					$data['login_customer_email'] = $customer_email; 
					
					
					
					$data['booking_list']=  $this->Manualbooking_model->get_booking_list_customer($customer_id);
					
					$this->load->view('front/booking/manual_booking_customer_login', $data);
				   
				   
				} */
				
				
				
				echo '1';
				
               
            } 
			else
			{
			   echo "0";
			}
        }
    }	
		  
		  
}