<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Item extends CI_Controller
{
    public function __construct()
	{
		parent::__construct();		
		$this->load->library('pagination');
		$this->load->database();		
		$this->load->library('Upload');
		$this->load->helper('security');	
		$this->load->library('Form_validation');
		$this->load->helper(array('form', 'html', 'file'));
		$this->load->library('pagination');
		$this->load->library('email');
        $this->load->model('Item_model');
        $this->load->library('userlib');
		
    	if ($this->session->userdata('logged_in')) {
            $session_data = $this->session->userdata('logged_in');
            $data['username'] = $session_data['username'];
            $data['firstname'] = $session_data['firstname'];
            $data['rolecode'] = $session_data['rolecode'];
			/* $timezone = $session_data['timezone'];
			date_default_timezone_set($timezone); */
         
        } else {
            //If no session, redirect to login page
            redirect('login', 'refresh');
        }
    }
	
    public function index()
	{
		if ($this->session->userdata('logged_in'))		
		{  
			$session_data = $this->session->userdata('logged_in');
			$data['rolecode'] = $session_data['rolecode'];
			$data['usertype'] = $session_data['rolecode'];
			$data['roles'] = $session_data['roles'];
			$data['title'] = "Users";
			$slang="english";
			$this->lang->load($slang, $slang);
			$data['lang']=$this->lang->language; 
			
			//echo "aaaaaaaaa";exit;
			$this->load->view('admin/item/item_list', $data);
		} 
		else 
		{
			//If no session, redirect to login page
			redirect('login', 'refresh');
		}
	}
	
	public function itemlist(){
		if ($this->session->userdata('logged_in')) 
        {
			$session_data=$this->session->userdata('logged_in');
			$usertype = $session_data['rolecode'];
			$userid = $session_data['id'];
			$slang="english";
		    $this->lang->load($slang, $slang);
		    $data['lang']=$this->lang->language; 
    	    $aColumns = array( 'item_id','item_name','item_description','item_price','item_image1','item_image2','item_image3','item_image4','item_image5','notes','status');
         
			/* Indexed column (used for fast and accurate table cardinality) */
			$sIndexColumn = "item_id";
         
			/* DB table to use */
			$sTable = "items";

            /*
			 * Paging
			 */
			$sLimit = "";
			if ( isset( $_GET['start'] ) && $_GET['length'] != '-1' )
			{
				$sLimit = "LIMIT ".intval( $_GET['start'] ).", ".
					intval( $_GET['length'] );
			}


    
     
			$sOrder = "";
			if ( isset( $_GET['order'] ) )
			{
				$sOrder = "ORDER BY  ";
			
						$sOrder .= $aColumns[0]."
							".($_GET['order'][0]['dir']==='desc' ? 'asc' : 'desc') .", ";
			   
				 
				$sOrder = substr_replace( $sOrder, "", -2 );
				if ( $sOrder == "ORDER BY" )
				{
					$sOrder = "";
				}
			}
	

        
        $commn="1=1 and deleted='0' ";
        
		
		//echo $_REQUEST['columns'][1]['search']['value']; exit();
		
		if(!empty($_GET['columns'][1]['search']['value']) ){  
	
		$commn.=" AND items.item_name  LIKE '".$_GET['columns'][1]['search']['value']."%' ";
		}

        $sWhere="where $commn";
         
		
		if (isset($_GET['search']['value']) && $_GET['search']['value'] != "" )
		{
			 $sWhere = "WHERE $commn   AND (";
			for ( $i=0 ; $i<count($aColumns) ; $i++ )
			{
				
				/*if ( isset($_GET['columns'][$i]['searchable']) && $_GET['columns'][$i]['searchable'] == "true" )
				{*/
					$sWhere .= $aColumns[$i]." LIKE '%".mysql_real_escape_string( $_GET['search']['value'] )."%' OR ";
				/*}*/
			}
			$sWhere = substr_replace( $sWhere, "", -3 );
			$sWhere .= ')';
		}
		
		
       $join='';
        /*
         * SQL queries
         * Get data to display
         */
        $sQuery = "
            SELECT SQL_CALC_FOUND_ROWS ".str_replace(" , ", " ", implode(", ", $aColumns))."
            FROM   $sTable $join
            $sWhere
            $sOrder
            $sLimit";

       //echo  $sQuery; exit();


        $rResult = mysql_query( $sQuery) or fatal_error( 'MySQL Error: ' . mysql_errno() );
         
        /* Data set length after filtering */
        $sQuery = "SELECT FOUND_ROWS()";
        $rResultFilterTotal = mysql_query( $sQuery) or fatal_error( 'MySQL Error: ' . mysql_errno() );
        $aResultFilterTotal = mysql_fetch_array($rResultFilterTotal);
        $iFilteredTotal = $aResultFilterTotal[0];
          
        /* Total data set length */
        $sQuery = "
            SELECT COUNT(".$sIndexColumn.")
            FROM   $sTable
        ";
        $rResultTotal = mysql_query( $sQuery) or fatal_error( 'MySQL Error: ' . mysql_errno() );
        $aResultTotal = mysql_fetch_array($rResultTotal);
        $iTotal = $aResultTotal[0];
         
        /*
         * Output
         */
        $output = array(
                        "sEcho" => '',
                        "iTotalRecords" => $iTotal,
                        "iTotalDisplayRecords" => $iFilteredTotal,
                        "aaData" => array()
                        );

            $sColumns = array('item_name','item_price','status','action');
         
            while ( $aRow = mysql_fetch_array( $rResult ) )
            {
                $row = array();
            	
				$item_id=$aRow['item_id'];
				//$user_type=$aRow['usertype_id'];
				//$business_id=$aRow['business_id'];
				$status=$aRow['status'];
                for ( $i=0 ; $i<count($sColumns) ; $i++ )
                {
                   /*  if ( $sColumns[$i] == "version" )
                    {
                       
                        $row[] = ($aRow[ $sColumns[$i] ]=="0") ? '-' : $aRow[ $sColumns[$i] ];
                    } */
					
            	    if($sColumns[$i] =='action')
            		{
            			if($usertype!=0){
							$roles=$session_data['roles'];
							$role=trim($roles,',');
							$role=explode(',',$role);
							if(!empty($role)){
								foreach($role as $result){
									$query=$this->db->query("select * from user_permissions where role_id='$result' and menu_id='3'")->result();
									if($query){
										foreach($query as $res){
											$new[]=$res->new_id;
											$edit[]=$res->edit_id;
											$delete[]=$res->delete_id;
											$view[]=$res->view_id;
										}
						            }
							    }
							}
							
							if(in_array('1',$edit)){
							$edit_id='<a href="javascript:void(0)"  class="btn btn-info edit" val="'.$item_id.'"><i class="fa fa-edit"></i></a>';
							}else {
								$edit_id='';
							}
							if(in_array('1',$delete)){
								$delete_id=' <a href="javascript:void(0)"  class="btn btn-danger delete" val="'.$item_id.'"><i class="fa fa-trash-o"></i></a>';
							}else {
								$delete_id='';
							}
							if(in_array('1',$view)){
								$view_id=' <a href="javascript:void(0)"  class="btn btn-success view" val="'.$item_id.'"><i class="fa fa-fw fa-eye"></i></a>';
							}else {
								$view_id='';
							}
							
							 $row[]=$view_id.'&nbsp;'.$edit_id.'&nbsp;'.$delete_id;
							
						}else {

						$row[]='<a href="javascript:void(0)"  class="btn btn-success view" val="'.$item_id.'"><i class="fa fa-fw fa-eye"></i></a>&nbsp;<a href="javascript:void(0)"  class="btn btn-info edit" val="'.$item_id.'"><i class="fa fa-edit"></i></a>&nbsp; <a href="javascript:void(0)"  class="btn btn-danger delete" val="'.$item_id.'"><i class="fa fa-trash-o"></i></a>
        			  ';
						}
            		}
					else if($sColumns[$i] =='usertype_id'){
						if($user_type==1){
							$row[] ='item';
						}elseif($user_type==2){ 
							$row[] ='Representative';
						}
						elseif($user_type==3){ 
							$row[] ='Employee';
						}
						else {
							$row[] ='Supplier';
						}
					}
					/*elseif($sColumns[$i] =='status'){
						if($status=='1'){
							$row[]='Active';
						
						}else {
							$row[]='Inactive';
						}
					}*/
					else if($sColumns[$i] =='business_id'){
						if($business_id!=0){
							//echo "select * from business where business_id='$business_id'";exit;
							$business=$this->db->query("select * from nrm_business where business_id='$business_id'")->result_array();
							if(!empty($business)){
								$row[] = $business[0]['business_name'];
							}else{
								$row[] ='';
							}
						}else {
							$row[] ='';
						}
					}
					
					else if($sColumns[$i] =='status')
            		{
            			
            			 $Enable_status="";
			             $Disable_status="";
						 
						 
					    if($status=='1')
						{
						 $Enable_status="selected";
			             $Disable_status="";
						
						}
						else if($status=='0')
						{
						 $Enable_status="";
			             $Disable_status="selected";
						 
						}
						
            			
            			 $row[]='<select name="status" id="status" class="selectpicker update_status">
								 <option value="1@@@'.$item_id.'"  '.$Enable_status.'>Enable</option>
								 <option value="0@@@'.$item_id.'"  '.$Disable_status.'>Disable</option>
								 </select>';
            			 
            		}
                    else if ( $sColumns[$i] != ' ' )
                    {
                        /* General output */
                        $row[] = $aRow[ $sColumns[$i] ];
                    }
                }
                $output['aaData'][] = $row;
            }
            //print_r($output);exit;
        echo json_encode( $output );
    }
	}
	
	
	public function add($item_id=''){
		
		if ($this->session->userdata('logged_in'))		
		{  
			$session_data = $this->session->userdata('logged_in');
			$slang="english";
		    $this->lang->load($slang, $slang);
		    $data['lang']=$this->lang->language; 
			$data['title'] = "User Role";
			if($item_id!=''){
				$user_details=$this->db->query("select * from items where item_id='$item_id'")->result_array();
				if(!empty($user_details)){
					$data['item_id'] = $user_details[0]['item_id'];
					$data['menu_id'] = $user_details[0]['menu_id'];
					$data['category_id'] = $user_details[0]['category_id'];
					$data['store_id'] = $user_details[0]['store_id'];
					$data['item_name'] = $user_details[0]['item_name'];
					$data['item_description'] = $user_details[0]['item_description'];
					$data['item_price'] = $user_details[0]['item_price'];
					$data['item_status'] = $user_details[0]['item_status'];
					$data['item_image1'] = $user_details[0]['item_image1'];
					$data['item_image2'] = $user_details[0]['item_image2'];
					$data['item_image3'] = $user_details[0]['item_image3'];
					$data['item_image4'] = $user_details[0]['item_image4'];
					$data['item_image5'] = $user_details[0]['item_image5'];
					$data['gluten_free'] = $user_details[0]['gluten_free'];
					$data['need_spice'] = $user_details[0]['need_spice'];
					$data['notes'] = $user_details[0]['notes'];
					$data['discount_amount'] = $user_details[0]['discount_amount'];
					
					
					if($user_details[0]['start_date']!='' && $user_details[0]['start_date']!='0000-00-00')
					{
					   $start_date=date("d-m-Y",strtotime($user_details[0]['start_date']));
					}
					else
					{
					 $start_date="";
					}
					
					if($user_details[0]['end_date']!='' && $user_details[0]['end_date']!='0000-00-00')
					{
					   $end_date=date("d-m-Y",strtotime($user_details[0]['end_date']));
					}
					else
					{
					 $end_date="";
					}
					
					$data['start_date'] = $start_date;
					$data['end_date'] = $end_date;
					$data['only_text'] = $user_details[0]['only_text'];
					$data['only_image'] = $user_details[0]['only_image'];
					$data['text_with_image'] = $user_details[0]['text_with_image'];
					
					
					$data['status'] = $user_details[0]['status'];
					
					
					$extra_id_arr=array();
					$sql = " select * from extras_to_items where item_id='$item_id' ";
					$mainquery = $this->db->query($sql);
					$arrayquery = $mainquery->result();
					if(!empty($arrayquery))
					{
						foreach ($arrayquery as $row) 
						{
						$extra_id_arr[] = $row->extra_id;
						}
					}
					
					//echo "<pre>";
					//print_r($extra_id_arr);
					$data['extra_id_sel'] =$extra_id_arr;
					
					
					$option_id_arr=array();
					$sql = " select * from options_to_items where item_id='$item_id' ";
					$mainquery = $this->db->query($sql);
					$arrayquery = $mainquery->result();
					if(!empty($arrayquery))
					{
						foreach ($arrayquery as $row) 
						{
						$option_id_arr[] = $row->option_id;
						}
					}
					
					//echo "<pre>";
					//print_r($option_id_arr);
					$data['option_id_sel'] =$option_id_arr;
					
					
					
					$seg_details=$this->db->query("select * from item_segment where item_id='$item_id'")->result_array();
				    if(!empty($seg_details)){
					 $data['segment_id_sel'] = $seg_details[0]['segment_id'];
					}
					else
					{
					 $data['segment_id_sel'] = '';
					}
					
					
					
					
					
					
					
				}
			}else {
			        $data['item_id'] = '';
					$data['menu_id'] = '';
					$data['category_id'] = '';
					$data['store_id'] = '';
					$data['item_name'] = '';
					$data['item_description'] = '';
					$data['item_price'] = '';
					$data['item_status'] = '';
					$data['item_image1'] = '';
					$data['item_image2'] = '';
					$data['item_image3'] = '';
					$data['item_image4'] = '';
					$data['item_image5'] = '';
					$data['gluten_free'] = '';
					$data['need_spice'] = '';
					$data['notes'] = '';
					$data['discount_amount'] = '';
					$data['start_date'] = '';
					$data['end_date'] = '';
					$data['only_text'] = '';
					$data['only_image'] = '';
					$data['text_with_image'] = '';
					$data['status'] = '';
					$extra_id_arr=array();
					$data['extra_id_sel'] = $extra_id_arr;
					$option_id_arr=array();
					$data['option_id_sel'] = $option_id_arr;
					$data['segment_id_sel'] = '';
			}
			$this->load->view('admin/item/item_management', $data);
		} 
		else 
		{
			//If no session, redirect to login page
			redirect('login', 'refresh');
		}
	}
	
	
		
	  public function checkexistemail()
{			// checking the email  exist  validation in add form
	
    $item_email = $this->input->post('item_email');
    $sql = "SELECT item_id,item_email from items WHERE item_email = '$item_email'";
    $query = $this->db->query($sql);
    if( $query->num_rows() > 0 ){
        echo 'false';
    } else {
        echo 'true';
    }
	
}

public function checkexisttelephone()
{			// checking the email  exist  validation in add form
	
    $item_telephone1 = $this->input->post('item_telephone1');
    $sql = "SELECT item_id,item_telephone1 from items WHERE item_telephone1 = '$item_telephone1'";
    $query = $this->db->query($sql);
    if( $query->num_rows() > 0 ){
        echo 'false';
    } else {
        echo 'true';
    }
	
}


public function checkexistmobile()
{			// checking the email  exist  validation in add form
	
    $item_mobile1 = $this->input->post('item_mobile1');
    $sql = "SELECT item_id,item_mobile1 from items WHERE item_mobile1 = '$item_mobile1'";
    $query = $this->db->query($sql);
    if( $query->num_rows() > 0 ){
        echo 'false';
    } else {
        echo 'true';
    }
	
}

public function get_segment()
{

		
		if ($this->session->userdata('logged_in'))		
		{  
			$session_data = $this->session->userdata('logged_in');
			$slang="english";
		    $this->lang->load($slang, $slang);
		    $data['lang']=$this->lang->language; 
			$data['title'] = "User Role";
			$item_id=$this->input->post('item_id');
			$segment_id=$this->input->post('segment_id');
			$level1segment_item_price = "";
			$level2segment_item_price = "";
			$level3segment_item_price = "";
			$level4segment_item_price = "";
			
			if($segment_id!=''){
			//echo "select * from segments where segment_id='$segment_id' ";
				$segments_details=$this->db->query("select * from segments where segment_id='$segment_id' ")->result_array();
				if(!empty($segments_details)){
				
					$segment_id = $segments_details[0]['segment_id'];
					$segment_name = $segments_details[0]['segment_name'];
					$level1segment = $segments_details[0]['level1segment'];
					$level2segment = $segments_details[0]['level2segment'];
					$level3segment = $segments_details[0]['level3segment'];
					$level4segment = $segments_details[0]['level4segment'];
					
					if($item_id!='')
					{
					
						$segments1_details=$this->db->query("select * from item_pricing where item_id_ref='$item_id' and  segment_id='$segment_id' and  segment_label='$level1segment' and status='1' and deleted='0' ")->result_array();
						if(!empty($segments1_details)){
						$level1segment_item_price = $segments1_details[0]['item_price'];
						}
						
						
						$segments2_details=$this->db->query("select * from item_pricing where item_id_ref='$item_id' and  segment_id='$segment_id' and  segment_label='$level2segment' and status='1' and deleted='0'  ")->result_array();
						if(!empty($segments2_details)){
						$level2segment_item_price = $segments2_details[0]['item_price'];
						}
						
						$segments3_details=$this->db->query("select * from item_pricing where item_id_ref='$item_id' and  segment_id='$segment_id' and  segment_label='$level3segment' and status='1' and deleted='0'  ")->result_array();
						if(!empty($segments3_details)){
						$level3segment_item_price = $segments3_details[0]['item_price'];
						}
						
						$segments4_details=$this->db->query("select * from item_pricing where item_id_ref='$item_id' and  segment_id='$segment_id' and  segment_label='$level4segment' and status='1' and deleted='0'  ")->result_array();
						if(!empty($segments4_details)){
						$level4segment_item_price = $segments4_details[0]['item_price'];
						}
						
					}
					else
					{
					    $level1segment_item_price = "";
						$level2segment_item_price = "";
						$level3segment_item_price = "";
						$level4segment_item_price = "";
					}
					
					
					
					
					
					if($level1segment!='No')
					{
					?>
					
					<div class="form-group">
					    <label for="inputEmail3"  class="col-sm-2 control-label"><?php echo $level1segment." Price" ;?>($)
                        <span style="color:#F00">*</span>
                        </label>
						<div class="col-sm-8">
						    <input  type='text'  class="form-control" id="level1segment_price" name="level1segment_price" value='<?php echo $level1segment_item_price;?>'>
                            <?php echo form_error('level1segment_price', '<div class="error" style="color:red;">', '</div>'); ?>
						</div>
						
					</div>
					<?php
					}
					else
					{
					?>
					<input  type='hidden'  class="form-control" id="level1segment_price" name="level1segment_price" value='0'>
                    
					<?php
					}
					
					
					
					
					if($level2segment!='No')
					{
					?>
					
					<div class="form-group">
					    <label for="inputEmail3"  class="col-sm-2 control-label"><?php echo $level2segment." Price" ;?>($)
                        <span style="color:#F00">*</span>
                        </label>
						<div class="col-sm-8">
						    <input  type='text'  class="form-control" id="level2segment_price" name="level2segment_price" value='<?php echo $level2segment_item_price;?>'>
                            <?php echo form_error('level2segment', '<div class="error" style="color:red;">', '</div>'); ?>
						</div>
						
					</div>
					<?php
					}
					else
					{
					?>
					<input  type='hidden'  class="form-control" id="level2segment_price" name="level2segment_price" value='0'>
                    
					<?php
					}
					
					
					
					
					
					
					
					if($level3segment!='No')
					{
					?>
					
					<div class="form-group">
					    <label for="inputEmail3"  class="col-sm-2 control-label"><?php echo $level3segment." Price" ;?>($)
                        <span style="color:#F00">*</span>
                        </label>
						<div class="col-sm-8">
						    <input  type='text'  class="form-control" id="level3segment_price" name="level3segment_price" value='<?php echo $level3segment_item_price;?>'>
                            <?php echo form_error('level3segment', '<div class="error" style="color:red;">', '</div>'); ?>
						</div>
						
					</div>
					<?php
					}
					else
					{
					?>
					<input  type='hidden'  class="form-control" id="level3segment_price" name="level3segment_price" value='0'>
                    
					<?php
					}
					
					if($level4segment!='No')
					{
					?>
					
					<div class="form-group">
					    <label for="inputEmail3"  class="col-sm-2 control-label"><?php echo $level4segment." Price" ;?>($)
                        <span style="color:#F00">*</span>
                        </label>
						<div class="col-sm-8">
						    <input  type='text'  class="form-control" id="level4segment_price" name="level4segment_price" value='<?php echo $level4segment_item_price;?>'>
                            <?php echo form_error('level4segment', '<div class="error" style="color:red;">', '</div>'); ?>
						</div>
						
					</div>
					<?php
					}
					else
					{
					?>
					<input  type='hidden'  class="form-control" id="level4segment_price" name="level4segment_price" value='0'>
                    
					<?php
					}
					
					
					
					
				}
			}
			else
			{
			?>
			<input  type='hidden'  class="form-control" id="level1segment_price" name="level1segment_price" value='0'>
            <input  type='hidden'  class="form-control" id="level2segment_price" name="level2segment_price" value='0'>
			<input  type='hidden'  class="form-control" id="level3segment_price" name="level3segment_price" value='0'>
			<input  type='hidden'  class="form-control" id="level4segment_price" name="level4segment_price" value='0'>
			<?php
			}
			
		} 
		else 
		{
			//If no session, redirect to login page
			redirect('login', 'refresh');
		}
	

}
	
	public function insert_item(){
		
		if ($this->session->userdata('logged_in')) {
			$session_data = $this->session->userdata('logged_in');
			$created_date = date('Y-m-d');
			$userid=$session_data['id'];
			$item_id=$this->input->post('item_id');
			
			if($item_id!='')
			{
			
			     $start_date=date("Y-m-d",strtotime($this->input->post('start_date')));
				 $end_date=date("Y-m-d",strtotime($this->input->post('end_date')));
				 
				 
				 $item_check_type=$this->input->post('item_check_type');
				 
				 if($item_check_type=='only_text')
				 {
				 $only_text='1';
				 $only_image='0';
				 $text_with_image='0';
				 }
				 else if($item_check_type=='only_image')
				 {
				  $only_text='0';
				  $only_image='1';
				  $text_with_image='0';
				 }
				 else if($item_check_type=='text_with_image')
				 {
				  $only_text='0';
				  $only_image='0';
				  $text_with_image='1';
				 }
			
				
				  $data = array(
				    'menu_id'=>$this->input->post('menu_id'),
					'category_id'=>$this->input->post('category_id'),
					/*'store_id'=>$this->input->post('store_id'),*/
					'item_name'=>$this->input->post('item_name'),
					'item_description'=>$this->input->post('item_description'),
					'item_price'=>$this->input->post('item_price'),
					'discount_amount'=>$this->input->post('discount_amount'),
					'start_date'=>$start_date,
					'end_date'=>$end_date,
					'only_text'=>$only_text,
					'only_image'=>$only_image,
					'text_with_image'=>$text_with_image,
					'item_status'=>$this->input->post('item_status'),
					'item_image1'=>$this->input->post('item_image1'),
					'item_image2'=>$this->input->post('item_image2'),
					'item_image3'=>$this->input->post('item_image3'),
					'item_image4'=>$this->input->post('item_image4'),
					'item_image5'=>$this->input->post('item_image5'),
					'notes'=>$this->input->post('notes'),
					'status'=>$this->input->post('status'),
					);
					
					echo "<pre>";
					print_r($data);
					
				   $updateuserrole = $this->Item_model->item_update($data,$item_id);
			       echo $item_id;
				   
				   
				   
				   
				   
				 /***************************************************Add category_to_items****************************************************************/
				$category_id=$this->input->post('category_id');
				
				    //$sql_category_item = 'select * from item_category  WHERE   item_id="'.$item_id.'"  and  category_id="'.$category_id.'"  ';
					$sql_category_item = 'select * from item_category  WHERE   item_id="'.$item_id.'"   ';
					$query_category_item = $this->db->query($sql_category_item)->result_array();
					
				    if(!empty($query_category_item))
					{
					
					    $item_category_id = stripslashes($query_category_item['0']['item_category_id']); 
					    //$this->Item_model->delete_item_category_item($item_id);
						
						$item_name=$this->input->post('item_name');
						$category_id=$this->input->post('category_id');
						$category_name='';
						$sql_category_item = 'select * from category  WHERE   category_id="'.$category_id.'"';
						$query_category_item = $this->db->query($sql_category_item)->result_array();
						if(!empty($query_category_item))
						{
						$category_name = stripslashes($query_category_item['0']['category_level3']);
						}
						
						
						$data = array(
						'item_id'=>$item_id,
						'item_name'=>$item_name,
						'category_id'=>$category_id,
						'category_name'=>$category_name,
						);
						
						
						echo "<pre>";
						print_r($data);
						echo "insert";
						
						$updatecategoryrole = $this->Item_model->category_to_items_update($data,$item_category_id);  
						
					}
				    else
					{
					
					   
					   // $this->Item_model->delete_item_category_item($item_id);
						
						$item_name=$this->input->post('item_name');
						$category_id=$this->input->post('category_id');
						$category_name='';
						$sql_category_item = 'select * from category  WHERE   category_id="'.$category_id.'"';
						$query_category_item = $this->db->query($sql_category_item)->result_array();
						if(!empty($query_category_item))
						{
						$category_name = stripslashes($query_category_item['0']['category_level3']);
						}
						
						
						$data = array(
						'item_id'=>$item_id,
						'item_name'=>$item_name,
						'category_id'=>$category_id,
						'category_name'=>$category_name,
						);
						
						
						echo "<pre>";
						print_r($data);
						echo "insert";
						
						$updatecategoryrole = $this->Item_model->category_to_items_insert($data);  
						
					
						
						
					}
				
				
			
					  
				 
				  
				   
				   
				   
				   
				   
				   
				   
				   
				  			
				/***************************************************Add extras_to_items****************************************************************/
				
				
				    $sql_check_extra_item = 'select * from extras_to_items  WHERE   item_id="'.$item_id.'"';
					$query_check_extra_item = $this->db->query($sql_check_extra_item)->result_array();
					
				    if(!empty($query_check_extra_item))
					{
						//$item_id = stripslashes($query_check_extra_item['0']['item_id']);
						$this->Item_model->delete_extra_item($item_id);
						
								$extra_id=trim($this->input->post('extra_id'),",");
								if(!empty($extra_id))
								{
								$extra_id_arr=explode(",",$extra_id);
								if(!empty($extra_id_arr))
								{
								foreach($extra_id_arr as $key => $value)
								{
								$extra_id=$value;
								
								
								
					$sql_check_extra_item = 'select * from extras_to_items  WHERE   item_id="'.$item_id.'" and  extra_id="'.$extra_id.'"  ';
					$query_check_extra_item = $this->db->query($sql_check_extra_item)->result_array();
					
				    if(empty($query_check_extra_item))
					{
					
								
								$data = array(
								'item_id'=>$item_id,
								'extra_id'=>$extra_id,
								);
								
								
								echo "<pre>";
								print_r($data);
								echo "insert";
								
								$updateuserrole = $this->Item_model->extras_to_items_insert($data);  
								
								
					}
								
								}
								}
								
								}

						
						
					}
					else
					{
				
				
				
				
				
								$extra_id=trim($this->input->post('extra_id'),",");
								if(!empty($extra_id))
								{
								$extra_id_arr=explode(",",$extra_id);
								if(!empty($extra_id_arr))
								{
								foreach($extra_id_arr as $key => $value)
								{
								$extra_id=$value;
								
								$data = array(
								'item_id'=>$item_id,
								'extra_id'=>$extra_id,
								);
								
								
								echo "<pre>";
								print_r($data);
								echo "insert";
								
								$updateuserrole = $this->Item_model->extras_to_items_insert($data);  
								
								}
								}
								
								}
				 
				 
					}
				
			/***************************************************End extras_to_items****************************************************************/
				
				
				
				
		   /***************************************************Add options_to_items****************************************************************/
				
				
				    $sql_check_option_item = 'select * from options_to_items  WHERE   item_id="'.$item_id.'"';
					$query_check_option_item = $this->db->query($sql_check_option_item)->result_array();
					
				    if(!empty($query_check_option_item))
					{
						//$item_id = stripslashes($query_check_option_item['0']['item_id']);
						$this->Item_model->delete_option_item($item_id);
						
						
								$option_id=trim($this->input->post('option_id'),",");
								 if(!empty($option_id))
								 {
								  $option_id_arr=explode(",",$option_id);
								  if(!empty($option_id_arr))
								  {
								  foreach($option_id_arr as $key => $value)
								  {
									$option_id=$value;
									
									
					$sql_check_option_item = 'select * from options_to_items  WHERE   item_id="'.$item_id.'" and  option_id="'.$option_id.'"  ';
					$query_check_option_item = $this->db->query($sql_check_option_item)->result_array();
					
				    if(empty($query_check_option_item))
					{
								
										  
									$data = array(
									'item_id'=>$item_id,
									'option_id'=>$option_id,
									);
								
									
									echo "<pre>";
									print_r($data);
									echo "insert";
									
									$updateuserrole = $this->Item_model->options_to_items_insert($data); 
									
									
					 }
									  
								  }
								  }
								  
								 }

						
						
					}
				   else
				   {
				
				
				
				
								 $option_id=trim($this->input->post('option_id'),",");
								 if(!empty($option_id))
								 {
								  $option_id_arr=explode(",",$option_id);
								  if(!empty($option_id_arr))
								  {
								  foreach($option_id_arr as $key => $value)
								  {
									$option_id=$value;
										  
									$data = array(
									'item_id'=>$item_id,
									'option_id'=>$option_id,
									);
								
									
									echo "<pre>";
									print_r($data);
									echo "insert";
									
									$updateuserrole = $this->Item_model->options_to_items_insert($data);  
									  
								  }
								  }
								  
								 }
				 
				 
				   }
				
			/***************************************************End options_to_items****************************************************************/ 
				   
				   
				   
				   
				    $menu_id=$this->input->post('menu_id');
					$menu_details=$this->db->query("select * from menu where menu_id='$menu_id'")->result_array();
					$menu_id ='0';
					$menu_name ='';
					if(!empty($menu_details)){
					$menu_id = $menu_details[0]['menu_id'];
					$menu_name = $menu_details[0]['menu_name'];
					}
				   
				   
					
					 
					
					
					
					$segment_id=$this->input->post('segment_id');
					
					if($segment_id!='')
					{
					      //  echo "select * from segments where segment_id='$segment_id'";
						  
						  
						   $this->db->query("delete from item_segment where item_id='$item_id'");
						  
							
							$segments_details=$this->db->query("select * from segments where segment_id='$segment_id'")->result_array();
							if(!empty($segments_details)){
							$segment_id = $segments_details[0]['segment_id'];
							$segment_name = $segments_details[0]['segment_name'];
							$level1segment = $segments_details[0]['level1segment'];
							$level2segment = $segments_details[0]['level2segment'];
							$level3segment = $segments_details[0]['level3segment'];
							$level4segment = $segments_details[0]['level4segment'];
							}
					
							$data = array(
							'item_id'=>$item_id,
							'item_name'=>$this->input->post('item_name'),
							'segment_id'=>$segment_id,
							'segment_name'=>$segment_name,
							'level1segment'=>$level1segment,
							'level2segment'=>$level2segment,
							'level3segment'=>$level3segment,
							'level4segment'=>$level4segment,
							);
							
							
							echo "<pre>";
							print_r($data);
							echo "insert";
							
							$updateuserrole = $this->Item_model->item_segment_insert($data);
							
							/*****************************************************************************************************/
							echo "ppppppppppppppppppppppppppppp";
							
							$sql_price_item = 'select * from item_pricing  WHERE   item_id_ref="'.$item_id.'" and status="1" and  deleted="0"  ';
							$query_price_item = $this->db->query($sql_price_item)->result_array();
							
							if(!empty($query_price_item))
							{
							
							
							$check_segment_id=$query_price_item[0]['segment_id'];
							
							if($check_segment_id!='No')
							{
								    $level1segment_price=$this->input->post('level1segment_price');
									$level2segment_price=$this->input->post('level2segment_price');
									$level3segment_price=$this->input->post('level3segment_price');
									$level4segment_price=$this->input->post('level4segment_price');
									
									
								
								  if($segment_id==$check_segment_id)
								  {
									  
									 $segments_item_price_details_level_1=$this->db->query("select * from item_pricing where item_id_ref ='$item_id' and segment_id='$segment_id' and segment_label='$level1segment' ")->result_array();     
									 
									 if(!empty($segments_item_price_details_level_1)){ 
									 
										 $item_price_id=$segments_item_price_details_level_1[0]['item_id'];
										 
											$data = array(
											'menu_id'=>$this->input->post('menu_id'),
											'menu_name'=>$menu_name,
											'category_id'=>$this->input->post('category_id'),
											'item_id_ref'=>$item_id,
											'item_name'=>$this->input->post('item_name'),
											'item_price'=>$level1segment_price,
											'segment_id'=>$segment_id,
											'segment_label'=>$level1segment,
											'status'=>$this->input->post('status'),
											);
											
											echo "aaaa";
											echo "<pre>";
											print_r($data);
											echo "insert";
											
											$updateuserrole = $this->Item_model->item_price_update($data,$item_price_id);
										 
									 }
									 
									 $segments_item_price_details_level_2=$this->db->query("select * from item_pricing where item_id_ref ='$item_id' and segment_id='$segment_id' and segment_label='$level2segment' ")->result_array();     
									 
									 if(!empty($segments_item_price_details_level_2)){ 
									 
										 $item_price_id=$segments_item_price_details_level_2[0]['item_id'];
										 
											$data = array(
											'menu_id'=>$this->input->post('menu_id'),
											'menu_name'=>$menu_name,
											'category_id'=>$this->input->post('category_id'),
											'item_id_ref'=>$item_id,
											'item_name'=>$this->input->post('item_name'),
											'item_price'=>$level2segment_price,
											'segment_id'=>$segment_id,
											'segment_label'=>$level2segment,
											'status'=>$this->input->post('status'),
											);
											
											echo "aaaa";
											echo "<pre>";
											print_r($data);
											echo "insert";
											
											$updateuserrole = $this->Item_model->item_price_update($data,$item_price_id);
										 
									 }
									 
									 
									 $segments_item_price_details_level_3=$this->db->query("select * from item_pricing where item_id_ref ='$item_id' and segment_id='$segment_id' and segment_label='$level3segment' ")->result_array();     
									 
									 if(!empty($segments_item_price_details_level_3)){ 
									 
										 $item_price_id=$segments_item_price_details_level_3[0]['item_id'];
										 
											$data = array(
											'menu_id'=>$this->input->post('menu_id'),
											'menu_name'=>$menu_name,
											'category_id'=>$this->input->post('category_id'),
											'item_id_ref'=>$item_id,
											'item_name'=>$this->input->post('item_name'),
											'item_price'=>$level3segment_price,
											'segment_id'=>$segment_id,
											'segment_label'=>$level3segment,
											'status'=>$this->input->post('status'),
											);
											
											echo "aaaa";
											echo "<pre>";
											print_r($data);
											echo "insert";
											
											$updateuserrole = $this->Item_model->item_price_update($data,$item_price_id);
										 
									 }
									 
									 echo "kkkkkkkkkkkkkkkkkkkkk";
									 
									 $segments_item_price_details_level_4=$this->db->query("select * from item_pricing where item_id_ref ='$item_id' and segment_id='$segment_id' and segment_label='$level4segment' ")->result_array();   
									 
									 //echo  "select * from item_pricing where item_id_ref ='$item_id' and segment_id='$segment_id' and segment_label='$level4segment' ";
									 
									 if(!empty($segments_item_price_details_level_4)){ 
									 
										 $item_price_id=$segments_item_price_details_level_4[0]['item_id'];
										 
											$data = array(
											'menu_id'=>$this->input->post('menu_id'),
											'menu_name'=>$menu_name,
											'category_id'=>$this->input->post('category_id'),
											'item_id_ref'=>$item_id,
											'item_name'=>$this->input->post('item_name'),
											'item_price'=>$level4segment_price,
											'segment_id'=>$segment_id,
											'segment_label'=>$level4segment,
											'status'=>$this->input->post('status'),
											);
											
											echo "aaaa";
											echo "<pre>";
											print_r($data);
											echo "insert";
											
											$updateuserrole = $this->Item_model->item_price_update($data,$item_price_id);
										 
									 }
									 else
									 {
									        // $item_price_id=$segments_item_price_details_level_4[0]['item_id'];
										 
											$data = array(
											'menu_id'=>$this->input->post('menu_id'),
											'menu_name'=>$menu_name,
											'category_id'=>$this->input->post('category_id'),
											'item_id_ref'=>$item_id,
											'item_name'=>$this->input->post('item_name'),
											'item_price'=>$level4segment_price,
											'segment_id'=>$segment_id,
											'segment_label'=>$level4segment,
											'status'=>$this->input->post('status'),
											);
											
											echo "aaaa";
											echo "<pre>";
											print_r($data);
											echo "insert";
											
											$updateuserrole = $this->Item_model->item_price_insert($data);
											
									 } 
									 
									  
								  }
								  else
								  {
								    $this->db->query("update item_pricing set deleted ='1',status='0' where item_id_ref ='$item_id'");
									  
									$level1segment_price=$this->input->post('level1segment_price');
									$level2segment_price=$this->input->post('level2segment_price');
									$level3segment_price=$this->input->post('level3segment_price');
									$level4segment_price=$this->input->post('level4segment_price');
									
									if($level1segment_price!='0' && $level1segment_price!='')
									{
									
									
									
									
									$data = array(
									'menu_id'=>$this->input->post('menu_id'),
									'menu_name'=>$menu_name,
									'category_id'=>$this->input->post('category_id'),
									'item_id_ref'=>$item_id,
									'item_name'=>$this->input->post('item_name'),
									'item_price'=>$level1segment_price,
									'segment_id'=>$segment_id,
									'segment_label'=>$level1segment,
									'status'=>$this->input->post('status'),
									);
									
									echo "aaaa";
									echo "<pre>";
									print_r($data);
									echo "insert";
									
									$updateuserrole = $this->Item_model->item_price_insert($data);
									
									}	
									
									if($level2segment_price!='0' && $level2segment_price!='')
									{
									$data = array(
									'menu_id'=>$this->input->post('menu_id'),
									'menu_name'=>$menu_name,
									'category_id'=>$this->input->post('category_id'),
									'item_id_ref'=>$item_id,
									'item_name'=>$this->input->post('item_name'),
									'item_price'=>$level2segment_price,
									'segment_id'=>$segment_id,
									'segment_label'=>$level2segment,
									'status'=>$this->input->post('status'),
									);
									
									echo "bbbb";
									echo "<pre>";
									print_r($data);
									echo "insert";
									
									$updateuserrole = $this->Item_model->item_price_insert($data);
									
									}
									
									if($level3segment_price!='0' && $level3segment_price!='')
									{
									$data = array(
									'menu_id'=>$this->input->post('menu_id'),
									'menu_name'=>$menu_name,
									'category_id'=>$this->input->post('category_id'),
									'item_id_ref'=>$item_id,
									'item_name'=>$this->input->post('item_name'),
									'item_price'=>$level3segment_price,
									'segment_id'=>$segment_id,
									'segment_label'=>$level3segment,
									'status'=>$this->input->post('status'),
									);
									
									echo "ccc";
									echo "<pre>";
									print_r($data);
									echo "insert";
									
									$updateuserrole = $this->Item_model->item_price_insert($data);
									
									}	
									
									if($level4segment_price!='0' && $level4segment_price!='')
									{
									$data = array(
									'menu_id'=>$this->input->post('menu_id'),
									'menu_name'=>$menu_name,
									'category_id'=>$this->input->post('category_id'),
									'item_id_ref'=>$item_id,
									'item_name'=>$this->input->post('item_name'),
									'item_price'=>$level3segment_price,
									'segment_id'=>$segment_id,
									'segment_label'=>$level4segment,
									'status'=>$this->input->post('status'),
									);
									
									echo "ccc";
									echo "<pre>";
									print_r($data);
									echo "insert";
									
									$updateuserrole = $this->Item_model->item_price_insert($data);
									
									}	
									
									
									
									
										
								  }
								   
							}
							else
							{
									$this->db->query("update item_pricing set deleted ='1',status='0' where item_id_ref ='$item_id'");
									
									//echo "delete from item_pricing where item_id_ref='$item_id'";
									// $this->db->query("delete from item_pricing where item_id_ref='$item_id'");  
									
									$level1segment_price=$this->input->post('level1segment_price');
									$level2segment_price=$this->input->post('level2segment_price');
									$level3segment_price=$this->input->post('level3segment_price');
									$level4segment_price=$this->input->post('level4segment_price');
									
									if($level1segment_price!='0' && $level1segment_price!='')
									{
									
									
									
									
									$data = array(
									'menu_id'=>$this->input->post('menu_id'),
									'menu_name'=>$menu_name,
									'category_id'=>$this->input->post('category_id'),
									'item_id_ref'=>$item_id,
									'item_name'=>$this->input->post('item_name'),
									'item_price'=>$level1segment_price,
									'segment_id'=>$segment_id,
									'segment_label'=>$level1segment,
									'status'=>$this->input->post('status'),
									);
									
									echo "aaaa";
									echo "<pre>";
									print_r($data);
									echo "insert";
									
									$updateuserrole = $this->Item_model->item_price_insert($data);
									
									}	
									
									if($level2segment_price!='0' && $level2segment_price!='')
									{
									$data = array(
									'menu_id'=>$this->input->post('menu_id'),
									'menu_name'=>$menu_name,
									'category_id'=>$this->input->post('category_id'),
									'item_id_ref'=>$item_id,
									'item_name'=>$this->input->post('item_name'),
									'item_price'=>$level2segment_price,
									'segment_id'=>$segment_id,
									'segment_label'=>$level2segment,
									'status'=>$this->input->post('status'),
									);
									
									echo "bbbb";
									echo "<pre>";
									print_r($data);
									echo "insert";
									
									$updateuserrole = $this->Item_model->item_price_insert($data);
									
									}
									
									if($level3segment_price!='0' && $level3segment_price!='')
									{
									$data = array(
									'menu_id'=>$this->input->post('menu_id'),
									'menu_name'=>$menu_name,
									'category_id'=>$this->input->post('category_id'),
									'item_id_ref'=>$item_id,
									'item_name'=>$this->input->post('item_name'),
									'item_price'=>$level3segment_price,
									'segment_id'=>$segment_id,
									'segment_label'=>$level3segment,
									'status'=>$this->input->post('status'),
									);
									
									echo "ccc";
									echo "<pre>";
									print_r($data);
									echo "insert";
									
									$updateuserrole = $this->Item_model->item_price_insert($data);
									
									}		
								
								
								   if($level4segment_price!='0' && $level4segment_price!='')
									{
									$data = array(
									'menu_id'=>$this->input->post('menu_id'),
									'menu_name'=>$menu_name,
									'category_id'=>$this->input->post('category_id'),
									'item_id_ref'=>$item_id,
									'item_name'=>$this->input->post('item_name'),
									'item_price'=>$level3segment_price,
									'segment_id'=>$segment_id,
									'segment_label'=>$level4segment,
									'status'=>$this->input->post('status'),
									);
									
									echo "ccc";
									echo "<pre>";
									print_r($data);
									echo "insert";
									
									$updateuserrole = $this->Item_model->item_price_insert($data);
									
									}	
								
							}
							
							
							
							
							}
									
									
									
							
					
					
					}
					else
					{
					
					//echo "delete from item_pricing where item_id_ref='$item_id'";
					//$this->db->query("delete from item_pricing where item_id_ref='$item_id'"); 
					
					$this->db->query("delete from item_segment where item_id='$item_id'");
					
					
									$sql_price_item = 'select * from item_pricing  WHERE   item_id_ref="'.$item_id.'"  and status="1"  and  deleted="0"  ';
									$query_price_item = $this->db->query($sql_price_item)->result_array();
									
									if(!empty($query_price_item))
									{
									$check_segment_id=$query_price_item[0]['segment_id'];
									
									if($check_segment_id!='No')
									{
										$this->db->query("update item_pricing set deleted ='1',status='0' where item_id_ref ='$item_id'");
										
										
										
											$data = array(
											'menu_id'=>$this->input->post('menu_id'),
											'menu_name'=>$menu_name,
											'category_id'=>$this->input->post('category_id'),
											'item_id_ref'=>$item_id,
											'item_name'=>$this->input->post('item_name'),
											'item_price'=>$this->input->post('item_price'),
											'segment_id'=>'No',
											'segment_label'=>'No',
											'status'=>$this->input->post('status'),
											);
											
											
											echo "<pre>";
											print_r($data);
											echo "insert";
											
											$updateuserrole = $this->Item_model->item_price_insert($data);
									}
									else
									{
									
									$item_price_id=$query_price_item[0]['item_id'];
									
									$data = array(
									'menu_id'=>$this->input->post('menu_id'),
									'menu_name'=>$menu_name,
									'category_id'=>$this->input->post('category_id'),
									'item_id_ref'=>$item_id,
									'item_name'=>$this->input->post('item_name'),
									'item_price'=>$this->input->post('item_price'),
									'segment_id'=>'No',
									'segment_label'=>'No',
									'status'=>$this->input->post('status'),
									);
									
									
									echo "<pre>";
									print_r($data);
									echo "insert";
									
									$updateuserrole = $this->Item_model->item_price_update($data,$item_price_id);
									
									}
									
									}
									
					
					
					}
				   
				   
				   
				   
				   
				   
				   /*$menu_id=$this->input->post('menu_id');
				   $menu_details=$this->db->query("select * from menu where menu_id='$menu_id'")->result_array();
				   $menu_id ='0';
				   $menu_name ='';
				   if(!empty($menu_details)){
					$menu_id = $menu_details[0]['menu_id'];
					$menu_name = $menu_details[0]['menu_name'];
				   }
				   
				 $data = array(
				'menu_id'=>$this->input->post('menu_id'),
				'menu_name'=>$menu_name,
				'category_id'=>$this->input->post('category_id'),
				'item_id_ref'=>$item_id,
				'item_name'=>$this->input->post('item_name'),
				'item_price'=>$this->input->post('item_price'),
				'segment_id'=>'No',
				'segment_label'=>'No',
				'status'=>$this->input->post('status'),
				);
				
				echo "<pre>";
				print_r($data);
				echo "update";
				
				$updateuserrole = $this->Item_model->item_price_update($data,$item_id);*/
				
				
				
				
				
	
				
				
				
				
				
			
				  
				  
				
			}else {
				
				
				
				  $start_date=date("Y-m-d",strtotime($this->input->post('start_date')));
				 $end_date=date("Y-m-d",strtotime($this->input->post('end_date')));
				 
				 
				  $item_check_type=$this->input->post('item_check_type');
				 
				 if($item_check_type=='only_text')
				 {
				 $only_text='1';
				 $only_image='0';
				 $text_with_image='0';
				 }
				 else if($item_check_type=='only_image')
				 {
				  $only_text='0';
				  $only_image='1';
				  $text_with_image='0';
				 }
				 else if($item_check_type=='text_with_image')
				 {
				  $only_text='0';
				  $only_image='0';
				  $text_with_image='1';
				 }
				
				
				     $data = array(
					 
					'menu_id'=>$this->input->post('menu_id'),
					'category_id'=>$this->input->post('category_id'),
					/*'store_id'=>$this->input->post('store_id'),*/
				    'item_name'=>$this->input->post('item_name'),
					'item_description'=>$this->input->post('item_description'),
					'item_price'=>$this->input->post('item_price'),
					'discount_amount'=>$this->input->post('discount_amount'),
					'start_date'=>$start_date,
					'end_date'=>$end_date,
					'only_text'=>$only_text,
					'only_image'=>$only_image,
					'text_with_image'=>$text_with_image,
					'item_status'=>$this->input->post('item_status'),
					'item_image1'=>$this->input->post('item_image1'),
					'item_image2'=>$this->input->post('item_image2'),
					'item_image3'=>$this->input->post('item_image3'),
					'item_image4'=>$this->input->post('item_image4'),
					'item_image5'=>$this->input->post('item_image5'),
					'notes'=>$this->input->post('notes'),
					'status'=>$this->input->post('status'),
					);
					echo "<pre>";
					print_r($data);
					$this->Item_model->item_insert($data);
					$item_id=$this->db->insert_id();
					
					
					
					
					
					   
				 /***************************************************Add category_to_items****************************************************************/
				
				$category_id=$this->input->post('category_id');
				    $sql_category_item = 'select * from item_category  WHERE   item_id="'.$item_id.'"  and  category_id="'.$category_id.'"  ';
					$query_category_item = $this->db->query($sql_category_item)->result_array();
					
				    if(empty($query_category_item))
					{
					
					   
					    $this->Item_model->delete_item_category_item($item_id);
						
						$item_name=$this->input->post('item_name');
						$category_id=$this->input->post('category_id');
						$category_name='';
						$sql_category_item = 'select * from category  WHERE   category_id="'.$category_id.'"';
						$query_category_item = $this->db->query($sql_category_item)->result_array();
						if(!empty($query_category_item))
						{
						$category_name = stripslashes($query_category_item['0']['category_level3']);
						}
						
						
						$data = array(
						'item_id'=>$item_id,
						'item_name'=>$item_name,
						'category_id'=>$category_id,
						'category_name'=>$category_name,
						);
						
						
						echo "<pre>";
						print_r($data);
						echo "insert";
						
						$updatecategoryrole = $this->Item_model->category_to_items_insert($data);  
						
					}
				
				
				
			
					  
				 
					
					
					
					
					
					
					
					
					
					/***************************************************Add extras_to_items****************************************************************/
				
				
				    $sql_check_extra_item = 'select * from extras_to_items  WHERE   item_id="'.$item_id.'"';
					$query_check_extra_item = $this->db->query($sql_check_extra_item)->result_array();
					
				    if(!empty($query_check_extra_item))
					{
						//$item_id = stripslashes($query_check_extra_item['0']['item_id']);
						$this->Item_model->delete_extra_item($item_id);
						
						
					}
				
				
				
				
				
				 $extra_id=trim($this->input->post('extra_id'),",");
				 if(!empty($extra_id))
				 {
				  $extra_id_arr=explode(",",$extra_id);
				  if(!empty($extra_id_arr))
				  {
				  foreach($extra_id_arr as $key => $value)
				  {
				    $extra_id=$value;
					      
					$data = array(
					'item_id'=>$item_id,
					'extra_id'=>$extra_id,
					);
				
					
					echo "<pre>";
					print_r($data);
					echo "insert";
					
					$updateuserrole = $this->Item_model->extras_to_items_insert($data);  
					  
				  }
				  }
				  
				 }
				
			/***************************************************End extras_to_items****************************************************************/
				   
				   
				   
				   
				   
				   
				   
				   
			/***************************************************Add options_to_items****************************************************************/
				
				
				    $sql_check_option_item = 'select * from options_to_items  WHERE   item_id="'.$item_id.'"';
					$query_check_option_item = $this->db->query($sql_check_option_item)->result_array();
					
				    if(!empty($query_check_option_item))
					{
						//$item_id = stripslashes($query_check_option_item['0']['item_id']);
						$this->Item_model->delete_option_item($item_id);
						
						
					}
				
				
				
				
				
				 $option_id=trim($this->input->post('option_id'),",");
				 if(!empty($option_id))
				 {
				  $option_id_arr=explode(",",$option_id);
				  if(!empty($option_id_arr))
				  {
				  foreach($option_id_arr as $key => $value)
				  {
				    $option_id=$value;
					      
					$data = array(
					'item_id'=>$item_id,
					'option_id'=>$option_id,
					);
				
					
					echo "<pre>";
					print_r($data);
					echo "insert";
					
					$updateuserrole = $this->Item_model->options_to_items_insert($data);  
					  
				  }
				  }
				  
				 }
				
			/***************************************************End options_to_items****************************************************************/
				
				
				   
				   
				   
				   
				   
				    $menu_id=$this->input->post('menu_id');
					$menu_details=$this->db->query("select * from menu where menu_id='$menu_id'")->result_array();
					$menu_id ='0';
					$menu_name ='';
					if(!empty($menu_details)){
					$menu_id = $menu_details[0]['menu_id'];
					$menu_name = $menu_details[0]['menu_name'];
					}
				   
				   
					
					
					
					
					
					$segment_id=$this->input->post('segment_id');
					
					if($segment_id!='')
					{
					      //  echo "select * from segments where segment_id='$segment_id'";
							
							$segments_details=$this->db->query("select * from segments where segment_id='$segment_id'")->result_array();
							if(!empty($segments_details)){
							$segment_id = $segments_details[0]['segment_id'];
							$segment_name = $segments_details[0]['segment_name'];
							$level1segment = $segments_details[0]['level1segment'];
							$level2segment = $segments_details[0]['level2segment'];
							$level3segment = $segments_details[0]['level3segment'];
							$level4segment = $segments_details[0]['level4segment'];
							}
					
							$data = array(
							'item_id'=>$item_id,
							'item_name'=>$this->input->post('item_name'),
							'segment_id'=>$segment_id,
							'segment_name'=>$segment_name,
							'level1segment'=>$level1segment,
							'level2segment'=>$level2segment,
							'level3segment'=>$level3segment,
							'level4segment'=>$level4segment,
							);
							
							
							echo "<pre>";
							print_r($data);
							echo "insert";
							
							$updateuserrole = $this->Item_model->item_segment_insert($data);
							
							/*****************************************************************************************************/
							
							$level1segment_price=$this->input->post('level1segment_price');
							$level2segment_price=$this->input->post('level2segment_price');
							$level3segment_price=$this->input->post('level3segment_price');
							$level4segment_price=$this->input->post('level4segment_price');
							
							if($level1segment_price!='0' && $level1segment_price!='')
							{
										$data = array(
										'menu_id'=>$this->input->post('menu_id'),
										'menu_name'=>$menu_name,
										'category_id'=>$this->input->post('category_id'),
										'item_id_ref'=>$item_id,
										'item_name'=>$this->input->post('item_name'),
										'item_price'=>$level1segment_price,
										'segment_id'=>$segment_id,
										'segment_label'=>$level1segment,
										'status'=>$this->input->post('status'),
										);
										
										
										echo "<pre>";
										print_r($data);
										echo "insert";
										
										$updateuserrole = $this->Item_model->item_price_insert($data);
										
							}	
							
							if($level2segment_price!='0' && $level2segment_price!='')
							{
										$data = array(
										'menu_id'=>$this->input->post('menu_id'),
										'menu_name'=>$menu_name,
										'category_id'=>$this->input->post('category_id'),
										'item_id_ref'=>$item_id,
										'item_name'=>$this->input->post('item_name'),
										'item_price'=>$level2segment_price,
										'segment_id'=>$segment_id,
										'segment_label'=>$level2segment,
										'status'=>$this->input->post('status'),
										);
										
										
										echo "<pre>";
										print_r($data);
										echo "insert";
										
										$updateuserrole = $this->Item_model->item_price_insert($data);
										
							}
							
							if($level3segment_price!='0' && $level3segment_price!='')
							{
										$data = array(
										'menu_id'=>$this->input->post('menu_id'),
										'menu_name'=>$menu_name,
										'category_id'=>$this->input->post('category_id'),
										'item_id_ref'=>$item_id,
										'item_name'=>$this->input->post('item_name'),
										'item_price'=>$level3segment_price,
										'segment_id'=>$segment_id,
										'segment_label'=>$level3segment,
										'status'=>$this->input->post('status'),
										);
										
										
										echo "<pre>";
										print_r($data);
										echo "insert";
										
										$updateuserrole = $this->Item_model->item_price_insert($data);
										
							}	
							
							if($level4segment_price!='0' && $level4segment_price!='')
							{
										$data = array(
										'menu_id'=>$this->input->post('menu_id'),
										'menu_name'=>$menu_name,
										'category_id'=>$this->input->post('category_id'),
										'item_id_ref'=>$item_id,
										'item_name'=>$this->input->post('item_name'),
										'item_price'=>$level3segment_price,
										'segment_id'=>$segment_id,
										'segment_label'=>$level4segment,
										'status'=>$this->input->post('status'),
										);
										
										
										echo "<pre>";
										print_r($data);
										echo "insert";
										
										$updateuserrole = $this->Item_model->item_price_insert($data);
										
							}			
					
					
					}
					else
					{
					
					$data = array(
					'menu_id'=>$this->input->post('menu_id'),
					'menu_name'=>$menu_name,
					'category_id'=>$this->input->post('category_id'),
					'item_id_ref'=>$item_id,
					'item_name'=>$this->input->post('item_name'),
					'item_price'=>$this->input->post('item_price'),
					'segment_id'=>'No',
					'segment_label'=>'No',
					'status'=>$this->input->post('status'),
					);
				
					
					echo "<pre>";
					print_r($data);
					echo "insert";
					
					$updateuserrole = $this->Item_model->item_price_insert($data);
					
					
					}
				   
				   
				   
				   
				   
				   
				
				   
				 
					
					
					
					
					echo $item_id;
				}
			
		}else {
		
            redirect('login', 'refresh');
       
		}  
	}
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
	public function change_password(){
		
		if ($this->session->userdata('logged_in'))		
		{  
			$session_data = $this->session->userdata('logged_in');
			$slang="english";
		    $this->lang->load($slang, $slang);
		    $data['lang']=$this->lang->language; 
			
			$item_id = $session_data['id'];
			$data['item_id'] = $item_id;
			$data['title'] = "User Role";
			if($item_id!=''){
				$user_details=$this->db->query("select * from user where item_id='$item_id'")->result_array();
				if(!empty($user_details)){}
			}else {}
			$this->load->view('admin/user/user_password', $data);
		} 
		else 
		{
			//If no session, redirect to login page
			redirect('login', 'refresh');
		}
	}
	
	public function update_change_password()			//for displaying the data of the staff into the table
   {
	
	          $item_id=$this->input->post('item_id');
	          $data = array(
				'password' => ($this->input->post('password'))
                 );
				//print_r($staffdata);
				$updateuserrole = $this->Item_model->item_update($data,$item_id);
			    echo $item_id;
				
				
				//redirect('manage-employee');
   }
   
 
	public function deleteitem()
    {
		if ($this->session->userdata('logged_in')) 
		{
			$item_id = $this->input->post('item_id');
		
		
		    /*$this->db->query("delete from extras_to_items where item_id='$item_id'");
			$this->db->query("delete from options_to_items where item_id='$item_id'");
			$this->db->query("delete from item_category where item_id='$item_id'");
			$this->db->query("delete from item_segment where item_id='$item_id'");
			$this->db->query("delete from item_pricing where item_id_ref='$item_id'");
			$this->db->query("delete from items where item_id='$item_id'");*/
			
			
			$this->db->query("delete from extras_to_items where item_id='$item_id'");
			$this->db->query("delete from options_to_items where item_id='$item_id'");
			$this->db->query("delete from item_category where item_id='$item_id'");
			$this->db->query("delete from item_segment where item_id='$item_id'");
			$this->db->query("update item_pricing set deleted ='1',status='0' where item_id_ref ='$item_id'");
			$this->db->query("update items set deleted ='1',status='0' where item_id ='$item_id'");
			
			
			
			$this->session->set_flashdata('added', '<div align="left" style="color:green; font-size:15px; border:1px #398AB9 solid;">
		    Service Deleted Successfully</div>');

			//redirect('users');
		} 
		else 
		{
			//If no session, redirect to login page
			redirect('login', 'refresh');
		}
	}
	
	
	public function get_locations(){
		if ($this->session->userdata('logged_in')) 
		{
			
			$business_id = $this->input->post('business_id');
			$session_data=$this->session->userdata('logged_in');
			$usertype = $session_data['rolecode'];
			$userid = $session_data['id'];
			$data['item_id'] = $session_data['item_id'];
			
			$locations=$this->db->query("select * from nrm_location where business_id='$business_id ' and status='1'")->result();
		    if($locations){
				echo "<select  name='location' id='location'  class='form-control'>
				<option value=''>Select</option>";
					   
				foreach($locations as $result){
					$location_id=$result->location_id;
					$location_name=$result->location_name;
					echo "
						  <option value='".$location_id."'>".$location_name."</option>";
				}
				echo "</select>";
			}else {
				echo 0;
			}
		  
		} 
		else 
		{
			//If no session, redirect to login page
			redirect('login', 'refresh');
		}
	}
	
	public function view($item_id=''){
		
		if ($this->session->userdata('logged_in'))		
		{  
			$session_data = $this->session->userdata('logged_in');
			$slang="english";
		    $this->lang->load($slang, $slang);
		    $data['lang']=$this->lang->language; 
			$data['title'] = "User Role";
			if($item_id!=''){
				$user_details=$this->db->query("select * from items where item_id='$item_id'")->result_array();
				if(!empty($user_details)){
					$data['item_id'] = $user_details[0]['item_id'];
					$data['menu_id'] = $user_details[0]['menu_id'];
					$data['category_id'] = $user_details[0]['category_id'];
					$data['store_id'] = $user_details[0]['store_id'];
					$data['item_name'] = $user_details[0]['item_name'];
					$data['item_description'] = $user_details[0]['item_description'];
					$data['item_price'] = $user_details[0]['item_price'];
					$data['item_status'] = $user_details[0]['item_status'];
					$data['item_image1'] = $user_details[0]['item_image1'];
					$data['item_image2'] = $user_details[0]['item_image2'];
					$data['item_image3'] = $user_details[0]['item_image3'];
					$data['item_image4'] = $user_details[0]['item_image4'];
					$data['item_image5'] = $user_details[0]['item_image5'];
					$data['gluten_free'] = $user_details[0]['gluten_free'];
					$data['need_spice'] = $user_details[0]['need_spice'];
					$data['notes'] = $user_details[0]['notes'];
					$data['discount_amount'] = $user_details[0]['discount_amount'];
					
					
					if($user_details[0]['start_date']!='' && $user_details[0]['start_date']!='0000-00-00')
					{
					   $start_date=date("d-m-Y",strtotime($user_details[0]['start_date']));
					}
					else
					{
					 $start_date="";
					}
					
					if($user_details[0]['end_date']!='' && $user_details[0]['end_date']!='0000-00-00')
					{
					   $end_date=date("d-m-Y",strtotime($user_details[0]['end_date']));
					}
					else
					{
					 $end_date="";
					}
					
					$data['start_date'] = $start_date;
					$data['end_date'] = $end_date;
					$data['only_text'] = $user_details[0]['only_text'];
					$data['only_image'] = $user_details[0]['only_image'];
					$data['text_with_image'] = $user_details[0]['text_with_image'];
					
					
					$data['status'] = $user_details[0]['status'];
					
					
					$extra_id_arr=array();
					$sql = " select * from extras_to_items where item_id='$item_id' ";
					$mainquery = $this->db->query($sql);
					$arrayquery = $mainquery->result();
					if(!empty($arrayquery))
					{
						foreach ($arrayquery as $row) 
						{
						$extra_id_arr[] = $row->extra_id;
						}
					}
					
					//echo "<pre>";
					//print_r($extra_id_arr);
					$data['extra_id_sel'] =$extra_id_arr;
					
					
					$option_id_arr=array();
					$sql = " select * from options_to_items where item_id='$item_id' ";
					$mainquery = $this->db->query($sql);
					$arrayquery = $mainquery->result();
					if(!empty($arrayquery))
					{
						foreach ($arrayquery as $row) 
						{
						$option_id_arr[] = $row->option_id;
						}
					}
					
					//echo "<pre>";
					//print_r($option_id_arr);
					$data['option_id_sel'] =$option_id_arr;
					
					
					
					$seg_details=$this->db->query("select * from item_segment where item_id='$item_id'")->result_array();
				    if(!empty($seg_details)){
					 $data['segment_id_sel'] = $seg_details[0]['segment_id'];
					}
					else
					{
					 $data['segment_id_sel'] = '';
					}
					
					
					
					
					
					
					
				}
			}else {
			        $data['item_id'] = '';
					$data['menu_id'] = '';
					$data['category_id'] = '';
					$data['store_id'] = '';
					$data['item_name'] = '';
					$data['item_description'] = '';
					$data['item_price'] = '';
					$data['item_status'] = '';
					$data['item_image1'] = '';
					$data['item_image2'] = '';
					$data['item_image3'] = '';
					$data['item_image4'] = '';
					$data['item_image5'] = '';
					$data['gluten_free'] = '';
					$data['need_spice'] = '';
					$data['notes'] = '';
					$data['discount_amount'] = '';
					$data['start_date'] = '';
					$data['end_date'] = '';
					$data['only_text'] = '';
					$data['only_image'] = '';
					$data['text_with_image'] = '';
					$data['status'] = '';
					$extra_id_arr=array();
					$data['extra_id_sel'] = $extra_id_arr;
					$option_id_arr=array();
					$data['option_id_sel'] = $option_id_arr;
					$data['segment_id_sel'] = '';
			}
			$this->load->view('admin/item/item_management', $data);
		} 
		else 
		{
			//If no session, redirect to login page
			redirect('login', 'refresh');
		}
	}
	
public function updatestatus()
{				//for email exist validation in edit item form 
	
	
  $id=trim($this->input->get('id'));
  $status=trim($this->input->get('status'));
  //echo "update item_master set status ='$status' where item_id ='$id'";
  $this->db->query("update items set status ='$status' where item_id ='$id'");


		
}
	
	function get_unlink_path()
	{
	
	
	
	
		
		 if ($this->session->userdata('logged_in')) {
            $session_data = $this->session->userdata('logged_in');
			$data['id'] = $session_data['id'];
			$id = $session_data['id'];
            $data['username'] = $session_data['username'];
            $data['rolecode'] = $session_data['rolecode'];
          
			
			
			 $dir = "upload_item_image/";
			 $image_delete_value = $this->input->get('image_delete_value').".png"; 
			 unlink($dir.$image_delete_value);
			 echo "success";
		      
		 }
		
		
		
	}
		
	
}
?>