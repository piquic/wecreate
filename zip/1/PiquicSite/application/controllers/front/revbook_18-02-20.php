<?php 
class Revbook extends CI_Controller{
	
	function __construct(){
		parent::__construct();
		$this->load->library(['session']); 
        $this->load->helper(['url','file','form']);
        $this->load->model('Revbook_model');
        $this->load->model('plans_model');
	}

	public function index($tmb_book_id){
		//echo $book_id;
		if ($this->session->userdata('front_logged_in')) {
			$session_data = $this->session->userdata('front_logged_in');
			$data['user_id'] = $session_data['user_id'];
			$data['user_name'] = $session_data['user_name'];
			$user_id = $session_data['user_id'];
			// $data['type1']=$session_data['type1'];

			$userquery = "SELECT * FROM `tbl_users` where  user_id='$user_id' ";
			$userres=$this->db->query($userquery);
			$userrow=$userres->result_array();
			$data['user_name'] = $userrow[0]['user_name'];
			$data['user_company'] = $userrow[0]['user_company'];
			$data['user_address'] = $userrow[0]['user_address'];
			$data['user_mobile'] = $userrow[0]['user_mobile'];
			
			$data['book_id']=$tmb_book_id;
			$data['booking_data']= $this->db->query("select * from tbl_tmp_booking where book_id=$tmb_book_id and user_id=$user_id;")->result_array();
			//print_r($data['booking_data']);//exit();
			if(!empty($data['booking_data']))
			{
				$this->load->view('front/revbook',$data );
			}
			else{
				redirect('booking');	
			}
			
			
		}
		else
		{
			redirect('/');
		}
		
	}
	public function add_book(){
		if ($this->session->userdata('front_logged_in')) {
			$session_data = $this->session->userdata('front_logged_in');
			$data['user_id'] = $session_data['user_id'];
			$data['user_name'] = $session_data['user_name'];
			$user_id = $session_data['user_id'];
			// $data['type1']=$session_data['type1'];
			 //echo"<pre>";
		$created_date = date("Y-m-d H:i:s");
		$order_id=$this->input->post('order_id');
		$user_id=$this->input->post('user_id');
		$gender=$this->input->post('gender');
		$m_status=$this->input->post('m_status');
		$category=$this->input->post('category');	
		$quty=$this->input->post('quty');
		$angle=$this->input->post('angle');
		$category1=array_filter(explode(",", $this->input->post('category')));	
		$quty1=array_filter(explode(",", $this->input->post('quty')));
		$angle1=array_filter(explode(",", $this->input->post('angle')));
		$d_status=$this->input->post('d_status');
		$notes=$this->input->post('notes');
		$total_price=$this->input->post('total_price');
		$total_image=$this->input->post('total_image');
		$currency=$this->input->post('currency');
		$plan_id=$this->input->post('plan_id'); 
		$plan_left_amt=$this->input->post('plan_left_amt');
		$plan_left_credit=$this->input->post('plan_left_credit');
		$plan_used_amt=$this->input->post('plan_used_amt');
		$plan_used_credit=$this->input->post('plan_used_credit');
		$p_status=$this->input->post('p_status');
		
		$data = array(

			'user_id'=>$user_id,
			'order_id'=>$order_id,
			'gender'=>$gender,
			'category'=>$category,
			'quty'=>$quty,
			'm_status'=>$m_status,
			'angle'=>$angle,
			'd_status'=>$d_status,
			'notes'=>$notes,
			'b_status'=>"1",
			'total_price'=>$total_price,
			'total_image'=>$total_image,
			'plan_type'=>$plan_id,
			'p_status'=>$p_status,
			'create_date'=>$created_date,
			'currency'=>$currency,
			'plan_used_amt'=>$plan_used_amt,
			'plan_used_credit'=>$plan_used_credit,
			'plan_left_amt'=>$plan_left_amt,
			'plan_left_credit'=>$plan_left_credit,
		);
	   // print_r($data);exit();
		//$this->load->view('front/revbook',$data );
		$query1 = $this->db->get_where('tbl_plan', array('planid'=>$plan_id,'plan_for'=>'booking'));
		//$query = $this->db->get("tbl_upload_img");
		$result1=$query1->result_array(); 
		//print_r($result1);

		$plan_amount=$result1[0]['plan_amount'];
		$plan_credit=$result1[0]['plan_credit'];
		$plntyid=$result1[0]['plntyid'];
		$plan_name=$result1[0]['plan_name'];
		$result=$this->Revbook_model->add_booking($data);
		$booking_id=$this->db->insert_id();

		if($plntyid==2)
		{
		    $plan_start_date      = date("Y/m/d");
			$plan_end_date   = date('Y/m/d', strtotime('+1 month'));
			//$plan_left_amt= $result[0]['total_price']-$plan_used_amt;
	        $account_cal_details['user_id']=$session_data['user_id'];
			$account_cal_details['form_id']=$booking_id;
			$account_cal_details['plan_for']="1";
			//$account_cal_details['billing_unique_id']=$billing_unique_id;
			$account_cal_details['plan_id']=$plan_id;
			$account_cal_details['plan_type_id']=$plntyid;

			$account_cal_details['plan_start_date']=$plan_start_date;
			$account_cal_details['plan_end_date']=$plan_end_date;

			$account_cal_details['plan_amt']=$plan_amount;
			$account_cal_details['plan_credit']=$plan_credit;
			$account_cal_details['plan_used_amt']=$plan_used_amt;
			$account_cal_details['plan_used_credit']=$plan_used_credit;
			$account_cal_details['plan_left_amt']=$plan_left_amt;
			$account_cal_details['plan_left_credit']=$plan_left_credit;
			$account_cal_details['currency']=$currency;
			

			$status_query1 = $this->db->get_where('tbl_account_cal', array('user_id'=>$user_id,'plan_status'=>'Active','payment_status'=>'Paid'));
			//$query = $this->db->get("tbl_upload_img");
			$status_result1=$status_query1->result_array(); 
			//print_r($status_result1);
			if(!empty($status_result1))
			{
				$account_cal_id=$status_result1[0]['account_cal_id'];
				$account_cal_details['payment_status']="Paid";
				$account_cal_details['plan_status']="Active";
				$result1=$this->Revbook_model->update_account_cal($account_cal_details,$account_cal_id);
				//$account_cal_details['plan_status']="Pending";
			}
			else{
				$account_cal_details['plan_status']="InActive";
				$account_cal_details['payment_status']="Not Paid";
				$result1=$this->Revbook_model->add_account_cal($account_cal_details);
				
			}
		}
		//echo $account_cal_details['plan_status'];
		// exit();
		//print_r($account_cal_details); 
		//exit();

		//print_r($result);exit();
		$tmb_book_id=$this->input->post('tmb_book_id');
		$this->db->where('book_id', $tmb_book_id);
		$this->db->delete('tbl_tmp_booking');

		$query = $this->db->get_where('tbl_booking', array('book_id'=>$booking_id));
		//$query = $this->db->get("tbl_upload_img");
		$result=$query->result_array(); 
      	$p_status1=$result[0]['p_status'];

       	if($p_status1=="paid")
       	{	

       		$plansTypeDetails = $this->plans_model->get_plansById($plan_id);
			//print_r($plansTypeDetails);
			$plntypname=$plansTypeDetails[0]['plntypname'];


       		$query=$this->db->query("select * from  tbl_users  WHERE user_id='$user_id' ");
			$userDetails= $query->result_array();
			$user_name=$userDetails[0]['user_name'];
			$user_email1=$userDetails[0]['user_email'];
			$user_email_value=explode("@",$user_email1);
			$user_email=substr_replace($user_email_value[0], "*****", 2)."@".$user_email_value[1];
			$user_mobile=$userDetails[0]['user_mobile'];
			$user_company=$userDetails[0]['user_company'];
			$user_address=$userDetails[0]['user_address'];

			$book_date_time=date("D, M d ,Y H:i A",strtotime($created_date));
			// $query1=$this->db->query("select * FROM tbl_plan INNER JOIN tbl_plantype on tbl_plan.plntyid=tbl_plantype.plntyid WHERE tbl_plan.planid='$plan_type'");
			// // print_r("select * FROM tbl_plan INNER JOIN tbl_plantype on tbl_plan.plntyid=tbl_plantype.plntyid WHERE tbl_plan.planid='$plan_type'");
			// $bookDetails1= $query1->result_array();
			// //print_r($bookDetails1);
		    
			// $plan_name=$bookDetails1[0]['plan_name'];
			// // $plan_credit=$bookDetails1[0]['plan_credit'];
			// // $plan_amount=$bookDetails1[0]['plan_amount'];


   			if($currency=="USD") { $currency1="&#36;"; }
	        elseif($currency=="INR") { $currency1="&#x20B9;"; }
	        elseif($currency=="EUR") { $currency1="&#128;"; }
	        elseif($currency=="GBP") { $currency1="&#163;"; }
	        elseif($currency=="AUD") { $currency1="&#36;"; }
	        elseif($currency=="CNY") { $currency1="&#165;"; }


			if($gender=="1"){ $gender1= "Male" ; } 
            if($gender=="2"){ $gender1="Female" ; } 
            if($gender=="4"){ $gender1="Boy" ; } 
            if($gender=="6"){ $gender1="Girl" ; }
            if($m_status=="1"){
	           $m_status1= "Let \"Piquic Style Expert\" select my models & style." ; 
	         } 
	         else{ 
	          $m_status1= "I want to select my models and styling once my apparels gets digitized."; 
	         } 
	         if($d_status=="1"){
               $d_status1= "Standard (6-7 days after receiving your products)." ; 
             } 
             else{ 
               $d_status1= "Express (2-3 days after receiving your products)."; 
             }     
            //$user_email="demo.intexom@gmail.com";
            
            $mail_text='Payment Email Piquic - Pay Using Balance';
            $subject='Piquic - Pay Using Plan Balance';
            
            $payment_type_query1=$this->db->query("select * FROM tbl_paymenttype where paytyid='$plntyid'");

			$payment_typeDetails1= $payment_type_query1->result_array();
			$payment_type_name=$payment_typeDetails1[0]['paytypname'];
		    $table_book_body="";
            if($plntypname=="Pay as you go" || $plntypname=="Pay Monthly"){
            	$table_book_body.="<h4>Booking Details</h4>";
            	$table_book_body.='
            		<table class="table">
                             <thead>';
	            $table_book_body.= '<tr>
	                  <th class="border-0 text-uppercase small font-weight-bold">ID</th>
	                  <th class="border-0 text-uppercase small font-weight-bold">Gender</th>
	                  <th class="border-0 text-uppercase small font-weight-bold">Category</th>
	                  <th class="border-0 text-uppercase small font-weight-bold">Quantity</th>
	                  <th class="border-0 text-uppercase small font-weight-bold">Angle</th>

	                </tr>';
            $table_book_body.= '</thead> <tbody>';
         
	            $table_book_body.= '<tr>
	            	<td>1</td>
	            	<td>'.$gender1. ' </td>
					<td>
	                    <table>';

                foreach ($category1 as $key => $value ) {
	                $table_book_body.= '<tr>
	                    <td style="border-top: none; border-bottom: 1px solid #dee2e6;">
	                     '.$value.'
	                   </td>
	                 </tr>';
                                
                }
                $table_book_body.= '</table>
                           </td>
                           <td><table>';

                foreach ($quty1 as $key1 => $value1 ) {
                    $table_book_body.= '<tr>
	                    <td style="border-top: none; border-bottom: 1px solid #dee2e6;">
	                     '.$value1.'
	                   	</td>
	                 	</tr>';
                 
           		} 

               $table_book_body.= '</table></td><td><table>';
              	//echo "<pre>";
              	//print_r($angle);
                unset($arr_filter);

               	foreach ($angle1 as $key2 => $value2 ) {
          			//echo $value2;
	                $angle_value=explode("_",$value2);
	                $cat=$angle_value[0];
	                $arr_filter[$cat][]=ucfirst($angle_value[1]);
              	}
              	//echo "<pre>";
              	//print_r($arr_filter);

              	foreach ($category1 as $key => $value ) {
	                $table_book_body.= '<tr>
	                  <td style="border-top: none; border-bottom: 1px solid #dee2e6;">
	                   '.implode(", ",$arr_filter[$value]) .'
	                </td>
	               	</tr>';
            
             	} 
 				$table_book_body.= '</table></td></tr>';
                
                $table_book_body.= '</tbody></table>';

            }    
               $table_plan_body="";
                if($plntypname=="Pay Monthly"){
                	$table_plan_body.="<h4>Plan Details</h4>";
                	$table_plan_body.='<table class="table">
                            <thead>';

	             	$table_plan_body.= '                   
		                <tr>
		                  <th class="border-0 text-uppercase small font-weight-bold">ID</th>
		                  <th class="border-0 text-uppercase small font-weight-bold">Plan Name</th>
		                  <th class="border-0 text-uppercase small font-weight-bold">Duatation</th>
		                  <th class="border-0 text-uppercase small font-weight-bold">Total Image</th>
		                  <th class="border-0 text-uppercase small font-weight-bold">Total Amount</th>
		                  <th class="border-0 text-uppercase small font-weight-bold">Left Image</th>
		                  <th class="border-0 text-uppercase small font-weight-bold">Left Amount</th>
		                </tr>';
				
                         
            	$table_plan_body.= '</thead><tbody>';
   			
                   $table_plan_body.= '
                    <tr>
                      <td>1</td>
                      <td>'.$plan_name.'</td>
                      <td>1 Month</td>
                      <td>'.$plan_credit.'</td>
                      <td>'.$plan_amount.'</td>
                      <td>'.$plan_left_credit.'</td>
                      <td>'.$plan_left_amt.'</td>
                    </tr>';
                     $table_plan_body.= '</tbody></table>';
                }
               
           		// print_r($table_body); //exit();
	            if($plntypname=="Pay as you go")
	            {
	            	$amount_content="Total Amount";
					$plan_content="Plan Type";
					$image_content="Total Image";
					$total_price=$total_price;
					$plntypname=$plntypname;
					$total_image=$total_image;
	            }
	            if($plntypname=='Pay Monthly')
	            {
	            	$amount_content="Used Amount";
					$plan_content="Plan Type";
					$image_content="Used Image";
					$total_price=$plan_used_amt;
					$plntypname=$plntypname;
					$total_image=$plan_used_credit;
	            }

/*********************************Send mail*************************************/

				$query=$this->db->query("select * from  tbl_settings  WHERE setting_key='MAIL_SMTP_USER' ");
				$userDetails= $query->result_array();
				$MAIL_SMTP_USER=$userDetails[0]['setting_value'];

				$query=$this->db->query("select * from  tbl_settings  WHERE setting_key='MAIL_SMTP_PASSWORD' ");
				$userDetails= $query->result_array();
				$MAIL_SMTP_PASSWORD=$userDetails[0]['setting_value'];

				$query=$this->db->query("select * from  tbl_settings  WHERE setting_key='MAIL_BODY_BOOKING' ");
				$userDetails= $query->result_array();
				$MAIL_BODY_BOOKING=$userDetails[0]['setting_value'];

				//Load email library
				$this->load->library('email');

				$config = array();
				//$config['protocol'] = 'smtp';
				$config['protocol']     = 'mail';
				$config['smtp_host'] = 'localhost';
				$config['smtp_user'] = $MAIL_SMTP_USER;
				$config['smtp_pass'] = $MAIL_SMTP_PASSWORD;
				//$config['smtp_port'] = 25;

				$this->email->initialize($config);
				$this->email->set_newline("\r\n");

		
				$from_email = "demo.intexom@gmail.com";
				$to_email = $user_email;
				$copy_right= date("Y");
				//$billing_date_time=date("d/m/y",strtotime($billing_date_time));

				//echo $message=$MAIL_BODY_BOOKING;
				$find_arr = array('#ORDER_ID##','##BOOK_DATE##',"##USER_NAME##","##USER_EMAIL##","##USER_MOBILE##","##USER_COMPANY##","##USER_ADDRESS##","##M_STATUS##","##D_STATUS##","##NOTES##","##P_STATUS##",'##CUR##',"##TABLE_BOOK_BODY##","##TABLE_PLAN_BODY##","##AMOUNT_CONTENT##", "##TOTAL_AMOUNT##","##PLAN_CONTENT##",'##PLAN_TYPE##',"##IMAGE_CONTENT##",'##TOTAL_IMAGE##','##COPY_RIGHT##','##PAYMENT_TYPE##');
				$replace_arr = array($order_id,$book_date_time,$user_name,$user_email,$user_mobile,$user_company,$user_address, $m_status1,$d_status1,$notes,$p_status,$currency1,$table_book_body,$table_plan_body,$amount_content,$total_price,$plan_content,$plntypname,$image_content,$total_image,$copy_right,$payment_type_name);
				$message=str_replace($find_arr,$replace_arr,$MAIL_BODY_BOOKING);

			//	echo $subject."----".$message;exit;

				$this->email->from($from_email, 'Piquic');
				$this->email->to($to_email);
				$this->email->cc('poulami@mokshaproductions.in');
				$this->email->cc('raja.priya@mokshaproductions.in');
		       // $this->email->cc('raja.priya@mokshaproductions.in');
				$this->email->subject($subject);
				$this->email->message($message);
				//Send mail
				if($this->email->send())
				{
					//echo "mail sent";exit;
					$this->session->set_flashdata("email_sent","Congragulation Email Send Successfully.");
				}
				else
				{
					//echo $this->email->print_debugger();
					//echo "mail not sent";exit;
					$this->session->set_flashdata("email_sent","You have encountered an error");
				}
				//redirect('booksuccess');
				echo "Paid";
       		}
			else{
				echo $booking_id;
			}
				
		}
		else
		{
			redirect('/');
		}
		

	}

	public function delete_book(){
		// echo"oooooo";exit();
		$tmb_book_id=$this->input->post('tmb_book_id');
		$this->db->where('book_id', $tmb_book_id);
		$this->db->delete('tbl_tmp_booking');
	}


	public function booksuccess(){
		


		if ($this->session->userdata('front_logged_in')) {
            $session_data = $this->session->userdata('front_logged_in');
            $data['user_id'] = $session_data['user_id'];
            $data['user_name'] = $session_data['user_name'];
            $user_id = $session_data['user_id'];


            $this->load->view('front/booksuccess',$data);
		   
			
         
        } else {
        	
            $session_data = $this->session->userdata('front_logged_in');
            $data['user_id'] = $session_data['user_id'];
            $data['user_name'] = $session_data['user_name'];
            
			redirect('home');
        }
	}
	public function bookfailure(){
		


		if ($this->session->userdata('front_logged_in')) {
            $session_data = $this->session->userdata('front_logged_in');
            $data['user_id'] = $session_data['user_id'];
            $data['user_name'] = $session_data['user_name'];
            $user_id = $session_data['user_id'];


            $this->load->view('front/bookfailure',$data);
		   
			
         
        } else {
        	
            $session_data = $this->session->userdata('front_logged_in');
            $data['user_id'] = $session_data['user_id'];
            $data['user_name'] = $session_data['user_name'];
            
			redirect('home');
        }
	}
}

?>