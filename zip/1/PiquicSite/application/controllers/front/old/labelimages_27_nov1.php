<?php 
class Labelimages extends CI_Controller{
	
	function __construct(){
	parent::__construct();
	$this->load->library(['session']); 
	$this->load->database();
	$this->load->helper(['url','file','form']);
	$this->load->helper('directory');
	$this->load->helper('file'); 
	$this->load->model('Labelimg_model');

	}

	public function index($sku_key=""){


    if ($this->session->userdata('front_logged_in')) {
    $session_data = $this->session->userdata('front_logged_in');
    $data['user_id'] = $session_data['user_id'];
    $data['user_name'] = $session_data['user_name'];
    }
    else
    {
    $data['user_id'] = '';
    $data['user_name'] = '';
    }
    $data['sku_key'] = $sku_key;

//echo $sku;

		/*$data['title'] = "label images";
		$this->load->view('inc/header', $data);
		$this->load->view('inc/menu');		
		$this->load->view('inc/nav');*/
		$data['labelimagesinfo']=$this->Labelimg_model->labelimages($sku_key);
	    $this->load->view('front/labelimages',$data);
		//$this->load->view('inc/footer');
	}

function update(){
      $data=$this->Labelimg_model->update_skuinfo();
        echo json_encode($data);
    }

    function genupdate(){
      $data=$this->Labelimg_model->update_genskuinfo();
        echo json_encode($data);
    }

function delete(){
	     $skuno=$this->input->post('skuno');
      $data=$this->Labelimg_model->delete_sku($skuno);
        echo json_encode($data);
    }

function mulcatupdate(){	    
     $skuno=$this->input->post('skuno');
     $category=$this->input->post('category');   
   
       $data=$this->Labelimg_model->updatemult_catskuinfo($skuno,$category);
       echo json_encode($data);
    }

    function mulgenupdate(){
    $skuno=$this->input->post('skuno');
    $gender=$this->input->post('gender');	      
    $data=$this->Labelimg_model->updatemult_genskuinfo($skuno,$gender);
    echo json_encode($data);
    }


  function oneupload(){
    $skuno=$this->input->post('sku_data');
    $user_id=$this->input->post('user_id');
    $file_name=$_FILES["upld_img_SKU"]["name"];
    $file_tmp=$_FILES["upld_img_SKU"]["tmp_name"];
    $ext=pathinfo($file_name,PATHINFO_EXTENSION);
    $Fpsdimg=$file_name;
    $targetDirpsd= './upload/'.$user_id.'/'.$skuno.'/';  
        //echo $targetDirpsd.$Fpsdimg;      
   move_uploaded_file($file_tmp=$_FILES["upld_img_SKU"]["tmp_name"],$targetDirpsd.$Fpsdimg);
   $data = $this->Labelimg_model->insert_file($file_name,$skuno);
   // echo json_encode($data);
   }

function imgangchng(){	    
     $id=$this->input->post('id');
      $imgid=$this->input->post('imgid');
      $selval=$this->input->post('selval'); 
      $img=$this->input->post('img'); 


   
       $data=$this->Labelimg_model->updateimg_anginfo($id,$selval,$img,$imgid);
        // echo json_encode($data);
    }


function sendforstyle(){      
     $sku=$this->input->post('sku');
     $cat=$this->input->post('cat');
     $gen=$this->input->post('gen');
     $data=$this->Labelimg_model->updatestyle_queinfo($sku,$cat,$gen);
      echo json_encode($data);
    }


  public function upload_files_fromsku(){

     if ($this->session->userdata('front_logged_in')) {
    $session_data = $this->session->userdata('front_logged_in');
    $data['user_id'] = $session_data['user_id'];
    $data['user_name'] = $session_data['user_name'];

    $user_id=$session_data['user_id'];;
    }
    else
    {
    $data['user_id'] = '';
    $data['user_name'] = '';
    $user_id='';
    }

    //echo "<pre>";
    //print_r($_FILES);
    
   
    if (!is_dir("./temp_upload/".$user_id)) {
    mkdir("./temp_upload/".$user_id, 0777, true);
    }
    $dir = "./temp_upload/".$user_id."/";
    
    $file=time().$_FILES["image"]["name"];
    move_uploaded_file($_FILES["image"]["tmp_name"], $dir. $file);

    echo ltrim($file);
  }




    



}
?>