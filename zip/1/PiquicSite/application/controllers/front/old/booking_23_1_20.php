<?php 
class Booking extends CI_Controller{
	
	function __construct(){
		parent::__construct();
		$this->load->library(['session']); 
    	$this->load->helper(['url','file','form']);
    	$this->load->model('Booking_model');
   
	}

	public function index($tmb_book_id=""){
		//print_r("Temp booking".$tmb_book_id);
		if ($this->session->userdata('front_logged_in')) {
			$session_data = $this->session->userdata('front_logged_in');
			$data['user_id'] = $session_data['user_id'];
			$data['user_name'] = $session_data['user_name'];
			if(!empty($tmb_book_id))
			{
				$data['book_id']=$tmb_book_id;
				$data['booking_data']= $this->db->query("select * from tbl_tmp_booking where book_id=$tmb_book_id;")->result_array();
				$this->load->view('front/booking',$data );
			}
			else
			{
				$this->load->view('front/booking',$data );
			}
			
			

		}
		else
		{
			redirect('/');
		}

		
	}

	public function add_book($tmb_book_id=""){
		// echo"oooooo";exit();
			$created_date = date("Y-m-d H:i:s");
			
			$order_id=$this->input->post('order_id');
			$user_id=$this->input->post('user_id');
			$gender=$this->input->post('gender');
			$m_status=$this->input->post('m_status');
			$category=",".implode(",",$this->input->post('cat')).",";	
			$quty=",".implode(",",$this->input->post('quty')).",";
			$angle = '';
			if($this->input->post('angle_Top')!="")
			{
				$angle .=",".implode(",",$this->input->post('angle_Top')).",";
			}

			if($this->input->post('angle_Bottom')!="")
			{
				$angle .=implode(",",$this->input->post('angle_Bottom')).",";		
			}

			if($this->input->post('angle_Dress')!="")
			{
				$angle .=implode(",",$this->input->post('angle_Dress')).",";			
			}
			$d_status=$this->input->post('d_status');
			$notes=$this->input->post('notes');
			$total_price=$this->input->post('total_price');
			$total_image=$this->input->post('total_image');
			 $currency=$this->input->post('currency');
			$planid=$this->input->post('planid');
			$p_status="Not paid";
			$data = array(
				'user_id'=>$user_id,
				'order_id'=>$order_id,
				'gender'=>$gender,
				'category'=>$category,
				'quty'=>$quty,
				'm_status'=>$m_status,
				'angle'=>$angle,
				'd_status'=>$d_status,
				'notes'=>$notes,
				'b_status'=>"1",
				'total_price'=>$total_price,
				'total_image'=>$total_image,
				'plan_type'=>$planid,
				'p_status'=>$p_status,
				'create_date'=>$created_date,
				'currency'=>$currency,
			);
		if(!empty($tmb_book_id))
		{
			$result=$this->Booking_model->update_booking($data,$tmb_book_id);		
			echo $tmb_book_id;
		}
		else
		{
			$result=$this->Booking_model->add_booking($data);
			$tmb_book_id=$this->db->insert_id();					
			echo $tmb_book_id;
		}
	}
	public function price_calculation()
	{
		$total_price=0;
		$total_image=$this->input->post('total_image');
		$user_id=$this->input->post('user_id');
		$payment_type=$this->input->post('payment_type_val');
		$query = $this->db->get_where('tbl_users', array('user_id'=>$user_id));
		//$query = $this->db->get("tbl_upload_img");
		$result=$query->result_array(); 
		$curr=$result[0]['user_currency'];
		if(!empty($curr))
		{
			$this->db->select('*');
			$this->db->from('tbl_plan');
			$this->db->where('plan_for', 'booking');
			$this->db->where('plan_currency',$curr);
			$this->db->where('status','0');
			$this->db->where('deleted','0');
			if($payment_type==1)
			{
				$this->db->where('plntyid',$payment_type);
				if($total_image>=1&&$total_image<=99)
				{
					$this->db->where('plan_credit >', 1);
					$this->db->where('plan_credit <=', 99);
				}
				else if($total_image==100&&$total_image>=99)
				{
					$this->db->where('plan_credit ==', 100);
					$this->db->where('plan_credit >=', 100);
				}
				else if($total_image>=101&&$total_image<=500)
				{
					$this->db->where('plan_credit >', 100);
					$this->db->where('plan_credit <=', 500);
				}
				else if($total_image>=501&&$total_image<=1000)
				{
					$this->db->where('plan_credit >', 500);
					$this->db->where('plan_credit <=', 1000);
				}
				else if($total_image>=1001&&$total_image<=5000)
				{
					$this->db->where('plan_credit >', 1000);
					$this->db->where('plan_credit <=', 5000);
				}
				else if($total_image>5000)
				{
					$this->db->where('plan_credit >', 5000);
				}
			}
			
			if($payment_type==2){
				//print_r($payment_type);
				$payment_type=explode("_",$payment_type);
				$this->db->where('plntyid',$payment_type[0]);
				$this->db->where('plan_name',$payment_type[1]);
			}
			

			
			$query = $this->db->get();
			$result=$query->result_array();
			//echo $this->db->last_query(); exit();
			//print_r($result);
		if(!empty($result))
			{
				$data['planid']=$result[0]['planid'];
				if($payment_type==1)
				{
					$data['price']=$result[0]['plan_per_image'];
				}
				else{
					$data['price']=$result[0]['plan_amount'];
				}
			}
			else{
				$data['planid']="";
				$data['price']="";
			}			echo json_encode($data);
			//print_r($data);
		}
	}
	
}

?>