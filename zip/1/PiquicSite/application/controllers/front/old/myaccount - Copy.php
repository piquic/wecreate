<?php 
class Myaccount extends CI_Controller{
	
	function __construct(){
		parent::__construct();
		$this->load->library(['session']); 
        $this->load->helper(['url','file','form']);
        $this->load->model('UpdateUser_model');
	}

	public function index(){


		if ($this->session->userdata('front_logged_in')) {
            $session_data = $this->session->userdata('front_logged_in');
            $data['user_id'] = $session_data['user_id'];
            $data['user_name'] = $session_data['user_name'];
            $user_id = $session_data['user_id'];
            
            $param = $this->uri->segment('2');
            $data['param'] = $param;


            $stylequery = "SELECT * FROM `tbl_users` where  user_id='$user_id' ";
			$styleres=$this->db->query($stylequery);
			$stylerow=$styleres->result_array();
			$user_currency = $stylerow[0]['user_currency'];

			if($user_currency!='')
			{
				$currency=$user_currency;
			}
			else
			{
			    $currency="USD";
			}
            
           // $currency;

            $data['currency_default'] = $currency;

            $data['user_details'] = $this->UpdateUser_model->get_usr_data($data['user_id']);

			$this->load->view('front/myaccount', $data);
			
         
        } else {
        	redirect('home');
        }
		
	}

	public function usrUpdate()
	{
		$user_id=$this->input->post('txtUserId');

		$data = array(
			'user_name'=>$this->input->post('txtUser'),
			'user_company'=>$this->input->post('txtCompany'),
			'user_email'=>$this->input->post('txtEmail'),
			'user_mobile'=>$this->input->post('txtPhone'),
			'user_address'=>$this->input->post('txtAddress'),
			'user_currency'=>$this->input->post('currency'),
		);

		//print_r($data);

		if($this->UpdateUser_model->update_user($data, $user_id)){
			redirect('myaccount/success');
		} else {
			redirect('myaccount/error');
		}
	}

	public function pswdUpdate()
	{
		$user_id = $this->input->post('txtUserIdPswd');

		$data['user_details'] = $this->UpdateUser_model->get_usr_data($user_id);

		// print_r($data['user_details']);
		
		$usrpswd = $data['user_details'][0]['user_password'];
		
		$oldPswd = sha1($this->input->post('txtOldPswd'));
		$newPswd = sha1($this->input->post('txtPswd'));

		if($oldPswd == $usrpswd){
			$data = array(
				'user_password' => $newPswd,
			);

			$this->UpdateUser_model->update_pswd($data, $user_id);
			redirect('myaccount/success');
		} else {
			redirect('myaccount/error');
		}

	}

}

?>