<?php 
class Booking extends CI_Controller{
	
	function __construct(){
		parent::__construct();
		$this->load->library(['session']); 
    $this->load->helper(['url','file','form']);
   
	}

	public function index(){
		
		if ($this->session->userdata('front_logged_in')) {
			$session_data = $this->session->userdata('front_logged_in');
			$data['user_id'] = $session_data['user_id'];
			$data['user_name'] = $session_data['user_name'];
			// $data['type1']=$session_data['type1'];
			$this->load->view('front/booking',$data );

		}
		else
		{
			redirect('/');
		}

		
	}
	
}

?>