<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
class Download extends CI_Controller
{
    public function __construct()
    {
		
        parent::__construct();
        $this->load->library('pagination');
       // $this->load->helper('url','file');
        $this->load->model('Download_model');
		//$this->load->helper("file");
		 $this->load->library('session');
		
    }

    public function index()
    {
        if ($this->session->userdata('front_logged_in')) 
        {
            $session_data = $this->session->userdata('front_logged_in');
            $data['user_id'] = $session_data['user_id'];
            $data['user_name'] = $session_data['user_name'];
            // Row per page
            $rowperpage = 2;
            $rowno=0;
        
            if(!empty($this->input->post('lastID')))
            {
                $lastid = $this->input->post('lastID');
            }
            else
            {
                $lastid='';  
            }
            if(!empty($this->input->post('ordno')))
            {
                $ordno = $this->input->post('ordno');
            }
            else
            {
                $ordno='';  
            }
            if(!empty($this->input->post('modelgen')))
            {
                $modelgen = $this->input->post('modelgen');
            }
            else
            {
                $modelgen='';  
            }
            if(!empty($this->input->post('modelcat')))
            {
                $modelcat = $this->input->post('modelcat');
            }
            else
            {
                $modelcat='';  
            }
            if(!empty($this->input->post('status')))
            {
                $status = $this->input->post('status');
            }
            else
            {
                $status='';  
            }
           //print_r($status);
      //exit();

            
            $allcount = $this->Download_model->getimagerecordCount($data['user_id'],$rowperpage,$lastid,$ordno,$modelgen,$modelcat,$status);
            $data['showLimit']=$rowperpage;
            $data['allNumRows']=$allcount;
            $data['download_image'] = $this->Download_model->get_image($data['user_id'],$rowperpage,$lastid,$ordno,$modelgen,$modelcat,$status);
           // print_r($data['download_image']);
            // exit();
            //  redirect('administrator/dashboard', 'refresh');
            $this->load->view('front/download', $data);  
        } 
        else 
        {
            $data['user_id'] = '';
            $data['user_name'] = '';
            redirect('/', $data);   
        }
    }
    public function showdownloader()
    {         
        if ($this->session->userdata('front_logged_in')) 
        {
            $session_data = $this->session->userdata('front_logged_in');
            $data['user_id'] = $session_data['user_id'];
            $data['user_name'] = $session_data['user_name'];
            $rowperpage = 2;
            //$rowno=0;
            if(!empty($this->input->post('lastID')))
            {
                $lastid = $this->input->post('lastID');
            }
            else
            {
                $lastid='';  
            }
            if(!empty($this->input->post('ordno')))
            {
                $ordno = $this->input->post('ordno');
            }
            else
            {
                $ordno='';  
            }
            if(!empty($this->input->post('modelgen')))
            {
                $modelgen = $this->input->post('modelgen');
            }
            else
            {
                $modelgen='';  
            }
            if(!empty($this->input->post('modelcat')))
            {
                $modelcat = $this->input->post('modelcat');
            }
            else
            {
                $modelcat='';  
            }
            if(!empty($this->input->post('status')))
            {
                $status = $this->input->post('status');
            }
            else
            {
                $status='';  
            }
      
            // All records count
            $allcount = $this->Download_model->getimagerecordCount($data['user_id'],$rowperpage,$lastid,$ordno,$modelgen,$modelcat,$status);
            $data['showLimit']=$rowperpage;
            $data['allNumRows']=$allcount;
            $data['download_image'] = $this->Download_model->get_image($data['user_id'],$rowperpage,$lastid,$ordno,$modelgen,$modelcat,$status);
            $this->load->view('front/download_loader', $data);  
        } 
        else 
        {
            $data['user_id'] = '';
            $data['user_name'] = '';
            redirect('/', $data);  
        }
    }
    public function download_sku()
    {

        $this->load->helper('download');
        $downloadimgid = $this->input->post('images');
        // print_r($downloadimgid);
        //$downloadimgid_array = explode(",", $downloadimgid);
        // $downloadimgid_array = explode(",", "1");
        $sku_no_array = array();
        $sku_thumb = array();
        foreach ($downloadimgid as $downloadimgid_key => $downloadimgid_value) {
            $down_id = $downloadimgid_value;
            //print_r($down_id);
            $sku_data= $this->db->query("select * from tbl_download_img where zipfile_name='$down_id';")->result_array();
            // echo "<pre>";
            $sku_no_array[] = $sku_data[0]['zipfile_name'];
            $sku_thumb[]=$sku_data[0]['sku_img_path']."crop/";
            //$urls[]='http://203.122.46.51:82/'.$row['sku_no'].'/';
        }
        // echo "<pre>";
        // print_r($sku_no_array);exit();
        // print_r($sku_thumb);exit();

        $this->db->query("UPDATE `tbl_download_img` SET `downloaded`='1' where zipfile_name='$down_id';");
        
        $micro = microtime();
        foreach ($sku_no_array as $sku_no_array_key => $sku_no_array_value) {
          $query33 =$this->db->query("SELECT * FROM `tbl_download_img_path` where zipfile_name='$sku_no_array_value'")->result_array();
        //print_r($query33);
        // $x = 0;
          foreach ($query33 as $query33_key => $query33_value) {
            // print_r($query33_value);
            $sku_img_path= $query33_value['location_name'];

             $urls[] = str_replace("http://203.122.46.51:82/","http://172.25.0.71:82/",$sku_img_path);
            //  $x++;
            }
        }
        // echo "<pre>";
        // print_r($urls);exit();
        $dir = $micro;
        if (!file_exists($micro)) {
          $save_to =$dir."/SKU_IMAGES_".$dir."/";
          mkdir($dir."/SKU_IMAGES_".$dir, 0777, true);
        }
        $mh = curl_multi_init();
        foreach ($urls as $i => $url) {
            //print_r($save_to.basename($url));
            $g=$save_to.basename($url);
            if(!is_file($g)){
                $conn[$i]=curl_init($url);
                $fp[$i]=fopen ($g, "w");
                curl_setopt ($conn[$i], CURLOPT_FILE, $fp[$i]);
                curl_setopt ($conn[$i], CURLOPT_HEADER ,0);
                curl_setopt($conn[$i],CURLOPT_CONNECTTIMEOUT,1000);
                curl_multi_add_handle ($mh,$conn[$i]);
            }
        }
        do {
            $n=curl_multi_exec($mh,$active);
        }
        while ($active);
        foreach ($urls as $i => $url) {
            curl_multi_remove_handle($mh,$conn[$i]);
            curl_close($conn[$i]);
            fclose ($fp[$i]);
        }
        curl_multi_close($mh);

        $zip_file ='SKU_IMAGES'.time().'.zip';
        // Get real path for our folder
        $rootPath = realpath($dir);

        // Initialize archive object
        $zip = new ZipArchive();
        $zip->open($zip_file, ZipArchive::CREATE | ZipArchive::OVERWRITE);

        // Create recursive directory iterator
        /** @var SplFileInfo[] $files */
        $files = new RecursiveIteratorIterator(
            new RecursiveDirectoryIterator($rootPath),
            RecursiveIteratorIterator::LEAVES_ONLY
        );
        foreach ($sku_thumb as $sku_thumb_key => $sku_thumb_value) {
            $sku_thumb_images = glob($sku_thumb_value."*.{png,PNG,jpg,JPG}",GLOB_BRACE);
           // print_r($save_to);
            foreach($sku_thumb_images as $image){ 
               // print_r($image);
                $image_name1=explode("/",$image);
                $image_name=$save_to.$image_name1[3];
                                    //print_r($image_name);
                if(!copy($image,$image_name)){
                       // echo "failed to copy $image";
                }
                else{
                      //  echo "copied $image_name into $image\n";
                }
            }

        }
        foreach ($files as $name => $file)
        {
            // Skip directories (they would be added automatically)
            if (!$file->isDir())
            {
                // Get real and relative path for current file
                $filePath = $file->getRealPath();
                $relativePath = substr($filePath, strlen($rootPath) + 1);

                // Add current file to archive
                $zip->addFile($filePath, $relativePath);
            }
        }
        //exit();
        //Zip archive will be created only after closing object
        $zip->close();
        // We'll be outputting a PDF
        header('Content-type: application/zip');

        // It will be called downloaded.pdf
        header('Content-Disposition: attachment; filename='.$zip_file);

        // The PDF source is in original.pdf
        readfile($zip_file);
        //force_download($zip_file, NULL);
        if(file_exists($zip_file))
        {   
            unlink($zip_file);
        }
        else
        {
            echo "error".$zip_file;
        }
        if(file_exists($dir))
        {
            if (is_dir($dir)) {
                $scan = glob(rtrim($dir,'/').'/*');
                //print_r($scan);
                foreach($scan as $index=>$path) {
                    $scan2 = glob(rtrim($path,'/').'/*');
                    foreach($scan2 as $index2=>$path2) {
                        if (is_file($path2)) {
                           unlink($path2);
                       }
                   }
                   @rmdir($scan[0]);
               }
               @rmdir($dir);
           }
        }
        //exit();
    }

    public function upload_file(){

        
        if(!empty($_REQUEST['folder_name']))
        {
            $dir = $_REQUEST['folder_name']."/";
        }
        else
        {
            $dir = "./sku_img_thumb/".$_POST['sku_no']."/crop/";
             if (!file_exists($dir)) {
                mkdir($dir, 0777, true);
            }

        }
        $image_name=$_FILES["croppedImage"]["name"];
       // $file=$image_name;
        move_uploaded_file($_FILES["croppedImage"]["tmp_name"], $dir.$image_name);

        echo ltrim($image_name);
    }
    public function upload_file_delete(){
        
         if(!empty($_REQUEST['folder_name']))
        {
            $dir = $_REQUEST['folder_name']."/";
        }
        else
        {
           
            $dir="./sku_img_thumb/".$_POST['sku_no']."/crop/".$_POST['image_name'].".jpg";
            if (file_exists($dir)) {
                unlink($dir);
                echo "Deleted";
               
            }else {
                echo "Not Delete";
             }
           
        }

    }
}

?>
