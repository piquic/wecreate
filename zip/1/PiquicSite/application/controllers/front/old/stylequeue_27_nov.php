<?php 
class stylequeue extends CI_Controller{
	
	function __construct(){
	parent::__construct();
	$this->load->library(['session']); 
	$this->load->database();
    $this->load->helper(['url','file','form']);
    $this->load->helper('directory');
    $this->load->helper('file'); 
	$this->load->model('stylequeue_model');


	}

	public function stylequeueview($sku_key=""){

     if ($this->session->userdata('front_logged_in')) {
    $session_data = $this->session->userdata('front_logged_in');
    $data['user_id'] = $session_data['user_id'];
    $data['user_name'] = $session_data['user_name'];
    }
    else
    {
    $data['user_id'] = '';
    $data['user_name'] = '';
    }

		 $data['sku_key'] = $sku_key;
		
    $data['stylequeinfo']=$this->stylequeue_model->queueimages($sku_key); 		
    $this->load->view('front/stylequeue',$data);
		
	}
 
  function mulmodelupdate(){	    
     $skuno=$this->input->post('skuno');
     $model=$this->input->post('model');   
   
       $data=$this->stylequeue_model->updatemult_modskuinfo($skuno,$model);
       echo json_encode($data);
    }
 
  function autostylingskufuc(){	    
     $skuno=$this->input->post('skuno');
     $autoval=$this->input->post('autoval');   
   
       $data=$this->stylequeue_model->update_autostylingsku($skuno,$autoval);
       echo json_encode($data);
    }



      function vendorsku(){	    
     $skuno=$this->input->post('skuno');
     $vendor=$this->input->post('vendor');   
   
       $data=$this->stylequeue_model->update_vendorsku($skuno,$vendor);
       echo json_encode($data);
    }


    function getModelByGender(){      
     $gender_id=$this->input->post('gender_id');
        
   
       $modelinfo=$this->stylequeue_model->getmodelbygender($gender_id);

        $str='<select class="custom-select navSlct mt-2 text-white" name="slct_model" id="slct_model" onchange="modlmultsku(this);">
        <option class="text-piquic" value="0" >Model</option>';


        foreach($modelinfo as $mrow)
        {


        $mid = $mrow['m_id'];
        $mod_name = $mrow['mod_name'];
        $mod_name =strtoupper($mod_name);
        $str.='<option class="text-piquic" value="'.$mid.'">'.$mod_name.'</option>';
        } 
        $str.='</select>';
          echo $str;
    }


      function updateprocessing(){  

        if ($this->session->userdata('front_logged_in')) {
            $session_data = $this->session->userdata('front_logged_in');
            $data['user_id'] = $session_data['user_id'];
            $data['user_name'] = $session_data['user_name'];
            $user_id = $session_data['user_id'];

            $skuno=$this->input->post('skuno');
            $sty_id=$this->input->post('sty_id');   

            $data=$this->stylequeue_model->update_processing($skuno,$sty_id);
            echo json_encode($data);
      
         
        } else {
          
            $session_data = $this->session->userdata('front_logged_in');
            $data['user_id'] = $session_data['user_id'];
            $data['user_name'] = $session_data['user_name'];
            
      redirect('home');
        }
    

       
      
    }


}
?>