<?php 
class Payments extends CI_Controller{
	
	function __construct(){
		parent::__construct();
		$this->load->library(['session']); 
        $this->load->helper(['url','file','form']);
        $this->load->model('plans_model'); //load model upload 
	}

	public function index(){
		


		if ($this->session->userdata('front_logged_in')) {
            $session_data = $this->session->userdata('front_logged_in');
            $data['user_id'] = $session_data['user_id'];
            $data['user_name'] = $session_data['user_name'];


           
            $plantype_id=$this->input->post('hid_plantype_id');
            $plan_id=$this->input->post('hid_plan_id');
            $plan_amount=$this->input->post('hid_plan_amount');
            $plan_credit=$this->input->post('hid_plan_credit');
            $currency=$this->input->post('currency');
            
			//$data['plansTypeDetails'] = $this->plans_model->getAllPlans();
			//$data['billingDetails'] = $this->plans_model->getAllbilling($data['user_id']);
			$data['plantype_id'] = $plantype_id;
			$data['plan_id'] = $plan_id;
			$data['plansDetails'] = $this->plans_model->get_plansById($plan_id);
			$data['plan_amount'] = $plan_amount;
			$data['plan_credit'] = $plan_credit;
			$data['currency'] = $currency;


			$this->session->set_userdata('sess_plan_id', $plan_id);
			$this->session->set_userdata('sess_currency', $currency);


		    //$this->load->view('front/payments',$data);
		    redirect('paymentdetails/'.$plan_id);
			
         
        } else {
        	
            $session_data = $this->session->userdata('front_logged_in');
            $data['user_id'] = $session_data['user_id'];
            $data['user_name'] = $session_data['user_name'];
            
			redirect('home');
        }
	}



	public function paymentdetails($plan_id){
		


		if ($this->session->userdata('front_logged_in')) {
            $session_data = $this->session->userdata('front_logged_in');
            $data['user_id'] = $session_data['user_id'];
            $data['user_name'] = $session_data['user_name'];


           
            $plantype_id=$this->input->post('hid_plantype_id');
            //$plan_id=$this->input->post('hid_plan_id');
            $plan_amount=$this->input->post('hid_plan_amount');
            $plan_credit=$this->input->post('hid_plan_credit');


            $currency=$this->session->userdata('sess_currency');
            
			//$data['plansTypeDetails'] = $this->plans_model->getAllPlans();
			//$data['billingDetails'] = $this->plans_model->getAllbilling($data['user_id']);
			$data['plantype_id'] = $plantype_id;
			$data['plan_id'] = $plan_id;
			$data['plansDetails'] = $this->plans_model->get_plansById($plan_id);
			$data['plan_amount'] = $plan_amount;
			$data['plan_credit'] = $plan_credit;

			$data['currency'] = $currency;


		    $this->load->view('front/payments',$data);
			
         
        } else {
        	
            $session_data = $this->session->userdata('front_logged_in');
            $data['user_id'] = $session_data['user_id'];
            $data['user_name'] = $session_data['user_name'];
            
			redirect('home');
        }
	}


	public function insert_billing(){
		


		if ($this->session->userdata('front_logged_in')) {
            $session_data = $this->session->userdata('front_logged_in');
            $data['user_id'] = $session_data['user_id'];
            $data['user_name'] = $session_data['user_name'];
            $user_id = $session_data['user_id'];


           
            $billing_id=$this->input->post('billing_id');
            $payment_type_id=$this->input->post('payment_type_id');
            $plan_id=$this->input->post('plan_id');
            $plan_credit=$this->input->post('plan_credit');
            $plan_amount=$this->input->post('plan_amount');
            $currency=$this->input->post('currency');
            $billing_unique_id=md5(uniqid(mt_rand(), true)).$user_id;


            if($billing_id!='')
			{
				
				  $data = array(
				    'user_id'=>$user_id,
					'billing_unique_id'=>$billing_unique_id,
					'billing_plan_id'=>$plan_id,
					'billing_plan_credit'=>$plan_credit,
					'billing_plan_amount'=>$plan_amount,
					'billing_date_time'=>date("Y-m-d H:i:s"),
					'billing_status'=>'Pending',
					'payment_type'=>$payment_type_id,
					'payment_status'=>'Notpaid',
					'currency'=>$currency,
					);
					
				  $updateuserrole = $this->plans_model->billing_update($data,$billing_id);
			      echo $billing_id;
				
			}else {
				
				
				
				     $data = array(
				    'user_id'=>$user_id,
					'billing_unique_id'=>$billing_unique_id,
					'billing_plan_id'=>$plan_id,
					'billing_plan_credit'=>$plan_credit,
					'billing_plan_amount'=>$plan_amount,
					'billing_date_time'=>date("Y-m-d H:i:s"),
					'billing_status'=>'Pending',
					'payment_type'=>$payment_type_id,
					'payment_status'=>'Notpaid',
					'currency'=>$currency,
					);
					$this->plans_model->billing_insert($data);
					$billing_id=$this->db->insert_id();
					
					
					echo $billing_id;
				}
            
            
			


		    //redirect('home');
			
         
        } else {
        	
            $session_data = $this->session->userdata('front_logged_in');
            $data['user_id'] = $session_data['user_id'];
            $data['user_name'] = $session_data['user_name'];
            
			//redirect('home');
        }
	}






	public function paymentsuccess(){
		


		if ($this->session->userdata('front_logged_in')) {
            $session_data = $this->session->userdata('front_logged_in');
            $data['user_id'] = $session_data['user_id'];
            $data['user_name'] = $session_data['user_name'];
            $user_id = $session_data['user_id'];


            //echo "<pre>"; print_r($_POST);echo "</pre>";
            //echo "<pre>"; print_r($this->input->post());echo "</pre>";

            $payment_amount=$this->input->post('udf2');
            $payment_credit=$this->input->post('udf3');

            /************************************************************************/
				$pautoquery = "SELECT * FROM `tbl_users` where user_id='$user_id'  ";
				// echo $pautoquery;
				$query2=$this->db->query($pautoquery);
				$usrinfo= $query2->result_array();
				$total_amount=$usrinfo[0]['total_amount'];
				$total_credit=$usrinfo[0]['total_credit'];


				if($total_amount!='')
				{
				$current_amount=$total_amount+$payment_amount;
				$amount_history=$payment_amount;
				$payment_status="Paid";
				}
				else
				{
				$current_amount=0+$payment_amount;
				$amount_history=$payment_amount;
				$payment_status="Paid";
				}



				if($total_credit!='')
				{
				$current_credit=$total_credit+$payment_credit;
				$credit_history=$payment_credit;
				$payment_status="Paid";
				}
				else
				{
				$current_credit=0+$payment_credit;
				$credit_history=$payment_credit;
				$payment_status="Paid";
				}


				$userdata=array(
				'total_amount' => $current_amount,
				'total_credit' => $current_credit);

				$this->db->where('user_id', $user_id);
				$report = $this->db->update('tbl_users', $userdata);






				$data=array(
				'user_id' => $user_id,
				'billing_id' => '0',
				'sty_id' => '0',
				'upimg_id' => '0',
				'amount' => $amount_history,
				'credit' => $credit_history,
				'account_history_date' => date("Y-m-d"),
				'account_history_time' => date("H:i:s"),
				'payment_status' => $payment_status,
				);
				$this->db->insert('tbl_account_history', $data);

      /************************************************************************/
            

             $status=$this->input->post('status');
            $billing_id=$this->input->post('udf1');
            if($status=='success')
            {
            	$data = array(
				    'billing_date_time'=>date("Y-m-d H:i:s"),
					'billing_status'=>'Completed',
					'payment_status'=>'Paid',
					);
					
			    $updateuserrole = $this->plans_model->billing_update($data,$billing_id);
            }
            
			 

            $this->load->view('front/paymentssuccess',$data);
		   
			
         
        } else {
        	
            $session_data = $this->session->userdata('front_logged_in');
            $data['user_id'] = $session_data['user_id'];
            $data['user_name'] = $session_data['user_name'];
            
			redirect('home');
        }
	}



	public function paymentfailure(){
		


		if ($this->session->userdata('front_logged_in')) {
            $session_data = $this->session->userdata('front_logged_in');
            $data['user_id'] = $session_data['user_id'];
            $data['user_name'] = $session_data['user_name'];

            /*echo "<pre>"; print_r($_POST);echo "</pre>";
            echo "<pre>"; print_r($this->input->post());echo "</pre>";
            exit;*/

            $status=$this->input->post('status');
            $billing_id=$this->input->post('udf1');
            if($status=='failure')
            {
            	$this->plans_model->delete_billing($billing_id);
            }

            $this->load->view('front/paymentsfailure',$data);
		   
			
         
        } else {
        	
            $session_data = $this->session->userdata('front_logged_in');
            $data['user_id'] = $session_data['user_id'];
            $data['user_name'] = $session_data['user_name'];
            
			redirect('home');
        }
	}












}

?>