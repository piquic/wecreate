<?php 
class Revbook extends CI_Controller{
	
	function __construct(){
		parent::__construct();
		$this->load->library(['session']); 
        $this->load->helper(['url','file','form']);
         $this->load->model('Revbook_model');
	}

	public function index(){
		if ($this->session->userdata('front_logged_in')) {
			$session_data = $this->session->userdata('front_logged_in');
			$data['user_id'] = $session_data['user_id'];
			$data['user_name'] = $session_data['user_name'];
			$data['type1']=$session_data['type1'];
			$this->load->view('front/revbook',$data );

		}
		else
		{
			redirect('/');
		}
		
	}
	public function add_book(){
		// echo"oooooo";exit();
		$created_date = date('Y-m-d');
		$order_id="piquic_".time();
		$user_id=$this->input->post('user_id');
		$gender=$this->input->post('gender');
		$m_status=$this->input->post('m_status');
		$category=",".$this->input->post('category').",";	
		$quty=",".$this->input->post('quty').",";
		$angle=",".$this->input->post('angle').",";
		$category1=$this->input->post('category');	
		$quty1=$this->input->post('quty');
		$angle1=$this->input->post('angle');
		$d_status=$this->input->post('d_status');
		$notes=$this->input->post('notes');
		$total_price=$this->input->post('total_price');
		$p_status="Payment Pending";
		$data = array(
		
					'user_id'=>$user_id,
					'order_id'=>$order_id,
					'gender'=>$gender,
					'category'=>$category,
					'quty'=>$quty,
					'm_status'=>$m_status,
					'angle'=>$angle,
					'd_status'=>$d_status,
					'notes'=>$notes,
					'b_status'=>"1",
					'total_price'=>$total_price,
					'p_status'=>$p_status,
					'create_date'=>$created_date,
					);
					//print_r($data); exit();
					//$this->load->view('front/revbook',$data );
					$result=$this->Revbook_model->add_booking($data);
	    $sql = "SELECT * FROM tbl_users WHERE user_id = '$user_id'";
	    $query = $this->db->query($sql);
	    if( $query->num_rows() ==1 ){
	    	
	    	$userDetails= $query->result_array();
	    
			$user_name=$userDetails[0]['user_name'];
			$user_email1=$userDetails[0]['user_email'];
			$user_email_value=explode("@",$user_email1);
			$user_email=substr_replace($user_email_value[0], "*****", 2)."@".$user_email_value[1];
			//print_r($url);
			$subject='Piquic Account - Booking Conform !!';
			//echo base64_encode("http://localhost/piquic_client_new/reset_password");	       	

			/*********************************Send mail*************************************/

			$query=$this->db->query("select * from tbl_settings WHERE setting_key='MAIL_SMTP_USER' ");
			$userDetails= $query->result_array();
			$MAIL_SMTP_USER=$userDetails[0]['setting_value'];

			$query=$this->db->query("select * from tbl_settings WHERE setting_key='MAIL_SMTP_PASSWORD' ");
			$userDetails= $query->result_array();
			$MAIL_SMTP_PASSWORD=$userDetails[0]['setting_value'];

			$query=$this->db->query("select * from tbl_settings WHERE setting_key='MAIL_BODY_BOOKING' ");
			$userDetails= $query->result_array();
			$MAIL_BODY_BOOKING=$userDetails[0]['setting_value'];


			//Load email library
			$this->load->library('email');

			$config = array();
			//$config['protocol'] = 'smtp';
			$config['protocol'] = 'mail';
			$config['smtp_host'] = 'localhost';
			$config['smtp_user'] = $MAIL_SMTP_USER;
			$config['smtp_pass'] = $MAIL_SMTP_PASSWORD;
			$config['smtp_port'] = 25;




			$this->email->initialize($config);
			$this->email->set_newline("\r\n");


			$from_email = "demo.intexom@gmail.com";
			$to_email = $user_email1;


			$message=$MAIL_BODY_BOOKING;
			$find_arr = array("##USER_NAME##","##USER_EMAIL##","##GENDER##","##CATEGORY##","##QUANTITY##","##MODEL##","##ANGLES##","##DELIVERY##","##NOTES##","##P_STATUS##","##TOTAL_PRICE##");
			$replace_arr = array($user_name,$user_email,$gender,$category1,$quty1,$m_status,$angle1,$d_status,$notes,$p_status,$total_price);


			$message=str_replace($find_arr,$replace_arr,$MAIL_BODY_BOOKING);

			//echo $message;exit;
			$this->email->from($from_email, 'Piquic');
			$this->email->to($to_email);
			$this->email->cc('poulami@mokshaproductions.in');
			$this->email->cc('pranav@mokshaproductions.in');
			$this->email->bcc('raja.priya@mokshaproductions.in');
			$this->email->subject($subject);
			$this->email->message($message);
			//Send mail
			if($this->email->send())
			$this->session->set_flashdata("email_sent","Congragulation Email Send Successfully.");
		
			else
			$this->session->set_flashdata("email_sent","You have encountered an error");

			//exit;
			 echo '1';
			/************************************************************************/
	    } else {
	        echo '0';
	    }
		
	
					//$user_id=$this->db->insert_id();					
					//echo $result;
				
	}
	public function booksuccess(){
		


		if ($this->session->userdata('front_logged_in')) {
            $session_data = $this->session->userdata('front_logged_in');
            $data['user_id'] = $session_data['user_id'];
            $data['user_name'] = $session_data['user_name'];
            $user_id = $session_data['user_id'];


            $this->load->view('front/booksuccess',$data);
		   
			
         
        } else {
        	
            $session_data = $this->session->userdata('front_logged_in');
            $data['user_id'] = $session_data['user_id'];
            $data['user_name'] = $session_data['user_name'];
            
			redirect('home');
        }
	}
	public function bookfailure(){
		


		if ($this->session->userdata('front_logged_in')) {
            $session_data = $this->session->userdata('front_logged_in');
            $data['user_id'] = $session_data['user_id'];
            $data['user_name'] = $session_data['user_name'];
            $user_id = $session_data['user_id'];


            $this->load->view('front/bookfailure',$data);
		   
			
         
        } else {
        	
            $session_data = $this->session->userdata('front_logged_in');
            $data['user_id'] = $session_data['user_id'];
            $data['user_name'] = $session_data['user_name'];
            
			redirect('home');
        }
	}
}

?>