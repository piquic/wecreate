<?php 
class Upload extends CI_Controller{
    
    function __construct(){
        parent::__construct();
        $this->load->library(['session']); 
        $this->load->helper(['url','file','form']); 
        $this->load->model('upload_model'); //load model upload 
        $this->load->model('home_model'); //load model upload 
        $this->load->library('upload'); //load library upload 
    }

    public function index(){


        $data['title'] = "upload images";
        $this->load->view('inc/header', $data);
        $this->load->view('inc/menu');      
        $this->load->view('inc/nav');
        $this->load->view('uploadimage');
        $this->load->view('inc/footer');
    }

        
public function checkexistsku()
{           // checking the email  exist  validation in add form
    
       if ($this->session->userdata('front_logged_in')) {
    $session_data = $this->session->userdata('front_logged_in');
    $data['user_id'] = $session_data['user_id'];
    $data['user_name'] = $session_data['user_name'];

    $user_id=$session_data['user_id'];;
    }
    else
    {
    $data['user_id'] = '';
    $data['user_name'] = '';
    $user_id='';
    }


    $txtSKUNew = $this->input->post('txtSKUNew');
    $sql = "SELECT * FROM tbl_upload_img WHERE user_id='$user_id' and zipfile_name = '$txtSKUNew' ";
    $query = $this->db->query($sql);
    if( $query->num_rows() > 0 ){
        echo 'false';
    } else {
        echo 'true';
    }
    
}   





public function uploadimage(){


        if ($this->session->userdata('front_logged_in')) {
            $session_data = $this->session->userdata('front_logged_in');
            $data['user_id'] = $session_data['user_id'];
            $data['user_name'] = $session_data['user_name'];
            
            $id='4';
            $data['pagecontents'] = $this->home_model->get_page($id);
            $this->load->view('front/uploadimage');    
            
         
        } else {
            //echo "aaaaaa";exit;
            //If no session, redirect to login page
            //redirect('login', 'refresh');
            $session_data = $this->session->userdata('front_logged_in');
            $data['user_id'] = $session_data['user_id'];
            $data['user_name'] = $session_data['user_name'];
            
            $id='3';
            $data['pagecontents'] = $this->home_model->get_page($id);
            $this->load->view('front/uploadimage');    
        }
            
            
            
            
        }



    public function uploadfile(){

        if ($this->session->userdata('front_logged_in')) {
            $session_data = $this->session->userdata('front_logged_in');
            $data['user_id'] = $session_data['user_id'];
            $data['user_name'] = $session_data['user_name'];
            $folderName=$data['user_id'];


            
                $fileName = $_FILES["upld_zip"]["name"]; // The file name
                $fileTmpLoc = $_FILES["upld_zip"]["tmp_name"]; // File in the PHP tmp folder
                $fileType = $_FILES["upld_zip"]["type"]; // The type of file it is
                $fileSize = $_FILES["upld_zip"]["size"]; // File size in bytes
                $fileErrorMsg = $_FILES["upld_zip"]["error"]; // 0 for false... and 1 for true
                if (!$fileTmpLoc) { // if file not chosen
                echo "ERROR: Please select a file to upload.";
                exit();
                }
                
                $folder_path="zip/$folderName";
                if (!file_exists($folder_path)) {
                mkdir($folder_path, 0777, true);
                }
                if(move_uploaded_file($fileTmpLoc, "$folder_path/$fileName")){
                echo "ZIP file uploaded successfully!";

                } else {
                echo "ZIP file upload failed!";
                } 
            
         
        } else {
            //echo "aaaaaa";exit;
            //If no session, redirect to login page
            redirect('login', 'refresh');
        }
            
            
            
                
        }

   public function moveunzipfile(){


        if ($this->session->userdata('front_logged_in')) {
        $session_data = $this->session->userdata('front_logged_in');
        $data['user_id'] = $session_data['user_id'];
        $data['user_name'] = $session_data['user_name'];
        $user_name = $session_data['user_name'];
        $user_id=$data['user_id'];
        $folderName=$data['user_id'];

        $file_nm=$this->input->post('file_nm');
        $current_path="zip/".$user_id."/";

        $dir = "zip/".$user_id."/";
        // Open a directory, and read its contents
        if (is_dir($dir)){
        if ($dh = opendir($dir)){
        while (($file = readdir($dh)) !== false){
       
        if($file!='.' && $file!='..')
        {
           //echo "filename:" . $file . "<br>";
            $sku_folder=$file;
            
            $data = array(
            'user_id'=>$user_id,
            'zipfile_name'=>$sku_folder,
            'client_name'=>$user_name,
             );

            $this->upload_model->upload_img_insert($data);
            $upimg_id=$this->db->insert_id();

                $src = "zip/".$user_id."/".$sku_folder; 

                $dst = "upload/".$user_id."/".$sku_folder; 


                if (!file_exists($dst)) {
                mkdir($dst, 0777, true);
                }

                $this->custom_copy($src, $dst); 
                



 ///////////////////////////////////////////////////////////////////////////////////////////    

                // /$directories = glob('upload/'.$user_id.'/'.$sku_folder . '/*' , GLOB_ONLYDIR);
                
                // echo "upload/".$user_id."/".$sku_folder."/*.*";
                $directories = glob("upload/".$user_id."/".$sku_folder."/*.*", GLOB_BRACE);

                 //echo "<pre>";

               // print_r( $directories);
              


               

                foreach($directories as $file)
                {
                //echo "---".$dir."---";


                $ext = pathinfo($file, PATHINFO_EXTENSION);

                //echo $file."-----";
                $arr=explode("/",$file); 



                $dir = "./".$arr[0]."/".$arr[1]."/".$arr[2]."/";

                $img = $file;

                $file_image=pathinfo($arr[3], PATHINFO_FILENAME);

                if(strtoupper($ext)=='PNG')
                {
                //////////////////////////Convert JPG start//////////////////////////////////////

                $this->convertToJpeg($dir,$img,$file_image);
                unlink($file);
                }


                $file_image=pathinfo($arr[3], PATHINFO_FILENAME);
                $new_thumb = "./".$arr[0]."/".$arr[1]."/".$arr[2]."/"."thumb_".$file_image.".jpg";
                $file_p = "./".$arr[0]."/".$arr[1]."/".$arr[2]."/".$file_image.".jpg";

                $this->convertToThumb($new_thumb,$user_id,$file_p);







                



                }   




                //////////////////////////////////////////////////////////////////








            
        ////////////////////////////////////////////////////////////////////////////

                $dir_sub = "zip/".$user_id."/".$sku_folder."/";
                // Open a directory, and read its contents
                if (is_dir($dir_sub)){
                if ($dh_sub = opendir($dir_sub)){
                while (($file_sub = readdir($dh_sub)) !== false){

                if($file_sub!='.' && $file_sub!='..')
                {
                //echo "filename:" . $file_sub . "<br>";
                $sku_file=$file_sub;

                ////////////////////////////////////////////////////////////////////////////


                    $img_name_check=strtoupper($file_sub);

                    if(strstr($img_name_check,'FRONT'))
                    {
                    $img_ang='Front';
                    }
                    elseif(strstr($img_name_check,'BACK'))
                    {
                    $img_ang='Back';
                    }
                    elseif(strstr($img_name_check,'LEFT'))
                    {
                    $img_ang='LeftSide';
                    }
                    elseif(strstr($img_name_check,'RIGHT'))
                    {
                    $img_ang='RightSide';
                    }
                    elseif(strstr($img_name_check,'TAG'))
                    {
                    $img_ang='Tag';
                    }
                    else
                    {
                    $img_ang='';
                    }


                    $ext_item = pathinfo($file_sub, PATHINFO_EXTENSION);
                    if(strtoupper($ext_item)=='PNG')
                    {
                    $file_image_item=str_replace("png","jpg",str_replace("PNG","JPG",$file_sub));
                    }
                    else
                    {
                    $file_image_item=$file_sub;
                    }



                    $data = array(
                    //'user_id'=>$user_id,
                    'upimg_id'=>$upimg_id,
                    'img_name'=>$file_image_item,
                    'img_ang'=>$img_ang,
                    );

                    $checkquerys = $this->db->query("select id from tbl_upload_images where upimg_id='$upimg_id' and img_name='$file'  and upimg_id='$upimg_id'")->result_array();
                    if(empty($checkquerys))
                    {
                    $this->upload_model->upload_images_insert($data);
                    $id=$this->db->insert_id();
                    }
                    else
                    {
                    $id=$checkquerys[0]['id'];
                    }



                ////////////////////////////////////////////////////////////////////////////



                }
                }
                closedir($dh_sub);
                }
                } 


           $this->delete_directory($src); 

          ////////////////////////////////////////////////////////////////////////////



       


        }
        }
        closedir($dh);
        }
        }





















        
                
        }

    }


function custom_copy($src, $dst) {  
  
    // open the source directory 
    $dir = opendir($src);  
  
    // Make the destination directory if not exist 
    @mkdir($dst);  
  
    // Loop through the files in source directory 
    while( $file = readdir($dir) ) {  
  
        if (( $file != '.' ) && ( $file != '..' )) {  
            if ( is_dir($src . '/' . $file) )  
            {  
  
                // Recursively calling custom copy function 
                // for sub directory  
                custom_copy($src . '/' . $file, $dst . '/' . $file);  
  
            }  
            else {  
                copy($src . '/' . $file, $dst . '/' . $file);  
            }  
        }  
    }  
  
    closedir($dir); 
} 

public function delete_directory( $dir )
{
        if( is_dir( $dir ) )
        {
        $files = glob( $dir . '*', GLOB_MARK ); //GLOB_MARK adds a slash to directories returned

        foreach( $files as $file )
        {
        $this->delete_directory( $file );      
        }

        @rmdir( $dir );
        } 
        elseif( is_file( $dir ) ) 
        {
        unlink( $dir );  
        }
}
   
    public function moveunzipfile_old(){


        if ($this->session->userdata('front_logged_in')) {
        $session_data = $this->session->userdata('front_logged_in');
        $data['user_id'] = $session_data['user_id'];
        $data['user_name'] = $session_data['user_name'];
        $user_name = $session_data['user_name'];
        $user_id=$data['user_id'];
        $folderName=$data['user_id'];

        $file_nm=$this->input->post('file_nm');
        $current_path="zip/$folderName/$file_nm";

        $unzip = new ZipArchive;
        $out = $unzip->open($current_path);
        if ($out === TRUE) {

        //echo getcwd();
        //$unzip->extractTo(getcwd());
        $unzip->extractTo('zip/'.$folderName.'/');
        $unzip->close();
        //echo 'File unzipped';


        //////////////////////////////////////////////////////////////////
        $directories = glob('zip/'.$folderName . '/*' , GLOB_ONLYDIR);

        //echo "<pre>";
        //print_r( $directories);exit;

        foreach($directories as $dir)
        {

        $dir_path=str_replace('zip/'.$folderName.'/', '', $dir);




        $data = array(
        'user_id'=>$user_id,
        'zipfile_name'=>$dir_path,
        'client_name'=>$user_name,

        );

        $checkquerys = $this->db->query("select upimg_id from tbl_upload_img where zipfile_name='$dir_path' and user_id='$user_id' ")->result_array();
        if(empty($checkquerys))
        {
        $this->upload_model->upload_img_insert($data);
        $upimg_id=$this->db->insert_id();
        }
        else
        {
        $upimg_id=$checkquerys[0]['upimg_id'];
        }




        $sub_path='zip/'.$folderName . '/'.$dir_path . '/';

        $files = array_diff(scandir($sub_path), array('.', '..'));

        /* echo "<pre>";
        print_r( $files);
        exit;*/



        foreach($files as $file)
        {



        $img_name_check=strtoupper($file);

        if(strstr($img_name_check,'FRONT'))
        {
        $img_ang='Front';
        }
        elseif(strstr($img_name_check,'BACK'))
        {
        $img_ang='Back';
        }
        elseif(strstr($img_name_check,'LEFT'))
        {
        $img_ang='LeftSide';
        }
        elseif(strstr($img_name_check,'RIGHT'))
        {
        $img_ang='RightSide';
        }
        elseif(strstr($img_name_check,'TAG'))
        {
        $img_ang='Tag';
        }
        else
        {
        $img_ang='';
        }


        $ext_item = pathinfo($file, PATHINFO_EXTENSION);
        if(strtoupper($ext_item)=='PNG')
        {
        $file_image_item=str_replace("png","jpg",str_replace("PNG","JPG",$file));
        }
        else
        {
        $file_image_item=$file;
        }



        $data = array(
        //'user_id'=>$user_id,
        'upimg_id'=>$upimg_id,
        'img_name'=>$file_image_item,
        'img_ang'=>$img_ang,
        );

        $checkquerys = $this->db->query("select id from tbl_upload_images where upimg_id='$upimg_id' and img_name='$file'  and upimg_id='$upimg_id'")->result_array();
        if(empty($checkquerys))
        {
        $this->upload_model->upload_images_insert($data);
        $id=$this->db->insert_id();
        }
        else
        {
        $id=$checkquerys[0]['id'];
        }






        }



        }

        ///////////////////////////////////////////////////////////////////////////////////////////
        $current_path="zip/$folderName/$file_nm";


        $folder_path_target="upload/$folderName";
        if (!file_exists($folder_path_target)) {
        mkdir($folder_path_target, 0777, true);
        }
        $target_path=$folder_path_target."/$file_nm";
        if(copy($current_path, $target_path)){
        echo "ZIP file moved successfully!";


        $unzip = new ZipArchive;
        $out = $unzip->open($target_path);
        if ($out === TRUE) {

        //echo getcwd();
        //$unzip->extractTo(getcwd());
        $unzip->extractTo('upload/'.$folderName);
        $unzip->close();
        //echo 'File unzipped';
        //unlink($current_user_path);

        unlink($current_path);
        unlink($target_path);




        $directories = glob('zip/'.$folderName . '/*' , GLOB_ONLYDIR);

        //echo "<pre>";
        //print_r( $directories);exit;

        foreach($directories as $dir)
        {
        //echo "---".$dir."---";

        $files = glob($dir.'/*'); // get all file names
        foreach($files as $file){ // iterate files
        if(is_file($file))
        unlink($file); // delete file
        }

        rmdir($dir);

        }

        rmdir('zip/'.$folderName);



        } else {
        echo 'Error';
        }





        } else {
        echo "ZIP file moved failed!";
        }






        /////////////////////////////////////////////////////////////////////////////////////////// 

        $directories = glob('upload/'.$folderName . '/*' , GLOB_ONLYDIR);


        //echo "<pre>";

        //print_r( $directories);exit;

        foreach($directories as $dir)
        {
        //echo "---".$dir."---";

        $files = glob($dir.'/*'); // get all file names
        foreach($files as $file){ // iterate files

        $ext = pathinfo($file, PATHINFO_EXTENSION);

        //echo $file."-----";
        $arr=explode("/",$file); 



        $dir = "./".$arr[0]."/".$arr[1]."/".$arr[2]."/";

        $img = $file;

        $file_image=pathinfo($arr[3], PATHINFO_FILENAME);

        if(strtoupper($ext)=='PNG')
        {



        //////////////////////////Convert JPG start//////////////////////////////////////

        $this->convertToJpeg($dir,$img,$file_image);
        unlink($file);






        }


        $file_image=pathinfo($arr[3], PATHINFO_FILENAME);
        $new_thumb = "./".$arr[0]."/".$arr[1]."/".$arr[2]."/"."thumb_".$file_image.".jpg";
        $file_p = "./".$arr[0]."/".$arr[1]."/".$arr[2]."/".$file_image.".jpg";

        $this->convertToThumb($new_thumb,$user_id,$file_p);







        }



        }   




        //////////////////////////////////////////////////////////////////
        } else {
        echo 'Error';
        }







        } else {
        //echo "aaaaaa";exit;
        //If no session, redirect to login page
        redirect('login', 'refresh');
        }

            
                
        }


public function moveunzipfileCopy(){


        if ($this->session->userdata('front_logged_in')) {
        $session_data = $this->session->userdata('front_logged_in');
        $data['user_id'] = $session_data['user_id'];
        $data['user_name'] = $session_data['user_name'];
        $user_name = $session_data['user_name'];
        $user_id=$data['user_id'];
        $folderName=$data['user_id'];

        $file_nm=$this->input->post('file_nm');
        $current_path="zip/$folderName/$file_nm";

        $unzip = new ZipArchive;
        $out = $unzip->open($current_path);
        if ($out === TRUE) {

        //echo getcwd();
        //$unzip->extractTo(getcwd());
        $unzip->extractTo('zip/'.$folderName.'/');
        $unzip->close();
        echo 'File unzipped';


        //////////////////////////////////////////////////////////////////
        $directories = glob('zip/'.$folderName . '/*' , GLOB_ONLYDIR);

        //echo "<pre>";
        //print_r( $directories);exit;

        foreach($directories as $dir)
        {

        $dir_path=str_replace('zip/'.$folderName.'/', '', $dir);


        $data = array(
        'user_id'=>$user_id,
        'zipfile_name'=>$dir_path,
        'client_name'=>$user_name,

        );

        $checkquerys = $this->db->query("select upimg_id from tbl_upload_img where zipfile_name='$dir_path' and user_id='$user_id' ")->result_array();
        if(empty($checkquerys))
        {
        $this->upload_model->upload_img_insert($data);
        $upimg_id=$this->db->insert_id();
        }
        else
        {
        $upimg_id=$checkquerys[0]['upimg_id'];
        }




        $sub_path='zip/'.$folderName . '/'.$dir_path . '/';

        $files = array_diff(scandir($sub_path), array('.', '..'));

        /* echo "<pre>";
        print_r( $files);
        exit;*/



        foreach($files as $file)
        {



        $img_name_check=strtoupper($file);

        if(strstr($img_name_check,'FRONT'))
        {
        $img_ang='Front';
        }
        elseif(strstr($img_name_check,'BACK'))
        {
        $img_ang='Back';
        }
        elseif(strstr($img_name_check,'LEFT'))
        {
        $img_ang='LeftSide';
        }
        elseif(strstr($img_name_check,'RIGHT'))
        {
        $img_ang='RightSide';
        }
        elseif(strstr($img_name_check,'TAG'))
        {
        $img_ang='Tag';
        }
        else
        {
        $img_ang='';
        }


        $ext_item = pathinfo($file, PATHINFO_EXTENSION);
        if(strtoupper($ext_item)=='PNG')
        {
        $file_image_item=str_replace("png","jpg",str_replace("PNG","JPG",$file));
        }
        else
        {
        $file_image_item=$file;
        }



        $data = array(
        //'user_id'=>$user_id,
        'upimg_id'=>$upimg_id,
        'img_name'=>$file_image_item,
        'img_ang'=>$img_ang,
        );

        $checkquerys = $this->db->query("select id from tbl_upload_images where upimg_id='$upimg_id' and img_name='$file'  and upimg_id='$upimg_id'")->result_array();
        if(empty($checkquerys))
        {
        $this->upload_model->upload_images_insert($data);
        $id=$this->db->insert_id();
        }
        else
        {
        $id=$checkquerys[0]['id'];
        }






        }



        }

        ///////////////////////////////////////////////////////////////////////////////////////////
        $current_path="zip/$folderName/$file_nm";


        $folder_path_target="upload/$folderName";
        if (!file_exists($folder_path_target)) {
        mkdir($folder_path_target, 0777, true);
        }
        $target_path=$folder_path_target."/$file_nm";
        if(copy($current_path, $target_path)){
        echo "ZIP file moved successfully!";


        $unzip = new ZipArchive;
        $out = $unzip->open($target_path);
        if ($out === TRUE) {

        //echo getcwd();
        //$unzip->extractTo(getcwd());
        $unzip->extractTo('upload/'.$folderName);
        $unzip->close();
        //echo 'File unzipped';
        //unlink($current_user_path);

        unlink($current_path);
        unlink($target_path);




        $directories = glob('zip/'.$folderName . '/*' , GLOB_ONLYDIR);

        //echo "<pre>";
        //print_r( $directories);exit;

        foreach($directories as $dir)
        {
        //echo "---".$dir."---";

        $files = glob($dir.'/*'); // get all file names
        foreach($files as $file){ // iterate files
        if(is_file($file))
        unlink($file); // delete file
        }

        rmdir($dir);

        }

        rmdir('zip/'.$folderName);



        } else {
        echo 'Error';
        }





        } else {
        echo "ZIP file moved failed!";
        }






        /////////////////////////////////////////////////////////////////////////////////////////// 

        $directories = glob('upload/'.$folderName . '/*' , GLOB_ONLYDIR);


        //echo "<pre>";

        //print_r( $directories);exit;

        foreach($directories as $dir)
        {
        //echo "---".$dir."---";

        $files = glob($dir.'/*'); // get all file names
        foreach($files as $file){ // iterate files

        $ext = pathinfo($file, PATHINFO_EXTENSION);

        //echo $file."-----";
        $arr=explode("/",$file); 



        $dir = "./".$arr[0]."/".$arr[1]."/".$arr[2]."/";

        $img = $file;

        $file_image=pathinfo($arr[3], PATHINFO_FILENAME);

        if(strtoupper($ext)=='PNG')
        {



        //////////////////////////Convert JPG start//////////////////////////////////////

        $this->convertToJpeg($dir,$img,$file_image);
        unlink($file);






        }


        $file_image=pathinfo($arr[3], PATHINFO_FILENAME);
        $new_thumb = "./".$arr[0]."/".$arr[1]."/".$arr[2]."/"."thumb_".$file_image.".jpg";
        $file_p = "./".$arr[0]."/".$arr[1]."/".$arr[2]."/".$file_image.".jpg";

        $this->convertToThumb($new_thumb,$user_id,$file_p);







        }



        } 




        //////////////////////////////////////////////////////////////////
        } else {
        echo 'Error';
        }







        } else {
        //echo "aaaaaa";exit;
        //If no session, redirect to login page
        redirect('login', 'refresh');
        }

      
        
    }

public function convertToThumb($new_thumb,$user_id,$file_path) {

  if(!file_exists($new_thumb)){
  $width = "400";
  $height = "600";
  $quality = 90;
  $img = $file_path;

  //Generate Thumbnail Images

  $file = $img;
  $dest = $new_thumb;
  $height = 600;
  $width = 400;
  $output_format = "jpg";
  $output_quality = 90;
  $bg_color = array(255, 255, 255);


  //Justify the Image format and create a GD resource
  $image_info = getimagesize($file);
  list($cur_width, $cur_height, $cur_type, $cur_attr) = getimagesize($file);




  $image_type = $image_info[2];
  switch($image_type){
    case IMAGETYPE_JPEG:
    $image = imagecreatefromjpeg($file);
    break;
    case IMAGETYPE_GIF:
    $image = imagecreatefromgif($file);
    break;
    case IMAGETYPE_GIF:
    $image = imagecreatefrompng($file);
    break;
    default:
    die("Image Format not Suppoted");
  }

 if($cur_width>$cur_height){
  $degrees = -90;
  $image = imagerotate($image, $degrees, 0);
  }

  $image_width = imagesx($image);
  $image_height = imagesy($image);




  //echo $file;

  //Get The Calculations
  // Calculate the size of the Image
  //If Image width is bigger than the Thumbnail Width
  if($image_width>$image_height){





//echo "hoko";
//echo $image_width.">".$image_height;

        //Set Image Width to Thumbnail Width
    $new["width"] = $width;
        //Calculate Height according to width
    $new["height"] = ($new["width"]/$image_width)*$image_height;

        //If Resulting height is bigger than the thumbnail Height
    if($new["height"]>$height){

            //Set the image Height to THUmbnail Height
      $new["height"] = $height;
            //Recalculate width according to height of the thumbnail
      $new["width"] = ($new["height"]/$image_height)*$image_width;

    }

  }else{
//echo "moko";
    $new["height"] = $height;
    $new["width"] = ($new["height"]/$image_height)*$image_width;

    if($new["width"]>$width){

      $new["width"] = $width;
      $new["height"] = ($new["width"]/$image_width)*$image_height;

    }

  }

    //Calculate the image position based on the difference between the dimensons of the new image and thumbnail
  $x = ($width-$new["width"])/2;
  $y = ($height-$new["height"])/2;

  $calc =  array_merge($new, array("x"=>$x,"y"=>$y));


  // End Calculate The Image



  //Create an Empty image
  $canvas = imagecreatetruecolor($width, $height);

    //Load Background color
  $color = imagecolorallocate($canvas,
    $bg_color[0],
    $bg_color[1],
    $bg_color[2]
  );

    //FIll the Image with the Background color
  imagefilledrectangle(
    $canvas,
    0,
    0,
    $width,
    $height,
    $color
  );

    //The REAL Magic
  imagecopyresampled(
    $canvas,
    $image,
    $calc["x"],
    $calc["y"],
    0,
    0,
    $calc["width"],
    $calc["height"],
    $image_width,
    $image_height
  );

// Create Output Image

  $image = $canvas;
  $format = $output_format;
  $quality = $output_quality;



  switch($format){
    case "jpg":
    imagejpeg($image, $dest, $quality);
    break;
    case "gif":
    imagegif($image, $dest);
    break;
    case "png":
            //Png Quality is measured from 1 to 9
    imagepng($image, $dest, round(($quality/100)*9) );
    break;
  }

  //unlink($img);

}
else{
  echo "file is exist.";
}




}

public function convertToJpeg($dir,$img,$file_image) {

        $dst = $dir . $file_image;

        if (($img_info = getimagesize($img)) === FALSE)
        die("Image not found or not an image");

        $width = $img_info[0];
        $height = $img_info[1];

        switch ($img_info[2]) {
        case IMAGETYPE_GIF  : $src = imagecreatefromgif($img);  break;
        case IMAGETYPE_JPEG : $src = imagecreatefromjpeg($img); break;
        case IMAGETYPE_PNG  : $src = imagecreatefrompng($img);  break;
        default : die("Unknown filetype");
        }

        $tmp = imagecreatetruecolor($width, $height);

        $file =$dst.".jpg";

        imagecopyresampled($tmp, $src, 0, 0, 0, 0, $width, $height, $width, $height);
        imagejpeg($tmp, $file);
        return $file_image.".jpg";

}



    public function insertSKUFromSidebar(){

     if ($this->session->userdata('front_logged_in')) {
    $session_data = $this->session->userdata('front_logged_in');
    $data['user_id'] = $session_data['user_id'];
    $data['user_name'] = $session_data['user_name'];

    $user_id=$session_data['user_id'];
    $user_name=$session_data['user_name'];
    }
    else
    {
    $data['user_id'] = '';
    $data['user_name'] = '';
    $user_id='';
    $user_name='';
    }

    $txtSKUNew=$this->input->post('txtSKUNew');
    $pimg=$this->input->post('pimg');


    $data = array(
    'user_id'=>$user_id,
    'zipfile_name'=>$txtSKUNew,
    'client_name'=>$user_name,

    );

    $checkquerys = $this->db->query("select upimg_id from tbl_upload_img where zipfile_name='$txtSKUNew' and user_id='$user_id' ")->result_array();
    if(empty($checkquerys))
    {
    $this->upload_model->upload_img_insert($data);
    $upimg_id=$this->db->insert_id();
    }
    else
    {
    $upimg_id=$checkquerys[0]['upimg_id'];
    }



    $allImage=trim($pimg,",");
    $allImageArr=explode(",",$allImage);

    foreach($allImageArr as $key => $file)
    {

        if (!is_dir("./upload/".$user_id."/".$txtSKUNew)) {
       mkdir("./upload/".$user_id."/".$txtSKUNew, 0777, true);
       }

        $source_path="./temp_upload/".$user_id."/".$file;
        $destination_path="./upload/".$user_id."/".$txtSKUNew."/".$file;
        copy($source_path, $destination_path);
        unlink($source_path);

        $thumb_source_path="./temp_upload/".$user_id."/thumb_".$file;
        $thumb_destination_path="./upload/".$user_id."/".$txtSKUNew."/thumb_".$file;
        copy($thumb_source_path, $thumb_destination_path);
        unlink($thumb_source_path);


            $img_name_check=strtoupper($file);

            if(strstr($img_name_check,'FRONT'))
            {
            $img_ang='Front';
            }
            elseif(strstr($img_name_check,'BACK'))
            {
            $img_ang='Back';
            }
            elseif(strstr($img_name_check,'LEFT'))
            {
            $img_ang='LeftSide';
            }
            elseif(strstr($img_name_check,'RIGHT'))
            {
            $img_ang='RightSide';
            }
            elseif(strstr($img_name_check,'TAG'))
            {
            $img_ang='Tag';
            }
            else
            {
            $img_ang='';
            }

        $data = array(
            //'user_id'=>$user_id,
          'upimg_id'=>$upimg_id,
          'img_name'=>$file,
          'img_ang'=>$img_ang,
          );

                   $checkquerys = $this->db->query("select id from tbl_upload_images where upimg_id='$upimg_id' and img_name='$file'  and upimg_id='$upimg_id'")->result_array();
                    if(empty($checkquerys))
                    {
                    $this->upload_model->upload_images_insert($data);
                    $id=$this->db->insert_id();
                    }
                    else
                    {
                     $id=$checkquerys[0]['id'];
                    }
    }


   echo "success";

  }










































public function create()
    {
        $file_name = $this->input->post('file_nm');     
        $uid = $this->input->post('uid');

        $data = array(
            'user_id'   => $uid,
            'zipfile_name'  => $file_name,              
        );
        $this->load->model('Upload_image');
        $insert = $this->upload_image->createData($data);
        echo json_encode($insert);
    }




    public function uploadSKUFolder(){

      
      if ($this->session->userdata('front_logged_in')) {
            $session_data = $this->session->userdata('front_logged_in');
            $data['user_id'] = $session_data['user_id'];
            $data['user_name'] = $session_data['user_name'];
            $user_id=$data['user_id'];
            $folderName=$data['user_id'];

            $upld_skufolder_name_path=$_POST['upld_skufolder_name'];

            $upld_skufolder_name_path_arr=explode("/",$upld_skufolder_name_path);

            $path_count=count($upld_skufolder_name_path_arr);

            if($path_count==3)
            {
              $upld_skufolder_name=$upld_skufolder_name_path_arr[1];
            }
            elseif($path_count==2)
            {
              $upld_skufolder_name=$upld_skufolder_name_path_arr[0];
            }
            //echo $path_count;exit;

            //echo "<pre>";
            //print_r($_FILES);exit;



            $fileName = $_FILES["upld_skufolder"]["name"]; // The file name
            $fileTmpLoc = $_FILES["upld_skufolder"]["tmp_name"]; // File in the PHP tmp folder
            $fileType = $_FILES["upld_skufolder"]["type"]; // The type of file it is
            $fileSize = $_FILES["upld_skufolder"]["size"]; // File size in bytes
            $fileErrorMsg = $_FILES["upld_skufolder"]["error"]; // 0 for false... and 1 for true

            //echo $fileType;
            //exit;
            $ext = strtoupper(pathinfo($fileName, PATHINFO_EXTENSION));

            // if($fileType!='image/jpeg')
            // {
            //  echo "not_image";   
            //  exit;
            // }

            if($ext=='JPG' || $ext=='JPEG' || $ext=='PNG') { }
            else { echo "not_image"; exit; }

            if (!$fileTmpLoc) { // if file not chosen
            echo "ERROR: Please select a file to upload.";
            exit();
            }

            $folder_path="temp_folder_upload/".$user_id."/".$upld_skufolder_name."/";
            if (!file_exists($folder_path)) {
            mkdir($folder_path, 0777, true);
            }


          // if($_FILES["upld_skufolder"]["type"]=='image/jpeg'|| $_FILES["upld_skufolder"]["type"]=='image/jpg')
            if($ext=='JPG' || $ext=='JPEG' )
          {
          $file=$_FILES["upld_skufolder"]["name"];
          move_uploaded_file($fileTmpLoc, $folder_path."/".$fileName);
          }
          else
          {

            $dir = "./temp_folder_upload/".$user_id."/".$upld_skufolder_name."/";

            $img = $_FILES['upld_skufolder']['tmp_name'];

            $file_image=pathinfo($_FILES["upld_skufolder"]["name"], PATHINFO_FILENAME);


            //////////////////////////Convert JPG start//////////////////////////////////////

            $file=$this->convertToJpeg($dir,$img,$file_image);

            //////////////////////////Convert JPG end//////////////////////////////////////

          }


/*          $file_image=pathinfo($_FILES["upld_skufolder"]["name"], PATHINFO_FILENAME);
          $new_thumb = "./temp_folder_upload/".$user_id."/".$upld_skufolder_name."/"."thumb_".$file_image.".jpg";

          $file_pat = "./temp_folder_upload/".$user_id."/".$upld_skufolder_name."/".$file;
          $this->convertToThumb($new_thumb,$user_id,$file_pat);*/
           

            echo $upld_skufolder_name."@@@@@Folder uploaded successfully!";



            } else {
            //echo "aaaaaa";exit;
            //If no session, redirect to login page
            redirect('login', 'refresh');
            }


        
    }





  public function moveSkuFolderUpload(){



  if ($this->session->userdata('front_logged_in')) {
  $session_data = $this->session->userdata('front_logged_in');
  $data['user_id'] = $session_data['user_id'];
  $data['user_name'] = $session_data['user_name'];
  $user_name = $session_data['user_name'];
  $user_id=$data['user_id'];
  $folderName=$data['user_id'];

   $skufolderfile_nm=$this->input->post('skufolderfile_nm');
  $skufolderfile_nm_arr=explode(",",$skufolderfile_nm);

//echo "<pre>";
//print_r($skufolderfile_nm_arr);
  if(!empty($skufolderfile_nm_arr))
  {
    foreach($skufolderfile_nm_arr as $value)
    {
      if($value!='')
      {

      $skufolderfile_nm=$value;




      $target_path="temp_folder_upload/".$user_id."/".$skufolderfile_nm."/";

      $destination_path="upload/".$user_id."/".$skufolderfile_nm."/";




      $data = array(
      'user_id'=>$user_id,
      'zipfile_name'=>$skufolderfile_nm,
      'client_name'=>$user_name,

      );

      $checkquerys = $this->db->query("select upimg_id from tbl_upload_img where zipfile_name='$skufolderfile_nm' and user_id='$user_id' ")->result_array();
      if(empty($checkquerys))
      {
      $this->upload_model->upload_img_insert($data);
      $upimg_id=$this->db->insert_id();
      }
      else
      {
      $upimg_id=$checkquerys[0]['upimg_id'];
      }






        if (!file_exists($destination_path)) {
        mkdir($destination_path, 0777, true);
        }


        // Open a directory, and read its contents
        if (is_dir($target_path)){
        if ($dh = opendir($target_path)){
        while (($file = readdir($dh)) !== false){
        echo "filename:" . $file . "<br>";
        if($file!='.' && $file!='..')
        {
        copy($target_path.$file, $destination_path.$file);
        unlink($target_path.$file);



        $file_image=pathinfo($file, PATHINFO_FILENAME);
        $new_thumb = "./upload/".$user_id."/".$skufolderfile_nm."/"."thumb_".$file_image.".jpg";

        $file_pat = "./upload/".$user_id."/".$skufolderfile_nm."/".$file;
        $this->convertToThumb($new_thumb,$user_id,$file_pat);




        $img_name_check=strtoupper($file);

        if(strstr($img_name_check,'FRONT'))
        {
        $img_ang='Front';
        }
        elseif(strstr($img_name_check,'BACK'))
        {
        $img_ang='Back';
        }
        elseif(strstr($img_name_check,'LEFT'))
        {
        $img_ang='LeftSide';
        }
        elseif(strstr($img_name_check,'RIGHT'))
        {
        $img_ang='RightSide';
        }
        elseif(strstr($img_name_check,'TAG'))
        {
        $img_ang='Tag';
        }
        else
        {
        $img_ang='';
        }


        $ext_item = pathinfo($file, PATHINFO_EXTENSION);
        if(strtoupper($ext_item)=='PNG')
        {
        $file_image_item=str_replace("png","jpg",str_replace("PNG","JPG",$file));
        }
        else
        {
        $file_image_item=$file;
        }



        $data = array(
        //'user_id'=>$user_id,
        'upimg_id'=>$upimg_id,
        'img_name'=>$file_image_item,
        'img_ang'=>$img_ang,
        );

        $checkquerys = $this->db->query("select id from tbl_upload_images where upimg_id='$upimg_id' and img_name='$file'  and upimg_id='$upimg_id'")->result_array();
        if(empty($checkquerys))
        {
        $this->upload_model->upload_images_insert($data);
        $id=$this->db->insert_id();
        }
        else
        {
        $id=$checkquerys[0]['id'];
        }








        }


        }
        closedir($dh);
        }
        }

        rmdir($target_path);
        rmdir("temp_folder_upload/".$user_id."/");

        /*if(copy($target_path, $destination_path)){

        }*/



      }


    }

  }


  




 






  }
  else {
  //echo "aaaaaa";exit;
  //If no session, redirect to login page
  redirect('login', 'refresh');
  }






  }








    function create_tree($path,$skufolderfile_nm_arr) {

   //public $files;
   //private $folder;

   $files = array();  
    
    if( file_exists( $path)) {
      if( $path[ strlen( $path ) - 1 ] ==  '/' )
        $folder = $path;
      else
        $folder = $path . '/';
      
      $dir = opendir( $path );
      while(( $file = readdir( $dir ) ) != false )
      {

       //echo $file;
        if(in_array($file, $skufolderfile_nm_arr))
        {
          echo $file;
          $files[] = $file;
        }
        
      }

      return $files;
      closedir( $dir );
    }

      
    if( count( $files ) > 2 ) { /* First 2 entries are . and ..  -skip them */
      natcasesort( $files );
      $list = '<ul class="filetree" style="display: none;">';
      // Group folders first
      foreach( $files as $file ) {
        if( file_exists( $folder . $file ) && $file != '.' && $file != '..' && is_dir( $folder . $file )) {
          $list .= '<li class="folder collapsed"><a href="#" rel="' . htmlentities( $folder . $file ) . '/">' . htmlentities( $file ) . '</a></li>';
        }
      }
      // Group all files
      foreach( $files as $file ) {
        if( file_exists( $folder . $file ) && $file != '.' && $file != '..' && !is_dir( $folder . $file )) {
          $ext = preg_replace('/^.*\./', '', $file);
          $list .= '<li class="file ext_' . $ext . '"><a href="#" rel="' . htmlentities( $folder . $file ) . '">' . htmlentities( $file ) . '</a></li>';
        }
      }
      $list .= '</ul>'; 
      return $list;
    }
  }



    function Foldertree()
    {
   

    $path = urldecode( $this->input->post('dir'));
    $skufolderfile_nm_field= trim(urldecode( $this->input->post('skufolderfile_nm_field')),",");
    $skufolderfile_nm_arr=explode(",",$skufolderfile_nm_field);
    echo $this->create_tree($path,$skufolderfile_nm_arr);


    }


 
    function openSKUFolder()
    {
    
    $str='';
    $path = urldecode( $this->input->post('path'));
    $foldername= urldecode( $this->input->post('foldername'));
    
    $skufolderfile_nm_field= trim(urldecode( $this->input->post('skufolderfile_nm')),",");
    $skufolderfile_nm_arr=explode(",",$skufolderfile_nm_field);

    if( file_exists( $path)) {
    if( $path[ strlen( $path ) - 1 ] ==  '/' )
    $folder = $path;
    else
    $folder = $path . '/';

    $path=$path.'/'.$foldername;
    $dir = opendir( $path );
    $i = 0;
    while(( $file = readdir( $dir ) ) != false )
    {
        $imgid = $foldername.'_'.$i;
    //echo $file;
    if($file!='.' && $file!='..')
    {
     
     $files[] = $file;

      //$inside_path=

     $str.='<div class="col-12 col-sm-12 col-md-4 col-lg-4 col-xl-4">
            <script type="text/javascript">
            var img = document.getElementById("img_'.$imgid.'");
            var width = img.naturalWidth;
            var height = img.naturalHeight;
            // console.log(width);
            // console.log(height);

            if (width > height){
                $("#rtImg_'.$imgid.'").removeClass("d-none");
            } else {
                $("#orgImg_'.$imgid.'").removeClass("d-none");
            }
            </script>
            <div id="orgImg_'.$imgid.'" class="w-100 d-none">
                <img id="img_'.$imgid.'" class="w-100" src="'.$path.'/'.$file.'" alt="'.$file.'">
            </div>

            <div id="rtImg_'.$imgid.'" class="img-box d-none">
                <img id="img_'.$imgid.'" class="img" src="'.$path.'/'.$file.'" alt="'.$file.'">
            </div>
            </div>';


    //$str.=' <div class="col-12 col-sm-12 col-md-4 col-lg-4 col-xl-4"> <img class="w-100 mb-3" src="'.$path.'/'.$file.'"> </div>';
     }
     $i++;
    }

    echo $str;
     //echo "<pre>";
    // print_r($files);
    //return $files;
    closedir( $dir );
    }


    }

    //remove folder rajendra
    
    function removeSKUFolder()
    {
    
    $str='';
    $path = urldecode( $this->input->post('path'));
    $foldername= urldecode( $this->input->post('foldername'));
    //echo $path."/".$foldername;
    $skufolderfile_nm_field= trim(urldecode( $this->input->post('skufolderfile_nm')),",");
    $skufolderfile_nm_arr=explode(",",$skufolderfile_nm_field);

    $path=$path."/".$foldername;
    if( file_exists($path)) {
        
        
        $files = glob($path . '/*');
 
//Loop through the file list.
foreach($files as $file){
    //Make sure that this is a file and not a directory.
    if(is_file($file)){
        //Use the unlink function to delete the file.
        unlink($file);
    }
}
        
        if(rmdir($path))
        {
          echo ("$foldername successfully removed");
        }
        else
        {
          echo ("$foldername couldn't be removed"); 
        }
        
    }
    else
    {
        echo "error";
    }
    
    }
    //remove folder ends rajendra
    

        //logincheck rajendra
    function logincheck()
    {
       $session_data = $this->session->userdata('front_logged_in');
        $data['user_id'] = $session_data['user_id'];
        $data['user_name'] = $session_data['user_name'];
        $user_name = $session_data['user_name'];
        $user_id=$data['user_id'];
       
        if($user_id=='') { echo "sessionout"; } else { echo "sessionin"; }
    }   
    //logincheck end rajendra
    

    
    function checkSKULengthExistOrNotFolder()
    {

        $session_data = $this->session->userdata('front_logged_in');
        $data['user_id'] = $session_data['user_id'];
        $data['user_name'] = $session_data['user_name'];
        $user_name = $session_data['user_name'];
        $user_id=$data['user_id'];
       
       if($user_id!='')
       { 

       $skufolder = trim( $this->input->post('skufolderfile_nm'),",");

       $skufolder_arr = explode(",",$skufolder);



      $skufolder_arr_temp=$skufolder_arr;
       if(!empty($skufolder_arr))
       {
        $sku_str="";
        foreach($skufolder_arr as $value)
        {
              $skufolder=$value;


           
            $check_path = glob("temp_folder_upload/".$user_id."/".$skufolder."/*.{jpg,jpeg,png,gif,JPG,JPEG}", GLOB_BRACE);
            $directories = glob("temp_folder_upload/".$user_id."/".$skufolder."/*.*", GLOB_BRACE);
            if(count($check_path)!=count($directories))
            {
             echo 'not_image';

              /////////////////////remove dir////////////////////////////////

            if(!empty($skufolder_arr_temp))
            {
            foreach($skufolder_arr as $value)
            {
                 $skufolder=$value;

                 $src = "zip/".$user_id."/".$skufolder; 
                  $this->delete_directory($src); 


            }
            }
             ///////////////////////////////////////////////////
            exit;
            }
           else if(strlen($skufolder)<=2)
            {
             echo 'less_strlen';

               /////////////////////remove dir////////////////////////////////

            if(!empty($skufolder_arr_temp))
            {
            foreach($skufolder_arr as $value)
            {
                $skufolder=$value;
                $src = "temp_folder_upload/".$user_id."/".$skufolder; 
                $this->delete_directory($src); 

                            /*$dirsku = "zip/".$user_id."/".$skufolder."/";
                            // Open a directory, and read its contents
                            if (is_dir($dirsku)){

                            if ($dhsku = opendir($dirsku)){
                            while (($filesku = readdir($dhsku)) !== false){
                               // echo "filesku:" . $filesku . "<br>";
                                if($filesku!='.' && $filesku!='..')
                                {
                                    
                                    unlink($dirsku.$filesku);
                                }
                            
                            
                            }
                            closedir($dhsku);
                             }
                            }
                            rmdir($dirsku);*/
                            

            }
            }
             ///////////////////////////////////////////////////

             exit;
            }
            else
            {
              $sku_str.="'".$skufolder."',";
            }

        }
       }
       

       $sku_str=trim($sku_str,",");


     // echo "select * from tbl_upload_img  where zipfile_name IN (".$skufolder.") and user_id='$user_id' ";

       //exit;
        if($sku_str!='')
        {
        $checkquerys = $this->db->query("select * from tbl_upload_img  where zipfile_name IN (".$sku_str.") and user_id='$user_id' ")->result_array();
        if(!empty($checkquerys))
        {
        echo 'exist';
        exit;
        }
        else
        {
        echo 'notexist';
        exit;
        }
        }

    }
    else
    {
        echo "sessionout";
    }




    }













    function generateSKUForExistFolder()
    {

        $session_data = $this->session->userdata('front_logged_in');
        $data['user_id'] = $session_data['user_id'];
        $data['user_name'] = $session_data['user_name'];
        $user_name = $session_data['user_name'];
        $user_id=$data['user_id'];
        

       $skufolder = trim( $this->input->post('skufolderfile_nm'),",");

       $skufolder_arr = explode(",",$skufolder);

       if(!empty($skufolder_arr))
       {
        $sku_str="";
        foreach($skufolder_arr as $value)
        {


            $skufolder=$value;
            

            $checkquerys = $this->db->query("select * from tbl_upload_img  where user_id='$user_id' and zipfile_name like '%".$skufolder."_%' order by upimg_id desc ")->result_array();
            if(empty($checkquerys))
            {
            $newskufolder=$skufolder."_1";
            }
            else
            {
            $ctn=count($checkquerys)+1;

            $newskufolder=$skufolder."_".$ctn;
            }

            //echo $newskufolder;

            $src_path = "temp_folder_upload/".$user_id."/".$skufolder; 
            $des_path = "temp_folder_upload/".$user_id."/".$newskufolder; 

            rename($src_path,$des_path);

            $sku_str.=$newskufolder.",";
           

        }
       }
       

       echo $sku_str=",".trim($sku_str,",").",";


      



    }



    function deleteCancelSKUForExistFolder()
    {

        $session_data = $this->session->userdata('front_logged_in');
        $data['user_id'] = $session_data['user_id'];
        $data['user_name'] = $session_data['user_name'];
        $user_name = $session_data['user_name'];
        $user_id=$data['user_id'];
        

        $file_nm = $this->input->post('file_nm');

        

        $dir = "temp_folder_upload/".$user_id."/";
        $sku_str="";
        // Open a directory, and read its contents
        if (is_dir($dir)){
            if ($dh = opendir($dir)){
                while (($file = readdir($dh)) !== false){

                    if($file!='.' && $file!='..')
                    {
                    //echo "filename:" . $file . "<br>";
                    $skufolder=$file;

                        $src = "temp_folder_upload/".$user_id."/".$skufolder; 
                        $this->delete_directory($src); 

                    }

                }

            }

        }


  echo "success";
       

    }
































function checkSKULenExistMoveunzipfile()
    {

        $session_data = $this->session->userdata('front_logged_in');
        $data['user_id'] = $session_data['user_id'];
        $data['user_name'] = $session_data['user_name'];
        $user_name = $session_data['user_name'];
        $user_id=$data['user_id'];

        if($user_id!='')
        {
        
        $file_nm = trim( $this->input->post('file_nm'));
        $current_path="zip/".$user_id."/".$file_nm;


     //////////////////////////unzipped/////////////////////////////////
        if(file_exists($current_path))
        {

        
        $unzip = new ZipArchive;
        $out = $unzip->open($current_path);
        if ($out === TRUE) {

        //echo getcwd();
        //$unzip->extractTo(getcwd());
        $unzip->extractTo('zip/'.$user_id.'/');
        $unzip->close();
        //echo 'File unzipped';
        }
        unlink($current_path);
        }

        
     //////////////////////////unzipped/////////////////////////////////

        $dir = "zip/".$user_id."/";
        // Open a directory, and read its contents
        if (is_dir($dir)){

        if ($dh = opendir($dir)){
            $skufolder_arr=array();
        while (($file = readdir($dh)) !== false){
        //echo "filename:" . $file . "<br>";
            if($file!='.' && $file!='..')
            {
            $skufolder_arr[]=$file;
            }
        }
        closedir($dh);
        }
        }

//echo "<pre>";
//print_r($skufolder_arr);
        $skufolder_arr_temp=$skufolder_arr;
       $check= '';
       $sku_str="";
       if(!empty($skufolder_arr))
       {
        
        foreach($skufolder_arr as $value)
        {
            $skufolder=$value;
            
            //echo "zip/".$user_id."/".$skufolder."/*.*";
            $check_path = glob("zip/".$user_id."/".$skufolder."/*.{jpg,jpeg,png,gif,JPG,JPEG}", GLOB_BRACE);
            $directories = glob("zip/".$user_id."/".$skufolder."/*.*", GLOB_BRACE);
            //echo count($check_path)."--".count($directories);exit;
            if(count($check_path)!=count($directories))
            {
             echo 'not_image';

              /////////////////////remove dir////////////////////////////////

            if(!empty($skufolder_arr_temp))
            {
            foreach($skufolder_arr as $value)
            {
                 $skufolder=$value;

                 $src = "zip/".$user_id."/".$skufolder; 
                  $this->delete_directory($src); 


            }
            }
             ///////////////////////////////////////////////////
            exit;
            }
            else if(strlen($skufolder)<=2)
            {
             echo 'less_strlen';

              /////////////////////remove dir////////////////////////////////

            if(!empty($skufolder_arr_temp))
            {
            foreach($skufolder_arr as $value)
            {
                 $skufolder=$value;

                 $src = "zip/".$user_id."/".$skufolder; 
                  $this->delete_directory($src); 


                            /*$dirsku = "zip/".$user_id."/".$skufolder."/";
                            // Open a directory, and read its contents
                            if (is_dir($dirsku)){

                            if ($dhsku = opendir($dirsku)){
                            while (($filesku = readdir($dhsku)) !== false){
                               // echo "filesku:" . $filesku . "<br>";
                                if($filesku!='.' && $filesku!='..')
                                {
                                    
                                    unlink($dirsku.$filesku);
                                }
                            
                            
                            }
                            closedir($dhsku);
                             }
                            }
                            rmdir($dirsku);*/
                            

            }
            }
             ///////////////////////////////////////////////////


             exit;
            }
            else
            {
              $sku_str.="'".$skufolder."',";
            }

        }
       }





       

       $sku_str=trim($sku_str,",");

        if($sku_str!='')
        {

        //echo "select * from tbl_upload_img  where zipfile_name IN (".$sku_str.") and user_id='$user_id' ";

        //exit;
        $checkquerys = $this->db->query("select * from tbl_upload_img  where zipfile_name IN (".$sku_str.") and user_id='$user_id' ")->result_array();
        if(!empty($checkquerys))
        {
        echo 'exist';
        exit;
        }
        else
        {
        echo 'notexist';
        exit;
        }

        }

    }
    else
    {
        echo "sessionout";
    }


    }





    function generateSKUForExistZip()
    {

        $session_data = $this->session->userdata('front_logged_in');
        $data['user_id'] = $session_data['user_id'];
        $data['user_name'] = $session_data['user_name'];
        $user_name = $session_data['user_name'];
        $user_id=$data['user_id'];
        

        $file_nm = $this->input->post('file_nm');

        $current_path="zip/".$user_id."/";

        $dir = "zip/".$user_id."/";
         $sku_str="";
        // Open a directory, and read its contents
        if (is_dir($dir)){
        if ($dh = opendir($dir)){
        while (($file = readdir($dh)) !== false){

            if($file!='.' && $file!='..')
            {
            //echo "filename:" . $file . "<br>";
            $skufolder=$file;

            $checkquerys = $this->db->query("select * from tbl_upload_img  where user_id='$user_id' and zipfile_name like '%".$skufolder."_%' order by upimg_id desc ")->result_array();
            if(empty($checkquerys))
            {
            $newskufolder=$skufolder."_1";
            }
            else
            {
            $ctn=count($checkquerys)+1;

            $newskufolder=$skufolder."_".$ctn;
            }

            //echo $newskufolder;

            $src_path = "zip/".$user_id."/".$skufolder; 
            $des_path = "zip/".$user_id."/".$newskufolder; 

            rename($src_path,$des_path);

            $sku_str.=$newskufolder.",";

            }

          }
         }
        }

       

  echo $sku_str;

      



    }









    function deleteCancelSKUForExistZip()
    {

        $session_data = $this->session->userdata('front_logged_in');
        $data['user_id'] = $session_data['user_id'];
        $data['user_name'] = $session_data['user_name'];
        $user_name = $session_data['user_name'];
        $user_id=$data['user_id'];
        

        $file_nm = $this->input->post('file_nm');

        

        $dir = "zip/".$user_id."/";
        $sku_str="";
        // Open a directory, and read its contents
        if (is_dir($dir)){
            if ($dh = opendir($dir)){
                while (($file = readdir($dh)) !== false){

                    if($file!='.' && $file!='..')
                    {
                    //echo "filename:" . $file . "<br>";
                    $skufolder=$file;

                        $src = "zip/".$user_id."/".$skufolder; 
                        $this->delete_directory($src); 

                    }

                }

            }

        }


  echo "success";
       

    } 

}

?>