<?php 
class Booking extends CI_Controller{
	
	function __construct(){
		parent::__construct();
		$this->load->library(['session']); 
		$this->load->helper(['url','file','form']);
		$this->load->model('Booking_model');
		
	}

	public function index($tmb_book_id=""){
		//print_r("Temp booking".$tmb_book_id);
		if ($this->session->userdata('front_logged_in')) {
			$session_data = $this->session->userdata('front_logged_in');
			$data['user_id'] = $session_data['user_id'];
			$data['user_name'] = $session_data['user_name'];
			if(!empty($tmb_book_id))
			{
				$data['book_id']=$tmb_book_id;
				$data['booking_data']= $this->db->query("select * from tbl_tmp_booking where book_id=$tmb_book_id;")->result_array();
				$this->load->view('front/booking',$data );
			}
			else
			{
				$this->load->view('front/booking',$data );
			}
			
			

		}
		else
		{
			redirect('/');
		}

		
	}

	public function add_book($tmb_book_id=""){
		if(!empty($tmb_book_id))
		{
			// echo"oooooo";exit();
			$created_date = date('Y-m-d');
			
			$order_id=$this->input->post('order_id');
			$user_id=$this->input->post('user_id');
			$gender=$this->input->post('gender');
			$m_status=$this->input->post('m_status');
			$category=",".implode(",",$this->input->post('cat')).",";	
			$quty=",".implode(",",$this->input->post('quty')).",";
			//echo $this->input->post('angle_Top');exit();
			if($this->input->post('angle_Top')!="")
			{
				$angle =",".implode(",",$this->input->post('angle_Top')).",";
			//	print_r($this->input->post('angle_Top'));  exit();
			}

			if($this->input->post('angle_Bottom')!="")
			{
				$angle .=implode(",",$this->input->post('angle_Bottom')).",";
			//	print_r($this->input->post('angle_Top'));  exit();
			}

			if($this->input->post('angle_Dress')!="")
			{
				$angle .=implode(",",$this->input->post('angle_Dress')).",";
			//	print_r($this->input->post('angle_Top'));  exit();
			}
			// $category1=$this->input->post('category');	
			// $quty1=$this->input->post('quty');
			// $angle1=$this->input->post('angle');
			$d_status=$this->input->post('d_status');
			$notes=$this->input->post('notes');
			$total_price=$this->input->post('total_price');
			$p_status="pending";
			$data = array(
				'user_id'=>$user_id,
				'order_id'=>$order_id,
				'gender'=>$gender,
				'category'=>$category,
				'quty'=>$quty,
				'm_status'=>$m_status,
				'angle'=>$angle,
				'd_status'=>$d_status,
				'notes'=>$notes,
				'b_status'=>"1",
				'total_price'=>$total_price,
				'p_status'=>$p_status,
				'create_date'=>$created_date,
			);
			//print_r($data); exit();
			//$this->load->view('front/revbook',$data );
			$result=$this->Booking_model->update_booking($data,$tmb_book_id);
			
			//$tmb_book_id=$this->db->insert_id();					
			echo $tmb_book_id;
		}
		else{
			// echo"oooooo";exit();
			$created_date = date('Y-m-d');
			
			$order_id=$this->input->post('order_id');
			$user_id=$this->input->post('user_id');
			$gender=$this->input->post('gender');
			$m_status=$this->input->post('m_status');
			// print_r($this->input->post('cat'));
			$category=",".implode(",",$this->input->post('cat')).",";
			$quty=",".implode(",",$this->input->post('quty')).",";
			
			if($this->input->post('angle_Top')!="")
			{
				$angle =",".implode(",",$this->input->post('angle_Top')).",";
			//	print_r($this->input->post('angle_Top'));  exit();
			}

			if($this->input->post('angle_Bottom')!="")
			{
				$angle .=implode(",",$this->input->post('angle_Bottom')).",";
			//	print_r($this->input->post('angle_Top'));  exit();
			}

			if($this->input->post('angle_Dress')!="")
			{
				$angle .=implode(",",$this->input->post('angle_Dress')).",";
			//	print_r($this->input->post('angle_Top'));  exit();
			}
				//print_r($angle);
			// $category1=$this->input->post('category');	
			// $quty1=$this->input->post('quty');
			// $angle1=$this->input->post('angle');
			$d_status=$this->input->post('d_status');
			$notes=$this->input->post('notes');
			$total_price=$this->input->post('total_price');
			$p_status="pending";
			$data = array(
				'user_id'=>$user_id,
				'order_id'=>$order_id,
				'gender'=>$gender,
				'category'=>$category,
				'quty'=>$quty,
				'm_status'=>$m_status,
				'angle'=>$angle,
				'd_status'=>$d_status,
				'notes'=>$notes,
				'b_status'=>"1",
				'total_price'=>$total_price,
				'p_status'=>$p_status,
				'create_date'=>$created_date,
			);
			//print_r($data); exit();
			//$this->load->view('front/revbook',$data );
			$result=$this->Booking_model->add_booking($data);
			
			$tmb_book_id=$this->db->insert_id();					
			echo $tmb_book_id;
		}
	}
	
}

?>