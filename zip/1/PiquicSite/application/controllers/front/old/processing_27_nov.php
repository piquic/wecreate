<?php 
class Processing extends CI_Controller{
	
	function __construct(){
		parent::__construct();
		$this->load->library(['session']); 
        $this->load->helper(['url','file','form']); 
		$this->load->model('upload_model'); //load model upload 
		$this->load->model('home_model'); //load model upload 
		$this->load->library('upload'); //load library upload 
	}

	public function index(){


		/*$data['title'] = "upload images";
		$this->load->view('inc/header', $data);
		$this->load->view('inc/menu');		
		$this->load->view('inc/nav');*/
		$this->load->view('front/processing');
		/*$this->load->view('inc/footer');*/
	}



	public function processing_list(){
		
		if ($this->session->userdata('front_logged_in')) 
        {
			$session_data=$this->session->userdata('logged_in');
			$usertype = $session_data['rolecode'];
			$userid = $session_data['id'];
			
    $aColumns = array( 'sty_id','user_id','skuno','category','stage','status','comments','status_type');
         
			/* Indexed column (used for fast and accurate table cardinality) */
			$sIndexColumn = "sty_id";
         
			/* DB table to use */
			$sTable = "tbl_style";

            /*
			 * Paging
			 */
			$sLimit = "";
			if ( isset( $_GET['start'] ) && $_GET['length'] != '-1' )
			{
				$sLimit = "LIMIT ".intval( $_GET['start'] ).", ".
					intval( $_GET['length'] );
			}


    
     
			$sOrder = "";
			if ( isset( $_GET['order'] ) )
			{
				$sOrder = "ORDER BY  ";
			
						$sOrder .= $aColumns[0]."
							".($_GET['order'][0]['dir']==='desc' ? 'asc' : 'desc') .", ";
			   
				 
				$sOrder = substr_replace( $sOrder, "", -2 );
				if ( $sOrder == "ORDER BY" )
				{
					$sOrder = "";
				}
			}
	

        
        $commn="1=1 and status='2' ";
        
		
		//echo $_REQUEST['columns'][1]['search']['value']; exit();
		
		if(!empty($_GET['columns'][1]['search']['value']) ){  
	
		$commn.=" AND tbl_style.skuno  LIKE '".$_GET['columns'][1]['search']['value']."%' ";
		}

        $sWhere="where $commn";
         
		
		if (isset($_GET['search']['value']) && $_GET['search']['value'] != "" )
		{
			 $sWhere = "WHERE $commn   AND (";
			for ( $i=0 ; $i<count($aColumns) ; $i++ )
			{
				
				/*if ( isset($_GET['columns'][$i]['searchable']) && $_GET['columns'][$i]['searchable'] == "true" )
				{*/
					$sWhere .= $aColumns[$i]." LIKE '%".( $_GET['search']['value'] )."%' OR ";
				/*}*/
			}
			$sWhere = substr_replace( $sWhere, "", -3 );
			$sWhere .= ')';
		}
		
		
       $join='';
        /*
         * SQL queries
         * Get data to display
         */
        $sQuery = "
            SELECT SQL_CALC_FOUND_ROWS ".str_replace(" , ", " ", implode(", ", $aColumns))."
            FROM   $sTable $join
            $sWhere
            $sOrder
            $sLimit";

       //echo  $sQuery; exit();

        $rResult = $this->db->query($sQuery);
        $rResult = $rResult->result_array();
        //$rResult = mysqli_query( $sQuery) or fatal_error( 'MySQL Error: ' . mysqli_errno() );

        
        /* Data set length after filtering */
        //$sQuery = "SELECT FOUND_ROWS()";
        //$rResultFilterTotal = mysqli_query( $sQuery) or fatal_error( 'MySQL Error: ' . mysqli_errno() );
        //$query = $this->db->query($sQuery);
        //$aResultFilterTotal = $query->result_array();
        //$aResultFilterTotal = mysqli_fetch_array($rResultFilterTotal);
       $iFilteredTotal = count( $rResult);
          
        /* Total data set length */
        $sQuery = "
            SELECT COUNT(".$sIndexColumn.") as count
            FROM   $sTable
        ";
        //$rResultTotal = mysqli_query( $sQuery) or fatal_error( 'MySQL Error: ' . mysqli_errno() );
        //$aResultTotal = mysqli_fetch_array($rResultTotal);

        $query = $this->db->query($sQuery);
        $aResultTotal = $query->result_array();

        //echo "<pre>";
        //print_r($aResultTotal);
         $iTotal = $aResultTotal[0]['count'];
         
        /*
         * Output
         */
        $output = array(
                        "sEcho" => '',
                        "iTotalRecords" => $iFilteredTotal,
                        "iTotalDisplayRecords" => $iTotal,
                        "aaData" => array()
                        );

            $sColumns = array('sty_id','skuno','category','stage','comments');
         
            //while ( $aRow = mysqli_fetch_array( $rResult ) )
            //echo "<pre>";
           //print_r($output);
         
            foreach($rResult as $aRow)
            {
                $row = array();
            	
				$id=$aRow['sty_id'];
				$status=$aRow['status'];
				$status_type=$aRow['status_type'];
				for ( $i=0 ; $i<count($sColumns) ; $i++ )
                {
                    if($sColumns[$i] =='action')
            		{
            			

						$row[]='<a href="javascript:void(0)"  class="btn btn-success view" val="'.$id.'"><i class="fa fa-fw fa-eye"></i></a>&nbsp;<a href="javascript:void(0)"  class="btn btn-info edit" val="'.$id.'"><i class="fa fa-edit"></i></a>&nbsp; <a href="javascript:void(0)"  class="btn btn-danger delete" val="'.$id.'"><i class="fa fa-trash-o"></i></a>';
						
            		}
            		if($sColumns[$i] =='sty_id')
            		{
            			
                        if(strtoupper($status_type)=='REJECT')
                        {
					$row[]='<span class="text-danger"><i class="fas fa-exclamation-circle"></i></span>';
                        }
                        else
                        {
                        	$row[]='<span class="text-info"><i class="fas fa-sync-alt"></i></span>';
                        }
						
						
            		}
            		else if ( $sColumns[$i] != '' )
                    {
                        /* General output */
                        $row[] = $aRow[ $sColumns[$i] ];
                    }
                }
                $output['aaData'][] = $row;
            }
            //print_r($output);exit;
        echo json_encode( $output );
    }
	}
	





public function uploadimage(){


    	if ($this->session->userdata('front_logged_in')) {
            $session_data = $this->session->userdata('front_logged_in');
            $data['user_id'] = $session_data['user_id'];
            $data['user_name'] = $session_data['user_name'];
            
			$id='3';
			$data['pagecontents'] = $this->home_model->get_page($id);
			$this->load->view('front/uploadimage', $data);    
			
         
        } else {
        	//echo "aaaaaa";exit;
            //If no session, redirect to login page
            //redirect('login', 'refresh');
            $session_data = $this->session->userdata('front_logged_in');
            $data['user_id'] = $session_data['user_id'];
            $data['user_name'] = $session_data['user_name'];
            
			$id='3';
			$data['pagecontents'] = $this->home_model->get_page($id);
			$this->load->view('front/uploadimage', $data);    
        }
			
			
			
			
		}



    public function uploadfile(){

    	if ($this->session->userdata('front_logged_in')) {
            $session_data = $this->session->userdata('front_logged_in');
            $data['user_id'] = $session_data['user_id'];
            $data['user_name'] = $session_data['user_name'];
            $folderName=$data['user_id'];


            
			    $fileName = $_FILES["upld_zip"]["name"]; // The file name
				$fileTmpLoc = $_FILES["upld_zip"]["tmp_name"]; // File in the PHP tmp folder
				$fileType = $_FILES["upld_zip"]["type"]; // The type of file it is
				$fileSize = $_FILES["upld_zip"]["size"]; // File size in bytes
				$fileErrorMsg = $_FILES["upld_zip"]["error"]; // 0 for false... and 1 for true
				if (!$fileTmpLoc) { // if file not chosen
				echo "ERROR: Please select a file to upload.";
				exit();
				}
                
                $folder_path="zip/$folderName";
				if (!file_exists($folder_path)) {
				mkdir($folder_path, 0777, true);
				}
				if(move_uploaded_file($fileTmpLoc, "$folder_path/$fileName")){
				echo "ZIP file uploaded successfully!";

				} else {
				echo "ZIP file upload failed!";
				} 
			
         
        } else {
        	//echo "aaaaaa";exit;
            //If no session, redirect to login page
            redirect('login', 'refresh');
        }
			
			
			
				
		}


   
    public function moveunzipfile(){


    	if ($this->session->userdata('front_logged_in')) {
            $session_data = $this->session->userdata('front_logged_in');
            $data['user_id'] = $session_data['user_id'];
            $data['user_name'] = $session_data['user_name'];
            $user_id=$data['user_id'];
             $folderName=$data['user_id'];

            $file_nm=$this->input->post('file_nm');
            $current_path="zip/$folderName/$file_nm";

			$unzip = new ZipArchive;
			$out = $unzip->open($current_path);
			if ($out === TRUE) {

			//echo getcwd();
			//$unzip->extractTo(getcwd());
			$unzip->extractTo('zip/'.$folderName.'/');
			$unzip->close();
			echo 'File unzipped';
			

             //////////////////////////////////////////////////////////////////
            $directories = glob('zip/'.$folderName . '/*' , GLOB_ONLYDIR);

            //echo "<pre>";
            //print_r( $directories);exit;

            foreach($directories as $dir)
            {
            	
                $dir_path=str_replace('zip/'.$folderName.'/', '', $dir);


                    $data = array(
				    'user_id'=>$user_id,
					'zipfile_name'=>$dir_path,
					);

                    $checkquerys = $this->db->query("select upimg_id from tbl_upload_img where zipfile_name='$dir_path'")->result_array();
                    if(empty($checkquerys))
                    {
                    $this->upload_model->upload_img_insert($data);
					$upimg_id=$this->db->insert_id();
                    }
                    else
                    {
                     $upimg_id=$checkquerys[0]['upimg_id'];
                    }
					



                echo $sub_path='zip/'.$folderName . '/'.$dir_path . '/';

                $files = array_diff(scandir($sub_path), array('.', '..'));

             /* echo "<pre>";
              print_r( $files);
              exit;*/

				foreach($files as $file)
				{

					$data = array(
				    //'user_id'=>$user_id,
				    'upimg_id'=>$upimg_id,
					'img_name'=>$file,
					);

                   $checkquerys = $this->db->query("select id from tbl_upload_images where upimg_id='$upimg_id' and img_name='$file'")->result_array();
                    if(empty($checkquerys))
                    {
                    $this->upload_model->upload_images_insert($data);
					$id=$this->db->insert_id();
                    }
                    else
                    {
                     $id=$checkquerys[0]['id'];
                    }
					

					



				}



            }

///////////////////////////////////////////////////////////////////////////////////////////
             $current_path="zip/$folderName/$file_nm";
			 $target_path="upload/$file_nm";
			if(copy($current_path, $target_path)){
				echo "ZIP file moved successfully!";

				
				$unzip = new ZipArchive;
				$out = $unzip->open($target_path);
				if ($out === TRUE) {

					//echo getcwd();
				  //$unzip->extractTo(getcwd());
				  $unzip->extractTo('upload/');
				  $unzip->close();
				  //echo 'File unzipped';
				  //unlink($current_user_path);

				   unlink($current_path);
				   unlink($target_path);




							$directories = glob('zip/'.$folderName . '/*' , GLOB_ONLYDIR);

							//echo "<pre>";
							//print_r( $directories);exit;

							foreach($directories as $dir)
							{
							//echo "---".$dir."---";

								$files = glob($dir.'/*'); // get all file names
								foreach($files as $file){ // iterate files
								if(is_file($file))
								unlink($file); // delete file
								}

								rmdir($dir);

							}

                             rmdir('zip/'.$folderName);



				} else {
				  echo 'Error';
				}





				} else {
				echo "ZIP file moved failed!";
				}
            

			    
			   	
					
	
///////////////////////////////////////////////////////////////////////////////////////////				

             //////////////////////////////////////////////////////////////////
			} else {
			echo 'Error';
			}





			
         
        } else {
        	//echo "aaaaaa";exit;
            //If no session, redirect to login page
            redirect('login', 'refresh');
        }
			
			
				
		}















































public function create()
	{
		$file_name = $this->input->post('file_nm');		
		$uid = $this->input->post('uid');

		$data = array(
			'user_id'	=> $uid,
			'zipfile_name'	=> $file_name,				
		);
		$this->load->model('Upload_image');
		$insert = $this->upload_image->createData($data);
		echo json_encode($insert);
	}
	

}

?>