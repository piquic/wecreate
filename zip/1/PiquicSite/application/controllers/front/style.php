<?php 
class Style extends CI_Controller{
	
	function __construct(){
		parent::__construct();
		$this->load->library(['session']); 
		$this->load->database();
		$this->load->helper(['url','file','form']);
		$this->load->helper('directory');
		$this->load->helper('file'); 
		$this->load->model('style_model');
		
	}

	public function styleview($skuno){

		/*$query = $this->db->get("tbl_users");
		echo "<pre>";
		print_r($query->result());


		$second_DB = $this->load->database('another_db', TRUE); 
		$query2 = $second_DB->get("tbl_model");
		print_r($query2->result());
		exit;*/

		
       // echo $skuno;
		$result['skuno']=$skuno;
		
		$result['imginfo']=$this->style_model->styleimg($skuno);
		$result['imagesinfo']=$this->style_model->styleimages($skuno);
		$result['styleinfo']=$this->style_model->getStyleDetail($skuno);


        $result['modelinfo']=$this->style_model->getAllModel($skuno);


	    $this->load->view('front/style',$result);

	    //$this->load->database();




		
	}


	
  public function styleframeview($skuno,$selecttype){

		/*$query = $this->db->get("tbl_users");
		echo "<pre>";
		print_r($query->result());


		$second_DB = $this->load->database('another_db', TRUE); 
		$query2 = $second_DB->get("tbl_model");
		print_r($query2->result());
		exit;*/

		
       // echo $skuno;
		$result['skuno']=$skuno;
		$result['selecttype']=$selecttype;
		
		$result['imginfo']=$this->style_model->styleimg($skuno);
		$result['imagesinfo']=$this->style_model->styleimages($skuno);
		$result['styleinfo']=$this->style_model->getStyleDetail($skuno);


        $result['modelinfo']=$this->style_model->getAllModel($skuno);


	    $this->load->view('front/styleframe',$result);

	    //$this->load->database();




		
	}


 

}
?>