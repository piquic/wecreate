<?php 
class Labelimages extends CI_Controller{
	
	function __construct(){
	parent::__construct();
	$this->load->library(['session']); 
	$this->load->database();
	$this->load->helper(['url','file','form']);
	$this->load->helper('directory');
	$this->load->helper('file'); 
	$this->load->model('Labelimg_model');

	}

	public function index($sku_key=""){


    if ($this->session->userdata('front_logged_in')) {
    $session_data = $this->session->userdata('front_logged_in');
    $data['user_id'] = $session_data['user_id'];
    $data['user_name'] = $session_data['user_name'];
    }
    else
    {
    $data['user_id'] = '';
    $data['user_name'] = '';
    }
    $data['sku_key'] = $sku_key;

//echo $sku;

		/*$data['title'] = "label images";
		$this->load->view('inc/header', $data);
		$this->load->view('inc/menu');		
		$this->load->view('inc/nav');*/
		$data['labelimagesinfo']=$this->Labelimg_model->labelimages($sku_key);
	    $this->load->view('front/labelimages',$data);
		//$this->load->view('inc/footer');
	}


    public function label_select_ajax($sku_key=""){


    if ($this->session->userdata('front_logged_in')) {
    $session_data = $this->session->userdata('front_logged_in');
    $data['user_id'] = $session_data['user_id'];
    $data['user_name'] = $session_data['user_name'];
    }
    else
    {
    $data['user_id'] = '';
    $data['user_name'] = '';
    }
    $data['sku_key'] = $sku_key;

    $sku_select=$this->input->post('sku_select');   
    $upimg_id="";
    //echo $upimg_id."zzzzzzzzz";
    $data['sku_select'] = $sku_select;
    /*$data['title'] = "label images";
    $this->load->view('inc/header', $data);
    $this->load->view('inc/menu');    
    $this->load->view('inc/nav');*/
    $data['labelimagesinfo']=$this->Labelimg_model->labelimages_ajax($sku_key,$upimg_id);
    $this->load->view('front/labelimages_select_ajax',$data);
    //$this->load->view('inc/footer');
  }

    public function label_ajax($sku_key=""){


    if ($this->session->userdata('front_logged_in')) {
    $session_data = $this->session->userdata('front_logged_in');
    $data['user_id'] = $session_data['user_id'];
    $data['user_name'] = $session_data['user_name'];
    }
    else
    {
    $data['user_id'] = '';
    $data['user_name'] = '';
    }
    $data['sku_key'] = $sku_key;

    $upimg_id=$this->input->post('upimg_id');  
    $sku_select=$this->input->post('sku_select');    
    $data['sku_select'] = $sku_select;
    //echo $upimg_id."zzzzzzzzz";

    /*$data['title'] = "label images";
    $this->load->view('inc/header', $data);
    $this->load->view('inc/menu');    
    $this->load->view('inc/nav');*/
    $data['labelimagesinfo']=$this->Labelimg_model->labelimages_ajax($sku_key,$upimg_id);
    $this->load->view('front/labelimages_ajax',$data);
    //$this->load->view('inc/footer');
  }

function update(){
      $data=$this->Labelimg_model->update_skuinfo();
        echo json_encode($data);
    }

    function genupdate(){
      $data=$this->Labelimg_model->update_genskuinfo();
        echo json_encode($data);
    }

function delete(){
	     $skuno=$this->input->post('skuno');
      $data=$this->Labelimg_model->delete_sku($skuno);
        echo json_encode($data);
    }

function mulcatupdate(){	    
     $skuno=$this->input->post('skuno');
     $category=$this->input->post('category');   
   
       $data=$this->Labelimg_model->updatemult_catskuinfo($skuno,$category);
       echo json_encode($data);
    }

    function mulgenupdate(){
    $skuno=$this->input->post('skuno');
    $gender=$this->input->post('gender');	      
    $data=$this->Labelimg_model->updatemult_genskuinfo($skuno,$gender);
    echo json_encode($data);
    }


  function oneupload(){

 if ($this->session->userdata('front_logged_in')) {
    $session_data = $this->session->userdata('front_logged_in');
    $user_id = $session_data['user_id'];
    $user_name = $session_data['user_name'];

    $time=time();
    $skuno=$this->input->post('sku_data');
    $user_id=$this->input->post('user_id');
    $file_name=$time.$_FILES["upld_img_SKU"]["name"];
    $file_tmp=$_FILES["upld_img_SKU"]["tmp_name"];
    $ext=pathinfo($file_name,PATHINFO_EXTENSION);

    
    $Fpsdimg=$file_name;
    $targetDirpsd= './upload/'.$user_id.'/'.$skuno.'/';  
        //echo $targetDirpsd.$Fpsdimg;  

       // echo $_FILES["upld_img_SKU"]["type"];exit; 
    if(is_dir($targetDirpsd))
    {

   if($_FILES["upld_img_SKU"]["type"]=='image/jpeg'|| $_FILES["upld_img_SKU"]["type"]=='image/jpg' || $_FILES["upld_img_SKU"]["type"]=='image/png')
   {      

  if($_FILES["upld_img_SKU"]["type"]=='image/jpeg'|| $_FILES["upld_img_SKU"]["type"]=='image/jpg')
  {
   //move_uploaded_file($file_tmp=$_FILES["upld_img_SKU"]["tmp_name"],$targetDirpsd.$Fpsdimg);
    move_uploaded_file($_FILES["upld_img_SKU"]["tmp_name"],$targetDirpsd.$Fpsdimg);
    $file=$file_name;
  }
  else
  {


  $dir = './upload/'.$user_id.'/'.$skuno.'/';  

  $img = $_FILES['upld_img_SKU']['tmp_name'];

  $file_image=pathinfo($time.$_FILES["upld_img_SKU"]["name"], PATHINFO_FILENAME);


  //////////////////////////Convert JPG start//////////////////////////////////////

  $file=$this->convertToJpeg($dir,$img,$file_image);

  //////////////////////////Convert JPG end//////////////////////////////////////

  } 
 $file_image=pathinfo($time.$_FILES["upld_img_SKU"]["name"], PATHINFO_FILENAME);
 $new_thumb = "./upload/".$user_id."/".$skuno."/"."thumb_".$file_image.".jpg";

 $file_pat = "./upload/".$user_id."/".$skuno."/".$file;
 $this->convertToThumb($new_thumb,$user_id,$file_pat);
   
  $file_name=$file_image.".jpg";


   $data = $this->Labelimg_model->insert_file($file_name,$skuno);
   // echo json_encode($data);

   echo $data;

  }
  else
  {
    echo "0";
  }
}
else
{
  echo "error";
}

}


else
{
  echo "sessionout";
}



   }

function imgangchng(){	    
     $id=$this->input->post('id');
      $imgid=$this->input->post('imgid');
      $selval=$this->input->post('selval'); 
      $img=$this->input->post('img'); 


   
       $data=$this->Labelimg_model->updateimg_anginfo($id,$selval,$img,$imgid);
        // echo json_encode($data);

       echo $data;
    }


function sendforstyle(){      
     $sku=$this->input->post('sku');
     $cat=$this->input->post('cat');
     $gen=$this->input->post('gen');
     $data=$this->Labelimg_model->updatestyle_queinfo($sku,$cat,$gen);
      echo json_encode($data);
    }


  public function upload_files_fromsku(){

     if ($this->session->userdata('front_logged_in')) {
    $session_data = $this->session->userdata('front_logged_in');
    $data['user_id'] = $session_data['user_id'];
    $data['user_name'] = $session_data['user_name'];

    $user_id=$session_data['user_id'];


        //echo "<pre>";
    //print_r($_FILES);

    $timestamp=time();
   
    if (!is_dir("./temp_upload/".$user_id)) {
    mkdir("./temp_upload/".$user_id, 0777, true);
    }
    $dir = "./temp_upload/".$user_id."/";
    //$file=$timestamp.$_FILES["image"]["name"];
    $file=$_FILES["image"]["name"];



    if($_FILES["image"]["type"]=='image/jpeg'|| $_FILES["image"]["type"]=='image/jpg')
    {
       move_uploaded_file($_FILES["image"]["tmp_name"], $dir. $file);
    }
    else
    {
      

        $dir = "./temp_upload/".$user_id."/";

        $img = $_FILES['image']['tmp_name'];

        //$file_image=$timestamp.pathinfo($_FILES["image"]["name"], PATHINFO_FILENAME);
        $file_image=pathinfo($_FILES["image"]["name"], PATHINFO_FILENAME);


        //////////////////////////Convert JPG start//////////////////////////////////////

        $file=$this->convertToJpeg($dir,$img,$file_image);

       //////////////////////////Convert JPG end//////////////////////////////////////

    }



 //$file_image=$timestamp.pathinfo($_FILES["image"]["name"], PATHINFO_FILENAME);
 $file_image=pathinfo($_FILES["image"]["name"], PATHINFO_FILENAME);
 $new_thumb = "./temp_upload/".$user_id."/"."thumb_".$file_image.".jpg";

 $file_pat = "./temp_upload/".$user_id."/".$file;
 $this->convertToThumb($new_thumb,$user_id,$file_pat);




    echo ltrim($file);






    }
    else
    {
    $data['user_id'] = '';
    $data['user_name'] = '';
    $user_id='';

    echo "sessionout";
    }






  }

public function convertToThumb($new_thumb,$user_id,$file_path) {

  
  if(!file_exists($new_thumb)){
  $width = "400";
  $height = "600";
  $quality = 90;
  $img = $file_path;

  //Generate Thumbnail Images

  $file = $img;
  $dest = $new_thumb;
  $height = 600;
  $width = 400;
  $output_format = "jpg";
  $output_quality = 90;
  $bg_color = array(255, 255, 255);


  //Justify the Image format and create a GD resource
  $image_info = getimagesize($file);
  list($cur_width, $cur_height, $cur_type, $cur_attr) = getimagesize($file);




  $image_type = $image_info[2];
  switch($image_type){
    case IMAGETYPE_JPEG:
    $image = imagecreatefromjpeg($file);
    break;
    case IMAGETYPE_GIF:
    $image = imagecreatefromgif($file);
    break;
    case IMAGETYPE_GIF:
    $image = imagecreatefrompng($file);
    break;
    default:
    /////////////////changes//////////////////////////
    die("notsupport");
  }

 if($cur_width>$cur_height){
  $degrees = -90;
  $image = imagerotate($image, $degrees, 0);
  }

  $image_width = imagesx($image);
  $image_height = imagesy($image);




  //echo $file;

  //Get The Calculations
  // Calculate the size of the Image
  //If Image width is bigger than the Thumbnail Width
  if($image_width>$image_height){





//echo "hoko";
//echo $image_width.">".$image_height;

        //Set Image Width to Thumbnail Width
    $new["width"] = $width;
        //Calculate Height according to width
    $new["height"] = ($new["width"]/$image_width)*$image_height;

        //If Resulting height is bigger than the thumbnail Height
    if($new["height"]>$height){

            //Set the image Height to THUmbnail Height
      $new["height"] = $height;
            //Recalculate width according to height of the thumbnail
      $new["width"] = ($new["height"]/$image_height)*$image_width;

    }

  }else{
//echo "moko";
    $new["height"] = $height;
    $new["width"] = ($new["height"]/$image_height)*$image_width;

    if($new["width"]>$width){

      $new["width"] = $width;
      $new["height"] = ($new["width"]/$image_width)*$image_height;

    }

  }

    //Calculate the image position based on the difference between the dimensons of the new image and thumbnail
  $x = ($width-$new["width"])/2;
  $y = ($height-$new["height"])/2;

  $calc =  array_merge($new, array("x"=>$x,"y"=>$y));


  // End Calculate The Image



  //Create an Empty image
  $canvas = imagecreatetruecolor($width, $height);

    //Load Background color
  $color = imagecolorallocate($canvas,
    $bg_color[0],
    $bg_color[1],
    $bg_color[2]
  );

    //FIll the Image with the Background color
  imagefilledrectangle(
    $canvas,
    0,
    0,
    $width,
    $height,
    $color
  );

    //The REAL Magic
  imagecopyresampled(
    $canvas,
    $image,
    $calc["x"],
    $calc["y"],
    0,
    0,
    $calc["width"],
    $calc["height"],
    $image_width,
    $image_height
  );

// Create Output Image

  $image = $canvas;
  $format = $output_format;
  $quality = $output_quality;



  switch($format){
    case "jpg":
    imagejpeg($image, $dest, $quality);
    break;
    case "gif":
    imagegif($image, $dest);
    break;
    case "png":
            //Png Quality is measured from 1 to 9
    imagepng($image, $dest, round(($quality/100)*9) );
    break;
  }

  //unlink($img);

}
else{
  echo "file is exist.";
}


}

public function convertToJpeg($dir,$img,$file_image) {

        $dst = $dir . $file_image;

        if (($img_info = getimagesize($img)) === FALSE)
        die("Image not found or not an image");

        $width = $img_info[0];
        $height = $img_info[1];

        switch ($img_info[2]) {
        case IMAGETYPE_GIF  : $src = imagecreatefromgif($img);  break;
        case IMAGETYPE_JPEG : $src = imagecreatefromjpeg($img); break;
        case IMAGETYPE_PNG  : $src = imagecreatefrompng($img);  break;
        default : die("Unknown filetype");
        }

        $tmp = imagecreatetruecolor($width, $height);

        $file =$dst.".jpg";

        imagecopyresampled($tmp, $src, 0, 0, 0, 0, $width, $height, $width, $height);
        imagejpeg($tmp, $file);
        return $file_image.".jpg";

}


function deleteUploadImages(){
       $imgid=$this->input->post('imgid');
       $sku=$this->input->post('sku');
       $data=$this->Labelimg_model->delete_upload_images($imgid,$sku);

       echo $data;
        
    }
    



function getSKUForUpdate(){


   if ($this->session->userdata('front_logged_in')) {
    $session_data = $this->session->userdata('front_logged_in');
    $user_id = $session_data['user_id'];
    $user_name = $session_data['user_name'];


     $imgid=$this->input->post('imgid');
      $sku=$this->input->post('sku');

      $query=$this->db->query("select * from  tbl_upload_img where user_id='$user_id' and upimg_id='$imgid' ");   
      $row=$query->result_array();
      $zipfile_name=$row[0]['zipfile_name'];
      echo  $zipfile_name;


    }
    else
    {
    $user_id = '';
    $user_name = '';
    }


      
        
    }

function saveSKUForUpdate(){



   if ($this->session->userdata('front_logged_in')) {
    $session_data = $this->session->userdata('front_logged_in');
    $user_id = $session_data['user_id'];
    $user_name = $session_data['user_name'];



       $imgid=$this->input->post('imgid');
       $sku=$this->input->post('sku');
       $sku_edit=$this->input->post('sku_edit');

      //echo "select * from  tbl_upload_img where zipfile_name='$sku' and user_id='$user_id' ";exit;
      /* $query=$this->db->query("select * from  tbl_upload_img where zipfile_name='$sku_edit' and user_id='$user_id' ");  */ 
       $query=$this->db->query("select * from  tbl_upload_img where zipfile_name='$sku_edit'  and zipfile_name!='$sku' ");  
       $row_chk=$query->result_array();
       if(empty($row_chk))
       {
        $data=$this->Labelimg_model->save_upload_images($imgid,$sku,$sku_edit);
        echo $sku_edit;
       }
       else
       {
        echo "0";
       }




    }
    else
    {
    $user_id = '';
    $user_name = '';
    }



  
        
    }






}
?>