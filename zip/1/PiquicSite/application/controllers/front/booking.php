<?php 
class Booking extends CI_Controller{
	
	function __construct(){
		parent::__construct();
		$this->load->library(['session']); 
    	$this->load->helper(['url','file','form']);
    	$this->load->model('Booking_model');
   
	}

	public function index($tmb_book_id=""){
		//print_r("Temp booking".$tmb_book_id);
		if ($this->session->userdata('front_logged_in')) {
			$session_data = $this->session->userdata('front_logged_in');
			$data['user_id'] = $session_data['user_id'];
			$data['user_name'] = $session_data['user_name'];
			if(!empty($tmb_book_id))
			{
				$data['book_id']=$tmb_book_id;
				$data['booking_data']= $this->db->query("select * from tbl_tmp_booking where book_id=$tmb_book_id;")->result_array();
			
				$this->load->view('front/booking',$data );
			}
			else
			{
				$this->load->view('front/booking',$data );
			}
			
			

		}
		else
		{
			redirect('/');
		}

		
	}

	public function add_book($tmb_book_id=""){
		// echo"oooooo";exit();
			$created_date = date("Y-m-d H:i:s");
			
			$order_id=$this->input->post('order_id');
			$user_id=$this->input->post('user_id');
			$gender=$this->input->post('gender');
			$m_status=$this->input->post('m_status');
			$category=",".implode(",",$this->input->post('cat')).",";	
			$quty=",".implode(",",$this->input->post('quty')).",";
			$angle="";
			if($this->input->post('angle_Top')!="")
			{
				$angle .=",".implode(",",$this->input->post('angle_Top')).",";
			}

			if($this->input->post('angle_Bottom')!="")
			{
				$angle .=implode(",",$this->input->post('angle_Bottom')).",";		
			}

			if($this->input->post('angle_Dress')!="")
			{
				$angle .=implode(",",$this->input->post('angle_Dress')).",";			
			}
			$d_status=$this->input->post('d_status');
			$notes=$this->input->post('notes');
			$total_price=$this->input->post('total_price');
			$total_image=$this->input->post('total_image');
			$currency=$this->input->post('currency');
			$plan_id=$this->input->post('planid'); 
			$plan_left_credit=$this->input->post('bal_total_image'); 
			$plan_left_amt=$this->input->post('bal_total_price'); 

			$query1=$this->db->query("select * from  tbl_plan where plan_for='booking' && planid=$plan_id && plan_currency='$currency' && status='0' && deleted='0'");

			$planDetails1= $query1->result_array();
			$plntyid=$planDetails1[0]['plntyid'];
			//print_r($planDetails1);
			$query=$this->db->query("select * from tbl_account_cal where user_id='$user_id' and currency='$currency' and plan_status='Active' and payment_status='Paid' and plan_end_date >= NOW() || plan_end_date = NOW() and plan_left_credit>0  and plan_left_amt>0 ORDER BY plan_end_date ASC LIMIT 1");
			// print_r("select * from tbl_account_cal where user_id='$user_id' and currency='$currency' and plan_status='Active' and payment_status='Paid' and plan_end_date >= NOW() || plan_end_date = NOW() and plan_left_credit>0  and plan_left_amt>0 ORDER BY plan_end_date ASC LIMIT 1");
			$planbookDetails= $query->result_array();



			if(!empty($planbookDetails))
			{

				
					if(!empty($plan_left_credit)!='0' && !empty($plan_left_amt)!='0')
					{

						$plan_used_amt=$total_price;
						$plan_used_credit=$total_image;
						$p_status="paid";
						
					}
					if($plan_left_credit=='0' && $plan_left_amt=='0')
					{

					
						$plan_used_amt=$planbookDetails[0]['plan_left_amt'];
						$plan_used_credit=$planbookDetails[0]['plan_left_credit'];
						$p_status="Not paid";
						
					}

		
				// $plan_left_amt=$planbookDetails[0]['plan_left_amt']-$total_price;
				// $plan_left_credit=$planbookDetails[0]['plan_left_credit']-$total_image;
				// $plan_used_amt=$total_price;
				// $plan_used_credit=$total_image;
				// $p_status="paid";
			}
			else if($plntyid==2){
				$img_price=$total_image*$planDetails1[0]['plan_per_image'];
				$plan_left_amt=$planDetails1[0]['plan_amount']-$img_price;
				$plan_left_credit=$planDetails1[0]['plan_credit']-$total_image;
				$plan_used_amt=$img_price;
				$plan_used_credit=$total_image;
				$p_status="Not paid";
			}
			else{
				$plan_left_amt='0';
				$plan_left_credit='0';
				$plan_used_amt="0";
				$plan_used_credit="0";
				$p_status="Not paid";
			}
// if(!empty($this->input->post('bal_plan')))
			// {
			// 	$bal_plan=$this->input->post('bal_plan');
			// }
			// else{
			// 	$planid=$this->input->post('planid'); 
			// }
			
			$data = array(
				'user_id'=>$user_id,
				'order_id'=>$order_id,
				'gender'=>$gender,
				'category'=>$category,
				'quty'=>$quty,
				'm_status'=>$m_status,
				'angle'=>$angle,
				'd_status'=>$d_status,
				'notes'=>$notes,
				'b_status'=>"1",
				'total_price'=>$total_price,
				'total_image'=>$total_image,
				'plan_type'=>$plan_id,
				'p_status'=>$p_status,
				'create_date'=>$created_date,
				'plan_used_amt'=>$plan_used_amt,
				'plan_used_credit'=>$plan_used_credit,
				'plan_left_amt'=>$plan_left_amt,
				'plan_left_credit'=>$plan_left_credit,

			);
			//print_r($data);exit();
		if(!empty($tmb_book_id))
		{
			$result=$this->Booking_model->update_booking($data,$tmb_book_id);		
			echo $tmb_book_id;
		}
		else
		{
			$result=$this->Booking_model->add_booking($data);
			$tmb_book_id=$this->db->insert_id();					
			echo $tmb_book_id;
		}
	}


	public function price_calculation()
	{
		$total_price=0;
		$total_image=$this->input->post('total_image');
		$user_id=$this->input->post('user_id');
		$payment_type=$this->input->post('payment_type_val');
		$top_total_sum=$this->input->post('top_total_sum');
		$bottom_total_sum=$this->input->post('bottom_total_sum');
		$dress_total_sum=$this->input->post('dress_total_sum');
		if(!empty($this->input->post('bal_plan'))){
			$bal_plan=$this->input->post('bal_plan');
		}else{
			$bal_plan="";
		}
		//echo "asddfasd". $bal_plan;
		$query = $this->db->get_where('tbl_users', array('user_id'=>$user_id));
		//$query = $this->db->get("tbl_upload_img");
		$result=$query->result_array(); 
		$curr=$result[0]['user_currency'];
		if(!empty($curr) && !empty($payment_type) && !empty($bal_plan))
		{
			//echo $payment_type;
			//echo "asdfasd".$bal_plan;
			$bal_plan1=explode("_",$bal_plan);
			//echo $bal_plan1[0].$bal_plan1[1].$bal_plan1[2].$bal_plan1[3];
			$bal_image=$total_image-$bal_plan1[3];

			

			if($bal_plan1[0]==2){
				$this->db->select('*');
				$this->db->from('tbl_plan');
				$this->db->where('plan_for', 'booking');
				$this->db->where('plan_currency',$curr);
				$this->db->where('status','0');
				$this->db->where('deleted','0');
				$this->db->where('plntyid',$bal_plan1[0]);
				$this->db->where('plan_name',$bal_plan1[1]);
				$query = $this->db->get();
				$result=$query->result_array();

				//echo $this->db->last_query();
				//print_r($result);
				if(!empty($result))
				{
					$bal_planid=$result[0]['planid'];
					$bal_price=$result[0]['plan_per_image'];
					$bal_price1=$bal_plan1[3]*$bal_price-1;
					//echo "Plan Balance  ------  ". $bal_price."  ------  ";
				}
			}
			if($payment_type==1){
				$this->db->select('*');
				$this->db->from('tbl_plan');
				$this->db->where('plan_for', 'booking');
				$this->db->where('plan_currency',$curr);
				$this->db->where('status','0');
				$this->db->where('deleted','0');
				$this->db->where('plntyid',$payment_type);
				if($bal_image>=1&&$bal_image<=99)
				{
					$this->db->where('plan_credit >', 1);
					$this->db->where('plan_credit <=', 99);
				}
				else if($bal_image>=100&&$bal_image<=499)
				{
					$this->db->where('plan_credit >', 100);
					$this->db->where('plan_credit <=', 500);
				}
				else if($bal_image>=500&&$bal_image<=999)
				{
					$this->db->where('plan_credit >', 500);
					$this->db->where('plan_credit <=', 1000);
				}
				else if($bal_image>=1000&&$bal_image<=5000)
				{
					$this->db->where('plan_credit >', 1000);
					$this->db->where('plan_credit <=', 5000);
				}
				// else if($bal_image>5000)
				// {
				// 	$this->db->where('plan_credit >', 5000);
				// }
				$query = $this->db->get();
				$result=$query->result_array();

				//echo $this->db->last_query();
				//print_r($result);
				if(!empty($result))
				{
					$planid=$result[0]['planid'];
					$price=$result[0]['plan_per_image'];
					$price1=$bal_image*$price;
					//echo "Pay as you go  ------  ".$price1."  ------ Perimage ".$price."";
				}
			}
			
			$data['planid']=$planid;
			$data['price']=$price1;
			$data['bal_planid']=$bal_planid;
			$data['bal_price']=$bal_price1;
			echo json_encode($data);
		}
		else if(!empty($curr) && !empty($payment_type) && empty($bal_plan))
		{
			//echo"asdf";
			$this->db->select('*');
			$this->db->from('tbl_plan');
			$this->db->where('plan_for', 'booking');
			$this->db->where('plan_currency',$curr);
			$this->db->where('status','0');
			$this->db->where('deleted','0');
			if($payment_type==1)
			{
				$this->db->where('plntyid',$payment_type);
				if($total_image>=1&&$total_image<=99)
				{
					$this->db->where('plan_credit >', 1);
					$this->db->where('plan_credit <=', 99);
				}
				else if($total_image>=100&&$total_image<=499)
				{
					$this->db->where('plan_credit >', 100);
					$this->db->where('plan_credit <=', 500);
				}
				else if($total_image>=500&&$total_image<=999)
				{
					$this->db->where('plan_credit >', 500);
					$this->db->where('plan_credit <=', 1000);
				}
				else if($total_image>=1000&&$total_image<=5000)
				{
					$this->db->where('plan_credit >', 1000);
					$this->db->where('plan_credit <=', 5000);
				}
			}
			else if($payment_type==2){
			//	echo ":asdfasdf1111111111111";
				if($payment_type==2){
					//print_r($payment_type);
					$payment_type=explode("_",$payment_type);
					$this->db->where('plntyid',$payment_type[0]);
					$this->db->where('plan_name',$payment_type[1]);
				}
			
			}
		
			
			$query = $this->db->get();
			$result=$query->result_array();

			//echo $this->db->last_query();
			//print_r($result);
			if(!empty($result))
			{
				$data['planid']=$result[0]['planid'];
				if($payment_type==1)
				{
					$data['price']=$result[0]['plan_per_image'];
				}
				else if(!empty($bal_plan)){
					if($bal_plan1[0]==2){

						$data['price']=$result[0]['plan_per_image'];
					}
					else{

						$data['price']=$result[0]['plan_amount'];
					}
				}
				else if(empty($bal_plan)){
						$data['price']=$result[0]['plan_amount'];
				}
			}
			else{
				$data['planid']="0";
				$data['price']="0";
			}
			// $data['planid']=$result[0]['planid'];
			// if($payment_type==1)
			// {
			// 	$data['price']=$result[0]['plan_per_image'];
			// }
			// else{
			// 	$data['price']=$result[0]['plan_amount'];
			// }
			echo json_encode($data);
			//print_r($data);
		}
		else if(!empty($curr) && empty($payment_type) && !empty($bal_plan))
		{
			//echo"asdf";
			$this->db->select('*');
			$this->db->from('tbl_plan');
			$this->db->where('plan_for', 'booking');
			$this->db->where('plan_currency',$curr);
			$this->db->where('status','0');
			$this->db->where('deleted','0');
			
			if(!empty($bal_plan))
			{
				//echo ":asdfasdf333333333333333";
				$bal_plan1=explode("_",$bal_plan);
				//echo $bal_plan1[0].$bal_plan1[1].$bal_plan1[2].$bal_plan1[3];
				if($bal_plan1[0]==2){
					$this->db->where('plntyid',$bal_plan1[0]);
					$this->db->where('plan_name',$bal_plan1[1]);
				}
					
				
			}
			$query = $this->db->get();
			$result=$query->result_array();

			//echo $this->db->last_query();
			//print_r($result);
			if(!empty($result))
			{
				$data['planid']=$result[0]['planid'];
				if($payment_type==1)
				{
					$data['price']=$result[0]['plan_per_image'];
				}
				else if(!empty($bal_plan)){
					if($bal_plan1[0]==2){

						$data['price']=$result[0]['plan_per_image'];
					}
					else{

						$data['price']=$result[0]['plan_amount'];
					}
				}
				else if(empty($bal_plan)){
						$data['price']=$result[0]['plan_amount'];
				}
			}
			else{
				$data['planid']="0";
				$data['price']="0";
			}
			
			echo json_encode($data);
			//print_r($data);
		}
		
		else{
				$data['planid']="0";
				$data['price']="0";
				echo json_encode($data);
		}
	}
}

?>