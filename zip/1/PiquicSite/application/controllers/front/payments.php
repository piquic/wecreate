<?php 
class Payments extends CI_Controller{
	
	function __construct(){
		parent::__construct();
		$this->load->library(['session']); 
        $this->load->helper(['url','file','form']);
        $this->load->model('plans_model'); //load model upload 
	}

	public function index(){
		


		if ($this->session->userdata('front_logged_in')) {
            $session_data = $this->session->userdata('front_logged_in');
            $data['user_id'] = $session_data['user_id'];
            $user_id = $session_data['user_id'];
            $data['user_name'] = $session_data['user_name'];


           
            
            $plan_id=$this->input->post('hid_plan_id');

            /*$plantype_id=$this->input->post('hid_plantype_id');
            $plan_amount=$this->input->post('hid_plan_amount');
            $plan_credit=$this->input->post('hid_plan_credit');
            $currency=$this->input->post('currency');*/


			$planquery = "SELECT * FROM `tbl_plan` where planid='$plan_id' ";
			$query2=$this->db->query($planquery);
			$planinfo= $query2->result_array();

			$plantype_id=$planinfo[0]['plntyid'];
            $plan_amount=$planinfo[0]['plan_amount'];
            $plan_credit=$planinfo[0]['plan_credit'];

            $userquery = "SELECT * FROM `tbl_users` where user_id='$user_id' ";
			$query2=$this->db->query($userquery);
			$userinfo= $query2->result_array();
            $currency=$userinfo[0]['user_currency'];

            
            
			//$data['plansTypeDetails'] = $this->plans_model->getAllPlans();
			//$data['billingDetails'] = $this->plans_model->getAllbilling($data['user_id']);
			/*$data['plantype_id'] = $plantype_id;
			$data['plan_id'] = $plan_id;
			$data['plansDetails'] = $this->plans_model->get_plansById($plan_id);
			$data['plan_amount'] = $plan_amount;
			$data['plan_credit'] = $plan_credit;
			$data['currency'] = $currency;*/


			$this->session->set_userdata('sess_plan_id', $plan_id);
			$this->session->set_userdata('sess_currency', $currency);


		    //$this->load->view('front/payments',$data);
		    redirect('paymentdetails/'.$plan_id);
			
         
        } else {
        	
            $session_data = $this->session->userdata('front_logged_in');
            $data['user_id'] = $session_data['user_id'];
            $data['user_name'] = $session_data['user_name'];
            
			redirect('home');
        }
	}







	public function paymentdetails($plan_id){
		


		if ($this->session->userdata('front_logged_in')) {
            $session_data = $this->session->userdata('front_logged_in');
            $data['user_id'] = $session_data['user_id'];
            $data['user_name'] = $session_data['user_name'];
            $user_id = $session_data['user_id'];

           
            //$plantype_id=$this->input->post('hid_plantype_id');
            //$plan_id=$this->input->post('hid_plan_id');
            //$plan_amount=$this->input->post('hid_plan_amount');
            //$plan_credit=$this->input->post('hid_plan_credit');


            $planquery = "SELECT * FROM `tbl_plan` where planid='$plan_id' ";
			$query2=$this->db->query($planquery);
			$planinfo= $query2->result_array();

			$plantype_id=$planinfo[0]['plntyid'];
            $plan_amount=$planinfo[0]['plan_amount'];
            $plan_credit=$planinfo[0]['plan_credit'];

            $userquery = "SELECT * FROM `tbl_users` where user_id='$user_id' ";
			$query2=$this->db->query($userquery);
			$userinfo= $query2->result_array();
            $currency=$userinfo[0]['user_currency'];




            $currency=$this->session->userdata('sess_currency');
            
			//$data['plansTypeDetails'] = $this->plans_model->getAllPlans();
			//$data['billingDetails'] = $this->plans_model->getAllbilling($data['user_id']);
			$data['plantype_id'] = $plantype_id;
			$data['plan_id'] = $plan_id;
			$data['plansDetails'] = $this->plans_model->get_plansById($plan_id);
			$data['plan_amount'] = $plan_amount;
			$data['plan_credit'] = $plan_credit;

			$data['currency'] = $currency;
			$data['action'] = '';

//////////////////////////////////////////////////////////////////////////////////
			$data['hash'] = '';
			$data['udf1'] = '';
			$data['udf2'] = '';
			$data['udf3'] = '';
			$data['udf4'] = '';
			$data['udf5'] = '';


		    $this->load->view('front/payments',$data);
			
         
        } else {
        	
            $session_data = $this->session->userdata('front_logged_in');
            $data['user_id'] = $session_data['user_id'];
            $data['user_name'] = $session_data['user_name'];
            
			redirect('home');
        }
	}





public function payumoney_old($plan_id){
if ($this->session->userdata('front_logged_in')) {
            $session_data = $this->session->userdata('front_logged_in');
            $data['user_id'] = $session_data['user_id'];
            $data['user_name'] = $session_data['user_name'];
            $user_id = $session_data['user_id'];

           	$plantype_id=$this->input->post('hid_plantype_id');
            //echo $plan_id=$this->input->post('plan_id');
            $plan_amount=$this->input->post('hid_plan_amount');
            $plan_credit=$this->input->post('hid_plan_credit');


            $currency=$this->session->userdata('sess_currency');
            
			//$data['plansTypeDetails'] = $this->plans_model->getAllPlans();
			//$data['billingDetails'] = $this->plans_model->getAllbilling($data['user_id']);
			$data['plantype_id'] = $plantype_id;
			$data['plan_id'] = $plan_id;
			$data['plansDetails'] = $this->plans_model->get_plansById($plan_id);
			$data['plan_amount'] = $plan_amount;
			$data['plan_credit'] = $plan_credit;

			$data['currency'] = $currency;




			// Merchant key here as provided by PayUMoney
			$MERCHANT_KEY = $this->input->post('key');

			// Merchant Salt as provided by Payu
			$SALT = $this->input->post('salt');

			// End point - change to https://secure.payu.in for if LIVE mode
			$PAYU_BASE_URL = $this->input->post('payu_base_url');

			$action = '';

			$posted = array();
			if(!empty($_POST)) {
			//print_r($_POST);exit;
			foreach($_POST as $key => $value) {    
			$posted[$key] = $value; 

			}
			}

			$formError = 0;

			if(empty($posted['txnid'])) {
			// Generate random transaction id
			$txnid = substr(hash('sha256', mt_rand() . microtime()), 0, 20);
			} else {
			$txnid = $posted['txnid'];
			}
			$hash = '';
			// Hash Sequence
			$hashSequence = "key|txnid|amount|productinfo|firstname|email|udf1|udf2|udf3|udf4|udf5|udf6|udf7|udf8|udf9|udf10";
			if(empty($posted['hash']) && sizeof($posted) > 0) {
			if(
			empty($posted['key'])
			|| empty($posted['txnid'])
			|| empty($posted['amount'])
			|| empty($posted['firstname'])
			|| empty($posted['email'])
			|| empty($posted['phone'])
			|| empty($posted['productinfo'])
			|| empty($posted['surl'])
			|| empty($posted['furl'])
			|| empty($posted['service_provider'])
			) {
			$formError = 1;
			} else {
			//$posted['productinfo'] = json_encode(json_decode('[{"name":"tutionfee","description":"","value":"500","isRequired":"false"},{"name":"developmentfee","description":"monthly tution fee","value":"1500","isRequired":"false"}]'));
			$hashVarsSeq = explode('|', $hashSequence);
			$hash_string = '';	
			foreach($hashVarsSeq as $hash_var) {
			$hash_string .= isset($posted[$hash_var]) ? $posted[$hash_var] : '';
			$hash_string .= '|';
			}

			$hash_string .= $SALT;


			$hash = strtolower(hash('sha512', $hash_string));
			$action = $PAYU_BASE_URL . '/_payment';
			}
			} elseif(!empty($posted['hash'])) {
			$hash = $posted['hash'];
			$action = $PAYU_BASE_URL . '/_payment';
			}


//////////////////////////////////////////////////////////////////////////////////

			
			$data['action'] = $action;
			$data['hash'] = $hash;
			$data['MERCHANT_KEY'] = $MERCHANT_KEY;
			$data['SALT'] = $SALT;
			$data['txnid'] = $this->input->post('txnid');
			$data['PAYU_BASE_URL'] = $this->input->post('payu_base_url');
			$data['amount'] = $this->input->post('amount');
			$data['firstname'] = $this->input->post('firstname');
			$data['email'] = $this->input->post('email');
			$data['phone'] = $this->input->post('phone');
			$data['productinfo'] = $this->input->post('productinfo');
			$data['surl'] = $this->input->post('surl');
			$data['furl'] = $this->input->post('furl');
			$data['udf1'] = $this->input->post('udf1');
			$data['udf2'] = $this->input->post('udf2');
			$data['udf3'] = $this->input->post('udf3');
			$data['udf4'] = $this->input->post('udf4');
			$data['udf5'] = $this->input->post('udf5');
	











		    $this->load->view('front/payments',$data);

            


        }else{


         $session_data = $this->session->userdata('front_logged_in');
            $data['user_id'] = $session_data['user_id'];
            $data['user_name'] = $session_data['user_name'];
            
			//redirect('home');
        }


}






























	public function insert_billing(){
		


		if ($this->session->userdata('front_logged_in')) {
            $session_data = $this->session->userdata('front_logged_in');
            $data['user_id'] = $session_data['user_id'];
            $data['user_name'] = $session_data['user_name'];
            $user_id = $session_data['user_id'];


           
            $billing_id=$this->input->post('billing_id');
            $payment_type_id=$this->input->post('payment_type_id');
            $plan_id=$this->input->post('plan_id');
            $plan_credit=$this->input->post('plan_credit');
            $plan_amount=$this->input->post('plan_amount');
            $currency=$this->input->post('currency');
            //$billing_unique_id=md5(uniqid(mt_rand(), true)).$user_id;
            $billing_unique_id=$user_id.substr(md5(mt_rand(1000000, 9999999)), 0, 12);


            if($billing_id!='')
			{
				
				  $data = array(
				    'user_id'=>$user_id,
					'billing_unique_id'=>$billing_unique_id,
					'billing_plan_id'=>$plan_id,
					'billing_plan_credit'=>$plan_credit,
					'billing_plan_amount'=>$plan_amount,
					'billing_date_time'=>date("Y-m-d H:i:s"),
					'billing_status'=>'Pending',
					'payment_type'=>$payment_type_id,
					'payment_status'=>'Notpaid',
					'currency'=>$currency,
					);
					
				  $updateuserrole = $this->plans_model->billing_update($data,$billing_id);
			      echo $billing_id;
				
			}else {
				
				
				
				     $data = array(
				    'user_id'=>$user_id,
					'billing_unique_id'=>$billing_unique_id,
					'billing_plan_id'=>$plan_id,
					'billing_plan_credit'=>$plan_credit,
					'billing_plan_amount'=>$plan_amount,
					'billing_date_time'=>date("Y-m-d H:i:s"),
					'billing_status'=>'Pending',
					'payment_type'=>$payment_type_id,
					'payment_status'=>'Notpaid',
					'currency'=>$currency,
					);
					$this->plans_model->billing_insert($data);
					$billing_id=$this->db->insert_id();
					
					
					echo $billing_id;
				}
            
            
			


		    //redirect('home');
			
         
        } else {
        	
            $session_data = $this->session->userdata('front_logged_in');
            $data['user_id'] = $session_data['user_id'];
            $data['user_name'] = $session_data['user_name'];
            
			//redirect('home');
        }
	}






	public function paymentsuccess(){
		


		if ($this->session->userdata('front_logged_in')) {
            $session_data = $this->session->userdata('front_logged_in');
            $data['user_id'] = $session_data['user_id'];
            $data['user_name'] = $session_data['user_name'];
            $user_id = $session_data['user_id'];


            //echo "<pre>"; print_r($_POST);echo "</pre>";
            //echo "<pre>"; print_r($this->input->post());echo "</pre>";
            

			$billing_id=$this->input->post('udf1');
            $payment_amount=$this->input->post('udf2');
            $payment_credit=$this->input->post('udf3');

            /************************************************************************/
				$pautoquery = "SELECT * FROM `tbl_users` where user_id='$user_id'  ";
				// echo $pautoquery;
				$query2=$this->db->query($pautoquery);
				$usrinfo= $query2->result_array();
				$total_amount=$usrinfo[0]['total_amount'];
				$total_credit=$usrinfo[0]['total_credit'];


				$current_amount=$total_amount+$payment_amount;
				$current_credit=$total_credit+$payment_credit;
				$payment_status="Paid";



				$userdata=array(
				'total_amount' => $current_amount,
				'total_credit' => $current_credit);

				$this->db->where('user_id', $user_id);
				$report = $this->db->update('tbl_users', $userdata);

		/***************************updating Style page****************************/
//ppppppppppppppppp

		$pautoquery = "SELECT * FROM `tbl_style` where user_id='$user_id' and style_payment_status='Not paid' order by sty_id asc ";
		// echo $pautoquery;
		$query2=$this->db->query($pautoquery);
		$styleinfo= $query2->result_array();

		$tot_payment_amount=$payment_amount;
		$tot_payment_credit=$payment_credit;

		if(!empty($styleinfo))
		{
			foreach($styleinfo as $key => $value)
			{
				$sty_id=$value['sty_id'];
				$style_amount=$value['style_amount'];
				$style_credit=$value['style_credit'];
				$style_pending_amount=$value['style_pending_amount'];
				$style_pending_credit=$value['style_pending_credit'];
				$style_payment_status=$value['style_payment_status'];



				if(strstr($style_pending_amount,"-") && strstr($style_pending_credit,"-"))
				{
					//echo "<br>".$tot_payment_amount."--".trim($style_pending_amount,"-")."--".$sty_id."<br>";

					$style_pending_amount=$tot_payment_amount-trim($style_pending_amount,"-");
					$style_pending_credit=$tot_payment_credit-trim($style_pending_credit,"-");

					

					

					if($style_pending_amount>=0 )
					{
						$this->db->set('style_payment_status', 'Paid'); 
						//$this->db->set('comments', '');
						//$this->db->set('status_type', '');

						$this->db->set('style_pending_amount', '0');
					    $this->db->set('style_pending_credit', '0'); 


						$tot_payment_amount=$style_pending_amount;
						$tot_payment_credit=$style_pending_credit;

					
					}
					else 
					{
						$this->db->set('style_pending_amount', $style_pending_amount);
					    $this->db->set('style_pending_credit', $style_pending_credit); 

					   $this->db->set('style_payment_status', 'Not paid'); 


						$tot_payment_amount=$style_pending_amount;
						$tot_payment_credit=$style_pending_credit;
					}

					//$this->db->where('zipfile_name', $sku);
					$array = array('sty_id' => $sty_id);
					$this->db->where($array); 
					$result=$this->db->update('tbl_style');


				}

				






			}

		}



//exit;
  /************************************************************************/




				$data=array(
				'user_id' => $user_id,
				'billing_id' => '0',
				'sty_id' => '0',
				'upimg_id' => '0',
				'history_type' => 'Addition',
				'amount' => $payment_amount,
				'credit' => $payment_credit,
				'account_history_date' => date("Y-m-d"),
				'account_history_time' => date("H:i:s"),
				'payment_status' => $payment_status,
				);
				$this->db->insert('tbl_account_history', $data);




      /************************************************************************/
            

             $status=$this->input->post('status');
            $billing_id=$this->input->post('udf1');
            if($status=='success')
            {
            	$data = array(
				    'billing_date_time'=>date("Y-m-d H:i:s"),
					'billing_status'=>'Completed',
					'payment_status'=>'Paid',
					);
					
			    $updateuserrole = $this->plans_model->billing_update($data,$billing_id);
            }
            $data['billing_id'] = $billing_id;


			$query=$this->db->query("select * from  tbl_users  WHERE user_id='$user_id' ");
			$userDetails= $query->result_array();
			$user_name=$userDetails[0]['user_name'];
			$user_email=$userDetails[0]['user_email'];
			$user_mobile=$userDetails[0]['user_mobile'];





			$billquery=$this->db->query("select * from  tbl_users_billing  WHERE billing_id='$billing_id' ");
			$billDetails= $billquery->result_array();

			$billing_plan_id=$billDetails[0]['billing_plan_id'];

			$billing_plan_credit=$billDetails[0]['billing_plan_credit'];
			$billing_plan_amount=$billDetails[0]['billing_plan_amount'];
			$currency=$billDetails[0]['currency'];
			$billing_date_time=$billDetails[0]['billing_date_time'];
			$billing_status=$billDetails[0]['billing_status'];
			$payment_status=$billDetails[0]['payment_status'];
			$payment_type=$billDetails[0]['payment_type'];


			$query=$this->db->query("select tbl_plan.*,tbl_plantype.plntypname from  tbl_plan
			INNER JOIN  tbl_plantype on tbl_plan.plntyid=tbl_plantype.plntyid
			WHERE tbl_plan.planid='$billing_plan_id'");
			$plansDetails=$query->result_array();
			$plan_name=$plansDetails[0]['plan_name'];

            //$user_email="demo.intexom@gmail.com";
            if($payment_type=='1')
            {
            	$mail_text='';
            	$subject='Piquic- Payment success';
            }
            elseif($payment_type=='2')
            {
            	$mail_text='';
            	$subject='Piquic- Payment success';
            }
            elseif($payment_type=='3')
            {
            	$mail_text='';
            	$subject='Piquic- Payment success';
            }
		    
		    elseif($payment_type=='4')
            {
            	$mail_text='';
            	$subject='Piquic- Payment success';
            }
		    
		    elseif($payment_type=='5')
            {
            	$mail_text='';
            	$subject='Piquic- Payment success';
            }
            elseif($payment_type=='6')
            {
            	$mail_text='Payment Email Piquic- Pay-Later';
            	$subject='Piquic- Payment Pending';
            }
		    
		    
            

/*********************************Send mail*************************************/

	$query=$this->db->query("select * from  tbl_settings  WHERE setting_key='MAIL_SMTP_USER' ");
	$userDetails= $query->result_array();
	$MAIL_SMTP_USER=$userDetails[0]['setting_value'];

	$query=$this->db->query("select * from  tbl_settings  WHERE setting_key='MAIL_SMTP_PASSWORD' ");
	$userDetails= $query->result_array();
	$MAIL_SMTP_PASSWORD=$userDetails[0]['setting_value'];

	$query=$this->db->query("select * from  tbl_settings  WHERE setting_key='MAIL_BODY' ");
	$userDetails= $query->result_array();
	$MAIL_BODY=$userDetails[0]['setting_value'];

		
		//Load email library
		$this->load->library('email');

		$config = array();
		//$config['protocol'] = 'smtp';
		$config['protocol']     = 'mail';
		$config['smtp_host'] = 'localhost';
		$config['smtp_user'] = $MAIL_SMTP_USER;
		$config['smtp_pass'] = $MAIL_SMTP_PASSWORD;
		$config['smtp_port'] = 25;




		$this->email->initialize($config);
		$this->email->set_newline("\r\n");

		
		$from_email = "demo.intexom@gmail.com";
		$to_email = $user_email;

		$billing_date_time=date("d/m/y",strtotime($billing_date_time));
		$copy_right= date("Y");
		//echo $message=$MAIL_BODY;
		$find_arr = array("##USER_NAME##","##USER_EMAIL##","##PLAN_NAME##","##PLAN_CREDIT##","##PLAN_AMOUNT##","##BILLING_DATE##","##BILLING_STATUS##","##PAYMENT_STATUS##",'##MAIL_TEXT##','##COPYRIGHT##');
		$replace_arr = array($user_name,$user_email,$plan_name,$billing_plan_credit,$billing_plan_amount,$billing_date_time,$billing_status,$payment_status,$mail_text,$copy_right);
		$message=str_replace($find_arr,$replace_arr,$MAIL_BODY);

		//echo $message;exit;



		$this->email->from($from_email, 'Piquic');
		$this->email->to($to_email);
		$this->email->cc('poulami@mokshaproductions.in');
        $this->email->bcc('pranav@mokshaproductions.in');
		$this->email->subject($subject);
		$this->email->message($message);
		//Send mail
		if($this->email->send())
		{

			//echo "mail sent";exit;
			$this->session->set_flashdata("email_sent","Congragulation Email Send Successfully.");
		}
		
		else
		{
			//echo $this->email->print_debugger();
			//echo "mail not sent";exit;
			$this->session->set_flashdata("email_sent","You have encountered an error");
		}
		

	//exit;

/************************************************************************/

			 

            //$this->load->view('front/paymentssuccess',$data);

           redirect("paysuccess");
		   
			
         
        } else {
        	
            $session_data = $this->session->userdata('front_logged_in');
            $data['user_id'] = $session_data['user_id'];
            $data['user_name'] = $session_data['user_name'];
            
			redirect('home');
        }
	}



	
	public function paypalsuccess(){




		if ($this->session->userdata('front_logged_in')) {
            $session_data = $this->session->userdata('front_logged_in');
            $data['user_id'] = $session_data['user_id'];
            $data['user_name'] = $session_data['user_name'];
            $user_id = $session_data['user_id'];


            //echo "<pre>"; print_r($_POST);echo "</pre>";
            //echo "<pre>"; print_r($this->input->post());echo "</pre>";
            

			 $billing_id=$this->input->post('custom');

			$billquery=$this->db->query("select * from  tbl_users_billing  WHERE billing_id='$billing_id' ");
			$billDetails= $billquery->result_array();

			$plan_id=$billDetails[0]['billing_plan_id'];
			$payment_credit=$billDetails[0]['billing_plan_credit'];
			 $payment_amount=$billDetails[0]['billing_plan_amount'];
        

            /************************************************************************/
				$pautoquery = "SELECT * FROM `tbl_users` where user_id='$user_id'  ";
				// echo $pautoquery;
				$query2=$this->db->query($pautoquery);
				$usrinfo= $query2->result_array();
				$total_amount=$usrinfo[0]['total_amount'];
				$total_credit=$usrinfo[0]['total_credit'];

				$current_amount=$total_amount+$payment_amount;
				$current_credit=$total_credit+$payment_credit;
				$payment_status="Paid";

			




				$userdata=array(
				'total_amount' => $current_amount,
				'total_credit' => $current_credit);

				$this->db->where('user_id', $user_id);
				$report = $this->db->update('tbl_users', $userdata);


	/***************************updating Style page****************************/
//ppppppppppppppppp

		$pautoquery = "SELECT * FROM `tbl_style` where user_id='$user_id' and style_payment_status='Not paid' order by sty_id asc ";
		// echo $pautoquery;
		$query2=$this->db->query($pautoquery);
		$styleinfo= $query2->result_array();

		$tot_payment_amount=$payment_amount;
		$tot_payment_credit=$payment_credit;

		if(!empty($styleinfo))
		{
			foreach($styleinfo as $key => $value)
			{
				$sty_id=$value['sty_id'];
				$style_amount=$value['style_amount'];
				$style_credit=$value['style_credit'];
				$style_pending_amount=$value['style_pending_amount'];
				$style_pending_credit=$value['style_pending_credit'];
				$style_payment_status=$value['style_payment_status'];



				if(strstr($style_pending_amount,"-") && strstr($style_pending_credit,"-"))
				{
					//echo "<br>".$tot_payment_amount."--".trim($style_pending_amount,"-")."--".$sty_id."<br>";

					$style_pending_amount=$tot_payment_amount-trim($style_pending_amount,"-");
					$style_pending_credit=$tot_payment_credit-trim($style_pending_credit,"-");

					

					

					if($style_pending_amount>=0 )
					{
						$this->db->set('style_payment_status', 'Paid'); 
						//$this->db->set('comments', '');
						//$this->db->set('status_type', '');

						$this->db->set('style_pending_amount', '0');
					    $this->db->set('style_pending_credit', '0'); 


						$tot_payment_amount=$style_pending_amount;
						$tot_payment_credit=$style_pending_credit;

					
					}
					else 
					{
						$this->db->set('style_pending_amount', $style_pending_amount);
					    $this->db->set('style_pending_credit', $style_pending_credit); 

					   $this->db->set('style_payment_status', 'Not paid'); 


						$tot_payment_amount=$style_pending_amount;
						$tot_payment_credit=$style_pending_credit;
					}

					//$this->db->where('zipfile_name', $sku);
					$array = array('sty_id' => $sty_id);
					$this->db->where($array); 
					$result=$this->db->update('tbl_style');


				}

				






			}

		}



//exit;
  /************************************************************************/








				$data=array(
				'user_id' => $user_id,
				'billing_id' => '0',
				'sty_id' => '0',
				'upimg_id' => '0',
				'history_type' => 'Addition',
				'amount' => $payment_amount,
				'credit' => $payment_credit,
				'account_history_date' => date("Y-m-d"),
				'account_history_time' => date("H:i:s"),
				'payment_status' => 'Paid',
				);
				$this->db->insert('tbl_account_history', $data);




      /************************************************************************/
            

            $payment_status=$this->input->post('payment_status');
          
            if($payment_status=='Completed')
            {
            	$data = array(
				    'billing_date_time'=>date("Y-m-d H:i:s"),
					'billing_status'=>'Completed',
					'payment_status'=>'Paid',
					);
					
			    $updateuserrole = $this->plans_model->billing_update($data,$billing_id);
            }
            $data['billing_id'] = $billing_id;




			$query=$this->db->query("select * from  tbl_users  WHERE user_id='$user_id' ");
			$userDetails= $query->result_array();
			$user_name=$userDetails[0]['user_name'];
			$user_email=$userDetails[0]['user_email'];
			$user_mobile=$userDetails[0]['user_mobile'];





			$billquery=$this->db->query("select * from  tbl_users_billing  WHERE billing_id='$billing_id' ");
			$billDetails= $billquery->result_array();

			$billing_plan_id=$billDetails[0]['billing_plan_id'];

			$billing_plan_credit=$billDetails[0]['billing_plan_credit'];
			$billing_plan_amount=$billDetails[0]['billing_plan_amount'];
			$currency=$billDetails[0]['currency'];
			$billing_date_time=$billDetails[0]['billing_date_time'];
			$billing_status=$billDetails[0]['billing_status'];
			$payment_status=$billDetails[0]['payment_status'];
			$payment_type=$billDetails[0]['payment_type'];


			$query=$this->db->query("select tbl_plan.*,tbl_plantype.plntypname from  tbl_plan
			INNER JOIN  tbl_plantype on tbl_plan.plntyid=tbl_plantype.plntyid
			WHERE tbl_plan.planid='$billing_plan_id'");
			$plansDetails=$query->result_array();
			$plan_name=$plansDetails[0]['plan_name'];

            //$user_email="demo.intexom@gmail.com";
            if($payment_type=='1')
            {
            	$mail_text='';
            	$subject='Piquic- Payment success';
            }
            elseif($payment_type=='2')
            {
            	$mail_text='';
            	$subject='Piquic- Payment success';
            }
            elseif($payment_type=='3')
            {
            	$mail_text='';
            	$subject='Piquic- Payment success';
            }
		    
		    elseif($payment_type=='4')
            {
            	$mail_text='';
            	$subject='Piquic- Payment success';
            }
		    
		    elseif($payment_type=='5')
            {
            	$mail_text='';
            	$subject='Piquic- Payment success';
            }
            elseif($payment_type=='6')
            {
            	$mail_text='Payment Email Piquic- Pay-Later';
            	$subject='Piquic- Payment Pending';
            }
		    
		    
            

/*********************************Send mail*************************************/

	$query=$this->db->query("select * from  tbl_settings  WHERE setting_key='MAIL_SMTP_USER' ");
	$userDetails= $query->result_array();
	$MAIL_SMTP_USER=$userDetails[0]['setting_value'];

	$query=$this->db->query("select * from  tbl_settings  WHERE setting_key='MAIL_SMTP_PASSWORD' ");
	$userDetails= $query->result_array();
	$MAIL_SMTP_PASSWORD=$userDetails[0]['setting_value'];

	$query=$this->db->query("select * from  tbl_settings  WHERE setting_key='MAIL_BODY' ");
	$userDetails= $query->result_array();
	$MAIL_BODY=$userDetails[0]['setting_value'];

		
		//Load email library
		$this->load->library('email');

		$config = array();
		//$config['protocol'] = 'smtp';
		$config['protocol']     = 'mail';
		$config['smtp_host'] = 'localhost';
		$config['smtp_user'] = $MAIL_SMTP_USER;
		$config['smtp_pass'] = $MAIL_SMTP_PASSWORD;
		$config['smtp_port'] = 25;




		$this->email->initialize($config);
		$this->email->set_newline("\r\n");

		
		$from_email = "demo.intexom@gmail.com";
		$to_email = $user_email;

		$billing_date_time=date("d/m/y",strtotime($billing_date_time));
		$copy_right= date("Y");
		//echo $message=$MAIL_BODY;
		$find_arr = array("##USER_NAME##","##USER_EMAIL##","##PLAN_NAME##","##PLAN_CREDIT##","##PLAN_AMOUNT##","##BILLING_DATE##","##BILLING_STATUS##","##PAYMENT_STATUS##",'##MAIL_TEXT##','##COPYRIGHT##');
		$replace_arr = array($user_name,$user_email,$plan_name,$billing_plan_credit,$billing_plan_amount,$billing_date_time,$billing_status,$payment_status,$mail_text,$copy_right);
		$message=str_replace($find_arr,$replace_arr,$MAIL_BODY);

		//echo $subject."----".$message;exit;



		$this->email->from($from_email, 'Piquic');
		$this->email->to($to_email);
		$this->email->cc('poulami@mokshaproductions.in');
        $this->email->bcc('pranav@mokshaproductions.in');
		$this->email->subject($subject);
		$this->email->message($message);
		//Send mail
		if($this->email->send())
		{

			//echo "mail sent";exit;
			$this->session->set_flashdata("email_sent","Congragulation Email Send Successfully.");
		}
		
		else
		{
			//echo $this->email->print_debugger();
			//echo "mail not sent";exit;
			$this->session->set_flashdata("email_sent","You have encountered an error");
		}
		

	//exit;

/************************************************************************/

			 

            //$this->load->view('front/paymentssuccess',$data);

 redirect("paysuccess");
		   
			
         
        } else {
        	
            $session_data = $this->session->userdata('front_logged_in');
            $data['user_id'] = $session_data['user_id'];
            $data['user_name'] = $session_data['user_name'];
            
			redirect('home');
        }



	}



		public function paylatersuccess(){
		


		if ($this->session->userdata('front_logged_in')) {
            $session_data = $this->session->userdata('front_logged_in');
            $data['user_id'] = $session_data['user_id'];
            $data['user_name'] = $session_data['user_name'];
            $user_id = $session_data['user_id'];


            //echo "<pre>"; print_r($_POST);echo "</pre>";
            //echo "<pre>"; print_r($this->input->post());echo "</pre>";
            

			 $billing_id=$this->input->post('paylater_billing_id');

			$billquery=$this->db->query("select * from  tbl_users_billing  WHERE billing_id='$billing_id' ");
			$billDetails= $billquery->result_array();

			$plan_id=$billDetails[0]['billing_plan_id'];
			$payment_credit=$billDetails[0]['billing_plan_credit'];
			$payment_amount=$billDetails[0]['billing_plan_amount'];
        

            /************************************************************************/
				$pautoquery = "SELECT * FROM `tbl_users` where user_id='$user_id'  ";
				// echo $pautoquery;
				$query2=$this->db->query($pautoquery);
				$usrinfo= $query2->result_array();
				$total_amount=$usrinfo[0]['total_amount'];
				$total_credit=$usrinfo[0]['total_credit'];


				$current_amount=$total_amount-$payment_amount;
				$current_credit=$total_credit-$payment_credit;
				$payment_status="Not Paid";


				$userdata=array(
				'total_amount' => $current_amount,
				'total_credit' => $current_credit);

				$this->db->where('user_id', $user_id);
				$report = $this->db->update('tbl_users', $userdata);






				$data=array(
				'user_id' => $user_id,
				'billing_id' => '0',
				'sty_id' => '0',
				'upimg_id' => '0',
				'history_type' => 'Deduction',
				'amount' => $payment_amount,
				'credit' => $payment_credit,
				'account_history_date' => date("Y-m-d"),
				'account_history_time' => date("H:i:s"),
				'payment_status' => $payment_status,
				);
				$this->db->insert('tbl_account_history', $data);




      /************************************************************************/
            

            //$status=$this->input->post('status');
           // $billing_id=$this->input->post('hid_billing_id');
            //if($status=='success')
            //{
            	$data = array(
				    'billing_date_time'=>date("Y-m-d H:i:s"),
					'billing_status'=>'Pay Later',
					'payment_status'=>'Not Paid',
					);
					
			    $updateuserrole = $this->plans_model->billing_update($data,$billing_id);
            //}
            $data['billing_id'] = $billing_id;




			$query=$this->db->query("select * from  tbl_users  WHERE user_id='$user_id' ");
			$userDetails= $query->result_array();
			$user_name=$userDetails[0]['user_name'];
			$user_email=$userDetails[0]['user_email'];
			$user_mobile=$userDetails[0]['user_mobile'];





			$billquery=$this->db->query("select * from  tbl_users_billing  WHERE billing_id='$billing_id' ");
			$billDetails= $billquery->result_array();

			$billing_plan_id=$billDetails[0]['billing_plan_id'];

			$billing_plan_credit=$billDetails[0]['billing_plan_credit'];
			$billing_plan_amount=$billDetails[0]['billing_plan_amount'];
			$currency=$billDetails[0]['currency'];
			$billing_date_time=$billDetails[0]['billing_date_time'];
			$billing_status=$billDetails[0]['billing_status'];
			$payment_status=$billDetails[0]['payment_status'];
			$payment_type=$billDetails[0]['payment_type'];


			$query=$this->db->query("select tbl_plan.*,tbl_plantype.plntypname from  tbl_plan
			INNER JOIN  tbl_plantype on tbl_plan.plntyid=tbl_plantype.plntyid
			WHERE tbl_plan.planid='$billing_plan_id'");
			$plansDetails=$query->result_array();
			$plan_name=$plansDetails[0]['plan_name'];

            //$user_email="demo.intexom@gmail.com";
            if($payment_type=='1')
            {
            	$mail_text='';
            	$subject='Piquic- Payment success';
            }
            elseif($payment_type=='2')
            {
            	$mail_text='';
            	$subject='Piquic- Payment success';
            }
            elseif($payment_type=='3')
            {
            	$mail_text='';
            	$subject='Piquic- Payment success';
            }
		    
		    elseif($payment_type=='4')
            {
            	$mail_text='';
            	$subject='Piquic- Payment success';
            }
		    
		    elseif($payment_type=='5')
            {
            	$mail_text='';
            	$subject='Piquic- Payment success';
            }
            elseif($payment_type=='6')
            {
            	$mail_text='Payment Email Piquic- Pay-Later';
            	$subject='Piquic- Payment Pending';
            }
		    
		    
            

/*********************************Send mail*************************************/

	$query=$this->db->query("select * from  tbl_settings  WHERE setting_key='MAIL_SMTP_USER' ");
	$userDetails= $query->result_array();
	$MAIL_SMTP_USER=$userDetails[0]['setting_value'];

	$query=$this->db->query("select * from  tbl_settings  WHERE setting_key='MAIL_SMTP_PASSWORD' ");
	$userDetails= $query->result_array();
	$MAIL_SMTP_PASSWORD=$userDetails[0]['setting_value'];

	$query=$this->db->query("select * from  tbl_settings  WHERE setting_key='MAIL_BODY' ");
	$userDetails= $query->result_array();
	$MAIL_BODY=$userDetails[0]['setting_value'];

		
		//Load email library
		$this->load->library('email');

		$config = array();
		//$config['protocol'] = 'smtp';
		$config['protocol']     = 'mail';
		$config['smtp_host'] = 'localhost';
		$config['smtp_user'] = $MAIL_SMTP_USER;
		$config['smtp_pass'] = $MAIL_SMTP_PASSWORD;
		//$config['smtp_port'] = 25;




		$this->email->initialize($config);
		$this->email->set_newline("\r\n");

		
		$from_email = "demo.intexom@gmail.com";
		$to_email = $user_email;

		$billing_date_time=date("d/m/y",strtotime($billing_date_time));
		$copy_right= date("Y");
		//echo $message=$MAIL_BODY;
		$find_arr = array("##USER_NAME##","##USER_EMAIL##","##PLAN_NAME##","##PLAN_CREDIT##","##PLAN_AMOUNT##","##BILLING_DATE##","##BILLING_STATUS##","##PAYMENT_STATUS##",'##MAIL_TEXT##','##COPYRIGHT##');
		$replace_arr = array($user_name,$user_email,$plan_name,$billing_plan_credit,$billing_plan_amount,$billing_date_time,$billing_status,$payment_status,$mail_text,$copy_right);
		$message=str_replace($find_arr,$replace_arr,$MAIL_BODY);

		//echo $subject."----".$message;exit;



		$this->email->from($from_email, 'Piquic');
		$this->email->to($to_email);
		$this->email->cc('poulami@mokshaproductions.in');
        $this->email->bcc('pranav@mokshaproductions.in');
		$this->email->subject($subject);
		$this->email->message($message);
		//Send mail
		if($this->email->send())
		{

			//echo "mail sent";exit;
			$this->session->set_flashdata("email_sent","Congragulation Email Send Successfully.");
		}
		
		else
		{
			//echo $this->email->print_debugger();
			//echo "mail not sent";exit;
			$this->session->set_flashdata("email_sent","You have encountered an error");
		}
		

	//exit;

/************************************************************************/

			 

            //$this->load->view('front/paymentssuccess',$data);

 redirect("paysuccess");
		   
			
         
        } else {
        	
            $session_data = $this->session->userdata('front_logged_in');
            $data['user_id'] = $session_data['user_id'];
            $data['user_name'] = $session_data['user_name'];
            
			redirect('home');
        }
	}


	public function paysuccess(){
		


		if ($this->session->userdata('front_logged_in')) {
            $session_data = $this->session->userdata('front_logged_in');
            $data['user_id'] = $session_data['user_id'];
            $data['user_name'] = $session_data['user_name'];
            $user_id = $session_data['user_id'];


            $this->load->view('front/paymentssuccess',$data);
		   
			
         
        } else {
        	
            $session_data = $this->session->userdata('front_logged_in');
            $data['user_id'] = $session_data['user_id'];
            $data['user_name'] = $session_data['user_name'];
            
			redirect('home');
        }
	}




	public function paymentfailure(){
		


		if ($this->session->userdata('front_logged_in')) {
            $session_data = $this->session->userdata('front_logged_in');
            $data['user_id'] = $session_data['user_id'];
            $data['user_name'] = $session_data['user_name'];

            /*echo "<pre>"; print_r($_POST);echo "</pre>";
            echo "<pre>"; print_r($this->input->post());echo "</pre>";
            exit;*/

            $status=$this->input->post('status');
            $billing_id=$this->input->post('udf1');
            if($status=='failure')
            {
            	$this->plans_model->delete_billing($billing_id);
            }

            $this->load->view('front/paymentsfailure',$data);
		   
			
         
        } else {
        	
            $session_data = $this->session->userdata('front_logged_in');
            $data['user_id'] = $session_data['user_id'];
            $data['user_name'] = $session_data['user_name'];
            
			redirect('home');
        }
	}



	public function payfailure(){
		


		if ($this->session->userdata('front_logged_in')) {
            $session_data = $this->session->userdata('front_logged_in');
            $data['user_id'] = $session_data['user_id'];
            $data['user_name'] = $session_data['user_name'];
            $user_id = $session_data['user_id'];


            $this->load->view('front/paymentsfailure',$data);
		   
			
         
        } else {
        	
            $session_data = $this->session->userdata('front_logged_in');
            $data['user_id'] = $session_data['user_id'];
            $data['user_name'] = $session_data['user_name'];
            
			redirect('home');
        }
	}



/////////////////////////////////////By Papri//////////////////////////////


public function tbuser_bill_update(){
if ($this->session->userdata('front_logged_in')) {
            $session_data = $this->session->userdata('front_logged_in');
            $data['user_id'] = $session_data['user_id'];
            $data['user_name'] = $session_data['user_name'];
            $user_id = $session_data['user_id'];
             $plan_credit=$this->input->post('plan_credit');
            $plan_amount=$this->input->post('plan_amount');

            
					
 $updateuserrole = $this->plans_model->tbuser_billing_update($plan_credit,$plan_amount,$user_id);

 // return $report;

        }else{


         $session_data = $this->session->userdata('front_logged_in');
            $data['user_id'] = $session_data['user_id'];
            $data['user_name'] = $session_data['user_name'];
            
			//redirect('home');
        }


}





public function paylatersuccess_old(){



if ($this->session->userdata('front_logged_in')) {
$session_data = $this->session->userdata('front_logged_in');
$data['user_id'] = $session_data['user_id'];
$data['user_name'] = $session_data['user_name'];


// echo "<pre>"; print_r($_POST);echo "</pre>";
// echo "<pre>"; print_r($this->input->post());echo "</pre>";


$billing_id=$this->input->post('billing_id');
$status='success';
if($status=='success')
{
$data = array(
'billing_date_time'=>date("Y-m-d H:i:s"),
'billing_status'=>'Completed',
'payment_status'=>'Notpaid',
);

$updateuserrole = $this->plans_model->billing_update($data,$billing_id);

}





/////////////////////////////Send mail start////////////////////////////////



/////////////////////////////Send mail end////////////////////////////////




$this->load->view('front/paymentssuccess',$billing_id);



} else {

$session_data = $this->session->userdata('front_logged_in');
$data['user_id'] = $session_data['user_id'];
$data['user_name'] = $session_data['user_name'];

redirect('home');
}
}



public function Ccavenuesuccess(){

        if ($this->session->userdata('front_logged_in')) {
            $session_data = $this->session->userdata('front_logged_in');
            $data['user_id'] = $session_data['user_id'];
            $data['user_name'] = $session_data['user_name'];
            $user_id = $session_data['user_id'];        
            // echo "<pre>"; print_r($_POST);echo "</pre>";

            //echo "<pre>"; print_r($this->input->post());echo "</pre>";
            $billing_id=$this->input->post('orderNo');

        
        $ccvqueryKEY=$this->db->query("select * from  tbl_settings  WHERE setting_key='CCAVENUE_WORKING_KEY' ");
     $ccvuserDetailsKEY= $ccvqueryKEY->result_array();
     $CCAVENUE_WORKING_KEY=$ccvuserDetailsKEY[0]['setting_value'];

     $workingKey=$CCAVENUE_WORKING_KEY;          //Working Key should be provided here.
        $encResponse=$_POST["encResp"];         //This is the response sent by the CCAvenue Server
        $rcvdString=$this->decrypt($encResponse,$workingKey);      //Crypto Decryption used as per the specified working key.
        $order_status="";
        $decryptValues=explode('&', $rcvdString);
        $dataSize=sizeof($decryptValues);

     // echo "<pre>"; print_r($decryptValues);
        for($i = 0; $i < $dataSize; $i++) 
        {
         $information=explode('=',$decryptValues[$i]);
         if($i==3)   $order_status=$information[1];
        }
        
       // echo $order_status;
         if($order_status==="Success")
          {

           
            $billquery=$this->db->query("select * from  tbl_users_billing  WHERE billing_id='$billing_id' ");
            $billDetails= $billquery->result_array();

            $plan_id=$billDetails[0]['billing_plan_id'];
            $payment_credit=$billDetails[0]['billing_plan_credit'];
            $payment_amount=$billDetails[0]['billing_plan_amount'];
        
                $pautoquery = "SELECT * FROM `tbl_users` where user_id='$user_id'  ";
                // echo $pautoquery;
                $query2=$this->db->query($pautoquery);
                $usrinfo= $query2->result_array();
                $total_amount=$usrinfo[0]['total_amount'];
                $total_credit=$usrinfo[0]['total_credit'];

                $current_amount=$total_amount+$payment_amount;
                $current_credit=$total_credit+$payment_credit;
                $payment_status="Paid";

                $userdata=array(
                'total_amount' => $current_amount,
                'total_credit' => $current_credit);

                $this->db->where('user_id', $user_id);
                $report = $this->db->update('tbl_users', $userdata);




                	/***************************updating Style page****************************/
//ppppppppppppppppp

		$pautoquery = "SELECT * FROM `tbl_style` where user_id='$user_id' and style_payment_status='Not paid' order by sty_id asc ";
		// echo $pautoquery;
		$query2=$this->db->query($pautoquery);
		$styleinfo= $query2->result_array();

		$tot_payment_amount=$payment_amount;
		$tot_payment_credit=$payment_credit;

		if(!empty($styleinfo))
		{
			foreach($styleinfo as $key => $value)
			{
				$sty_id=$value['sty_id'];
				$style_amount=$value['style_amount'];
				$style_credit=$value['style_credit'];
				$style_pending_amount=$value['style_pending_amount'];
				$style_pending_credit=$value['style_pending_credit'];
				$style_payment_status=$value['style_payment_status'];



				if(strstr($style_pending_amount,"-") && strstr($style_pending_credit,"-"))
				{
					//echo "<br>".$tot_payment_amount."--".trim($style_pending_amount,"-")."--".$sty_id."<br>";

					$style_pending_amount=$tot_payment_amount-trim($style_pending_amount,"-");
					$style_pending_credit=$tot_payment_credit-trim($style_pending_credit,"-");

					

					

					if($style_pending_amount>=0 )
					{
						$this->db->set('style_payment_status', 'Paid'); 
						//$this->db->set('comments', '');
						//$this->db->set('status_type', '');

						$this->db->set('style_pending_amount', '0');
					    $this->db->set('style_pending_credit', '0'); 


						$tot_payment_amount=$style_pending_amount;
						$tot_payment_credit=$style_pending_credit;

					
					}
					else 
					{
						$this->db->set('style_pending_amount', $style_pending_amount);
					    $this->db->set('style_pending_credit', $style_pending_credit); 

					   $this->db->set('style_payment_status', 'Not paid'); 


						$tot_payment_amount=$style_pending_amount;
						$tot_payment_credit=$style_pending_credit;
					}

					//$this->db->where('zipfile_name', $sku);
					$array = array('sty_id' => $sty_id);
					$this->db->where($array); 
					$result=$this->db->update('tbl_style');


				}

				






			}

		}



//exit;
  /************************************************************************/











                $data=array(
                'user_id' => $user_id,
                'billing_id' => $billing_id,
                'sty_id' => '0',
                'upimg_id' => '0',
                'history_type' => 'Addition',
                'amount' => $payment_amount,
                'credit' => $payment_credit,
                'account_history_date' => date("Y-m-d"),
                'account_history_time' => date("H:i:s"),
                'payment_status' => 'Paid',
                );
            $this->db->insert('tbl_account_history', $data); 
            // $payment_status=$this->input->post('payment_status'); 

            // if($payment_status=='Completed')
            // {
                $data = array(
                    'billing_date_time'=>date("Y-m-d H:i:s"),
                    'billing_status'=>'Completed',
                    'payment_status'=>'Paid',
                    );
                    
                  

                $updateuserrole = $this->plans_model->billing_update($data,$billing_id);
            // }
          $data['billing_id'] = $billing_id;

            $query=$this->db->query("select * from  tbl_users  WHERE user_id='$user_id' ");
            $userDetails= $query->result_array();
            $user_name=$userDetails[0]['user_name'];
            $user_email=$userDetails[0]['user_email'];
            $user_mobile=$userDetails[0]['user_mobile'];
            $billquery=$this->db->query("select * from  tbl_users_billing  WHERE billing_id='$billing_id' ");
            $billDetails= $billquery->result_array();
            $billing_plan_id=$billDetails[0]['billing_plan_id'];
            $billing_plan_credit=$billDetails[0]['billing_plan_credit'];
            $billing_plan_amount=$billDetails[0]['billing_plan_amount'];
            $currency=$billDetails[0]['currency'];
            $billing_date_time=$billDetails[0]['billing_date_time'];
            $billing_status=$billDetails[0]['billing_status'];
            $payment_status=$billDetails[0]['payment_status'];
            $payment_type=$billDetails[0]['payment_type'];
            $query=$this->db->query("select tbl_plan.*,tbl_plantype.plntypname from  tbl_plan
            INNER JOIN  tbl_plantype on tbl_plan.plntyid=tbl_plantype.plntyid
            WHERE tbl_plan.planid='$billing_plan_id'");
            $plansDetails=$query->result_array();
            $plan_name=$plansDetails[0]['plan_name'];
          
                $mail_text='Payment Email Piquic- CCAvenue';
                $subject='Piquic- Payment success';
       

        $query=$this->db->query("select * from  tbl_settings  WHERE setting_key='MAIL_SMTP_USER' ");
        $userDetails= $query->result_array();
        $MAIL_SMTP_USER=$userDetails[0]['setting_value'];
        $query=$this->db->query("select * from  tbl_settings  WHERE setting_key='MAIL_SMTP_PASSWORD' ");
        $userDetails= $query->result_array();
        $MAIL_SMTP_PASSWORD=$userDetails[0]['setting_value'];
        $query=$this->db->query("select * from  tbl_settings  WHERE setting_key='MAIL_BODY' ");
        $userDetails= $query->result_array();
        $MAIL_BODY=$userDetails[0]['setting_value'];
        $this->load->library('email');
        $config = array();
        //$config['protocol'] = 'smtp';
        $config['protocol']     = 'mail';
        $config['smtp_host'] = 'localhost';
        $config['smtp_user'] = $MAIL_SMTP_USER;
        $config['smtp_pass'] = $MAIL_SMTP_PASSWORD;
        $config['smtp_port'] = 25;

        $this->email->initialize($config);
        $this->email->set_newline("\r\n");        
        $from_email = "demo.intexom@gmail.com";
        $to_email = $user_email;
        $billing_date_time=date("d/m/y",strtotime($billing_date_time));
        $copy_right= date("Y");
        $find_arr = array("##USER_NAME##","##USER_EMAIL##","##PLAN_NAME##","##PLAN_CREDIT##","##PLAN_AMOUNT##","##BILLING_DATE##","##BILLING_STATUS##","##PAYMENT_STATUS##",'##MAIL_TEXT##','##COPYRIGHT##');
        $replace_arr = array($user_name,$user_email,$plan_name,$billing_plan_credit,$billing_plan_amount,$billing_date_time,$billing_status,$payment_status,$mail_text,$copy_right);
        $message=str_replace($find_arr,$replace_arr,$MAIL_BODY);

        $this->email->from($from_email, 'Piquic');
        $this->email->to($to_email);
        $this->email->cc('poulami@mokshaproductions.in');
        $this->email->bcc('pranav@mokshaproductions.in');
        $this->email->subject($subject);
        $this->email->message($message);
        //Send mail
        if($this->email->send())
        {

            //echo "mail sent";exit;
            $this->session->set_flashdata("email_sent","Congragulation Email Send Successfully.");
        }
        
        else
        {
            //echo $this->email->print_debugger();
            //echo "mail not sent";exit;
            $this->session->set_flashdata("email_sent","You have encountered an error");
        }       

     redirect("paysuccess");
        }

        if($order_status==="Failure"){

            $data = array(
                    'billing_date_time'=>date("Y-m-d H:i:s"),
                    'billing_status'=>'Payment Failed',
                    'payment_status'=>'Not Paid',
                    );
                    
                $updateuserrole = $this->plans_model->billing_update($data,$billing_id);


        redirect("paymentfailure");
        


        }

         
        } else {
            
            $session_data = $this->session->userdata('front_logged_in');
            $data['user_id'] = $session_data['user_id'];
            $data['user_name'] = $session_data['user_name'];
            
            redirect('home');
        }



    }



/*
* @param1 : Plain String
* @param2 : Working key provided by CCAvenue
* @return : Decrypted String
*/
function encrypt($plainText,$key)
{
    $key = $this->hextobin(md5($key));
    $initVector = pack("C*", 0x00, 0x01, 0x02, 0x03, 0x04, 0x05, 0x06, 0x07, 0x08, 0x09, 0x0a, 0x0b, 0x0c, 0x0d, 0x0e, 0x0f);
    $openMode = openssl_encrypt($plainText, 'AES-128-CBC', $key, OPENSSL_RAW_DATA, $initVector);
    $encryptedText = bin2hex($openMode);
    return $encryptedText;
}

/*
* @param1 : Encrypted String
* @param2 : Working key provided by CCAvenue
* @return : Plain String
*/
function decrypt($encryptedText,$key)
{
      // echo $encryptedText;
      // echo $key;


    $key = $this->hextobin(md5($key));
    $initVector = pack("C*", 0x00, 0x01, 0x02, 0x03, 0x04, 0x05, 0x06, 0x07, 0x08, 0x09, 0x0a, 0x0b, 0x0c, 0x0d, 0x0e, 0x0f);
    $encryptedText = $this->hextobin($encryptedText);
    $decryptedText = openssl_decrypt($encryptedText, 'AES-128-CBC', $key, OPENSSL_RAW_DATA, $initVector);
    return $decryptedText;
}

function hextobin($hexString) 
 { 
    $length = strlen($hexString); 
    $binString="";   
    $count=0; 
    while($count<$length) 
    {       
        $subString =substr($hexString,$count,2);           
        $packedString = pack("H*",$subString); 
        if ($count==0)
        {
            $binString=$packedString;
        } 
        
        else 
        {
            $binString.=$packedString;
        } 
        
        $count+=2; 
    } 
        return $binString; 
  } 



public function paytmsuccess(){

if ($this->session->userdata('front_logged_in')) {
$session_data = $this->session->userdata('front_logged_in');
$data['user_id'] = $session_data['user_id'];
$data['user_name'] = $session_data['user_name'];
$billing_id=$this->input->post('orderNo');

$billquery=$this->db->query("select * from tbl_users_billing WHERE billing_id='$billing_id' ");
$billDetails= $billquery->result_array();
$plan_id=$billDetails[0]['billing_plan_id'];
$payment_credit=$billDetails[0]['billing_plan_credit'];
$payment_amount=$billDetails[0]['billing_plan_amount'];

$pautoquery = "SELECT * FROM `tbl_users` where user_id='$user_id' ";
$query2=$this->db->query($pautoquery);
$usrinfo= $query2->result_array();
$total_amount=$usrinfo[0]['total_amount'];
$total_credit=$usrinfo[0]['total_credit'];
$current_amount=$total_amount+$payment_amount;
$current_credit=$total_credit+$payment_credit;
$payment_status="Paid";
$userdata=array(
'total_amount' => $current_amount,
'total_credit' => $current_credit);
$this->db->where('user_id', $user_id);
$report = $this->db->update('tbl_users', $userdata);

$data=array(
'user_id' => $user_id,
'billing_id' => $billing_id,
'sty_id' => '0',
'upimg_id' => '0',
'history_type' => 'Addition',
'amount' => $payment_amount,
'credit' => $payment_credit,
'account_history_date' => date("Y-m-d"),
'account_history_time' => date("H:i:s"),
'payment_status' => 'Paid',
);
$this->db->insert('tbl_account_history', $data); 
$data = array(
'billing_date_time'=>date("Y-m-d H:i:s"),
'billing_status'=>'Completed',
'payment_status'=>'Paid',
); 

$updateuserrole = $this->plans_model->billing_update($data,$billing_id);
$data['billing_id'] = $billing_id;

$query=$this->db->query("select * from tbl_users WHERE user_id='$user_id' ");
$userDetails= $query->result_array();
$user_name=$userDetails[0]['user_name'];
$user_email=$userDetails[0]['user_email'];
$user_mobile=$userDetails[0]['user_mobile'];
$billquery=$this->db->query("select * from tbl_users_billing WHERE billing_id='$billing_id' ");
$billDetails= $billquery->result_array();
$billing_plan_id=$billDetails[0]['billing_plan_id'];
$billing_plan_credit=$billDetails[0]['billing_plan_credit'];
$billing_plan_amount=$billDetails[0]['billing_plan_amount'];
$currency=$billDetails[0]['currency'];
$billing_date_time=$billDetails[0]['billing_date_time'];
$billing_status=$billDetails[0]['billing_status'];
$payment_status=$billDetails[0]['payment_status'];
$payment_type=$billDetails[0]['payment_type'];
$query=$this->db->query("select tbl_plan.*,tbl_plantype.plntypname from tbl_plan
INNER JOIN tbl_plantype on tbl_plan.plntyid=tbl_plantype.plntyid
WHERE tbl_plan.planid='$billing_plan_id'");
$plansDetails=$query->result_array();
$plan_name=$plansDetails[0]['plan_name'];

$mail_text='Payment Email Piquic- Paytm';
$subject='Piquic- Payment success';


$query=$this->db->query("select * from tbl_settings WHERE setting_key='MAIL_SMTP_USER' ");
$userDetails= $query->result_array();
$MAIL_SMTP_USER=$userDetails[0]['setting_value'];
$query=$this->db->query("select * from tbl_settings WHERE setting_key='MAIL_SMTP_PASSWORD' ");
$userDetails= $query->result_array();
$MAIL_SMTP_PASSWORD=$userDetails[0]['setting_value'];
$query=$this->db->query("select * from tbl_settings WHERE setting_key='MAIL_BODY' ");
$userDetails= $query->result_array();
$MAIL_BODY=$userDetails[0]['setting_value'];
$this->load->library('email');
$config = array();
//$config['protocol'] = 'smtp';
$config['protocol'] = 'mail';
$config['smtp_host'] = 'localhost';
$config['smtp_user'] = $MAIL_SMTP_USER;
$config['smtp_pass'] = $MAIL_SMTP_PASSWORD;
$config['smtp_port'] = 25;

$this->email->initialize($config);
$this->email->set_newline("\r\n"); 
$from_email = "demo.intexom@gmail.com";
$to_email = $user_email;
$billing_date_time=date("d/m/y",strtotime($billing_date_time));
$copy_right= date("Y");
$find_arr = array("##USER_NAME##","##USER_EMAIL##","##PLAN_NAME##","##PLAN_CREDIT##","##PLAN_AMOUNT##","##BILLING_DATE##","##BILLING_STATUS##","##PAYMENT_STATUS##",'##MAIL_TEXT##','##COPYRIGHT##');
$replace_arr = array($user_name,$user_email,$plan_name,$billing_plan_credit,$billing_plan_amount,$billing_date_time,$billing_status,$payment_status,$mail_text,$copy_right);
$message=str_replace($find_arr,$replace_arr,$MAIL_BODY);

$this->email->from($from_email, 'Piquic');
$this->email->to($to_email);
$this->email->cc('poulami@mokshaproductions.in');
$this->email->bcc('pranav@mokshaproductions.in');
$this->email->subject($subject);
$this->email->message($message);
//Send mail
if($this->email->send())
{

//echo "mail sent";exit;
$this->session->set_flashdata("email_sent","Congragulation Email Send Successfully.");
}

else
{
//echo $this->email->print_debugger();
//echo "mail not sent";exit;
$this->session->set_flashdata("email_sent","You have encountered an error");
} 

redirect("paysuccess");


} else {

$session_data = $this->session->userdata('front_logged_in');
$data['user_id'] = $session_data['user_id'];
$data['user_name'] = $session_data['user_name'];

redirect('home');
}

}



/////////////////////////////////////end Papri//////////////////////////////









}

?>