<?php 
class Booking_payments extends CI_Controller{
	
	function __construct(){
		parent::__construct();
		$this->load->library(['session']); 
        $this->load->helper(['url','file','form']);
        $this->load->model('plans_model'); //load model upload 
        $this->load->model('Revbook_model');
	}

	public function index($book_id){

		if ($this->session->userdata('front_logged_in')) {
            $session_data = $this->session->userdata('front_logged_in');
            $data['user_id'] = $session_data['user_id'];
            $data['user_name'] = $session_data['user_name'];
			// $data['book_id']=$book_id;

            $query = $this->db->get_where('tbl_booking', array('book_id'=>$book_id));
			//$query = $this->db->get("tbl_upload_img");
			$result=$query->result_array(); 
	      	$plan_id=$result[0]['plan_type'];
			$query1 = $this->db->get_where('tbl_plan', array('planid'=>$plan_id,'plan_for'=>'booking'));
			//$query = $this->db->get("tbl_upload_img");
			$result1=$query1->result_array(); 
			//print_r($result1);
	        $payment_details['user_id']=$session_data['user_id'];
			$payment_details['book_id']=$book_id;
			//$payment_details['billing_id']=$billing_id;
			//$payment_details['billing_unique_id']=$billing_unique_id;
			$payment_details['plan_id']=$result[0]['plan_type'];
			$payment_details['plan_name']=$result1[0]['plan_name'];
			$payment_details['total_image']=$result[0]['total_image'];
			$payment_details['total_price']=$result[0]['total_price'];
			$payment_details['billing_status']='Pending';
			$payment_details['payment_status']='Notpaid';
			$payment_details['currency']=$result[0]['currency'];

			$payment_details['hash'] = '';
			$payment_details['udf1'] = '';
			$payment_details['udf2'] = '';
			$payment_details['udf3'] = '';
			$payment_details['udf4'] = '';
			$payment_details['udf5'] = '';

	        //print_r( $payment_details);
			$this->load->view('front/booking_payments',$payment_details);

        } else {
        	
            $session_data = $this->session->userdata('front_logged_in');
            $data['user_id'] = $session_data['user_id'];
            $data['user_name'] = $session_data['user_name'];
            
			redirect('home');
        }
	}


	public function booking_paymentdetails($book_id){
		


		if ($this->session->userdata('front_logged_in')) {
            $session_data = $this->session->userdata('front_logged_in');
            $data['user_id'] = $session_data['user_id'];
            $data['user_name'] = $session_data['user_name'];

           // print_r($book_id);
            $query = $this->db->get_where('tbl_booking', array('book_id'=>$book_id));
			//$query = $this->db->get("tbl_upload_img");
			$result=$query->result_array(); 
			$plan_id=$result[0]['plan_type'];
			$query1 = $this->db->get_where('tbl_plan', array('planid'=>$plan_id,'plan_for'=>'booking'));
			//$query = $this->db->get("tbl_upload_img");
			$result1=$query1->result_array(); 
			//print_r($result1);
	        $payment_details['user_id']=$session_data['user_id'];
			$payment_details['book_id']=$book_id;
			//$payment_details['billing_id']=$billing_id;
			//$payment_details['billing_unique_id']=$billing_unique_id;
			$payment_details['plan_id']=$result[0]['plan_type'];
			$payment_details['plan_name']=$result1[0]['plan_name'];
			$payment_details['total_image']=$result[0]['total_image'];
			$payment_details['total_price']=$result[0]['total_price'];
			$payment_details['billing_status']='Pending';
			$payment_details['payment_status']='Notpaid';
			$payment_details['currency']=$result[0]['currency'];

			$payment_details['action'] = '';
			$payment_details['hash'] = '';
			$payment_details['udf1'] = '';
			$payment_details['udf2'] = '';
			$payment_details['udf3'] = '';
			$payment_details['udf4'] = '';
			$payment_details['udf5'] = '';

			$this->load->view('front/booking_payments',$payment_details);
			//$this->load->view('front/booking_payment',$data);
			
         
        } else {
        	
            $session_data = $this->session->userdata('front_logged_in');
            $data['user_id'] = $session_data['user_id'];
            $data['user_name'] = $session_data['user_name'];
            
			redirect('home');
        }
	}


	public function payumoney_old($plan_id){
		if ($this->session->userdata('front_logged_in')) {
            $session_data = $this->session->userdata('front_logged_in');
            $data['user_id'] = $session_data['user_id'];
            $data['user_name'] = $session_data['user_name'];
            $user_id = $session_data['user_id'];

           	$plantype_id=$this->input->post('hid_plantype_id');
            //echo $plan_id=$this->input->post('plan_id');
            $plan_amount=$this->input->post('hid_plan_amount');
            $plan_credit=$this->input->post('hid_plan_credit');


            $currency=$this->session->userdata('sess_currency');
            
			//$data['plansTypeDetails'] = $this->plans_model->getAllPlans();
			//$data['billingDetails'] = $this->plans_model->getAllbilling($data['user_id']);
			$data['plantype_id'] = $plantype_id;
			$data['plan_id'] = $plan_id;
			$data['plansDetails'] = $this->plans_model->get_plansById($plan_id);
			$data['plan_amount'] = $plan_amount;
			$data['plan_credit'] = $plan_credit;

			$data['currency'] = $currency;




			// Merchant key here as provided by PayUMoney
			$MERCHANT_KEY = $this->input->post('key');

			// Merchant Salt as provided by Payu
			$SALT = $this->input->post('salt');

			// End point - change to https://secure.payu.in for if LIVE mode
			$PAYU_BASE_URL = $this->input->post('payu_base_url');

			$action = '';

			$posted = array();
			if(!empty($_POST)) {
			//print_r($_POST);exit;
			foreach($_POST as $key => $value) {    
			$posted[$key] = $value; 

			}
			}

			$formError = 0;

			if(empty($posted['txnid'])) {
			// Generate random transaction id
			$txnid = substr(hash('sha256', mt_rand() . microtime()), 0, 20);
			} else {
			$txnid = $posted['txnid'];
			}
			$hash = '';
			// Hash Sequence
			$hashSequence = "key|txnid|amount|productinfo|firstname|email|udf1|udf2|udf3|udf4|udf5|udf6|udf7|udf8|udf9|udf10";
			if(empty($posted['hash']) && sizeof($posted) > 0) {
			if(
			empty($posted['key'])
			|| empty($posted['txnid'])
			|| empty($posted['amount'])
			|| empty($posted['firstname'])
			|| empty($posted['email'])
			|| empty($posted['phone'])
			|| empty($posted['productinfo'])
			|| empty($posted['surl'])
			|| empty($posted['furl'])
			|| empty($posted['service_provider'])
			) {
			$formError = 1;
			} else {
			//$posted['productinfo'] = json_encode(json_decode('[{"name":"tutionfee","description":"","value":"500","isRequired":"false"},{"name":"developmentfee","description":"monthly tution fee","value":"1500","isRequired":"false"}]'));
			$hashVarsSeq = explode('|', $hashSequence);
			$hash_string = '';	
			foreach($hashVarsSeq as $hash_var) {
			$hash_string .= isset($posted[$hash_var]) ? $posted[$hash_var] : '';
			$hash_string .= '|';
			}

			$hash_string .= $SALT;


			$hash = strtolower(hash('sha512', $hash_string));
			$action = $PAYU_BASE_URL . '/_payment';
			}
			} elseif(!empty($posted['hash'])) {
			$hash = $posted['hash'];
			$action = $PAYU_BASE_URL . '/_payment';
			}


//////////////////////////////////////////////////////////////////////////////////

			
			$data['action'] = $action;
			$data['hash'] = $hash;
			$data['MERCHANT_KEY'] = $MERCHANT_KEY;
			$data['SALT'] = $SALT;
			$data['txnid'] = $this->input->post('txnid');
			$data['PAYU_BASE_URL'] = $this->input->post('payu_base_url');
			$data['amount'] = $this->input->post('amount');
			$data['firstname'] = $this->input->post('firstname');
			$data['email'] = $this->input->post('email');
			$data['phone'] = $this->input->post('phone');
			$data['productinfo'] = $this->input->post('productinfo');
			$data['surl'] = $this->input->post('surl');
			$data['furl'] = $this->input->post('furl');
			$data['udf1'] = $this->input->post('udf1');
			$data['udf2'] = $this->input->post('udf2');
			$data['udf3'] = $this->input->post('udf3');
			$data['udf4'] = $this->input->post('udf4');
			$data['udf5'] = $this->input->post('udf5');

		    $this->load->view('front/payments',$data);

            


        }else{


         $session_data = $this->session->userdata('front_logged_in');
            $data['user_id'] = $session_data['user_id'];
            $data['user_name'] = $session_data['user_name'];
            
			//redirect('home');
        }


}


	public function insert_billing(){
		
		if ($this->session->userdata('front_logged_in')) {
            $session_data = $this->session->userdata('front_logged_in');
            $data['user_id'] = $session_data['user_id'];
            $data['user_name'] = $session_data['user_name'];
            $user_id = $session_data['user_id'];


           
            $billing_id=$this->input->post('billing_id');
            $book_id=$this->input->post('book_id');
            $payment_type_id=$this->input->post('payment_type_id');
            $plan_id=$this->input->post('plan_id');
            $plan_credit=$this->input->post('plan_credit');
            $plan_amount=$this->input->post('plan_amount');
            $currency=$this->input->post('currency');
            $billing_unique_id=md5(uniqid(mt_rand(), true)).$user_id;

            $plansTypeDetails = $this->plans_model->get_plansById($plan_id);
            if($plansTypeDetails[0]['plntypname']=="Pay Monthly")
            {
            	$total_credit=$plansTypeDetails[0]['plan_credit']-$plan_credit;
            }
            else{
            	$total_credit=$plan_credit;
            }
           // exit();

            if($billing_id!='')
			{
			  	$data = array(
				    'user_id'=>$user_id,
					'billing_unique_id'=>$billing_unique_id,
					'billing_plan_id'=>$plan_id,
					'billing_plan_credit'=>$total_credit,
					'billing_plan_amount'=>$plan_amount,
					'billing_date_time'=>date("Y-m-d H:i:s"),
					'billing_status'=>'Pending',
					'payment_type'=>$payment_type_id,
					'payment_status'=>'Notpaid',
					'payment_form'=>'1',
					'currency'=>$currency,
					'book_id'=>$book_id,
				);
					
				  $updateuserrole = $this->plans_model->billing_update($data,$billing_id);
			      echo $billing_id;
				
			}else {

			     $data = array(
				    'user_id'=>$user_id,
					'billing_unique_id'=>$billing_unique_id,
					'billing_plan_id'=>$plan_id,
					'billing_plan_credit'=>$total_credit,
					'billing_plan_amount'=>$plan_amount,
					'billing_date_time'=>date("Y-m-d H:i:s"),
					'billing_status'=>'Pending',
					'payment_type'=>$payment_type_id,
					'payment_status'=>'Notpaid',
					'payment_form'=>'1',
					'currency'=>$currency,
					'book_id'=>$book_id,
				);
				$this->plans_model->billing_insert($data);
				$billing_id=$this->db->insert_id();
				echo $billing_id;
			}
		    //redirect('home');
        }else {
        	
            $session_data = $this->session->userdata('front_logged_in');
            $data['user_id'] = $session_data['user_id'];
            $data['user_name'] = $session_data['user_name'];
            
			//redirect('home');
        }
	}

	public function paymentsuccess(){
		if ($this->session->userdata('front_logged_in')) {
            $session_data = $this->session->userdata('front_logged_in');
            $data['user_id'] = $session_data['user_id'];
            $data['user_name'] = $session_data['user_name'];
            $user_id = $session_data['user_id'];


            //echo "<pre>"; print_r($_POST);echo "</pre>";
            //echo "<pre>"; print_r($this->input->post());echo "</pre>";
            

			$billing_id=$this->input->post('udf1');
            $payment_amount=$this->input->post('udf2');
            $payment_credit=$this->input->post('udf3');

       
  /************************************************************************/
	 $status=$this->input->post('status');  
		$billing_id=$this->input->post('udf1');

		if($status=='success'){
			$data=array(
				'user_id' => $user_id,
				'billing_id' => '0',
				'sty_id' => '0',
				'upimg_id' => '0',
				'history_type' => 'Addition',
				'amount' => $payment_amount,
				'credit' => $payment_credit,
				'account_history_date' => date("Y-m-d"),
				'account_history_time' => date("H:i:s"),
				'payment_status' => 'Paid',
			);
			$this->db->insert('tbl_account_history', $data);

		}
				



      /************************************************************************/
            

          
            if($status=='success')
            {
            	$data = array(
				    'billing_date_time'=>date("Y-m-d H:i:s"),
					'billing_status'=>'Completed',
					'payment_status'=>'Paid',
					);
					
			    $updateuserrole = $this->plans_model->billing_update($data,$billing_id);

			  	$status_query1 = $this->db->get_where('tbl_account_cal', array('user_id'=>$user_id,'plan_status'=>'InActive','payment_status'=>'Not Paid'));
				//$query = $this->db->get("tbl_upload_img");
				$status_result1=$status_query1->result_array(); 
				//print_r($status_result1);exit();
				if(!empty($status_result1))
				{
					$account_cal_id=$status_result1[0]['account_cal_id'];
					$account_cal_details['payment_status']="Paid";
					$account_cal_details['plan_status']="Active";
					$result1=$this->Revbook_model->update_account_cal($account_cal_details,$account_cal_id);
					//$account_cal_details['plan_status']="Pending";
				}
            }
            $data['billing_id'] = $billing_id;


			$query=$this->db->query("select * from  tbl_users  WHERE user_id='$user_id' ");
			$userDetails= $query->result_array();
			$user_name=$userDetails[0]['user_name'];
			$user_email1=$userDetails[0]['user_email'];
			$user_email_value=explode("@",$user_email1);
			$user_email=substr_replace($user_email_value[0], "*****", 2)."@".$user_email_value[1];
			$user_mobile=$userDetails[0]['user_mobile'];
			$user_company=$userDetails[0]['user_company'];
			$user_address=$userDetails[0]['user_address'];




			$billquery=$this->db->query("select * from  tbl_users_billing  WHERE billing_id='$billing_id' ");
			$billDetails= $billquery->result_array();

			$billing_plan_id=$billDetails[0]['billing_plan_id'];

			$billing_plan_credit=$billDetails[0]['billing_plan_credit'];
			$billing_plan_amount=$billDetails[0]['billing_plan_amount'];
			$currency=$billDetails[0]['currency'];
			$billing_date_time=$billDetails[0]['billing_date_time'];
			$billing_status=$billDetails[0]['billing_status'];
			$payment_status=$billDetails[0]['payment_status'];
			$payment_type=$billDetails[0]['payment_type'];

			$payment_type_id=$billDetails[0]['payment_type'];
			//print_r($billDetails);
			$payment_type_query1=$this->db->query("select * FROM tbl_paymenttype where paytyid=$payment_type_id");

			$payment_typeDetails1= $payment_type_query1->result_array();
			$payment_type_name=$payment_typeDetails1[0]['paytypname'];
			
			$plansTypeDetails = $this->plans_model->get_plansById($billing_plan_id);
            // print_r($plansTypeDetails);
			
			$plntypname=$plansTypeDetails[0]['plntypname'];
			

			$query1 = $this->db->get_where('tbl_plan', array('planid'=>$plan_id,'plan_for'=>'booking'));
		//$query = $this->db->get("tbl_upload_img");
		$result1=$query1->result_array(); 
		//print_r($result1);

		$plan_amount=$result1[0]['plan_amount'];
		$plan_credit=$result1[0]['plan_credit'];
		$plntyid=$result1[0]['plntyid'];
		$plan_name=$result1[0]['plan_name'];


// 			if($plntypname=="Pay Monthly")
// 			{
// 			 /************************************************************************/
// 				$pautoquery = "SELECT * FROM `tbl_users` where user_id='$user_id'  ";
// 				// echo $pautoquery;
// 				$query2=$this->db->query($pautoquery);
// 				$usrinfo= $query2->result_array();
// 				 $total_amount=$usrinfo[0]['total_amount'];
// 				$total_credit=$usrinfo[0]['total_credit'];
				

// 				$current_amount=$total_amount+$billing_plan_amount;
// 				$current_credit=$total_credit+$billing_plan_credit;
// 				$payment_status="Paid";

			




// 				$userdata=array(
// 				'total_amount' => $current_amount,
// 				'total_credit' => $current_credit);

// 				$this->db->where('user_id', $user_id);
// 				$report = $this->db->update('tbl_users', $userdata);

// }
  /************************************************************************/

			echo $book_id=$billDetails[0]['book_id']; 
 // exit();
$array = array('book_id' => $book_id);
				$this->db->set('p_status', 'Paid'); 
				$this->db->where($array); 
				$result=$this->db->update('tbl_booking');

			if(!empty($book_id)){
				$array = array('book_id' => $book_id);
				$this->db->set('p_status', 'Paid'); 
				$this->db->where($array); 
				$result=$this->db->update('tbl_booking');
			}
			//exit();
			$query=$this->db->query("select * from  tbl_booking where book_id=$book_id and p_status='Paid'");
			$bookDetails=$query->result_array();


						
			$order_id=$bookDetails[0]['order_id'];
			$gender=$bookDetails[0]['gender'];
			$m_status=$bookDetails[0]['m_status'];
			$category=array_filter(explode(",", $bookDetails[0]['category']));	
			$quty=array_filter(explode(",", $bookDetails[0]['quty']));
			$angle=array_filter(explode(",", $bookDetails[0]['angle']));
			$d_status=$bookDetails[0]['d_status'];
			$notes=$bookDetails[0]['notes'];
			$total_price=$bookDetails[0]['total_price'];
			$total_image=$bookDetails[0]['total_image'];
			$create_date=$bookDetails[0]['create_date'];
			$book_date_time=date("D, M d ,Y H:i A",strtotime($create_date));
			//$plan_id=$bookDetails[0]['plan_id'];
			$currency=$bookDetails[0]['currency'];

			$plan_left_credit=$bookDetails[0]['plan_left_credit'];
			$plan_left_amt=$bookDetails[0]['plan_left_amt'];
			$plan_used_amt=$bookDetails[0]['plan_used_amt'];
			$plan_used_credit=$bookDetails[0]['plan_used_credit'];
			//$currency=$bookDetails[0]['currency'];

			$p_status=$bookDetails[0]['p_status'];


			  if($currency=="USD") { $currency1="&#36;"; }
              elseif($currency=="INR") { $currency1="&#x20B9;"; }
              elseif($currency=="EUR") { $currency1="&#128;"; }
              elseif($currency=="GBP") { $currency1="&#163;"; }
              elseif($currency=="AUD") { $currency1="&#36;"; }
              elseif($currency=="CNY") { $currency1="&#165;"; }


			if($gender=="1"){ $gender1= "Male" ; } 
            if($gender=="2"){ $gender1="Female" ; } 
            if($gender=="4"){ $gender1="Boy" ; } 
            if($gender=="6"){ $gender1="Girl" ; }
            if($m_status=="1"){
	           $m_status1= "Let \"Piquic Style Expert\" select my models & style." ; 
	         } 
	         else{ 
	          $m_status1= "I want to select my models and styling once my apparels gets digitized."; 
	         } 
	         if($d_status=="1"){
               $d_status1= "Standard (6-7 days after receiving your products)." ; 
             } 
             else{ 
               $d_status1= "Express (2-3 days after receiving your products)."; 
             } 
			// print_r($bookDetails);exit();
            //$user_email="demo.intexom@gmail.com";
            if($payment_type=='1')
            {
            	$mail_text='';
            	$subject='Piquic - Payment success';
            }
            elseif($payment_type=='2')
            {
            	$mail_text='';
            	$subject='Piquic - Payment success';
            }
            elseif($payment_type=='3')
            {
            	$mail_text='';
            	$subject='Piquic - Payment success';
            }
		    
		    elseif($payment_type=='4')
            {
            	$mail_text='';
            	$subject='Piquic - Payment success';
            }
		    
		    elseif($payment_type=='5')
            {
            	$mail_text='';
            	$subject='Piquic - Payment success';
            }
            elseif($payment_type=='6')
            {
            	$mail_text='Payment Email Piquic - Pay-Later';
            	$subject='Piquic - Payment Pending';
            }
		    
		    		    $table_book_body="";
            if($plntypname=="Pay as you go" || $plntypname=="Pay Monthly"){
            	$table_book_body.="<h4>Booking Details</h4>";
            	$table_book_body.='
            		<table class="table">
                             <thead>';
	            $table_book_body.= '<tr>
	                  <th class="border-0 text-uppercase small font-weight-bold">ID</th>
	                  <th class="border-0 text-uppercase small font-weight-bold">Gender</th>
	                  <th class="border-0 text-uppercase small font-weight-bold">Category</th>
	                  <th class="border-0 text-uppercase small font-weight-bold">Quantity</th>
	                  <th class="border-0 text-uppercase small font-weight-bold">Angle</th>

	                </tr>';
            $table_book_body.= '</thead> <tbody>';
         
	            $table_book_body.= '<tr>
	            	<td>1</td>
	            	<td>'.$gender1. ' </td>
					<td>
	                    <table>';

                foreach ($category as $key => $value ) {
	                $table_book_body.= '<tr>
	                    <td style="border-top: none; border-bottom: 1px solid #dee2e6;">
	                     '.$value.'
	                   </td>
	                 </tr>';
                                
                }
                $table_book_body.= '</table>
                           </td>
                           <td><table>';

                foreach ($quty as $key1 => $value1 ) {
                    $table_book_body.= '<tr>
	                    <td style="border-top: none; border-bottom: 1px solid #dee2e6;">
	                     '.$value1.'
	                   	</td>
	                 	</tr>';
                 
           		} 

               $table_book_body.= '</table></td><td><table>';
              	//echo "<pre>";
              	//print_r($angle);
                unset($arr_filter);

               	foreach ($angle as $key2 => $value2 ) {
          			//echo $value2;
	                $angle_value=explode("_",$value2);
	                $cat=$angle_value[0];
	                $arr_filter[$cat][]=ucfirst($angle_value[1]);
              	}
              	//echo "<pre>";
              	//print_r($arr_filter);

              	foreach ($category as $key => $value ) {
	                $table_book_body.= '<tr>
	                  <td style="border-top: none; border-bottom: 1px solid #dee2e6;">
	                   '.implode(", ",$arr_filter[$value]) .'
	                </td>
	               	</tr>';
            
             	} 
 				$table_book_body.= '</table></td></tr>';
                
                $table_book_body.= '</tbody></table>';

            }    
               $table_plan_body="";
                if($plntypname=="Pay Monthly"){
                	$table_plan_body.="<h4>Plan Details</h4>";
                	$table_plan_body.='<table class="table">
                            <thead>';

	             	$table_plan_body.= '                   
		                <tr>
		                  <th class="border-0 text-uppercase small font-weight-bold">ID</th>
		                  <th class="border-0 text-uppercase small font-weight-bold">Plan Name</th>
		                  <th class="border-0 text-uppercase small font-weight-bold">Duatation</th>
		                  <th class="border-0 text-uppercase small font-weight-bold">Total Image</th>
		                  <th class="border-0 text-uppercase small font-weight-bold">Total Amount</th>
		                  <th class="border-0 text-uppercase small font-weight-bold">Left Image</th>
		                  <th class="border-0 text-uppercase small font-weight-bold">Left Amount</th>
		                </tr>';
				
                         
            	$table_plan_body.= '</thead><tbody>';
   			
                   $table_plan_body.= '
                    <tr>
                      <td>1</td>
                      <td>'.$plan_name.'</td>
                      <td>1 Month</td>
                      <td>'.$plan_credit.'</td>
                      <td>'.$plan_amount.'</td>
                      <td>'.$plan_left_credit.'</td>
                      <td>'.$plan_left_amt.'</td>
                    </tr>';
                     $table_plan_body.= '</tbody></table>';
                }
               
           		// print_r($table_body); //exit();
	            if($plntypname=="Pay as you go")
	            {
	            	$amount_content="Total Amount";
					$plan_content="Plan Type";
					$image_content="Total Image";
					$total_price=$total_price;
					$plntypname=$plntypname;
					$total_image=$total_image;
	            }
	            if($plntypname=='Pay Monthly')
	            {
	            	$amount_content="Used Amount";
					$plan_content="Plan Type";
					$image_content="Used Image";
					$total_price=$plan_used_amt;
					$plntypname=$plntypname;
					$total_image=$plan_used_credit;
	            }
           // print_r($table_body); //exit();
            

/*********************************Send mail*************************************/

	$query=$this->db->query("select * from  tbl_settings  WHERE setting_key='MAIL_SMTP_USER' ");
	$userDetails= $query->result_array();
	$MAIL_SMTP_USER=$userDetails[0]['setting_value'];

	$query=$this->db->query("select * from  tbl_settings  WHERE setting_key='MAIL_SMTP_PASSWORD' ");
	$userDetails= $query->result_array();
	$MAIL_SMTP_PASSWORD=$userDetails[0]['setting_value'];

	$query=$this->db->query("select * from  tbl_settings  WHERE setting_key='MAIL_BODY_BOOKING' ");
	$userDetails= $query->result_array();
	$MAIL_BODY_BOOKING=$userDetails[0]['setting_value'];

		
		//Load email library
		$this->load->library('email');

		$config = array();
		//$config['protocol'] = 'smtp';
		$config['protocol']     = 'mail';
		$config['smtp_host'] = 'localhost';
		$config['smtp_user'] = $MAIL_SMTP_USER;
		$config['smtp_pass'] = $MAIL_SMTP_PASSWORD;
		$config['smtp_port'] = 25;




		$this->email->initialize($config);
		$this->email->set_newline("\r\n");

		
		$from_email = "demo.intexom@gmail.com";
		$to_email = $user_email1;
		$copy_right= date("Y");
		$billing_date_time=date("d/m/y",strtotime($billing_date_time));

			//echo $message=$MAIL_BODY_BOOKING;
		$find_arr = array('#ORDER_ID##','##BOOK_DATE##',"##USER_NAME##","##USER_EMAIL##","##USER_MOBILE##","##USER_COMPANY##","##USER_ADDRESS##","##M_STATUS##","##D_STATUS##","##NOTES##","##P_STATUS##",'##CUR##',"##TABLE_BOOK_BODY##","##TABLE_PLAN_BODY##","##AMOUNT_CONTENT##", "##TOTAL_AMOUNT##","##PLAN_CONTENT##",'##PLAN_TYPE##',"##IMAGE_CONTENT##",'##TOTAL_IMAGE##','##COPY_RIGHT##','##PAYMENT_TYPE##');
		$replace_arr = array($order_id,$book_date_time,$user_name,$user_email,$user_mobile,$user_company,$user_address, $m_status1,$d_status1,$notes,$p_status,$currency1,$table_book_body,$table_plan_body,$amount_content,$total_price,$plan_content,$plntypname,$image_content,$total_image,$copy_right,$payment_type_name);
		$message=str_replace($find_arr,$replace_arr,$MAIL_BODY_BOOKING);
		// echo $message;exit;



		$this->email->from($from_email, 'Piquic');
		$this->email->to($to_email);
		$this->email->cc('poulami@mokshaproductions.in');
        $this->email->cc('raja.priya@mokshaproductions.in');
		$this->email->subject($subject);
		$this->email->message($message);
		//Send mail
		if($this->email->send())
		{

			//echo "mail sent";exit;
			$this->session->set_flashdata("email_sent","Congragulation Email Send Successfully.");
		}
		
		else
		{
			//echo $this->email->print_debugger();
			//echo "mail not sent";exit;
			$this->session->set_flashdata("email_sent","You have encountered an error");
		}
		

	//exit;

/************************************************************************/

			 

            //$this->load->view('front/booksuccess',$data);

          redirect("booksuccess");
		   
			
         
        } else {
        	
            $session_data = $this->session->userdata('front_logged_in');
            $data['user_id'] = $session_data['user_id'];
            $data['user_name'] = $session_data['user_name'];
            
			redirect('home');
        }
	}



	
	public function paypalsuccess(){




		if ($this->session->userdata('front_logged_in')) {
            $session_data = $this->session->userdata('front_logged_in');
            $data['user_id'] = $session_data['user_id'];
            $data['user_name'] = $session_data['user_name'];
            $user_id = $session_data['user_id'];


            //echo "<pre>"; print_r($_POST);echo "</pre>";
            //echo "<pre>"; print_r($this->input->post());echo "</pre>";
            

			 $billing_id=$this->input->post('custom');

			$billquery=$this->db->query("select * from  tbl_users_billing  WHERE billing_id='$billing_id' ");
			$billDetails= $billquery->result_array();
// print_r($billDetails);
			$plan_id=$billDetails[0]['billing_plan_id'];
			$payment_credit=$billDetails[0]['billing_plan_credit'];
			 $payment_amount=$billDetails[0]['billing_plan_amount'];
        $plansTypeDetails = $this->plans_model->get_plansById($plan_id);
			//print_r($plansTypeDetails);
			$plntypname=$plansTypeDetails[0]['plntypname'];
			
// 			if($plntypname=="Pay Monthly")
// 			{

//             /************************************************************************/
// 				$pautoquery = "SELECT * FROM `tbl_users` where user_id='$user_id'  ";
// 				// echo $pautoquery;
// 				$query2=$this->db->query($pautoquery);
// 				$usrinfo= $query2->result_array();
// 				$total_amount=$usrinfo[0]['total_amount'];
// 				$total_credit=$usrinfo[0]['total_credit'];

// 				$current_amount=$total_amount+$payment_amount;
// 				$current_credit=$total_credit+$payment_credit;
// 				$payment_status="Paid";

			




// 				$userdata=array(
// 				'total_amount' => $current_amount,
// 				'total_credit' => $current_credit);

// 				$this->db->where('user_id', $user_id);
// 				$report = $this->db->update('tbl_users', $userdata);


//   /************************************************************************/

// }


$payment_status=$this->input->post('payment_status');

		if($payment_status=='Completed'){
			
				$data=array(
				'user_id' => $user_id,
				'billing_id' => '0',
				'sty_id' => '0',
				'upimg_id' => '0',
				'history_type' => 'Addition',
				'amount' => $payment_amount,
				'credit' => $payment_credit,
				'account_history_date' => date("Y-m-d"),
				'account_history_time' => date("H:i:s"),
				'payment_status' => 'Paid',
				);
				$this->db->insert('tbl_account_history', $data);

		}







      /************************************************************************/
            

            
          
            if($payment_status=='Completed')
            {
            	$data = array(
				    'billing_date_time'=>date("Y-m-d H:i:s"),
					'billing_status'=>'Completed',
					'payment_status'=>'Paid',
					);
					
			    $updateuserrole = $this->plans_model->billing_update($data,$billing_id);

				$status_query1 = $this->db->get_where('tbl_account_cal', array('user_id'=>$user_id,'plan_status'=>'InActive','payment_status'=>'Not Paid'));
				//$query = $this->db->get("tbl_upload_img");
				$status_result1=$status_query1->result_array(); 
				//print_r($status_result1);exit();
				if(!empty($status_result1))
				{
					$account_cal_id=$status_result1[0]['account_cal_id'];
					$account_cal_details['payment_status']="Paid";
					$account_cal_details['plan_status']="Active";
					$result1=$this->Revbook_model->update_account_cal($account_cal_details,$account_cal_id);
					//$account_cal_details['plan_status']="Pending";
				}
            }
            $data['billing_id'] = $billing_id;




			$query=$this->db->query("select * from  tbl_users  WHERE user_id='$user_id' ");
			$userDetails= $query->result_array();
			$user_name=$userDetails[0]['user_name'];
			$user_email1=$userDetails[0]['user_email'];
			$user_email_value=explode("@",$user_email1);
			$user_email=substr_replace($user_email_value[0], "*****", 2)."@".$user_email_value[1];
			$user_mobile=$userDetails[0]['user_mobile'];
			$user_company=$userDetails[0]['user_company'];
			$user_address=$userDetails[0]['user_address'];





			$billquery=$this->db->query("select * from  tbl_users_billing  WHERE billing_id='$billing_id' ");
			$billDetails= $billquery->result_array();

			$billing_plan_id=$billDetails[0]['billing_plan_id'];

			$billing_plan_credit=$billDetails[0]['billing_plan_credit'];
			$billing_plan_amount=$billDetails[0]['billing_plan_amount'];
			$currency=$billDetails[0]['currency'];
			$billing_date_time=$billDetails[0]['billing_date_time'];
			$billing_status=$billDetails[0]['billing_status'];
			$payment_status=$billDetails[0]['payment_status'];
			$payment_type=$billDetails[0]['payment_type'];

			$payment_type_id=$billDetails[0]['payment_type'];
			//print_r($billDetails);
			$payment_type_query1=$this->db->query("select * FROM tbl_paymenttype where paytyid=$payment_type_id");

			$payment_typeDetails1= $payment_type_query1->result_array();
			$payment_type_name=$payment_typeDetails1[0]['paytypname'];
			$plansTypeDetails = $this->plans_model->get_plansById($billing_plan_id);

			$query1 = $this->db->get_where('tbl_plan', array('planid'=>$plan_id,'plan_for'=>'booking'));
		//$query = $this->db->get("tbl_upload_img");
		$result1=$query1->result_array(); 
		//print_r($result1);

		$plan_amount=$result1[0]['plan_amount'];
		$plan_credit=$result1[0]['plan_credit'];
		$plntyid=$result1[0]['plntyid'];
		$plan_name=$result1[0]['plan_name'];

			$book_id=$billDetails[0]['book_id']; 


			if(!empty($book_id)){
				$array = array('book_id' => $book_id);
				$this->db->set('p_status', 'Paid'); 
				$this->db->where($array); 
				$result=$this->db->update('tbl_booking');
			}
			//exit();
			$query=$this->db->query("select * from  tbl_booking where book_id=$book_id");
			$bookDetails=$query->result_array();


						
			$order_id=$bookDetails[0]['order_id'];
			$gender=$bookDetails[0]['gender'];
			$m_status=$bookDetails[0]['m_status'];
			$category=array_filter(explode(",", $bookDetails[0]['category']));	
			$quty=array_filter(explode(",", $bookDetails[0]['quty']));
			$angle=array_filter(explode(",", $bookDetails[0]['angle']));
			$d_status=$bookDetails[0]['d_status'];
			$notes=$bookDetails[0]['notes'];
			$total_price=$bookDetails[0]['total_price'];
			$total_image=$bookDetails[0]['total_image'];
			$create_date=$bookDetails[0]['create_date'];
			$book_date_time=date("D, M d ,Y H:i A",strtotime($create_date));
			//$plan_id=$bookDetails[0]['plan_id'];
			$currency=$bookDetails[0]['currency'];

			$plan_left_credit=$bookDetails[0]['plan_left_credit'];
			$plan_left_amt=$bookDetails[0]['plan_left_amt'];
			$plan_used_amt=$bookDetails[0]['plan_used_amt'];
			$plan_used_credit=$bookDetails[0]['plan_used_credit'];

			$p_status=$bookDetails[0]['p_status'];


			  if($currency=="USD") { $currency1="&#36;"; }
              elseif($currency=="INR") { $currency1="&#x20B9;"; }
              elseif($currency=="EUR") { $currency1="&#128;"; }
              elseif($currency=="GBP") { $currency1="&#163;"; }
              elseif($currency=="AUD") { $currency1="&#36;"; }
              elseif($currency=="CNY") { $currency1="&#165;"; }


			if($gender=="1"){ $gender1= "Male" ; } 
            if($gender=="2"){ $gender1="Female" ; } 
            if($gender=="4"){ $gender1="Boy" ; } 
            if($gender=="6"){ $gender1="Girl" ; }
            if($m_status=="1"){
	           $m_status1= "Let \"Piquic Style Expert\" select my models & style." ; 
	         } 
	         else{ 
	          $m_status1= "I want to select my models and styling once my apparels gets digitized."; 
	         } 
	         if($d_status=="1"){
               $d_status1= "Standard (6-7 days after receiving your products)." ; 
             } 
             else{ 
               $d_status1= "Express (2-3 days after receiving your products)."; 
             } 

            //$user_email="demo.intexom@gmail.com";
            if($payment_type=='1')
            {
            	$mail_text='';
            	$subject='Piquic - Payment success';
            }
            elseif($payment_type=='2')
            {
            	$mail_text='';
            	$subject='Piquic - Payment success';
            }
            elseif($payment_type=='3')
            {
            	$mail_text='';
            	$subject='Piquic - Payment success';
            }
		    
		    elseif($payment_type=='4')
            {
            	$mail_text='';
            	$subject='Piquic - Payment success';
            }
		    
		    elseif($payment_type=='5')
            {
            	$mail_text='';
            	$subject='Piquic - Payment success';
            }
            elseif($payment_type=='6')
            {
            	$mail_text='Payment Email Piquic - Pay-Later';
            	$subject='Piquic - Payment Pending';
            }
		    
		    		    $table_book_body="";
            if($plntypname=="Pay as you go" || $plntypname=="Pay Monthly"){
            	$table_book_body.="<h4>Booking Details</h4>";
            	$table_book_body.='
            		<table class="table">
                             <thead>';
	            $table_book_body.= '<tr>
	                  <th class="border-0 text-uppercase small font-weight-bold">ID</th>
	                  <th class="border-0 text-uppercase small font-weight-bold">Gender</th>
	                  <th class="border-0 text-uppercase small font-weight-bold">Category</th>
	                  <th class="border-0 text-uppercase small font-weight-bold">Quantity</th>
	                  <th class="border-0 text-uppercase small font-weight-bold">Angle</th>

	                </tr>';
            $table_book_body.= '</thead> <tbody>';
         
	            $table_book_body.= '<tr>
	            	<td>1</td>
	            	<td>'.$gender1. ' </td>
					<td>
	                    <table>';

                foreach ($category as $key => $value ) {
	                $table_book_body.= '<tr>
	                    <td style="border-top: none; border-bottom: 1px solid #dee2e6;">
	                     '.$value.'
	                   </td>
	                 </tr>';
                                
                }
                $table_book_body.= '</table>
                           </td>
                           <td><table>';

                foreach ($quty as $key1 => $value1 ) {
                    $table_book_body.= '<tr>
	                    <td style="border-top: none; border-bottom: 1px solid #dee2e6;">
	                     '.$value1.'
	                   	</td>
	                 	</tr>';
                 
           		} 

               $table_book_body.= '</table></td><td><table>';
              	//echo "<pre>";
              	//print_r($angle);
                unset($arr_filter);

               	foreach ($angle as $key2 => $value2 ) {
          			//echo $value2;
	                $angle_value=explode("_",$value2);
	                $cat=$angle_value[0];
	                $arr_filter[$cat][]=ucfirst($angle_value[1]);
              	}
              	//echo "<pre>";
              	//print_r($arr_filter);

              	foreach ($category as $key => $value ) {
	                $table_book_body.= '<tr>
	                  <td style="border-top: none; border-bottom: 1px solid #dee2e6;">
	                   '.implode(", ",$arr_filter[$value]) .'
	                </td>
	               	</tr>';
            
             	} 
 				$table_book_body.= '</table></td></tr>';
                
                $table_book_body.= '</tbody></table>';

            }    
               $table_plan_body="";
                if($plntypname=="Pay Monthly"){
                	$table_plan_body.="<h4>Plan Details</h4>";
                	$table_plan_body.='<table class="table">
                            <thead>';

	             	$table_plan_body.= '                   
		                <tr>
		                  <th class="border-0 text-uppercase small font-weight-bold">ID</th>
		                  <th class="border-0 text-uppercase small font-weight-bold">Plan Name</th>
		                  <th class="border-0 text-uppercase small font-weight-bold">Duatation</th>
		                  <th class="border-0 text-uppercase small font-weight-bold">Total Image</th>
		                  <th class="border-0 text-uppercase small font-weight-bold">Total Amount</th>
		                  <th class="border-0 text-uppercase small font-weight-bold">Left Image</th>
		                  <th class="border-0 text-uppercase small font-weight-bold">Left Amount</th>
		                </tr>';
				
                         
            	$table_plan_body.= '</thead><tbody>';
   			
                   $table_plan_body.= '
                    <tr>
                      <td>1</td>
                      <td>'.$plan_name.'</td>
                      <td>1 Month</td>
                      <td>'.$plan_credit.'</td>
                      <td>'.$plan_amount.'</td>
                      <td>'.$plan_left_credit.'</td>
                      <td>'.$plan_left_amt.'</td>
                    </tr>';
                     $table_plan_body.= '</tbody></table>';
                }
               
           		// print_r($table_body); //exit();
	            if($plntypname=="Pay as you go")
	            {
	            	$amount_content="Total Amount";
					$plan_content="Plan Type";
					$image_content="Total Image";
					$total_price=$total_price;
					$plntypname=$plntypname;
					$total_image=$total_image;
	            }
	            if($plntypname=='Pay Monthly')
	            {
	            	$amount_content="Used Amount";
					$plan_content="Plan Type";
					$image_content="Used Image";
					$total_price=$plan_used_amt;
					$plntypname=$plntypname;
					$total_image=$plan_used_credit;
	            }
           // print_r($table_body); //exit();
            

/*********************************Send mail*************************************/

	$query=$this->db->query("select * from  tbl_settings  WHERE setting_key='MAIL_SMTP_USER' ");
	$userDetails= $query->result_array();
	$MAIL_SMTP_USER=$userDetails[0]['setting_value'];

	$query=$this->db->query("select * from  tbl_settings  WHERE setting_key='MAIL_SMTP_PASSWORD' ");
	$userDetails= $query->result_array();
	$MAIL_SMTP_PASSWORD=$userDetails[0]['setting_value'];

	$query=$this->db->query("select * from  tbl_settings  WHERE setting_key='MAIL_BODY_BOOKING' ");
	$userDetails= $query->result_array();
	$MAIL_BODY_BOOKING=$userDetails[0]['setting_value'];

		
		//Load email library
		$this->load->library('email');

		$config = array();
		//$config['protocol'] = 'smtp';
		$config['protocol']     = 'mail';
		$config['smtp_host'] = 'localhost';
		$config['smtp_user'] = $MAIL_SMTP_USER;
		$config['smtp_pass'] = $MAIL_SMTP_PASSWORD;
		$config['smtp_port'] = 25;




		$this->email->initialize($config);
		$this->email->set_newline("\r\n");

		
		$from_email = "demo.intexom@gmail.com";
		$to_email = $user_email1;
		$copy_right= date("Y");
		$billing_date_time=date("d/m/y",strtotime($billing_date_time));

		
			//echo $message=$MAIL_BODY_BOOKING;
		$find_arr = array('#ORDER_ID##','##BOOK_DATE##',"##USER_NAME##","##USER_EMAIL##","##USER_MOBILE##","##USER_COMPANY##","##USER_ADDRESS##","##M_STATUS##","##D_STATUS##","##NOTES##","##P_STATUS##",'##CUR##',"##TABLE_BOOK_BODY##","##TABLE_PLAN_BODY##","##AMOUNT_CONTENT##", "##TOTAL_AMOUNT##","##PLAN_CONTENT##",'##PLAN_TYPE##',"##IMAGE_CONTENT##",'##TOTAL_IMAGE##','##COPY_RIGHT##','##PAYMENT_TYPE##');
		$replace_arr = array($order_id,$book_date_time,$user_name,$user_email,$user_mobile,$user_company,$user_address, $m_status1,$d_status1,$notes,$p_status,$currency1,$table_book_body,$table_plan_body,$amount_content,$total_price,$plan_content,$plntypname,$image_content,$total_image,$copy_right,$payment_type_name);
		$message=str_replace($find_arr,$replace_arr,$MAIL_BODY_BOOKING);
		// echo $message;exit;

		//echo $subject."----".$message;exit;



		$this->email->from($from_email, 'Piquic');
		$this->email->to($to_email);
		$this->email->cc('poulami@mokshaproductions.in');
        $this->email->cc('raja.priya@mokshaproductions.in');
		$this->email->subject($subject);
		$this->email->message($message);
		//Send mail
		if($this->email->send())
		{

			//echo "mail sent";exit;
			$this->session->set_flashdata("email_sent","Congragulation Email Send Successfully.");
		}
		
		else
		{
			//echo $this->email->print_debugger();
			//echo "mail not sent";exit;
			$this->session->set_flashdata("email_sent","You have encountered an error");
		}
		

	//exit;

/************************************************************************/

			 

            //$this->load->view('front/booksuccess',$data);

 redirect("booksuccess");
		   
			
         
        } else {
        	
            $session_data = $this->session->userdata('front_logged_in');
            $data['user_id'] = $session_data['user_id'];
            $data['user_name'] = $session_data['user_name'];
            
			redirect('home');
        }



	}



		public function paylatersuccess(){
		


		if ($this->session->userdata('front_logged_in')) {
            $session_data = $this->session->userdata('front_logged_in');
            $data['user_id'] = $session_data['user_id'];
            $data['user_name'] = $session_data['user_name'];
            $user_id = $session_data['user_id'];


            //echo "<pre>"; print_r($_POST);echo "</pre>";
            //echo "<pre>"; print_r($this->input->post());echo "</pre>";
            

			$billing_id=$this->input->post('paylater_billing_id');

			$billquery=$this->db->query("select * from  tbl_users_billing  WHERE billing_id='$billing_id' ");
			$billDetails= $billquery->result_array();
			//print_r($billDetails);
			
			$plan_id=$billDetails[0]['billing_plan_id'];
			$payment_credit=$billDetails[0]['billing_plan_credit'];
			$payment_amount=$billDetails[0]['billing_plan_amount'];
        
			
			$plansTypeDetails = $this->plans_model->get_plansById($plan_id);
			//print_r($plansTypeDetails);
			$plntypname=$plansTypeDetails[0]['plntypname'];
			
			// if($plntypname=="Pay Monthly")
			// {
			// 	/************************************************************************/
			// 	$pautoquery = "SELECT * FROM `tbl_users` where user_id='$user_id'  ";
			// 	// echo $pautoquery;
			// 	$query2=$this->db->query($pautoquery);
			// 	$usrinfo= $query2->result_array();
			// 	$total_amount=$usrinfo[0]['total_amount'];
			// 	$total_credit=$usrinfo[0]['total_credit'];


			// 	$current_amount=$total_amount-$payment_amount;
			// 	$current_credit=$total_credit-$payment_credit;
			// 	$payment_status="Not Paid";


			// 	$userdata=array(
			// 	'total_amount' => $current_amount,
			// 	'total_credit' => $current_credit);

			// 	$this->db->where('user_id', $user_id);
			// 	$report = $this->db->update('tbl_users', $userdata);
			// }
			//exit();
            



         $status=$this->input->post('status');
          // 	$billing_id=$this->input->post('hid_billing_id');

		if($status=='success'){
			
				$data=array(
				'user_id' => $user_id,
				'billing_id' => '0',
				'sty_id' => '0',
				'upimg_id' => '0',
				'history_type' => 'Deduction',
				'amount' => $payment_amount,
				'credit' => $payment_credit,
				'account_history_date' => date("Y-m-d"),
				'account_history_time' => date("H:i:s"),
				'payment_status' => 'Not Paid',
				);
				$this->db->insert('tbl_account_history', $data);

		}






      /************************************************************************/
            


            // if($status=='success')
            // {
            	$data = array(
				    'billing_date_time'=>date("Y-m-d H:i:s"),
					'billing_status'=>'Pay Later',
					'payment_status'=>'Not Paid',
					);
					
			    $updateuserrole = $this->plans_model->billing_update($data,$billing_id);

			    
				
            // }
            $status_query1 = $this->db->get_where('tbl_account_cal', array('user_id'=>$user_id,'plan_status'=>'InActive','payment_status'=>'Not Paid'));
				//$query = $this->db->get("tbl_upload_img");
				$status_result1=$status_query1->result_array(); 
				//print_r($status_result1);exit();
				if(!empty($status_result1))
				{
					$account_cal_id=$status_result1[0]['account_cal_id'];
					$account_cal_details['payment_status']="Not Paid";
					$account_cal_details['plan_status']="InActive";
					$result1=$this->Revbook_model->update_account_cal($account_cal_details,$account_cal_id);
					//$account_cal_details['plan_status']="Pending";
				}
            $data['billing_id'] = $billing_id;

          //  exit();


			$query=$this->db->query("select * from  tbl_users  WHERE user_id='$user_id' ");
			$userDetails= $query->result_array();
			$user_name=$userDetails[0]['user_name'];
			$user_email1=$userDetails[0]['user_email'];
			$user_email_value=explode("@",$user_email1);
			$user_email=substr_replace($user_email_value[0], "*****", 2)."@".$user_email_value[1];
			$user_mobile=$userDetails[0]['user_mobile'];
			$user_company=$userDetails[0]['user_company'];
			$user_address=$userDetails[0]['user_address'];





			$billquery=$this->db->query("select * from  tbl_users_billing  WHERE billing_id='$billing_id' ");
			$billDetails= $billquery->result_array();

			$billing_plan_id=$billDetails[0]['billing_plan_id'];

			$billing_plan_credit=$billDetails[0]['billing_plan_credit'];
			$billing_plan_amount=$billDetails[0]['billing_plan_amount'];
			$currency=$billDetails[0]['currency'];
			$billing_date_time=$billDetails[0]['billing_date_time'];
			$billing_status=$billDetails[0]['billing_status'];
			$payment_status=$billDetails[0]['payment_status'];
			$payment_type=$billDetails[0]['payment_type'];

			$payment_type_id=$billDetails[0]['payment_type'];
			//print_r($billDetails);
			$payment_type_query1=$this->db->query("select * FROM tbl_paymenttype where paytyid=$payment_type_id");

			$payment_typeDetails1= $payment_type_query1->result_array();
			$payment_type_name=$payment_typeDetails1[0]['paytypname'];
			$plansTypeDetails = $this->plans_model->get_plansById($billing_plan_id);


			$query1 = $this->db->get_where('tbl_plan', array('planid'=>$plan_id,'plan_for'=>'booking'));
		//$query = $this->db->get("tbl_upload_img");
		$result1=$query1->result_array(); 
		//print_r($result1);

		$plan_amount=$result1[0]['plan_amount'];
		$plan_credit=$result1[0]['plan_credit'];
		$plntyid=$result1[0]['plntyid'];
		$plan_name=$result1[0]['plan_name'];


			$book_id=$billDetails[0]['book_id']; 


			if(!empty($book_id)){
				$array = array('book_id' => $book_id);
				$this->db->set('p_status', 'Not Paid'); 
				$this->db->where($array); 
				$result=$this->db->update('tbl_booking');
			}
			//exit();
			$query=$this->db->query("select * from  tbl_booking where book_id=$book_id");
			$bookDetails=$query->result_array();


						
			$order_id=$bookDetails[0]['order_id'];
			$gender=$bookDetails[0]['gender'];
			$m_status=$bookDetails[0]['m_status'];
			$category=array_filter(explode(",", $bookDetails[0]['category']));	
			$quty=array_filter(explode(",", $bookDetails[0]['quty']));
			$angle=array_filter(explode(",", $bookDetails[0]['angle']));
			$d_status=$bookDetails[0]['d_status'];
			$notes=$bookDetails[0]['notes'];
			$total_price=$bookDetails[0]['total_price'];
			$total_image=$bookDetails[0]['total_image'];
			$create_date=$bookDetails[0]['create_date'];
			$book_date_time=date("D, M d ,Y H:i A",strtotime($create_date));
			//$plan_id=$bookDetails[0]['plan_id'];
			$currency=$bookDetails[0]['currency'];

			$plan_left_credit=$bookDetails[0]['plan_left_credit'];
			$plan_left_amt=$bookDetails[0]['plan_left_amt'];
			$plan_used_amt=$bookDetails[0]['plan_used_amt'];
			$plan_used_credit=$bookDetails[0]['plan_used_credit'];

			$p_status=$bookDetails[0]['p_status'];


			  if($currency=="USD") { $currency1="&#36;"; }
              elseif($currency=="INR") { $currency1="&#x20B9;"; }
              elseif($currency=="EUR") { $currency1="&#128;"; }
              elseif($currency=="GBP") { $currency1="&#163;"; }
              elseif($currency=="AUD") { $currency1="&#36;"; }
              elseif($currency=="CNY") { $currency1="&#165;"; }


			if($gender=="1"){ $gender1= "Male" ; } 
            if($gender=="2"){ $gender1="Female" ; } 
            if($gender=="4"){ $gender1="Boy" ; } 
            if($gender=="6"){ $gender1="Girl" ; }
            if($m_status=="1"){
	           $m_status1= "Let \"Piquic Style Expert\" select my models & style." ; 
	         } 
	         else{ 
	          $m_status1= "I want to select my models and styling once my apparels gets digitized."; 
	         } 
	         if($d_status=="1"){
               $d_status1= "Standard (6-7 days after receiving your products)." ; 
             } 
             else{ 
               $d_status1= "Express (2-3 days after receiving your products)."; 
             } 
            //$user_email="demo.intexom@gmail.com";
            if($payment_type=='1')
            {
            	$mail_text='';
            	$subject='Piquic - Payment success';
            }
            elseif($payment_type=='2')
            {
            	$mail_text='';
            	$subject='Piquic - Payment success';
            }
            elseif($payment_type=='3')
            {
            	$mail_text='';
            	$subject='Piquic - Payment success';
            }
		    
		    elseif($payment_type=='4')
            {
            	$mail_text='';
            	$subject='Piquic - Payment success';
            }
		    
		    elseif($payment_type=='5')
            {
            	$mail_text='';
            	$subject='Piquic - Payment success';
            }
            elseif($payment_type=='6')
            {
            	$mail_text='Payment Email Piquic - Pay-Later';
            	$subject='Piquic - Payment Pending';
            }
		    
		    		   $table_book_body="";
            if($plntypname=="Pay as you go" || $plntypname=="Pay Monthly"){
            	$table_book_body.="<h4>Booking Details</h4>";
            	$table_book_body.='
            		<table class="table">
                             <thead>';
	            $table_book_body.= '<tr>
	                  <th class="border-0 text-uppercase small font-weight-bold">ID</th>
	                  <th class="border-0 text-uppercase small font-weight-bold">Gender</th>
	                  <th class="border-0 text-uppercase small font-weight-bold">Category</th>
	                  <th class="border-0 text-uppercase small font-weight-bold">Quantity</th>
	                  <th class="border-0 text-uppercase small font-weight-bold">Angle</th>

	                </tr>';
            $table_book_body.= '</thead> <tbody>';
         
	            $table_book_body.= '<tr>
	            	<td>1</td>
	            	<td>'.$gender1. ' </td>
					<td>
	                    <table>';

                foreach ($category as $key => $value ) {
	                $table_book_body.= '<tr>
	                    <td style="border-top: none; border-bottom: 1px solid #dee2e6;">
	                     '.$value.'
	                   </td>
	                 </tr>';
                                
                }
                $table_book_body.= '</table>
                           </td>
                           <td><table>';

                foreach ($quty as $key1 => $value1 ) {
                    $table_book_body.= '<tr>
	                    <td style="border-top: none; border-bottom: 1px solid #dee2e6;">
	                     '.$value1.'
	                   	</td>
	                 	</tr>';
                 
           		} 

               $table_book_body.= '</table></td><td><table>';
              	//echo "<pre>";
              	//print_r($angle);
                unset($arr_filter);

               	foreach ($angle as $key2 => $value2 ) {
          			//echo $value2;
	                $angle_value=explode("_",$value2);
	                $cat=$angle_value[0];
	                $arr_filter[$cat][]=ucfirst($angle_value[1]);
              	}
              	//echo "<pre>";
              	//print_r($arr_filter);

              	foreach ($category as $key => $value ) {
	                $table_book_body.= '<tr>
	                  <td style="border-top: none; border-bottom: 1px solid #dee2e6;">
	                   '.implode(", ",$arr_filter[$value]) .'
	                </td>
	               	</tr>';
            
             	} 
 				$table_book_body.= '</table></td></tr>';
                
                $table_book_body.= '</tbody></table>';

            }    
               $table_plan_body="";
                if($plntypname=="Pay Monthly"){
                	$table_plan_body.="<h4>Plan Details</h4>";
                	$table_plan_body.='<table class="table">
                            <thead>';

	             	$table_plan_body.= '                   
		                <tr>
		                  <th class="border-0 text-uppercase small font-weight-bold">ID</th>
		                  <th class="border-0 text-uppercase small font-weight-bold">Plan Name</th>
		                  <th class="border-0 text-uppercase small font-weight-bold">Duatation</th>
		                  <th class="border-0 text-uppercase small font-weight-bold">Total Image</th>
		                  <th class="border-0 text-uppercase small font-weight-bold">Total Amount</th>
		                  <th class="border-0 text-uppercase small font-weight-bold">Left Image</th>
		                  <th class="border-0 text-uppercase small font-weight-bold">Left Amount</th>
		                </tr>';
				
                         
            	$table_plan_body.= '</thead><tbody>';
   			
                   $table_plan_body.= '
                    <tr>
                      <td>1</td>
                      <td>'.$plan_name.'</td>
                      <td>1 Month</td>
                      <td>'.$plan_credit.'</td>
                      <td>'.$plan_amount.'</td>
                      <td>'.$plan_left_credit.'</td>
                      <td>'.$plan_left_amt.'</td>
                    </tr>';
                     $table_plan_body.= '</tbody></table>';
                }
               
           		// print_r($table_body); //exit();
	            if($plntypname=="Pay as you go")
	            {
	            	$amount_content="Total Amount";
					$plan_content="Plan Type";
					$image_content="Total Image";
					$total_price=$total_price;
					$plntypname=$plntypname;
					$total_image=$total_image;
	            }
	            if($plntypname=='Pay Monthly')
	            {
	            	$amount_content="Used Amount";
					$plan_content="Plan Type";
					$image_content="Used Image";
					$total_price=$plan_used_amt;
					$plntypname=$plntypname;
					$total_image=$plan_used_credit;
	            }
           // print_r($table_body); //exit();
            

/*********************************Send mail*************************************/

	$query=$this->db->query("select * from  tbl_settings  WHERE setting_key='MAIL_SMTP_USER' ");
	$userDetails= $query->result_array();
	$MAIL_SMTP_USER=$userDetails[0]['setting_value'];

	$query=$this->db->query("select * from  tbl_settings  WHERE setting_key='MAIL_SMTP_PASSWORD' ");
	$userDetails= $query->result_array();
	$MAIL_SMTP_PASSWORD=$userDetails[0]['setting_value'];

	$query=$this->db->query("select * from  tbl_settings  WHERE setting_key='MAIL_BODY_BOOKING' ");
	$userDetails= $query->result_array();
	$MAIL_BODY_BOOKING=$userDetails[0]['setting_value'];

		
		//Load email library
		$this->load->library('email');

		$config = array();
		//$config['protocol'] = 'smtp';
		$config['protocol']     = 'mail';
		$config['smtp_host'] = 'localhost';
		$config['smtp_user'] = $MAIL_SMTP_USER;
		$config['smtp_pass'] = $MAIL_SMTP_PASSWORD;
		//$config['smtp_port'] = 25;




		$this->email->initialize($config);
		$this->email->set_newline("\r\n");

		
		$from_email = "demo.intexom@gmail.com";
		$to_email = $user_email1;
		$copy_right= date("Y");
		$billing_date_time=date("d/m/y",strtotime($billing_date_time));

			//echo $message=$MAIL_BODY_BOOKING;
		$find_arr = array('#ORDER_ID##','##BOOK_DATE##',"##USER_NAME##","##USER_EMAIL##","##USER_MOBILE##","##USER_COMPANY##","##USER_ADDRESS##","##M_STATUS##","##D_STATUS##","##NOTES##","##P_STATUS##",'##CUR##',"##TABLE_BOOK_BODY##","##TABLE_PLAN_BODY##","##AMOUNT_CONTENT##", "##TOTAL_AMOUNT##","##PLAN_CONTENT##",'##PLAN_TYPE##',"##IMAGE_CONTENT##",'##TOTAL_IMAGE##','##COPY_RIGHT##','##PAYMENT_TYPE##');
		$replace_arr = array($order_id,$book_date_time,$user_name,$user_email,$user_mobile,$user_company,$user_address, $m_status1,$d_status1,$notes,$p_status,$currency1,$table_book_body,$table_plan_body,$amount_content,$total_price,$plan_content,$plntypname,$image_content,$total_image,$copy_right,$payment_type_name);
		$message=str_replace($find_arr,$replace_arr,$MAIL_BODY_BOOKING);
		// echo $message;exit;
		//echo $subject."----".$message;exit;



		$this->email->from($from_email, 'Piquic');
		$this->email->to($to_email);
		$this->email->cc('poulami@mokshaproductions.in');
		$this->email->cc('raja.priya@mokshaproductions.in');
       // $this->email->cc('raja.priya@mokshaproductions.in');
		$this->email->subject($subject);
		$this->email->message($message);
		//Send mail
		if($this->email->send())
		{

			//echo "mail sent";exit;
			$this->session->set_flashdata("email_sent","Congragulation Email Send Successfully.");
		}
		
		else
		{
			//echo $this->email->print_debugger();
			//echo "mail not sent";exit;
			$this->session->set_flashdata("email_sent","You have encountered an error");
		}
		

	//exit;

/************************************************************************/

			 

            //$this->load->view('front/booksuccess',$data);

 redirect("booksuccess");
		   
			
         
        } else {
        	
            $session_data = $this->session->userdata('front_logged_in');
            $data['user_id'] = $session_data['user_id'];
            $data['user_name'] = $session_data['user_name'];
            
			redirect('home');
        }
	}


	public function booksuccess(){
		


		if ($this->session->userdata('front_logged_in')) {
            $session_data = $this->session->userdata('front_logged_in');
            $data['user_id'] = $session_data['user_id'];
            $data['user_name'] = $session_data['user_name'];
            $user_id = $session_data['user_id'];


            $this->load->view('front/booksuccess',$data);
		   
			
         
        } else {
        	
            $session_data = $this->session->userdata('front_logged_in');
            $data['user_id'] = $session_data['user_id'];
            $data['user_name'] = $session_data['user_name'];
            
			redirect('home');
        }
	}




	public function bookfailure(){
		


		if ($this->session->userdata('front_logged_in')) {
            $session_data = $this->session->userdata('front_logged_in');
            $data['user_id'] = $session_data['user_id'];
            $data['user_name'] = $session_data['user_name'];

            /*echo "<pre>"; print_r($_POST);echo "</pre>";
            echo "<pre>"; print_r($this->input->post());echo "</pre>";
            exit;*/

            $status=$this->input->post('status');
            $billing_id=$this->input->post('udf1');
            if($status=='failure')
            {
            	$this->plans_model->delete_billing($billing_id);
            }

            $this->load->view('front/bookfailure',$data);
		   
			
         
        } else {
        	
            $session_data = $this->session->userdata('front_logged_in');
            $data['user_id'] = $session_data['user_id'];
            $data['user_name'] = $session_data['user_name'];
            
			redirect('home');
        }
	}



	public function payfailure(){
		


		if ($this->session->userdata('front_logged_in')) {
            $session_data = $this->session->userdata('front_logged_in');
            $data['user_id'] = $session_data['user_id'];
            $data['user_name'] = $session_data['user_name'];
            $user_id = $session_data['user_id'];


            $this->load->view('front/bookfailure',$data);
		   
			
         
        } else {
        	
            $session_data = $this->session->userdata('front_logged_in');
            $data['user_id'] = $session_data['user_id'];
            $data['user_name'] = $session_data['user_name'];
            
			redirect('home');
        }
	}



/////////////////////////////////////By Papri//////////////////////////////


public function tbuser_bill_update(){
if ($this->session->userdata('front_logged_in')) {
            $session_data = $this->session->userdata('front_logged_in');
            $data['user_id'] = $session_data['user_id'];
            $data['user_name'] = $session_data['user_name'];
            $user_id = $session_data['user_id'];
             $plan_credit=$this->input->post('plan_credit');
            $plan_amount=$this->input->post('plan_amount');

            
					
 $updateuserrole = $this->plans_model->tbuser_billing_update($plan_credit,$plan_amount,$user_id);

 // return $report;

        }else{


         $session_data = $this->session->userdata('front_logged_in');
            $data['user_id'] = $session_data['user_id'];
            $data['user_name'] = $session_data['user_name'];
            
			//redirect('home');
        }


}





public function paylatersuccess_old(){



if ($this->session->userdata('front_logged_in')) {
$session_data = $this->session->userdata('front_logged_in');
$data['user_id'] = $session_data['user_id'];
$data['user_name'] = $session_data['user_name'];


// echo "<pre>"; print_r($_POST);echo "</pre>";
// echo "<pre>"; print_r($this->input->post());echo "</pre>";


$billing_id=$this->input->post('billing_id');
$status='success';
if($status=='success')
{
$data = array(
'billing_date_time'=>date("Y-m-d H:i:s"),
'billing_status'=>'Completed',
'payment_status'=>'Notpaid',
);

$updateuserrole = $this->plans_model->billing_update($data,$billing_id);

}





/////////////////////////////Send mail start////////////////////////////////



/////////////////////////////Send mail end////////////////////////////////




$this->load->view('front/booksuccess',$billing_id);



} else {

$session_data = $this->session->userdata('front_logged_in');
$data['user_id'] = $session_data['user_id'];
$data['user_name'] = $session_data['user_name'];

redirect('home');
}
}



public function Ccavenuesuccess(){

        if ($this->session->userdata('front_logged_in')) {
            $session_data = $this->session->userdata('front_logged_in');
            $data['user_id'] = $session_data['user_id'];
            $data['user_name'] = $session_data['user_name'];
            $user_id = $session_data['user_id'];        
            // echo "<pre>"; print_r($_POST);echo "</pre>";

            //echo "<pre>"; print_r($this->input->post());echo "</pre>";
            $billing_id=$this->input->post('orderNo');

        
        $ccvqueryKEY=$this->db->query("select * from  tbl_settings  WHERE setting_key='CCAVENUE_WORKING_KEY' ");
     $ccvuserDetailsKEY= $ccvqueryKEY->result_array();
     $CCAVENUE_WORKING_KEY=$ccvuserDetailsKEY[0]['setting_value'];

     $workingKey=$CCAVENUE_WORKING_KEY;          //Working Key should be provided here.
        $encResponse=$_POST["encResp"];         //This is the response sent by the CCAvenue Server
        $rcvdString=$this->decrypt($encResponse,$workingKey);      //Crypto Decryption used as per the specified working key.
        $order_status="";
        $decryptValues=explode('&', $rcvdString);
        $dataSize=sizeof($decryptValues);

     // echo "<pre>"; print_r($decryptValues);
        for($i = 0; $i < $dataSize; $i++) 
        {
         $information=explode('=',$decryptValues[$i]);
         if($i==3)   $order_status=$information[1];
        }
        
       // echo $order_status;
         if($order_status==="Success")
          {

           
            $billquery=$this->db->query("select * from  tbl_users_billing  WHERE billing_id='$billing_id' ");
            $billDetails= $billquery->result_array();

            $plan_id=$billDetails[0]['billing_plan_id'];
            $payment_credit=$billDetails[0]['billing_plan_credit'];
            $payment_amount=$billDetails[0]['billing_plan_amount'];



                	/***************************updating Booking Status****************************/


//exit;
  /************************************************************************/









$payment_status=$this->input->post('payment_status'); 

            if($payment_status=='Completed')
             {

                $data=array(
                'user_id' => $user_id,
                'billing_id' => $billing_id,
                'sty_id' => '0',
                'upimg_id' => '0',
                'history_type' => 'Addition',
                'amount' => $payment_amount,
                'credit' => $payment_credit,
                'account_history_date' => date("Y-m-d"),
                'account_history_time' => date("H:i:s"),
                'payment_status' => 'Paid',
                );
            }
            $this->db->insert('tbl_account_history', $data); 
            //$payment_status=$this->input->post('payment_status'); 

            if($payment_status=='Completed')
             {
                $data = array(
                    'billing_date_time'=>date("Y-m-d H:i:s"),
                    'billing_status'=>'Completed',
                    'payment_status'=>'Paid',
                    );
                    
                  

                $updateuserrole = $this->plans_model->billing_update($data,$billing_id);

                $status_query1 = $this->db->get_where('tbl_account_cal', array('user_id'=>$user_id,'plan_status'=>'InActive','payment_status'=>'Not Paid'));
				//$query = $this->db->get("tbl_upload_img");
				$status_result1=$status_query1->result_array(); 
				//print_r($status_result1);exit();
				if(!empty($status_result1))
				{
					$account_cal_id=$status_result1[0]['account_cal_id'];
					$account_cal_details['payment_status']="Paid";
					$account_cal_details['plan_status']="Active";
					$result1=$this->Revbook_model->update_account_cal($account_cal_details,$account_cal_id);
					//$account_cal_details['plan_status']="Pending";
				}
            }
          $data['billing_id'] = $billing_id;

            $query=$this->db->query("select * from  tbl_users  WHERE user_id='$user_id' ");
            $userDetails= $query->result_array();
            $user_name=$userDetails[0]['user_name'];
            $user_email1=$userDetails[0]['user_email'];
			$user_email_value=explode("@",$user_email1);
			$user_email=substr_replace($user_email_value[0], "*****", 2)."@".$user_email_value[1];
            $user_mobile=$userDetails[0]['user_mobile'];
            $user_company=$userDetails[0]['user_company'];
			$user_address=$userDetails[0]['user_address'];
            $billquery=$this->db->query("select * from  tbl_users_billing  WHERE billing_id='$billing_id' ");
            $billDetails= $billquery->result_array();
            $billing_plan_id=$billDetails[0]['billing_plan_id'];
            $billing_plan_credit=$billDetails[0]['billing_plan_credit'];
            $billing_plan_amount=$billDetails[0]['billing_plan_amount'];
            $currency=$billDetails[0]['currency'];
            $billing_date_time=$billDetails[0]['billing_date_time'];
            $billing_status=$billDetails[0]['billing_status'];
            $payment_status=$billDetails[0]['payment_status'];
            $payment_type=$billDetails[0]['payment_type'];

            $payment_type_id=$billDetails[0]['payment_type'];
			//print_r($billDetails);
			$payment_type_query1=$this->db->query("select * FROM tbl_paymenttype where paytyid=$payment_type_id");

			$payment_typeDetails1= $payment_type_query1->result_array();
			$payment_type_name=$payment_typeDetails1[0]['paytypname'];
			$plansTypeDetails = $this->plans_model->get_plansById($billing_plan_id);


			$query1 = $this->db->get_where('tbl_plan', array('planid'=>$plan_id,'plan_for'=>'booking'));
		//$query = $this->db->get("tbl_upload_img");
		$result1=$query1->result_array(); 
		//print_r($result1);

		$plan_amount=$result1[0]['plan_amount'];
		$plan_credit=$result1[0]['plan_credit'];
		$plntyid=$result1[0]['plntyid'];
		$plan_name=$result1[0]['plan_name'];

		

            $book_id=$billDetails[0]['book_id']; 


			if(!empty($book_id)){
				$array = array('book_id' => $book_id);
				$this->db->set('p_status', 'Paid'); 
				$this->db->where($array); 
				$result=$this->db->update('tbl_booking');
			}
			//exit();
			$query=$this->db->query("select * from  tbl_booking where book_id=$book_id");
			$bookDetails=$query->result_array();


						
			$order_id=$bookDetails[0]['order_id'];
			$gender=$bookDetails[0]['gender'];
			$m_status=$bookDetails[0]['m_status'];
			$category=array_filter(explode(",", $bookDetails[0]['category']));	
			$quty=array_filter(explode(",", $bookDetails[0]['quty']));
			$angle=array_filter(explode(",", $bookDetails[0]['angle']));
			$d_status=$bookDetails[0]['d_status'];
			$notes=$bookDetails[0]['notes'];
			$total_price=$bookDetails[0]['total_price'];
			$total_image=$bookDetails[0]['total_image'];
			$create_date=$bookDetails[0]['create_date'];
			$book_date_time=date("D, M d ,Y H:i A",strtotime($create_date));
			//$plan_id=$bookDetails[0]['plan_id'];
			$currency=$bookDetails[0]['currency'];


			$p_status=$bookDetails[0]['p_status'];


			  if($currency=="USD") { $currency1="&#36;"; }
              elseif($currency=="INR") { $currency1="&#x20B9;"; }
              elseif($currency=="EUR") { $currency1="&#128;"; }
              elseif($currency=="GBP") { $currency1="&#163;"; }
              elseif($currency=="AUD") { $currency1="&#36;"; }
              elseif($currency=="CNY") { $currency1="&#165;"; }


			if($gender=="1"){ $gender1= "Male" ; } 
            if($gender=="2"){ $gender1="Female" ; } 
            if($gender=="4"){ $gender1="Boy" ; } 
            if($gender=="6"){ $gender1="Girl" ; }
            if($m_status=="1"){
	           $m_status1= "Let \"Piquic Style Expert\" select my models & style." ; 
	         } 
	         else{ 
	          $m_status1= "I want to select my models and styling once my apparels gets digitized."; 
	         } 
	         if($d_status=="1"){
               $d_status1= "Standard (6-7 days after receiving your products)." ; 
             } 
             else{ 
               $d_status1= "Express (2-3 days after receiving your products)."; 
             } 
          
                $mail_text='Payment Email Piquic - CCAvenue';
                $subject='Piquic - Payment success';
       $table_book_body="";
            if($plntypname=="Pay as you go" || $plntypname=="Pay Monthly"){
            	$table_book_body.="<h4>Booking Details</h4>";
            	$table_book_body.='
            		<table class="table">
                             <thead>';
	            $table_book_body.= '<tr>
	                  <th class="border-0 text-uppercase small font-weight-bold">ID</th>
	                  <th class="border-0 text-uppercase small font-weight-bold">Gender</th>
	                  <th class="border-0 text-uppercase small font-weight-bold">Category</th>
	                  <th class="border-0 text-uppercase small font-weight-bold">Quantity</th>
	                  <th class="border-0 text-uppercase small font-weight-bold">Angle</th>

	                </tr>';
            $table_book_body.= '</thead> <tbody>';
         
	            $table_book_body.= '<tr>
	            	<td>1</td>
	            	<td>'.$gender1. ' </td>
					<td>
	                    <table>';

                foreach ($category as $key => $value ) {
	                $table_book_body.= '<tr>
	                    <td style="border-top: none; border-bottom: 1px solid #dee2e6;">
	                     '.$value.'
	                   </td>
	                 </tr>';
                                
                }
                $table_book_body.= '</table>
                           </td>
                           <td><table>';

                foreach ($quty as $key1 => $value1 ) {
                    $table_book_body.= '<tr>
	                    <td style="border-top: none; border-bottom: 1px solid #dee2e6;">
	                     '.$value1.'
	                   	</td>
	                 	</tr>';
                 
           		} 

               $table_book_body.= '</table></td><td><table>';
              	//echo "<pre>";
              	//print_r($angle);
                unset($arr_filter);

               	foreach ($angle as $key2 => $value2 ) {
          			//echo $value2;
	                $angle_value=explode("_",$value2);
	                $cat=$angle_value[0];
	                $arr_filter[$cat][]=ucfirst($angle_value[1]);
              	}
              	//echo "<pre>";
              	//print_r($arr_filter);

              	foreach ($category as $key => $value ) {
	                $table_book_body.= '<tr>
	                  <td style="border-top: none; border-bottom: 1px solid #dee2e6;">
	                   '.implode(", ",$arr_filter[$value]) .'
	                </td>
	               	</tr>';
            
             	} 
 				$table_book_body.= '</table></td></tr>';
                
                $table_book_body.= '</tbody></table>';

            }    
               $table_plan_body="";
                if($plntypname=="Pay Monthly"){
                	$table_plan_body.="<h4>Plan Details</h4>";
                	$table_plan_body.='<table class="table">
                            <thead>';

	             	$table_plan_body.= '                   
		                <tr>
		                  <th class="border-0 text-uppercase small font-weight-bold">ID</th>
		                  <th class="border-0 text-uppercase small font-weight-bold">Plan Name</th>
		                  <th class="border-0 text-uppercase small font-weight-bold">Duatation</th>
		                  <th class="border-0 text-uppercase small font-weight-bold">Total Image</th>
		                  <th class="border-0 text-uppercase small font-weight-bold">Total Amount</th>
		                  <th class="border-0 text-uppercase small font-weight-bold">Left Image</th>
		                  <th class="border-0 text-uppercase small font-weight-bold">Left Amount</th>
		                </tr>';
				
                         
            	$table_plan_body.= '</thead><tbody>';
   			
                   $table_plan_body.= '
                    <tr>
                      <td>1</td>
                      <td>'.$plan_name.'</td>
                      <td>1 Month</td>
                      <td>'.$plan_credit.'</td>
                      <td>'.$plan_amount.'</td>
                      <td>'.$plan_left_credit.'</td>
                      <td>'.$plan_left_amt.'</td>
                    </tr>';
                     $table_plan_body.= '</tbody></table>';
                }
               
           		// print_r($table_body); //exit();
	            if($plntypname=="Pay as you go")
	            {
	            	$amount_content="Total Amount";
					$plan_content="Plan Type";
					$image_content="Total Image";
					$total_price=$total_price;
					$plntypname=$plntypname;
					$total_image=$total_image;
	            }
	            if($plntypname=='Pay Monthly')
	            {
	            	$amount_content="Used Amount";
					$plan_content="Plan Type";
					$image_content="Used Image";
					$total_price=$plan_used_amt;
					$plntypname=$plntypname;
					$total_image=$plan_used_credit;
	            }

        $query=$this->db->query("select * from  tbl_settings  WHERE setting_key='MAIL_SMTP_USER' ");
        $userDetails= $query->result_array();
        $MAIL_SMTP_USER=$userDetails[0]['setting_value'];
        $query=$this->db->query("select * from  tbl_settings  WHERE setting_key='MAIL_SMTP_PASSWORD' ");
        $userDetails= $query->result_array();
        $MAIL_SMTP_PASSWORD=$userDetails[0]['setting_value'];
        $query=$this->db->query("select * from  tbl_settings  WHERE setting_key='MAIL_BODY_BOOKING' ");
        $userDetails= $query->result_array();
        $MAIL_BODY_BOOKING=$userDetails[0]['setting_value'];
        $this->load->library('email');
        $config = array();
        //$config['protocol'] = 'smtp';
        $config['protocol']     = 'mail';
        $config['smtp_host'] = 'localhost';
        $config['smtp_user'] = $MAIL_SMTP_USER;
        $config['smtp_pass'] = $MAIL_SMTP_PASSWORD;
        $config['smtp_port'] = 25;

        $this->email->initialize($config);
        $this->email->set_newline("\r\n");        
        $from_email = "demo.intexom@gmail.com";
        $to_email = $user_email1;
        $copy_right= date("Y");
        $billing_date_time=date("d/m/y",strtotime($billing_date_time));
       	//echo $message=$MAIL_BODY_BOOKING;
		$find_arr = array('#ORDER_ID##','##BOOK_DATE##',"##USER_NAME##","##USER_EMAIL##","##USER_MOBILE##","##USER_COMPANY##","##USER_ADDRESS##","##M_STATUS##","##D_STATUS##","##NOTES##","##P_STATUS##",'##CUR##',"##TABLE_BOOK_BODY##","##TABLE_PLAN_BODY##","##AMOUNT_CONTENT##", "##TOTAL_AMOUNT##","##PLAN_CONTENT##",'##PLAN_TYPE##',"##IMAGE_CONTENT##",'##TOTAL_IMAGE##','##COPY_RIGHT##','##PAYMENT_TYPE##');
		$replace_arr = array($order_id,$book_date_time,$user_name,$user_email,$user_mobile,$user_company,$user_address, $m_status1,$d_status1,$notes,$p_status,$currency1,$table_book_body,$table_plan_body,$amount_content,$total_price,$plan_content,$plntypname,$image_content,$total_image,$copy_right,$payment_type_name);
		$message=str_replace($find_arr,$replace_arr,$MAIL_BODY_BOOKING);
// echo $message;exit;
       

        $this->email->from($from_email, 'Piquic');
        $this->email->to($to_email);
        $this->email->cc('poulami@mokshaproductions.in');
        $this->email->cc('raja.priya@mokshaproductions.in');
        $this->email->subject($subject);
        $this->email->message($message);
        //Send mail
        if($this->email->send())
        {

            //echo "mail sent";exit;
            $this->session->set_flashdata("email_sent","Congragulation Email Send Successfully.");
        }
        
        else
        {
            //echo $this->email->print_debugger();
            //echo "mail not sent";exit;
            $this->session->set_flashdata("email_sent","You have encountered an error");
        }       

     redirect("booksuccess");
        }

        if($order_status==="Failure"){

            $data = array(
                    'billing_date_time'=>date("Y-m-d H:i:s"),
                    'billing_status'=>'Payment Failed',
                    'payment_status'=>'Not Paid',
                    );
                    
                $updateuserrole = $this->plans_model->billing_update($data,$billing_id);

                $status_query1 = $this->db->get_where('tbl_account_cal', array('user_id'=>$user_id,'plan_status'=>'InActive','payment_status'=>'Not Paid'));
				//$query = $this->db->get("tbl_upload_img");
				$status_result1=$status_query1->result_array(); 
				//print_r($status_result1);exit();
				if(!empty($status_result1))
				{
					$account_cal_id=$status_result1[0]['account_cal_id'];
					$account_cal_details['payment_status']="Not Paid";
					$account_cal_details['plan_status']="InActive";
					$result1=$this->Revbook_model->update_account_cal($account_cal_details,$account_cal_id);
					//$account_cal_details['plan_status']="Pending";
				}

        redirect("bookfailure");
        


        }

         
        } else {
            
            $session_data = $this->session->userdata('front_logged_in');
            $data['user_id'] = $session_data['user_id'];
            $data['user_name'] = $session_data['user_name'];
            
            redirect('home');
        }



    }



/*
* @param1 : Plain String
* @param2 : Working key provided by CCAvenue
* @return : Decrypted String
*/
function encrypt($plainText,$key)
{
    $key = $this->hextobin(md5($key));
    $initVector = pack("C*", 0x00, 0x01, 0x02, 0x03, 0x04, 0x05, 0x06, 0x07, 0x08, 0x09, 0x0a, 0x0b, 0x0c, 0x0d, 0x0e, 0x0f);
    $openMode = openssl_encrypt($plainText, 'AES-128-CBC', $key, OPENSSL_RAW_DATA, $initVector);
    $encryptedText = bin2hex($openMode);
    return $encryptedText;
}

/*
* @param1 : Encrypted String
* @param2 : Working key provided by CCAvenue
* @return : Plain String
*/
function decrypt($encryptedText,$key)
{
      // echo $encryptedText;
      // echo $key;


    $key = $this->hextobin(md5($key));
    $initVector = pack("C*", 0x00, 0x01, 0x02, 0x03, 0x04, 0x05, 0x06, 0x07, 0x08, 0x09, 0x0a, 0x0b, 0x0c, 0x0d, 0x0e, 0x0f);
    $encryptedText = $this->hextobin($encryptedText);
    $decryptedText = openssl_decrypt($encryptedText, 'AES-128-CBC', $key, OPENSSL_RAW_DATA, $initVector);
    return $decryptedText;
}

function hextobin($hexString) 
 { 
    $length = strlen($hexString); 
    $binString="";   
    $count=0; 
    while($count<$length) 
    {       
        $subString =substr($hexString,$count,2);           
        $packedString = pack("H*",$subString); 
        if ($count==0)
        {
            $binString=$packedString;
        } 
        
        else 
        {
            $binString.=$packedString;
        } 
        
        $count+=2; 
    } 
        return $binString; 
  } 



public function paytmsuccess(){

	if ($this->session->userdata('front_logged_in')) {
		$session_data = $this->session->userdata('front_logged_in');
		$data['user_id'] = $session_data['user_id'];
		$data['user_name'] = $session_data['user_name'];
		$billing_id=$this->input->post('orderNo');

		$billquery=$this->db->query("select * from tbl_users_billing WHERE billing_id='$billing_id' ");
		$billDetails= $billquery->result_array();
		$plan_id=$billDetails[0]['billing_plan_id'];
		$payment_credit=$billDetails[0]['billing_plan_credit'];
		$payment_amount=$billDetails[0]['billing_plan_amount'];

		$plansTypeDetails = $this->plans_model->get_plansById($plan_id);
			//print_r($plansTypeDetails);
			$plntypname=$plansTypeDetails[0]['plntypname'];
			
// 			if($plntypname=="Pay Monthly")
// 			{
// 		$pautoquery = "SELECT * FROM `tbl_users` where user_id='$user_id' ";
// 		$query2=$this->db->query($pautoquery);
// 		$usrinfo= $query2->result_array();
// 		$total_amount=$usrinfo[0]['total_amount'];
// 		$total_credit=$usrinfo[0]['total_credit'];
// 		$current_amount=$total_amount+$payment_amount;
// 		$current_credit=$total_credit+$payment_credit;
// 		$payment_status="Paid";
// 		$userdata=array(
// 			'total_amount' => $current_amount,
// 			'total_credit' => $current_credit);
// 		$this->db->where('user_id', $user_id);
// 		$report = $this->db->update('tbl_users', $userdata);
// }
		$data=array(
			'user_id' => $user_id,
			'billing_id' => $billing_id,
			'sty_id' => '0',
			'upimg_id' => '0',
			'history_type' => 'Addition',
			'amount' => $payment_amount,
			'credit' => $payment_credit,
			'account_history_date' => date("Y-m-d"),
			'account_history_time' => date("H:i:s"),
			'payment_status' => 'Paid',
		);
		$this->db->insert('tbl_account_history', $data); 
		$data = array(
			'billing_date_time'=>date("Y-m-d H:i:s"),
			'billing_status'=>'Completed',
			'payment_status'=>'Paid',
		); 

		$updateuserrole = $this->plans_model->billing_update($data,$billing_id);

		$status_query1 = $this->db->get_where('tbl_account_cal', array('user_id'=>$user_id,'plan_status'=>'InActive','payment_status'=>'Not Paid'));
				//$query = $this->db->get("tbl_upload_img");
		$status_result1=$status_query1->result_array(); 
		//print_r($status_result1);exit();
		if(!empty($status_result1))
		{
			$account_cal_id=$status_result1[0]['account_cal_id'];
			$account_cal_details['payment_status']="Paid";
			$account_cal_details['plan_status']="Active";
			$result1=$this->Revbook_model->update_account_cal($account_cal_details,$account_cal_id);
			//$account_cal_details['plan_status']="Pending";
		}

		$data['billing_id'] = $billing_id;

		$query=$this->db->query("select * from tbl_users WHERE user_id='$user_id' ");
		$userDetails= $query->result_array();
		$user_name=$userDetails[0]['user_name'];
		$user_email1=$userDetails[0]['user_email'];
			$user_email_value=explode("@",$user_email1);
			$user_email=substr_replace($user_email_value[0], "*****", 2)."@".$user_email_value[1];
		$user_mobile=$userDetails[0]['user_mobile'];
		$user_company=$userDetails[0]['user_company'];
			$user_address=$userDetails[0]['user_address'];
		$billquery=$this->db->query("select * from tbl_users_billing WHERE billing_id='$billing_id' ");
		$billDetails= $billquery->result_array();

		$billing_plan_id=$billDetails[0]['billing_plan_id'];
		$billing_plan_credit=$billDetails[0]['billing_plan_credit'];
		$billing_plan_amount=$billDetails[0]['billing_plan_amount'];
		$currency=$billDetails[0]['currency'];
		$billing_date_time=$billDetails[0]['billing_date_time'];
		$billing_status=$billDetails[0]['billing_status'];
		$payment_status=$billDetails[0]['payment_status'];
		$payment_type=$billDetails[0]['payment_type'];

		$payment_type_id=$billDetails[0]['payment_type'];
			//print_r($billDetails);
			$payment_type_query1=$this->db->query("select * FROM tbl_paymenttype where paytyid=$payment_type_id");

			$payment_typeDetails1= $payment_type_query1->result_array();
			$payment_type_name=$payment_typeDetails1[0]['paytypname'];
			$plansTypeDetails = $this->plans_model->get_plansById($billing_plan_id);

		$book_id=$billDetails[0]['book_id']; 


			if(!empty($book_id)){
				$array = array('book_id' => $book_id);
				$this->db->set('p_status', 'Paid'); 
				$this->db->where($array); 
				$result=$this->db->update('tbl_booking');
			}
			//exit();
			$query=$this->db->query("select * from  tbl_booking where book_id=$book_id");
			$bookDetails=$query->result_array();

			
			$order_id=$bookDetails[0]['order_id'];
			$gender=$bookDetails[0]['gender'];
			$m_status=$bookDetails[0]['m_status'];
			$category=array_filter(explode(",", $bookDetails[0]['category']));	
			$quty=array_filter(explode(",", $bookDetails[0]['quty']));
			$angle=array_filter(explode(",", $bookDetails[0]['angle']));
			$d_status=$bookDetails[0]['d_status'];
			$notes=$bookDetails[0]['notes'];
			$total_price=$bookDetails[0]['total_price'];
			$total_image=$bookDetails[0]['total_image'];
			$create_date=$bookDetails[0]['create_date'];
			$book_date_time=date("D, M d ,Y H:i A",strtotime($create_date));
			//$plan_id=$bookDetails[0]['plan_id'];
			$currency=$bookDetails[0]['currency'];


			$p_status=$bookDetails[0]['p_status'];


			  if($currency=="USD") { $currency1="&#36;"; }
              elseif($currency=="INR") { $currency1="&#x20B9;"; }
              elseif($currency=="EUR") { $currency1="&#128;"; }
              elseif($currency=="GBP") { $currency1="&#163;"; }
              elseif($currency=="AUD") { $currency1="&#36;"; }
              elseif($currency=="CNY") { $currency1="&#165;"; }


			if($gender=="1"){ $gender1= "Male" ; } 
            if($gender=="2"){ $gender1="Female" ; } 
            if($gender=="4"){ $gender1="Boy" ; } 
            if($gender=="6"){ $gender1="Girl" ; }
            if($m_status=="1"){
	           $m_status1= "Let \"Piquic Style Expert\" select my models & style." ; 
	         } 
	         else{ 
	          $m_status1= "I want to select my models and styling once my apparels gets digitized."; 
	         } 
	         if($d_status=="1"){
               $d_status1= "Standard (6-7 days after receiving your products)." ; 
             } 
             else{ 
               $d_status1= "Express (2-3 days after receiving your products)."; 
             } 

		$mail_text='Payment Email Piquic - Paytm';
		$subject='Piquic - Payment success';


		$query=$this->db->query("select * from tbl_settings WHERE setting_key='MAIL_SMTP_USER' ");
		$userDetails= $query->result_array();
		$MAIL_SMTP_USER=$userDetails[0]['setting_value'];
		$query=$this->db->query("select * from tbl_settings WHERE setting_key='MAIL_SMTP_PASSWORD' ");
		$userDetails= $query->result_array();
		$MAIL_SMTP_PASSWORD=$userDetails[0]['setting_value'];
		$query=$this->db->query("select * from tbl_settings WHERE setting_key='MAIL_BODY_BOOKING' ");
		$userDetails= $query->result_array();
		$MAIL_BODY_BOOKING=$userDetails[0]['setting_value'];
		$this->load->library('email');
		$config = array();
		//$config['protocol'] = 'smtp';
		$config['protocol'] = 'mail';
		$config['smtp_host'] = 'localhost';
		$config['smtp_user'] = $MAIL_SMTP_USER;
		$config['smtp_pass'] = $MAIL_SMTP_PASSWORD;
		$config['smtp_port'] = 25;

		$this->email->initialize($config);
		$this->email->set_newline("\r\n"); 
		$from_email = "demo.intexom@gmail.com";
		$to_email = $user_email1;
		$copy_right= date("Y");
		$billing_date_time=date("d/m/y",strtotime($billing_date_time));
			//echo $message=$MAIL_BODY_BOOKING;
		$find_arr = array('#ORDER_ID##','##BOOK_DATE##',"##USER_NAME##","##USER_EMAIL##","##USER_MOBILE##","##USER_COMPANY##","##USER_ADDRESS##","##M_STATUS##","##D_STATUS##","##NOTES##","##P_STATUS##",'##CUR##',"##TOTAL_AMOUNT##", '##PLAN_TYPE##' ,"##TABLE_BODY##",'##TOTAL_IMAGE##','##COPY_RIGHT##','##PAYMENT_TYPE##');
		$replace_arr = array($order_id,$book_date_time,$user_name,$user_email,$user_mobile,$user_company,$user_address, $m_status1,$d_status1,$notes,$p_status,$currency1,$total_price,$plntypname,$table_body,$total_image,$copy_right,$payment_type_name);
		$message=str_replace($find_arr,$replace_arr,$MAIL_BODY_BOOKING);
		// echo $message;exit;
		$this->email->from($from_email, 'Piquic');
		$this->email->to($to_email);
		$this->email->cc('poulami@mokshaproductions.in');
		$this->email->cc('raja.priya@mokshaproductions.in');
		$this->email->subject($subject);
		$this->email->message($message);
		//Send mail
		if($this->email->send())
		{

		//echo "mail sent";exit;
		$this->session->set_flashdata("email_sent","Congragulation Email Send Successfully.");
		}

		else
		{
		//echo $this->email->print_debugger();
		//echo "mail not sent";exit;
		$this->session->set_flashdata("email_sent","You have encountered an error");
		} 

		redirect("booksuccess");


		} else {

		$session_data = $this->session->userdata('front_logged_in');
		$data['user_id'] = $session_data['user_id'];
		$data['user_name'] = $session_data['user_name'];

		redirect('home');
	}

}



/////////////////////////////////////end Papri//////////////////////////////









}

?>