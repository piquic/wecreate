<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
class Genderatethumb extends CI_Controller
{
    public function __construct()
    {
		  parent::__construct();
      $this->load->library('pagination');
      // $this->load->helper('url','file');
      // $this->load->model('Genderatethumb_model');
		  //$this->load->helper("file");
		  $this->load->library('session');
		
    }

    public function index()
    {
      $new_updated_url = "http://203.122.46.51:7088/CMSInternalAPI.svc/InternalAPI/GetFinalImageDetails";
      $url=$new_updated_url;
      $ch = curl_init( $url );
      curl_setopt( $ch, CURLOPT_HTTPHEADER, array('Content-Type:application/json','authorization: aW50ZXJuYWxjbXNAMTIz'));
      curl_setopt( $ch, CURLOPT_RETURNTRANSFER, true );
      $result = curl_exec($ch);
      $err = curl_error($ch);
      curl_close($ch);
          // sleep(3);
      if ($err) {
        echo "cURL Error #:" . $err;
      } 
      else {
        $res_arr=json_decode($result);
        $details_array = $res_arr->Details;
        foreach ($details_array as $key => $value) {
          $user_id = $details_array[$key]->UserID;
          $order_no = $details_array[$key]->OrderNo;
          $sku_no = $details_array[$key]->SKUNo;
          if (!file_exists('sku_img_thumb/'.$sku_no)) {
           mkdir('sku_img_thumb/'.$sku_no, 0777, true);
          }
         $ImagePath_array =  $details_array[$key]->ImagePath;
         foreach ($ImagePath_array as $ImagePath_array_key => $ImagePath_array_value) {
          $sku_img_path = $ImagePath_array_value->Path;
                  // echo $sku_img_path;
          $sku_img_path = str_replace("\\","/",$sku_img_path);
//echo $sku_img_path."<br>";



          $arr = $sku_img_path;
          $arr = str_replace("\\","/",$arr);
                          // echo $arr;
          $arr_array = explode("/", $arr);
          $original_img_name = end($arr_array);

          $ch = curl_init($sku_img_path);
          $fp = fopen('sku_img_thumb/'.$sku_no.'/'.$original_img_name, 'wb');
          curl_setopt($ch, CURLOPT_FILE, $fp);
          curl_setopt($ch, CURLOPT_HEADER, 0);
          curl_exec($ch);
          curl_close($ch);
          fclose($fp);

          $path_parts = pathinfo($original_img_name);
          $path_parts_thumb = $path_parts['filename'];
          $im = 'sku_img_thumb/'.$sku_no.'/'.$path_parts_thumb."_thumb.jpg";
          $new_thumb = 'sku_img_thumb/'.$sku_no.'/'.$path_parts_thumb."_thumb.jpg";
//print_r($im);

          if(!file_exists($new_thumb)){
            $width = "400";
            $height = "400";
            $quality = 90;
            $img = 'sku_img_thumb/'.$sku_no.'/'.$original_img_name;

//Generate Thumbnail Images

            $file = $img;
            $dest = $new_thumb;
            $height = 400;
            $width = 400;
            $output_format = "jpg";
            $output_quality = 90;
            $bg_color = array(255, 255, 255);


//Justify the Image format and create a GD resource
            $image_info = getimagesize($file);
            $image_type = $image_info[2];
            switch($image_type){
              case IMAGETYPE_JPEG:
              $image = imagecreatefromjpeg($file);
              break;
              case IMAGETYPE_GIF:
              $image = imagecreatefromgif($file);
              break;
              case IMAGETYPE_GIF:
              $image = imagecreatefrompng($file);
              break;
              default:
              die("Image Format not Suppoted");
            }

            $image_width = imagesx($image);
            $image_height = imagesy($image);

//Get The Calculations
// Calculate the size of the Image
//If Image width is bigger than the Thumbnail Width
            if($image_width>$height){

      //Set Image Width to Thumbnail Width
              $new["width"] = $width;
      //Calculate Height according to width
              $new["height"] = ($new["width"]/$image_width)*$image_height;

      //If Resulting height is bigger than the thumbnail Height
              if($new["height"]>$height){

          //Set the image Height to THUmbnail Height
                $new["height"] = $height;
          //Recalculate width according to height of the thumbnail
                $new["width"] = ($new["height"]/$image_height)*$image_width;

              }

            }else{

              $new["height"] = $height;
              $new["width"] = ($new["height"]/$image_height)*$image_width;

              if($new["width"]>$width){

                $new["width"] = $width;
                $new["height"] = ($new["width"]/$image_width)*$image_height;

              }

            }

  //Calculate the image position based on the difference between the dimensons of the new image and thumbnail
            $x = ($width-$new["width"])/2;
            $y = ($height-$new["height"])/2;

            $calc =  array_merge($new, array("x"=>$x,"y"=>$y));


// End Calculate The Image



//Create an Empty image
            $canvas = imagecreatetruecolor($width, $height);

  //Load Background color
            $color = imagecolorallocate($canvas,
              $bg_color[0],
              $bg_color[1],
              $bg_color[2]
            );

  //FIll the Image with the Background color
            imagefilledrectangle(
              $canvas,
              0,
              0,
              $width,
              $height,
              $color
            );

  //The REAL Magic
            imagecopyresampled(
              $canvas,
              $image,
              $calc["x"],
              $calc["y"],
              0,
              0,
              $calc["width"],
              $calc["height"],
              $image_width,
              $image_height
            );

// Create Output Image

            $image = $canvas;
            $format = $output_format;
            $quality = $output_quality;

            switch($format){
              case "jpg":
              imagejpeg($image, $dest, $quality);
              break;
              case "gif":
              imagegif($image, $dest);
              break;
              case "png":
          //Png Quality is measured from 1 to 9
              imagepng($image, $dest, round(($quality/100)*9) );
              break;
            }

            unlink($img);

          }
          else{
            echo "file is exit";
          }
        }

      }
    }


  }
  
}

?>
