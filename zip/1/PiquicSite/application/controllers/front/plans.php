<?php 
class Plans extends CI_Controller{
	
	function __construct(){
		parent::__construct();
		$this->load->library(['session']); 
        $this->load->helper(['url','file','form']); 
		$this->load->model('upload_model'); //load model upload 
		$this->load->model('plans_model'); //load model upload 
		$this->load->library('upload'); //load library upload 
	}

	public function index(){


		if ($this->session->userdata('front_logged_in')) {
            $session_data = $this->session->userdata('front_logged_in');
            $data['user_id'] = $session_data['user_id'];
            $data['user_name'] = $session_data['user_name'];
            $user_id = $session_data['user_id'];


			$stylequery = "SELECT * FROM `tbl_users` where  user_id='$user_id' ";
			$styleres=$this->db->query($stylequery);
			$stylerow=$styleres->result_array();
			$user_currency = $stylerow[0]['user_currency'];

			if($user_currency!='')
			{
				$currency=$user_currency;
			}
			else
			{
			    $currency="USD";
			}
            
           // $currency;

            $data['currency_default'] = $currency;
            
			$data['plansTypeDetails'] = $this->plans_model->getAllPlans($currency);
			$data['billingDetails'] = $this->plans_model->getAllbilling($data['user_id']);


		    $this->load->view('front/plans',$data);
			
         
        } else {
        	
            $session_data = $this->session->userdata('front_logged_in');
            $data['user_id'] = $session_data['user_id'];
            $data['user_name'] = $session_data['user_name'];
            
			redirect('home');
        }
			

       
		
	}



		public function getPlanDetailByCurrency(){


		if ($this->session->userdata('front_logged_in')) {
            $session_data = $this->session->userdata('front_logged_in');
            $data['user_id'] = $session_data['user_id'];
            $data['user_name'] = $session_data['user_name'];
            $user_id = $session_data['user_id'];




         
            $currency=$this->input->post('currency');

            $this->db->query("update  tbl_users set user_currency='$currency' where user_id='$user_id' ");



            
			$data['plansTypeDetails'] = $this->plans_model->getAllPlans($currency);
			$data['billingDetails'] = $this->plans_model->getAllbilling($data['user_id']);


		    $this->load->view('front/plans_ajax_currency',$data);
			
         
        } else {
        	
            $session_data = $this->session->userdata('front_logged_in');
            $data['user_id'] = $session_data['user_id'];
            $data['user_name'] = $session_data['user_name'];
            
			redirect('home');
        }
			

       
		
	}








	

}

?>