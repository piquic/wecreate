<?php 
class Myaccount extends CI_Controller{
	
	function __construct(){
		parent::__construct();
		$this->load->library(['session']); 
        $this->load->helper(['url','file','form']);
        $this->load->model('UpdateUser_model');
	}

	public function index(){


		if ($this->session->userdata('front_logged_in')) {
            $session_data = $this->session->userdata('front_logged_in');
            $data['user_id'] = $session_data['user_id'];
            $data['user_name'] = $session_data['user_name'];
            $user_id = $session_data['user_id'];
            
            $param = $this->uri->segment('2');
            $data['param'] = $param;


            $stylequery = "SELECT * FROM `tbl_users` where  user_id='$user_id' ";
			$styleres=$this->db->query($stylequery);
			$stylerow=$styleres->result_array();
			$user_currency = $stylerow[0]['user_currency'];
			$login_type = $stylerow[0]['login_type'];

			if($user_currency!='')
			{
				$currency=$user_currency;
			}
			else
			{
			    $currency="USD";
			}
            
           // $currency;

            $data['currency_default'] = $currency;
            $data['login_type'] = $login_type;

            $data['user_details'] = $this->UpdateUser_model->get_usr_data($data['user_id']);

			$this->load->view('front/myaccount', $data);
			
         
        } else {
        	redirect('home');
        }
		
	}

	public function usrUpdate()
	{
		$user_id=$this->input->post('txtUserId');

		$data = array(
			'user_name'=>$this->input->post('txtUser'),
			'user_company'=>$this->input->post('txtCompany'),
			'user_email'=>$this->input->post('txtEmail'),
			'user_mobile'=>$this->input->post('txtPhone'),
			'user_address'=>$this->input->post('txtAddress'),
			'user_currency'=>$this->input->post('currency'),
		);

		//print_r($data);

		if($this->UpdateUser_model->update_user($data, $user_id)){
			redirect('myaccount/success');
		} else {
			redirect('myaccount/error');
		}
	}

	public function pswdUpdate()
	{
		$user_id = $this->input->post('txtUserIdPswd');

		$data['user_details'] = $this->UpdateUser_model->get_usr_data($user_id);

		// print_r($data['user_details']);
		
		$usrpswd = $data['user_details'][0]['user_password'];
		
		$oldPswd = sha1($this->input->post('txtOldPswd'));
		$newPswd = sha1($this->input->post('txtPswd'));
		$txtRePswd = sha1($this->input->post('txtRePswd'));

		if($oldPswd == $usrpswd){
			$data = array(
				'user_password' => $newPswd,
			);

			$this->UpdateUser_model->update_pswd($data, $user_id);
						//updated by rajendra			
			$sql = "SELECT * FROM tbl_users WHERE user_id = '$user_id'";
	    $query = $this->db->query($sql);
	    if( $query->num_rows() ==1 ){
	    	
	    	$userDetails= $query->result_array();
	    	$user_id=$userDetails[0]['user_id'];
			$user_name=ucfirst($userDetails[0]['user_name']);
			$user_email1=$userDetails[0]['user_email'];
			$user_email_value=explode("@",$user_email1);
			$user_email=substr_replace($user_email_value[0], "*****", 2).'@'.$user_email_value[1];
			$copy_right= date("Y");
			$url=base_url()."reset_password/".$user_id."/".time();
			//print_r($url);
			$subject='Piquic Account - Password Reset';
			//echo base64_encode("http://localhost/piquic_client_new/reset_password");	       	

			/*********************************Send mail*************************************/

			$query=$this->db->query("select * from tbl_settings WHERE setting_key='MAIL_SMTP_USER' ");
			$userDetails= $query->result_array();
			$MAIL_SMTP_USER=$userDetails[0]['setting_value'];

			$query=$this->db->query("select * from tbl_settings WHERE setting_key='MAIL_SMTP_PASSWORD' ");
			$userDetails= $query->result_array();
			$MAIL_SMTP_PASSWORD=$userDetails[0]['setting_value'];

			$query=$this->db->query("select * from tbl_settings WHERE setting_key='MAIL_BODY_PASSWORD_SUCCESS' ");
			$userDetails= $query->result_array();
			$MAIL_BODY_PASSWORD_SUCCESS=$userDetails[0]['setting_value'];
			//Load email library
			$this->load->library('email');
			$config = array();
			//$config['protocol'] = 'smtp';
			$config['protocol'] = 'mail';
			$config['smtp_host'] = 'localhost';
			$config['smtp_user'] = $MAIL_SMTP_USER;
			$config['smtp_pass'] = $MAIL_SMTP_PASSWORD;
			$config['smtp_port'] = 25;
			$this->email->initialize($config);
			$this->email->set_newline("\r\n");
			$from_email = "demo.intexom@gmail.com";
			$to_email = $user_email1;
			$message=$MAIL_BODY_PASSWORD_SUCCESS;
			$find_arr = array("##USER_NAME##","##USER_EMAIL##",'##COPY_RIGHT##');
			$replace_arr = array($user_name,$user_email,$copy_right);
			$message=str_replace($find_arr,$replace_arr,$MAIL_BODY_PASSWORD_SUCCESS);
			// echo $message;exit;
			$this->email->from($from_email, 'Piquic');
			$this->email->to($to_email);
			$this->email->cc('poulami@mokshaproductions.in');
			$this->email->cc('pranav@mokshaproductions.in');
			$this->email->bcc('raja.priya@mokshaproductions.in');
			$this->email->subject($subject);
			$this->email->message($message);
			//Send mail
			if($this->email->send())
			$this->session->set_flashdata("email_sent","Congragulation Email Send Successfully.");
			else
			$this->session->set_flashdata("email_sent","You have encountered an error");
			//exit;

			/************************************************************************/
	    } else {
	        echo 'flase';
	    }								
		//update by rajendra
			
			redirect('myaccount/success');
		} else {
			redirect('myaccount/error');
		}

	}





	public function checkexistPass()
	{

			if ($this->session->userdata('front_logged_in')) {
						$session_data = $this->session->userdata('front_logged_in');
						$data['user_id'] = $session_data['user_id'];
						$data['user_name'] = $session_data['user_name'];
						$user_id = $session_data['user_id'];			

						if($this->input->post('txtOldPswd'))
						{
						$txtOldPswd = sha1($this->input->post('txtOldPswd'));
						  $sql = "SELECT user_id,user_email,user_password FROM tbl_users WHERE user_password = '$txtOldPswd' and user_id='$user_id' ";
						$query = $this->db->query($sql);
						if( $query->num_rows() > 0 ){
						echo 'true';
						} else {
						echo 'false';
						}
						}


			}
			else
			{
				redirect('home');

			}
				
		
	}









}

?>