<?php 
class stylequeue extends CI_Controller{
	
	function __construct(){
	parent::__construct();
	$this->load->library(['session']); 
	$this->load->database();
    $this->load->helper(['url','file','form']);
    $this->load->helper('directory');
    $this->load->helper('file'); 
	$this->load->model('stylequeue_model');
    $this->load->library('user_agent');
  



	}

    public function autostyleview($sku_key=""){

     if ($this->session->userdata('front_logged_in')) {
    $session_data = $this->session->userdata('front_logged_in');
    $data['user_id'] = $session_data['user_id'];
    $data['user_name'] = $session_data['user_name'];
    }
    else
    {
    $data['user_id'] = '';
    $data['user_name'] = '';
    }

         $data['sku_key'] = $sku_key;
        
    $this->load->view('front/autostyle',$data);
        
    }

	public function stylequeueview($sku_key="",$filter_val=""){

     if ($this->session->userdata('front_logged_in')) {
    $session_data = $this->session->userdata('front_logged_in');
    $data['user_id'] = $session_data['user_id'];
    $data['user_name'] = $session_data['user_name'];
    }
    else
    {
    $data['user_id'] = '';
    $data['user_name'] = '';
    }

		
    $sku_key1="";
    $filter_val1="";
    if($sku_key=="-")
    {
        $sku_key1="";
        $data['sku_key'] = "";
    }
    else{
         $sku_key1=$sku_key;
         $data['sku_key'] = $sku_key;
        
    
    }

     if($filter_val=="-")
    {
        $filter_val1="";
        $data['filter_val'] = "";
    }
     else{
         $filter_val1=$filter_val;
         $data['filter_val'] = $filter_val;
    }


    



    $allvalue=$this->stylequeue_model->queueimages($sku_key1,$filter_val1);  
    $data['stylequeinfo']=$allvalue;

    //echo count($allvalue);

    /*if(count($allvalue)!='0')
    {
      $this->load->view('front/stylequeue',$data);
    } 
    else{
         redirect('stylequeue/-/3');

    } */


    if($filter_val1!='3')
    {
            if(count($allvalue)!='0')
            {
            $this->load->view('front/stylequeue',$data);
            } 
            else{
            redirect('stylequeue/-/3');

            } 
    }
    else
    {
            $this->load->view('front/stylequeue',$data);
    }



   
  
		
	}
 
  function mulmodelupdate(){	    
     $skuno=$this->input->post('skuno');
     $model=$this->input->post('model');   
   
       $data=$this->stylequeue_model->updatemult_modskuinfo($skuno,$model);
       echo $data;
    }

  function autostylingUpdate(){     
     $sku=$this->input->post('sku');
     $cat=$this->input->post('cat');
     $gen=$this->input->post('gen');
     $autoval=$this->input->post('autoval');   
   
       $data=$this->stylequeue_model->update_autostylingUpdate($sku,$cat,$gen,$autoval);
       echo json_encode($data);
    }

 
  function autostylingskufuc(){	    
     $skuno=$this->input->post('sku');
     $autoval=$this->input->post('autoval');   
   
       $data=$this->stylequeue_model->update_autostylingsku($skuno,$autoval);
       echo json_encode($data);
    }



      function vendorsku(){	    
     $skuno=$this->input->post('skuno');
     $vendor=$this->input->post('vendor');   
   
       $data=$this->stylequeue_model->update_vendorsku($skuno,$vendor);
       echo json_encode($data);
    }


    function getModelByGender(){      
     $gender_id=$this->input->post('gender_id');
        
   
       $modelinfo=$this->stylequeue_model->getmodelbygender($gender_id);

        $str='<select class="custom-select navSlct mt-2 text-white" name="slct_model" id="slct_model" onchange="modlmultsku(this);">
        <option class="text-piquic" value="0" >Model</option>';


        foreach($modelinfo as $mrow)
        {


        $mid = $mrow['m_id'];
        $mod_name = $mrow['mod_name'];
        $mod_name =strtoupper($mod_name);
        $str.='<option class="text-piquic" value="'.$mid.'">'.$mod_name.'</option>';
        } 
        $str.='</select>';
          echo $str;
    }


      function updateprocessing(){  

        if ($this->session->userdata('front_logged_in')) {
            $session_data = $this->session->userdata('front_logged_in');
            $data['user_id'] = $session_data['user_id'];
            $data['user_name'] = $session_data['user_name'];
            $user_id = $session_data['user_id'];

            $skuno=$this->input->post('skuno');
            $sty_id=$this->input->post('sty_id');   

            $data=$this->stylequeue_model->update_processing($skuno,$sty_id);
            echo json_encode($data);
      
         
        } else {
          
            $session_data = $this->session->userdata('front_logged_in');
            $data['user_id'] = $session_data['user_id'];
            $data['user_name'] = $session_data['user_name'];
            
      redirect('home');
        }
    

       
      
    }




function deleteStyleque(){
         $skuno=$this->input->post('skuno');
         $sty_id=$this->input->post('sty_id');
       $data=$this->stylequeue_model->delete_skuStyleque($skuno,$sty_id);
        echo json_encode($data);
    }





function genCutoutFrmApi(){


         $upimg_id=$this->input->post('upimg_id');
         $skuno=$this->input->post('skuno');
         $user_id=$this->input->post('user_id');

        /*$sql = "SELECT * FROM tbl_upload_img WHERE queue='1' AND  AND  cutout_flag='0' and upimg_id IN(SELECT DISTINCT upimg_id  FROM tbl_upload_images WHERE   img_name LIKE '%Front%' AND img_name NOT LIKE '%thumb_%') AND category != ''  order by upimg_id asc ";*/

       $sql = "SELECT * FROM tbl_upload_img WHERE queue='1' AND upimg_id='$upimg_id' AND  cutout_flag='0' and upimg_id IN(SELECT DISTINCT upimg_id  FROM tbl_upload_images WHERE   img_name LIKE '%Front%' AND img_name NOT LIKE '%thumb_%') AND category != ''  order by upimg_id asc ";
        $query=$this->db->query($sql);
        $result = $query->result_array();

        foreach($result as $key => $value)
        {

            $id=$value['upimg_id'];
            $user_id=$value['user_id'];
            $sku=$value['zipfile_name'];
            $category=$value['category'];


                $query=$this->db->query("SELECT * FROM tbl_upload_images WHERE upimg_id='$id' and  img_name LIKE '%Front%' AND img_name NOT LIKE '%thumb_%' ");
                $row2 = $query->result_array();
                $value1=$row2[0];



                $img_name=$value1['img_name'];

                $image_path="upload\\".$user_id."\\".$sku."\\".$img_name."";
                $filepath="upload\\".$user_id."\\".$sku."\\";
                $filedata = realpath($image_path);
                $filename = $img_name;

                $data = array(
                'filedata' => $image_path,
                'filename' => $filename,
                'filepath' => $filepath,
                'category' => $category,
                );

                //echo "<pre>";
                //print_r($data);


                if (file_exists($image_path)) {

                //API URL
                $url = 'http://172.25.0.92:8007/image_cutout';

                //create a new cURL resource
                $ch = curl_init($url);

                //setup request to send json via POST

                $payload = json_encode($data);

                //attach encoded JSON string to the POST fields
                curl_setopt($ch, CURLOPT_POSTFIELDS, $payload);

                //set the content type to application/json
                curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type:application/json'));

                //return response instead of outputting
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

                //execute the POST request
                $result = curl_exec($ch);

                $result_arr=json_decode($result);
                curl_close($ch);

                //echo "<pre>";
                //print_r($result_arr);
                //echo $result_arr[0]->response;


                if($result_arr[0]->response=='Success')
                {

                $update = "UPDATE tbl_upload_img SET cutout_flag='1' WHERE upimg_id='$upimg_id' ";
                $this->db->query($update);

                echo "Success";


                //echo "Success For ".$sku."<br>";
                }

                //close cURL resource

                }

        }


       
         
    }



}
?>