<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Order_upload extends CI_Controller
{
    public function __construct()
	{
		parent::__construct();		
		$this->load->library('pagination');
		$this->load->database();		
		$this->load->library('Upload');
		$this->load->helper('security');	
		$this->load->library('Form_validation');
		$this->load->helper(array('form', 'html', 'file'));
		$this->load->library('pagination');
		$this->load->library('email');
        $this->load->model('orders_upload_model');
        $this->load->library('userlib');
		
    	if ($this->session->userdata('logged_in')) {
            $session_data = $this->session->userdata('logged_in');
            $data['username'] = $session_data['username'];
            $data['firstname'] = $session_data['firstname'];
            $data['rolecode'] = $session_data['rolecode'];
			/* $timezone = $session_data['timezone'];
			date_default_timezone_set($timezone); */
         
        } else {
            //If no session, redirect to login page
            redirect('login', 'refresh');
        }
    }
	
    public function index()
	{
		if ($this->session->userdata('logged_in'))		
		{  
			$session_data = $this->session->userdata('logged_in');
			$data['rolecode'] = $session_data['rolecode'];
			$data['usertype'] = $session_data['rolecode'];
			$data['roles'] = $session_data['roles'];
			$data['title'] = "Users";
			$slang="english";
			$this->lang->load($slang, $slang);
			$data['lang']=$this->lang->language; 
			
			//echo "aaaaaaaaa";exit;
			$this->load->view('admin/orderupload/order_upload_list', $data);
		} 
		else 
		{
			//If no session, redirect to login page
			redirect('login', 'refresh');
		}
	}
	
	public function orderlist(){
		if ($this->session->userdata('logged_in')) 
        {
			$session_data=$this->session->userdata('logged_in');
			$usertype = $session_data['rolecode'];
			$userid = $session_data['id'];
			$slang="english";
		    $this->lang->load($slang, $slang);
		    $data['lang']=$this->lang->language; 
    	   /* $aColumns = array( 'sty_id', 'user_id','skuno','gender','category','model_id','style_amount','style_credit','style_pending_amount','status_type','style_pending_credit','style_payment_status');*/
    	   $gender_select="(CASE tbl_style.gender
				WHEN 1 THEN 'Male'
				WHEN 2 THEN 'Female'
				WHEN 3 THEN 'Boys (0-2 years)'
				WHEN 4 THEN 'Boys (2-9 years)'
				WHEN 5 THEN 'Girls (0-2 years)'
				WHEN 6 THEN 'Girl (2-9 years)'
				ELSE 'None'
				END)";

    	    $aColumns = array( 'tbl_style.sty_id', 'tbl_style.user_id','tbl_users.user_name','tbl_style.skuno',''.$gender_select.' AS gender','tbl_style.category','tbl_style.model_id','tbl_style.style_amount','tbl_style.style_credit','tbl_style.style_pending_amount','tbl_style.status_type','tbl_style.style_pending_credit','tbl_style.style_payment_status');
         
			/* Indexed column (used for fast and accurate table cardinality) */
			$sIndexColumn = "sty_id";
         
			/* DB table to use */
			$sTable = "tbl_style";

            /*
			 * Paging
			 */
			$sLimit = "";
			if ( isset( $_GET['start'] ) && $_GET['length'] != '-1' )
			{
				$sLimit = "LIMIT ".intval( $_GET['start'] ).", ".
					intval( $_GET['length'] );
			}


    
     
			$sOrder = "";
			if ( isset( $_GET['order'] ) )
			{
				$sOrder = "ORDER BY  ";
			
						$sOrder .= $aColumns[0]."
							".($_GET['order'][0]['dir']==='desc' ? 'asc' : 'desc') .", ";
			   
				 
				$sOrder = substr_replace( $sOrder, "", -2 );
				if ( $sOrder == "ORDER BY" )
				{
					$sOrder = "";
				}
			}
	

        
        $commn="1=1 and tbl_style.status='2'";
        
		
		//echo $_REQUEST['columns'][1]['search']['value']; exit();
		
		if(!empty($_GET['columns'][1]['search']['value']) ){  
	
		$commn.=" AND tbl_style.sty_id  LIKE '".$_GET['columns'][1]['search']['value']."%' ";
		}

        $sWhere="where $commn";
         
		
		if (isset($_GET['search']['value']) && $_GET['search']['value'] != "" )
		{
			 $sWhere = "WHERE $commn   AND (";
			for ( $i=0 ; $i<count($aColumns) ; $i++ )
			{
				
				/*if ( isset($_GET['columns'][$i]['searchable']) && $_GET['columns'][$i]['searchable'] == "true" )
				{*/
					//$sWhere .= $aColumns[$i]." LIKE '%".( $_GET['search']['value'] )."%' OR ";
				/*}*/
				if($i=='4')
				{
					$sWhere .= " (CASE tbl_style.gender
				WHEN 1 THEN 'Male'
				WHEN 2 THEN 'Female'
				WHEN 3 THEN 'Boys (0-2 years)'
				WHEN 4 THEN 'Boys (2-9 years)'
				WHEN 5 THEN 'Girls (0-2 years)'
				WHEN 6 THEN 'Girl (2-9 years)'
				ELSE 'None'
				END) LIKE '%".( $_GET['search']['value'] )."%' OR ";
				}
				else
				{
					$sWhere .= $aColumns[$i]." LIKE '%".( $_GET['search']['value'] )."%' OR ";
				}
				
			}
			$sWhere = substr_replace( $sWhere, "", -3 );
			$sWhere .= ')';
		}
		
		
       $join=' INNER JOIN tbl_users ON tbl_style.user_id=tbl_users.user_id';
        /*
         * SQL queries
         * Get data to display
         */
        $sQuery = "
            SELECT SQL_CALC_FOUND_ROWS ".str_replace(" , ", " ", implode(", ", $aColumns))."
            FROM   $sTable $join
            $sWhere
            $sOrder
            $sLimit";

       //echo  $sQuery; exit();

        $rResult = $this->db->query($sQuery);
        $rResult = $rResult->result_array();
        //$rResult = mysqli_query( $sQuery) or fatal_error( 'MySQL Error: ' . mysqli_errno() );

        
        /* Data set length after filtering */
        //$sQuery = "SELECT FOUND_ROWS()";
        //$rResultFilterTotal = mysqli_query( $sQuery) or fatal_error( 'MySQL Error: ' . mysqli_errno() );
        //$query = $this->db->query($sQuery);
        //$aResultFilterTotal = $query->result_array();
        //$aResultFilterTotal = mysqli_fetch_array($rResultFilterTotal);
       $iFilteredTotal = count( $rResult);
          
        /* Total data set length */
        $sQuery = "
            SELECT COUNT(".$sIndexColumn.") as count
            FROM   $sTable
        ";
        //$rResultTotal = mysqli_query( $sQuery) or fatal_error( 'MySQL Error: ' . mysqli_errno() );
        //$aResultTotal = mysqli_fetch_array($rResultTotal);

        $query = $this->db->query($sQuery);
        $aResultTotal = $query->result_array();

        //echo "<pre>";
        //print_r($aResultTotal);
         $iTotal = $aResultTotal[0]['count'];
         
        /*
         * Output
         */
        $output = array(
                        "sEcho" => '',
                        "iTotalRecords" => $iFilteredTotal,
                        "iTotalDisplayRecords" => $iTotal,
                        "aaData" => array()
                        );

            /*$sColumns = array( 'sty_id', 'user_id','skuno','gender','category','qnty','style_payment_status','action');*/
            $sColumns = array( 'sty_id', 'user_id','skuno','gender','category','qnty','style_payment_status','action');
         
            //while ( $aRow = mysqli_fetch_array( $rResult ) )
            //echo "<pre>";
           //print_r($output);
         
            foreach($rResult as $aRow)
            {
                $row = array();
            	
				$sty_id=$aRow['sty_id'];
				$user_id=$aRow['user_id'];
				$status_type=$aRow['status_type'];
				$payment_status=$aRow['style_payment_status'];
				$skuno=$aRow['skuno'];
				
				
                for ( $i=0 ; $i<count($sColumns) ; $i++ )
                {
                  
            	    if($sColumns[$i] =='action')
            		{
            			
            				$row[]='<a href="javascript:void(0)"  class="btn btn-success view" val="'.$sty_id.'"><i class="fa fa-fw fa-eye"></i></a>
        			  ';
						
						// $row[]='<a href="javascript:void(0)"  class="btn btn-success view" val="'.$order_id.'"><i class="fa fa-fw fa-eye"></i></a>&nbsp;<a href="javascript:void(0)"  class="btn btn-info edit" val="'.$order_id.'"><i class="fa fa-edit"></i></a>&nbsp; <a href="javascript:void(0)"  class="btn btn-danger delete" val="'.$order_id.'"><i class="fa fa-trash-o"></i></a>
      //   			  ';
						
            		}

            		else if($sColumns[$i] =='sty_id')
            		{


            			
							$row[]='Piquic_'.$user_id.$sty_id;
						
            		}

            		else if($sColumns[$i] =='user_id')
            		{


            			$ordertype_details=$this->db->query("select * from tbl_users where user_id='$user_id'")->result_array();
						if(!empty($ordertype_details)){
						 $row[]=$ordertype_details[0]['user_name'];
						}
						else
						{
							$row[]='';
						}
            		}

            		else if($sColumns[$i] =='qnty')
            		{


            			
            			$ordertype_details=$this->db->query("select * from tbl_upload_img where user_id='$user_id' and zipfile_name='$skuno' ")->result_array();
						if(!empty($ordertype_details)){
						 $upimg_id=$ordertype_details[0]['upimg_id'];
							$ordertype_details=$this->db->query("select count(*) as total_img from tbl_upload_images where upimg_id='$upimg_id' ")->result_array();
							if(!empty($ordertype_details)){
							$row[]=$ordertype_details[0]['total_img'];
							}


						}
						else
						{
							$row[]='';
						}
            		}
					
				
					// else if($sColumns[$i] =='status')
     //        		{
            			
     //        			 $Enable_status="";
			  //            $Disable_status="";
						 
						 
					//     if($status=='0')
					// 	{
					// 	 $Enable_status="selected";
			  //            $Disable_status="";
						
					// 	}
					// 	else if($status=='1')
					// 	{
					// 	 $Enable_status="";
			  //            $Disable_status="selected";
						 
					// 	}
						
            			
     //        			 $row[]='<select name="status" id="status" class="selectpicker update_status">
					// 			 <option value="0@@@'.$order_id.'"  '.$Enable_status.'>Enable</option>
					// 			 <option value="1@@@'.$order_id.'"  '.$Disable_status.'>Disable</option>
					// 			 </select>';
            			 
     //        		}
                    else if ( $sColumns[$i] != ' ' )
                    {
                        /* General output */
                        $row[] = $aRow[ $sColumns[$i] ];
                    }
                }
                $output['aaData'][] = $row;
            }
            //print_r($output);exit;
        echo json_encode( $output );
    }
	}
	
	
	public function add($order_id=''){
		
		if ($this->session->userdata('logged_in'))		
		{  
			$session_data = $this->session->userdata('logged_in');
			$slang="english";
		    $this->lang->load($slang, $slang);
		    $data['lang']=$this->lang->language; 
			$data['title'] = "User Role";
			if($order_id!=''){
				$order_details=$this->db->query("select * from tbl_booking where order_id='$order_id'")->result_array();
				if(!empty($order_details)){
					$data['order_id'] = $order_details[0]['order_id'];
					$data['order_for'] = $order_details[0]['order_for'];
					$data['plntyid_sel'] = $order_details[0]['plntyid'];
					$data['order_currency'] = $order_details[0]['order_currency'];
					$data['order_currency_symbol'] = $order_details[0]['order_currency_symbol'];
					$data['order_name'] = $order_details[0]['order_name'];
					$data['order_amount'] = $order_details[0]['order_amount'];
					$data['order_credit'] = $order_details[0]['order_credit'];
					$data['order_per_image'] = $order_details[0]['order_per_image'];
					$data['status'] = $order_details[0]['status'];
					$data['deleted'] = $order_details[0]['deleted'];
					
					
				}
			}else {
			        $data['order_id'] = '';
			        $data['order_for'] = '';
					$data['plntyid_sel'] = '';
					$data['order_currency'] = '';
					$data['order_currency_symbol'] = '';
					$data['order_name'] = '';
					$data['order_amount'] = '';
					$data['order_credit'] = '';
					$data['order_per_image'] = '';
					$data['status'] = '';
					$data['deleted'] = '';
					
					
			}
			$this->load->view('admin/orderupload/order_management', $data);
		} 
		else 
		{
			//If no session, redirect to login page
			redirect('login', 'refresh');
		}
	}
	
	

	
	public function insert_order(){
		
		if ($this->session->userdata('logged_in')) {
			$session_data = $this->session->userdata('logged_in');
			$created_date = date('Y-m-d');
			$userid=$session_data['id'];
			$order_id=$this->input->post('order_id');
			$order_currency=$this->input->post('order_currency');



		if($order_currency=='INR')
 		{
 			$order_currency_symbol='&#x20B9;';
 		}
 		else if($order_currency=='USD')
 		{
 			$order_currency_symbol='&#36;';
 		}
 		else if($order_currency=='EUR')
 		{
 			$order_currency_symbol='&#128;';
 		}
 		else if($order_currency=='GBP')
 		{
 			$order_currency_symbol='&#163;';
 		}
 		else if($order_currency=='AUD')
 		{
 			$order_currency_symbol='&#36;';
 		}
 		else if($order_currency=='CNY')
 		{
 			$order_currency_symbol='&#165;';
 		}

 		else
 		{
 			$order_currency_symbol='';
 		}
			
			if($order_id!='')
			{
				//echo $this->input->post('order_for');exit();
				  $data = array(
				    'order_id'=>$this->input->post('order_id'),
				    'order_for'=>$this->input->post('order_for'),
					'plntyid'=>$this->input->post('plntyid'),
					'order_currency'=>$this->input->post('order_currency'),
					'order_currency_symbol'=>$order_currency_symbol,
					'order_name'=>$this->input->post('order_name'),
					'order_amount'=>$this->input->post('order_amount'),
					'order_credit'=>$this->input->post('order_credit'),
					'order_per_image'=>$this->input->post('order_per_image'),
					'status'=>$this->input->post('status'),
					);
					
				  $updateuserrole = $this->orders_upload_model->order_update($data,$order_id);
			      echo $order_id;
				
			}else {
				
				
				
				   $data = array(
				    'order_id'=>$this->input->post('order_id'),
				    'order_for'=>$this->input->post('order_for'),
					'plntyid'=>$this->input->post('plntyid'),
					'order_currency'=>$this->input->post('order_currency'),
					'order_currency_symbol'=>$order_currency_symbol,
					'order_name'=>$this->input->post('order_name'),
					'order_amount'=>$this->input->post('order_amount'),
					'order_credit'=>$this->input->post('order_credit'),
					'order_per_image'=>$this->input->post('order_per_image'),
					'status'=>$this->input->post('status'),
					);

					$this->orders_upload_model->order_insert($data);
					$order_id=$this->db->insert_id();
					
					
					echo $order_id;
				}
			
		}else {
		
            redirect('login', 'refresh');
       
		}  
	}
		
		
		
		

 
	public function deleteorder()
    {
		if ($this->session->userdata('logged_in')) 
		{
			$order_id = $this->input->post('order_id');
		
			//$this->db->query("delete from user where user_id='$user_id'");
			$this->db->query("update tbl_booking set deleted ='1',status='1' where order_id ='$order_id'");
			$this->session->set_flashdata('added', '<div align="left" style="color:green; font-size:15px; border:1px #398AB9 solid;">
		    Service Deleted Successfully</div>');

			//redirect('users');
		} 
		else 
		{
			//If no session, redirect to login page
			redirect('login', 'refresh');
		}
	}
	
	

	public function view($sty_id=''){
		
		
		if ($this->session->userdata('logged_in'))		
		{  
			$session_data = $this->session->userdata('logged_in');
			$slang="english";
		    $this->lang->load($slang, $slang);
		    $data['lang']=$this->lang->language; 
			$data['title'] = "User Role";
			if($sty_id!=''){
				$style_details=$this->db->query("select * from tbl_style where sty_id='$sty_id'")->result_array();
				if(!empty($style_details)){
					$data['sty_id'] = $style_details[0]['sty_id'];
					$data['user_id'] = $style_details[0]['user_id'];
					$data['skuno'] = $style_details[0]['skuno'];
					$data['gender'] = $style_details[0]['gender'];
					$data['category'] = $style_details[0]['category'];
					$data['bg'] = $style_details[0]['bg'];
					$data['shadow'] = $style_details[0]['shadow'];
					$data['model_id'] = $style_details[0]['model_id'];
					$data['modbody'] = $style_details[0]['modbody'];
					$data['poseids'] = $style_details[0]['poseids'];
					$data['top'] = $style_details[0]['top'];
					$data['bottom'] = $style_details[0]['bottom'];
					$data['shoes'] = $style_details[0]['shoes'];
					$data['accessories'] = $style_details[0]['accessories'];
					$data['lipcol'] = $style_details[0]['lipstick'];
					$data['lipefft'] = $style_details[0]['lipstick_effrt'];

					$data['eyemkup']     = $style_details[0]['eyemkup'];
					$data['eyeshcol']    = $style_details[0]['eyeshadowcol'];
					$data['eyeint']      = $style_details[0]['eyeintensity'];
					$data['blshcol']     = $style_details[0]['blushcolcde'];
					$data['blshint']     = $style_details[0]['blushintensity'];

					
					$data['autostyle'] = $style_details[0]['autostyle'];




					
					
				}
			}else {
			        $data['sty_id'] = '';
					$data['user_id'] = '';
					$data['skuno'] = '';
					$data['gender'] = '';
					$data['category'] = '';
					$data['bg'] = '';
					$data['shadow'] = '';
					$data['model_id'] = '';
					$data['modbody'] = '';
					$data['poseids'] = '';
					$data['top'] = '';
					$data['bottom'] = '';
					$data['shoes'] = '';
					$data['accessories'] = '';
					$data['lipcol'] = '';
					$data['lipefft'] = '';
					$data['eyemkup']  = '';
					$data['eyeshcol'] = '';
					$data['eyeint'] = '';
					$data['blshcol'] = '';
					$data['blshint'] = '';
					$data['autostyle'] = '';
					
					
			}
			$this->load->view('admin/orderupload/order_upload_view', $data);
		} 
		else 
		{
			//If no session, redirect to login page
			redirect('login', 'refresh');
		}
	}



	public function updatestatus()
	{				//for email exist validation in edit customer form 


	$id=trim($this->input->get('id'));
	$status=trim($this->input->get('status'));
	
	$this->db->query("update tbl_booking set status ='$status' where order_id ='$id'");



	}

	
}
?>