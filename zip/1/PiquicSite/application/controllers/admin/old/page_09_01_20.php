<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Page extends CI_Controller
{
    public function __construct()
	{
		parent::__construct();		
		$this->load->library('pagination');
		$this->load->database();		
		$this->load->library('Upload');
		$this->load->helper('security');	
		$this->load->library('Form_validation');
		$this->load->helper(array('form', 'html', 'file'));
		$this->load->library('pagination');
		$this->load->library('email');
        $this->load->model('Page_model');
        $this->load->library('userlib');
		
    	if ($this->session->userdata('logged_in')) {
            $session_data = $this->session->userdata('logged_in');
            $data['username'] = $session_data['username'];
            $data['firstname'] = $session_data['firstname'];
            $data['rolecode'] = $session_data['rolecode'];
			/* $timezone = $session_data['timezone'];
			date_default_timezone_set($timezone); */
         
        } else {
            //If no session, redirect to login page
            redirect('login', 'refresh');
        }
    }
	
    public function index()
	{
		if ($this->session->userdata('logged_in'))		
		{  
			$session_data = $this->session->userdata('logged_in');
			$data['rolecode'] = $session_data['rolecode'];
			$data['usertype'] = $session_data['rolecode'];
			$data['roles'] = $session_data['roles'];
			$data['title'] = "Pages";
			 
			
			//echo "aaaaaaaaa";exit;
			$this->load->view('admin/page/page_list', $data);
		} 
		else 
		{
			//If no session, redirect to login page
			redirect('login', 'refresh');
		}
	}
	
	public function pagelist(){
		if ($this->session->userdata('logged_in')) 
        {
			$session_data=$this->session->userdata('logged_in');
			$usertype = $session_data['rolecode'];
			$userid = $session_data['id'];
			
    	    $aColumns = array( 'id','page_title','content','pimg');
         
			/* Indexed column (used for fast and accurate table cardinality) */
			$sIndexColumn = "id";
         
			/* DB table to use */
			$sTable = "tbl_page_content";

            /*
			 * Paging
			 */
			$sLimit = "";
			if ( isset( $_GET['start'] ) && $_GET['length'] != '-1' )
			{
				$sLimit = "LIMIT ".intval( $_GET['start'] ).", ".
					intval( $_GET['length'] );
			}


    
     
			$sOrder = "";
			if ( isset( $_GET['order'] ) )
			{
				$sOrder = "ORDER BY  ";
			
						$sOrder .= $aColumns[0]."
							".($_GET['order'][0]['dir']==='desc' ? 'asc' : 'desc') .", ";
			   
				 
				$sOrder = substr_replace( $sOrder, "", -2 );
				if ( $sOrder == "ORDER BY" )
				{
					$sOrder = "";
				}
			}
	

        
        $commn="1=1 and deleted='0' ";
        
		
		//echo $_REQUEST['columns'][1]['search']['value']; exit();
		
		if(!empty($_GET['columns'][1]['search']['value']) ){  
	
		$commn.=" AND tbl_page_content.page_title  LIKE '".$_GET['columns'][1]['search']['value']."%' ";
		}

        $sWhere="where $commn";
         
		
		if (isset($_GET['search']['value']) && $_GET['search']['value'] != "" )
		{
			 $sWhere = "WHERE $commn   AND (";
			for ( $i=0 ; $i<count($aColumns) ; $i++ )
			{
				
				/*if ( isset($_GET['columns'][$i]['searchable']) && $_GET['columns'][$i]['searchable'] == "true" )
				{*/
					$sWhere .= $aColumns[$i]." LIKE '%".( $_GET['search']['value'] )."%' OR ";
				/*}*/
			}
			$sWhere = substr_replace( $sWhere, "", -3 );
			$sWhere .= ')';
		}
		
		
       $join='';
        /*
         * SQL queries
         * Get data to display
         */
        $sQuery = "
            SELECT SQL_CALC_FOUND_ROWS ".str_replace(" , ", " ", implode(", ", $aColumns))."
            FROM   $sTable $join
            $sWhere
            $sOrder
            $sLimit";

       //echo  $sQuery; exit();

        $rResult = $this->db->query($sQuery);
        $rResult = $rResult->result_array();
        //$rResult = mysqli_query( $sQuery) or fatal_error( 'MySQL Error: ' . mysqli_errno() );

        
        /* Data set length after filtering */
        //$sQuery = "SELECT FOUND_ROWS()";
        //$rResultFilterTotal = mysqli_query( $sQuery) or fatal_error( 'MySQL Error: ' . mysqli_errno() );
        //$query = $this->db->query($sQuery);
        //$aResultFilterTotal = $query->result_array();
        //$aResultFilterTotal = mysqli_fetch_array($rResultFilterTotal);
       $iFilteredTotal = count( $rResult);
          
        /* Total data set length */
        $sQuery = "
            SELECT COUNT(".$sIndexColumn.") as count
            FROM   $sTable
        ";
        //$rResultTotal = mysqli_query( $sQuery) or fatal_error( 'MySQL Error: ' . mysqli_errno() );
        //$aResultTotal = mysqli_fetch_array($rResultTotal);

        $query = $this->db->query($sQuery);
        $aResultTotal = $query->result_array();

        //echo "<pre>";
        //print_r($aResultTotal);
         $iTotal = $aResultTotal[0]['count'];
         
        /*
         * Output
         */
        $output = array(
                        "sEcho" => '',
                        "iTotalRecords" => $iFilteredTotal,
                        "iTotalDisplayRecords" => $iTotal,
                        "aaData" => array()
                        );

            $sColumns = array('page_title','content','pimg','action');
         
            //while ( $aRow = mysqli_fetch_array( $rResult ) )
            //echo "<pre>";
           //print_r($output);
         
            foreach($rResult as $aRow)
            {
                $row = array();
            	
				$id=$aRow['id'];
				//$user_type=$aRow['usertype_id'];
				//$business_id=$aRow['business_id'];
				//$status=$aRow['user_status'];
                for ( $i=0 ; $i<count($sColumns) ; $i++ )
                {
                   /*  if ( $sColumns[$i] == "version" )
                    {
                       
                        $row[] = ($aRow[ $sColumns[$i] ]=="0") ? '-' : $aRow[ $sColumns[$i] ];
                    } */
                    if( $sColumns[$i] == "content" )
                    {
                    	 $content = substr($aRow[ $sColumns[$i] ],0,90);
                    	 $row[] = $content."....";
                    }
                    else if ( $sColumns[$i] == "pimg" )
                    {
                       
                        $row[] = '<img style="width: 50px;" src="'.base_url().'assets/images/pages-img/'.$aRow[ $sColumns[$i] ].'">';
                    } 
					
            	    else if($sColumns[$i] =='action')
            		{
            			if($usertype!=0){
							$roles=$session_data['roles'];
							$role=trim($roles,',');
							$role=explode(',',$role);
							if(!empty($role)){
								foreach($role as $result){
									$query=$this->db->query("select * from user_permissions where role_id='$result' and menu_id='3'")->result();
									if($query){
										foreach($query as $res){
											$new[]=$res->new_id;
											$edit[]=$res->edit_id;
											$delete[]=$res->delete_id;
											$view[]=$res->view_id;
										}
						            }
							    }
							}
							
							if(in_array('1',$edit)){
							$edit_id='<a href="javascript:void(0)"  class="btn btn-info edit" val="'.$id.'"><i class="fa fa-edit"></i></a>';
							}else {
								$edit_id='';
							}
							if(in_array('1',$delete)){
								$delete_id=' <a href="javascript:void(0)"  class="btn btn-danger delete" val="'.$id.'"><i class="fa fa-trash-o"></i></a>';
							}else {
								$delete_id='';
							}
							if(in_array('1',$view)){
								$view_id=' <a href="javascript:void(0)"  class="btn btn-success view" val="'.$id.'"><i class="fa fa-fw fa-eye"></i></a>';
							}else {
								$view_id='';
							}
							
							 $row[]=$view_id.'&nbsp;'.$edit_id.'&nbsp;'.$delete_id;
							
						}else {

						$row[]='<a href="javascript:void(0)"  class="btn btn-success view" val="'.$id.'"><i class="fa fa-fw fa-eye"></i></a>&nbsp;<a href="javascript:void(0)"  class="btn btn-info edit" val="'.$id.'"><i class="fa fa-edit"></i></a>&nbsp; <a href="javascript:void(0)"  class="btn btn-danger delete" val="'.$id.'"><i class="fa fa-trash-o"></i></a>
        			  ';
						}
            		}
            		else if ( $sColumns[$i] != ' ' )
                    {
                        /* General output */
                        $row[] = $aRow[ $sColumns[$i] ];
                    }
                }
                $output['aaData'][] = $row;
            }
            //print_r($output);exit;
        echo json_encode( $output );
    }
	}
	
	
	public function add($id=''){
		
		if ($this->session->userdata('logged_in'))		
		{  
			$session_data = $this->session->userdata('logged_in');
			$slang="english";
		    $this->lang->load($slang, $slang);
		    $data['lang']=$this->lang->language; 
			$data['title'] = "Page Content";
			if($id!=''){
				$page_details=$this->db->query("select * from tbl_page_content where id='$id'")->result_array();
				if(!empty($page_details)){
					$data['id'] = $page_details[0]['id'];
					$data['page_title'] = $page_details[0]['page_title'];
					$data['content'] = $page_details[0]['content'];					
					$data['pimg'] = $page_details[0]['pimg'];
					$data['updated_datetime'] = $page_details[0]['updated_datetime'];
					$data['deleted'] = $page_details[0]['deleted'];
				}
			}else {
			        $data['id'] = '';
					$data['page_title'] = '';
					$data['content'] = '';
					$data['pimg'] = '';
					$data['updated_datetime'] = '';
					$data['deleted'] = '';
					
			}
			$this->load->view('admin/page/page_management', $data);
		} 
		else 
		{
			//If no session, redirect to login page
			redirect('login', 'refresh');
		}
	}
	


	public function insert_page(){
		
		if ($this->session->userdata('logged_in')) {
			$session_data = $this->session->userdata('logged_in');
			$created_date = date('Y-m-d');
			$userid=$session_data['id'];
			$id=$this->input->post('id');
			//upload file
	        
	                
	          
	       
			if($id!='')
			{
				
				  $data = array(
				    'id'=>$this->input->post('id'),
					'page_title'=>$this->input->post('page_title'),
					'content'=>$this->input->post('content'),
					'pimg'=>$this->input->post('pimg'),
					'updated_datetime'=>$created_date,
					'deleted'=>0,
					);
					
				  $report = $this->Page_model->page_update($data,$id);
			      ///echo $report;
			      if($report==1)
			      {
			      	echo "Page content has been updated Successfully";
			      }
			      else{
			      	echo "Error";
			      }
				
			}else {
				
				
				
				   $data = array(
				    'id'=>$this->input->post('id'),
					
					'page_title'=>$this->input->post('page_title'),
					'content'=>$this->input->post('content'),
					'pimg'=>$this->input->post('pimg'),	
					'updated_datetime'=>$created_date,
					'deleted'=>0,				
					);
					$report =$this->Page_model->page_insert($data);
					$id=$this->db->insert_id();
					
					
					//echo $report;
					if($report==1)
			      	{
			      		echo "Page content has been Added Successfully";
			      	}
			      	else{
			      		echo "Error";
			      	}
				}
			
		}else {
		
            redirect('login', 'refresh');
       
		}  
	}
		 
	public function deletepage()
    {
		if ($this->session->userdata('logged_in')) 
		{
			$id = $this->input->post('id');
		
			//$this->db->query("delete from user where id='$id'");
			$this->db->query("update tbl_page_content set deleted ='1' where id ='$id'");
			$this->session->set_flashdata('added', '<div align="left" style="color:green; font-size:15px; border:1px #398AB9 solid;">
		    Page Content has been Deleted Successfully</div>');

			//redirect('users');
		} 
		else 
		{
			//If no session, redirect to login page
			redirect('login', 'refresh');
		}
	}
	

	
	public function view($id=''){
		
		if ($this->session->userdata('logged_in'))		
		{  
			$session_data = $this->session->userdata('logged_in');
			$slang="english";
		    $this->lang->load($slang, $slang);
		    $data['lang']=$this->lang->language; 
			$data['title'] = "Page Content";
			if($id!=''){
				$page_details=$this->db->query("select * from tbl_page_content where id='$id'")->result_array();
				if(!empty($page_details)){
					$data['id'] = $page_details[0]['id'];
					$data['page_title'] = $page_details[0]['page_title'];
					$data['content'] = $page_details[0]['content'];
					$data['pimg'] = $page_details[0]['pimg'];					
				}
			}else {
			        $data['id'] = '';
					$data['page_title'] = '';
					$data['content'] = '';
					$data['pimg'] = '';					
			}
			$this->load->view('admin/page/page_view', $data);
		} 
		else 
		{
			//If no session, redirect to login page
			redirect('login', 'refresh');
		}
	}



	public function upload_file(){
		
		// $config['upload_path'] = './assets/img/pages-img';
	 //        $config['allowed_types'] = 'gif|jpg|png|jpeg';
	 //       // $config['encrypt_name'] = TRUE;
	 //        $config['max_size'] = '2048';
		// 	$config['max_width'] = '2000';
		// 	$config['max_height'] = '2000';

	       
	 //                    $this->load->library('upload', $config);

	 //                    if ($this->upload->do_upload('pimg')) {
	 //                    	$data =  array('upload_data' => $this->upload->data());
	 //                        $post_image = $_FILES['pimg']['name'];
	 //                    } else {
	                    	
	 //                         $error= $this->upload->display_errors();
	 //                       print_r($error);
	 //                       exit();
	 //                       $post_image = 'noimage.png';
	 //                    }
		if(!empty($_REQUEST['folder_name']))
		{
			$dir = $_REQUEST['folder_name']."/";
		}
		else
		{
			$dir = "./assets/images/pages-img/";
		}
		$file=time().$_FILES["image"]["name"];
		move_uploaded_file($_FILES["image"]["tmp_name"], $dir. $file);

		echo ltrim($file);
	}

	
}
?>