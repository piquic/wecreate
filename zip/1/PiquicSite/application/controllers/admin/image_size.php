<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Image_size extends CI_Controller
{
    public function __construct()
	{
		parent::__construct();		
		$this->load->library('pagination');
		$this->load->database();		
		$this->load->library('Upload');
		$this->load->helper('security');	
		$this->load->library('Form_validation');
		$this->load->helper(array('form', 'html', 'file'));
		$this->load->library('pagination');
		$this->load->library('email');
        $this->load->model('Image_size_model');
        $this->load->library('userlib');
		
    	if ($this->session->userdata('logged_in')) {
            $session_data = $this->session->userdata('logged_in');
            $data['username'] = $session_data['username'];
            $data['firstname'] = $session_data['firstname'];
            $data['rolecode'] = $session_data['rolecode'];
			/* $timezone = $session_data['timezone'];
			date_default_timezone_set($timezone); */
         
        } else {
            //If no session, redirect to login page
            redirect('login', 'refresh');
        }
    }
	
    public function index()
	{
		if ($this->session->userdata('logged_in'))		
		{  
			$session_data = $this->session->userdata('logged_in');
			$data['rolecode'] = $session_data['rolecode'];
			$data['usertype'] = $session_data['rolecode'];
			$data['roles'] = $session_data['roles'];
			$data['title'] = "Image Size";
			 
			
			//echo "aaaaaaaaa";exit;
			$this->load->view('admin/image_size/image_size_list', $data);
		} 
		else 
		{
			//If no session, redirect to login page
			redirect('login', 'refresh');
		}
	}
	
	public function image_size_list(){
		if ($this->session->userdata('logged_in')) 
        {
			$session_data=$this->session->userdata('logged_in');
			$usertype = $session_data['rolecode'];
			$userid = $session_data['id'];
			
    	    $aColumns = array( 'id','image_size','status');
         
			/* Indexed column (used for fast and accurate table cardinality) */
			$sIndexColumn = "id";
         
			/* DB table to use */
			$sTable = "tbl_image_size";

            /*
			 * Paging
			 */
			$sLimit = "";
			if ( isset( $_GET['start'] ) && $_GET['length'] != '-1' )
			{
				$sLimit = "LIMIT ".intval( $_GET['start'] ).", ".
					intval( $_GET['length'] );
			}


    
     
			$sOrder = "";
			if ( isset( $_GET['order'] ) )
			{
				$sOrder = "ORDER BY  ";
			
						$sOrder .= $aColumns[0]."
							".($_GET['order'][0]['dir']==='desc' ? 'asc' : 'desc') .", ";
			   
				 
				$sOrder = substr_replace( $sOrder, "", -2 );
				if ( $sOrder == "ORDER BY" )
				{
					$sOrder = "";
				}
			}
	

        
        $commn="1=1 and deleted='0' ";
        
		
		//echo $_REQUEST['columns'][1]['search']['value']; exit();
		
		if(!empty($_GET['columns'][1]['search']['value']) ){  
	
		$commn.=" AND tbl_image_size.image_size  LIKE '".$_GET['columns'][1]['search']['value']."%' ";
		}

        $sWhere="where $commn";
         
		
		if (isset($_GET['search']['value']) && $_GET['search']['value'] != "" )
		{
			 $sWhere = "WHERE $commn   AND (";
			for ( $i=0 ; $i<count($aColumns) ; $i++ )
			{
				
				/*if ( isset($_GET['columns'][$i]['searchable']) && $_GET['columns'][$i]['searchable'] == "true" )
				{*/
					$sWhere .= $aColumns[$i]." LIKE '%".( $_GET['search']['value'] )."%' OR ";
				/*}*/
			}
			$sWhere = substr_replace( $sWhere, "", -3 );
			$sWhere .= ')';
		}
		
		
       $join='';
        /*
         * SQL queries
         * Get data to display
         */
        $sQuery = "
            SELECT SQL_CALC_FOUND_ROWS ".str_replace(" , ", " ", implode(", ", $aColumns))."
            FROM   $sTable $join
            $sWhere
            $sOrder
            $sLimit";

       //echo  $sQuery; exit();

        $rResult = $this->db->query($sQuery);
        $rResult = $rResult->result_array();
        //$rResult = mysqli_query( $sQuery) or fatal_error( 'MySQL Error: ' . mysqli_errno() );

        
        /* Data set length after filtering */
        //$sQuery = "SELECT FOUND_ROWS()";
        //$rResultFilterTotal = mysqli_query( $sQuery) or fatal_error( 'MySQL Error: ' . mysqli_errno() );
        //$query = $this->db->query($sQuery);
        //$aResultFilterTotal = $query->result_array();
        //$aResultFilterTotal = mysqli_fetch_array($rResultFilterTotal);
       $iFilteredTotal = count( $rResult);
          
        /* Total data set length */
        $sQuery = "
            SELECT COUNT(".$sIndexColumn.") as count
            FROM   $sTable $join
            $sWhere
        ";
        //$rResultTotal = mysqli_query( $sQuery) or fatal_error( 'MySQL Error: ' . mysqli_errno() );
        //$aResultTotal = mysqli_fetch_array($rResultTotal);

        $query = $this->db->query($sQuery);
        $aResultTotal = $query->result_array();

        //echo "<pre>";
        //print_r($aResultTotal);
         $iTotal = $aResultTotal[0]['count'];
         
        /*
         * Output
         */
        $output = array(
                        "sEcho" => '',
                        "iTotalRecords" => $iFilteredTotal,
                        "iTotalDisplayRecords" => $iTotal,
                        "aaData" => array()
                        );

            $sColumns = array('image_size','status','action');
         
            //while ( $aRow = mysqli_fetch_array( $rResult ) )
            //echo "<pre>";
           //print_r($output);
         	
            foreach($rResult as $aRow)
            {
                $row = array();
            	$status=$aRow['status'];
				$id=$aRow['id'];
				for ( $i=0 ; $i<count($sColumns) ; $i++ )
                {
                	if($sColumns[$i] =='status')
            		{
            			
            			 $Enable_status="";
			             $Disable_status="";
						 
						 
					    if($status=='0')
						{
						 $Enable_status="selected";
			             $Disable_status="";
						
						}
						else if($status=='1')
						{
						 $Enable_status="";
			             $Disable_status="selected";
						 
						}
						
            			
            			 $row[]='<select name="status" id="status" class="selectpicker update_status">
								 <option value="0@@@'.$id.'"  '.$Enable_status.'>Enable</option>
								 <option value="1@@@'.$id.'"  '.$Disable_status.'>Disable</option>
								 </select>';
            			 
            		}
                   	else if($sColumns[$i] =='action')
            		{
            			if($usertype!=0){
							$roles=$session_data['roles'];
							$role=trim($roles,',');
							$role=explode(',',$role);
							if(!empty($role)){
								foreach($role as $result){
									$query=$this->db->query("select * from user_permissions where role_id='$result' and menu_id='3'")->result();
									if($query){
										foreach($query as $res){
											$new[]=$res->new_id;
											$edit[]=$res->edit_id;
											$delete[]=$res->delete_id;
											$view[]=$res->view_id;
										}
						            }
							    }
							}
							
							if(in_array('1',$edit)){
							$edit_id='<a href="javascript:void(0)"  class="btn btn-info edit" val="'.$id.'"><i class="fas fa-edit"></i></a>';
							}else {
								$edit_id='';
							}
							if(in_array('1',$delete)){
								$delete_id=' <a href="javascript:void(0)"  class="btn btn-danger delete" val="'.$id.'"><i class="fas fa-trash-alt"></i></a>';
							}else {
								$delete_id='';
							}
							if(in_array('1',$view)){
								$view_id=' <a href="javascript:void(0)"  class="btn btn-success view" val="'.$id.'"><i class="fas fa-eye"></i></a>';
							}else {
								$view_id='';
							}
							
							 $row[]=$view_id.'&nbsp;'.$edit_id.'&nbsp;'.$delete_id;
							
						}else {

						$row[]='<a href="javascript:void(0)"  class="btn btn-success view" val="'.$id.'"><i class="fas fa-eye"></i></a>&nbsp;<a href="javascript:void(0)"  class="btn btn-info edit" val="'.$id.'"><i class="fas fa-edit"></i></a>&nbsp; <a href="javascript:void(0)"  class="btn btn-danger delete" val="'.$id.'"><i class="fas fa-trash-alt"></i></a>
        			  ';
						}
            		}
            		else if ( $sColumns[$i] != ' ' )
                    {
                        /* General output */
                        $row[] = $aRow[ $sColumns[$i] ];
                    }
                }
                $output['aaData'][] = $row;
            }
            //print_r($output);exit;
        echo json_encode( $output );
    }
	}
	
	
	public function add($id=''){
		
		if ($this->session->userdata('logged_in'))		
		{  
			$session_data = $this->session->userdata('logged_in');
			$slang="english";
		    $this->lang->load($slang, $slang);
		    $data['lang']=$this->lang->language; 
			$data['title'] = "Image Size";
			if($id!=''){
				$image_size_details=$this->db->query("select * from tbl_image_size where id='$id'")->result_array();
				if(!empty($image_size_details)){
					$data['id'] = $image_size_details[0]['id'];
					$data['image_size'] = $image_size_details[0]['image_size'];				
					$data['updated_datetime'] = $image_size_details[0]['updated_datetime'];
					$data['status'] = $image_size_details[0]['status'];
					$data['deleted'] = $image_size_details[0]['deleted'];
				}
			}else {
			        $data['id'] = '';
					$data['image_size'] = '';
					$data['updated_datetime'] = '';
					$data['status'] = '';
					$data['deleted'] = '';
					
			}
			$this->load->view('admin/image_size/image_size_management', $data);
		} 
		else 
		{
			//If no session, redirect to login page
			redirect('login', 'refresh');
		}
	}
	


	public function insert_image_size(){
		
		if ($this->session->userdata('logged_in')) {
			$session_data = $this->session->userdata('logged_in');
			$created_date = date('Y-m-d');
			$userid=$session_data['id'];
			$id=$this->input->post('id');
			//upload file
	       
			if($id!='')
			{
				
				  $data = array(
				    'id'=>$this->input->post('id'),
					'image_size'=>$this->input->post('image_size'),
					'updated_datetime'=>$created_date,
					'status'=>$this->input->post('status'),
					'deleted'=>"0",
					);
					
				  $report = $this->Image_size_model->image_size_update($data,$id);
			      ///echo $report;
			      if($report==1)
			      {
			      	echo "Image size content has been updated Successfully";
			      }
			      else{
			      	echo "Error";
			      }
				
			}else {
				
				
				
				   $data = array(
				    'id'=>$this->input->post('id'),					
					'image_size'=>$this->input->post('image_size'),
					'updated_datetime'=>$created_date,
					'status'=>$this->input->post('status'),
					'deleted'=>"0",				
					);
					$report =$this->Image_size_model->image_size_insert($data);
					$id=$this->db->insert_id();
					
					
					//echo $report;
					if($report==1)
			      	{
			      		echo "Image size content has been Added Successfully";
			      	}
			      	else{
			      		echo "Error";
			      	}
				}
			
		}else {
		
            redirect('login', 'refresh');
       
		}  
	}
		 
	public function deleteimage_size()
    {
		if ($this->session->userdata('logged_in')) 
		{
			$id = $this->input->post('id');
		
			//$this->db->query("delete from user where id='$id'");
			$this->db->query("update tbl_image_size set deleted ='1' where id ='$id'");
			$this->session->set_flashdata('added', '<div align="left" style="color:green; font-size:15px; border:1px #398AB9 solid;">
		    Image size Content has been Deleted Successfully</div>');

			//redirect('users');
		} 
		else 
		{
			//If no session, redirect to login page
			redirect('login', 'refresh');
		}
	}
	

	
	public function view($id=''){
		
		if ($this->session->userdata('logged_in'))		
		{  
			$session_data = $this->session->userdata('logged_in');
			$slang="english";
		    $this->lang->load($slang, $slang);
		    $data['lang']=$this->lang->language; 
			$data['title'] = "Image Size";
			if($id!=''){
				$image_size_details=$this->db->query("select * from tbl_image_size where id='$id'")->result_array();
				if(!empty($image_size_details)){
					$data['id'] = $image_size_details[0]['id'];
					$data['image_size'] = $image_size_details[0]['image_size'];
					$data['status'] = $image_size_details[0]['status'];
									
				}
			}else {
			        $data['id'] = '';
					$data['image_size'] = '';
					$data['status'] = '';
								
			}
			$this->load->view('admin/image_size/image_size_view', $data);
		} 
		else 
		{
			//If no session, redirect to login page
			redirect('login', 'refresh');
		}
	}

	public function updatestatus()
	{				//for email exist validation in edit customer form 


	$id=trim($this->input->get('id'));
	$status=trim($this->input->get('status'));
	
	$this->db->query("update tbl_image_size set status ='$status' where id ='$id'");



	}
	
	
}
?>