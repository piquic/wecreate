<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Plantype extends CI_Controller
{
    public function __construct()
	{
		parent::__construct();		
		$this->load->library('pagination');
		$this->load->database();		
		$this->load->library('Upload');
		$this->load->helper('security');	
		$this->load->library('Form_validation');
		$this->load->helper(array('form', 'html', 'file'));
		$this->load->library('pagination');
		$this->load->library('email');
        $this->load->model('Plantype_model');
        $this->load->library('userlib');
		
    	if ($this->session->userdata('logged_in')) {
            $session_data = $this->session->userdata('logged_in');
            $data['username'] = $session_data['username'];
            $data['firstname'] = $session_data['firstname'];
            $data['rolecode'] = $session_data['rolecode'];
			/* $timezone = $session_data['timezone'];
			date_default_timezone_set($timezone); */
         
        } else {
            //If no session, redirect to login page
            redirect('login', 'refresh');
        }
    }
	
    public function index()
	{
		if ($this->session->userdata('logged_in'))		
		{  
			$session_data = $this->session->userdata('logged_in');
			$data['rolecode'] = $session_data['rolecode'];
			$data['usertype'] = $session_data['rolecode'];
			$data['roles'] = $session_data['roles'];
			$data['title'] = "Users";
			$slang="english";
			$this->lang->load($slang, $slang);
			$data['lang']=$this->lang->language; 
			
			//echo "aaaaaaaaa";exit;
			$this->load->view('admin/plantype/plantype_list', $data);
		} 
		else 
		{
			//If no session, redirect to login page
			redirect('login', 'refresh');
		}
	}
	
	public function plantypelist(){
		if ($this->session->userdata('logged_in')) 
        {
			$session_data=$this->session->userdata('logged_in');
			$usertype = $session_data['rolecode'];
			$userid = $session_data['id'];
			$slang="english";
		    $this->lang->load($slang, $slang);
		    $data['lang']=$this->lang->language; 
    	    $aColumns = array( 'plntyid','plntypname','status');
         
			/* Indexed column (used for fast and accurate table cardinality) */
			$sIndexColumn = "plntyid";
         
			/* DB table to use */
			$sTable = "tbl_plantype";

            /*
			 * Paging
			 */
			$sLimit = "";
			if ( isset( $_GET['start'] ) && $_GET['length'] != '-1' )
			{
				$sLimit = "LIMIT ".intval( $_GET['start'] ).", ".
					intval( $_GET['length'] );
			}


    
     
			$sOrder = "";
			if ( isset( $_GET['order'] ) )
			{
				$sOrder = "ORDER BY  ";
			
						$sOrder .= $aColumns[0]."
							".($_GET['order'][0]['dir']==='desc' ? 'asc' : 'desc') .", ";
			   
				 
				$sOrder = substr_replace( $sOrder, "", -2 );
				if ( $sOrder == "ORDER BY" )
				{
					$sOrder = "";
				}
			}
	

        
        $commn="1=1 and deleted='0' ";
        
		
		//echo $_REQUEST['columns'][1]['search']['value']; exit();
		
		if(!empty($_GET['columns'][1]['search']['value']) ){  
	
		$commn.=" AND tbl_plantype.plntyid  LIKE '".$_GET['columns'][1]['search']['value']."%' ";
		}

        $sWhere="where $commn";
         
		
		if (isset($_GET['search']['value']) && $_GET['search']['value'] != "" )
		{
			 $sWhere = "WHERE $commn   AND (";
			for ( $i=0 ; $i<count($aColumns) ; $i++ )
			{
				
				/*if ( isset($_GET['columns'][$i]['searchable']) && $_GET['columns'][$i]['searchable'] == "true" )
				{*/
					$sWhere .= $aColumns[$i]." LIKE '%".( $_GET['search']['value'] )."%' OR ";
				/*}*/
			}
			$sWhere = substr_replace( $sWhere, "", -3 );
			$sWhere .= ')';
		}
		
		
       $join='';
        /*
         * SQL queries
         * Get data to display
         */
        $sQuery = "
            SELECT SQL_CALC_FOUND_ROWS ".str_replace(" , ", " ", implode(", ", $aColumns))."
            FROM   $sTable $join
            $sWhere
            $sOrder
            $sLimit";

       //echo  $sQuery; exit();

        $rResult = $this->db->query($sQuery);
        $rResult = $rResult->result_array();
        //$rResult = mysqli_query( $sQuery) or fatal_error( 'MySQL Error: ' . mysqli_errno() );

        
        /* Data set length after filtering */
        //$sQuery = "SELECT FOUND_ROWS()";
        //$rResultFilterTotal = mysqli_query( $sQuery) or fatal_error( 'MySQL Error: ' . mysqli_errno() );
        //$query = $this->db->query($sQuery);
        //$aResultFilterTotal = $query->result_array();
        //$aResultFilterTotal = mysqli_fetch_array($rResultFilterTotal);
       $iFilteredTotal = count( $rResult);
          
        /* Total data set length */
        $sQuery = "
            SELECT COUNT(".$sIndexColumn.") as count
            FROM   $sTable $join
            $sWhere
        ";
        //$rResultTotal = mysqli_query( $sQuery) or fatal_error( 'MySQL Error: ' . mysqli_errno() );
        //$aResultTotal = mysqli_fetch_array($rResultTotal);

        $query = $this->db->query($sQuery);
        $aResultTotal = $query->result_array();

        //echo "<pre>";
        //print_r($aResultTotal);
         $iTotal = $aResultTotal[0]['count'];
         
        /*
         * Output
         */
        $output = array(
                        "sEcho" => '',
                        "iTotalRecords" => $iFilteredTotal,
                        "iTotalDisplayRecords" => $iTotal,
                        "aaData" => array()
                        );

            $sColumns = array('plntypname','status','action');
         
            //while ( $aRow = mysqli_fetch_array( $rResult ) )
            //echo "<pre>";
           //print_r($output);
         
            foreach($rResult as $aRow)
            {
                $row = array();
            	
				$plntyid=$aRow['plntyid'];
				$status=$aRow['status'];
                for ( $i=0 ; $i<count($sColumns) ; $i++ )
                {
                  
            	    if($sColumns[$i] =='action')
            		{
            			

						$row[]='<a href="javascript:void(0)"  class="btn btn-success view" val="'.$plntyid.'"><i class="fas fa-eye"></i></a>&nbsp;<a href="javascript:void(0)"  class="btn btn-info edit" val="'.$plntyid.'"><i class="fas fa-edit"></i></a>&nbsp; <a href="javascript:void(0)"  class="btn btn-danger delete" val="'.$plntyid.'"><i class="fas fa-trash-alt"></i></a>
        			  ';
						
            		}
					
				
					else if($sColumns[$i] =='status')
            		{
            			
            			 $Enable_status="";
			             $Disable_status="";
						 
						 
					    if($status=='0')
						{
						 $Enable_status="selected";
			             $Disable_status="";
						
						}
						else if($status=='1')
						{
						 $Enable_status="";
			             $Disable_status="selected";
						 
						}
						
            			
            			 $row[]='<select name="status" id="status" class="selectpicker update_status">
								 <option value="0@@@'.$plntyid.'"  '.$Enable_status.'>Enable</option>
								 <option value="1@@@'.$plntyid.'"  '.$Disable_status.'>Disable</option>
								 </select>';
            			 
            		}
                    else if ( $sColumns[$i] != ' ' )
                    {
                        /* General output */
                        $row[] = $aRow[ $sColumns[$i] ];
                    }
                }
                $output['aaData'][] = $row;
            }
            //print_r($output);exit;
        echo json_encode( $output );
    }
	}
	
	
	public function add($plntyid=''){
		
		if ($this->session->userdata('logged_in'))		
		{  
			$session_data = $this->session->userdata('logged_in');
			$slang="english";
		    $this->lang->load($slang, $slang);
		    $data['lang']=$this->lang->language; 
			$data['title'] = "User Role";
			if($plntyid!=''){
				$plantype_details=$this->db->query("select * from tbl_plantype where plntyid='$plntyid'")->result_array();
				if(!empty($plantype_details)){
					$data['plntyid'] = $plantype_details[0]['plntyid'];
					$data['plntypname'] = $plantype_details[0]['plntypname'];
					$data['status'] = $plantype_details[0]['status'];
					$data['deleted'] = $plantype_details[0]['deleted'];
					
					
				}
			}else {
			        $data['plntyid'] = '';
					$data['plntypname'] = '';
					$data['status'] = '';
					$data['deleted'] = '';
					
					
			}
			$this->load->view('admin/plantype/plantype_management', $data);
		} 
		else 
		{
			//If no session, redirect to login page
			redirect('login', 'refresh');
		}
	}
	
	

	
	public function insert_plantype(){
		
		if ($this->session->userdata('logged_in')) {
			$session_data = $this->session->userdata('logged_in');
			$created_date = date('Y-m-d');
			$userid=$session_data['id'];
			$plntyid=$this->input->post('plntyid');
			
			if($plntyid!='')
			{
				
				  $data = array(
				    'plntyid'=>$this->input->post('plntyid'),
					'plntypname'=>$this->input->post('plntypname'),
					'status'=>$this->input->post('status'),
					);
					
				  $updateuserrole = $this->Plantype_model->plantype_update($data,$plntyid);
			      echo $plntyid;
				
			}else {
				
				
				
				   $data = array(
				    'plntyid'=>$this->input->post('plntyid'),
					'plntypname'=>$this->input->post('plntypname'),
					'status'=>$this->input->post('status'),
					
					);
					$this->Plantype_model->plantype_insert($data);
					$plntyid=$this->db->insert_id();
					
					
					echo $plntyid;
				}
			
		}else {
		
            redirect('login', 'refresh');
       
		}  
	}
		
		
		
		

 
	public function deleteplantype()
    {
		if ($this->session->userdata('logged_in')) 
		{
			$plntyid = $this->input->post('plntyid');
		
			//$this->db->query("delete from user where user_id='$user_id'");
			$this->db->query("update tbl_plantype set deleted ='1',status='1' where plntyid ='$plntyid'");
			$this->session->set_flashdata('added', '<div align="left" style="color:green; font-size:15px; border:1px #398AB9 solid;">
		    Service Deleted Successfully</div>');

			//redirect('users');
		} 
		else 
		{
			//If no session, redirect to login page
			redirect('login', 'refresh');
		}
	}
	
	

	public function view($plntyid=''){
		
		if ($this->session->userdata('logged_in'))		
		{  
			$session_data = $this->session->userdata('logged_in');
			$slang="english";
		    $this->lang->load($slang, $slang);
		    $data['lang']=$this->lang->language; 
			$data['title'] = "User Role";
			if($plntyid!=''){
				$plantype_details=$this->db->query("select * from tbl_plantype where plntyid='$plntyid'")->result_array();
				if(!empty($plantype_details)){
					$data['plntyid'] = $plantype_details[0]['plntyid'];
					$data['plntypname'] = $plantype_details[0]['plntypname'];
					$data['status'] = $plantype_details[0]['status'];
					$data['deleted'] = $plantype_details[0]['deleted'];
					
					
				}
			}else {
			        $data['plntyid'] = '';
					$data['plntypname'] = '';
					$data['status'] = '';
					$data['deleted'] = '';
					
					
			}
			$this->load->view('admin/plantype/plantype_view', $data);
		} 
		else 
		{
			//If no session, redirect to login page
			redirect('login', 'refresh');
		}
	}



	public function updatestatus()
	{				//for email exist validation in edit customer form 


	$id=trim($this->input->get('id'));
	$status=trim($this->input->get('status'));
	
	$this->db->query("update tbl_plantype set status ='$status' where plntyid ='$id'");



	}

	
}
?>