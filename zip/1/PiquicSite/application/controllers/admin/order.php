<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Order extends CI_Controller
{
    public function __construct()
	{
		parent::__construct();		
		$this->load->library('pagination');
		$this->load->database();		
		$this->load->library('Upload');
		$this->load->helper('security');	
		$this->load->library('Form_validation');
		$this->load->helper(array('form', 'html', 'file'));
		$this->load->library('pagination');
		$this->load->library('email');
        $this->load->model('orders_model');
        $this->load->model('plans_model');
        $this->load->library('userlib');
		
    	if ($this->session->userdata('logged_in')) {
            $session_data = $this->session->userdata('logged_in');
            $data['username'] = $session_data['username'];
            $data['firstname'] = $session_data['firstname'];
            $data['rolecode'] = $session_data['rolecode'];
			/* $timezone = $session_data['timezone'];
			date_default_timezone_set($timezone); */
         
        } else {
            //If no session, redirect to login page
            redirect('login', 'refresh');
        }
    }
	
    public function index()
	{
		if ($this->session->userdata('logged_in'))		
		{  
			$session_data = $this->session->userdata('logged_in');
			$data['rolecode'] = $session_data['rolecode'];
			$data['usertype'] = $session_data['rolecode'];
			$data['roles'] = $session_data['roles'];
			$data['title'] = "Users";
			$slang="english";
			$this->lang->load($slang, $slang);
			$data['lang']=$this->lang->language; 
			
			//echo "aaaaaaaaa";exit;
			$this->load->view('admin/order/order_list', $data);
		} 
		else 
		{
			//If no session, redirect to login page
			redirect('login', 'refresh');
		}
	}
	
	
	public function orderlist(){
		if ($this->session->userdata('logged_in')) 
        {
			$session_data=$this->session->userdata('logged_in');
			$usertype = $session_data['rolecode'];
			$userid = $session_data['id'];
			$slang="english";
		    $this->lang->load($slang, $slang);
		    $data['lang']=$this->lang->language; 

           $gender_select="(CASE tbl_booking.gender
				WHEN 1 THEN 'Male'
				WHEN 2 THEN 'Female'
				WHEN 3 THEN 'Boys (0-2 years)'
				WHEN 4 THEN 'Boys (2-9 years)'
				WHEN 5 THEN 'Girls (0-2 years)'
				WHEN 6 THEN 'Girl (2-9 years)'
				ELSE 'None'
				END)";

    	    $aColumns = array( 'book_id', 'tbl_booking.user_id','order_id',''.$gender_select.' AS gender','total_image','total_price','p_status','tbl_users.user_name');
         
			/* Indexed column (used for fast and accurate table cardinality) */
			$sIndexColumn = "book_id";
         
			/* DB table to use */
			$sTable = "tbl_booking";

            /*
			 * Paging
			 */
			$sLimit = "";
			if ( isset( $_GET['start'] ) && $_GET['length'] != '-1' )
			{
				$sLimit = "LIMIT ".intval( $_GET['start'] ).", ".
					intval( $_GET['length'] );
			}


    
     
			$sOrder = "";
			if ( isset( $_GET['order'] ) )
			{
				$sOrder = "ORDER BY  ";
			
						$sOrder .= $aColumns[0]."
							".($_GET['order'][0]['dir']==='desc' ? 'asc' : 'desc') .", ";
			   
				 
				$sOrder = substr_replace( $sOrder, "", -2 );
				if ( $sOrder == "ORDER BY" )
				{
					$sOrder = "";
				}
			}
	

        
        $commn="1=1 ";
        
		
		//echo $_REQUEST['columns'][1]['search']['value']; exit();
		
		if(!empty($_GET['columns'][1]['search']['value']) ){  
	
		$commn.=" AND tbl_booking.order_name  LIKE '".$_GET['columns'][1]['search']['value']."%' ";
		}

        $sWhere="where $commn";
         
		
		if (isset($_GET['search']['value']) && $_GET['search']['value'] != "" )
		{
			 $sWhere = "WHERE $commn   AND (";
			for ( $i=0 ; $i<count($aColumns) ; $i++ )
			{
				
				/*if ( isset($_GET['columns'][$i]['searchable']) && $_GET['columns'][$i]['searchable'] == "true" )
				{*/
					//$sWhere .= $aColumns[$i]." LIKE '%".( $_GET['search']['value'] )."%' OR ";
				/*}*/

				if($i=='3')
				{
					$sWhere .= "(CASE tbl_booking.gender
								WHEN 1 THEN 'Male'
								WHEN 2 THEN 'Female'
								WHEN 3 THEN 'Boys (0-2 years)'
								WHEN 4 THEN 'Boys (2-9 years)'
								WHEN 5 THEN 'Girls (0-2 years)'
								WHEN 6 THEN 'Girl (2-9 years)'
								ELSE 'None'
								END) LIKE '%".( $_GET['search']['value'] )."%' OR ";
				}
				else
				{
					$sWhere .= $aColumns[$i]." LIKE '%".( $_GET['search']['value'] )."%' OR ";
				}



			}
			$sWhere = substr_replace( $sWhere, "", -3 );
			$sWhere .= ')';
		}
		
		
       $join=' INNER JOIN tbl_users ON tbl_users.user_id=tbl_booking.user_id';
        /*
         * SQL queries
         * Get data to display
         */
        $sQuery = "
            SELECT SQL_CALC_FOUND_ROWS ".str_replace(" , ", " ", implode(", ", $aColumns))."
            FROM   $sTable $join
            $sWhere
            $sOrder
            $sLimit";

      // echo  $sQuery; exit();

        $rResult = $this->db->query($sQuery);
        $rResult = $rResult->result_array();
        //$rResult = mysqli_query( $sQuery) or fatal_error( 'MySQL Error: ' . mysqli_errno() );

        
        /* Data set length after filtering */
        //$sQuery = "SELECT FOUND_ROWS()";
        //$rResultFilterTotal = mysqli_query( $sQuery) or fatal_error( 'MySQL Error: ' . mysqli_errno() );
        //$query = $this->db->query($sQuery);
        //$aResultFilterTotal = $query->result_array();
        //$aResultFilterTotal = mysqli_fetch_array($rResultFilterTotal);
       $iFilteredTotal = count( $rResult);
          
        /* Total data set length */
        $sQuery = "
            SELECT COUNT(".$sIndexColumn.") as count
            FROM   $sTable $join
            $sWhere
        ";
        //$rResultTotal = mysqli_query( $sQuery) or fatal_error( 'MySQL Error: ' . mysqli_errno() );
        //$aResultTotal = mysqli_fetch_array($rResultTotal);

        $query = $this->db->query($sQuery);
        $aResultTotal = $query->result_array();

        //echo "<pre>";
        //print_r($aResultTotal);
         $iTotal = $aResultTotal[0]['count'];
         
        /*
         * Output
         */
        $output = array(
                        "sEcho" => '',
                        "iTotalRecords" => $iFilteredTotal,
                        "iTotalDisplayRecords" => $iTotal,
                        "aaData" => array()
                        );

            $sColumns = array( 'book_id', 'user_name','order_id','gender','total_image','total_price','p_status','action');
         
            //while ( $aRow = mysqli_fetch_array( $rResult ) )
            //echo "<pre>";
           //print_r($output);
         
            foreach($rResult as $aRow)
            {
                $row = array();
            	$book_id=$aRow['book_id'];
				$order_id=$aRow['order_id'];
				$user_id=$aRow['user_id'];
				$gender=$aRow['gender'];
				
				
			//	$plan_type=$aRow['plan_type'];
				
                for ( $i=0 ; $i<count($sColumns) ; $i++ )
                {
                  
            	    if($sColumns[$i] =='action')
            		{
            			
            				$row[]='<a href="javascript:void(0)"  class="btn btn-success view" val="'.$book_id.'"><i class="fas fa-eye"></i></a>
        			  ';
						
						// $row[]='<a href="javascript:void(0)"  class="btn btn-success view" val="'.$order_id.'"><i class="fas fa-eye"></i></a>&nbsp;<a href="javascript:void(0)"  class="btn btn-info edit" val="'.$order_id.'"><i class="fas fa-edit"></i></a>&nbsp; <a href="javascript:void(0)"  class="btn btn-danger delete" val="'.$order_id.'"><i class="fas fa-trash-alt"></i></a>
      //   			  ';
						
            		}
            		/*else if($sColumns[$i]=='gender')
            		{

                        if($gender=="1"){ $row[]= "Male" ; } 
                        if($gender=="2"){ $row[]= "Female" ; } 
                        if($gender=="4"){ $row[]= "Boy" ; } 
                        if($gender=="6"){ $row[]= "Girl" ; } 
                                                
            		}*/
            		
            		/*else if($sColumns[$i] =='user_id')
            		{


            			$ordertype_details=$this->db->query("select * from tbl_users where user_id='$user_id'")->result_array();
						if(!empty($ordertype_details)){
						 $row[]=$ordertype_details[0]['user_name'];
						}
						else
						{
							$row[]='';
						}
            		}*/
					
				
					// else if($sColumns[$i] =='status')
     //        		{
            			
     //        			 $Enable_status="";
			  //            $Disable_status="";
						 
						 
					//     if($status=='0')
					// 	{
					// 	 $Enable_status="selected";
			  //            $Disable_status="";
						
					// 	}
					// 	else if($status=='1')
					// 	{
					// 	 $Enable_status="";
			  //            $Disable_status="selected";
						 
					// 	}
						
            			
     //        			 $row[]='<select name="status" id="status" class="selectpicker update_status">
					// 			 <option value="0@@@'.$order_id.'"  '.$Enable_status.'>Enable</option>
					// 			 <option value="1@@@'.$order_id.'"  '.$Disable_status.'>Disable</option>
					// 			 </select>';
            			 
     //        		}
                    else if ( $sColumns[$i] != ' ' )
                    {
                        /* General output */
                        $row[] = $aRow[ $sColumns[$i] ];
                    }
                }
                $output['aaData'][] = $row;
            }
            //print_r($output);exit;
        echo json_encode( $output );
    }
	}
	
	public function add($order_id=''){
		
		if ($this->session->userdata('logged_in'))		
		{  
			$session_data = $this->session->userdata('logged_in');
			$slang="english";
		    $this->lang->load($slang, $slang);
		    $data['lang']=$this->lang->language; 
			$data['title'] = "User Role";
			if($order_id!=''){
				$order_details=$this->db->query("select * from tbl_booking where order_id='$order_id'")->result_array();

				if(!empty($order_details)){
					$data['order_id'] = $order_details[0]['order_id'];
					$data['order_for'] = $order_details[0]['order_for'];
					$data['plntyid_sel'] = $order_details[0]['plntyid'];
					$data['order_currency'] = $order_details[0]['order_currency'];
					$data['order_currency_symbol'] = $order_details[0]['order_currency_symbol'];
					$data['order_name'] = $order_details[0]['order_name'];
					$data['order_amount'] = $order_details[0]['order_amount'];
					$data['order_credit'] = $order_details[0]['order_credit'];
					$data['order_per_image'] = $order_details[0]['order_per_image'];
					$data['status'] = $order_details[0]['status'];
					$data['deleted'] = $order_details[0]['deleted'];
					
					
				}
			}else {
			        $data['order_id'] = '';
			        $data['order_for'] = '';
					$data['plntyid_sel'] = '';
					$data['order_currency'] = '';
					$data['order_currency_symbol'] = '';
					$data['order_name'] = '';
					$data['order_amount'] = '';
					$data['order_credit'] = '';
					$data['order_per_image'] = '';
					$data['status'] = '';
					$data['deleted'] = '';
					
					
			}
			$this->load->view('admin/order/order_management', $data);
		} 
		else 
		{
			//If no session, redirect to login page
			redirect('login', 'refresh');
		}
	}
	
	

	
	public function insert_order(){
		
		if ($this->session->userdata('logged_in')) {
			$session_data = $this->session->userdata('logged_in');
			$created_date = date('Y-m-d');
			$userid=$session_data['id'];
			$order_id=$this->input->post('order_id');
			$order_currency=$this->input->post('order_currency');



		if($order_currency=='INR')
 		{
 			$order_currency_symbol='&#x20B9;';
 		}
 		else if($order_currency=='USD')
 		{
 			$order_currency_symbol='&#36;';
 		}
 		else if($order_currency=='EUR')
 		{
 			$order_currency_symbol='&#128;';
 		}
 		else if($order_currency=='GBP')
 		{
 			$order_currency_symbol='&#163;';
 		}
 		else if($order_currency=='AUD')
 		{
 			$order_currency_symbol='&#36;';
 		}
 		else if($order_currency=='CNY')
 		{
 			$order_currency_symbol='&#165;';
 		}

 		else
 		{
 			$order_currency_symbol='';
 		}
			
			if($order_id!='')
			{
				//echo $this->input->post('order_for');exit();
				  $data = array(
				    'order_id'=>$this->input->post('order_id'),
				    'order_for'=>$this->input->post('order_for'),
					'plntyid'=>$this->input->post('plntyid'),
					'order_currency'=>$this->input->post('order_currency'),
					'order_currency_symbol'=>$order_currency_symbol,
					'order_name'=>$this->input->post('order_name'),
					'order_amount'=>$this->input->post('order_amount'),
					'order_credit'=>$this->input->post('order_credit'),
					'order_per_image'=>$this->input->post('order_per_image'),
					'status'=>$this->input->post('status'),
					);
					
				  $updateuserrole = $this->orders_model->order_update($data,$order_id);
			      echo $order_id;
				
			}else {
				
				
				
				   $data = array(
				    'order_id'=>$this->input->post('order_id'),
				    'order_for'=>$this->input->post('order_for'),
					'plntyid'=>$this->input->post('plntyid'),
					'order_currency'=>$this->input->post('order_currency'),
					'order_currency_symbol'=>$order_currency_symbol,
					'order_name'=>$this->input->post('order_name'),
					'order_amount'=>$this->input->post('order_amount'),
					'order_credit'=>$this->input->post('order_credit'),
					'order_per_image'=>$this->input->post('order_per_image'),
					'status'=>$this->input->post('status'),
					);

					$this->orders_model->order_insert($data);
					$order_id=$this->db->insert_id();
					
					
					echo $order_id;
				}
			
		}else {
		
            redirect('login', 'refresh');
       
		}  
	}
		
		
		
		

 
	public function deleteorder()
    {
		if ($this->session->userdata('logged_in')) 
		{
			$order_id = $this->input->post('order_id');
		
			//$this->db->query("delete from user where user_id='$user_id'");
			$this->db->query("update tbl_booking set deleted ='1',status='1' where order_id ='$order_id'");
			$this->session->set_flashdata('added', '<div align="left" style="color:green; font-size:15px; border:1px #398AB9 solid;">
		    Service Deleted Successfully</div>');

			//redirect('users');
		} 
		else 
		{
			//If no session, redirect to login page
			redirect('login', 'refresh');
		}
	}
	
	

	public function view($book_id=''){
		
		
		if ($this->session->userdata('logged_in'))		
		{  
			$session_data = $this->session->userdata('logged_in');
			$slang="english";
		    $this->lang->load($slang, $slang);
		    $data['lang']=$this->lang->language; 
			$data['title'] = "User Role";
			if($book_id!=''){
				$order_details=$this->db->query("select * from tbl_booking where book_id='$book_id'")->result_array();
				//echo '<pre>';
				// print_r($order_details);
				$plansTypeDetails = $this->plans_model->get_plansById($order_details[0]['plan_type']);
				// print_r($plansTypeDetails);
				$user_id=$order_details[0]['user_id'];
				$user_details=$this->db->query("SELECT * FROM `tbl_users` where  user_id='$user_id' ")->result_array();
//print_r($user_details);


				if(!empty($order_details)){
					$data['book_id'] = $order_details[0]['book_id'];
					$data['user_id'] = $order_details[0]['user_id'];
					$data['order_id'] = $order_details[0]['order_id'];
					$data['gender'] = $order_details[0]['gender'];
					$data['category'] = array_filter(explode(",", $order_details[0]['category']));
					$data['quty'] = array_filter(explode(",", $order_details[0]['quty']));
					$data['m_status'] = $order_details[0]['m_status'];
					$data['angle'] =  array_filter(explode(",", $order_details[0]['angle']));
					$data['d_status'] =$order_details[0]['d_status'];
					$data['notes'] = $order_details[0]['notes'];
					$data['b_status'] = $order_details[0]['b_status'];
					$data['total_image'] = $order_details[0]['total_image'];
					$data['total_price'] = $order_details[0]['total_price'];
					$data['payment_status'] = $order_details[0]['p_status'];
					$data['create_date'] = date("D, M d ,Y H:i A",strtotime($order_details[0]['create_date']));
					$data['currency_default'] = $order_details[0]['currency'];

					$data['planid'] = $plansTypeDetails[0]['planid'];
					$data['plan_name'] = $plansTypeDetails[0]['plan_name'];
					$data['plntypname'] = $plansTypeDetails[0]['plntypname'];

					$data['plan_credit'] = $plansTypeDetails[0]['plan_credit'];
					$data['plan_amount'] = $plansTypeDetails[0]['plan_amount'];

					$data['payment_type']="";


					$data['user_name'] = $user_details[0]['user_name'];
					$data['user_company'] = $user_details[0]['user_company'];
					$data['user_address'] = $user_details[0]['user_address'];
					$data['user_mobile'] = $user_details[0]['user_mobile'];
					
					
				}
			}else {
			        $data['book_id'] = '';
					$data['user_id'] = '';
					$data['order_id'] = '' ;
					$data['gender'] = '';
					$data['category'] = '';
					$data['quty'] = '';
					$data['m_status'] = '';
					$data['angle'] = '';
					$data['d_status'] = '';
					$data['notes'] = '';
					$data['b_status'] = '';
					$data['total_image'] = '';
					$data['total_price'] = '';
					$data['p_status'] = '';
					$data['create_date'] = '';
					$data['currency'] = '';

					$data['planid'] = '';
					$data['plan_name'] = '';
					$data['plntypname'] = '';
					
					$data['plan_credit'] = '';
					$data['plan_amount'] = '';

					$data['payment_type']="";

			}
			$this->load->view('admin/order/order_view', $data);
		} 
		else 
		{
			//If no session, redirect to login page
			redirect('login', 'refresh');
		}
	}



	public function updatestatus()
	{				//for email exist validation in edit customer form 


	$id=trim($this->input->get('id'));
	$status=trim($this->input->get('status'));
	
	$this->db->query("update tbl_booking set status ='$status' where order_id ='$id'");



	}

	
}
?>