<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Plan extends CI_Controller
{
    public function __construct()
	{
		parent::__construct();		
		$this->load->library('pagination');
		$this->load->database();		
		$this->load->library('Upload');
		$this->load->helper('security');	
		$this->load->library('Form_validation');
		$this->load->helper(array('form', 'html', 'file'));
		$this->load->library('pagination');
		$this->load->library('email');
        $this->load->model('plans_model');
        $this->load->library('userlib');
		
    	if ($this->session->userdata('logged_in')) {
            $session_data = $this->session->userdata('logged_in');
            $data['username'] = $session_data['username'];
            $data['firstname'] = $session_data['firstname'];
            $data['rolecode'] = $session_data['rolecode'];
			/* $timezone = $session_data['timezone'];
			date_default_timezone_set($timezone); */
         
        } else {
            //If no session, redirect to login page
            redirect('login', 'refresh');
        }
    }
	
    public function index()
	{
		if ($this->session->userdata('logged_in'))		
		{  
			$session_data = $this->session->userdata('logged_in');
			$data['rolecode'] = $session_data['rolecode'];
			$data['usertype'] = $session_data['rolecode'];
			$data['roles'] = $session_data['roles'];
			$data['title'] = "Users";
			$slang="english";
			$this->lang->load($slang, $slang);
			$data['lang']=$this->lang->language; 
			
			//echo "aaaaaaaaa";exit;
			$this->load->view('admin/plan/plan_list', $data);
		} 
		else 
		{
			//If no session, redirect to login page
			redirect('login', 'refresh');
		}
	}
	
	public function planlist(){
		if ($this->session->userdata('logged_in')) 
        {
			$session_data=$this->session->userdata('logged_in');
			$usertype = $session_data['rolecode'];
			$userid = $session_data['id'];
			$slang="english";
		    $this->lang->load($slang, $slang);
		    $data['lang']=$this->lang->language; 
    	    $aColumns = array( 'planid','plan_for','plntyid','plan_currency','plan_name','plan_amount','plan_credit','plan_per_image','status');
         
			/* Indexed column (used for fast and accurate table cardinality) */
			$sIndexColumn = "planid";
         
			/* DB table to use */
			$sTable = "tbl_plan";

            /*
			 * Paging
			 */
			$sLimit = "";
			if ( isset( $_GET['start'] ) && $_GET['length'] != '-1' )
			{
				$sLimit = "LIMIT ".intval( $_GET['start'] ).", ".
					intval( $_GET['length'] );
			}


    
     
			$sOrder = "";
			if ( isset( $_GET['order'] ) )
			{
				$sOrder = "ORDER BY  ";
			
						$sOrder .= $aColumns[0]."
							".($_GET['order'][0]['dir']==='desc' ? 'asc' : 'desc') .", ";
			   
				 
				$sOrder = substr_replace( $sOrder, "", -2 );
				if ( $sOrder == "ORDER BY" )
				{
					$sOrder = "";
				}
			}
	

        
        $commn="1=1 and deleted='0' ";
        
		
		//echo $_REQUEST['columns'][1]['search']['value']; exit();
		
		if(!empty($_GET['columns'][1]['search']['value']) ){  
	
		$commn.=" AND tbl_plan.plan_name  LIKE '".$_GET['columns'][1]['search']['value']."%' ";
		}

        $sWhere="where $commn";
         
		
		if (isset($_GET['search']['value']) && $_GET['search']['value'] != "" )
		{
			 $sWhere = "WHERE $commn   AND (";
			for ( $i=0 ; $i<count($aColumns) ; $i++ )
			{
				
				/*if ( isset($_GET['columns'][$i]['searchable']) && $_GET['columns'][$i]['searchable'] == "true" )
				{*/
					$sWhere .= $aColumns[$i]." LIKE '%".( $_GET['search']['value'] )."%' OR ";
				/*}*/
			}
			$sWhere = substr_replace( $sWhere, "", -3 );
			$sWhere .= ')';
		}
		
		
       $join='';
        /*
         * SQL queries
         * Get data to display
         */
        $sQuery = "
            SELECT SQL_CALC_FOUND_ROWS ".str_replace(" , ", " ", implode(", ", $aColumns))."
            FROM   $sTable $join
            $sWhere
            $sOrder
            $sLimit";

       //echo  $sQuery; exit();

        $rResult = $this->db->query($sQuery);
        $rResult = $rResult->result_array();
        //$rResult = mysqli_query( $sQuery) or fatal_error( 'MySQL Error: ' . mysqli_errno() );

        
        /* Data set length after filtering */
        //$sQuery = "SELECT FOUND_ROWS()";
        //$rResultFilterTotal = mysqli_query( $sQuery) or fatal_error( 'MySQL Error: ' . mysqli_errno() );
        //$query = $this->db->query($sQuery);
        //$aResultFilterTotal = $query->result_array();
        //$aResultFilterTotal = mysqli_fetch_array($rResultFilterTotal);
       $iFilteredTotal = count( $rResult);
          
        /* Total data set length */
        $sQuery = "
            SELECT COUNT(".$sIndexColumn.") as count
            FROM   $sTable $join
            $sWhere
        ";
        //$rResultTotal = mysqli_query( $sQuery) or fatal_error( 'MySQL Error: ' . mysqli_errno() );
        //$aResultTotal = mysqli_fetch_array($rResultTotal);

        $query = $this->db->query($sQuery);
        $aResultTotal = $query->result_array();

        //echo "<pre>";
        //print_r($aResultTotal);
         $iTotal = $aResultTotal[0]['count'];
         
        /*
         * Output
         */
        $output = array(
                        "sEcho" => '',
                        "iTotalRecords" => $iFilteredTotal,
                        "iTotalDisplayRecords" => $iTotal,
                        "aaData" => array()
                        );

            $sColumns = array('plan_for','plntyid','plan_currency','plan_name','plan_amount','plan_credit','plan_per_image','status','action');
         
            //while ( $aRow = mysqli_fetch_array( $rResult ) )
            //echo "<pre>";
           //print_r($output);
         
            foreach($rResult as $aRow)
            {
                $row = array();
            	
				$planid=$aRow['planid'];
				$plntyid=$aRow['plntyid'];
				$status=$aRow['status'];
                for ( $i=0 ; $i<count($sColumns) ; $i++ )
                {
                  
            	    if($sColumns[$i] =='action')
            		{
            			

						$row[]='<a href="javascript:void(0)"  class="btn btn-success view" val="'.$planid.'"><i class="fas fa-eye"></i></a>&nbsp;<a href="javascript:void(0)"  class="btn btn-info edit" val="'.$planid.'"><i class="fas fa-edit"></i></a>&nbsp; <a href="javascript:void(0)"  class="btn btn-danger delete" val="'.$planid.'"><i class="fas fa-trash-alt"></i></a>
        			  ';
						
            		}

            		else if($sColumns[$i] =='plntyid')
            		{


            			$plantype_details=$this->db->query("select * from tbl_plantype where plntyid='$plntyid'")->result_array();
						if(!empty($plantype_details)){
						 $row[]=$plantype_details[0]['plntypname'];
						}
						else
						{
							$row[]='';
						}
            		}
					
				
					else if($sColumns[$i] =='status')
            		{
            			
            			 $Enable_status="";
			             $Disable_status="";
						 
						 
					    if($status=='0')
						{
						 $Enable_status="selected";
			             $Disable_status="";
						
						}
						else if($status=='1')
						{
						 $Enable_status="";
			             $Disable_status="selected";
						 
						}
						
            			
            			 $row[]='<select name="status" id="status" class="selectpicker update_status">
								 <option value="0@@@'.$planid.'"  '.$Enable_status.'>Enable</option>
								 <option value="1@@@'.$planid.'"  '.$Disable_status.'>Disable</option>
								 </select>';
            			 
            		}
                    else if ( $sColumns[$i] != ' ' )
                    {
                        /* General output */
                        $row[] = $aRow[ $sColumns[$i] ];
                    }
                }
                $output['aaData'][] = $row;
            }
            //print_r($output);exit;
        echo json_encode( $output );
    }
	}
	
	
	public function add($planid=''){
		
		if ($this->session->userdata('logged_in'))		
		{  
			$session_data = $this->session->userdata('logged_in');
			$slang="english";
		    $this->lang->load($slang, $slang);
		    $data['lang']=$this->lang->language; 
			$data['title'] = "User Role";
			if($planid!=''){
				$plan_details=$this->db->query("select * from tbl_plan where planid='$planid'")->result_array();
				if(!empty($plan_details)){
					$data['planid'] = $plan_details[0]['planid'];
					$data['plan_for'] = $plan_details[0]['plan_for'];
					$data['plntyid_sel'] = $plan_details[0]['plntyid'];
					$data['plan_currency'] = $plan_details[0]['plan_currency'];
					$data['plan_currency_symbol'] = $plan_details[0]['plan_currency_symbol'];
					$data['plan_name'] = $plan_details[0]['plan_name'];
					$data['plan_amount'] = $plan_details[0]['plan_amount'];
					$data['plan_credit'] = $plan_details[0]['plan_credit'];
					$data['plan_per_image'] = $plan_details[0]['plan_per_image'];
					$data['status'] = $plan_details[0]['status'];
					$data['deleted'] = $plan_details[0]['deleted'];
					
					
				}
			}else {
			        $data['planid'] = '';
			        $data['plan_for'] = '';
					$data['plntyid_sel'] = '';
					$data['plan_currency'] = '';
					$data['plan_currency_symbol'] = '';
					$data['plan_name'] = '';
					$data['plan_amount'] = '';
					$data['plan_credit'] = '';
					$data['plan_per_image'] = '';
					$data['status'] = '';
					$data['deleted'] = '';
					
					
			}
			$this->load->view('admin/plan/plan_management', $data);
		} 
		else 
		{
			//If no session, redirect to login page
			redirect('login', 'refresh');
		}
	}
	
	

	
	public function insert_plan(){
		
		if ($this->session->userdata('logged_in')) {
			$session_data = $this->session->userdata('logged_in');
			$created_date = date('Y-m-d');
			$userid=$session_data['id'];
			$planid=$this->input->post('planid');
			$plan_currency=$this->input->post('plan_currency');



		if($plan_currency=='INR')
 		{
 			$plan_currency_symbol='&#x20B9;';
 		}
 		else if($plan_currency=='USD')
 		{
 			$plan_currency_symbol='&#36;';
 		}
 		else if($plan_currency=='EUR')
 		{
 			$plan_currency_symbol='&#128;';
 		}
 		else if($plan_currency=='GBP')
 		{
 			$plan_currency_symbol='&#163;';
 		}
 		else if($plan_currency=='AUD')
 		{
 			$plan_currency_symbol='&#36;';
 		}
 		else if($plan_currency=='CNY')
 		{
 			$plan_currency_symbol='&#165;';
 		}

 		else
 		{
 			$plan_currency_symbol='';
 		}
			
			if($planid!='')
			{
				
				  $data = array(
				    'planid'=>$this->input->post('planid'),
				    'plan_for'=>$this->input->post('plan_for'),
					'plntyid'=>$this->input->post('plntyid'),
					'plan_currency'=>$this->input->post('plan_currency'),
					'plan_currency_symbol'=>$plan_currency_symbol,
					'plan_name'=>$this->input->post('plan_name'),
					'plan_amount'=>$this->input->post('plan_amount'),
					'plan_credit'=>$this->input->post('plan_credit'),
					'plan_per_image'=>$this->input->post('plan_per_image'),
					'status'=>$this->input->post('status'),
					);
				//	print_r($data);exit();
				  $updateuserrole = $this->plans_model->plan_update($data,$planid);
			      echo $planid;
				
			}else {
				
				
				
				   $data = array(
				    'planid'=>$this->input->post('planid'),
				    'plan_for'=>$this->input->post('plan_for'),
					'plntyid'=>$this->input->post('plntyid'),
					'plan_currency'=>$this->input->post('plan_currency'),
					'plan_currency_symbol'=>$plan_currency_symbol,
					'plan_name'=>$this->input->post('plan_name'),
					'plan_amount'=>$this->input->post('plan_amount'),
					'plan_credit'=>$this->input->post('plan_credit'),
					'plan_per_image'=>$this->input->post('plan_per_image'),
					'status'=>$this->input->post('status'),
					);
					//print_r($data);exit();
					$this->plans_model->plan_insert($data);
					$planid=$this->db->insert_id();
					
					
					echo $planid;
				}
			
		}else {
		
            redirect('login', 'refresh');
       
		}  
	}
		
		
		
		

 
	public function deleteplan()
    {
		if ($this->session->userdata('logged_in')) 
		{
			$planid = $this->input->post('planid');
		
			//$this->db->query("delete from user where user_id='$user_id'");
			$this->db->query("update tbl_plan set deleted ='1',status='1' where planid ='$planid'");
			$this->session->set_flashdata('added', '<div align="left" style="color:green; font-size:15px; border:1px #398AB9 solid;">
		    Service Deleted Successfully</div>');

			//redirect('users');
		} 
		else 
		{
			//If no session, redirect to login page
			redirect('login', 'refresh');
		}
	}
	
	

	public function view($planid=''){
		
		
		if ($this->session->userdata('logged_in'))		
		{  
			$session_data = $this->session->userdata('logged_in');
			$slang="english";
		    $this->lang->load($slang, $slang);
		    $data['lang']=$this->lang->language; 
			$data['title'] = "User Role";
			if($planid!=''){
				$plan_details=$this->db->query("select * from tbl_plan where planid='$planid'")->result_array();
				if(!empty($plan_details)){
					$data['planid'] = $plan_details[0]['planid'];
					$data['plan_for'] = $plan_details[0]['plan_for'];
					$data['plntyid_sel'] = $plan_details[0]['plntyid'];
					$data['plan_currency'] = $plan_details[0]['plan_currency'];
					$data['plan_currency_symbol'] = $plan_details[0]['plan_currency_symbol'];
					$data['plan_name'] = $plan_details[0]['plan_name'];
					$data['plan_amount'] = $plan_details[0]['plan_amount'];
					$data['plan_credit'] = $plan_details[0]['plan_credit'];
					$data['plan_per_image'] = $plan_details[0]['plan_per_image'];
					$data['status'] = $plan_details[0]['status'];
					$data['deleted'] = $plan_details[0]['deleted'];
					
					
				}
			}else {
			        $data['planid'] = '';
			    	$data['plan_for'] = '';
					$data['plntyid_sel'] = '';
					$data['plan_currency'] = '';
					$data['plan_currency_symbol'] = '';
					$data['plan_name'] = '';
					$data['plan_amount'] = '';
					$data['plan_credit'] = '';
					$data['plan_per_image'] = '';
					$data['status'] = '';
					$data['deleted'] = '';
					
					
			}
			$this->load->view('admin/plan/plan_management', $data);
		} 
		else 
		{
			//If no session, redirect to login page
			redirect('login', 'refresh');
		}
	}



	public function updatestatus()
	{				//for email exist validation in edit customer form 


	$id=trim($this->input->get('id'));
	$status=trim($this->input->get('status'));
	
	$this->db->query("update tbl_plan set status ='$status' where planid ='$id'");



	}

	
}
?>