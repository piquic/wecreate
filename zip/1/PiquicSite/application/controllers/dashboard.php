<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
session_start();
class Dashboard extends CI_Controller
{
    public function __construct()
    {
		
        parent::__construct();
        $this->load->library('pagination');
        $this->load->helper('url');
        $this->load->model('Dashboard_model');
		
		if ($this->session->userdata('logged_in')) 
		{
            $session_data = $this->session->userdata('logged_in');
			//print_r($session_data);exit;
            $data['username'] = $session_data['username'];
            $data['firstname'] = $session_data['firstname'];
            $data['rolecode'] = $session_data['rolecode'];
			$data['userid'] = $session_data['id'];
			//$data['logo']=$session_data['logo'];
			//$data['color']=$this->getColor($session_data['id']);
			$timezone = $session_data['timezone'];
			date_default_timezone_set($timezone);
         
        } else {
            //If no session, redirect to login page
            redirect('login', 'refresh');
        }
		
    }

    public function index()
    {
        if ($this->session->userdata('logged_in')) {
			
            $session_data = $this->session->userdata('logged_in');
            $data['username'] = $session_data['username'];
            $userid = $session_data['id'];
            $data['firstname'] = $session_data['firstname'];
            $data['rolecode'] = $session_data['rolecode'];
			$data['userid'] = $session_data['id'];
            $data['email'] = $session_data['email'];
			//$data['color']=$this->getColor($session_data['id']);
            $data['title'] = "Dashboard";
            $data['title'] = 'Welcome To Admin Dashboard';	
			
			if($data['rolecode']=='1')
			{
			//$data['ownercount']=$this->Dashboard_model->getOwnerlist();	
			/* $data['customercount']=$this->Dashboard_model->getCustomerlist();	
			$data['servicecount']=$this->Dashboard_model->getServicelist();	
			$data['eventcount']=$this->Dashboard_model->getEventlist();	
			$data['todaysalecount']=$this->Dashboard_model->getTodaysalecount($userid);	
			$data['bookinglist']=$this->Dashboard_model->getBookinglist();	
			$data['bookinglistwithservice']=$this->Dashboard_model->getBookinglistwithservice();	 */
			} 
			else {
			//$data['ownercount']=$this->Dashboard_model->sgetBookinglist($userid);		
			
			}
			
			
			
			
			
			
			
			
			
						
			
			$this->load->view('admin/dashboard', $data);     
			
		}
		else {
            //If no session, redirect to login page
            redirect('login', 'refresh');
        }
    }

    public function logout()
    {
		 session_destroy();
        $this->session->unset_userdata('logged_in');
        redirect('login', 'refresh');
    }
	
	 public function getColor($id)		//for getting the servicename from the service table. 
	{
			$querys = $this->db->query("select color from nc_user where owner_id='$id'")->result_array();	
			$color = $querys['0']['color'];
			return $color;
	}
	
	public function bookind_detail($manual_booking_id)
	{
		
		 if ($this->session->userdata('logged_in')) {
            $session_data = $this->session->userdata('logged_in');
			$data['id'] = $session_data['id'];
			$id = $session_data['id'];
            $data['username'] = $session_data['username'];
            $data['firstname'] = $session_data['firstname'];
            $data['rolecode'] = $session_data['rolecode'];
			$data['manual_booking_id'] = $manual_booking_id;
			$data['color']=$this->getColor($session_data['id']);
			$data['logo']=$session_data['logo'];
			
			$res_invoice=$this->db->query("select * from invoice where manual_booking_id='$manual_booking_id'")->result_array();
			
			if(!$res_invoice)
			{
				$res_booking=$this->db->query("select start_time,time_booking,duration,options,manual_booking.description,service_master.price,bussiness_owner.sales_tax,bussiness_owner.business_name,bussiness_owner.owner_id,bussiness_owner.abn_no,manual_booking.unique_booking_id,manual_booking.created_date,manual_booking.start_date from manual_booking left join bussiness_owner on bussiness_owner.owner_id=manual_booking.owner_id left join service_master on service_master.service_id=manual_booking.service_id where manual_booking_id='$manual_booking_id'")->result_array();
				
				
				foreach($res_booking as $res)
				{			
					//BI-0001-01
					$bname=$res['business_name'];
					$bname=substr($bname, 0, 2);
					$mid=sprintf('%04d', $manual_booking_id);
					$invoice_num=$bname.'-'.$mid.'-'.'1';					

					$resdata = array(
					   'invoice_number' =>$invoice_num ,
					   'manual_booking_id' => $manual_booking_id
					);
					$this->db->insert('invoice', $resdata); 
					$data['invoice_num']=$invoice_num;
					
				}
			
			}else
			{
				$res_booking=$this->db->query("select * from invoice where manual_booking_id='$manual_booking_id'")->result_array();	
				
					
				foreach($res_booking as $res)
				{			
					//BI-0001-01
					$invoice_number=$res['invoice_number'];
					$inumber=explode("-",$invoice_number);
					$inum=end($inumber)+1;
					array_pop($inumber);
					array_push($inumber, $inum);   

					$invoice_num=implode('-',$inumber);
					$resdata = array(
					   'invoice_number' =>$invoice_num
					);
					$this->db->where('manual_booking_id', $manual_booking_id);
					$this->db->update('invoice', $resdata); 
					
					$data['invoice_num']=$invoice_num;

				}
		
			}
			
			$data['invoice_data']= $this->db->query("SELECT start_time,time_booking,duration,options,manual_booking.description,customer_master.name,customer_master.address,bussiness_owner.business_name,bussiness_owner.logo,service_master.payment_mode,bussiness_owner.sales_tax,customer_master.email, bussiness_owner.email as be ,bussiness_owner.mobile as bmobile,customer_master.mobile,manual_booking.service_id,manual_booking.customer_id,manual_booking.paid,manual_booking.tax,manual_booking.owner_id,bussiness_owner.address as ba,service_master.owner_id,service_master.service_title,service_master.price,bussiness_owner.abn_no,manual_booking.unique_booking_id,manual_booking.created_date,manual_booking.start_date,manual_booking.tax,manual_booking.service_price FROM manual_booking  INNER JOIN bussiness_owner on manual_booking.owner_id = bussiness_owner.owner_id INNER JOIN customer_master on customer_master.customer_id= manual_booking.customer_id INNER JOIN service_master on service_master.service_id= manual_booking.service_id where manual_booking.manual_booking_id='$manual_booking_id'")->result_array();
			//return $invoice_data;
			
			
			$data['invoice_package_data']= $this->db->query("SELECT packages.* FROM manual_booking  INNER JOIN packages on manual_booking.package_id = packages.package_id  where manual_booking.manual_booking_id='$manual_booking_id'")->result_array();
			$this->load->view('admin/booking/booking_popoup_detail', $data);
		 }
		
	}
	

}

?>