<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">

    <title>Image Checker</title>
  </head>
  <body>
    <header class="bg-light text-center p-3 shadow-sm">
      <img src="../PiquicTest/images/logo-piquic-md.png">
    </header>

    <main>
      <div class="container">
        <div class="row">
          <div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12 pt-3">
            <p class="display-4">Image Checker</p>
          </div>
        </div>

        <div class="row">
          <div class="col-12 col-sm-12 col-md-6 col-lg-6 col-xl-6">
            <div class="shadow-sm my-3 p-3 rounded">
              <div class="custom-file">
                <input type="file" class="custom-file-input" id="customFile">
                <label id="lblCustomFile" class="custom-file-label" for="customFile">Choose file</label>
              </div>
            </div>
          </div>

          <div class="col-12 col-sm-12 col-md-6 col-lg-6 col-xl-6">
            <div class="shadow-sm my-3 p-3 rounded text-center w-100" style="min-height: 25em;">
              <img style="max-height: 23em;" class="img-fluid" id='img-upload' src="https://via.placeholder.com/1333x2000?text=Uload+Image+For+Preview"/>
            </div>
          </div>
        </div>
      </div>
    </main>

    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>

    <script type="text/javascript">
      $(document).ready( function() {
        $(document).on('change', '#customFile :file', function() {
          var input = $(this),
          label = input.val().replace(/\\/g, '/').replace(/.*\//, '');
          input.trigger('fileselect', [label]);

        });

        function readURL(input) {
          if (input.files && input.files[0]) {
            var reader = new FileReader();
            
            // $('#lblCustomFile').text(label);

            reader.onload = function (e) {
              $('#img-upload').attr('src', e.target.result);
            }

            reader.readAsDataURL(input.files[0]);
          }
        }

        $("#customFile").change(function(){
          readURL(this);

        });   
      });
    </script>
  </body>
</html>