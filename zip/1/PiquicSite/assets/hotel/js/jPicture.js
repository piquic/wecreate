/*!
 * jPicture v1.1.3
 *
 * Copyright 2017, Frank Chao
 * Released under the MIT license
 *
 * Released on: August 29, 2017
 */

!(function ( factory ) {

	// 检测是否引入了 jQuery
	if ( typeof jQuery === "undefined" ) {
		throw new Error("jPicture\'s Javascript requires jQuery !");
	}

	// 判断引用方式
	if ( typeof define === "function" && define.amd ) {
		define(["jquery"], function ( $ ) {
			return factory($);
		});
	} else if ( typeof module === "object" && typeof module.exports === "object" ) {
		exports = factory(require("jquery"));
	} else {
		factory(jQuery);
	}

}(function ( $ ) {

	// 检测 jQuery 版本
	// 版本要求在 1.11.0 或以上
	// 本组件中设置 type="fade" 方法，若使用版本 1.11.0 之前的 jQuery，在快速切换时会出现透明度异常的问题
	var version = $.fn.jquery.split(".");
	if ( ~~version[0] === 1 && ~~version[1] < 11 ) {
		throw new Error("jPicture\'s Javascript requires at least jQuery v1.11.0 !");
	}

	// 创建全局变量 `jPicture`
	var jPicture = window.jPicture = function ( selector, settings ) {  

		// 检测桌面端和移动端
		var isDeskTop = navigator.userAgent.toLowerCase().match(/(windows nt|macintosh)/) ? true : false,
			isMobile = !isDeskTop && "ontouchend" in document ? true : false;

		$(function () {

			$(selector).each(function () {
				var $this = $(this),
					$link = $this.find("a");
				var width = $this.width(),
					height = $this.height();
				var type = isMobile ? "slide" : (settings.type || "fade"),
					arrows = settings.arrow,
					dots = settings.dot,
					dotTheme = settings.dotTheme || "circle",
					auto = settings.autoplay;

				$this.addClass("jPicture-container").children().first().addClass("jPicture-inner").children().addClass("jPicture-part");

				// 图片区域禁用选择功能
				// 防止快速点击按钮或箭头时出现选中内容的问题
				// 设置初始图片索引为 0
				$this.on("selectstart", function () {
					return false;
				}).data("cacheIndex", 0);

				var $inner = $this.find(".jPicture-inner"),
					$part = $this.find(".jPicture-part"),
					$imglink = $this.find("img, a"),
					imgSize = $inner.find("img").length;

				// 给图片容器和其内部所有元素强制设置宽高
				// 建议传入的图片都能保证尺寸一致并且与最外层容器的大小相同
				// 以防因相关尺寸不统一而导致图片变形
				$part.add($imglink).width(width).height(height);

				// 添加切换按钮
				var dot = "";
				for ( var i = 0; i < imgSize; i++ ) {
					dot += "<b></b>";
				}
				dot = "<div class='jPicture-dot'>" + dot + "</div>";
				$inner.after(dot);
				var $dot = $this.find(".jPicture-dot"),
					$point = $dot.find("b");
				$dot.addClass("jPicture-dot-" + dotTheme).each(function () {
					$(this).css("marginLeft", -($(this).find("b").outerWidth(true)*$part.length)/2 + "px");	
				});
				$point.first().addClass("active");
				$this.find(".jPicture-dot-square b").append("<i></i>");

				// 添加左右切换箭头
				isDeskTop && $this.append("<b class='jPicture-prev'></b><b class='jPicture-next'></b>");
				var $prev = $this.find(".jPicture-prev"),
					$next = $this.find(".jPicture-next");

				// 根据传入的参数判断是否显示按钮和箭头
				if ( typeof arrows === "boolean" && !arrows ) {
					$prev.add($next).remove();
				}
				if ( typeof dots === "boolean" && !dots ) {
					$dot.remove();
				}

				// 根据不同的图片切换效果进行不同的处理
				if ( type === "fade" ) {
					$inner.addClass("jPicture-fade");
					$part.first().show();
				}
				if ( type === "slide" ) {
					var $part = $inner.find(".jPicture-part");
					var $partFirst = $part.first(),
						$partLast = $part.last();
					$inner.addClass("jPicture-slide").width(width * (imgSize + 2)).append($partFirst.clone(true)).prepend($partLast.clone(true)).css("left", -width + "px");
				}

				// 设定统一的图片切换动画过渡时间为 700ms
				var time = 700;

				// 按钮切换和箭头切换
				if ( isDeskTop ) {
					$point.click(function () {
						var index = $(this).index();
						Animation(index);
						$this.data("cacheIndex", index);		
					})
					$prev.click(function () {
						var index = $this.data("cacheIndex");
						index--;
						Animation(index);
					})
					$next.click(function () {
						var index = $this.data("cacheIndex");
						index++
						Animation(index);
					})
				}

				// 动画函数
				function Animation ( i ) {
					if ( type === "fade" ) { 
						i = (i == -1 ? imgSize - 1 : (i == imgSize ? 0 : i));
						$part.eq(i).stop().fadeIn(time).siblings().stop().fadeOut(time, function () {
							dotSwitch(i);
							$this.data("cacheIndex", i);
						});
					}
					if ( type === "slide" ) {

						// 设置缓动效果
						$.easing.easeInOutQuart = function ( x, t, b, c, d ) {
							return ( (t/=d/2) < 1 ) ? c/2*t*t*t*t + b : -c/2 * ((t-=2)*t*t*t - 2) + b;
						}

						if ( !$inner.is(":animated") ) {
							$inner.stop().animate({
								left: -(width * i + width) + "px"
							}, time, "easeInOutQuart", function () {
								if ( i == -1 ) {
									$inner.css("left", "-" + width * imgSize + "px");
									i = imgSize - 1;
								}
								if ( i == imgSize ) {
									$inner.css("left", "-" + width + "px");
									i = 0;
								}
								dotSwitch(i);
								$this.data("cacheIndex", i);
								$link.removeClass("jPicture-pointerevents-none");
							});
						}
					}
				}

				// 按钮状态切换
				function dotSwitch ( i ) {
					$point.eq(i).addClass("active").siblings().removeClass("active");
				}

				// 自动执行动画
				if ( auto && typeof auto === "number" ) {
					var autoAnimate = setInterval(function () {
						var getIndex = $this.data("cacheIndex");
						getIndex++;
						Animation(getIndex);
					}, auto);
					if ( isDeskTop ) {
						$this.mouseenter(function () {
							clearInterval(autoAnimate);
						}).mouseleave(function () {
							autoAnimate = setInterval(function () {
								var getIndex = $this.data("cacheIndex");
								getIndex++;
								Animation(getIndex);
							}, auto);
						})
					} else {
						$this.on("touchstart", function () {
							clearInterval(autoAnimate);
						}).on("touchend", function () {
							autoAnimate = setInterval(function () {
								var getIndex = $this.data("cacheIndex");
								getIndex++;
								Animation(getIndex);
							}, auto);
						})
					}
				}

				// 移动端处理
				if ( isMobile ) {
					time = 500;
					var startX, startY, x, y, xx, yy;
					$this.on("touchstart", function ( e ) {
						startX = e.originalEvent.targetTouches[0].pageX;
					}).on("touchmove", function ( e ) {
						xx = e.originalEvent.changedTouches[0].pageX;
						$link.addClass("jPicture-pointerevents-none");
						if ( xx != startX ) {
							e.preventDefault();
						}
					}).on("touchend", function ( e ) {
						x = e.originalEvent.changedTouches[0].pageX;
						var i = $this.data("cacheIndex");
						if ( x - startX < -30 ) {
							i++;
						}
						if ( x - startX > 30 ) {
							i--;
						}
						Animation(i);
					})
				}

			});
		})

	};

}));
