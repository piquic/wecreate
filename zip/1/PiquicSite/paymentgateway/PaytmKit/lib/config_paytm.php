<?php
/*
- Use PAYTM_ENVIRONMENT as 'PROD' if you wanted to do transaction in production environment else 'TEST' for doing transaction in testing environment.
- Change the value of PAYTM_MERCHANT_KEY constant with details received from Paytm.
- Change the value of PAYTM_MERCHANT_MID constant with details received from Paytm.
- Change the value of PAYTM_MERCHANT_WEBSITE constant with details received from Paytm.
- Above details will be different for testing and production environment.
*/
include('../../ajaxpages/config.php');

$mdlqry   = "select * from  tbl_settings  WHERE setting_key='PAYTM_MERCHANT_KEY' ";
$mdlres   = $second_DB->query($mdlqry);
$mdlrow   = $mdlres->fetch_assoc();
$PAYTM_MERCHANT_KEY = $mdlrow['setting_value'];


$mdlqry   = "select * from  tbl_settings  WHERE setting_key='PAYTM_MERCHANT_MID' ";
$mdlres   = $second_DB->query($mdlqry);
$mdlrow   = $mdlres->fetch_assoc();
$PAYTM_MERCHANT_MID = $mdlrow['setting_value'];


define('PAYTM_ENVIRONMENT', 'TEST'); // PROD
define('PAYTM_MERCHANT_KEY', $PAYTM_MERCHANT_KEY); //Change this constant's value with Merchant key received from Paytm.
define('PAYTM_MERCHANT_MID', $PAYTM_MERCHANT_MID); //Change this constant's value with MID (Merchant ID) received from Paytm.
define('PAYTM_MERCHANT_WEBSITE', 'WEBSTAGING'); //Change this constant's value with Website name received from Paytm.



if (PAYTM_ENVIRONMENT == 'TEST') {
	$PAYTM_STATUS_QUERY_NEW_URL='https://securegw-stage.paytm.in/merchant-status/getTxnStatus';
    $PAYTM_TXN_URL='https://securegw-stage.paytm.in/theia/processTransaction';
}
else if (PAYTM_ENVIRONMENT == 'PROD') {
	$PAYTM_STATUS_QUERY_NEW_URL='https://securegw.paytm.in/merchant-status/getTxnStatus';
	$PAYTM_TXN_URL='https://securegw.paytm.in/theia/processTransaction';
}

define('PAYTM_REFUND_URL', '');
define('PAYTM_STATUS_QUERY_URL', $PAYTM_STATUS_QUERY_NEW_URL);
define('PAYTM_STATUS_QUERY_NEW_URL', $PAYTM_STATUS_QUERY_NEW_URL);
define('PAYTM_TXN_URL', $PAYTM_TXN_URL);
?>
