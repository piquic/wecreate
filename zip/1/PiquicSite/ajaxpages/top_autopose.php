<?php 

//echo "xxxxxxxxxxx";
   ///  Top Start ///
   if($dtypecat=='TOP'){

    if($neck=='ROUND NECK'){

      if($sleeve=='SLEEVELESS'  || $sleeve=='CAP SLEEVE'){
        $mquery = "SELECT * FROM `tbl_model_pose`  where chin_position IN ('chin-up','chin-straight','chin-down') AND arm_shape IN ('armmovement','forearmmovement','bothstraight') AND  m_id='$mid'  AND mpose_id IN($mod_pose_id) AND pose_status!='1' AND `pose_ang` ='1' ORDER BY RAND() LIMIT 0,2;";

        $lmquery = "SELECT * FROM `tbl_model_pose`  where chin_position IN ('chin-up','chin-straight','chin-down') AND arm_shape IN ('armmovement','forearmmovement','bothstraight') AND  m_id='$mid'  AND mpose_id IN($mod_pose_id) AND pose_status!='1' AND `pose_ang` ='3' ORDER BY RAND() LIMIT 0,1;";


        $bmquery = "SELECT * FROM `tbl_model_pose`  where chin_position IN ('chin-up','chin-straight','chin-down') AND arm_shape IN ('armmovement','forearmmovement','bothstraight') AND  m_id='$mid'  AND mpose_id IN($mod_pose_id) AND pose_status!='1' AND `pose_ang` ='2' ORDER BY RAND() LIMIT 0,1;";


        $rmquery = "SELECT * FROM `tbl_model_pose`  where chin_position IN ('chin-up','chin-straight','chin-down') AND arm_shape IN ('armmovement','forearmmovement','bothstraight') AND  m_id='$mid'  AND mpose_id IN($mod_pose_id) AND pose_status!='1' AND `pose_ang` ='4' ORDER BY RAND() LIMIT 0,1;";


      }

      if($sleeve=='ABOVE ELBOW SLEEVE' || $sleeve=='SHORT SLEEVE'){

        $mquery = "SELECT * FROM `tbl_model_pose`  where chin_position IN ('chin-up','chin-straight','chin-down') AND arm_shape IN ('forearmmovement','bothstraight') AND  m_id='$mid'   AND mpose_id IN($mod_pose_id) AND pose_status!='1' AND `pose_ang` ='1' ORDER BY RAND() LIMIT 0,2;";

        $lmquery = "SELECT * FROM `tbl_model_pose`  where chin_position IN ('chin-up','chin-straight','chin-down') AND arm_shape IN ('forearmmovement','bothstraight') AND  m_id='$mid'   AND mpose_id IN($mod_pose_id) AND pose_status!='1' AND `pose_ang` ='3' ORDER BY RAND() LIMIT 0,1;";
        $bmquery = "SELECT * FROM `tbl_model_pose`  where chin_position IN ('chin-up','chin-straight','chin-down') AND arm_shape IN ('forearmmovement','bothstraight') AND  m_id='$mid'   AND mpose_id IN($mod_pose_id) AND pose_status!='1' AND `pose_ang` ='2' ORDER BY RAND() LIMIT 0,1;";
        $rmquery = "SELECT * FROM `tbl_model_pose`  where chin_position IN ('chin-up','chin-straight','chin-down') AND arm_shape IN ('forearmmovement','bothstraight') AND  m_id='$mid'   AND mpose_id IN($mod_pose_id) AND pose_status!='1' AND `pose_ang` ='4' ORDER BY RAND() LIMIT 0,1;";
      }

      if($sleeve=='FULL SLEEVE' || $sleeve=='BELOW ELBOW SLEEVE' || $sleeve=='3/4th SLEEVE' ){
        $mquery = "SELECT * FROM `tbl_model_pose`  where chin_position IN ('chin-up','chin-straight','chin-down') AND arm_shape IN ('bothstraight') AND  m_id='$mid'   AND mpose_id IN($mod_pose_id) AND pose_status!='1'AND `pose_ang` ='1' ORDER BY RAND() LIMIT 0,2;";
         $lmquery = "SELECT * FROM `tbl_model_pose`  where chin_position IN ('chin-up','chin-straight','chin-down') AND arm_shape IN ('bothstraight') AND  m_id='$mid'   AND mpose_id IN($mod_pose_id) AND pose_status!='1'AND `pose_ang` ='3' ORDER BY RAND() LIMIT 0,1;";
          $bmquery = "SELECT * FROM `tbl_model_pose`  where chin_position IN ('chin-up','chin-straight','chin-down') AND arm_shape IN ('bothstraight') AND  m_id='$mid'   AND mpose_id IN($mod_pose_id) AND pose_status!='1'AND `pose_ang` ='2' ORDER BY RAND() LIMIT 0,1;";
           $rmquery = "SELECT * FROM `tbl_model_pose`  where chin_position IN ('chin-up','chin-straight','chin-down') AND arm_shape IN ('bothstraight') AND  m_id='$mid'   AND mpose_id IN($mod_pose_id) AND pose_status!='1'AND `pose_ang` ='4' ORDER BY RAND() LIMIT 0,1;";
      }

    }else if($neck=='OPEN NECK' || $neck=='V NECK'){

      if($sleeve=='SLEEVELESS'  || $sleeve=='CAP SLEEVE'){
        $mquery = "SELECT * FROM `tbl_model_pose`  where chin_position IN ('chin-up','chin-straight') AND arm_shape IN ('armmovement','forearmmovement','bothstraight') AND  m_id='$mid'  AND mpose_id IN($mod_pose_id) AND pose_status!='1'AND `pose_ang` ='1' ORDER BY RAND() LIMIT 0,2;";

        $lmquery = "SELECT * FROM `tbl_model_pose`  where chin_position IN ('chin-up','chin-straight') AND arm_shape IN ('armmovement','forearmmovement','bothstraight') AND  m_id='$mid'  AND mpose_id IN($mod_pose_id) AND pose_status!='1'AND `pose_ang` ='3' ORDER BY RAND() LIMIT 0,1;";


        $bmquery = "SELECT * FROM `tbl_model_pose`  where chin_position IN ('chin-up','chin-straight') AND arm_shape IN ('armmovement','forearmmovement','bothstraight') AND  m_id='$mid'  AND mpose_id IN($mod_pose_id) AND pose_status!='1'AND `pose_ang` ='2' ORDER BY RAND() LIMIT 0,1;";


        $rmquery = "SELECT * FROM `tbl_model_pose`  where chin_position IN ('chin-up','chin-straight') AND arm_shape IN ('armmovement','forearmmovement','bothstraight') AND  m_id='$mid'  AND mpose_id IN($mod_pose_id) AND pose_status!='1'AND `pose_ang` ='4' ORDER BY RAND() LIMIT 0,1;";
      }



      if($sleeve=='ABOVE ELBOW SLEEVE'|| $sleeve=='SHORT SLEEVE'){
        $mquery = "SELECT * FROM `tbl_model_pose`  where chin_position IN ('chin-up','chin-straight') AND arm_shape IN ('forearmmovement','bothstraight') AND  m_id='$mid'  AND mpose_id IN($mod_pose_id) AND pose_status!='1'AND `pose_ang` ='1' ORDER BY RAND() LIMIT 0,2;";

        $lmquery = "SELECT * FROM `tbl_model_pose`  where chin_position IN ('chin-up','chin-straight') AND arm_shape IN ('forearmmovement','bothstraight') AND  m_id='$mid'  AND mpose_id IN($mod_pose_id) AND pose_status!='1'AND `pose_ang` ='3' ORDER BY RAND() LIMIT 0,1;";


        $bmquery = "SELECT * FROM `tbl_model_pose`  where chin_position IN ('chin-up','chin-straight') AND arm_shape IN ('forearmmovement','bothstraight') AND  m_id='$mid'  AND mpose_id IN($mod_pose_id) AND pose_status!='1'AND `pose_ang` ='2' ORDER BY RAND() LIMIT 0,1;";


        $rmquery = "SELECT * FROM `tbl_model_pose`  where chin_position IN ('chin-up','chin-straight') AND arm_shape IN ('forearmmovement','bothstraight') AND  m_id='$mid'  AND mpose_id IN($mod_pose_id) AND pose_status!='1'AND `pose_ang` ='4' ORDER BY RAND() LIMIT 0,1;";
      }


      if($sleeve=='FULL SLEEVE' || $sleeve=='BELOW ELBOW SLEEVE' || $sleeve=='3/4th SLEEVE' ){
        $mquery = "SELECT * FROM `tbl_model_pose`  where chin_position IN ('chin-up','chin-straight') AND arm_shape IN ('bothstraight') AND  m_id='$mid'  AND mpose_id IN($mod_pose_id) AND pose_status!='1'AND `pose_ang` ='1' ORDER BY RAND() LIMIT 0,2;";
        $lmquery = "SELECT * FROM `tbl_model_pose`  where chin_position IN ('chin-up','chin-straight') AND arm_shape IN ('bothstraight') AND  m_id='$mid'  AND mpose_id IN($mod_pose_id) AND pose_status!='1'AND `pose_ang` ='3' ORDER BY RAND() LIMIT 0,1;";

        $bmquery = "SELECT * FROM `tbl_model_pose`  where chin_position IN ('chin-up','chin-straight') AND arm_shape IN ('bothstraight') AND  m_id='$mid'  AND mpose_id IN($mod_pose_id) AND pose_status!='1'AND `pose_ang` ='2' ORDER BY RAND() LIMIT 0,1;";

        $rmquery = "SELECT * FROM `tbl_model_pose`  where chin_position IN ('chin-up','chin-straight') AND arm_shape IN ('bothstraight') AND  m_id='$mid'  AND mpose_id IN($mod_pose_id) AND pose_status!='1'AND `pose_ang` ='4' ORDER BY RAND() LIMIT 0,1;";
      }

    }else if($neck=='HIGH NECK'){

      if($sleeve=='SLEEVELESS'  || $sleeve=='CAP SLEEVE'){
        $mquery = "SELECT * FROM `tbl_model_pose`  where chin_position IN ('chin-up','chin-straight') AND arm_shape IN ('armmovement','forearmmovement','bothstraight') AND  m_id='$mid'  AND mpose_id IN($mod_pose_id) AND pose_status!='1'AND `pose_ang` ='1' ORDER BY RAND() LIMIT 0,2;";
        $lmquery = "SELECT * FROM `tbl_model_pose`  where chin_position IN ('chin-up','chin-straight') AND arm_shape IN ('armmovement','forearmmovement','bothstraight') AND  m_id='$mid'  AND mpose_id IN($mod_pose_id) AND pose_status!='1'AND `pose_ang` ='3' ORDER BY RAND() LIMIT 0,1;";

        $bmquery = "SELECT * FROM `tbl_model_pose`  where chin_position IN ('chin-up','chin-straight') AND arm_shape IN ('armmovement','forearmmovement','bothstraight') AND  m_id='$mid'  AND mpose_id IN($mod_pose_id) AND pose_status!='1'AND `pose_ang` ='2' ORDER BY RAND() LIMIT 0,1;";

        $rmquery = "SELECT * FROM `tbl_model_pose`  where chin_position IN ('chin-up','chin-straight') AND arm_shape IN ('armmovement','forearmmovement','bothstraight') AND  m_id='$mid'  AND mpose_id IN($mod_pose_id) AND pose_status!='1'AND `pose_ang` ='4' ORDER BY RAND() LIMIT 0,1;";
      }

      if($sleeve=='ABOVE ELBOW SLEEVE' || $sleeve=='SHORT SLEEVE'){
        $mquery = "SELECT * FROM `tbl_model_pose`  where chin_position IN ('chin-up','chin-straight') AND arm_shape IN ('forearmmovement','bothstraight') AND  m_id='$mid'  AND mpose_id IN($mod_pose_id) AND pose_status!='1'AND `pose_ang` ='1' ORDER BY RAND() LIMIT 0,2;";
        $lmquery = "SELECT * FROM `tbl_model_pose`  where chin_position IN ('chin-up','chin-straight') AND arm_shape IN ('forearmmovement','bothstraight') AND  m_id='$mid'  AND mpose_id IN($mod_pose_id) AND pose_status!='1'AND `pose_ang` ='3' ORDER BY RAND() LIMIT 0,1;";
        $bmquery = "SELECT * FROM `tbl_model_pose`  where chin_position IN ('chin-up','chin-straight') AND arm_shape IN ('forearmmovement','bothstraight') AND  m_id='$mid'  AND mpose_id IN($mod_pose_id) AND pose_status!='1'AND `pose_ang` ='2' ORDER BY RAND() LIMIT 0,1;";
        $rmquery = "SELECT * FROM `tbl_model_pose`  where chin_position IN ('chin-up','chin-straight') AND arm_shape IN ('forearmmovement','bothstraight') AND  m_id='$mid'  AND mpose_id IN($mod_pose_id) AND pose_status!='1'AND `pose_ang` ='4' ORDER BY RAND() LIMIT 0,1;";
      }

      if($sleeve=='FULL SLEEVE' || $sleeve=='BELOW ELBOW SLEEVE' || $sleeve=='3/4th SLEEVE'){
        $mquery = "SELECT * FROM `tbl_model_pose`  where chin_position IN ('chin-up','chin-straight') AND arm_shape IN ('bothstraight') AND  m_id='$mid'  AND mpose_id IN($mod_pose_id) AND pose_status!='1'AND `pose_ang` ='1' ORDER BY RAND() LIMIT 0,2;";
        $lmquery = "SELECT * FROM `tbl_model_pose`  where chin_position IN ('chin-up','chin-straight') AND arm_shape IN ('bothstraight') AND  m_id='$mid'  AND mpose_id IN($mod_pose_id) AND pose_status!='1'AND `pose_ang` ='3' ORDER BY RAND() LIMIT 0,1;";
        $bmquery = "SELECT * FROM `tbl_model_pose`  where chin_position IN ('chin-up','chin-straight') AND arm_shape IN ('bothstraight') AND  m_id='$mid'  AND mpose_id IN($mod_pose_id) AND pose_status!='1'AND `pose_ang` ='2' ORDER BY RAND() LIMIT 0,1;";
        $rmquery = "SELECT * FROM `tbl_model_pose`  where chin_position IN ('chin-up','chin-straight') AND arm_shape IN ('bothstraight') AND  m_id='$mid'  AND mpose_id IN($mod_pose_id) AND pose_status!='1'AND `pose_ang` ='4' ORDER BY RAND() LIMIT 0,1;";
      }

    }else if($neck=='TURTLE NECK'){

      if($sleeve=='SLEEVELESS'   || $sleeve=='CAP SLEEVE'){
        $mquery = "SELECT * FROM `tbl_model_pose`  where chin_position IN ('chin-up','chin-straight') AND arm_shape IN ('armmovement','forearmmovement','bothstraight') AND  m_id='$mid'  AND mpose_id IN($mod_pose_id) AND pose_status!='1'AND `pose_ang` ='1' ORDER BY RAND() LIMIT 0,2;";
        $lmquery = "SELECT * FROM `tbl_model_pose`  where chin_position IN ('chin-up','chin-straight') AND arm_shape IN ('armmovement','forearmmovement','bothstraight') AND  m_id='$mid'  AND mpose_id IN($mod_pose_id) AND pose_status!='1'AND `pose_ang` ='3' ORDER BY RAND() LIMIT 0,1;";
        $bmquery = "SELECT * FROM `tbl_model_pose`  where chin_position IN ('chin-up','chin-straight') AND arm_shape IN ('armmovement','forearmmovement','bothstraight') AND  m_id='$mid'  AND mpose_id IN($mod_pose_id) AND pose_status!='1'AND `pose_ang` ='2' ORDER BY RAND() LIMIT 0,1;";
        $rmquery = "SELECT * FROM `tbl_model_pose`  where chin_position IN ('chin-up','chin-straight') AND arm_shape IN ('armmovement','forearmmovement','bothstraight') AND  m_id='$mid'  AND mpose_id IN($mod_pose_id) AND pose_status!='1'AND `pose_ang` ='4' ORDER BY RAND() LIMIT 0,1;";
      }

      if($sleeve=='ABOVE ELBOW SLEEVE' || $sleeve=='SHORT SLEEVE'){
        $mquery = "SELECT * FROM `tbl_model_pose`  where chin_position IN ('chin-up','chin-straight') AND arm_shape IN ('forearmmovement','bothstraight') AND  m_id='$mid'  AND mpose_id IN($mod_pose_id) AND pose_status!='1'AND `pose_ang` ='1' ORDER BY RAND() LIMIT 0,2;";
        $lmquery = "SELECT * FROM `tbl_model_pose`  where chin_position IN ('chin-up','chin-straight') AND arm_shape IN ('forearmmovement','bothstraight') AND  m_id='$mid'  AND mpose_id IN($mod_pose_id) AND pose_status!='1'AND `pose_ang` ='3' ORDER BY RAND() LIMIT 0,1;";
        $bmquery = "SELECT * FROM `tbl_model_pose`  where chin_position IN ('chin-up','chin-straight') AND arm_shape IN ('forearmmovement','bothstraight') AND  m_id='$mid'  AND mpose_id IN($mod_pose_id) AND pose_status!='1'AND `pose_ang` ='2' ORDER BY RAND() LIMIT 0,1;";
        $rmquery = "SELECT * FROM `tbl_model_pose`  where chin_position IN ('chin-up','chin-straight') AND arm_shape IN ('forearmmovement','bothstraight') AND  m_id='$mid'  AND mpose_id IN($mod_pose_id) AND pose_status!='1'AND `pose_ang` ='4' ORDER BY RAND() LIMIT 0,1;";
      }

      if($sleeve=='FULL SLEEVE' || $sleeve=='BELOW ELBOW SLEEVE' || $sleeve=='3/4th SLEEVE'){
        $mquery = "SELECT * FROM `tbl_model_pose`  where chin_position IN ('chin-up','chin-straight') AND arm_shape IN ('bothstraight') AND  m_id='$mid'  AND mpose_id IN($mod_pose_id) AND pose_status!='1'AND `pose_ang` ='1' ORDER BY RAND() LIMIT 0,2;";
        $lmquery = "SELECT * FROM `tbl_model_pose`  where chin_position IN ('chin-up','chin-straight') AND arm_shape IN ('bothstraight') AND  m_id='$mid'  AND mpose_id IN($mod_pose_id) AND pose_status!='1'AND `pose_ang` ='3' ORDER BY RAND() LIMIT 0,1;";
        $bmquery = "SELECT * FROM `tbl_model_pose`  where chin_position IN ('chin-up','chin-straight') AND arm_shape IN ('bothstraight') AND  m_id='$mid'  AND mpose_id IN($mod_pose_id) AND pose_status!='1'AND `pose_ang` ='2' ORDER BY RAND() LIMIT 0,1;";
        $rmquery = "SELECT * FROM `tbl_model_pose`  where chin_position IN ('chin-up','chin-straight') AND arm_shape IN ('bothstraight') AND  m_id='$mid'  AND mpose_id IN($mod_pose_id) AND pose_status!='1'AND `pose_ang` ='4' ORDER BY RAND() LIMIT 0,1;";
      }

    }else if($neck=='PLUNGE NECK'){

      if($sleeve=='SLEEVELESS' || $sleeve=='CAP SLEEVE'){
        $mquery = "SELECT * FROM `tbl_model_pose`  where chin_position IN ('chin-up','chin-straight','chin-down') AND arm_shape IN ('armmovement','forearmmovement','bothstraight') AND  m_id='$mid'  AND mpose_id IN($mod_pose_id) AND pose_status!='1'AND `pose_ang` ='1' ORDER BY RAND() LIMIT 0,2;";
        $lmquery = "SELECT * FROM `tbl_model_pose`  where chin_position IN ('chin-up','chin-straight','chin-down') AND arm_shape IN ('armmovement','forearmmovement','bothstraight') AND  m_id='$mid'  AND mpose_id IN($mod_pose_id) AND pose_status!='1'AND `pose_ang` ='3' ORDER BY RAND() LIMIT 0,1;";
        $bmquery = "SELECT * FROM `tbl_model_pose`  where chin_position IN ('chin-up','chin-straight','chin-down') AND arm_shape IN ('armmovement','forearmmovement','bothstraight') AND  m_id='$mid'  AND mpose_id IN($mod_pose_id) AND pose_status!='1'AND `pose_ang` ='2' ORDER BY RAND() LIMIT 0,1;";
        $rmquery = "SELECT * FROM `tbl_model_pose`  where chin_position IN ('chin-up','chin-straight','chin-down') AND arm_shape IN ('armmovement','forearmmovement','bothstraight') AND  m_id='$mid'  AND mpose_id IN($mod_pose_id) AND pose_status!='1'AND `pose_ang` ='4' ORDER BY RAND() LIMIT 0,1;";
      }

      if($sleeve=='ABOVE ELBOW SLEEVE' || $sleeve=='SHORT SLEEVE'){
        $mquery = "SELECT * FROM `tbl_model_pose`  where chin_position IN ('chin-up','chin-straight','chin-down') AND arm_shape IN ('forearmmovement','bothstraight') AND  m_id='$mid'  AND mpose_id IN($mod_pose_id) AND pose_status!='1'AND `pose_ang` ='1' ORDER BY RAND() LIMIT 0,2;";
        $lmquery = "SELECT * FROM `tbl_model_pose`  where chin_position IN ('chin-up','chin-straight','chin-down') AND arm_shape IN ('forearmmovement','bothstraight') AND  m_id='$mid'  AND mpose_id IN($mod_pose_id) AND pose_status!='1'AND `pose_ang` ='3' ORDER BY RAND() LIMIT 0,1;";
        $bmquery = "SELECT * FROM `tbl_model_pose`  where chin_position IN ('chin-up','chin-straight','chin-down') AND arm_shape IN ('forearmmovement','bothstraight') AND  m_id='$mid'  AND mpose_id IN($mod_pose_id) AND pose_status!='1'AND `pose_ang` ='2' ORDER BY RAND() LIMIT 0,1;";
        $rmquery = "SELECT * FROM `tbl_model_pose`  where chin_position IN ('chin-up','chin-straight','chin-down') AND arm_shape IN ('forearmmovement','bothstraight') AND  m_id='$mid'  AND mpose_id IN($mod_pose_id) AND pose_status!='1'AND `pose_ang` ='4' ORDER BY RAND() LIMIT 0,1;";
      }

      if($sleeve=='FULL SLEEVE' || $sleeve=='BELOW ELBOW SLEEVE' || $sleeve=='3/4th SLEEVE'){
        $mquery = "SELECT * FROM `tbl_model_pose`  where chin_position IN ('chin-up','chin-straight','chin-down') AND arm_shape IN ('bothstraight') AND  m_id='$mid'  AND mpose_id IN($mod_pose_id) AND pose_status!='1'AND `pose_ang` ='1' ORDER BY RAND() LIMIT 0,2;";
        $mquery = "SELECT * FROM `tbl_model_pose`  where chin_position IN ('chin-up','chin-straight','chin-down') AND arm_shape IN ('bothstraight') AND  m_id='$mid'  AND mpose_id IN($mod_pose_id) AND pose_status!='1'AND `pose_ang` ='3' ORDER BY RAND() LIMIT 0,1;";
        $mquery = "SELECT * FROM `tbl_model_pose`  where chin_position IN ('chin-up','chin-straight','chin-down') AND arm_shape IN ('bothstraight') AND  m_id='$mid'  AND mpose_id IN($mod_pose_id) AND pose_status!='1'AND `pose_ang` ='2' ORDER BY RAND() LIMIT 0,1;";
        $mquery = "SELECT * FROM `tbl_model_pose`  where chin_position IN ('chin-up','chin-straight','chin-down') AND arm_shape IN ('bothstraight') AND  m_id='$mid'  AND mpose_id IN($mod_pose_id) AND pose_status!='1'AND `pose_ang` ='4' ORDER BY RAND() LIMIT 0,1;";
      }

    }

  }
  ///  Top End ///
?>