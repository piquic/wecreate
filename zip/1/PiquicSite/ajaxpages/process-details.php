<?php
session_start();
include('config.php');


$user_id = $_SESSION['user_id'];
$user_name = $_SESSION['user_name'];


if(isset($_POST['style_id'])){
	$style_id = $_POST['style_id'];
	$ppqry = "SELECT * FROM `tbl_style` WHERE `status` = '2' and `sty_id`=$style_id";
//print_r($ppqry);
	$ppres   = $second_DB->query($ppqry);
	$row_cnt = $ppres->num_rows;

	if ($row_cnt>0){
		$i=1;
		while($pprow = $ppres->fetch_assoc()){
			$sty_id      = $pprow['sty_id'];
			$user_id     = $pprow['user_id'];
			$skuno       = $pprow['skuno'];
			$category    = $pprow['category'];
			$bg          = $pprow['bg'];

			$model_id    = $pprow['model_id'];
			$modbody     = $pprow['modbody'];
			$poseids     = $pprow['poseids'];

			$top         = $pprow['top'];
			$bottom      = $pprow['bottom'];
			$shoes       = $pprow['shoes'];
			$accessories = $pprow['accessories'];

			$lipcol      = $pprow['lipstick'];
			$lipefft     = $pprow['lipstick_effrt'];
			$eyemkup     = $pprow['eyemkup'];
			$eyeshcol    = $pprow['eyeshadowcol'];
			$eyeint      = $pprow['eyeintensity'];
			$blshcol     = $pprow['blushcolcde'];
			$blshint     = $pprow['blushintensity'];

			$comments    = $pprow['comments'];
			$status_type = $pprow['status_type'];

			if(!empty($pprow['source_x']))
			{
				$source_x = $pprow['source_x'];
			}
			else{
				$source_x = 0;
			}

			if(!empty($pprow['source_y']))
			{
				$source_y = $pprow['source_y'];
			}
			else{
				$source_y = 0;
			}

			if(!empty($pprow['source_w']))
			{
				$source_w = $pprow['source_w'];
			}
			else{
				$source_w = 0;
			}

			if(!empty($pprow['source_h']))
			{
				$source_h = $pprow['source_h'];
			}
			else{
				$source_h = 0;
			}

			if(!empty($pprow['source_angle']))
			{
				$source_angle = $pprow['source_angle'];
			}
			else{
				$source_angle = 0;
			}

			$mpqry       = "SELECT * FROM `tbl_model_pose` WHERE `m_id` = '$model_id' AND `mpose_img` LIKE '%MSTR%'";
			$mpres       = $mysqli->query($mpqry);
			$mprow       = $mpres->fetch_assoc();
			$mpose_img   = $mprow['mpose_img'];

			$mdlqry   = "SELECT * FROM `tbl_model` WHERE `m_id` = '$model_id'";
			$mdlres   = $mysqli->query($mdlqry);
			$mdlrow   = $mdlres->fetch_assoc();
			$mod_name = $mdlrow['mod_name'];
			$mod_img  = $mdlrow['mod_img'];

			$mbqry      = "SELECT * FROM `tbl_model` WHERE `mod_name` = '$modbody'";
			$mbres      = $mysqli->query($mbqry);
			$mbrow      = $mbres->fetch_assoc();
			$mbmod_img  = $mbrow['mod_img'];

			$acqry       = "SELECT * FROM `tbl_accessories` WHERE `acces_id` IN ($accessories)";
			$acres       = $mysqli->query($acqry);
			$ac_cnt      = $acres->num_rows;

			$acc = array();
			if ($ac_cnt > 0){
				while($acrow = $acres->fetch_assoc()){
					$acces_img = $acrow['acces_img'];

					array_push($acc, $acces_img);
				}
			}

			$bracelet = '';
			$earring = '';
			$necklace = '';
			$ring = '';

			foreach ($acc as $val) {
				if(strpos($val, "-ACCBR")) {
					$bracelet = $val;
				}
				if(strpos($val, "-ACCER")) {
					$earring = $val;
				}
				if(strpos($val, "-ACCNC")) {
					$necklace = $val;
				}
				if(strpos($val, "-ACCRNG")) {
					$ring = $val;
				}
			}

			if($lipcol != '' || $lipefft != '') {
				if($lipefft >= '0' && $lipefft < '35') {
					$lipefft = "MATTE";
				} else if($lipefft >= '35' && $lipefft <= '65') {
					$lipefft = "SATIN";
				} else if($lipefft > '65' && $lipefft <= '100') {
					$lipefft = "GLOSSY";
				}
				$lipstkimg   = $mod_name."-MSTR-".$lipcol."-".$lipefft.".png";
			}

			if($eyeshcol != ''){
				$eyeshdimg   = $mod_name."-MSTR-".$eyeshcol.".png";
			}

			if($eyemkup != '') {
				if($eyemkup >= '0') {
					$eyemkup = "NATURAL";
				} else if($eyemkup >= '40') {
					$eyemkup = "MEDIUM";
				} else if($eyemkup >= '80') {
					$eyemkup = "SMOKY";
				}
				$eyemkpimg   = $mod_name."-MSTR-".$eyemkup.".png";
			}

			if($blshcol != ''){
				$blushimg   = $mod_name."-MSTR-".$blshcol.".png";
			}

			if($cutout_manu == '1'){
				$pcqry       = "SELECT * FROM `tbl_upload_img` WHERE `zipfile_name` = '$skuno'";
				$pcres       = $second_DB->query($pcqry);
				$pcrow       = $pcres->fetch_assoc();
				$cutout_flag = $pcrow['cutout_flag'];
				$cutout_manu = $pcrow['cutout_manual'];

				$imgdirname = PATH_CUTOUTPNG . $skuno."\\";
				$images = glob($imgdirname."*.{png,PNG}",GLOB_BRACE);
				// $image= array_shift(array_values($images));
				// print_r($images);

				foreach ($images as $image) {
					// echo $modbody;
					if(strpos($image, "-".strtoupper($modbody)."-")){
						$src_img = $image;
					}
				}
				
				// echo $src_img;
				$arr=explode("/",$src_img);
				$arrval= end($arr);

				$src_img = file_get_contents($arrval);
				$image_codes = base64_encode($src_img);
			}
?>

<script type="text/javascript">
	function viewtag( upimg_id ) {
		// alert("Hi viewtag fun called....");
		// alert('#list-id'+upimg_id);
		// get the tag list with action remove and tag boxes and place it on the image.
      $.post( "<?php echo PIQUIC_AJAX_URL ;?>/taglist.php" ,  "upimg_id=" + upimg_id, function( data ) {
      	// console.log(data.lists);
      	// alert(data.lists);
      	$('#list-id'+upimg_id).empty();
      	// $('#list-id'+upimg_id+' tr').remove();
	      $('#list-id'+upimg_id).append(data.lists);
	      $('#tagbox'+upimg_id).empty();
	      $('#tagbox'+upimg_id).append(data.boxes);
   	}, "json");  
   }
</script>

<div class="row">
	<!-- <div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
		<h5>Raw Images</h5>
	</div> -->
	<div id="append_<?php echo $skuno; ?>">
		<div class="row">
			<div class="col-12 col-sm-12 col-md-6 col-lg-6 col-xl-6 order-sm-2">
				<h5>Raw Images</h5>
				<hr>

				<div class="row">
					<div class="col-3 col-sm-3 col-md-3 col-lg-3 col-xl-3 scroll-box">
						<?php
						$imgqry = "SELECT upimg_id FROM `tbl_upload_img` WHERE `zipfile_name` = '$skuno'";
						$imgres   = $second_DB->query($imgqry);
						$imgrow   = $imgres->fetch_assoc();
						$upimg_id = $imgrow['upimg_id'];

						$imagqry = "SELECT * FROM `tbl_upload_images` WHERE `upimg_id` = '$upimg_id'";

						$imagres   = $second_DB->query($imagqry);
						$row_cnt = $imagres->num_rows;
						if ($row_cnt>0){
							while($imagrow = $imagres->fetch_assoc()){
								$imgid = $imagrow['id'];
								$img_name = $imagrow['img_name'];
						?>
						<script type="text/javascript">
							$(window).on('load', function(){
								var img = document.getElementById('img_<?php echo $imgid; ?>');
								var width = img.naturalWidth;
								var height = img.naturalHeight;
								if (width < height){
									$('#img_<?php echo $imgid; ?>' ).removeClass('img').addClass('img-fluid');
								}
							});
						</script>
						<div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12 py-1">
							<img class="w-100 border img  " id="img_<?php echo $imgid; ?>" src="<?php echo PIQUIC_MAIN_URL ;?>upload/<?php echo $user_id?>/<?php echo $skuno?>/thumb_<?php echo $img_name; ?>" onclick="useimg('<?php echo $imgid; ?>','<?php echo $img_name; ?>', '<?php echo $skuno; ?>')"/ >
						</div>
						<?php } } ?>
					</div>
					<div class="col-9 col-sm-9 col-md-9 col-lg-9 col-xl-9">
						<?php
						$imgqry = "SELECT upimg_id FROM `tbl_upload_img` WHERE `zipfile_name` = '$skuno'";
						$imgres   = $second_DB->query($imgqry);
						$imgrow   = $imgres->fetch_assoc();
						$upimg_id = $imgrow['upimg_id'];

						$imagqry = "SELECT * FROM `tbl_upload_images` WHERE `upimg_id` = '$upimg_id'";

						$imagres   = $second_DB->query($imagqry);
						$row_cnt = $imagres->num_rows;
						$imagrow = $imagres->fetch_assoc();
						$imgid = $imagrow['id'];
						$img_name = $imagrow['img_name'];
						?>

						<script type="text/javascript">
							$(window).on('load', function(){
								var img = document.getElementById('img_<?php echo $imgid; ?>');
								var width = img.naturalWidth;
								var height = img.naturalHeight;
								if (width < height){
									$('#imgtag_<?php echo $imgid; ?>' ).removeClass('img').addClass('img-fluid');
								}
							});

						</script>
						<div id='imgtag_<?php echo $imgid; ?>' class="imgtag py-1">
							<img id="main_img_<?php echo $skuno; ?>" class="w-100 border main_img img  " src="<?php echo PIQUIC_MAIN_URL ;?>upload/<?php echo $user_id?>/<?php echo $skuno?>/thumb_<?php echo $img_name; ?>"  >
							<div id="tagbox<?php echo $imgid; ?>" class="tagbox">
							</div>
						</div>
					</div>
				</div>

				<div class="row">
					<div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
						<div class="taglist" id="taglist<?php echo $imgid; ?>" > 
							<table class="table table-striped" >
								<thead>
									<tr>
										<th>ID</th>
										<th>Annotation</th>

									</tr>
								</thead>

								<tbody id="list-id<?php echo $imgid; ?>" class="list-id">
									<script type="text/javascript">
										$(document).ready(function(){
											viewtag(<?php echo $imgid; ?>);
										});
									</script>

								</tbody>
							</table>
							<!--<ol id="list-id<?php echo $imgid; ?>" class="list-id">
							</ol>  -->
						</div>
					</div>
				</div>
			</div>

			<style type="text/css">
				.bmw { width: 72.66%; }
				.pmw { width: 100%; }

				@media (min-width: 1024px) {
					.accpbg { width: 4em; height: 5.5em; top: 3.55em; right: 8em; background-repeat: no-repeat !important; background-position: top right !important; background-size: 24em !important; }
				}
				@media (min-width: 768px) and (max-width: 1023px) {
					.accpbg { width: 5em; height: 6em; top: 3.6em; right: 8.462em; background-repeat: no-repeat !important; background-position: top right !important; background-size: 23.1em !important; }
				}
			</style>

			<div class="col-12 col-sm-12 col-md-6 col-lg-6 col-xl-6 order-sm-1">
				<h5>Final Preview</h5>
				<hr>
				<input type="hidden" name="source_x" id="source_x" value="<?php echo $source_x;?>" />
				<input type="hidden" name="source_y" id="source_y" value="<?php echo $source_y;?>" />

				<input type="hidden" name="source_w" id="source_w" value="<?php echo $source_w;?>" />
				<input type="hidden" name="source_h" id="source_h" value="<?php echo $source_h;?>" />

				<input type="hidden" name="source_angle" id="source_angle" value="<?php echo $source_angle;?>" />

				<div class="" style="height: 34em;">
					<div id="bg" class="bmw">
						<img id="bg-big" class="pmw <?php if($bg=="bg-white.jpg"){ echo 'shadow'; } ?>" src="<?php echo PIQUIC_URL;?>images/bg/<?php echo $bg;?>">
					</div>

					<div id="pose" class="bmw">
						<img id="pose-big" class="pmw" src="<?php echo PIQUIC_URL;?>images/model_pose/<?php echo $mpose_img;?>">
					</div>

					<div id="top" class="bmw">
						<?php //header("Content-type: text/css; charset: UTF-8"); ?>
						<?php if($category == "bottom" && ($top != "cutoutimg.png" || $top != null)) { ?>
							<img class="pmw" src="<?php echo PIQUIC_URL;?>images/top/<?php echo $top;?>">
						<?php } else if($category == "top" && $cutout_flag == 1 && $cutout_manu == 0) { ?>
							<div style="background-image: url(<?php echo PIQUIC_SITE_URL;?>upload/<?php echo $user_id;?>/<?php echo $skuno;?>/cutoutimg.png); height: 20em; background-repeat: no-repeat; background-size: calc(80px + <?php echo $source_w; ?>px) calc(121px + <?php echo $source_h; ?>px); background-position: calc(1px + <?php echo $source_x;?>px) calc(9px + <?php echo $source_y;?>px); transform: rotate(calc(2deg + <?php echo $source_angle;?>deg));">&nbsp;</div>
						<?php } else if($category == "top" && $cutout_manu == 1) { ?>
							<img id="top-big" class="pmw" src="data:image/jpg;charset=utf-8;base64,<?php echo $image_codes;?>" >
						<?php } ?>
					</div>

					<div id="bottom" class="bmw">
						<?php if($category == "top" && ($bottom != "cutoutimg.png" || $bottom != null)) { ?>
							<img class="pmw" src="<?php echo PIQUIC_URL;?>images/bottom/<?php echo $bottom;?>">
						<?php } else if($category == "bottom" && $cutout_flag == 1 && $cutout_manu == 0) { ?>
							<!-- <img id="bottom-big" class="pmw" src="upload/<?php echo $user_id;?>/<?php echo $skuno;?>/cutoutimg.png"> -->
							<div style="background-image: url(<?php echo PIQUIC_SITE_URL;?>upload/<?php echo $user_id;?>/<?php echo $skuno;?>/cutoutimg.png); height: 35em; background-repeat: no-repeat; background-size: calc(72px + <?php echo $source_w; ?>px) calc(70px + <?php echo $source_h; ?>px); background-position: calc(10*<?php echo $source_x;?>px) calc(5*<?php echo $source_y;?>px); transform: rotate(<?php echo $source_angle;?>deg);">&nbsp;</div>
						<?php } else if($category == "bottom" && $cutout_manu == 1) { ?>
							<img id="bottom-big" class="pmw" src="data:image/jpg;charset=utf-8;base64,<?php echo $image_codes;?>">
						<?php } ?>
					</div>

					<?php if($category == "dress") { ?>
					<div id="dress" class="bmw">
						<?php if($cutout_flag == 1 && $cutout_manu == 0){ ?>
							<img id="dress-big" class="pmw" src="upload/<?php echo $user_id;?>/<?php echo $skuno;?>/cutoutimg.png">
						<?php } else if($cutout_manu == 1){ ?>
							<img id="dress-big" class="pmw" src="data:image/jpg;charset=utf-8;base64,<?php echo $image_codes;?>">
						<?php } ?>
					</div>
					<?php } ?>

					<?php if($accessories) { ?>
						<?php if($ring || $earring || $necklace || $bracelet) { ?>
						<div id="access" class="accpbg" style="background: url(<?php echo PIQUIC_URL;?>images/bg/<?php echo $bg;?>);"></div>
						<?php } ?>

						<?php if($ring) { ?>
						<div id="accrn" class="bmw">
							<img id="accrn-big" class="pmw" src="<?php echo PIQUIC_URL;?>images/accessories/<?php echo $ring;?>">
						</div>
						<?php } ?>

						<?php if($earring) { ?>
						<div id="accer" class="bmw">
							<img id="accer-big" class="pmw" src="<?php echo PIQUIC_URL;?>images/accessories/<?php echo $earring;?>">
						</div>
						<?php } ?>

						<?php if($necklace) { ?>
						<div id="accnc" class="bmw">
							<img id="accnc-big" class="pmw" src="<?php echo PIQUIC_URL;?>images/accessories/<?php echo $necklace;?>">
						</div>
						<?php } ?>

						<?php if($bracelet) { ?>
						<div id="accbr" class="bmw">
							<img id="accbr-big" class="pmw" src="<?php echo PIQUIC_URL;?>images/accessories/<?php echo $bracelet;?>">
						</div>
						<?php } ?>
					<?php } ?>

					<div id="foot" class="bmw">
						<img id="foot-big" class="pmw" src="<?php echo PIQUIC_URL;?>images/foot/<?php echo $shoes;?>">
					</div>

					<?php if($lipcol != '' || $lipefft != '') { ?>
					<div id="lipstick" class="bmw">
						<img id="lipstick-big" class="pmw" src="<?php echo PIQUIC_URL;?>images/lipstick/<?php echo $lipstkimg;?>">
					</div>
					<?php } ?>

					<?php if($eyeshcol != ''){ ?>
					<div id="eyeshd" class="bmw">
						<img id="eyeshd-big" class="pmw" style="opacity: <?php echo $eyeintensity?>%;" src="<?php echo PIQUIC_URL;?>images/eyeshadow/<?php echo $eyeshdimg;?>">
					</div>
					<?php } ?>

					<?php if($eyemkup != '') { ?>
					<div id="eyemkp" class="bmw">
						<img id="eyemkp-big" class="pmw" src="<?php echo PIQUIC_URL;?>images/eyemakeup/<?php echo $eyemkpimg;?>">
					</div>
					<?php } ?>

					<?php if($blshcol != ''){ ?>
					<div id="blush" class="bmw">
						<img id="blush-big" class="pmw" style="opacity: <?php echo $blushintensity?>%;" src="<?php echo PIQUIC_URL;?>images/blush/<?php echo $blushimg;?>">
					</div>
					<?php } ?>
				</div>
			</div>
		</div>
	</div>
</div>


<?php } ?>
<?php $i++; }


}
else
{
	echo "Style id is not getting" .$_POST['style_id'];
}

?>

<script type="text/javascript">




	function useimg(id,src,sku){
        // alert("clicked");
        var imgurl ="<?php echo PIQUIC_MAIN_URL ;?>/upload/<?php echo $user_id ;?>/"+sku+"/thumb_"+src;
        var skuno = sku;
        var imgtagid="imgtag_"+id;
        var tagbox="tagbox"+id;
        var taglist="taglist"+id;
        var list_id="list-id"+id;

        $('#main_img_'+skuno).attr('src', imgurl);
      //  $('#main_img'+skuno).attr('onchange', 'viewtag('+id+')');
       //alert(taglist);
       $(".imgtag").attr('id',imgtagid);
       $(".tagbox").attr('id',tagbox);
       $(".taglist").attr('id',taglist);
       $(".list-id").attr('id',list_id);
       viewtag(id);
       // alert(id);

      // get the tag list with action remove and tag boxes and place it on the image.
      // $.post( "taglist.php" ,  "upimg_id=" + id, function( data ) {
      //   console.log(data);
      //   $('#taglist_'+id+' ol').html(data.lists);
      //   $('#tagbox'+id).html(data.boxes);
      // }, "json");

   }


    //   function viewtag( upimg_id )
    //   {

    //  // alert("Hi viewtag fun called....");

    //  //alert('#list-id'+upimg_id);
    //   // get the tag list with action remove and tag boxes and place it on the image.
    //   $.post( "<?php echo PIQUIC_AJAX_URL ;?>/taglist.php" ,  "upimg_id=" + upimg_id, function( data ) {
    //   // console.log(data.lists);
    //   //alert(data.lists);
    //   $('#list-id'+upimg_id).empty();
    //   // $('#list-id'+upimg_id+' tr').remove();
    //   $('#list-id'+upimg_id).append(data.lists);
    //   $('#tagbox'+upimg_id).empty();
    //   $('#tagbox'+upimg_id).append(data.boxes);
    //   }, "json");

    // }  

    // function tag_list_over(id){
    //  $('#view_' + id).css({ opacity: 1.0 });
    // } 

    $(document).ready(function(){

    	var counter = 0;
    	var mouseX = 0;
    	var mouseY = 0;


  // // Save button click - save tags
  //   $( document ).on( 'click',  '#tagit #btnsave', function(e){

  //     name = $('#tagname').val();
  //     tagid=$('#tag_id').val();
  //     alert(name);
  //            alert(tagid);
  //   var img = $('#imgtag').find( 'img' );

  //   var id = $('img[id^="sku_no_"]').attr( 'id' );
  
  //     $.ajax({
  //       type: "POST", 
  //       url: "savetag.php", 
  //       data: "upimg_id=" + tagid + "&name=" + name + "&pic_x=" + mouseX + "&pic_y=" + mouseY + "&type=insert",
  //       cache: true, 
  //       success: function(data){
  //         //alert(data);
  //         viewtag( id );
  //         $('#tagit').fadeOut();
  //       }
  //     });

  //   });

  // Cancel the tag box.
  $( document ).on( 'click', '#tagit #btncancel', function() {
  	$('#tagit').fadeOut();
  });

  // mouseover the taglist 
  $('div[id^="taglist"]').on( 'mouseover', 'tr', function( ) {
  	id = $(this).attr("id");
  //alert(id);
  $('#view_' + id).css({ opacity: 1.0 });
}).on( 'mouseout', 'tr', function( ) {
	$('#view_' + id).css({ opacity: 0.0 });
});

  // mouseover the tagboxes that is already there but opacity is 0.
  $( 'div[id^="tagbox"]').on( 'mouseover', '.tagview', function( ) {
  	var pos = $( this ).position();
    $(this).css({ opacity: 1.0 }); // div appears when opacity is set to 1.
 }).on( 'mouseout', '.tagview', function( ) {
    $(this).css({ opacity: 0.0 }); // hide the div by setting opacity to 0.
 });

  // // Remove tags.
  //   $('div[id^="taglist_"]').on('click', '.remove', function() {
  //     id = $(this).parent().attr("id");
  //     // Remove the tag
  //   $.ajax({
  //       type: "POST", 
  //       url: "savetag.php", 
  //       data: "tag_id=" + id + "&type=remove",
  //       success: function(data) {
  //     var img = $('#imgtag').find( 'img' );
  //     var id = $( img ).attr( 'id' );
  //     //get tags if present
  //     viewtag( id );
  //       }
  //     });
  //   });
  
  // load the tags for the image when page loads.
  var img = $('#imgtag').find( 'img' );
  var id = $( img ).attr( 'id' );
  
  //viewtag( id ); // view all tags available on page load
 // function viewtag( upimg_id )
 //    {
 //      alert("Hi viewtag fun called....");
 //      // get the tag list with action remove and tag boxes and place it on the image.
 //      $.post( "taglist.php" ,  "upimg_id=" + upimg_id, function( data ) {
 //        $('#list-id'+ upimg_id).empty();
 //        $('#list-id'+ upimg_id).html(data.lists);
 //        $('#tagbox'+upimg_id).html(data.boxes);

 //      }, "json");

 //    }  

});

      $('document').ready(function(){
      	//openImgRotDrgRes();
      });
       function openImgRotDrgRes(){

  	var click_check=$("#click_check").val();

	//alert(click_check);

	if($("#source_angle").val()!='')
	{
		var degrees=$("#source_angle").val(); 
	}
	else
	{
		var degrees= false;
	}


	if($("#source_x").val()!='' && $("#source_y").val()!='' )
	{
		var sourceX = $("#source_x").val();
		var sourceY = $("#source_y").val(); 
	}
	else
	{
		var sourceX = 0;
		var sourceY = 0; 
	}


	if($("#source_w").val()!='' && $("#source_h").val()!='' )
	{
		var startW = $("#source_w").val();
		var startH = $("#source_h").val(); 
	}
	else
	{
		var startW = 0;
		var startH = 0; 
	}

	$('#top').rotatable({

		degrees: degrees,
		create: function() {
			if(startW!='0' || startH!='0')
			{
				$("#top").css("width", startW+'px');
				$("#top").css("height", startH+'px');
			}
			if(sourceX!='0' || sourceY!='0')
			{
				$("#top").css("left", sourceX+'px');
				$("#top").css("top", sourceY+'px');
			}

		},   
		stop: function(event, ui){
			if(ui.angle.current<0){
				var given_angle=ui.angle.current+2*Math.PI;
			}
			else{ var given_angle=ui.angle.current;  }
			var new_angle=Math.round(given_angle*180/ Math.PI)+"deg";
			$("#source_angle").val(Math.round(given_angle*180/ Math.PI));
			$("#top").css("transform","rotate("+new_angle+")");
		} 


	}).draggable({
		degrees: degrees,	
		create: function() {
			if(startW!='0' || startH!='0')
			{
				$("#top").css("width", startW+'px');
				$("#top").css("height", startH+'px');
			}
			if(sourceX!='0' || sourceY!='0')
			{
				$("#top").css("left", sourceX+'px');
				$("#top").css("top", sourceY+'px');
			}

		},
		stop:function(event,ui) {
			var wrapper = $("#reswrapper").offset();
			var borderLeft = parseInt($("#reswrapper").css("border-left-width"),10);
			var borderTop = parseInt($("#reswrapper").css("border-top-width"),10);
			var pos = ui.helper.offset();
			$("#source_x").val(pos.left - wrapper.left - borderLeft);
			$("#source_y").val(pos.top - wrapper.top - borderTop);
		//alert($("#source_x").val() + "," + $("#source_y").val());

		//var top = $("#resizable").position().top;
		//var left = $("#resizable").position().left;

		//$("#source_x").val(top);
		//$("#source_y").val(left);
	}
}).resizable({
	handles: 'ne, se, sw, nw',	
	degrees: degrees,
	create: function() {

		if(startW!='0' || startH!='0')
		{
			$("#top").css("width", startW+'px');
			$("#top").css("height", startH+'px');
		}
		if(sourceX!='0' || sourceY!='0')
		{
			$("#top").css("left", sourceX+'px');
			$("#top").css("top", sourceY+'px');
		}

		
	},  
    //Other options
    start : function(event,ui) {
    	if($("#source_w").val()!='' && $("#source_h").val()!='' )
    	{
    		startW = $("#source_w").val();
    		startH = $("#source_h").val();
    	}
    	else
    	{
    		startW = $("#reswrapper").outerWidth();
    		startH = $("#reswrapper").outerHeight();
    	}

    },
    stop : function(event,ui) {
    	endW = $("#reswrapper").outerWidth();
    	endH = $("#reswrapper").outerHeight();

			/*var width = $(event.target).width();
			var height = $(event.target).height();*/

			$("#source_w").val(endW);
			$("#source_h").val(endH);
	        //alert("width changed:"+ (endW-startW) + " -- Height changed:" + (endH-endW));
	      }
	    });


// if(click_check=='')
// {
// 	$("#top").rotatable('destroy').removeClass("rotate"); 	
// 	$("#top").draggable('destroy').removeClass("drag");
// 	$("#top").resizable('destroy').removeClass("resize");

// }
// else
// {

// }


}
   </script> 