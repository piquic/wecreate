<?php   
session_start();
include('config.php');  
$client_name = $_POST['client_name'];
$status = $_POST['status'];
if($status=='2'){
	$usedate = $_POST['usedate'];
} 
$today= date("d-m-Y");
$autoquery = "SELECT * FROM `tbl_autopose`  WHERE `client_name`='$client_name' ;";
$autoresult=$mysqli->query($autoquery);
$row_cnt = $autoresult->num_rows; 
$autorow=$autoresult->fetch_assoc();
$autom_id=$autorow['model_id'];
$mod_pose_id=$autorow['mod_pose_id'];
$Sekcje = explode( ',',$autom_id );
$poseSekcje = explode(',',$mod_pose_id );
$poseSekcje=explode(",", ("pose_".implode(",pose_", $poseSekcje)));
$poseSekcje = implode(",",$poseSekcje);
$mod_skin_tone=$autorow['mod_skin_tone'];
$mst = explode( ',',$mod_skin_tone );

?>

<input type="hidden" name="model[]" id="model" value="<?php echo $autom_id; ?>"/>
<input type="hidden" name="modelpose[]" id="modelpose" value="<?php echo $poseSekcje; ?>"/>
<input type="hidden" id="skintoneval" name="skintoneval[]" value="<?php echo $mod_skin_tone; ?>">

<div class="col-md-2">
	<p class="lead font-weight-bold">Models:</p>
</div>

<div class="col-md-10">					
	<div class="row">

		<?php 
		$modquery = "SELECT * FROM `tbl_model` WHERE status='1';";
		$modresult=$mysqli->query($modquery);
		while($modrow=$modresult->fetch_assoc()){
			$m_id=$modrow['m_id'];
			$mod_name=$modrow['mod_name'];
			$mod_img=$modrow['mod_img'];
			$skintone=$modrow['skintone'];
			$arr=explode(",",$skintone);
			
			$arrjpg = array();

			foreach ($arr as $value) {
				if(strpos($value, '.jpg')){
					array_push($arrjpg, $value);
				}
			}

			if($status=='2'){

				if($usedate=='1'){

					$mkquery = "SELECT SUM(`count`)  AS value_sum FROM `tbl_model_count` where  model_id='$m_id' AND `client_name`='$client_name' AND `use_date`= CURDATE() ;";
				}

				if($usedate=='2'){

					$mkquery = "SELECT SUM(`count`)  AS value_sum FROM `tbl_model_count` where  model_id='$m_id' AND `client_name`='$client_name' AND  DATE(use_date) = CURDATE() - 1;;";
				}
				if($usedate=='3'){

					$mkquery = "SELECT SUM(`count`)  AS value_sum FROM `tbl_model_count` where  model_id='$m_id' AND `client_name`='$client_name' AND  use_date BETWEEN CURDATE() - INTERVAL 7 DAY AND CURDATE() ;";
				}

				if($usedate=='4'){

					$mkquery = "SELECT SUM(`count`)  AS value_sum FROM `tbl_model_count` where  model_id='$m_id' AND `client_name`='$client_name' AND `use_date`BETWEEN CURDATE() - INTERVAL 30 DAY AND CURDATE() ;";
				}

				if($usedate=='5'){

					$mkquery = "SELECT SUM(`count`)  AS value_sum FROM `tbl_model_count` where  model_id='$m_id' AND `client_name`='$client_name' AND YEAR(use_date) = YEAR(CURDATE());  ;";
				}

				if($usedate=='0'){

					$mkquery = "SELECT SUM(`count`)  AS value_sum FROM `tbl_model_count` where  model_id='$m_id' AND `client_name`='$client_name';";
				}

			}else{

				$mkquery = "SELECT SUM(`count`)  AS value_sum FROM `tbl_model_count` where  model_id='$m_id' AND `client_name`='$client_name';";
			}

			$mkresult=$mysqli->query($mkquery);
			$mprow=$mkresult->fetch_assoc();
			$count=$mprow['value_sum'];
			if($count!=''){
				$count=$count;
			}else{
				$count='0';
			}
		?>

		<div class="col-md-2 text-center">
			<div class="mcbox">

				<?php if($autom_id!=''){ ?>

					<input type="checkbox"  class="m-chkbox <?php if (in_array($m_id,$Sekcje)) { ?> activecat <?php }?>" id="<?php echo $m_id; ?>" name="<?php echo $m_id; ?>" onclick="modselFunction(<?php echo $m_id; ?>) "  <?php if (in_array($m_id, $Sekcje)) { ?> checked <?php }?>/>

				<?php }else{ ?>

					<input type="checkbox" class="m-chkbox activecat" id="<?php echo $m_id; ?>" name="<?php echo $m_id; ?>" onclick="modselFunction(<?php echo $m_id; ?>)" checked />

				<?php } ?>

			</div>

			<div class="mlbox">
				<label class="m-chklbl" for="<?php echo $m_id; ?>">&nbsp;</label>
			</div>

			<div class="border rounded-circle mb-2 crop">
				<img class="w-100 rounded-circle" src="<?php echo PIQUIC_URL;?>images/model_image/<?php echo $mod_img; ?>" />
			</div>

			<p class="lead text-uppercase">

				<?php if($autom_id!=''){ ?>
					<?php if (in_array($m_id, $Sekcje)) {?>
						<img style="width: 1.5rem;" src="<?php echo PIQUIC_URL;?>images/icon/sktn.png" data-toggle="collapse" data-target="#m_<?php echo $m_id; ?>">
					<?php   } ?>
				<?php }else{ ?>
					<img style="width: 1.5rem;" src="<?php echo PIQUIC_URL;?>images/icon/sktn.png" data-toggle="collapse" data-target="#m_<?php echo $m_id; ?>">
				<?php  } ?>

				<?php echo $mod_name; ?>
				<br><small>(<?php echo $count;?>)</small>
			</p>
		</div>

		<style type="text/css">
			@media (min-width: 576px){
				#modelfilter .modal-dialog {
					max-width: 1024px;
					margin: 1.75rem auto;
				}
			}
		</style>

		<!-- collapse -->
		<div class="collapse fade bg-white shadow position-fixed" id="m_<?php echo $m_id; ?>" tabindex="-1" role="dialog" aria-labelledby="m_<?php echo $m_id; ?>Title" aria-hidden="true" data-parent="#modelfilter" style="top: 40%; left: 24%; z-index: 111111; padding: 0; width: 63%;" >
			<div class="row p-3">
				<div class="col-md-10">
					<h5 class="text-uppercase" id="m_<?php echo $m_id; ?>Title">Skin Tone for <?php echo $mod_name; ?></h5>
				</div>
				<div class="col-md-2">
					<button type="button" class="close" data-toggle="collapse" data-target="#m_<?php echo $m_id; ?>" aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
				</div>
			</div>
			<div class="row p-3 rdst-group">
				<input type="hidden" id="rdst-value" name="rdst-value">

				<?php 	
				$i='1';
				foreach($arrjpg as $image){								
					?>
					<div class="col-md-3 text-center mb-3 rdst" id="div_<?php echo $m_id.$i; ?>" data-value="<?php echo $image; ?>" >
						<div class="shadow rounded p-1 <?php if (in_array("$image",$mst)) { ?> border <?php } ?>" id="<?php echo $image;?>">
							<img class="w-100" src="<?php echo PIQUIC_URL;?>images/skin_tone/<?php echo $image;?>">
							<small><?php echo $image;?></small>
						</div>
					</div>
					<?php
						$i++;
					} 
					?>
				</div>
			</div>
		<?php } ?>

	</div>
</div>

<script type="text/javascript">
	$(document).ready(function() { 
		var mi =$('#model').val();
		var poid= '<?php echo $mod_pose_id ;?>';
		var cli_name= '<?php echo $client_name ;?>';
		var usedate=$("select[name=usedate]").val();
		if(usedate!='0' ){
			var status='2';
		}else{
			var status='1';
		}
		if(mi!=''){
			$.ajax({
				type:'POST',
				url:'<?php echo PIQUIC_AJAX_URL ;?>posecheck_masterauto.php',
				data:{'mi':mi,'poid':poid,'cli_name':cli_name,'usedate':usedate,'status':status},
				success: function(checkresults) {	

					$("#poselist").html(checkresults);
				}
			});
		}
	});


	function modselFunction(m) {

		var lfckv = document.getElementById(m).checked;

		if(lfckv==true){
			$('#'+m+'').addClass('activecat').removeClass('inactivecat');
		}else{
			$('#'+m+'').addClass('inactivecat').removeClass('activecat');

		}
		var ids = $('.activecat').map(function(i) {
			return this.id;
		});
		$('#model').val(ids.get().join(',')); 
		var mi =$('#model').val();
		var poid= '<?php echo $mod_pose_id ;?>';
		var cli_name= '<?php echo $client_name ;?>';
		var usedate=$("select[name=usedate]").val();
      // alert(usedate);
      if(usedate!='0' ){
      	var status='2';
      }else{
      	var status='1';
      }



      $.ajax({
      	type:'POST',
      	url:'<?php echo PIQUIC_AJAX_URL ;?>posecheck_masterauto.php',
			//data:{'mi':mi,'poid':poid},
			data:{'mi':mi,'poid':poid,'cli_name':cli_name,'usedate':usedate,'status':status},
			success: function(checkresults) {	

				$("#poselist").html(checkresults);
			}
		});
   }

   $('.rdst-group .rdst').click(function(){
   	$(this).parent().find('div').removeClass('border');
   	$(this).children('div').addClass('border');
   	var val = $(this).attr('data-value');

   	$(this).parent().find('input').val(val);

   	var ids = $('.border').map(function(i) {
   		return this.id;
   	});
   	$('#skintoneval').val(ids.get().join(',')); 
   });

</script>