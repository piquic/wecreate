<?php
include('config.php');


if(isset($_POST['style_id'])){
	$style_id = $_POST['style_id'];
	// $style_id = 11;

	$pvqry = "SELECT * FROM `tbl_style` WHERE `sty_id` = $style_id";
	// echo "<br>";
	$pvres   = $second_DB->query($pvqry);
	$row_cnt = $pvres->num_rows;

	if ($row_cnt > 0){
		$i = 1;
		while($pvrow = $pvres->fetch_assoc()){
			$sty_id      = $pvrow['sty_id'];
			$user_id     = $pvrow['user_id'];
			$skuno       = $pvrow['skuno'];
			$category    = $pvrow['category'];
			$bg          = $pvrow['bg'];

			$model_id    = $pvrow['model_id'];
			$modbody     = $pvrow['modbody'];
			$poseids     = $pvrow['poseids'];

			$top         = $pvrow['top'];
			$bottom      = $pvrow['bottom'];
			$shoes       = $pvrow['shoes'];
			$accessories = $pvrow['accessories'];

			$lipcol      = $pvrow['lipstick'];
			$lipefft     = $pvrow['lipstick_effrt'];
			$eyemkup     = $pvrow['eyemkup'];
			$eyeshcol    = $pvrow['eyeshadowcol'];
			$eyeint      = $pvrow['eyeintensity'];
			$blshcol     = $pvrow['blushcolcde'];
			$blshint     = $pvrow['blushintensity'];

			$comments    = $pvrow['comments'];
			$status_type = $pvrow['status_type'];
			$autostyle   = $pvrow['autostyle'];

			$mpqry       = "SELECT * FROM `tbl_model_pose` WHERE `m_id` = '$model_id' AND `mpose_img` LIKE '%MSTR%'";
			$mpres       = $mysqli->query($mpqry);
			$mprow       = $mpres->fetch_assoc();
			$mpose_img   = $mprow['mpose_img'];

			$mdlqry      = "SELECT * FROM `tbl_model` WHERE `m_id` = '$model_id'";
			$mdlres      = $mysqli->query($mdlqry);
			$mdlrow      = $mdlres->fetch_assoc();
			$mod_name    = $mdlrow['mod_name'];
			$mod_img     = $mdlrow['mod_img'];

			$mbqry       = "SELECT * FROM `tbl_model` WHERE `mod_name` = '$modbody'";
			$mbres       = $mysqli->query($mbqry);
			$mbrow       = $mbres->fetch_assoc();
			$mbmod_img   = $mbrow['mod_img'];

			$acqry       = "SELECT * FROM `tbl_accessories` WHERE `acces_id` IN ($accessories)";
			$acres       = $mysqli->query($acqry);
			$ac_cnt      = $pvres->num_rows;

			$acc = array();
			if ($ac_cnt > 0){
				while($acrow = $acres->fetch_assoc()){
					$acces_img = $acrow['acces_img'];

					array_push($acc, $acces_img);
				}
			}

			$bracelet = '';
			$earring = '';
			$necklace = '';
			$ring = '';

			foreach ($acc as $val) {
				if(strpos($val, "-ACCBR")) {
					$bracelet = $val;
				}
				if(strpos($val, "-ACCER")) {
					$earring = $val;
				}
				if(strpos($val, "-ACCNC")) {
					$necklace = $val;
				}
				if(strpos($val, "-ACCRNG")) {
					$ring = $val;
				}
			}

			if($lipcol != '' || $lipefft != '') {
				if($lipefft >= '0' && $lipefft < '35') {
					$lipefft = "MATTE";
				} else if($lipefft >= '35' && $lipefft <= '65') {
					$lipefft = "SATIN";
				} else if($lipefft > '65' && $lipefft <= '100') {
					$lipefft = "GLOSSY";
				}
				$lipstkimg   = $mod_name."-MSTR-".$lipcol."-".$lipefft.".png";
			}

			if($eyeshcol != ''){
				$eyeshdimg   = $mod_name."-MSTR-".$eyeshcol.".png";
			}

			if($eyemkup != '') {
				if($eyemkup >= '0') {
					$eyemkup = "NATURAL";
				} else if($eyemkup >= '40') {
					$eyemkup = "MEDIUM";
				} else if($eyemkup >= '80') {
					$eyemkup = "SMOKY";
				}
				$eyemkpimg   = $mod_name."-MSTR-".$eyemkup.".png";
			}

			if($blshcol != ''){
				$blushimg   = $mod_name."-MSTR-".$blshcol.".png";
			}

			$pcqry       = "SELECT * FROM `tbl_upload_img` WHERE `zipfile_name` = '$skuno'";
			$pcres       = $second_DB->query($pcqry);
			$pcrow       = $pcres->fetch_assoc();
			$cutout_flag = $pcrow['cutout_flag'];
			$cutout_manu = $pcrow['cutout_manual'];

			$imgdirname = PATH_CUTOUTPNG . $skuno."\\";
			$images = glob($imgdirname."*.{png,PNG}",GLOB_BRACE);
			// $image= array_shift(array_values($images));
			// print_r($images);

			foreach ($images as $image) {
				// echo $modbody;
				if(strpos($image, "-".strtoupper($modbody)."-")){
					$src_img = $image;
				}
			}
			
			// echo $src_img;
			$arr=explode("/",$image);
			$arrval= end($arr);

			$image = file_get_contents($arrval);
			$image_codes = base64_encode($image);

?>
<link rel="stylesheet" type="text/css" href="../website-assets/css/custom.css">

<style type="text/css">
	.pmh {
		max-height: 32em;
		/*border: 1px solid green;*/
	}
</style>

<div style="height: 32em;">
	<div id="bg">
		<img id="bg-big" class="pmh" src="<?php echo PIQUIC_URL;?>images/bg/<?php echo $bg;?>">
	</div>

	<div id="pose">
		<img id="pose-big" class="pmh" src="<?php echo PIQUIC_URL;?>images/model_pose/<?php echo $mpose_img;?>">
	</div>

	<div id="top">
		<?php if($category == "bottom" && ($top != "cutoutimg.png" || $top != null)) { ?>
			<img class="pmh" src="<?php echo PIQUIC_URL;?>images/top/<?php echo $top;?>">
		<?php } else if($category == "top" && $cutout_flag == 1 && $cutout_manu == 0) { ?>
			<img id="top-big" class="pmh" src="upload/<?php echo $user_id;?>/<?php echo $skuno;?>/cutoutimg.png">
		<?php } else if($category == "top" && $cutout_manu == 1) { ?>
			<img id="top-big" class="pmh" src="data:image/jpg;charset=utf-8;base64,<?php echo $image_codes;?>">
		<?php } ?>
	</div>

	<div id="bottom">
		<?php if($category == "top" && ($bottom != "cutoutimg.png" || $bottom != null)) { ?>
			<img class="pmh" src="<?php echo PIQUIC_URL;?>images/bottom/<?php echo $bottom;?>">
		<?php } else if($category == "bottom" && $cutout_flag == 1 && $cutout_manu == 0) { ?>
			<img id="bottom-big" class="pmh" src="upload/<?php echo $user_id;?>/<?php echo $skuno;?>/cutoutimg.png">
		<?php } else if($category == "bottom" && $cutout_manu == 1) { ?>
			<img id="bottom-big" class="pmh" src="data:image/jpg;charset=utf-8;base64,<?php echo $image_codes;?>">
		<?php } ?>
	</div>

	<?php if($category == "dress") { ?>
	<div id="dress">
		<?php if($cutout_flag == 1 && $cutout_manu == 0){ ?>
			<img id="dress-big" class="pmh" src="upload/<?php echo $user_id;?>/<?php echo $skuno;?>/cutoutimg.png">
		<?php } else if($cutout_manu == 1){ ?>
			<img id="dress-big" class="pmh" src="data:image/jpg;charset=utf-8;base64,<?php echo $image_codes;?>" style="padding: 0 0 0 .5em">
		<?php } ?>
	</div>
	<?php } ?>

	<?php if($ring || $earring || $necklace || $bracelet) { ?>
	<div id="access" class="accbg" style="background: url(<?php echo PIQUIC_URL;?>images/bg/<?php echo $bg;?>);"></div>
	<?php } ?>

	<?php if($ring) { ?>
	<div id="accrn">
		<img id="accrn-big" class="pmh" src="<?php echo PIQUIC_URL;?>images/accessories/<?php echo $ring;?>">
	</div>
	<?php } ?>

	<?php if($earring) { ?>
	<div id="accer">
		<img id="accer-big" class="pmh" src="<?php echo PIQUIC_URL;?>images/accessories/<?php echo $earring;?>">
	</div>
	<?php } ?>

	<?php if($necklace) { ?>
	<div id="accnc">
		<img id="accnc-big" class="pmh" src="<?php echo PIQUIC_URL;?>images/accessories/<?php echo $necklace;?>">
	</div>
	<?php } ?>

	<?php if($bracelet) { ?>
	<div id="accbr">
		<img id="accbr-big" class="pmh" src="<?php echo PIQUIC_URL;?>images/accessories/<?php echo $bracelet;?>">
	</div>
	<?php } ?>

	<div id="foot">
		<img id="foot-big" class="pmh" src="<?php echo PIQUIC_URL;?>images/foot/<?php echo $shoes;?>">
	</div>

	<?php if($lipcol != '' || $lipefft != '') { ?>
	<div id="lipstick">
		<img id="lipstick-big" class="pmh" src="<?php echo PIQUIC_URL;?>images/lipstick/<?php echo $lipstkimg;?>">
	</div>
	<?php } ?>

	<?php if($eyeshcol != ''){ ?>
	<div id="eyeshd">
		<img id="eyeshd-big" class="pmh" style="opacity: <?php echo $eyeintensity?>%;" src="<?php echo PIQUIC_URL;?>images/eyeshadow/<?php echo $eyeshdimg;?>">
	</div>
	<?php } ?>

	<?php if($eyemkup != '') { ?>
	<div id="eyemkp">
		<img id="eyemkp-big" class="pmh" src="<?php echo PIQUIC_URL;?>images/eyemakeup/<?php echo $eyemkpimg;?>">
	</div>
	<?php } ?>

	<?php if($blshcol != ''){ ?>
	<div id="blush">
		<img id="blush-big" class="pmh" style="opacity: <?php echo $blushintensity?>%;" src="<?php echo PIQUIC_URL;?>images/blush/<?php echo $blushimg;?>">
	</div>
	<?php } ?>
</div>


<?php
		}
	}
}
?>