  <?php   
  error_reporting(0);
  session_start();
  include('config.php');
 $mid = $_POST['mid'];

  // $my_pose_id = $_POST['my_pose_id'];

  $string = $_POST['my_pose_id']; 
  $n=explode('poseID_', $string);
  array_shift($n);  
  $pose = implode('', $n);
  $armpose=array();
  $chinpose=array();
  $armquery = "SELECT DISTINCT `arm_shape` FROM `tbl_model_pose` WHERE `mpose_id` in ($pose) ;";
  $armresult=$mysqli->query($armquery);
  while($armrow=$armresult->fetch_assoc()){
    $arm=$armrow['arm_shape'];
    array_push($armpose,$arm);
  }
  

  $chinquery = "SELECT DISTINCT `chin_position` FROM `tbl_model_pose` WHERE `mpose_id` in ($pose) ;";
  $chinresult=$mysqli->query($chinquery);
  while($chinrow=$chinresult->fetch_assoc()){
    $chin=$chinrow['chin_position'];
    array_push($chinpose,$chin);
  }

  $neckpose = $chinpose;
  $armpose = $armpose;

  $mstquery= "SELECT * FROM `tbl_model` WHERE m_id='$mid' ";
  $result=$mysqli->query($mstquery);   
  $row=$result->fetch_assoc();           
  $mod_body = $row['mod_body'];

  $mstquery2= "SELECT * FROM `tbl_model` WHERE mod_name='$mod_body' ";
  $result2=$mysqli->query($mstquery2);   
  $row2=$result2->fetch_assoc();           
  $mid = $row2['m_id'];

///*******  chin1 Start      *******//////
  if (in_array("chin-down", $neckpose)) {
  // code...
    //echo "chin-down";
    if (in_array("bothstraight", $armpose)){
// echo "bothstraight";
      $tquery = "SELECT * FROM `tbl_top` where  neck IN ('ROUND NECK','OPEN NECK','PLUNGE NECK') AND sleeve IN ('SLEEVELESS','SHORT SLEEVE','CAP SLEEVE','ABOVE ELBOW SLEEVE','BELOW ELBOW SLEEVE','3/4th SLEEVE','FULL SLEEVE') AND model_id='$mid' AND status='1' AND psd_exist='1'ORDER by `top_id`;"; 

    }elseif (in_array("forearmmovement", $armpose)) {
  // echo "forearmmovement";
      $tquery = "SELECT * FROM `tbl_top` where  neck IN ('ROUND NECK','OPEN NECK','PLUNGE NECK') AND sleeve IN ('SLEEVELESS','SHORT SLEEVE','CAP SLEEVE','ABOVE ELBOW SLEEVE') AND model_id='$mid' AND status='1' AND psd_exist='1'ORDER by `top_id`;"; 
    }elseif (in_array("armmovement", $armpose)) {
  // echo "armmovement";

     $tquery = "SELECT * FROM `tbl_top` where  neck IN ('ROUND NECK','OPEN NECK','PLUNGE NECK') AND sleeve IN ('SLEEVELESS','SHORT SLEEVE','CAP SLEEVE') AND model_id='$mid' AND status='1' AND psd_exist='1'ORDER by `top_id`;"; 
   }


 }elseif (in_array("chin-straight", $neckpose)){

   //echo "chin-straight";
   if (in_array("bothstraight", $armpose)){
// echo "bothstraight";
    $tquery = "SELECT * FROM `tbl_top` where  neck IN ('HIGH NECK','ROUND NECK','OPEN NECK','PLUNGE NECK') AND sleeve IN ('SLEEVELESS','SHORT SLEEVE','CAP SLEEVE','ABOVE ELBOW SLEEVE','BELOW ELBOW SLEEVE','3/4th SLEEVE','FULL SLEEVE') AND model_id='$mid' AND status='1' AND psd_exist='1'ORDER by `top_id`;"; 

  }elseif (in_array("forearmmovement", $armpose)) {
  // echo "forearmmovement";
    $tquery = "SELECT * FROM `tbl_top` where  neck IN ('HIGH NECK','ROUND NECK','OPEN NECK','PLUNGE NECK') AND sleeve IN ('SLEEVELESS','SHORT SLEEVE','CAP SLEEVE','ABOVE ELBOW SLEEVE') AND model_id='$mid' AND status='1' AND psd_exist='1'ORDER by `top_id`;"; 
  }elseif (in_array("armmovement", $armpose)) {
  // echo "armmovement";

   $tquery = "SELECT * FROM `tbl_top` where  neck IN ('HIGH NECK','ROUND NECK','OPEN NECK','PLUNGE NECK') AND sleeve IN ('SLEEVELESS','SHORT SLEEVE','CAP SLEEVE') AND model_id='$mid' AND status='1' AND psd_exist='1'ORDER by `top_id`;"; 
 }

}elseif (in_array("chin-up", $neckpose)) {
  // code...
  //echo "chin-up";

  if (in_array("bothstraight", $armpose)){
// echo "bothstraight";
    $tquery = "SELECT * FROM `tbl_top` where  neck IN ('TURTLE NECK','HIGH NECK','ROUND NECK','OPEN NECK','PLUNGE NECK') AND sleeve IN ('SLEEVELESS','SHORT SLEEVE','CAP SLEEVE','ABOVE ELBOW SLEEVE','BELOW ELBOW SLEEVE','3/4th SLEEVE','FULL SLEEVE') AND model_id='$mid' AND status='1' AND psd_exist='1'ORDER by `top_id`;"; 

  }elseif (in_array("forearmmovement", $armpose)) {
  // echo "forearmmovement";
    $tquery = "SELECT * FROM `tbl_top` where  neck IN ('TURTLE NECK','HIGH NECK','ROUND NECK','OPEN NECK','PLUNGE NECK') AND sleeve IN ('SLEEVELESS','SHORT SLEEVE','CAP SLEEVE','ABOVE ELBOW SLEEVE') AND model_id='$mid' AND status='1' AND psd_exist='1'ORDER by `top_id`;"; 
  }elseif (in_array("armmovement", $armpose)) {
  // echo "armmovement";

   $tquery = "SELECT * FROM `tbl_top` where  neck IN ('TURTLE NECK','HIGH NECK','ROUND NECK','OPEN NECK','PLUNGE NECK') AND sleeve IN ('SLEEVELESS','SHORT SLEEVE','CAP SLEEVE') AND model_id='$mid' AND status='1' AND psd_exist='1'ORDER by `top_id`;"; 
 }


}else{

  $tquery = "SELECT * FROM `tbl_top` where  model_id='$mid' AND status='1' ORDER by `top_id`;"; 
}


// $tquery = "SELECT * FROM `tbl_top` where  model_id='$mid' AND status='1' ORDER by `top_id`;";

$tresult=$mysqli->query($tquery);
$row_cnt = $tresult->num_rows;  
if ($row_cnt>0){
  ?>   

  <div class="row pt-3 pb-3">

    <div class="col-6 col-md-6">
     <!--   <div class="custom-control custom-checkbox">
         <input type="checkbox" class="custom-control-input" id="showExtraTop">
         <label class="custom-control-label" for="showExtraTop">Show Extras</label>
       </div> -->
     </div>

     <div class="col-6 col-md-6">
      <select name="topWear" id="topWear" class="form-control" disabled="disabled">
        <option value="0" selected >All Top</option>
        <!-- <option value="top" >Top</option>
          <option value="shirts">Shirts</option> -->
        </select> 
      </div>

    </div>

    <div class="row" >
     <?php  

     while($trow=$tresult->fetch_assoc()){
      $timgname=explode('-', $trow['top_img']);
      $timgname=$timgname[1].'-'.$timgname[2];
      // echo $trow['top_img'];
       ?>

       <div class="col-6 col-sm-6 col-md-4 col-lg-4 col-xl-4 mb-3" onclick="topclick(<?php echo $trow['top_id'];?>,'<?php echo PIQUIC_URL ;?>images/top/<?php echo $trow['top_img'];?>');topvalidation('<?php echo $trow['top_img'];?>');topimg(<?php echo $trow['top_id'];?>,'<?php echo PIQUIC_URL ;?>images/top/<?php echo $trow['top_img'];?>');">

         <!-- <div class="border" style="background: url(images/top/<?php //echo $trow['top_img'];?>) no-repeat center center; background-size: contain; height: 14rem;"></div> -->

         <div class="w-100 border" style="height: 12rem; overflow: hidden;">
          <img src="<?php echo PIQUIC_URL ;?>images/top/<?php echo $trow['top_img'];?>"class="loadingTop topView">
        </div>
        <span style="font-size: .74rem;"><?php echo $timgname;?></span>
        <div class="custom-control custom-radio c-box">
          <input type="radio" class="custom-control-input" name="top" value="1" id="topID<?php echo $trow['top_id'];?>" onclick="topclick(<?php echo $trow['top_id'];?>,'<?php echo PIQUIC_URL ;?>images/top/<?php echo $trow['top_img'];?>');topimg(<?php echo $trow['top_id'];?>,'<?php echo PIQUIC_URL ;?>images/top/<?php echo $trow['top_img'];?>');topvalidation('<?php echo $trow['top_img'];?>');">
          <label class="custom-control-label" for="topID<?php echo $trow['top_id'];?>">&nbsp;</label>
        </div>
      </div>

    <?php }  ?>


  </div>   

<?php }else{   ?>     

 <div class="col-md-12 mt-3 text-center text-danger lead font-weight-bold">
  No Top Available for this Model.
</div>  
<?php   }?>     
