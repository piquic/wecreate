
<?php 
session_start();
include('config.php'); 
//print_r($_POST);

if(isset($_POST['cat']))
{
	$cat_val = $_POST['cat'];
//print_r($cat_val);
}
else{
	$cat_val[]="Top";
 //print_r($cat_val);
}
if(!empty($_POST['angle_text'])){
	$angle=array_filter(explode(",", $_POST['angle_text']));
	//print_r($_POST['angle_text']);
	//	$quty_arr=array_filter(explode(",", $quty));
	// foreach ($angle as $key => $value) {
	// 	echo $value;
	// }

}
else{
	$angle[]="";
}
//print_r($_POST);
// $angle=array_merge($_POST['angle_Top'],$_POST['angle_Bottom'],$_POST['angle_Dress']);
// 	print_r($angle);
if(!empty($_POST['angle_Top'])&&!empty($_POST['angle_Bottom'])&&!empty($_POST['angle_Dress'])){
	
	$angle=array_merge($_POST['angle_Top'],$_POST['angle_Bottom'],$_POST['angle_Dress']);
}
else if(!empty($_POST['angle_Top'])&&!empty($_POST['angle_Bottom'])&&empty($_POST['angle_Dress'])){
	$angle=array_merge($_POST['angle_Top'],$_POST['angle_Bottom']);
}
else if(empty($_POST['angle_Top'])&&!empty($_POST['angle_Bottom'])&&!empty($_POST['angle_Dress'])){
	$angle=array_merge($_POST['angle_Bottom'],$_POST['angle_Dress']);
}
else if(!empty($_POST['angle_Top'])&&empty($_POST['angle_Bottom'])&&!empty($_POST['angle_Dress'])){
	$angle=array_merge($_POST['angle_Top'],$_POST['angle_Dress']);
}
else if(!empty($_POST['angle_Top'])&&empty($_POST['angle_Bottom'])&&empty($_POST['angle_Dress'])){
	$angle=$_POST['angle_Top'];
}
else if(empty($_POST['angle_Top'])&&!empty($_POST['angle_Bottom'])&&empty($_POST['angle_Dress'])){
	$angle=$_POST['angle_Bottom'];
}
else if(empty($_POST['angle_Top'])&&empty($_POST['angle_Bottom'])&&!empty($_POST['angle_Dress'])){
	$angle=$_POST['angle_Dress'];
}

else {
	$angle[]="";
}
// print_r($angle);
//print_r($cat_val[0]);
if(!empty($_POST['gender']))
{
	$gender=$_POST['gender'];
}
else{
	$gender=1;
}
$i=1;
foreach ($cat_val as $cat_val1) {
	
	if(!empty($cat_val1))
	{

		?>
		<br>

		<div class="form-row pb-4">
			<div class="col-3 col-md-2 mb-3">
				<div id="img_<?php echo $cat_val1; ?>_front_<?php echo $gender ?>" class="text-center" onclick="angle_image('<?php echo $cat_val1; ?>_front_<?php echo $gender ?>')">
					<img class="d-block w-100 border rounded" src="<?php echo PIQUIC_URL;?>images/booking_thumb/<?php echo $gender ?>/<?php echo $cat_val1; ?>/front.jpg">

					<p>FRONT</p>
				</div>

				<div class="c-box">
					<div class="custom-control custom-checkbox">
						<input type="checkbox" class="custom-control-input <?php echo $cat_val1; ?> angle" id="<?php echo $cat_val1; ?>_front_<?php echo $gender ?>" name="angle_<?php echo $cat_val1 ?>[]" value="<?php echo $cat_val1; ?>_front_<?php echo $gender ?>" <?php if((in_array(($cat_val1.'_front_'.$gender), $angle))==1){echo "checked";} ?> >
						<label class="custom-control-label" for="<?php echo $cat_val1; ?>_front_<?php echo $gender ?>">&nbsp;</label>
					</div>
				</div>
			</div>

			<div class="col-3 col-md-2 mb-3">
				<div id="img_<?php echo $cat_val1; ?>_back_<?php echo $gender ?>" class="text-center" onclick="angle_image('<?php echo $cat_val1; ?>_back_<?php echo $gender ?>')">
					<img class="d-block w-100 border rounded" src="<?php echo PIQUIC_URL;?>/images/booking_thumb/<?php echo $gender ?>/<?php echo $cat_val1; ?>/back.jpg">

					<p>BACK</p>
				</div>

				<div class="c-box">
					<div class="custom-control custom-checkbox">
						<input type="checkbox" class="custom-control-input <?php echo $cat_val1; ?> angle" id="<?php echo $cat_val1; ?>_back_<?php echo $gender ?>" name="angle_<?php echo $cat_val1 ?>[]" value="<?php echo $cat_val1; ?>_back_<?php echo $gender ?>" <?php if((in_array($cat_val1."_back_".$gender, $angle))==1){echo "checked";} ?>>
						<label class="custom-control-label" for="<?php echo $cat_val1; ?>_back_<?php echo $gender ?>">&nbsp;</label>
					</div>
				</div>
			</div>

			<div class="col-3 col-md-2 mb-3">
				<div id="img_<?php echo $cat_val1; ?>_left_<?php echo $gender ?>" class="text-center" onclick="angle_image('<?php echo $cat_val1; ?>_left_<?php echo $gender ?>')">
					<img class="d-block w-100 border rounded" src="<?php echo PIQUIC_URL;?>/images/booking_thumb/<?php echo $gender ?>/<?php echo $cat_val1; ?>/left.jpg" value="left">

					<p>LEFT</p>
				</div>

				<div class="c-box">
					<div class="custom-control custom-checkbox">
						<input type="checkbox" class="custom-control-input <?php echo $cat_val1; ?> angle" id="<?php echo $cat_val1; ?>_left_<?php echo $gender ?>" name="angle_<?php echo $cat_val1 ?>[]" value="<?php echo $cat_val1; ?>_left_<?php echo $gender ?>" <?php if((in_array($cat_val1."_left_".$gender, $angle))==1){echo "checked";} ?>>
						<label class="custom-control-label" for="<?php echo $cat_val1; ?>_left_<?php echo $gender ?>">&nbsp;</label>
					</div>
				</div>
			</div>

			<div class="col-3 col-md-2 mb-3">
				<div id="img_<?php echo $cat_val1; ?>_right_<?php echo $gender ?>" class="text-center" onclick="angle_image('<?php echo $cat_val1; ?>_right_<?php echo $gender ?>')">
					<img class="d-block w-100 border rounded" src="<?php echo PIQUIC_URL;?>/images/booking_thumb/<?php echo $gender ?>/<?php echo $cat_val1; ?>/right.jpg">

					<p>RIGHT</p>
				</div>

				<div class="c-box">
					<div class="custom-control custom-checkbox">
						<input type="checkbox" class="custom-control-input <?php echo $cat_val1; ?> angle" id="<?php echo $cat_val1; ?>_right_<?php echo $gender ?>" name="angle_<?php echo $cat_val1 ?>[]" value="<?php echo $cat_val1; ?>_right_<?php echo $gender ?>" <?php if((in_array($cat_val1."_right_".$gender, $angle))==1){echo "checked";} ?>>
						<label class="custom-control-label" for="<?php echo $cat_val1; ?>_right_<?php echo $gender ?>">&nbsp;</label>
					</div>
				</div>
			</div>
			<span id="angle_validate<?php echo $cat_val1 ?>"></span>
		</div>


		<?php
					//	}
	}
	$i++;
}


?>