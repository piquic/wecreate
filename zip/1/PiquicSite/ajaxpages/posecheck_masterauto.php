  <?php   
  session_start();
  include('config.php');  
  $mi = $_POST['gen_model_id'];
   $poid = $_POST['poid'];
  $cli_name = $_POST['cli_name'];
  $status = $_POST['status'];
  if($status=='2'){
    $usedate = $_POST['usedate']; 
  }
  $poseSekcje = explode(',',$poid );
  //$poseSekcje=explode(",", ("pose_".implode(",pose_", $poseSekcje)));
  //echo "<pre>";
  //print_r($poseSekcje);

  ?>
  <div class="col-md-2">
    <p class="lead font-weight-bold">Model Poses:</p>
  </div>

  <div class="col-md-10" >
    <script type="text/javascript">
      $(document).ready(function () { 
        <?php 
        $mquery = "SELECT * FROM `tbl_model` where status='1'";
        $mresult=$mysqli->query($mquery);         
        while($mrow=$mresult->fetch_assoc()){
          $m_id=$mrow['m_id'];
          ?>

          $('#slctAll<?php echo $m_id;?>').on('click',function(){
            if(this.checked){
              $('.chkbCls<?php echo $m_id;?>').each(function(){
                this.checked = true;

                var lfckp = this.id;
                $('#'+lfckp+'').addClass('activepose').removeClass('inactivepose');
              });
            }else{
              $('.chkbCls<?php echo $m_id;?>').each(function(){
                this.checked = false;

                var lfckp = this.id;
                $('#'+lfckp+'').removeClass('activepose').addClass('inactivepose');
              });
            }

            var ids = $('.activepose').map(function(i) {
              return this.id;
            });
            
            $('#modelpose').val(ids.get().join(','));
          });

          $('.chkbCls<?php echo $m_id;?>').on('click',function(){
            if($('.chkbCls<?php echo $m_id;?>:checked').length == $('.chkbCls<?php echo $m_id;?>').length){
              $('#slctAll<?php echo $m_id;?>').prop('checked',true);
            }else{
              $('#slctAll<?php echo $m_id;?>').prop('checked',false);
            }
          });
        <?php } ?>

      });
    </script>

    <?php 
    if($mi!=''){
     $mquery = "SELECT * FROM `tbl_model` WHERE `m_id` IN ($mi) ";
   }else{
    $mquery = "SELECT * FROM `tbl_model`";
  }

  $mresult=$mysqli->query($mquery);         
  while($mrow=$mresult->fetch_assoc()){
   $m_id=$mrow['m_id'];
   $mod_name=$mrow['mod_name'];
   $mod_name=strtoupper($mod_name) ;
   ?>	 

   <div class="row">

    <?php
    $mapquery = "SELECT * FROM `tbl_autopose`;";

    $mapresult=$mysqli->query($mapquery);
    $maprow=$mapresult->fetch_assoc();

    $mapclient_name=$maprow['client_name'];
    $mapmodel_id=$maprow['model_id'];

    $mapmodel_id = explode(',', $mapmodel_id);
    ?>

    <div class="col-md-12 mb-3 text-center">
      <h3 class="h2"><?php echo $mod_name;?></h3>

      <div class="custom-control custom-checkbox">
        <input type="checkbox" class="custom-control-input" id="slctAll<?php echo $m_id;?>" <?php if ((in_array($m_id, $mapmodel_id)) && ($mapclient_name == $cli_name)) { ?> checked <?php } ?> >
        <label class="custom-control-label" for="slctAll<?php echo $m_id;?>">Select / Deselect All</label>
      </div>
    </div>


    <?php           
    $mpquery = "SELECT * FROM `tbl_model_pose` WHERE `m_id`='$m_id' AND `pose_status` != 1 AND psd_exist='1' ;";
    $mpresult=$mysqli->query($mpquery);  
                //  $row_cnt = $mpresult->num_rows; 

    while($mprow=$mpresult->fetch_assoc()){
      $mpose_id=$mprow['mpose_id'];
      $mpose_img=$mprow['mpose_img'];

      if($status=='2'){

        if($usedate=='1'){

          $mpcquery = "SELECT SUM(`pcount`)  AS pose_sum FROM `tbl_modelpose_count` where  mpose_id='$mpose_id' AND `client_name`='$cli_name' AND `use_date`= CURDATE() ;";
        }
        if($usedate=='2'){

          $mpcquery = "SELECT SUM(`pcount`)  AS pose_sum FROM `tbl_modelpose_count` where mpose_id='$mpose_id' AND `client_name`='$cli_name' AND  DATE(use_date) = CURDATE() - 1;;";
        }
        if($usedate=='3'){

          $mpcquery = "SELECT SUM(`pcount`)  AS pose_sum FROM `tbl_modelpose_count` where  mpose_id='$mpose_id' AND `client_name`='$cli_name' AND  use_date BETWEEN CURDATE() - INTERVAL 7 DAY AND CURDATE() ;";
        }

        if($usedate=='4'){

          $mpcquery = "SELECT SUM(`pcount`)  AS pose_sum FROM `tbl_modelpose_count` where  mpose_id='$mpose_id' AND `client_name`='$cli_name' AND `use_date`BETWEEN CURDATE() - INTERVAL 30 DAY AND CURDATE() ;";
        }

        if($usedate=='5'){

          $mpcquery = "SELECT SUM(`pcount`)  AS pose_sum FROM `tbl_modelpose_count` where  mpose_id='$mpose_id' AND `client_name`='$cli_name' AND YEAR(use_date) = YEAR(CURDATE());";
        }

      }else{
        $mpcquery = "SELECT SUM(`pcount`)  AS pose_sum  FROM `tbl_modelpose_count` where  mpose_id='$mpose_id' AND client_name='$cli_name';";
      }



      $mpcresult=$mysqli->query($mpcquery);
      $mpcrow=$mpcresult->fetch_assoc();
      $pcount=$mpcrow['pose_sum'];
      if($pcount!=''){
        $pcount=$pcount;
      }else{
        $pcount='0';
      }


      if (!in_array($mpose_id, $poseSekcje)) {

        $autoquery = "SELECT * FROM `tbl_model_pose`  WHERE `m_id`='$m_id' AND psd_exist='1';";
        $autoresult=$mysqli->query($autoquery);
        $row_cnt = $autoresult->num_rows; 

        ?>
        <div class="col-md-2 text-center mb-3">
          <div class="mpcbox">
            <?php if($poid!=''){ ?>

              <input type="checkbox" class="mp-chkbox chkbCls<?php echo $m_id;?> <?php if (in_array("pose_".$mpose_id, $poseSekcje)) { ?> activepose <?php }?>" id="pose_<?php echo $mpose_id; ?>" name="<?php echo $mpose_id; ?>" onclick="poseselFunction(<?php echo $mpose_id; ?>)" <?php if (in_array("pose_".$mpose_id, $poseSekcje)) { ?> checked <?php }?> />

            <?php }else{?>
              <input type="checkbox" checked class="mp-chkbox activepose chkbCls<?php echo $m_id;?>" id="pose_<?php echo $mpose_id; ?>" name="<?php echo $mpose_id; ?>" onclick="poseselFunction(<?php echo $mpose_id; ?>)"/>
            <?php } ?>

          </div>
          <div class="mplbox">
            <label class="mp-chklbl" for="pose_<?php echo $mpose_id; ?>">&nbsp;</label>
          </div>
          <div class="border mb-2">
            <img class="w-100" src="<?php echo PIQUIC_URL;?>images/thumbnails/model_pose/<?php echo $mpose_img ?>" />
          </div>
          <small class="text-uppercase"><?php echo basename($mpose_img); ?>
          <br>( <?php echo $pcount;?> )

        </small>
      </div>
      <br>
    <?php  } }?>

  </div>
  <hr>

<?php } ?>


</div>


<script type="text/javascript">
  function poseselFunction_old(p) {
  
  var lfckp = document.getElementById('pose_'+p).checked;


if(lfckp==true){
  $('#pose_'+p+'').addClass('activepose').removeClass('inactivepose');
}else{
  $('#pose_'+p+'').addClass('inactivepose').removeClass('activepose');

}
      // $('#pose_'+p+'').toggleClass('activepose');
      var ids = $('.activepose').map(function(i) {
        return this.id;
      });
      var idsall=ids.get().join(',');
      var gen_model_id=$("#gen_model_id").val();
      var mi =$('#model').val();
      var mpi=$('#modelpose').val();

      $.ajax({
        type:'POST',
        url:'<?php echo PIQUIC_AJAX_URL ;?>/posecheck_masterauto_ids.php',
        data:{'gen_model_id':gen_model_id,'mpi':mpi,'mi':mi,'idsall':idsall},
        success: function(checkresults) { 
           alert(checkresults);
           $('#modelpose').val(checkresults);
       
        }
        });

      //$('#modelpose').val(","+ids.get().join(',')+","); 
    }

    function poseselFunction(p) {
    // alert('Hello Pose');
    //alert(p);
    var lfckp = document.getElementById('pose_'+p).checked;
    // alert(lfckp);

    if(lfckp==true){
    $('#pose_'+p+'').addClass('activepose').removeClass('inactivepose');
    }else{
    $('#pose_'+p+'').addClass('inactivepose').removeClass('activepose');

    }


    var modelpose = $('#modelpose').val();
    var check_type ='';


    //alert(modelpose);
   /* if(modelpose!='')
    {
            var modelposearr=modelpose.split(",");

            console.log(modelposearr);

            // $('#pose_'+p+'').toggleClass('activepose');
            var ids = $('.activepose').map(function(i) {


            if($.inArray(this.id, modelposearr) !== -1)
            {
            //alert(this.id);
            }
            else
            {
            return this.id;
            }

            });

            alert(ids.get().join(','));

            var finalmodelpose=modelpose+','+ids.get().join(',');
            console.log(ids.get().join(','));
            //$('#modelpose').val(ids.get().join(',')); 

            $('#modelpose').val(finalmodelpose); 



    }
    else
    {*/
          var ids = $('.activepose').map(function(i) {
            return this.id;
           });
          var finalmodelpose=ids.get().join(','); 
          

          var model=$("#model").val();
          var modelpose=$("#modelpose").val(); 

          var gen_model_id=$("#gen_model_id").val();
          var client_name=$("#client_name").val();

          //alert("bbbbb");
          //alert(finalmodelpose);
           //alert(gen_model_id);

          $.ajax({
          type:'POST',
          url:'<?php echo PIQUIC_AJAX_URL ;?>/master-autopose-select-unselect-pose.php',
          data: {'check_type':check_type,'finalmodelpose':finalmodelpose,'model':model,'modelpose':modelpose,'gen_model_id':gen_model_id,'client_name':client_name},
          success: function(response){

           // alert("fffff");
          //alert(response);
           $('#modelpose').val($.trim(response)); 

          }
          });        
    //}




   
    









    }

  </script>