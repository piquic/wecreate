<?php  
session_start();
include('config.php');
$mid = $_POST['mid'];
$client_name = $_POST['client_name'];

 // echo $skuurl;

$mkskuquery = "SELECT * FROM `tbl_makup_info` where  model_id='$mid' AND client_name='$client_name' AND status='1';";
$mkskdata=$mysqli->query($mkskuquery);
$mkskrow=$mkskdata->fetch_assoc();
$mkskstatus = $mkskrow['status'];
$mklipcol = $mkskrow['lipcolcode'];
$lipcoleff = $mkskrow['lipcoleff'];

$eyeshdowcolcode = $mkskrow['eyeshdowcolcode'];
$eye_intensity = $mkskrow['eye_intensity'];
$eyemkupimg = $mkskrow['eyemkup'];
$blushcolcode = $mkskrow['blushcolcode'];
$blush_intensity = $mkskrow['blush_intensity'];
// $mkskstatus = $mkskrow['status'];



$mquery = "SELECT * FROM `tbl_model` where m_id='$mid';";
$mresult=$mysqli->query($mquery);
$mrow=$mresult->fetch_assoc();
$mod_name = $mrow['mod_name'];
$modnamecls=strtolower($mod_name);
$uppermodnamecls=strtoupper($mod_name);

$blush_query="SELECT * FROM `tbl_blush` where `blu_img` LIKE '%$uppermodnamecls%'";
$blush_result=$mysqli->query($blush_query);
$blush_count=$blush_result->num_rows;

$lipcol_query="SELECT * FROM `tbl_lipcol` where `lipcol_img` LIKE '%$uppermodnamecls%'";
$lipcol_result=$mysqli->query($blush_query);
$lipcol_count=$blush_result->num_rows;

$eyesh_query="SELECT * FROM `tbl_eyeshadow` where `eyesh_img` LIKE '%$uppermodnamecls%'";
$eyesh_result=$mysqli->query($blush_query);
$eyesh_count=$blush_result->num_rows;

if($blush_count==0 && $lipcol_count==0 && $eyesh_count==0)
{
	echo 0;
	die();
}
?>

<style type="text/css">
	#ma-ke-up .scroll-box::-webkit-scrollbar { width: .0em; }
	#ma-ke-up input[type="range" i] {
		color: #76aea1 !important;
	}
</style>

<div class="container-fluid p-2" id="ma-ke-up">
	<div class="row">
		<div class="col-md-12">
			<nav id="do-make-up">
				<div class="nav nav-tabs nav-justified" id="nav-sub-tab" role="tablist">
					<a class="nav-item nav-link active" id="nav-lipstick-tab" data-toggle="tab" href="#nav-lipstick" role="tab" aria-controls="nav-lipstick" aria-selected="false">Lipstick</a>

					<a class="nav-item nav-link" id="nav-eyes-tab" data-toggle="tab" href="#nav-eyes" role="tab" aria-controls="nav-eyes" aria-selected="false">Eyes</a>

					<a class="nav-item nav-link" id="nav-blush-tab" data-toggle="tab" href="#nav-blush" role="tab" aria-controls="nav-blush" aria-selected="false">Blush</a>
				</div>
			</nav>
		</div>
	</div>
	<div class="row">
		<div class="col-md-12">
			<div class="w-100" style="height: 22rem; overflow: hidden;" id="make_up">
				<!-- <img src="<?php echo PIQUIC_URL;?>images/model_pose/KRI-MSTR.png"; style="width: 200%; margin: -46px 0 0 -368px;"> -->
			</div>

			<div class="w-100" style="height: 22rem; overflow: hidden; position: absolute; z-index: 999; top: 0;" id="lip-img"> 

				<?php 
				$lipquery = "SELECT lipcol_img FROM `tbl_lipcol` WHERE `lipcol_img` LIKE '%$uppermodnamecls%' AND  `lipcol_img` LIKE '%$lipcoleff%'  AND `col_code` = '$mklipcol';";
				$lipresult=$mysqli->query($lipquery);
				$lip_row=$lipresult->fetch_assoc();
				$lip_img = $lip_row['lipcol_img'];
				?>

				<img class="<?php echo $modnamecls;?>" src="<?php echo PIQUIC_URL;?>images/lipstick/<?php echo $lip_img;?>">
			</div>

			<div class="w-100" style="height: 22rem; overflow: hidden; position: absolute; z-index: 999; top: 0;" id="eyeshimg">

				<?php 
				$eshquery = "SELECT * FROM `tbl_eyeshadow` where `eyesh_img` LIKE '%$uppermodnamecls%' AND eyesh_code='$eyeshdowcolcode';";
				$eshresult=$mysqli->query($eshquery);
				$esh_row=$eshresult->fetch_assoc();
				$eye_sh_img = $esh_row['eyesh_img'];
				?>

				<img class="<?php echo $modnamecls;?>-eyesh eyesh" src="<?php echo PIQUIC_URL;?>images/eyeshadow/<?php echo $eye_sh_img;?>";>
			</div>

			<div class="w-100" style="height: 22rem; overflow: hidden; position: absolute; z-index: 999; top: 0;" id="eyemkimg">				
				<!-- <img class="klad-eyemk" src="<?php echo PIQUIC_URL;?>images/eyemakeup/KLAD-MSTR-SMOKY.png";>  -->
				<img class="<?php echo $modnamecls;?>-eyemk" src="<?php echo PIQUIC_URL;?>images/eyemakeup/<?php echo $eyemkupimg;?>";>
			</div>

			<div class="w-100" style="height: 22rem; overflow: hidden; position: absolute; z-index: 999; top: 0;" id="bluimg">
				<?php 
				$bluquery = "SELECT * FROM `tbl_blush` where `blu_img` LIKE '%$uppermodnamecls%' AND blu_code='$blushcolcode';";
				$bluresult=$mysqli->query($bluquery);
				$blurow=$bluresult->fetch_assoc();
				$eyeblu_img = $blurow['blu_img'];
				?>
				<img class="<?php echo $modnamecls;?>-blu blu" src="<?php echo PIQUIC_URL;?>images/blush/<?php echo $eyeblu_img;?>";>
			</div>
			<div class="text-right shadow p-1 bg-white rounded" style="position: absolute; z-index: 999; bottom: 0;">
				<input type="hidden" name="client_name" id="client_name" value="<?php echo $client_name;?>">
				
				<div class="custom-control custom-checkbox">
					<input type="checkbox" class="custom-control-input" id="usethis" name="usethis" <?php if($mkskstatus==1){ echo 'checked';}?> >
					<label class="custom-control-label lead" for="usethis">Use this for next SKU.</label>
				</div>
			</div>
		</div>
	</div>
	<hr>
	<div class="row">
		<div class="col-md-12">
			<div class="tab-content" id="nav-tabContent">
				<div class="tab-pane fade show active" id="nav-lipstick" role="tabpanel" aria-labelledby="nav-lipstick-tab">
					<div class="row">
						<div class="col-md-7 scroll-box" style="height: 20rem;">
							<div id="misimg"></div>
							<div class="row text-center radioLip-group">


								<input type="hidden" id="radioLip-value" name="radioLip-value"  value="<?php echo $mklipcol;?>" />
								<br>
								<?php 
								$cquery1 = "SELECT * FROM `tbl_lipcol` GROUP BY `col_name` ORDER by `lip_id`;";
								$result=$mysqli->query($cquery1);				  
								while($row=$result->fetch_assoc()){
									$col_name = $row['col_name'];
									$lip_id = $row['lip_id'];					   
									$col_code = $row['col_code'];
									$hex = "#".$col_code."";
									list($r, $g, $b) = sscanf($hex, "#%02x%02x%02x");
									$r += 82;
									$g += 23;
									$b += 15; 
									?>
									<div class="col-md-6 radioLip" id="lipcol_<?php echo $lip_id;?>" data-value="<?php echo $col_code;?>">
										<div class="p-1 mb-3 shadow rounded <?php if($col_code === $mklipcol){ ?> border <?php } ?>" style="width: 100%;" title="<?php echo $col_name; ?>">
											<i class="fas fa-circle icon-lg" style="color:rgb(<?php echo $r.', '.$g.', '.$b; ?>);"></i><br><?php echo substr($col_name, 0, 10);?>
										</div>
									</div>	
								<?php }?>
							</div>
						</div>
						<div class="col-md-5">
							<div class="m-1">
								<label for="selectEffect" class="text-piquic">Effect</label>
								<select class="form-control form-control-sm" id="selectEffect" name="selectEffect" >
									<option value="Matte" <?php if($lipcoleff=='Matte'){?> selected <?php }?>>Matte</option>
									<option value="Satin" <?php if($lipcoleff=='Satin'){?> selected <?php }?>>Satin</option>
									<option value="Glossy" <?php if($lipcoleff=='Glossy'){?> selected <?php }?>>Glossy</option>
								</select>
							</div>
							<div class="mt-3 text-center">
								<button type="button" class="btn btn-outline-danger" id="clrlip" >&emsp;Clear Lipstick&emsp;</button>
							</div>
						</div>
					</div>
				</div>


				<div class="tab-pane fade" id="nav-eyes" role="tabpanel" aria-labelledby="nav-eyes-tab">
					<div class="row">
						<div class="col-md-6">
							<span class="text-piquic">Eye Shadow</span><br>

							<div class="row pt-2">
								<div class="col-md-4">
									<div id="slctNot" class="<?php if($eye_sh_img == '') { } else { echo "d-none"; } ?>" style="height: 5.5rem; width: 5.5rem; border: 1px solid #acacac;">
										<img class="w-100" src="<?php echo PIQUIC_URL;?>images/makeup/eye-shadow.png">
									</div>
									<div id="slctNat" class="<?php if($eye_sh_img == '') { echo "d-none"; } else { } ?> text-piquic">
										<div style="height: 5.5rem; width: 5.5rem; overflow: hidden; position: absolute; z-index: 999; top: 0; border: 1px solid #acacac;">
											<img src="<?php echo PIQUIC_URL;?>images/eyeshadow/<?php echo $eye_sh_img;?>"; style="width: 1600%; margin: -8.8rem 0 0 -38.3rem;">
										</div>
									</div>
								</div>
								<?php 
								$ecquery1 = "SELECT * FROM `tbl_eyeshadow` ORDER by `eyesh_id`;";
								$eresult=$mysqli->query($ecquery1);	
								$eye_row=$eresult->fetch_assoc();
								$eye_sh_code = $eye_row['eyesh_code'];
								?>
								<div class="col-md-8 text-center">
									<span class="text-piquic">Colors: </span><br>
									<div id="colorSlctd" class="btn" style="background-color: #<?php if($eyeshdowcolcode==''){ echo 'ffc107'; } else { echo $eyeshdowcolcode; }?>; color: #ffc107;" data-toggle="collapse" data-target="#slctColor">  &nbsp;</div>

									<div class="collapse mt-1 bg-white" id="slctColor" style="position:absolute; z-index: 999; width: 87% !important; margin-left: .6rem;">
										<div class="row p-1 border rounded rdsh-group" style="width: 100% !important;">
											<input type="hidden" id="rdsh-value" name="rdsh-value" value="<?php echo $eyeshdowcolcode;?>" />
											<br>
											<?php 
											$ecquery2 = "SELECT * FROM `tbl_eyeshadow` GROUP BY `eyesh_name` ORDER by `eyesh_id`;";
											$eresult2=$mysqli->query($ecquery2);
											while($erow=$eresult2->fetch_assoc()){
												$eyesh_name = $erow['eyesh_name'];
												$eyesh_id = $erow['eyesh_id'];					   
												$eyesh_code = $erow['eyesh_code'];
												
												?>
												<div class="col-md-1 ml-1 my-1 border rounded rdsh" id="eyesh_<?php echo $eyesh_code;?>" data-value="<?php echo $eyesh_code;?>" data-toggle="collapse" data-target="#slctColor" style="height: 2rem; background-color:#<?php echo $eyesh_code;?>; color:#<?php echo $eyesh_code;?>;">
												</div>
											<?php } ?>
										</div>
									</div>
								</div>
							</div>

							<div class="row mt-2 pt-5">
								<div class="col-md-10 text-piquic">
									
									<div class="form-group">
										<label for="s-intensity">Intensity: <span id="s-inVal"><?php if($eye_intensity == null) { ?>100<?php } else { echo $eye_intensity; } ?></span>%</label>
										<div class="row">
											<div class="col-1">0</div>
											<div class="col">
												<input type="range" class="custom-range" id="s-intensity" step="10" min="0" max="100" value="<?php if($eye_intensity == null) { ?>100<?php } else { echo $eye_intensity; } ?>">
											</div>
											<div class="col-1">100</div>
										</div>
									</div>
									
								</div>
							</div>
						</div>

						<div class="col-md-6 scroll-box border-left">
							<span class="text-piquic">Eye Make Up</span><br>
							<div class="row text-center pt-2 rdeyemk-group">

								<input type="hidden" id="rdeyemk-value" name="rdeyemk-value" value="<?php echo $eyemkupimg;?>" />

								<br>
								<div class="col-md-4 rdeyemk" id="eyemkup1" data-value="Natural">
									<div class="text-piquic">
										<img class="p-1 w-100 <?php if($eyemkupimg=='Natural'){ ?> border <?php } ?>" src="<?php echo PIQUIC_URL;?>images/makeup/eye-natural.png">
										<small <?php if($eyemkupimg=='Natural'){ ?> class="font-weight-bold"  <?php } ?>>Natural</small>
									</div>
								</div>
								<div class="col-md-4 rdeyemk" id="eyemkup2" data-value="Medium">
									<div class="text-piquic">
										<img class="p-1 w-100 <?php if($eyemkupimg=='Medium'){ ?> border <?php } ?>" src="<?php echo PIQUIC_URL;?>images/makeup/eye-medium.png">
										<small <?php if($eyemkupimg=='Medium') {?> class="font-weight-bold" <?php } ?>>Medium</small>
									</div>
								</div>
								<div class="col-md-4 rdeyemk" id="eyemkup3" data-value="Smoky">
									<div class="text-piquic">
										<img class="p-1 w-100 <?php if($eyemkupimg=='Smoky'){ ?> border <?php } ?>" src="<?php echo PIQUIC_URL;?>images/makeup/eye-smokey.png">
										<small <?php if($eyemkupimg=='Smoky'){ ?> class="font-weight-bold"  <?php } ?>>Smoky</small>
									</div>
								</div>
							</div>

						</div>

						
					</div>
					<div class="row">
						<div class="col-md-6 text-center">
							<button type="button" class="btn btn-outline-danger" id="clreyesh">Clear Eye Shadow</button>
						</div>
						<div class="col-md-6 text-center">
							<button type="button" class="btn btn-outline-danger" id="clreyemk">Clear Eye Makeup</button>
						</div>
					</div>
				</div>

				<div class="tab-pane fade" id="nav-blush" role="tabpanel" aria-labelledby="nav-blush-tab">
					<div class="row">
						<div class="col-md-8 scroll-box" style="max-height: 20rem;">
							<div class="row text-center rdBlsh-group">
								<input type="hidden" id="rdBlsh-value" name="rdBlsh-value" value="<?php echo $blushcolcode;?>" />
								<br>
								<?php 
								$bquery1 = "SELECT * FROM `tbl_blush` GROUP BY `blu_name` ORDER by `blu_id`;";
								$bresult=$mysqli->query($bquery1);				  
								while($brow=$bresult->fetch_assoc()){
									$blu_name = $brow['blu_name'];
									$blu_id = $brow['blu_id'];					   
									$blu_code = $brow['blu_code'];
									$bluhex = "#".$blu_code."";
									list($br, $bg, $bb) = sscanf($bluhex, "#%02x%02x%02x");

									$br += 82;
									$bg += 23;
									$bb += 15; 
									?>

									<div class="col-md-4 rdBlsh" id="blshcol_<?php echo $blu_id;?>" data-value="<?php echo $blu_code;?>">
										<div class="p-1 mb-3 shadow rounded <?php if($blu_code===$blushcolcode){?> border <?php }?>" style="width: 100%" title="<?php echo $blu_name;?>">
											<i class="fas fa-circle icon-lg" style="color:rgb(<?php echo $br.', '.$bg.', '.$bb; ?>);"></i><br>
											<small><?php echo substr($blu_name, 0, 10);?></small>
										</div>
									</div>

								<?php } ?>
							</div>
						</div>
						<div class="col-md-4">
							<button type="button" class="btn btn-outline-danger" id="clrblush">Clear Blush</button>
						</div>
					</div>
					<hr>
					<div class="row">
						<div class="col-md-12 pt-2">							
							<div class="form-group">
								<label for="b-intensity">Intensity: <span id="b-inVal"><?php if($blush_intensity == null) { ?>100<?php } else { echo $blush_intensity; } ?></span>%</label>
								<div class="row">
									<div class="col-1">0</div>
									<div class="col-10">
										<input type="range" class="custom-range" id="b-intensity" step="10" min="0" max="100" value="<?php if($blush_intensity == null) { ?>100<?php } else { echo $blush_intensity; } ?>">
									</div>
									<div class="col-1">100</div>
								</div>
							</div>							
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<style>
	.selected{

		border: 1px solid #000000
	}
</style>

<script type="text/javascript">

	$(document).ready(function() {

		<?php if($eye_intensity == null) { ?>
			$("#s-intensity").attr('disabled', 'disabled');
		<?php } ?>
		
		<?php if($blush_intensity == null) { ?>	
			$("#b-intensity").attr('disabled', 'disabled');
		<?php } ?>


		$("#clrlip").on("click", function() {
			$('#radioLip-value').val('');
			$('#lip-img img').attr('src','');
			$('.radioLip-group').find('.radioLip div').removeClass('border');
		});

		$("#clrblush").on("click", function() {
			$('#rdBlsh-value').val('');
			$('#bluimg img').attr('src','');
			$("#b-intensity").attr('disabled', 'disabled');
			$('.rdBlsh-group').find('.rdBlsh div').removeClass('border');
		});

		$("#clreyesh").on("click", function() {
			$('#rdsh-value').val('');
			$('#eyeshimg img').attr('src','');
			$("#s-intensity").attr('disabled', 'disabled');
			$('#slctNot').removeClass('d-none');
			$('#slctNat').addClass('d-none');
		});

		$("#clreyemk").on("click", function() {
			$('#rdeyemk-value').val('');
			$('#eyemkimg img').attr('src','');
			$('.rdeyemk-group').find('.rdeyemk div img').removeClass('border');
		});


		$('#s-intensity').on('change', function(){
			$('#s-inVal').html(this.value);

			// $("#usethis").prop('checked', false);

			if(this.value == 0)
				$('.eyesh').css('opacity','0');
			else if (this.value == 100)
				$('.eyesh').css('opacity','1');
			else
				$('.eyesh').css('opacity','.'+this.value+'');
		});

		$('#b-intensity').on('change', function(){
			$('#b-inVal').html(this.value);

			// $("#usethis").prop('checked', false);

			if(this.value == 0)
				$('.blu').css('opacity','0');
			else if (this.value == 100)
				$('.blu').css('opacity','1');
			else
				$('.blu').css('opacity','.'+this.value+'');
		});

		$("#usethis").on("click", function() {
			var cnm =$('#client_name').val();
			var model_id =$('#makeupmid').val();
			if(cnm==''){
				alert('client name missing');
			}
			if(model_id==''){
				alert('model id missing');
			}
			if($(this).prop("checked") == true){
				var lipcolcode =$('#radioLip-value').val();
				// alert(lipcolcode);
				if(lipcolcode==''){
					alert('lip color missing');
				}
				var selectEffect=$("select[name=selectEffect]").val(); 
				var eyeshdowcolcode =$('#rdsh-value').val();
				var eyemkup =$('#rdeyemk-value').val();
				var eyeintensity =$('#s-inVal').text();
				var blushcolcode =$('#rdBlsh-value').val();	
				var blushintensity =$('#b-inVal').text();


				if(lipcolcode!='' || eyeshdowcolcode!='' || eyemkup!='' || blushcolcode!='' ){
					$.ajax({
						type:'POST',
						url:'<?php echo PIQUIC_AJAX_URL ;?>/savemkunxtskuinfo.php',
						data: {'cnm':cnm,'model_id':model_id,'lipcolcode':lipcolcode,'selectEffect':selectEffect,'eyeshdowcolcode':eyeshdowcolcode,'eyemkup':eyemkup,'eyeintensity':eyeintensity,'blushcolcode':blushcolcode,'blushintensity':blushintensity},
						success: function(savemkinfores){
						}
					});
				}else{
					$(this).prop("checked", false);
					alert("Please select at least one Makeup Property.");
				}
			}else{ // modify status flag in database
				$.ajax({
					type:'POST',
					url:'<?php echo PIQUIC_AJAX_URL ;?>/flagchngmkunxtskuinfo.php',
					data: {'cnm':cnm,'model_id':model_id},
					success: function(flagsavemkinfores){
					}
				});
			}
		});


		$("#selectEffect").on("change", function() {	

			var selectEffect=$("select[name=selectEffect]").val();
			var val =$('#radioLip-value').val();
			var makeupmidval =$('#makeupmid').val();

			// $("#usethis").prop('checked', false);

			if(val!=''){
				var status='2';
				$.ajax({
					type:'POST',
					url:'<?php echo PIQUIC_AJAX_URL ;?>/lipstickcolchange.php',
					data: {'colid':val,'selectEffect':selectEffect,'status':status,'makeupmidval':makeupmidval},
					success: function(lipimgchngres){
						// alert(lipimgchngres);
						if(lipimgchngres=='1'){
							$('#misimg').html("<div class='alert alert-danger' role='alert'>This lipstick color image is missing for this model.</div>");
							alert('missing');
						}
						else{						
							$("#lip-img").html(lipimgchngres);
						}

					}
				});	
			}else{
				alert('Please select a Lipstick Color');
			}				

		});



		$('.radioLip-group .radioLip').click(function(){
			$(this).parent().find('.radioLip div').removeClass('border');
			$(this).children('div').addClass('border');

			var val = $(this).attr('data-value');
			$(this).parent().find('input').val(val);
			var status='1';
			var makeupmidval =$('#makeupmid').val();
			$('#selectEffect').prop('selectedIndex',0);
               // alert(makeupmidval);

            // $("#usethis").prop('checked', false);

               $.ajax({
               	type:'POST',
               	url:'<?php echo PIQUIC_AJAX_URL ;?>/lipstickcolchange.php', 
               	data: {'colid':val,'status':status,'makeupmidval':makeupmidval},
               	success: function(lipimgchngres) {	
					 // alert(lipimgchngres);				
					 if(lipimgchngres=='1'){
					 	// alert('missing');
					 	$('#misimg').html("<div class='alert alert-danger' role='alert'>This lipstick color image is missing for this model.</div>");
					 }
					 else{
					 	$("#lip-img").html(lipimgchngres);
					 }
					}
				});
           });

		$('.rdeyemk-group .rdeyemk').click(function(){
			$(this).parent().find('.rdeyemk div img').removeClass('border');
			$(this).children('div').children('img').addClass('border');
			$(this).parent().find('.rdeyemk div small').removeClass('font-weight-bold');
			$(this).children('div').children('small').addClass('font-weight-bold');
			var val = $(this).attr('data-value');
			$(this).parent().find('input').val(val);
			var makeupmidval =$('#makeupmid').val();

			// $("#usethis").prop('checked', false);

			$.ajax({
				type:'POST',
				url:'<?php echo PIQUIC_AJAX_URL ;?>/eyecolorchange.php', 
				data: {'ecid':val,'makeupmidval':makeupmidval},
				success: function(eyemkimgchngres) {	              		
					// alert(eyemkimgchngres);	
					$("#eyemkimg").html(eyemkimgchngres);				
				}
			});
		});


		$('.rdsh-group .rdsh').click(function(){
			var val = $(this).attr('data-value');
			// alert(val);
			$(this).parent().find('input').val(val);
			$('#colorSlctd').css('background-color', '#'+val+'');
			$('#colorSlctd').css('color', '#'+val+'');
			var makeupmidval =$('#makeupmid').val();
			if(val!=''){
				$("#s-intensity").removeAttr('disabled');
			}	else{
				$("#s-intensity").attr('disabled', 'disabled');
			}

			// $("#usethis").prop('checked', false);

			$.ajax({
				type:'POST',
				url:'<?php echo PIQUIC_AJAX_URL ;?>/eyeshadowcolorchange.php', 
				data: {'eshcid':val,'makeupmidval':makeupmidval},
				success: function(eyeshadimgchngres) {	              		
					// alert(eyeshadimgchngres);	
					$("#s-intensity").prop('value', '100');
					$('#s-inVal').html(100);
					$("#eyeshimg").html(eyeshadimgchngres);	

				}
			});

			$.ajax({
				type:'POST',
				url:'<?php echo PIQUIC_AJAX_URL ;?>/eyeshaiconcolorchge.php', 
				data: {'eshcid':val,'makeupmidval':makeupmidval},
				success: function(eyeshaiconimgchngres) {	              		
					// alert(eyeshaiconimgchngres);	
					$('#slctNat').removeClass('d-none');
					$('#slctNot').addClass('d-none');
					$("#slctNat").html(eyeshaiconimgchngres);				
				}
			});

		});


		$('.rdBlsh-group .rdBlsh').click(function(){
			$(this).parent().find('.rdBlsh div').removeClass('border');
			$(this).children('div').addClass('border');

			var val = $(this).attr('data-value');
			$(this).parent().find('input').val(val);
			var makeupmidval =$('#makeupmid').val();
			var rdBlsh =$('#rdBlsh-value').val();

			if(rdBlsh!=''){
				$("#b-intensity").removeAttr('disabled');
			}	else{
				$("#b-intensity").attr('disabled', 'disabled');
			}

			// $("#usethis").prop('checked', false);

			$.ajax({
				type:'POST',
				url:'<?php echo PIQUIC_AJAX_URL ;?>/blushcolchange.php', 
				data: {'blshid':val,'makeupmidval':makeupmidval},
				success: function(blshimgchngres) {	
					// alert(blshimgchngres);
					$("#b-intensity").prop('value', '100');
					$('#b-inVal').html(100);
					$("#bluimg").html(blshimgchngres);
				}
			});

		});
	});

</script>