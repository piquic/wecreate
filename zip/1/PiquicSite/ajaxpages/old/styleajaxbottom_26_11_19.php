<?php   
session_start();
error_reporting(0);
include('config.php');  
 $mid = $_POST['mid'];


 $string = $_POST['my_pose_id']; 
$n=explode('poseID_', $string);
array_shift($n);  
$pose = implode('', $n);
$btmpose=array();

$btmquery = "SELECT DISTINCT `leg_position` FROM `tbl_model_pose` WHERE `mpose_id` in ($pose) ;";
$btmresult=$mysqli->query($btmquery);
while($btmrow=$btmresult->fetch_assoc()){
  $btm=$btmrow['leg_position'];
  array_push($btmpose,$btm);
}
$btmposestr = implode(',', $btmpose);
// print_r($btmpose);

$mstquery= "SELECT * FROM `tbl_model` WHERE m_id='$mid' ";
 $result=$mysqli->query($mstquery);   
 $row=$result->fetch_assoc();           
 $mod_body = $row['mod_body'];

 $mstquery2= "SELECT * FROM `tbl_model` WHERE mod_name='$mod_body' ";
 $result2=$mysqli->query($mstquery2);   
 $row2=$result2->fetch_assoc();           
 $mid = $row2['m_id'];

if (in_array("spread-legs", $btmpose)) {
  // echo 'spread-legs';

  $bquery = "SELECT * FROM `tbl_bottom` where  bottom IN ('MICRO','MINI')  AND model_id='$mid' AND status='1' AND psd_exist='1'ORDER by `bot_id`;"; 
}elseif (in_array("upper-leg-straight", $btmpose)) {
  // echo 'upper-leg-straight';

  $bquery = "SELECT * FROM `tbl_bottom` where  bottom IN ('MICRO','MINI','KNEE LENGTH')  AND model_id='$mid' AND status='1' AND psd_exist='1'ORDER by `bot_id`;"; 
}elseif (in_array("cross-legs", $btmpose)) {
  // echo 'cross-legs';
  $bquery = "SELECT * FROM `tbl_bottom` where  bottom IN ('MICRO','MINI','KNEE LENGTH','BELOW KNEE')  AND model_id='$mid' AND status='1' AND psd_exist='1'ORDER by `bot_id`;"; 

}elseif (in_array("straight-legs", $btmpose)){
// echo 'straight-legs';

  $bquery = "SELECT * FROM `tbl_bottom` where  bottom IN ('MICRO','MINI','KNEE LENGTH','BELOW KNEE','ANKLE LENGTH','FLOOR LENGTH')  AND model_id='$mid' AND status='1' AND psd_exist='1'ORDER by `bot_id`;";
}else{
// echo 'All';
 $bquery = "SELECT * FROM `tbl_bottom` where  model_id='$mid' AND status='1' ORDER by `bot_id`;";
}


$bresult=$mysqli->query($bquery);              
$row_cnt = $bresult->num_rows;  
if ($row_cnt>0){
  ?>   


  <div class="row pt-3 pb-3">

    <div class="col-6 col-md-6">
      <!--  <div class="custom-control custom-checkbox">
         <input type="checkbox" class="custom-control-input" id="showExtraBottom">
         <label class="custom-control-label" for="showExtraBottom">Show Extras</label>
       </div> -->
     </div>

     <div class="col-6 col-md-6">                 

      <select name="bottomWear" id="bottomWear" class="form-control" disabled="disabled">
        <option value="0" selected>All Bottom</option>
        <option value="jeans" >Jeans</option>
        <option value="trousers">Trousers</option>

      </select>                   

    </div>

  </div>
  <div class="row" >


   <?php  

   while($brow=$bresult->fetch_assoc()){
    $bimgname=explode('-', $brow['bottom_img']);
    $bimgname=$bimgname[1].'-'.$bimgname[2];
     ?>


     <div class="col-6 col-sm-6 col-md-4 col-lg-4 col-xl-4 mb-3" onclick="bottomclick(<?php echo $brow['bot_id'];?>,'<?php echo PIQUIC_URL ;?>images/bottom/<?php echo $brow['bottom_img'];?>');bottomvalidation('<?php echo $brow['bottom_img'];?>');bottomimg(<?php echo $brow['bot_id'];?>,'<?php echo PIQUIC_URL ;?>images/bottom/<?php echo $brow['bottom_img'];?>');">
      <!-- <div class="border" style="background: url(images/bottom/<?php //echo $brow['bottom_img'];?>) no-repeat center center; background-size: cover; height: 18rem;"></div> -->

      <div class="w-100 border" style="height: 14.5rem; overflow: hidden;">
        <img src="<?php echo PIQUIC_URL ;?>images/bottom/<?php echo $brow['bottom_img'];?>"class="loadingBottom btmView">
      </div>
      <span style="font-size: .74rem;"><?php echo $bimgname;?></span>
      <div class="custom-control custom-radio c-box">
        <input type="radio" class="custom-control-input" name="bottom" value="1" id="bottomID<?php echo $brow['bot_id'];?>" onclick="bottomclick(<?php echo $brow['bot_id'];?>,'<?php echo PIQUIC_URL ;?>images/bottom/<?php echo $brow['bottom_img'];?>');bottomimg(<?php echo $brow['bot_id'];?>,'<?php echo PIQUIC_URL ;?>images/bottom/<?php echo $brow['bottom_img'];?>');bottomvalidation('<?php echo $brow['bottom_img'];?>');">
        <label class="custom-control-label" for="bottomID<?php echo $brow['bot_id'];?>">&nbsp;</label>
      </div>
    </div>              

  <?php } ?>



</div>  

<?php }else{    ?>    

 <div class="col-md-12 mt-3 text-center text-danger lead font-weight-bold">
  No Bottom Available for this Model.
</div>  
<?php   }


?>