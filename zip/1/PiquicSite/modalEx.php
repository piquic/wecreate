<div class="modal fade" id="modal_msg" tabindex="-1" role="dialog" aria-hidden="true">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<h5 class="modal-title text-dark" id="txtMsg"></h5>
					<button type="button" class="close text-danger" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true" >&times;</span>
					</button>
				</div>
			</div>
		</div>
	</div>

<div class="modal fade" id="modal_foldername" tabindex="-1" role="dialog" aria-hidden="true"> <div class="modal-dialog" role="document"> <div class="modal-content"> <div class="modal-header"> <h5 class="modal-title text-dark" id="modMsg"></h5> <button type="button" class="close text-danger" data-dismiss="modal" aria-label="Close"> <span aria-hidden="true" >&times;</span> </button> </div> <div class="modal-footer"> <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button> <button type="button" id="hideDel" class="btn btn-danger" onclick="sku_del('<?php  echo $sku; ?>','<?php  echo $sty_id; ?>','<?php  echo $status; ?>');">Delete</button> </div> </div> </div> </div>

$('#modal_msg').modal('show');
$('#txtMsg').html('Data Inserted.<br>Redirecting to the queue...');