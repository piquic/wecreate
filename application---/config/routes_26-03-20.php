<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	http://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There area two reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router what URI segments to use if those provided
| in the URL cannot be matched to a valid route.
|
*/
/*backend*/
/*$route['default_controller'] = 'login';
$route['admin_login'] = "login";
$route['logout'] = 'dashboard/logout';
$route['404_override'] = '';
$route['dashboard'] = 'dashboard';*/


////////////////////////////////ADMIN////////////////////////////////////////

$route['administrator'] = 'admin/administrator/index';
$route['administrator/check_login'] = 'admin/administrator/check_login';
$route['administrator/dashboard'] = 'admin/administrator/dashboard';


$route['administrator/manage-usertype'] = 'admin/usertype/index';
$route['administrator/add-usertype'] ='admin/usertype/add';
$route['administrator/view-usertype/(:any)/(:any)'] ='admin/usertype/add/$1/$2';
$route['administrator/edit-usertype/(:any)'] ='admin/usertype/add/$1';
$route['administrator/update-usertype/(:any)'] ='admin/usertype/update_user/$1';



$route['administrator/manage-module'] = 'admin/module/index';
$route['administrator/add-module'] ='admin/module/add';
$route['administrator/view-module/(:any)/(:any)'] ='admin/module/add/$1/$2';
$route['administrator/edit-module/(:any)'] ='admin/module/add/$1';
$route['administrator/update-module/(:any)'] ='admin/module/update_module/$1';


$route['administrator/manage-module-permission'] = 'admin/modulepermission/index';
$route['administrator/update-module-permission'] = 'admin/modulepermission/update_module_permission';

$route['administrator/add-module-permission'] ='admin/modulepermission/add';
$route['administrator/view-module-permission/(:any)/(:any)'] ='admin/modulepermissionmodulepermission/add/$1/$2';
$route['administrator/edit-module-permission/(:any)'] ='admin/modulepermission/add/$1';
$route['administrator/update-module-permission/(:any)'] ='admin/modulepermission/update_module/$1';




$route['administrator/manage-user'] = 'admin/user/index';
$route['administrator/add-user'] ='admin/user/add';
$route['administrator/view-user/(:any)/(:any)'] ='admin/user/add/$1/$2';
$route['administrator/edit-user/(:any)'] ='admin/user/add/$1';
$route['administrator/update-user/(:any)'] ='admin/user/update_user/$1';

$route['administrator/manage-brand'] = 'admin/brand/index';
$route['administrator/add-brand'] ='admin/brand/add';
$route['administrator/view-brand/(:any)/(:any)'] ='admin/brand/add/$1/$2';
$route['administrator/edit-brand/(:any)'] ='admin/brand/add/$1';
$route['administrator/update-brand/(:any)'] ='admin/brand/update_user/$1';





$route['administrator/manage-page-content'] = 'admin/page/index';
$route['administrator/add-page'] ='admin/page/add';
$route['administrator/view-page/(:any)/(:any)'] ='admin/page/add/$1/$2';
$route['administrator/edit-page/(:any)'] ='admin/page/add/$1';
$route['administrator/update-page/(:any)'] ='admin/page/update_user/$1';




















////////////////////////////////FRONT////////////////////////////////////////


$route['default_controller'] = 'front/login';



$route['login'] = 'front/login/index';

$route['logout'] = 'front/login/logout';
$route['reset_password/(:any)'] = 'front/home/reset_password/$1';

$route['uploadbrief'] = 'front/uploadbrief/index';

$route['dashboard/(:any)/(:any)/(:any)'] = 'front/dashboard/index/$1/$2/$3';
$route['dashboard/(:any)/(:any)'] = 'front/dashboard/index/$1/$2';
$route['dashboard/(:any)'] = 'front/dashboard/index/$1';
$route['dashboard'] = 'front/dashboard/index';


$route['page-content/(:any)/(:any)/(:any)'] = 'front/home/pageContent/$1/$2/$3';
$route['page-content/(:any)/(:any)'] = 'front/home/pageContent/$1/$2';

/*
$route['view-files'] = 'front/view_files/index';
$route['uploadimages'] = 'front/uploadimages/index';*/
$route['view-files/(:any)'] = 'front/view_files/index/$1';
$route['view-files'] = 'front/view_files/index';


$route['uploadimages/(:any)'] = 'front/uploadimages/index/$1';
$route['uploadimages'] = 'front/uploadimages/index';



$route['open-proofing/(:any)'] = 'front/proofing/index/$1';

