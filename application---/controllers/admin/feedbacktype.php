<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Feedbacktype extends CI_Controller
{
    public function __construct()
	{
		parent::__construct();		
		$this->load->library('pagination');
		$this->load->database();		
		$this->load->library('Upload');
		$this->load->helper('security');	
		$this->load->library('Form_validation');
		$this->load->helper(array('form', 'html', 'file'));
		$this->load->library('pagination');
		$this->load->library('email');
        $this->load->model('feedbacktype_model');
        $this->load->library('userlib');
		
    	if ($this->session->userdata('logged_in')) {
            $session_data = $this->session->userdata('logged_in');
            $data['username'] = $session_data['username'];
            $data['firstname'] = $session_data['firstname'];
            $data['rolecode'] = $session_data['rolecode'];
			/* $timezone = $session_data['timezone'];
			date_default_timezone_set($timezone); */
         
        } else {
            //If no session, redirect to login page
            /*redirect('login', 'refresh');*/
            redirect('administrator', 'refresh');
        }
    }
	
    public function index()
	{
		if ($this->session->userdata('logged_in'))		
		{  
			$session_data = $this->session->userdata('logged_in');
			$data['rolecode'] = $session_data['rolecode'];
			$data['feedbacktype'] = $session_data['rolecode'];
			$data['roles'] = $session_data['roles'];
			$data['title'] = "Users";
			$slang="english";
			$this->lang->load($slang, $slang);
			$data['lang']=$this->lang->language; 
			
			//echo "aaaaaaaaa";exit;
			$this->load->view('admin/feedbacktype/feedbacktype_list', $data);
		} 
		else 
		{
			//If no session, redirect to login page
			/*redirect('login', 'refresh');*/
            redirect('administrator', 'refresh');
		}
	}
	
	public function feedbacktypelist(){
		if ($this->session->userdata('logged_in')) 
        {
			$session_data=$this->session->userdata('logged_in');
			$feedbacktype = $session_data['rolecode'];
			$userid = $session_data['id'];
			$slang="english";
		    $this->lang->load($slang, $slang);
		    $data['lang']=$this->lang->language; 
    	    $aColumns = array( 'feedback_type_id','feedback_type','status');
         
			/* Indexed column (used for fast and accurate table cardinality) */
			$sIndexColumn = "feedback_type_id";
         
			/* DB table to use */
			$sTable = "wc_feedback_type";

            /*
			 * Paging
			 */
			$sLimit = "";
			if ( isset( $_GET['start'] ) && $_GET['length'] != '-1' )
			{
				$sLimit = "LIMIT ".intval( $_GET['start'] ).", ".
					intval( $_GET['length'] );
			}


    
     
			$sOrder = "";
			if ( isset( $_GET['order'] ) )
			{
				$sOrder = "ORDER BY  ";
			
						$sOrder .= $aColumns[0]."
							".($_GET['order'][0]['dir']==='desc' ? 'asc' : 'desc') .", ";
			   
				 
				$sOrder = substr_replace( $sOrder, "", -2 );
				if ( $sOrder == "ORDER BY" )
				{
					$sOrder = "";
				}
			}
	

        
        $commn="1=1 and deleted='0' ";
        
		
		//echo $_REQUEST['columns'][1]['search']['value']; exit();
		
		if(!empty($_GET['columns'][1]['search']['value']) ){  
	
		$commn.=" AND wc_feedback_type.feedback_type_id  LIKE '".$_GET['columns'][1]['search']['value']."%' ";
		}

        $sWhere="where $commn";
         
		
		if (isset($_GET['search']['value']) && $_GET['search']['value'] != "" )
		{
			 $sWhere = "WHERE $commn   AND (";
			for ( $i=0 ; $i<count($aColumns) ; $i++ )
			{
				
				/*if ( isset($_GET['columns'][$i]['searchable']) && $_GET['columns'][$i]['searchable'] == "true" )
				{*/
					$sWhere .= $aColumns[$i]." LIKE '%".( $_GET['search']['value'] )."%' OR ";
				/*}*/
			}
			$sWhere = substr_replace( $sWhere, "", -3 );
			$sWhere .= ')';
		}
		
		
       $join='';
        /*
         * SQL queries
         * Get data to display
         */
        $sQuery = "
            SELECT SQL_CALC_FOUND_ROWS ".str_replace(" , ", " ", implode(", ", $aColumns))."
            FROM   $sTable $join
            $sWhere
            $sOrder
            $sLimit";

       //echo  $sQuery; exit();

        $rResult = $this->db->query($sQuery);
        $rResult = $rResult->result_array();
        //$rResult = mysqli_query( $sQuery) or fatal_error( 'MySQL Error: ' . mysqli_errno() );

        
        /* Data set length after filtering */
        //$sQuery = "SELECT FOUND_ROWS()";
        //$rResultFilterTotal = mysqli_query( $sQuery) or fatal_error( 'MySQL Error: ' . mysqli_errno() );
        //$query = $this->db->query($sQuery);
        //$aResultFilterTotal = $query->result_array();
        //$aResultFilterTotal = mysqli_fetch_array($rResultFilterTotal);
       $iFilteredTotal = count( $rResult);
          
        /* Total data set length */
        $sQuery = "
            SELECT COUNT(".$sIndexColumn.") as count
            FROM   $sTable $join
            $sWhere
        ";
        //$rResultTotal = mysqli_query( $sQuery) or fatal_error( 'MySQL Error: ' . mysqli_errno() );
        //$aResultTotal = mysqli_fetch_array($rResultTotal);

        $query = $this->db->query($sQuery);
        $aResultTotal = $query->result_array();

        //echo "<pre>";
        //print_r($aResultTotal);
         $iTotal = $aResultTotal[0]['count'];
         
        /*
         * Output
         */
        $output = array(
                        "sEcho" => '',
                        "iTotalRecords" => $iFilteredTotal,
                        "iTotalDisplayRecords" => $iTotal,
                        "aaData" => array()
                        );

            $sColumns = array('feedback_type','status','action');
         
            //while ( $aRow = mysqli_fetch_array( $rResult ) )
            //echo "<pre>";
           //print_r($output);
         
            foreach($rResult as $aRow)
            {
                $row = array();
            	
				$feedback_type_id=$aRow['feedback_type_id'];
				$status=$aRow['status'];
                for ( $i=0 ; $i<count($sColumns) ; $i++ )
                {
                  
            	    if($sColumns[$i] =='action')
            		{
            			

						$row[]='<a href="javascript:void(0)"  class="btn btn-success view" val="'.$feedback_type_id.'"><i class="fas fa-eye"></i></a>&nbsp;<a href="javascript:void(0)"  class="btn btn-info edit" val="'.$feedback_type_id.'"><i class="fas fa-edit"></i></a>&nbsp; <a href="javascript:void(0)"  class="btn btn-danger delete" val="'.$feedback_type_id.'"><i class="fas fa-trash-alt"></i></a>
        			  ';
						
            		}
					
				
					else if($sColumns[$i] =='status')
            		{
            			
            			 $Active_status="";
			             $Inactive_status="";
						 
						 
					    if($status=='Active')
						{
						 $Active_status="selected";
			             $Inactive_status="";
						
						}
						else if($status=='Inactive')
						{
						 $Active_status="";
			             $Inactive_status="selected";
						 
						}
						
            			
            			 $row[]='<select name="status" id="status" class="selectpicker update_status">
								 <option value="Active@@@'.$feedback_type_id.'"  '.$Active_status.'>Active</option>
								 <option value="Inactive@@@'.$feedback_type_id.'"  '.$Inactive_status.'>Inactive</option>
								 </select>';
            			 
            		}
                    else if ( $sColumns[$i] != ' ' )
                    {
                        /* General output */
                        $row[] = $aRow[ $sColumns[$i] ];
                    }
                }
                $output['aaData'][] = $row;
            }
            //print_r($output);exit;
        echo json_encode( $output );
    }
	}
	
	
	public function add($feedback_type_id=''){
		
		if ($this->session->userdata('logged_in'))		
		{  
			$session_data = $this->session->userdata('logged_in');
			$slang="english";
		    $this->lang->load($slang, $slang);
		    $data['lang']=$this->lang->language; 
			$data['title'] = "User Role";
			if($feedback_type_id!=''){
				$feedbacktype_details=$this->db->query("select * from wc_feedback_type where feedback_type_id='$feedback_type_id'")->result_array();
				if(!empty($feedbacktype_details)){
					$data['feedback_type_id'] = $feedbacktype_details[0]['feedback_type_id'];
					$data['feedback_type'] = $feedbacktype_details[0]['feedback_type'];
					$data['status'] = $feedbacktype_details[0]['status'];
					$data['deleted'] = $feedbacktype_details[0]['deleted'];
					
					
				}
			}else {
			        $data['feedback_type_id'] = '';
					$data['feedback_type'] = '';
					$data['status'] = '';
					$data['deleted'] = '';
					
					
			}
			$this->load->view('admin/feedbacktype/feedbacktype_management', $data);
		} 
		else 
		{
			//If no session, redirect to login page
			/*redirect('login', 'refresh');*/
            redirect('administrator', 'refresh');
		}
	}
	
	

	
	public function insert_feedbacktype(){
		
		if ($this->session->userdata('logged_in')) {
			$session_data = $this->session->userdata('logged_in');
			$created_date = date('Y-m-d');
			$userid=$session_data['id'];
			$feedback_type_id=$this->input->post('feedback_type_id');
			
			if($feedback_type_id!='')
			{
				
				  $data = array(
				    'feedback_type_id'=>$this->input->post('feedback_type_id'),
					'feedback_type'=>$this->input->post('feedback_type'),
					'status'=>$this->input->post('status'),
					);
					
				  $updateuserrole = $this->feedbacktype_model->feedbacktype_update($data,$feedback_type_id);
			      echo $feedback_type_id;
				
			}else {
				
				
				
				   $data = array(
				    'feedback_type_id'=>$this->input->post('feedback_type_id'),
					'feedback_type'=>$this->input->post('feedback_type'),
					'status'=>$this->input->post('status'),
					
					);
					$this->feedbacktype_model->feedbacktype_insert($data);
					$feedback_type_id=$this->db->insert_id();
					
					
					echo $feedback_type_id;
				}
			
		}else {
		
            /*redirect('login', 'refresh');*/
            redirect('administrator', 'refresh');
       
		}  
	}
		
		
		
		

 
	public function deletefeedbacktype()
    {
		if ($this->session->userdata('logged_in')) 
		{
			$feedback_type_id = $this->input->post('feedback_type_id');
		
			//$this->db->query("delete from user where user_id='$user_id'");
			$this->db->query("update wc_feedback_type set deleted ='1',status='Inactive' where feedback_type_id ='$feedback_type_id'");
			$this->session->set_flashdata('added', '<div align="left" style="color:green; font-size:15px; border:1px #398AB9 solid;">
		    Service Deleted Successfully</div>');

			//redirect('users');
		} 
		else 
		{
			//If no session, redirect to login page
			/*redirect('login', 'refresh');*/
            redirect('administrator', 'refresh');
		}
	}
	
	

	public function view($feedback_type_id=''){
		
		if ($this->session->userdata('logged_in'))		
		{  
			$session_data = $this->session->userdata('logged_in');
			$slang="english";
		    $this->lang->load($slang, $slang);
		    $data['lang']=$this->lang->language; 
			$data['title'] = "User Role";
			if($feedback_type_id!=''){
				$feedbacktype_details=$this->db->query("select * from wc_feedback_type where feedback_type_id='$feedback_type_id'")->result_array();
				if(!empty($feedbacktype_details)){
					$data['feedback_type_id'] = $feedbacktype_details[0]['feedback_type_id'];
					$data['feedback_type'] = $feedbacktype_details[0]['feedback_type'];
					$data['status'] = $feedbacktype_details[0]['status'];
					$data['deleted'] = $feedbacktype_details[0]['deleted'];
					
					
				}
			}else {
			        $data['feedback_type_id'] = '';
					$data['feedback_type'] = '';
					$data['status'] = '';
					$data['deleted'] = '';
					
					
			}
			$this->load->view('admin/feedbacktype/feedbacktype_view', $data);
		} 
		else 
		{
			//If no session, redirect to login page
			/*redirect('login', 'refresh');*/
            redirect('administrator', 'refresh');
		}
	}



	public function updatestatus()
	{				//for email exist validation in edit customer form 


	$id=trim($this->input->get('id'));
	$status=trim($this->input->get('status'));
	//echo "update wc_feedback_type set status ='$status' where feedback_type_id ='$id'";
	$this->db->query("update wc_feedback_type set status ='$status' where feedback_type_id ='$id'");



	}

	
}
?>