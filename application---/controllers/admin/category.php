<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
 
class category extends CI_Controller
{
    public function __construct()
	{
		parent::__construct();		
		$this->load->library('pagination');
		$this->load->database();		
		$this->load->library('Upload');
		$this->load->helper('security');	
		$this->load->library('Form_validation');
		$this->load->helper(array('form', 'html', 'file'));
		$this->load->library('pagination');
		$this->load->library('email');
        $this->load->model('Category_model');
        $this->load->library('userlib');
		
    	if ($this->session->userdata('logged_in')) {
            $session_data = $this->session->userdata('logged_in');
            $data['username'] = $session_data['username'];
            $data['firstname'] = $session_data['firstname'];
            $data['rolecode'] = $session_data['rolecode'];
			/* $timezone = $session_data['timezone'];
			date_default_timezone_set($timezone); */
         
        } else {
            //If no session, redirect to login page
            /*redirect('login', 'refresh');*/
            redirect('administrator', 'refresh');
        }
    }
	
    public function index()
	{
		if ($this->session->userdata('logged_in'))		
		{  
			$session_data = $this->session->userdata('logged_in');
			$data['rolecode'] = $session_data['rolecode'];
			$data['usertype'] = $session_data['rolecode'];
			$data['roles'] = $session_data['roles'];
			$data['title'] = "Users";
			$slang="english";
			$this->lang->load($slang, $slang);
			$data['lang']=$this->lang->language; 
			
			//echo "aaaaaaaaa";exit;
			$this->load->view('admin/category/category_list', $data);
		} 
		else 
		{
			//If no session, redirect to login page
			/*redirect('login', 'refresh');*/
            redirect('administrator', 'refresh');
		}
	}
	
	public function categorylist(){
		if ($this->session->userdata('logged_in')) 
        {
			$session_data=$this->session->userdata('logged_in');
			$usertype = $session_data['rolecode'];
			$userid = $session_data['id'];
			$slang="english";
		    $this->lang->load($slang, $slang);
		    $data['lang']=$this->lang->language; 
    	    $aColumns = array( 'wc_category.category_id','wc_category.category_name','wc_clients.client_name','wc_category.status');
         
			/* Indexed column (used for fast and accurate table cardinality) */
			$sIndexColumn = "category_id";
         
			/* DB table to use */
			$sTable = "wc_category";

            /*
			 * Paging
			 */
			$sLimit = "";
			if ( isset( $_GET['start'] ) && $_GET['length'] != '-1' )
			{
				$sLimit = "LIMIT ".intval( $_GET['start'] ).", ".
					intval( $_GET['length'] );
			}


    
     
			$sOrder = "";
			if ( isset( $_GET['order'] ) )
			{
				$sOrder = "ORDER BY  ";
			
						$sOrder .= $aColumns[0]."
							".($_GET['order'][0]['dir']==='desc' ? 'asc' : 'desc') .", ";
			   
				 
				$sOrder = substr_replace( $sOrder, "", -2 );
				if ( $sOrder == "ORDER BY" )
				{
					$sOrder = "";
				}
			}
	

        
        $commn="1=1 and wc_category.deleted='0' ";
        
		
		//echo $_REQUEST['columns'][1]['search']['value']; exit();
		
		if(!empty($_GET['columns'][1]['search']['value']) ){  
	
		$commn.=" AND wc_category.category_name  LIKE '".$_GET['columns'][1]['search']['value']."%' ";
		}

        $sWhere="where $commn";
         
		
		if (isset($_GET['search']['value']) && $_GET['search']['value'] != "" )
		{
			 $sWhere = "WHERE $commn   AND (";
			for ( $i=0 ; $i<count($aColumns) ; $i++ )
			{
				
				/*if ( isset($_GET['columns'][$i]['searchable']) && $_GET['columns'][$i]['searchable'] == "true" )
				{*/
					$sWhere .= $aColumns[$i]." LIKE '%".( $_GET['search']['value'] )."%' OR ";
				/*}*/
			}
			$sWhere = substr_replace( $sWhere, "", -3 );
			$sWhere .= ')';
		}
		
		
       $join=' INNER JOIN wc_clients ON wc_category.client_id=wc_clients.client_id';
        /*
         * SQL queries
         * Get data to display
         */
        $sQuery = "
            SELECT SQL_CALC_FOUND_ROWS ".str_replace(" , ", " ", implode(", ", $aColumns))."
            FROM   $sTable $join
            $sWhere
            $sOrder
            $sLimit";

       //echo  $sQuery; exit();

        $rResult = $this->db->query($sQuery);
        $rResult = $rResult->result_array();
        //$rResult = mysqli_query( $sQuery) or fatal_error( 'MySQL Error: ' . mysqli_errno() );

        
        /* Data set length after filtering */
        //$sQuery = "SELECT FOUND_ROWS()";
        //$rResultFilterTotal = mysqli_query( $sQuery) or fatal_error( 'MySQL Error: ' . mysqli_errno() );
        //$query = $this->db->query($sQuery);
        //$aResultFilterTotal = $query->result_array();
        //$aResultFilterTotal = mysqli_fetch_array($rResultFilterTotal);
       $iFilteredTotal = count( $rResult);
          
        /* Total data set length */
        $sQuery = "
            SELECT COUNT(".$sIndexColumn.") as count
            FROM   $sTable $join
            $sWhere
        ";
        //$rResultTotal = mysqli_query( $sQuery) or fatal_error( 'MySQL Error: ' . mysqli_errno() );
        //$aResultTotal = mysqli_fetch_array($rResultTotal);

        $query = $this->db->query($sQuery);
        $aResultTotal = $query->result_array();

        //echo "<pre>";
        //print_r($aResultTotal);
         $iTotal = $aResultTotal[0]['count'];
         
        /*
         * Output
         */
        $output = array(
                        "sEcho" => '',
                        "iTotalRecords" => $iFilteredTotal,
                        "iTotalDisplayRecords" => $iTotal,
                        "aaData" => array()
                        );

            $sColumns = array('client_name','category_name','status','action');
         
            //while ( $aRow = mysqli_fetch_array( $rResult ) )
            //echo "<pre>";
           //print_r($output);
         
            foreach($rResult as $aRow)
            {
                $row = array();
            	
				$category_id=$aRow['category_id'];
				//$category_type_id=$aRow['category_type_id'];
				//$category_type=$aRow['usertype_id'];
				//$business_id=$aRow['business_id'];
				$status=$aRow['status'];
                for ( $i=0 ; $i<count($sColumns) ; $i++ )
                {
                   /*  if ( $sColumns[$i] == "version" )
                    {
                       
                        $row[] = ($aRow[ $sColumns[$i] ]=="0") ? '-' : $aRow[ $sColumns[$i] ];
                    } */
					
            	    if($sColumns[$i] =='action')
            		{
            			

						$row[]='<a href="javascript:void(0)"  class="btn btn-success view" val="'.$category_id.'"><i class="fas fa-eye"></i></a>&nbsp;<a href="javascript:void(0)"  class="btn btn-info edit" val="'.$category_id.'"><i class="fas fa-edit"></i></a>&nbsp; <a href="javascript:void(0)"  class="btn btn-danger delete" val="'.$category_id.'"><i class="fas fa-trash-alt"></i></a>
        			  ';
						
            		}
					
					
					
					
					
					
					
					
					
					else if($sColumns[$i] =='status')
            		{
            			
            			 $Active_status="";
			             $Inactive_status="";
						 
						 
					    if($status=='Active')
						{
						 $Active_status="selected";
			             $Inactive_status="";
						
						}
						else if($status=='Inactive')
						{
						 $Active_status="";
			             $Inactive_status="selected";
						 
						}
						
            			
            			 $row[]='<select name="status" id="status" class="selectpicker update_status">
								 <option value="Active@@@'.$category_id.'"  '.$Active_status.'>Active</option>
								 <option value="Inactive@@@'.$category_id.'"  '.$Inactive_status.'>Inactive</option>
								 </select>';
            			 
            		}
                    else if ( $sColumns[$i] != ' ' )
                    {
                        /* General output */
                        $row[] = $aRow[ $sColumns[$i] ];
                    }
                }
                $output['aaData'][] = $row;
            }
            //print_r($output);exit;
        echo json_encode( $output );
    }
	}
	
	
	public function add($category_id=''){
		
		if ($this->session->userdata('logged_in'))		
		{  
			$session_data = $this->session->userdata('logged_in');
			$slang="english";
		    $this->lang->load($slang, $slang);
		    $data['lang']=$this->lang->language; 
			$data['title'] = "User Role";
			if($category_id!=''){
				$category_details=$this->db->query("select * from wc_category where category_id='$category_id'")->result_array();
				if(!empty($category_details)){
					$data['category_id'] = $category_details[0]['category_id'];
					$data['client_id_sel'] = $category_details[0]['client_id'];
					$data['category_name'] = $category_details[0]['category_name'];
					$data['status'] = $category_details[0]['status'];
					
					
				}
			}else {
			        $data['category_id'] = '';
					$data['client_id_sel'] = '';
					$data['category_name'] = '';
					$data['status'] = '';
					
			}
			$this->load->view('admin/category/category_management', $data);
		} 
		else 
		{
			//If no session, redirect to login page
			/*redirect('login', 'refresh');*/
            redirect('administrator', 'refresh');
		}
	}
	
	
		
	  public function checkexistemail()
{			// checking the email  exist  validation in add form
	
    $category_email = $this->input->post('category_email');
    $sql = "SELECT category_id,category_email FROM wc_category WHERE category_email = '$category_email'";
    $query = $this->db->query($sql);
    if( $query->num_rows() > 0 ){
        echo 'false';
    } else {
        echo 'true';
    }
	
}

   public function checkexistmobile()
{			// checking the email  exist  validation in add form
	
    $client_phone = $this->input->post('client_phone');
    $sql = "SELECT category_id,client_phone FROM wc_category WHERE client_phone = '$client_phone'";
    $query = $this->db->query($sql);
    if( $query->num_rows() > 0 ){
        echo 'false';
    } else {
        echo 'true';
    }
	
}
	
	public function insert_category(){
		
		if ($this->session->userdata('logged_in')) {
			$session_data = $this->session->userdata('logged_in');
			$created_date = date('Y-m-d');
			$userid=$session_data['id'];
			$category_id=$this->input->post('category_id');
			$client_id=$this->input->post('client_id');
			$category_name=$this->input->post('category_name');
			
			if($category_id!='')
			{
				
				  $data = array(
				    'category_id'=>$this->input->post('category_id'),
					'client_id'=>$this->input->post('client_id'),
					'category_name'=>$this->input->post('category_name'),
					'status'=>$this->input->post('status'),
					);
					$client_query= $this->db->query("SELECT * FROM wc_category WHERE category_id='$category_id'");
                $client_rs= $client_query->result_array(); 

                $oldclient_id=$client_rs[0]['client_id'];
                $oldcategory_name=preg_replace('/[^a-zA-Z0-9_.]/', '_',$client_rs[0]['category_name']);

                if($oldclient_id==$client_id){

                	$client_query1= $this->db->query("SELECT * FROM wc_clients WHERE client_id='$client_id'");
	                $client_rs1= $client_query1->result_array(); 

	                $client_name=preg_replace('/[^a-zA-Z0-9_.]/', '_',$client_rs1[0]['client_name']);
                	/*$old_category_name=$_SERVER['DOCUMENT_ROOT']."/"."WeCreate/upload/".$client_id."-".$client_name."/".$category_id."-".$oldcategory_name;
                	$new_category_name=$_SERVER['DOCUMENT_ROOT']."/"."WeCreate/upload/".$client_id."-".$client_name."/".$category_id."-".$category_name;*/


                	$old_category_name=$_SERVER['DOCUMENT_ROOT']."/".FOLDER_NAME."/upload/".$client_id."-".$client_name."/".$category_id."-".$oldcategory_name;
                	$new_category_name=$_SERVER['DOCUMENT_ROOT']."/".FOLDER_NAME."/upload/".$client_id."-".$client_name."/".$category_id."-".$category_name;




                	if(rename( $old_category_name, $new_category_name)){ 
	                   // echo "Successfully Renamed $old_client_name to $new_client_name" ;
	                }else{
	                    //echo "A File With The Same Name Already Exists" ;
	                }
                }
                if($oldclient_id!=$client_id){
                	$client_query1= $this->db->query("SELECT * FROM wc_clients WHERE client_id='$oldclient_id'");
	                $client_rs1= $client_query1->result_array();
	                $oldclient_name=preg_replace('/[^a-zA-Z0-9_.]/', '_',$client_rs1[0]['client_name']);

	                $new_client_query= $this->db->query("SELECT * FROM wc_clients WHERE client_id='$client_id'");
	                $new_client_rs= $new_client_query->result_array(); 
	                $newclient_name=preg_replace('/[^a-zA-Z0-9_.]/', '_',$new_client_rs[0]['client_name']);


	                // echo $_SERVER['DOCUMENT_ROOT']."WeCreate/upload/".$client_id."-".$client_rs[0]['client_name'];
	                /*$old_client_name=$_SERVER['DOCUMENT_ROOT']."/"."WeCreate/upload/".$oldclient_id."-".$oldclient_name;
	                $new_client_name=$_SERVER['DOCUMENT_ROOT']."/"."WeCreate/upload/".$client_id."-".$newclient_name;*/

	                $old_client_name=$_SERVER['DOCUMENT_ROOT']."/".FOLDER_NAME."/upload/".$oldclient_id."-".$oldclient_name;
	                $new_client_name=$_SERVER['DOCUMENT_ROOT']."/".FOLDER_NAME."/upload/".$client_id."-".$newclient_name;

	                if(rename( $old_client_name, $new_client_name)){ 
	                    /*$old_category_name=$_SERVER['DOCUMENT_ROOT']."/"."WeCreate/upload/".$client_id."-".$newclient_name."/".$category_id."-".$oldcategory_name;
	                	$new_category_name=$_SERVER['DOCUMENT_ROOT']."/"."WeCreate/upload/".$client_id."-".$newclient_name."/".$category_id."-".$category_name;*/

	                	$old_category_name=$_SERVER['DOCUMENT_ROOT']."/".FOLDER_NAME."/upload/".$client_id."-".$newclient_name."/".$category_id."-".$oldcategory_name;
	                	$new_category_name=$_SERVER['DOCUMENT_ROOT']."/".FOLDER_NAME."/upload/".$client_id."-".$newclient_name."/".$category_id."-".$category_name;

	                	if(rename( $old_category_name, $new_category_name)){ 
		                   // echo "Successfully Renamed $old_client_name to $new_client_name" ;
		                }else{
		                    //echo "A File With The Same Name Already Exists" ;
		                }
	                }else{
	                    //echo "A File With The Same Name Already Exists" ;
	                }
                }
               
				  $updateuserrole = $this->Category_model->category_update($data,$category_id);
			      echo $category_id;
				
			}else {
				
				
				
				    $data = array(
				    'category_id'=>$this->input->post('category_id'),
					'client_id'=>$this->input->post('client_id'),
					'category_name'=>$this->input->post('category_name'),
					'status'=>$this->input->post('status'),
					);
					$this->Category_model->category_insert($data);
					$category_id=$this->db->insert_id();
					
					
					echo $category_id;
				}
			
		}else {
		
            /*redirect('login', 'refresh');*/
            redirect('administrator', 'refresh');
       
		}  
	}
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
	public function change_password(){
		
		if ($this->session->userdata('logged_in'))		
		{  
			$session_data = $this->session->userdata('logged_in');
			$slang="english";
		    $this->lang->load($slang, $slang);
		    $data['lang']=$this->lang->language; 
			
			$category_id = $session_data['id'];
			$data['category_id'] = $category_id;
			$data['title'] = "User Role";
			if($category_id!=''){
				$category_details=$this->db->query("select * from wc_category where category_id='$category_id'")->result_array();
				if(!empty($category_details)){}
			}else {}
			$this->load->view('admin/category/category_password', $data);
		} 
		else 
		{
			//If no session, redirect to login page
			/*redirect('login', 'refresh');*/
            redirect('administrator', 'refresh');
		}
	}
	
	public function update_change_password()			//for displaying the data of the staff into the table
   {
	
	       $category_id=$this->input->post('category_id');
	          $data = array(
				'category_password' => sha1($this->input->post('category_password'))
                 );
				//print_r($staffdata);
				$updateuserrole = $this->Category_model->category_update($data, $category_id);
			    echo $category_id;
				
				
				//redirect('manage-employee');
   }
   
 
	public function deletecategory()
    {
		if ($this->session->userdata('logged_in')) 
		{
			$category_id = $this->input->post('category_id');
		
			//$this->db->query("delete from wc_category where category_id='$category_id'");

			$this->db->query("update wc_category set deleted ='1',status='Inactive' where category_id ='$category_id'");
			$this->session->set_flashdata('added', '<div align="left" style="color:green; font-size:15px; border:1px #398AB9 solid;">
		    Service Deleted Successfully</div>');

			//redirect('users');
		} 
		else 
		{
			//If no session, redirect to login page
			/*redirect('login', 'refresh');*/
            redirect('administrator', 'refresh');
		}
	}
	
	
	
	
	public function view($category_id=''){
		
		if ($this->session->userdata('logged_in'))		
		{  
			$session_data = $this->session->userdata('logged_in');
			$slang="english";
		    $this->lang->load($slang, $slang);
		    $data['lang']=$this->lang->language; 
			$data['title'] = "User Role";
			if($category_id!=''){
				$category_details=$this->db->query("select * from wc_category where category_id='$category_id'")->result_array();
				if(!empty($category_details)){
					$data['category_id'] = $category_details[0]['category_id'];
					$data['client_id_sel'] = $category_details[0]['client_id'];
					$data['category_name'] = $category_details[0]['category_name'];
					$data['status'] = $category_details[0]['status'];
					
					
				}
			}else {
			        $data['category_id'] = '';
					$data['client_id_sel'] = '';
					$data['category_name'] = '';
					$data['status'] = '';
					
			}
			$this->load->view('admin/category/category_view', $data);
		} 
		else 
		{
			//If no session, redirect to login page
			/*redirect('login', 'refresh');*/
            redirect('administrator', 'refresh');
		}
	}
public function updatestatus()
{				//for email exist validation in edit customer form 
	
	
  $id=trim($this->input->get('id'));
  $status=trim($this->input->get('status'));
  //echo "update wc_category set status ='$status' where category_id ='$id'";
  $this->db->query("update wc_category set status ='$status' where category_id ='$id'");


		
}
	
	
}
?>