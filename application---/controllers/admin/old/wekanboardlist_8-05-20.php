<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Wekanboardlist extends CI_Controller
{
    public function __construct()
	{
		parent::__construct();		
		$this->load->library('pagination');
		$this->load->database();		
		$this->load->library('Upload');
		$this->load->helper('security');	
		$this->load->library('Form_validation');
		$this->load->helper(array('form', 'html', 'file'));
		$this->load->library('pagination');
		$this->load->library('email');
        $this->load->model('wekanboardlist_model');
        $this->load->library('userlib');
		
    	if ($this->session->userdata('logged_in')) {
            $session_data = $this->session->userdata('logged_in');
            $data['username'] = $session_data['username'];
            $data['firstname'] = $session_data['firstname'];
            $data['rolecode'] = $session_data['rolecode'];
			/* $timezone = $session_data['timezone'];
			date_default_timezone_set($timezone); */
         
        } else {
            //If no session, redirect to login page
            redirect('login', 'refresh');
        }
    }
	
    public function index()
	{
		if ($this->session->userdata('logged_in'))		
		{  
			$session_data = $this->session->userdata('logged_in');
			$data['rolecode'] = $session_data['rolecode'];
			$data['wekanboardlist'] = $session_data['rolecode'];
			$data['roles'] = $session_data['roles'];
			$data['title'] = "Users";
			$slang="english";
			$this->lang->load($slang, $slang);
			$data['lang']=$this->lang->language; 
			
			//echo "aaaaaaaaa";exit;
			$this->load->view('admin/wekanboardlist/wekanboardlist_list', $data);
		} 
		else 
		{
			//If no session, redirect to login page
			redirect('login', 'refresh');
		}
	}
	
	public function wekanboardlistlist(){
		if ($this->session->userdata('logged_in')) 
        {
			$session_data=$this->session->userdata('logged_in');
			$wekanboardlist = $session_data['rolecode'];
			$userid = $session_data['id'];
			$slang="english";
		    $this->lang->load($slang, $slang);
		    $data['lang']=$this->lang->language; 
    	    $aColumns = array( 'wekan_board_list_id','wekan_board_list','status');
         
			/* Indexed column (used for fast and accurate table cardinality) */
			$sIndexColumn = "wekan_board_list_id";
         
			/* DB table to use */
			$sTable = "wc_wekan_board_list";

            /*
			 * Paging
			 */
			$sLimit = "";
			if ( isset( $_GET['start'] ) && $_GET['length'] != '-1' )
			{
				$sLimit = "LIMIT ".intval( $_GET['start'] ).", ".
					intval( $_GET['length'] );
			}


    
     
			$sOrder = "";
			if ( isset( $_GET['order'] ) )
			{
				$sOrder = "ORDER BY  ";
			
						$sOrder .= $aColumns[0]."
							".($_GET['order'][0]['dir']==='desc' ? 'asc' : 'desc') .", ";
			   
				 
				$sOrder = substr_replace( $sOrder, "", -2 );
				if ( $sOrder == "ORDER BY" )
				{
					$sOrder = "";
				}
			}
	

        
        $commn="1=1 and deleted='0' ";
        
		
		//echo $_REQUEST['columns'][1]['search']['value']; exit();
		
		if(!empty($_GET['columns'][1]['search']['value']) ){  
	
		$commn.=" AND wc_wekan_board_list.wekan_board_list_id  LIKE '".$_GET['columns'][1]['search']['value']."%' ";
		}

        $sWhere="where $commn";
         
		
		if (isset($_GET['search']['value']) && $_GET['search']['value'] != "" )
		{
			 $sWhere = "WHERE $commn   AND (";
			for ( $i=0 ; $i<count($aColumns) ; $i++ )
			{
				
				/*if ( isset($_GET['columns'][$i]['searchable']) && $_GET['columns'][$i]['searchable'] == "true" )
				{*/
					$sWhere .= $aColumns[$i]." LIKE '%".( $_GET['search']['value'] )."%' OR ";
				/*}*/
			}
			$sWhere = substr_replace( $sWhere, "", -3 );
			$sWhere .= ')';
		}
		
		
       $join='';
        /*
         * SQL queries
         * Get data to display
         */
        $sQuery = "
            SELECT SQL_CALC_FOUND_ROWS ".str_replace(" , ", " ", implode(", ", $aColumns))."
            FROM   $sTable $join
            $sWhere
            $sOrder
            $sLimit";

       //echo  $sQuery; exit();

        $rResult = $this->db->query($sQuery);
        $rResult = $rResult->result_array();
        //$rResult = mysqli_query( $sQuery) or fatal_error( 'MySQL Error: ' . mysqli_errno() );

        
        /* Data set length after filtering */
        //$sQuery = "SELECT FOUND_ROWS()";
        //$rResultFilterTotal = mysqli_query( $sQuery) or fatal_error( 'MySQL Error: ' . mysqli_errno() );
        //$query = $this->db->query($sQuery);
        //$aResultFilterTotal = $query->result_array();
        //$aResultFilterTotal = mysqli_fetch_array($rResultFilterTotal);
       $iFilteredTotal = count( $rResult);
          
        /* Total data set length */
        $sQuery = "
            SELECT COUNT(".$sIndexColumn.") as count
            FROM   $sTable $join
            $sWhere
        ";
        //$rResultTotal = mysqli_query( $sQuery) or fatal_error( 'MySQL Error: ' . mysqli_errno() );
        //$aResultTotal = mysqli_fetch_array($rResultTotal);

        $query = $this->db->query($sQuery);
        $aResultTotal = $query->result_array();

        //echo "<pre>";
        //print_r($aResultTotal);
         $iTotal = $aResultTotal[0]['count'];
         
        /*
         * Output
         */
        $output = array(
                        "sEcho" => '',
                        "iTotalRecords" => $iFilteredTotal,
                        "iTotalDisplayRecords" => $iTotal,
                        "aaData" => array()
                        );

            $sColumns = array('wekan_board_list','status','action');
         
            //while ( $aRow = mysqli_fetch_array( $rResult ) )
            //echo "<pre>";
           //print_r($output);
         
            foreach($rResult as $aRow)
            {
                $row = array();
            	
				$wekan_board_list_id=$aRow['wekan_board_list_id'];
				$status=$aRow['status'];
                for ( $i=0 ; $i<count($sColumns) ; $i++ )
                {
                  
            	    if($sColumns[$i] =='action')
            		{
            			

						$row[]='<a href="javascript:void(0)"  class="btn btn-success view" val="'.$wekan_board_list_id.'"><i class="fas fa-eye"></i></a>&nbsp;<a href="javascript:void(0)"  class="btn btn-info edit" val="'.$wekan_board_list_id.'"><i class="fas fa-edit"></i></a>&nbsp; <a href="javascript:void(0)"  class="btn btn-danger delete" val="'.$wekan_board_list_id.'"><i class="fas fa-trash-alt"></i></a>
        			  ';
						
            		}
					
				
					else if($sColumns[$i] =='status')
            		{
            			
            			 $Active_status="";
			             $Inactive_status="";
						 
						 
					    if($status=='Active')
						{
						 $Active_status="selected";
			             $Inactive_status="";
						
						}
						else if($status=='Inactive')
						{
						 $Active_status="";
			             $Inactive_status="selected";
						 
						}
						
            			
            			 $row[]='<select name="status" id="status" class="selectpicker update_status">
								 <option value="Active@@@'.$wekan_board_list_id.'"  '.$Active_status.'>Active</option>
								 <option value="Inactive@@@'.$wekan_board_list_id.'"  '.$Inactive_status.'>Inactive</option>
								 </select>';
            			 
            		}
                    else if ( $sColumns[$i] != ' ' )
                    {
                        /* General output */
                        $row[] = $aRow[ $sColumns[$i] ];
                    }
                }
                $output['aaData'][] = $row;
            }
            //print_r($output);exit;
        echo json_encode( $output );
    }
	}
	
	
	public function add($wekan_board_list_id=''){
		
		if ($this->session->userdata('logged_in'))		
		{  
			$session_data = $this->session->userdata('logged_in');
			$slang="english";
		    $this->lang->load($slang, $slang);
		    $data['lang']=$this->lang->language; 
			$data['title'] = "User Role";
			if($wekan_board_list_id!=''){
				$wekanboardlist_details=$this->db->query("select * from wc_wekan_board_list where wekan_board_list_id='$wekan_board_list_id'")->result_array();
				if(!empty($wekanboardlist_details)){
					$data['wekan_board_list_id'] = $wekanboardlist_details[0]['wekan_board_list_id'];
					$data['wekan_board_list'] = $wekanboardlist_details[0]['wekan_board_list'];
					$data['status'] = $wekanboardlist_details[0]['status'];
					$data['deleted'] = $wekanboardlist_details[0]['deleted'];
					
					
				}
			}else {
			        $data['wekan_board_list_id'] = '';
					$data['wekan_board_list'] = '';
					$data['status'] = '';
					$data['deleted'] = '';
					
					
			}
			$this->load->view('admin/wekanboardlist/wekanboardlist_management', $data);
		} 
		else 
		{
			//If no session, redirect to login page
			redirect('login', 'refresh');
		}
	}
	
	

	
	public function insert_wekanboardlist(){
		
		if ($this->session->userdata('logged_in')) {
			$session_data = $this->session->userdata('logged_in');
			$created_date = date('Y-m-d');
			$userid=$session_data['id'];
			$wekan_board_list_id=$this->input->post('wekan_board_list_id');
			
			if($wekan_board_list_id!='')
			{
				
				  $data = array(
				    'wekan_board_list_id'=>$this->input->post('wekan_board_list_id'),
					'wekan_board_list'=>$this->input->post('wekan_board_list'),
					'status'=>$this->input->post('status'),
					);
					
				  $updateuserrole = $this->wekanboardlist_model->wekanboardlist_update($data,$wekan_board_list_id);
			      echo $wekan_board_list_id;
				
			}else {
				
				
				
				   $data = array(
				    'wekan_board_list_id'=>$this->input->post('wekan_board_list_id'),
					'wekan_board_list'=>$this->input->post('wekan_board_list'),
					'status'=>$this->input->post('status'),
					
					);
					$this->wekanboardlist_model->wekanboardlist_insert($data);
					$wekan_board_list_id=$this->db->insert_id();
					
					
					echo $wekan_board_list_id;
				}
			
		}else {
		
            redirect('login', 'refresh');
       
		}  
	}
		
		
		
		

 
	public function deletewekanboardlist()
    {
		if ($this->session->userdata('logged_in')) 
		{
			$wekan_board_list_id = $this->input->post('wekan_board_list_id');
		
			//$this->db->query("delete from user where user_id='$user_id'");
			$this->db->query("update wc_wekan_board_list set deleted ='1',status='Inactive' where wekan_board_list_id ='$wekan_board_list_id'");
			$this->session->set_flashdata('added', '<div align="left" style="color:green; font-size:15px; border:1px #398AB9 solid;">
		    Service Deleted Successfully</div>');

			//redirect('users');
		} 
		else 
		{
			//If no session, redirect to login page
			redirect('login', 'refresh');
		}
	}
	
	

	public function view($wekan_board_list_id=''){
		
		if ($this->session->userdata('logged_in'))		
		{  
			$session_data = $this->session->userdata('logged_in');
			$slang="english";
		    $this->lang->load($slang, $slang);
		    $data['lang']=$this->lang->language; 
			$data['title'] = "User Role";
			if($wekan_board_list_id!=''){
				$wekanboardlist_details=$this->db->query("select * from wc_wekan_board_list where wekan_board_list_id='$wekan_board_list_id'")->result_array();
				if(!empty($wekanboardlist_details)){
					$data['wekan_board_list_id'] = $wekanboardlist_details[0]['wekan_board_list_id'];
					$data['wekan_board_list'] = $wekanboardlist_details[0]['wekan_board_list'];
					$data['status'] = $wekanboardlist_details[0]['status'];
					$data['deleted'] = $wekanboardlist_details[0]['deleted'];
					
					
				}
			}else {
			        $data['wekan_board_list_id'] = '';
					$data['wekan_board_list'] = '';
					$data['status'] = '';
					$data['deleted'] = '';
					
					
			}
			$this->load->view('admin/wekanboardlist/wekanboardlist_view', $data);
		} 
		else 
		{
			//If no session, redirect to login page
			redirect('login', 'refresh');
		}
	}



	public function updatestatus()
	{				//for email exist validation in edit customer form 


	$id=trim($this->input->get('id'));
	$status=trim($this->input->get('status'));
	//echo "update wc_wekan_board_list set status ='$status' where wekan_board_list_id ='$id'";
	$this->db->query("update wc_wekan_board_list set status ='$status' where wekan_board_list_id ='$id'");



	}

	
}
?>