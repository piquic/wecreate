<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class User extends CI_Controller
{
    public function __construct()
	{
		parent::__construct();		
		$this->load->library('pagination');
		$this->load->database();		
		$this->load->library('Upload');
		$this->load->helper('security');	
		$this->load->library('Form_validation');
		$this->load->helper(array('form', 'html', 'file'));
		$this->load->library('pagination');
		$this->load->library('email');
        $this->load->model('User_model');
        $this->load->library('userlib');
        $this->load->library('email');
		
    	if ($this->session->userdata('logged_in')) {
            $session_data = $this->session->userdata('logged_in');
            $data['username'] = $session_data['username'];
            $data['firstname'] = $session_data['firstname'];
            $data['rolecode'] = $session_data['rolecode'];
			/* $timezone = $session_data['timezone'];
			date_default_timezone_set($timezone); */
         
        } else {
            //If no session, redirect to login page
            /*redirect('login', 'refresh');*/
            redirect('administrator', 'refresh');
        }
    }
	
    public function index()
	{
		if ($this->session->userdata('logged_in'))		
		{  
			$session_data = $this->session->userdata('logged_in');
			$data['rolecode'] = $session_data['rolecode'];
			$data['usertype'] = $session_data['rolecode'];
			$data['roles'] = $session_data['roles'];
			$data['title'] = "Users";
			$slang="english";
			$this->lang->load($slang, $slang);
			$data['lang']=$this->lang->language; 
			
			//echo "aaaaaaaaa";exit;
			$this->load->view('admin/user/user_list', $data);
		} 
		else 
		{
			//If no session, redirect to login page
			/*redirect('login', 'refresh');*/
            redirect('administrator', 'refresh');
		}
	}
	
	public function userlist(){
		if ($this->session->userdata('logged_in')) 
        {
			$session_data=$this->session->userdata('logged_in');
			$usertype = $session_data['rolecode'];
			$userid = $session_data['id'];
			$slang="english";
		    $this->lang->load($slang, $slang);
		    $data['lang']=$this->lang->language; 
    	    $aColumns = array( 'user_id','user_type_id','user_name','user_email','user_password','user_telephone','user_mobile','status');
         
			/* Indexed column (used for fast and accurate table cardinality) */
			$sIndexColumn = "user_id";
         
			/* DB table to use */
			$sTable = "wc_users";

            /*
			 * Paging
			 */
			$sLimit = "";
			if ( isset( $_GET['start'] ) && $_GET['length'] != '-1' )
			{
				$sLimit = "LIMIT ".intval( $_GET['start'] ).", ".
					intval( $_GET['length'] );
			}


    
     
			$sOrder = "";
			if ( isset( $_GET['order'] ) )
			{
				$sOrder = "ORDER BY  ";
			
						$sOrder .= $aColumns[0]."
							".($_GET['order'][0]['dir']==='desc' ? 'asc' : 'desc') .", ";
			   
				 
				$sOrder = substr_replace( $sOrder, "", -2 );
				if ( $sOrder == "ORDER BY" )
				{
					$sOrder = "";
				}
			}
	

        
        $commn="1=1 and user_type_id!='1' and deleted='0' ";
        
		
		//echo $_REQUEST['columns'][1]['search']['value']; exit();
		
		if(!empty($_GET['columns'][1]['search']['value']) ){  
	
		$commn.=" AND user.user_name  LIKE '".$_GET['columns'][1]['search']['value']."%' ";
		}

        $sWhere="where $commn";
         
		
		if (isset($_GET['search']['value']) && $_GET['search']['value'] != "" )
		{
			 $sWhere = "WHERE $commn   AND (";
			for ( $i=0 ; $i<count($aColumns) ; $i++ )
			{
				
				/*if ( isset($_GET['columns'][$i]['searchable']) && $_GET['columns'][$i]['searchable'] == "true" )
				{*/
					$sWhere .= $aColumns[$i]." LIKE '%".( $_GET['search']['value'] )."%' OR ";
				/*}*/
			}
			$sWhere = substr_replace( $sWhere, "", -3 );
			$sWhere .= ')';
		}
		
		
       $join='';
        /*
         * SQL queries
         * Get data to display
         */
        $sQuery = "
            SELECT SQL_CALC_FOUND_ROWS ".str_replace(" , ", " ", implode(", ", $aColumns))."
            FROM   $sTable $join
            $sWhere
            $sOrder
            $sLimit";

       //echo  $sQuery; exit();

        $rResult = $this->db->query($sQuery);
        $rResult = $rResult->result_array();
        //$rResult = mysqli_query( $sQuery) or fatal_error( 'MySQL Error: ' . mysqli_errno() );

        
        /* Data set length after filtering */
        //$sQuery = "SELECT FOUND_ROWS()";
        //$rResultFilterTotal = mysqli_query( $sQuery) or fatal_error( 'MySQL Error: ' . mysqli_errno() );
        //$query = $this->db->query($sQuery);
        //$aResultFilterTotal = $query->result_array();
        //$aResultFilterTotal = mysqli_fetch_array($rResultFilterTotal);
       $iFilteredTotal = count( $rResult);
          
        /* Total data set length */
        $sQuery = "
            SELECT COUNT(".$sIndexColumn.") as count
            FROM   $sTable $join
            $sWhere
        ";
        //$rResultTotal = mysqli_query( $sQuery) or fatal_error( 'MySQL Error: ' . mysqli_errno() );
        //$aResultTotal = mysqli_fetch_array($rResultTotal);

        $query = $this->db->query($sQuery);
        $aResultTotal = $query->result_array();

        //echo "<pre>";
        //print_r($aResultTotal);
         $iTotal = $aResultTotal[0]['count'];
         
        /*
         * Output
         */
        $output = array(
                        "sEcho" => '',
                        "iTotalRecords" => $iFilteredTotal,
                        "iTotalDisplayRecords" => $iTotal,
                        "aaData" => array()
                        );

            $sColumns = array('user_type_id','user_name','user_email','user_mobile','status','action');
         
            //while ( $aRow = mysqli_fetch_array( $rResult ) )
            //echo "<pre>";
           //print_r($output);
         
            foreach($rResult as $aRow)
            {
                $row = array();
            	
				$user_id=$aRow['user_id'];
				$user_type_id=$aRow['user_type_id'];
				//$user_type=$aRow['usertype_id'];
				//$business_id=$aRow['business_id'];
				$status=$aRow['status'];
                for ( $i=0 ; $i<count($sColumns) ; $i++ )
                {
                   /*  if ( $sColumns[$i] == "version" )
                    {
                       
                        $row[] = ($aRow[ $sColumns[$i] ]=="0") ? '-' : $aRow[ $sColumns[$i] ];
                    } */
					
            	    if($sColumns[$i] =='action')
            		{
            			

						$row[]='<a href="javascript:void(0)"  class="btn btn-success view" val="'.$user_id.'"><i class="fas fa-eye"></i></a>&nbsp;<a href="javascript:void(0)"  class="btn btn-info edit" val="'.$user_id.'"><i class="fas fa-edit"></i></a>&nbsp; <a href="javascript:void(0)"  class="btn btn-danger delete" val="'.$user_id.'"><i class="fas fa-trash-alt"></i></a>
        			  ';
						
            		}
					else if($sColumns[$i] =='user_type_id'){

						$users_type_details=$this->db->query("select * from wc_users_type where user_type_id='$user_type_id'  ")->result_array();
						$user_type_name=$users_type_details[0]['user_type_name'];


						$row[] =$user_type_name;
						/*if($user_type_id==1){
							$row[] ='Customer';
						}elseif($user_type==2){ 
							$row[] ='Representative';
						}
						elseif($user_type==3){ 
							$row[] ='Employee';
						}
						else {
							$row[] ='Supplier';
						}*/
					}
				
					
					
					
					
					
					
					
					
					
					else if($sColumns[$i] =='status')
            		{
            			
            			 $Active_status="";
			             $Inactive_status="";
						 
						 
					    if($status=='Active')
						{
						 $Active_status="selected";
			             $Inactive_status="";
						
						}
						else if($status=='Inactive')
						{
						 $Active_status="";
			             $Inactive_status="selected";
						 
						}
						
            			
            			 $row[]='<select name="status" id="status" class="selectpicker update_status">
								 <option value="Active@@@'.$user_id.'"  '.$Active_status.'>Active</option>
								 <option value="Inactive@@@'.$user_id.'"  '.$Inactive_status.'>Inactive</option>
								 </select>';
            			 
            		}
                    else if ( $sColumns[$i] != ' ' )
                    {
                        /* General output */
                        $row[] = $aRow[ $sColumns[$i] ];
                    }
                }
                $output['aaData'][] = $row;
            }
            //print_r($output);exit;
        echo json_encode( $output );
    }
	}
	
	
	public function add($user_id=''){
		
		if ($this->session->userdata('logged_in'))		
		{  
			$session_data = $this->session->userdata('logged_in');
			$slang="english";
		    $this->lang->load($slang, $slang);
		    $data['lang']=$this->lang->language; 
			$data['title'] = "User Role";
			if($user_id!=''){
				$user_details=$this->db->query("select * from wc_users where user_id='$user_id'")->result_array();
				if(!empty($user_details)){
					$data['user_id'] = $user_details[0]['user_id'];
					$data['user_type_id_sel'] = $user_details[0]['user_type_id'];
					$data['client_id_sel'] = $user_details[0]['client_id'];
					$data['brand_id_sel'] = $user_details[0]['brand_id'];
					$data['user_name'] = $user_details[0]['user_name'];
					$data['user_address'] = $user_details[0]['user_address'];
					$data['user_email'] = $user_details[0]['user_email'];
					$data['user_telephone'] = $user_details[0]['user_telephone'];
					$data['user_mobile'] = $user_details[0]['user_mobile'];
					$data['user_password'] = $user_details[0]['user_password'];
					
					$data['status'] = $user_details[0]['status'];
					
					
				}
			}else {
			        $data['user_id'] = '';
			         $data['user_type_id_sel'] = array();
			         $data['client_id_sel'] = '';
			        $data['brand_id_sel'] = '';
					$data['user_name'] = '';
					$data['user_address'] = '';
					$data['user_email'] = '';
					$data['user_telephone'] = '';
					$data['user_mobile'] = '';
					$data['user_password'] = '';
					
					$data['status'] = '';
					
			}
			$this->load->view('admin/user/user_management', $data);
		} 
		else 
		{
			//If no session, redirect to login page
			/*redirect('login', 'refresh');*/
            redirect('administrator', 'refresh');
		}
	}
	
	
		
	  public function checkexistemail()
{			// checking the email  exist  validation in add form
	
    $user_email = $this->input->post('user_email');
    $sql = "SELECT user_id,user_email FROM wc_users WHERE user_email = '$user_email'";
    $query = $this->db->query($sql);
    if( $query->num_rows() > 0 ){
        echo 'false';
    } else {
        echo 'true';
    }
	
}

   public function checkexistmobile()
{			// checking the email  exist  validation in add form
	
    $user_mobile = $this->input->post('user_mobile');
    $sql = "SELECT user_id,user_mobile FROM wc_users WHERE user_mobile = '$user_mobile'";
    $query = $this->db->query($sql);
    if( $query->num_rows() > 0 ){
        echo 'false';
    } else {
        echo 'true';
    }
	
}


public function insert_member_wekan_old($client_id,$user_type_id,$user_name,$user_email,$user_password){


    $query=$this->db->query("select * from wc_settings WHERE setting_key='WEKAN_LINK' ");
    $userDetails= $query->result_array();
    $WEKAN_LINK=$userDetails[0]['setting_value'];

    $query=$this->db->query("select * from wc_settings WHERE setting_key='WEKAN_USERNAME' ");
    $userDetails= $query->result_array();
    $WEKAN_USERNAME=$userDetails[0]['setting_value'];


    $query=$this->db->query("select * from wc_settings WHERE setting_key='WEKAN_PASSWORD' ");
    $userDetails= $query->result_array();
    $WEKAN_PASSWORD=$userDetails[0]['setting_value'];

    

    if ($this->session->userdata('wekan_integration')) {



        $wekan_session_data = $this->session->userdata('wekan_integration');
        $wekan_authorId = $wekan_session_data['wekan_authorId'];
        $wekan_token = $wekan_session_data['wekan_token'];
        $wekan_tokenExpires = $wekan_session_data['wekan_tokenExpires'];

         //echo "<pre>";
        //print_r($wekan_session_data);exit;
    }
    else
    {

        $curl = curl_init();
        curl_setopt_array($curl, array(
        CURLOPT_URL => $WEKAN_LINK."users/login",
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_ENCODING => "",
        CURLOPT_MAXREDIRS => 10,
        CURLOPT_TIMEOUT => 30,
        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
        CURLOPT_CUSTOMREQUEST => "POST",
        CURLOPT_POSTFIELDS => "username=".$WEKAN_USERNAME."&password=".$WEKAN_PASSWORD."",
        CURLOPT_HTTPHEADER => array(
        "cache-control: no-cache",
        "content-type: application/x-www-form-urlencoded"
        ),
        ));
        $response = curl_exec($curl);
        $err = curl_error($curl);

        curl_close($curl);

        if (!$err)
        {
        //var_dump($response);



        $res_arr=json_decode($response);

        //echo "<pre>";
        //print_r($res_arr);exit;
        $id=$res_arr->id;
        $token=$res_arr->token;
        $tokenExpires=$res_arr->tokenExpires;


        $sess_array = array(
        'wekan_authorId' => $id,
        'wekan_token' => $token,
        'wekan_tokenExpires' => $tokenExpires
        );
        $this->session->set_userdata('wekan_integration', $sess_array);


        $wekan_session_data = $this->session->userdata('wekan_integration');
        $wekan_authorId = $wekan_session_data['wekan_authorId'];
        $wekan_token = $wekan_session_data['wekan_token'];
        $wekan_tokenExpires = $wekan_session_data['wekan_tokenExpires'];

       

        }

    }


   // echo $client_id,$user_name,$user_email,$user_password;exit;

      //////////////////////////////////////////insert member////////////////////////////////

    $user_name_arr=explode("@",$user_email);
    $user_name=$user_name_arr[0];

    $ch = curl_init();

    curl_setopt($ch, CURLOPT_URL,$WEKAN_LINK."api/users");
    curl_setopt($ch, CURLOPT_HTTPHEADER, array(
    'Content-Type: application/x-www-form-urlencoded',
    'Authorization: Bearer ' . $wekan_token
    ));
    curl_setopt($ch, CURLOPT_POST, 1);
    curl_setopt($ch, CURLOPT_POSTFIELDS,
    "username=".$user_email."&email=".$user_email."&password=".$user_password);

    // In real life you should use something like:
    // curl_setopt($ch, CURLOPT_POSTFIELDS, 
    //          http_build_query(array('postvar1' => 'value1')));

    // Receive server response ...
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

    $server_output = curl_exec($ch);

    curl_close ($ch);
	$id_arr=json_decode($server_output);
	//echo "<pre>";
    //print_r($id_arr);exit;
	$wekan_member_id=$id_arr->_id;



  //////////////////////////////////////////assign member to board///////////////////////////////



    $wekan_member_assign_id="";
    $user_details=$this->db->query("select * from wc_clients where client_id='$client_id'")->result_array();
	$wekan_client_id = $user_details[0]['wekan_client_id'];

	if($wekan_client_id!='')
	{


			$isAdmin=true;
			$isNoComments=true;
			$isCommentOnly=true;
			$action="add";

			if($user_type_id=='1' && $user_type_id=='2' && $user_type_id=='3')
			{

			$isAdmin=true;
			$isNoComments=true;
			$isCommentOnly=true;

			}
			elseif($user_type_id=='4')
			{

			$isAdmin=false;
			$isNoComments=true;
			$isCommentOnly=true;

			}


			$ch = curl_init();

			//echo $WEKAN_LINK."api/boards/".$wekan_client_id."/members/".$wekan_member_id."/add";

			curl_setopt($ch, CURLOPT_URL,$WEKAN_LINK."api/boards/".$wekan_client_id."/members/".$wekan_member_id."/add");
			curl_setopt($ch, CURLOPT_HTTPHEADER, array(
			'Content-Type: application/x-www-form-urlencoded',
			'Authorization: Bearer ' . $wekan_token
			));
			curl_setopt($ch, CURLOPT_POST, 1);
			curl_setopt($ch, CURLOPT_POSTFIELDS,
			"action=".$action."&isAdmin=".$isAdmin."&isNoComments=".$isNoComments."&isCommentOnly=".$isCommentOnly);

			// In real life you should use something like:
			// curl_setopt($ch, CURLOPT_POSTFIELDS, 
			//          http_build_query(array('postvar1' => 'value1')));

			// Receive server response ...
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

			$server_output = curl_exec($ch);

			curl_close ($ch);
			$id_arr=json_decode($server_output);
			//echo "<pre>";
			//print_r($id_arr);exit;
			$wekan_member_assign_id=$id_arr->_id;
			$wekan_member_assign_title=$id_arr->title;







	}

    


    return $wekan_member_id;

    

}

public function update_member_wekan_old($wekan_member_id,$client_id,$user_type_id,$user_name,$user_email,$user_password){


    $query=$this->db->query("select * from wc_settings WHERE setting_key='WEKAN_LINK' ");
    $userDetails= $query->result_array();
    $WEKAN_LINK=$userDetails[0]['setting_value'];

    $query=$this->db->query("select * from wc_settings WHERE setting_key='WEKAN_USERNAME' ");
    $userDetails= $query->result_array();
    $WEKAN_USERNAME=$userDetails[0]['setting_value'];


    $query=$this->db->query("select * from wc_settings WHERE setting_key='WEKAN_PASSWORD' ");
    $userDetails= $query->result_array();
    $WEKAN_PASSWORD=$userDetails[0]['setting_value'];

    

    if ($this->session->userdata('wekan_integration')) {



        $wekan_session_data = $this->session->userdata('wekan_integration');
        $wekan_authorId = $wekan_session_data['wekan_authorId'];
        $wekan_token = $wekan_session_data['wekan_token'];
        $wekan_tokenExpires = $wekan_session_data['wekan_tokenExpires'];

         //echo "<pre>";
        //print_r($wekan_session_data);exit;
    }
    else
    {

        $curl = curl_init();
        curl_setopt_array($curl, array(
        CURLOPT_URL => $WEKAN_LINK."users/login",
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_ENCODING => "",
        CURLOPT_MAXREDIRS => 10,
        CURLOPT_TIMEOUT => 30,
        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
        CURLOPT_CUSTOMREQUEST => "POST",
        CURLOPT_POSTFIELDS => "username=".$WEKAN_USERNAME."&password=".$WEKAN_PASSWORD."",
        CURLOPT_HTTPHEADER => array(
        "cache-control: no-cache",
        "content-type: application/x-www-form-urlencoded"
        ),
        ));
        $response = curl_exec($curl);
        $err = curl_error($curl);

        curl_close($curl);

        if (!$err)
        {
        //var_dump($response);



        $res_arr=json_decode($response);

        //echo "<pre>";
        //print_r($res_arr);exit;
        $id=$res_arr->id;
        $token=$res_arr->token;
        $tokenExpires=$res_arr->tokenExpires;


        $sess_array = array(
        'wekan_authorId' => $id,
        'wekan_token' => $token,
        'wekan_tokenExpires' => $tokenExpires
        );
        $this->session->set_userdata('wekan_integration', $sess_array);


        $wekan_session_data = $this->session->userdata('wekan_integration');
        $wekan_authorId = $wekan_session_data['wekan_authorId'];
        $wekan_token = $wekan_session_data['wekan_token'];
        $wekan_tokenExpires = $wekan_session_data['wekan_tokenExpires'];

       

        }

    }


    /////////////////////////////////////////////////////////////////////////////////

    if($wekan_member_id!='')
    {

			

			$curl = curl_init();

			curl_setopt_array($curl, array(
			CURLOPT_URL => $WEKAN_LINK."api/users/".$wekan_member_id,
			CURLOPT_RETURNTRANSFER => true,
			CURLOPT_ENCODING => "",
			CURLOPT_MAXREDIRS => 10,
			CURLOPT_TIMEOUT => 0,
			CURLOPT_FOLLOWLOCATION => true,
			CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
			CURLOPT_CUSTOMREQUEST => "DELETE",
			CURLOPT_HTTPHEADER => array(
			"Accept: application/json",
			"Authorization: Bearer ".$wekan_token
			),
			));

			$response = curl_exec($curl);

			curl_close($curl);
			//echo $response;

    }



   // echo $client_id,$user_name,$user_email,$user_password;exit;

      //////////////////////////////////////////insert member////////////////////////////////

    $user_name_arr=explode("@",$user_email);
    $user_name=$user_name_arr[0];

    $ch = curl_init();

    curl_setopt($ch, CURLOPT_URL,$WEKAN_LINK."api/users");
    curl_setopt($ch, CURLOPT_HTTPHEADER, array(
    'Content-Type: application/x-www-form-urlencoded',
    'Authorization: Bearer ' . $wekan_token
    ));
    curl_setopt($ch, CURLOPT_POST, 1);
    curl_setopt($ch, CURLOPT_POSTFIELDS,
    "username=".$user_email."&email=".$user_email."&password=".$user_password);

    // In real life you should use something like:
    // curl_setopt($ch, CURLOPT_POSTFIELDS, 
    //          http_build_query(array('postvar1' => 'value1')));

    // Receive server response ...
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

    $server_output = curl_exec($ch);

    curl_close ($ch);
	$id_arr=json_decode($server_output);
	//echo "<pre>";
    //print_r($id_arr);exit;
	$wekan_member_id=$id_arr->_id;



  //////////////////////////////////////////assign member to board///////////////////////////////




    $user_details=$this->db->query("select * from wc_clients where client_id='$client_id'")->result_array();
	$wekan_client_id = $user_details[0]['wekan_client_id'];

	if($wekan_client_id!='')
	{


			$isAdmin=true;
			$isNoComments=true;
			$isCommentOnly=true;
			$action="add";

			if($user_type_id=='1' && $user_type_id=='2' && $user_type_id=='3')
			{

			$isAdmin=true;
			$isNoComments=true;
			$isCommentOnly=true;

			}
			elseif($user_type_id=='4')
			{

			$isAdmin=false;
			$isNoComments=true;
			$isCommentOnly=true;

			}


			$ch = curl_init();

			curl_setopt($ch, CURLOPT_URL,$WEKAN_LINK."api/boards/".$wekan_client_id."/members/".$wekan_member_id."/add");
			curl_setopt($ch, CURLOPT_HTTPHEADER, array(
			'Content-Type: application/x-www-form-urlencoded',
			'Authorization: Bearer ' . $wekan_token
			));
			curl_setopt($ch, CURLOPT_POST, 1);
			curl_setopt($ch, CURLOPT_POSTFIELDS,
			"action=".$action."&isAdmin=".$isAdmin."&isNoComments=".$isNoComments."&isCommentOnly=".$isCommentOnly);

			// In real life you should use something like:
			// curl_setopt($ch, CURLOPT_POSTFIELDS, 
			//          http_build_query(array('postvar1' => 'value1')));

			// Receive server response ...
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

			$server_output = curl_exec($ch);

			curl_close ($ch);
			$id_arr=json_decode($server_output);
			//echo "<pre>";
			//print_r($id_arr);exit;
			$wekan_member_assign_id=$id_arr->_id;
			$wekan_member_assign_title=$id_arr->title;







	}

    


    return $wekan_member_id;

    

}




public function insert_member_wekan($client_id,$user_type_id,$user_name,$user_email,$user_password){


    $query=$this->db->query("select * from wc_settings WHERE setting_key='WEKAN_LINK' ");
    $userDetails= $query->result_array();
    $WEKAN_LINK=$userDetails[0]['setting_value'];

    $query=$this->db->query("select * from wc_settings WHERE setting_key='WEKAN_USERNAME' ");
    $userDetails= $query->result_array();
    $WEKAN_USERNAME=$userDetails[0]['setting_value'];


    $query=$this->db->query("select * from wc_settings WHERE setting_key='WEKAN_PASSWORD' ");
    $userDetails= $query->result_array();
    $WEKAN_PASSWORD=$userDetails[0]['setting_value'];

    

    if ($this->session->userdata('wekan_integration')) {



        $wekan_session_data = $this->session->userdata('wekan_integration');
        $wekan_authorId = $wekan_session_data['wekan_authorId'];
        $wekan_token = $wekan_session_data['wekan_token'];
        $wekan_tokenExpires = $wekan_session_data['wekan_tokenExpires'];

         //echo "<pre>";
        //print_r($wekan_session_data);exit;
    }
    else
    {

        $curl = curl_init();
        curl_setopt_array($curl, array(
        CURLOPT_URL => $WEKAN_LINK."users/login",
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_ENCODING => "",
        CURLOPT_MAXREDIRS => 10,
        CURLOPT_TIMEOUT => 30,
        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
        CURLOPT_CUSTOMREQUEST => "POST",
        CURLOPT_POSTFIELDS => "username=".$WEKAN_USERNAME."&password=".$WEKAN_PASSWORD."",
        CURLOPT_HTTPHEADER => array(
        "cache-control: no-cache",
        "content-type: application/x-www-form-urlencoded"
        ),
        ));
        $response = curl_exec($curl);
        $err = curl_error($curl);

        curl_close($curl);

        if (!$err)
        {
        //var_dump($response);



        $res_arr=json_decode($response);

        //echo "<pre>";
        //print_r($res_arr);exit;
        $id=$res_arr->id;
        $token=$res_arr->token;
        $tokenExpires=$res_arr->tokenExpires;


        $sess_array = array(
        'wekan_authorId' => $id,
        'wekan_token' => $token,
        'wekan_tokenExpires' => $tokenExpires
        );
        $this->session->set_userdata('wekan_integration', $sess_array);


        $wekan_session_data = $this->session->userdata('wekan_integration');
        $wekan_authorId = $wekan_session_data['wekan_authorId'];
        $wekan_token = $wekan_session_data['wekan_token'];
        $wekan_tokenExpires = $wekan_session_data['wekan_tokenExpires'];

       

        }

    }


   // echo $client_id,$user_name,$user_email,$user_password;exit;

      //////////////////////////////////////////insert member////////////////////////////////

    $user_name_arr=explode("@",$user_email);
    $user_name=$user_name_arr[0];

    $ch = curl_init();

    curl_setopt($ch, CURLOPT_URL,$WEKAN_LINK."api/users");
    curl_setopt($ch, CURLOPT_HTTPHEADER, array(
    'Content-Type: application/x-www-form-urlencoded',
    'Authorization: Bearer ' . $wekan_token
    ));
    curl_setopt($ch, CURLOPT_POST, 1);
    curl_setopt($ch, CURLOPT_POSTFIELDS,
    "username=".$user_email."&email=".$user_email."&password=".$user_password);

    // In real life you should use something like:
    // curl_setopt($ch, CURLOPT_POSTFIELDS, 
    //          http_build_query(array('postvar1' => 'value1')));

    // Receive server response ...
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

    $server_output = curl_exec($ch);

    curl_close ($ch);
	$id_arr=json_decode($server_output);
	//echo "<pre>";
    //print_r($id_arr);exit;

    if(isset($id_arr->_id))
    {
    	$wekan_member_id=$id_arr->_id;

    }
    else
    {
    	$wekan_member_id="0";
    }
    
	



  //////////////////////////////////////////assign member to board///////////////////////////////



    $wekan_member_assign_id="";
    $user_details=$this->db->query("select * from wc_clients where client_id='$client_id'")->result_array();
	$wekan_client_id = $user_details[0]['wekan_client_id'];

	if($wekan_client_id!='')
	{


			$isAdmin=true;
			$isNoComments=true;
			$isCommentOnly=true;
			$action="add";

			/*if($user_type_id=='1' && $user_type_id=='2' && $user_type_id=='3')
			{

			$isAdmin=true;
			$isNoComments=true;
			$isCommentOnly=true;

			}
			elseif($user_type_id=='4')
			{

			$isAdmin=true;
			$isNoComments=true;
			$isCommentOnly=true;

			}*/


			$ch = curl_init();

			//echo $WEKAN_LINK."api/boards/".$wekan_client_id."/members/".$wekan_member_id."/add";

			curl_setopt($ch, CURLOPT_URL,$WEKAN_LINK."api/boards/".$wekan_client_id."/members/".$wekan_member_id."/add");
			curl_setopt($ch, CURLOPT_HTTPHEADER, array(
			'Content-Type: application/x-www-form-urlencoded',
			'Authorization: Bearer ' . $wekan_token
			));
			curl_setopt($ch, CURLOPT_POST, 1);
			/*curl_setopt($ch, CURLOPT_POSTFIELDS,
			"action=".$action."&isAdmin=".$isAdmin."&isNoComments=".$isNoComments."&isCommentOnly=".$isCommentOnly);*/

			curl_setopt($ch, CURLOPT_POSTFIELDS,
			"action=add&isAdmin=true&isNoComments=true&isCommentOnly=true");

			// In real life you should use something like:
			// curl_setopt($ch, CURLOPT_POSTFIELDS, 
			//          http_build_query(array('postvar1' => 'value1')));

			// Receive server response ...
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

			$server_output = curl_exec($ch);

			curl_close ($ch);
			$id_arr=json_decode($server_output);
			//echo "<pre>";
			//print_r($id_arr);exit;

			if(isset($id_arr->_id))
			{
			$wekan_member_assign_id=$id_arr->_id;
			$wekan_member_assign_title=$id_arr->title;
            }
			
			
             if($user_type_id!=4)
             {

             	$curl = curl_init();

				curl_setopt_array($curl, array(
				CURLOPT_URL => $WEKAN_LINK."/api/boards/".$wekan_client_id."/members/".$wekan_member_id."",
				CURLOPT_RETURNTRANSFER => true,
				CURLOPT_ENCODING => "",
				CURLOPT_MAXREDIRS => 10,
				CURLOPT_TIMEOUT => 0,
				CURLOPT_FOLLOWLOCATION => true,
				CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
				CURLOPT_CUSTOMREQUEST => "POST",
				 CURLOPT_POSTFIELDS => "isAdmin=true&isNoComments=true&isCommentOnly=true",
				CURLOPT_HTTPHEADER => array(
				"Accept: application/json",
				"Content-Type: application/x-www-form-urlencoded",
				"Authorization: Bearer " . $wekan_token,
				"Content-Type: application/x-www-form-urlencoded"
				),
				));

				$response = curl_exec($curl);

				curl_close($curl);
				//echo $response;



             }


				






	}

    


    return $wekan_member_id;

    

}

public function update_member_wekan($wekan_member_id,$client_id,$user_type_id,$user_name,$user_email,$user_password){


    $query=$this->db->query("select * from wc_settings WHERE setting_key='WEKAN_LINK' ");
    $userDetails= $query->result_array();
    $WEKAN_LINK=$userDetails[0]['setting_value'];

    $query=$this->db->query("select * from wc_settings WHERE setting_key='WEKAN_USERNAME' ");
    $userDetails= $query->result_array();
    $WEKAN_USERNAME=$userDetails[0]['setting_value'];


    $query=$this->db->query("select * from wc_settings WHERE setting_key='WEKAN_PASSWORD' ");
    $userDetails= $query->result_array();
    $WEKAN_PASSWORD=$userDetails[0]['setting_value'];

    

    if ($this->session->userdata('wekan_integration')) {



        $wekan_session_data = $this->session->userdata('wekan_integration');
        $wekan_authorId = $wekan_session_data['wekan_authorId'];
        $wekan_token = $wekan_session_data['wekan_token'];
        $wekan_tokenExpires = $wekan_session_data['wekan_tokenExpires'];

         //echo "<pre>";
        //print_r($wekan_session_data);exit;
    }
    else
    {

        $curl = curl_init();
        curl_setopt_array($curl, array(
        CURLOPT_URL => $WEKAN_LINK."users/login",
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_ENCODING => "",
        CURLOPT_MAXREDIRS => 10,
        CURLOPT_TIMEOUT => 30,
        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
        CURLOPT_CUSTOMREQUEST => "POST",
        CURLOPT_POSTFIELDS => "username=".$WEKAN_USERNAME."&password=".$WEKAN_PASSWORD."",
        CURLOPT_HTTPHEADER => array(
        "cache-control: no-cache",
        "content-type: application/x-www-form-urlencoded"
        ),
        ));
        $response = curl_exec($curl);
        $err = curl_error($curl);

        curl_close($curl);

        if (!$err)
        {
        //var_dump($response);



        $res_arr=json_decode($response);

        //echo "<pre>";
        //print_r($res_arr);exit;
        $id=$res_arr->id;
        $token=$res_arr->token;
        $tokenExpires=$res_arr->tokenExpires;


        $sess_array = array(
        'wekan_authorId' => $id,
        'wekan_token' => $token,
        'wekan_tokenExpires' => $tokenExpires
        );
        $this->session->set_userdata('wekan_integration', $sess_array);


        $wekan_session_data = $this->session->userdata('wekan_integration');
        $wekan_authorId = $wekan_session_data['wekan_authorId'];
        $wekan_token = $wekan_session_data['wekan_token'];
        $wekan_tokenExpires = $wekan_session_data['wekan_tokenExpires'];

       

        }

    }


    /////////////////////////////////////////////////////////////////////////////////

    if($wekan_member_id!='')
    {

			

			$curl = curl_init();

			curl_setopt_array($curl, array(
			CURLOPT_URL => $WEKAN_LINK."api/users/".$wekan_member_id,
			CURLOPT_RETURNTRANSFER => true,
			CURLOPT_ENCODING => "",
			CURLOPT_MAXREDIRS => 10,
			CURLOPT_TIMEOUT => 0,
			CURLOPT_FOLLOWLOCATION => true,
			CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
			CURLOPT_CUSTOMREQUEST => "DELETE",
			CURLOPT_HTTPHEADER => array(
			"Accept: application/json",
			"Authorization: Bearer ".$wekan_token
			),
			));

			$response = curl_exec($curl);

			curl_close($curl);
			//echo $response;

    }



   // echo $client_id,$user_name,$user_email,$user_password;exit;

      //////////////////////////////////////////insert member////////////////////////////////

    $user_name_arr=explode("@",$user_email);
    $user_name=$user_name_arr[0];

    $ch = curl_init();

    curl_setopt($ch, CURLOPT_URL,$WEKAN_LINK."api/users");
    curl_setopt($ch, CURLOPT_HTTPHEADER, array(
    'Content-Type: application/x-www-form-urlencoded',
    'Authorization: Bearer ' . $wekan_token
    ));
    curl_setopt($ch, CURLOPT_POST, 1);
    curl_setopt($ch, CURLOPT_POSTFIELDS,
    "username=".$user_email."&email=".$user_email."&password=".$user_password);

    // In real life you should use something like:
    // curl_setopt($ch, CURLOPT_POSTFIELDS, 
    //          http_build_query(array('postvar1' => 'value1')));

    // Receive server response ...
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

    $server_output = curl_exec($ch);

    curl_close ($ch);
	$id_arr=json_decode($server_output);
	//echo "<pre>";
    //print_r($id_arr);exit;
	

	if(isset($id_arr->_id))
    {
    	$wekan_member_id=$id_arr->_id;

    }
    else
    {
    	$wekan_member_id="0";
    }



  //////////////////////////////////////////assign member to board///////////////////////////////




    $user_details=$this->db->query("select * from wc_clients where client_id='$client_id'")->result_array();
	$wekan_client_id = $user_details[0]['wekan_client_id'];

	if($wekan_client_id!='')
	{


			$isAdmin=true;
			$isNoComments=true;
			$isCommentOnly=true;
			$action="add";

			/*if($user_type_id=='1' && $user_type_id=='2' && $user_type_id=='3')
			{

			$isAdmin=true;
			$isNoComments=true;
			$isCommentOnly=true;

			}
			elseif($user_type_id=='4')
			{

			$isAdmin=false;
			$isNoComments=true;
			$isCommentOnly=true;

			}*/


			$ch = curl_init();

			curl_setopt($ch, CURLOPT_URL,$WEKAN_LINK."api/boards/".$wekan_client_id."/members/".$wekan_member_id."/add");
			curl_setopt($ch, CURLOPT_HTTPHEADER, array(
			'Content-Type: application/x-www-form-urlencoded',
			'Authorization: Bearer ' . $wekan_token
			));
			curl_setopt($ch, CURLOPT_POST, 1);
			curl_setopt($ch, CURLOPT_POSTFIELDS,
			"action=".$action."&isAdmin=".$isAdmin."&isNoComments=".$isNoComments."&isCommentOnly=".$isCommentOnly);

			// In real life you should use something like:
			// curl_setopt($ch, CURLOPT_POSTFIELDS, 
			//          http_build_query(array('postvar1' => 'value1')));

			// Receive server response ...
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

			$server_output = curl_exec($ch);

			curl_close ($ch);
			$id_arr=json_decode($server_output);
			//echo "<pre>";
			//print_r($id_arr);exit;

			if(isset($id_arr->_id))
			{
			$wekan_member_assign_id=$id_arr->_id;
			$wekan_member_assign_title=$id_arr->title;

			}
			




			if($user_type_id!=4)
			{

			$curl = curl_init();

			curl_setopt_array($curl, array(
			CURLOPT_URL => $WEKAN_LINK."/api/boards/".$wekan_client_id."/members/".$wekan_member_id."",
			CURLOPT_RETURNTRANSFER => true,
			CURLOPT_ENCODING => "",
			CURLOPT_MAXREDIRS => 10,
			CURLOPT_TIMEOUT => 0,
			CURLOPT_FOLLOWLOCATION => true,
			CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
			CURLOPT_CUSTOMREQUEST => "POST",
			CURLOPT_POSTFIELDS => "isAdmin=true&isNoComments=true&isCommentOnly=true",
			CURLOPT_HTTPHEADER => array(
			"Accept: application/json",
			"Content-Type: application/x-www-form-urlencoded",
			"Authorization: Bearer " . $wekan_token,
			"Content-Type: application/x-www-form-urlencoded"
			),
			));

			$response = curl_exec($curl);

			curl_close($curl);
			//echo $response;



			}







	}

    


    return $wekan_member_id;

    

}




public function insert_user(){
		
		if ($this->session->userdata('logged_in')) {
			$session_data = $this->session->userdata('logged_in');
			$created_date = date('Y-m-d');
			$userid=$session_data['id'];
			$user_id=$this->input->post('user_id');
			
			if($user_id!='')
			{

				  $user_details=$this->db->query("select * from wc_users where user_id='$user_id'")->result_array();
				  $wekan_member_id = $user_details[0]['wekan_member_id'];
				  $user_email_exist = $user_details[0]['user_email'];

				  $user_type_id=$this->input->post('user_type_id');
				  $user_name=$this->input->post('user_name');
				  $user_email=$this->input->post('user_email');
				  $user_password=$this->input->post('user_password');
				  $client_id=$this->input->post('client_id');


				  if($user_email_exist!=$user_email)
				  {
				  	 $wekan_member_id=$this->update_member_wekan($wekan_member_id,$client_id,$user_type_id,$user_name,$user_email,$user_password);


				
				  $data = array(
				    /*'user_id'=>$this->input->post('user_id'),*/
				    'wekan_member_id'=>$wekan_member_id,
					'user_type_id'=>$this->input->post('user_type_id'),
					'client_id'=>$this->input->post('client_id'),
					'brand_id'=>$this->input->post('brand_id'),
					'user_name'=>$this->input->post('user_name'),
					//'user_address'=>$this->input->post('user_address'),
					'user_email'=>$this->input->post('user_email'),
					//'user_telephone'=>$this->input->post('user_telephone'),
					'user_mobile'=>$this->input->post('user_mobile'),
					/*'user_password'=>$this->input->post('user_password'),*/
					
					'status'=>$this->input->post('status'),
					);



				  /*********************************Send mail start*************************************/

                $query=$this->db->query("select * from  wc_settings  WHERE setting_key='MAIL_SMTP_USER' ");
                $userDetails= $query->result_array();
                $MAIL_SMTP_USER=$userDetails[0]['setting_value'];

                $query=$this->db->query("select * from  wc_settings  WHERE setting_key='MAIL_SMTP_PASSWORD' ");
                $userDetails= $query->result_array();
                $MAIL_SMTP_PASSWORD=$userDetails[0]['setting_value'];

                $query=$this->db->query("select * from  wc_settings  WHERE setting_key='MAIL_FROM' ");
                $userDetails= $query->result_array();
                $MAIL_FROM=$userDetails[0]['setting_value'];


                $query=$this->db->query("select * from  wc_settings  WHERE setting_key='MAIL_FROM_TEXT' ");
                $userDetails= $query->result_array();
                $MAIL_FROM_TEXT=$userDetails[0]['setting_value'];


                $query=$this->db->query("select * from wc_settings WHERE setting_key='WEKAN_URL_LINK' ");
				$settingDetails= $query->result_array();
				$WEKAN_URL_LINK=$settingDetails[0]['setting_value'];

                $query=$this->db->query("select * from  wc_settings  WHERE setting_key='MAIL_BODY_WEKAN_LOGIN' ");
                $userDetails= $query->result_array();
                $MAIL_BODY=$userDetails[0]['setting_value'];

                $query=$this->db->query("select * from  wc_settings  WHERE setting_key='MAIL_BODY_WEKAN_LOGIN' ");
                $userDetails= $query->result_array();
                $MAIL_BODY=$userDetails[0]['setting_value'];


                $client_id=$this->input->post('client_id');
                $user_name=$this->input->post('user_name');
                $user_email=$this->input->post('user_email');
                $user_password=$this->input->post('user_password');






                $checkquerys = $this->db->query("select * from wc_clients where  client_id='$client_id' ")->result_array();
                $client_name=ucfirst($checkquerys[0]['client_name']);
                $client_image=$checkquerys[0]['client_image'];
                $client_email=$checkquerys[0]['client_email'];

                $subject=$client_name.' - Wekan Login';

                //$unique_user_id=time()."-".$user_id;
                //$activation_link=base_url()."activate-registration/".base64_encode($unique_user_id);

                    
                    //Load email library
                    $this->load->library('email');

                    $config = array();
                    //$config['protocol'] = 'smtp';
                    $config['protocol']     = 'mail';
                    $config['smtp_host'] = 'localhost';
                    $config['smtp_user'] = $MAIL_SMTP_USER;
                    $config['smtp_pass'] = $MAIL_SMTP_PASSWORD;
                    $config['smtp_port'] = 25;




                    $this->email->initialize($config);
                    $this->email->set_newline("\r\n");


					if($client_id!='') {
					$checkquerys = $this->db->query("select * from wc_clients where client_id='$client_id' ")->result_array();

					$client_image=$checkquerys[0]['client_image'];
					$wekan_client_id=$checkquerys[0]['wekan_client_id'];

					$client_logo=base_url()."/upload_client_logo/".$client_image;
					}
					else
					{
					$client_logo=base_url()."/website-assets/images/logo.png";
					}
					$client_logo="http://style.piquic.com/WeCreate//website-assets/images/logo.png";

					$client_name=str_replace(" ","-",strtolower($checkquerys[0]['client_name']));


					//$wekan_link=$WEKAN_URL_LINK."b/".$wekan_client_id."/".$client_name;
					$wekan_link=$WEKAN_URL_LINK."sign-in";

					

                    
                   




                    $from_email = $MAIL_FROM;
                    $to_email = $user_email;

                    $date_time=date("d/m/y");
                    $copy_right= date("Y");
                    //echo $message=$MAIL_BODY;
                    $find_arr = array("##CLIENT_LOGO##","##USER_NAME##","##WEKAN_LINK##","##USER_EMAIL##","##USER_PASSWORD##","##CLIENT_NAME##","##COPY_RIGHT##");
                    $replace_arr = array($client_logo,$user_name,$wekan_link,$user_email,$user_password,$client_name,$copy_right);
                    $message=str_replace($find_arr,$replace_arr,$MAIL_BODY);

                    //echo $message;exit;



                    $this->email->from($from_email, $MAIL_FROM_TEXT);
                    $this->email->to($to_email);
                    $this->email->cc('poulami@mokshaproductions.in');
                    //$this->email->bcc('pranav@mokshaproductions.in');
                    $this->email->subject($subject);
                    $this->email->message($message);
                    //Send mail
                    if($this->email->send())
                    {

                        //echo "mail sent";exit;
                        $this->session->set_flashdata("email_sent","Congratulations Email Send Successfully.");
                    }
                    
                    else
                    {
                        //echo $this->email->print_debugger();
                        //echo "mail not sent";exit;
                        $this->session->set_flashdata("email_sent","You have encountered an error");
                    }


                    /*********************************Send mail end*************************************/









				  }
				  else
				  {
				  	 $data = array(
				    /*'user_id'=>$this->input->post('user_id'),*/
				    'user_type_id'=>$this->input->post('user_type_id'),
					'client_id'=>$this->input->post('client_id'),
					'brand_id'=>$this->input->post('brand_id'),
					'user_name'=>$this->input->post('user_name'),
					//'user_address'=>$this->input->post('user_address'),
					'user_email'=>$this->input->post('user_email'),
					//'user_telephone'=>$this->input->post('user_telephone'),
					'user_mobile'=>$this->input->post('user_mobile'),
					/*'user_password'=>$this->input->post('user_password'),*/
					
					'status'=>$this->input->post('status'),
					);

				  }

				  
					
				  $updateuserrole = $this->User_model->user_update($data,$user_id);
			      echo $user_id;
				
			}else {
				
				  $user_type_id=$this->input->post('user_type_id');
				  $user_name=$this->input->post('user_name');
				  $user_email=$this->input->post('user_email');
				  $user_password=$this->input->post('user_password');
				  $client_id=$this->input->post('client_id');


                  $wekan_member_id=$this->insert_member_wekan($client_id,$user_type_id,$user_name,$user_email,$user_password);
                  
                  if($wekan_member_id=='0')
                  {
                  	echo "wekanexist";

                  }
                  else
                  {




				
				   $data = array(
				    /*'user_id'=>$this->input->post('user_id'),*/
				    'wekan_member_id'=>$wekan_member_id,
					'user_type_id'=>$this->input->post('user_type_id'),
					'client_id'=>$this->input->post('client_id'),
					'brand_id'=>$this->input->post('brand_id'),
					'user_name'=>$this->input->post('user_name'),
					//'user_address'=>$this->input->post('user_address'),
					'user_email'=>$this->input->post('user_email'),
					//'user_telephone'=>$this->input->post('user_telephone'),
					'user_mobile'=>$this->input->post('user_mobile'),
					'user_password'=>sha1($this->input->post('user_password')),
					
					'status'=>$this->input->post('status'),
					);
					$this->User_model->user_insert($data);
					$user_id=$this->db->insert_id();
					
					
					echo $user_id;

				/*********************************Send mail start*************************************/

                $query=$this->db->query("select * from  wc_settings  WHERE setting_key='MAIL_SMTP_USER' ");
                $userDetails= $query->result_array();
                $MAIL_SMTP_USER=$userDetails[0]['setting_value'];

                $query=$this->db->query("select * from  wc_settings  WHERE setting_key='MAIL_SMTP_PASSWORD' ");
                $userDetails= $query->result_array();
                $MAIL_SMTP_PASSWORD=$userDetails[0]['setting_value'];

                $query=$this->db->query("select * from  wc_settings  WHERE setting_key='MAIL_FROM' ");
                $userDetails= $query->result_array();
                $MAIL_FROM=$userDetails[0]['setting_value'];


                $query=$this->db->query("select * from  wc_settings  WHERE setting_key='MAIL_FROM_TEXT' ");
                $userDetails= $query->result_array();
                $MAIL_FROM_TEXT=$userDetails[0]['setting_value'];


                $query=$this->db->query("select * from wc_settings WHERE setting_key='WEKAN_URL_LINK' ");
				$settingDetails= $query->result_array();
				$WEKAN_URL_LINK=$settingDetails[0]['setting_value'];

                $query=$this->db->query("select * from  wc_settings  WHERE setting_key='MAIL_BODY_WEKAN_LOGIN' ");
                $userDetails= $query->result_array();
                $MAIL_BODY=$userDetails[0]['setting_value'];

                $query=$this->db->query("select * from  wc_settings  WHERE setting_key='MAIL_BODY_WEKAN_LOGIN' ");
                $userDetails= $query->result_array();
                $MAIL_BODY=$userDetails[0]['setting_value'];


                $client_id=$this->input->post('client_id');
                $user_name=$this->input->post('user_name');
                $user_email=$this->input->post('user_email');
                $user_password=$this->input->post('user_password');






                $checkquerys = $this->db->query("select * from wc_clients where  client_id='$client_id' ")->result_array();
                $client_name=ucfirst($checkquerys[0]['client_name']);
                $client_image=$checkquerys[0]['client_image'];
                $client_email=$checkquerys[0]['client_email'];

                $subject=$client_name.' - Wekan Login';

                //$unique_user_id=time()."-".$user_id;
                //$activation_link=base_url()."activate-registration/".base64_encode($unique_user_id);

                    
                    //Load email library
                    $this->load->library('email');

                    $config = array();
                    //$config['protocol'] = 'smtp';
                    $config['protocol']     = 'mail';
                    $config['smtp_host'] = 'localhost';
                    $config['smtp_user'] = $MAIL_SMTP_USER;
                    $config['smtp_pass'] = $MAIL_SMTP_PASSWORD;
                    $config['smtp_port'] = 25;




                    $this->email->initialize($config);
                    $this->email->set_newline("\r\n");


					if($client_id!='') {
					$checkquerys = $this->db->query("select * from wc_clients where client_id='$client_id' ")->result_array();

					$client_image=$checkquerys[0]['client_image'];
					$wekan_client_id=$checkquerys[0]['wekan_client_id'];

					$client_logo=base_url()."/upload_client_logo/".$client_image;
					}
					else
					{
					$client_logo=base_url()."/website-assets/images/logo.png";
					}
					$client_logo="http://style.piquic.com/WeCreate//website-assets/images/logo.png";

					$client_name=str_replace(" ","-",strtolower($checkquerys[0]['client_name']));


					//$wekan_link=$WEKAN_URL_LINK."b/".$wekan_client_id."/".$client_name;
					$wekan_link=$WEKAN_URL_LINK."sign-in";

					

                    
                   




                    $from_email = $MAIL_FROM;
                    $to_email = $user_email;

                    $date_time=date("d/m/y");
                    $copy_right= date("Y");
                    //echo $message=$MAIL_BODY;
                    $find_arr = array("##CLIENT_LOGO##","##USER_NAME##","##WEKAN_LINK##","##USER_EMAIL##","##USER_PASSWORD##","##CLIENT_NAME##","##COPY_RIGHT##");
                    $replace_arr = array($client_logo,$user_name,$wekan_link,$user_email,$user_password,$client_name,$copy_right);
                    $message=str_replace($find_arr,$replace_arr,$MAIL_BODY);

                    //echo $message;exit;



                    $this->email->from($from_email, $MAIL_FROM_TEXT);
                    $this->email->to($to_email);
                    $this->email->cc('poulami@mokshaproductions.in');
                    //$this->email->bcc('pranav@mokshaproductions.in');
                    $this->email->subject($subject);
                    $this->email->message($message);
                    //Send mail
                    if($this->email->send())
                    {

                        //echo "mail sent";exit;
                        $this->session->set_flashdata("email_sent","Congratulations Email Send Successfully.");
                    }
                    
                    else
                    {
                        //echo $this->email->print_debugger();
                        //echo "mail not sent";exit;
                        $this->session->set_flashdata("email_sent","You have encountered an error");
                    }


                    /*********************************Send mail end*************************************/

                    }


				}
			
		}else {
		
            /*redirect('login', 'refresh');*/
            redirect('administrator', 'refresh');
       
		}  
	}



public function insert_user_old_latest(){
		
		if ($this->session->userdata('logged_in')) {
			$session_data = $this->session->userdata('logged_in');
			$created_date = date('Y-m-d');
			$userid=$session_data['id'];
			$user_id=$this->input->post('user_id');
			
			if($user_id!='')
			{

				  $user_details=$this->db->query("select * from wc_users where user_id='$user_id'")->result_array();
				  $wekan_member_id = $user_details[0]['wekan_member_id'];
				  $user_email_exist = $user_details[0]['user_email'];

				  $user_type_id=$this->input->post('user_type_id');
				  $user_name=$this->input->post('user_name');
				  $user_email=$this->input->post('user_email');
				  $user_password=$this->input->post('user_password');
				  $client_id=$this->input->post('client_id');


				  if($user_email_exist!=$user_email)
				  {
				  	 $wekan_member_id=$this->update_member_wekan($wekan_member_id,$client_id,$user_type_id,$user_name,$user_email,$user_password);


				
				  $data = array(
				    /*'user_id'=>$this->input->post('user_id'),*/
				     'wekan_member_id'=>$wekan_member_id,
					'user_type_id'=>$this->input->post('user_type_id'),
					'client_id'=>$this->input->post('client_id'),
					'brand_id'=>$this->input->post('brand_id'),
					'user_name'=>$this->input->post('user_name'),
					//'user_address'=>$this->input->post('user_address'),
					'user_email'=>$this->input->post('user_email'),
					//'user_telephone'=>$this->input->post('user_telephone'),
					'user_mobile'=>$this->input->post('user_mobile'),
					/*'user_password'=>$this->input->post('user_password'),*/
					
					'status'=>$this->input->post('status'),
					);

				  }
				  else
				  {
				  	 $data = array(
				    /*'user_id'=>$this->input->post('user_id'),*/
				    'user_type_id'=>$this->input->post('user_type_id'),
					'client_id'=>$this->input->post('client_id'),
					'brand_id'=>$this->input->post('brand_id'),
					'user_name'=>$this->input->post('user_name'),
					//'user_address'=>$this->input->post('user_address'),
					'user_email'=>$this->input->post('user_email'),
					//'user_telephone'=>$this->input->post('user_telephone'),
					'user_mobile'=>$this->input->post('user_mobile'),
					/*'user_password'=>$this->input->post('user_password'),*/
					
					'status'=>$this->input->post('status'),
					);

				  }

				  
					
				  $updateuserrole = $this->User_model->user_update($data,$user_id);
			      echo $user_id;
				
			}else {
				
				  $user_type_id=$this->input->post('user_type_id');
				  $user_name=$this->input->post('user_name');
				  $user_email=$this->input->post('user_email');
				  $user_password=$this->input->post('user_password');
				  $client_id=$this->input->post('client_id');


                  $wekan_member_id=$this->insert_member_wekan($client_id,$user_type_id,$user_name,$user_email,$user_password);

				
				   $data = array(
				    /*'user_id'=>$this->input->post('user_id'),*/
				    'wekan_member_id'=>$wekan_member_id,
					'user_type_id'=>$this->input->post('user_type_id'),
					'client_id'=>$this->input->post('client_id'),
					'brand_id'=>$this->input->post('brand_id'),
					'user_name'=>$this->input->post('user_name'),
					//'user_address'=>$this->input->post('user_address'),
					'user_email'=>$this->input->post('user_email'),
					//'user_telephone'=>$this->input->post('user_telephone'),
					'user_mobile'=>$this->input->post('user_mobile'),
					'user_password'=>sha1($this->input->post('user_password')),
					
					'status'=>$this->input->post('status'),
					);
					$this->User_model->user_insert($data);
					$user_id=$this->db->insert_id();
					
					
					echo $user_id;
				}
			
		}else {
		
            /*redirect('login', 'refresh');*/
            redirect('administrator', 'refresh');
       
		}  
	}

	
	public function insert_user_old(){
		
		if ($this->session->userdata('logged_in')) {
			$session_data = $this->session->userdata('logged_in');
			$created_date = date('Y-m-d');
			$userid=$session_data['id'];
			$user_id=$this->input->post('user_id');
			
			if($user_id!='')
			{
				
				  $data = array(
				    'user_id'=>$this->input->post('user_id'),
					'user_type_id'=>$this->input->post('user_type_id'),
					'client_id'=>$this->input->post('client_id'),
					'brand_id'=>$this->input->post('brand_id'),
					'user_name'=>$this->input->post('user_name'),
					//'user_address'=>$this->input->post('user_address'),
					'user_email'=>$this->input->post('user_email'),
					//'user_telephone'=>$this->input->post('user_telephone'),
					'user_mobile'=>$this->input->post('user_mobile'),
					/*'user_password'=>$this->input->post('user_password'),*/
					
					'status'=>$this->input->post('status'),
					);
					
				  $updateuserrole = $this->User_model->user_update($data,$user_id);
			      echo $user_id;
				
			}else {
				
				
				
				   $data = array(
				    'user_id'=>$this->input->post('user_id'),
					'user_type_id'=>$this->input->post('user_type_id'),
					'client_id'=>$this->input->post('client_id'),
					'brand_id'=>$this->input->post('brand_id'),
					'user_name'=>$this->input->post('user_name'),
					//'user_address'=>$this->input->post('user_address'),
					'user_email'=>$this->input->post('user_email'),
					//'user_telephone'=>$this->input->post('user_telephone'),
					'user_mobile'=>$this->input->post('user_mobile'),
					'user_password'=>sha1($this->input->post('user_password')),
					
					'status'=>$this->input->post('status'),
					);
					$this->User_model->user_insert($data);
					$user_id=$this->db->insert_id();
					
					
					echo $user_id;
				}
			
		}else {
		
            /*redirect('login', 'refresh');*/
            redirect('administrator', 'refresh');
       
		}  
	}


	public function showDivClient()			
   {

   	$client_id_sel=$this->input->post('client_id');
   	$brand_id_sel=$this->input->post('brand_id');
	
	       ?>

	       <div class="form-group">
					<label for="inputEmail3"  class="col-sm-2 control-label">Client</label>
					 <span style="color:#F00">*</span>
					<div class="col-sm-8">
				
							
								<select name="client_id" id="client_id" class="custom-select"  style="width: 100%" >

									<option value=""  >Select</option>
								
								<?php
								$client_details=$this->db->query("select * from wc_clients where deleted='0' and status='Active' order by  client_name asc")->result_array();
								if(!empty($client_details))
								{
								foreach($client_details as $key => $clientdetails)
								{
								$client_id=$clientdetails['client_id'];
								$client_name=$clientdetails['client_name'];
								?>
								<option value="<?php echo $client_id;?>" <?php if($client_id_sel==$client_id) { ?> selected="selected" <?php }  ?>  ><?php echo $client_name;?></option>
								<?php

								}
								}
								?>
								
								 </select>
								  <input type='hidden' name='brand_id' value='0' id='brand_id'>
					
					</div>
					</div>


	       <?php
				
				
				//redirect('manage-employee');
   }


   public function showDivClientBrand()			
   {
   	$client_id_sel=$this->input->post('client_id');
   	$brand_id_sel=$this->input->post('brand_id');
	
	       ?>


	       <div class="form-group">
					<label for="inputEmail3"  class="col-sm-2 control-label">Client</label>
					 <span style="color:#F00">*</span>
					<div class="col-sm-8">
				
							
								<select name="client_id" id="client_id" class="custom-select"  style="width: 100%" onchange="selectClient();" >

									<option value=""  >Select</option>
								
								<?php
								$client_details=$this->db->query("select * from wc_clients where deleted='0' and status='Active' order by  client_name asc")->result_array();
								if(!empty($client_details))
								{
								foreach($client_details as $key => $clientdetails)
								{
								$client_id=$clientdetails['client_id'];
								$client_name=$clientdetails['client_name'];
								?>
								<option value="<?php echo $client_id;?>" <?php if($client_id_sel==$client_id) { ?> selected="selected" <?php }  ?>  ><?php echo $client_name;?></option>
								<?php

								}
								}
								?>
								
								 </select>
								 
					
					</div>
					</div>

					<div id="brandDiv">

	       <div class="form-group">
					<label for="inputEmail3"  class="col-sm-2 control-label">Brand</label>
					 <span style="color:#F00">*</span>
					<div class="col-sm-8">
				            
							
								<select name="brand_id" id="brand_id" class="custom-select"  style="width: 100%" >

									<option value=""  >Select</option>
								
								<?php
								$brand_details=$this->db->query("select * from wc_brands where deleted='0' and status='Active' order by  brand_name asc")->result_array();
								if(!empty($brand_details))
								{
								foreach($brand_details as $key => $branddetails)
								{
								$brand_id=$branddetails['brand_id'];
								$brand_name=$branddetails['brand_name'];
								?>
								<option value="<?php echo $brand_id;?>" <?php if($brand_id_sel==$brand_id) { ?> selected="selected" <?php }  ?>   ><?php echo $brand_name;?></option>
								<?php

								}
								}
								?>
								
								 </select>
					
					</div>
					</div>
					</div>


	       <?php
				
				
				//redirect('manage-employee');
   }
   



 public function showDivBrand()			
   {
   	$client_id_sel=$this->input->post('client_id');
   	$brand_id_sel=$this->input->post('brand_id');
	
	       ?>


	       

			

	       <div class="form-group">
					<label for="inputEmail3"  class="col-sm-2 control-label">Brand</label>
					 <span style="color:#F00">*</span>
					<div class="col-sm-8">
				            
							
								<select name="brand_id" id="brand_id" class="custom-select"  style="width: 100%" >

									<option value=""  >Select</option>
								
								<?php
								$brand_details=$this->db->query("select * from wc_brands where deleted='0' and status='Active' and client_id='$client_id_sel' order by  brand_name asc")->result_array();
								if(!empty($brand_details))
								{
								foreach($brand_details as $key => $branddetails)
								{
								$brand_id=$branddetails['brand_id'];
								$brand_name=$branddetails['brand_name'];
								?>
								<option value="<?php echo $brand_id;?>" <?php if($brand_id_sel==$brand_id) { ?> selected="selected" <?php }  ?>   ><?php echo $brand_name;?></option>
								<?php

								}
								}
								?>
								
								 </select>
					
					</div>
					</div>
				


	       <?php
				
				
				//redirect('manage-employee');
   }
   






















		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
	public function change_password(){
		
		if ($this->session->userdata('logged_in'))		
		{  
			$session_data = $this->session->userdata('logged_in');
			$slang="english";
		    $this->lang->load($slang, $slang);
		    $data['lang']=$this->lang->language; 
			
			$user_id = $session_data['id'];
			$data['user_id'] = $user_id;
			$data['title'] = "User Role";
			if($user_id!=''){
				$user_details=$this->db->query("select * from wc_users where user_id='$user_id'")->result_array();
				if(!empty($user_details)){}
			}else {}
			$this->load->view('admin/user/user_password', $data);
		} 
		else 
		{
			//If no session, redirect to login page
			/*redirect('login', 'refresh');*/
            redirect('administrator', 'refresh');
		}
	}
	
	public function update_change_password()			//for displaying the data of the staff into the table
   {

   			$user_id=$this->input->post('user_id');

              
			$user_details=$this->db->query("select * from wc_users where user_id='$user_id'")->result_array();
			$wekan_member_id = $user_details[0]['wekan_member_id'];
			$user_type_id = $user_details[0]['user_type_id'];
			$user_name = $user_details[0]['user_name'];
			$user_email = $user_details[0]['user_email'];
			$client_id = $user_details[0]['client_id'];



			
			$user_password=$this->input->post('user_password');
			


			$wekan_member_id=$this->update_member_wekan($wekan_member_id,$client_id,$user_type_id,$user_name,$user_email,$user_password);

	
	       $user_id=$this->input->post('user_id');
	          $data = array(
	          	'wekan_member_id'=>$wekan_member_id,
				'user_password' => sha1($this->input->post('user_password'))
                 );
				//print_r($staffdata);
				$updateuserrole = $this->User_model->user_update($data, $user_id);
			    echo $user_id;
				
				
				//redirect('manage-employee');
   }
   
 
	public function deleteuser()
    {
		if ($this->session->userdata('logged_in')) 
		{
			$user_id = $this->input->post('user_id');
		
			$this->db->query("delete from wc_users where user_id='$user_id'");

			// $this->db->query("update wc_users set deleted ='1',status='Disable' where user_id ='$user_id'");
			$this->session->set_flashdata('added', '<div align="left" style="color:green; font-size:15px; border:1px #398AB9 solid;">
		    Service Deleted Successfully</div>');

			//redirect('users');
		} 
		else 
		{
			//If no session, redirect to login page
			/*redirect('login', 'refresh');*/
            redirect('administrator', 'refresh');
		}
	}
	
	
	
	
	public function view($user_id=''){
		
		if ($this->session->userdata('logged_in'))		
		{  
			$session_data = $this->session->userdata('logged_in');
			$slang="english";
		    $this->lang->load($slang, $slang);
		    $data['lang']=$this->lang->language; 
			$data['title'] = "User Role";
			if($user_id!=''){
				$user_details=$this->db->query("select * from wc_users where user_id='$user_id'")->result_array();
				if(!empty($user_details)){
					$data['user_id'] = $user_details[0]['user_id'];
					$data['user_type_id_sel'] = $user_details[0]['user_type_id'];
					$data['brand_id_sel'] = explode(",",trim($user_details[0]['brand_id'],","));
					$data['user_name'] = $user_details[0]['user_name'];
					$data['user_address'] = $user_details[0]['user_address'];
					$data['user_email'] = $user_details[0]['user_email'];
					$data['user_telephone'] = $user_details[0]['user_telephone'];
					$data['user_mobile'] = $user_details[0]['user_mobile'];
					$data['user_password'] = $user_details[0]['user_password'];
					
					$data['status'] = $user_details[0]['status'];
					
				}
			}else {
			        $data['user_id'] = '';
			        $data['user_type_id_sel'] = array();
			        $data['brand_id_sel'] = '';
					$data['user_name'] = '';
					$data['user_address'] = '';
					$data['user_email'] = '';
					$data['user_telephone'] = '';
					$data['user_mobile'] = '';
					$data['user_password'] = '';
					
					$data['status'] = '';
			}
			$this->load->view('admin/user/user_view', $data);
		} 
		else 
		{
			//If no session, redirect to login page
			/*redirect('login', 'refresh');*/
            redirect('administrator', 'refresh');
		}
	}
public function updatestatus()
{				//for email exist validation in edit customer form 
	
	
  $id=trim($this->input->get('id'));
  $status=trim($this->input->get('status'));
  //echo "update wc_users set status ='$status' where customer_id ='$id'";
  $this->db->query("update wc_users set status ='$status' where user_id ='$id'");


		
}
	
	
}
?>