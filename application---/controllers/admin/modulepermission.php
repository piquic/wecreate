<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Modulepermission extends CI_Controller
{
    public function __construct()
	{
		parent::__construct();		
		$this->load->library('pagination');
		$this->load->database();		
		$this->load->library('Upload');
		$this->load->helper('security');	
		$this->load->library('Form_validation');
		$this->load->helper(array('form', 'html', 'file'));
		$this->load->library('pagination');
		$this->load->library('email');
        $this->load->model('modulepermission_model');
        $this->load->library('userlib');
		
    	if ($this->session->userdata('logged_in')) {
            $session_data = $this->session->userdata('logged_in');
            $data['username'] = $session_data['username'];
            $data['firstname'] = $session_data['firstname'];
            $data['rolecode'] = $session_data['rolecode'];
			/* $timezone = $session_data['timezone'];
			date_default_timezone_set($timezone); */
         
        } else {
            //If no session, redirect to login page
            /*redirect('login', 'refresh');*/
            redirect('administrator', 'refresh');
        }
    }
	
    public function index()
	{
		if ($this->session->userdata('logged_in'))		
		{  
			$session_data = $this->session->userdata('logged_in');
			$data['rolecode'] = $session_data['rolecode'];
			$data['module'] = $session_data['rolecode'];
			$data['roles'] = $session_data['roles'];
			$data['title'] = "Users";
			$slang="english";
			$this->lang->load($slang, $slang);
			$data['lang']=$this->lang->language; 
			
			//echo "aaaaaaaaa";exit;
			$this->load->view('admin/modulepermission/module_permission', $data);
		} 
		else 
		{
			//If no session, redirect to login page
			/*redirect('login', 'refresh');*/
            redirect('administrator', 'refresh');
		}
	}

	


	public function update_module_permission(){
		
		if ($this->session->userdata('logged_in')) {
			$session_data = $this->session->userdata('logged_in');
			$created_date = date('Y-m-d');
			$userid=$session_data['id'];
			//$module_id=$this->input->post('module_id');

			$arr=$this->input->post();

			$upd_query="update wc_module_permission set access='0',upload='0',accept_reject='0',view='0',edit='0',approve_reject='0',add_revision='0',view_export='0',export='0' where 1=1 ";
			//echo $up_query."<br>";
			$this->db->query($upd_query);

			//echo "<pre>";
			//print_r($arr);

			foreach($arr as $key => $value_arr)
			{
                $field_name=$key;
                $arr_val=$value_arr;


                //echo $field_name."<br>";

				foreach($arr_val as $key_val => $value_arr_val)
				{
				   $field_id=$key_val;
				   $field_value=$value_arr_val;


				    //echo $field_name."<br>";
				  /* $up_query="update wc_module_permission set ".$field_name." ='".$field_value."' where module_permission_id ='$field_id'";*/
				   $up_query="update wc_module_permission set ".$field_name." ='1' where module_permission_id ='$field_id'";
				  // echo $up_query."<br>";
				   $this->db->query($up_query);
				}
			}
			//exit;
			
			redirect('administrator/manage-module-permission', 'refresh');
			
		}else {
		
            /*redirect('login', 'refresh');*/
            redirect('administrator', 'refresh');
       
		}  
	}
		
		




































	
	public function modulelist(){
		if ($this->session->userdata('logged_in')) 
        {
			$session_data=$this->session->userdata('logged_in');
			$module = $session_data['rolecode'];
			$userid = $session_data['id'];
			$slang="english";
		    $this->lang->load($slang, $slang);
		    $data['lang']=$this->lang->language; 
    	    $aColumns = array( 'module_id','module_name','status');
         
			/* Indexed column (used for fast and accurate table cardinality) */
			$sIndexColumn = "module_id";
         
			/* DB table to use */
			$sTable = "wc_module";

            /*
			 * Paging
			 */
			$sLimit = "";
			if ( isset( $_GET['start'] ) && $_GET['length'] != '-1' )
			{
				$sLimit = "LIMIT ".intval( $_GET['start'] ).", ".
					intval( $_GET['length'] );
			}


    
     
			$sOrder = "";
			if ( isset( $_GET['order'] ) )
			{
				$sOrder = "ORDER BY  ";
			
						$sOrder .= $aColumns[0]."
							".($_GET['order'][0]['dir']==='desc' ? 'asc' : 'desc') .", ";
			   
				 
				$sOrder = substr_replace( $sOrder, "", -2 );
				if ( $sOrder == "ORDER BY" )
				{
					$sOrder = "";
				}
			}
	

        
        $commn="1=1 and deleted='0' ";
        
		
		//echo $_REQUEST['columns'][1]['search']['value']; exit();
		
		if(!empty($_GET['columns'][1]['search']['value']) ){  
	
		$commn.=" AND wc_module.module_id  LIKE '".$_GET['columns'][1]['search']['value']."%' ";
		}

        $sWhere="where $commn";
         
		
		if (isset($_GET['search']['value']) && $_GET['search']['value'] != "" )
		{
			 $sWhere = "WHERE $commn   AND (";
			for ( $i=0 ; $i<count($aColumns) ; $i++ )
			{
				
				/*if ( isset($_GET['columns'][$i]['searchable']) && $_GET['columns'][$i]['searchable'] == "true" )
				{*/
					$sWhere .= $aColumns[$i]." LIKE '%".( $_GET['search']['value'] )."%' OR ";
				/*}*/
			}
			$sWhere = substr_replace( $sWhere, "", -3 );
			$sWhere .= ')';
		}
		
		
       $join='';
        /*
         * SQL queries
         * Get data to display
         */
        $sQuery = "
            SELECT SQL_CALC_FOUND_ROWS ".str_replace(" , ", " ", implode(", ", $aColumns))."
            FROM   $sTable $join
            $sWhere
            $sOrder
            $sLimit";

       //echo  $sQuery; exit();

        $rResult = $this->db->query($sQuery);
        $rResult = $rResult->result_array();
        //$rResult = mysqli_query( $sQuery) or fatal_error( 'MySQL Error: ' . mysqli_errno() );

        
        /* Data set length after filtering */
        //$sQuery = "SELECT FOUND_ROWS()";
        //$rResultFilterTotal = mysqli_query( $sQuery) or fatal_error( 'MySQL Error: ' . mysqli_errno() );
        //$query = $this->db->query($sQuery);
        //$aResultFilterTotal = $query->result_array();
        //$aResultFilterTotal = mysqli_fetch_array($rResultFilterTotal);
       $iFilteredTotal = count( $rResult);
          
        /* Total data set length */
        $sQuery = "
            SELECT COUNT(".$sIndexColumn.") as count
            FROM   $sTable $join
            $sWhere
        ";
        //$rResultTotal = mysqli_query( $sQuery) or fatal_error( 'MySQL Error: ' . mysqli_errno() );
        //$aResultTotal = mysqli_fetch_array($rResultTotal);

        $query = $this->db->query($sQuery);
        $aResultTotal = $query->result_array();

        //echo "<pre>";
        //print_r($aResultTotal);
         $iTotal = $aResultTotal[0]['count'];
         
        /*
         * Output
         */
        $output = array(
                        "sEcho" => '',
                        "iTotalRecords" => $iFilteredTotal,
                        "iTotalDisplayRecords" => $iTotal,
                        "aaData" => array()
                        );

            $sColumns = array('module_name','status','action');
         
            //while ( $aRow = mysqli_fetch_array( $rResult ) )
            //echo "<pre>";
           //print_r($output);
         
            foreach($rResult as $aRow)
            {
                $row = array();
            	
				$module_id=$aRow['module_id'];
				$status=$aRow['status'];
                for ( $i=0 ; $i<count($sColumns) ; $i++ )
                {
                  
            	    if($sColumns[$i] =='action')
            		{
            			

						$row[]='<a href="javascript:void(0)"  class="btn btn-success view" val="'.$module_id.'"><i class="fas fa-eye"></i></a>&nbsp;<a href="javascript:void(0)"  class="btn btn-info edit" val="'.$module_id.'"><i class="fas fa-edit"></i></a>&nbsp; <a href="javascript:void(0)"  class="btn btn-danger delete" val="'.$module_id.'"><i class="fas fa-trash-alt"></i></a>
        			  ';
						
            		}
					
				
					else if($sColumns[$i] =='status')
            		{
            			
            			 $Active_status="";
			             $Inactive_status="";
						 
						 
					    if($status=='Active')
						{
						 $Active_status="selected";
			             $Inactive_status="";
						
						}
						else if($status=='Inactive')
						{
						 $Active_status="";
			             $Inactive_status="selected";
						 
						}
						
            			
            			 $row[]='<select name="status" id="status" class="selectpicker update_status">
								 <option value="Active@@@'.$module_id.'"  '.$Active_status.'>Active</option>
								 <option value="Inactive@@@'.$module_id.'"  '.$Inactive_status.'>Inactive</option>
								 </select>';
            			 
            		}
                    else if ( $sColumns[$i] != ' ' )
                    {
                        /* General output */
                        $row[] = $aRow[ $sColumns[$i] ];
                    }
                }
                $output['aaData'][] = $row;
            }
            //print_r($output);exit;
        echo json_encode( $output );
    }
	}
	
	
	public function add($module_id=''){
		
		if ($this->session->userdata('logged_in'))		
		{  
			$session_data = $this->session->userdata('logged_in');
			$slang="english";
		    $this->lang->load($slang, $slang);
		    $data['lang']=$this->lang->language; 
			$data['title'] = "User Role";
			if($module_id!=''){
				$module_details=$this->db->query("select * from wc_module where module_id='$module_id'")->result_array();
				if(!empty($module_details)){
					$data['module_id'] = $module_details[0]['module_id'];
					$data['module_name'] = $module_details[0]['module_name'];
					$data['status'] = $module_details[0]['status'];
					$data['deleted'] = $module_details[0]['deleted'];
					
					
				}
			}else {
			        $data['module_id'] = '';
					$data['module_name'] = '';
					$data['status'] = '';
					$data['deleted'] = '';
					
					
			}
			$this->load->view('admin/module/module_management', $data);
		} 
		else 
		{
			//If no session, redirect to login page
			/*redirect('login', 'refresh');*/
            redirect('administrator', 'refresh');
		}
	}
	
	

	
	public function insert_module(){
		
		if ($this->session->userdata('logged_in')) {
			$session_data = $this->session->userdata('logged_in');
			$created_date = date('Y-m-d');
			$userid=$session_data['id'];
			$module_id=$this->input->post('module_id');
			
			if($module_id!='')
			{
				
				  $data = array(
				    'module_id'=>$this->input->post('module_id'),
					'module_name'=>$this->input->post('module_name'),
					'status'=>$this->input->post('status'),
					);
					
				  $updateuserrole = $this->modulepermission_model->module_update($data,$module_id);
			      echo $module_id;
				
			}else {
				
				
				
				   $data = array(
				    'module_id'=>$this->input->post('module_id'),
					'module_name'=>$this->input->post('module_name'),
					'status'=>$this->input->post('status'),
					
					);
					$this->modulepermission_model->module_insert($data);
					$module_id=$this->db->insert_id();
					
					
					echo $module_id;
				}
			
		}else {
		
            /*redirect('login', 'refresh');*/
            redirect('administrator', 'refresh');
       
		}  
	}
		
		
		
		

 
	public function deletemodule()
    {
		if ($this->session->userdata('logged_in')) 
		{
			$module_id = $this->input->post('module_id');
		
			//$this->db->query("delete from user where user_id='$user_id'");
			$this->db->query("update wc_module set deleted ='1',status='Inactive' where module_id ='$module_id'");
			$this->session->set_flashdata('added', '<div align="left" style="color:green; font-size:15px; border:1px #398AB9 solid;">
		    Service Deleted Successfully</div>');

			//redirect('users');
		} 
		else 
		{
			//If no session, redirect to login page
			/*redirect('login', 'refresh');*/
            redirect('administrator', 'refresh');
		}
	}
	
	

	public function view($module_id=''){
		
		if ($this->session->userdata('logged_in'))		
		{  
			$session_data = $this->session->userdata('logged_in');
			$slang="english";
		    $this->lang->load($slang, $slang);
		    $data['lang']=$this->lang->language; 
			$data['title'] = "User Role";
			if($module_id!=''){
				$module_details=$this->db->query("select * from wc_module where module_id='$module_id'")->result_array();
				if(!empty($module_details)){
					$data['module_id'] = $module_details[0]['module_id'];
					$data['module_name'] = $module_details[0]['module_name'];
					$data['status'] = $module_details[0]['status'];
					$data['deleted'] = $module_details[0]['deleted'];
					
					
				}
			}else {
			        $data['module_id'] = '';
					$data['module_name'] = '';
					$data['status'] = '';
					$data['deleted'] = '';
					
					
			}
			$this->load->view('admin/module/module_view', $data);
		} 
		else 
		{
			//If no session, redirect to login page
			/*redirect('login', 'refresh');*/
            redirect('administrator', 'refresh');
		}
	}



	public function updatestatus()
	{				//for email exist validation in edit customer form 


	$id=trim($this->input->get('id'));
	$status=trim($this->input->get('status'));
	//echo "update wc_module set status ='$status' where module_id ='$id'";
	$this->db->query("update wc_module set status ='$status' where module_id ='$id'");



	}

	
}
?>