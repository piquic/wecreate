<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
 
class Brand extends CI_Controller
{
    public function __construct()
	{
		parent::__construct();		
		$this->load->library('pagination');
		$this->load->database();		
		$this->load->library('Upload');
		$this->load->helper('security');	
		$this->load->library('Form_validation');
		$this->load->helper(array('form', 'html', 'file'));
		$this->load->library('pagination');
		$this->load->library('email');
        $this->load->model('Brand_model');
        $this->load->library('userlib');
		
    	if ($this->session->userdata('logged_in')) {
            $session_data = $this->session->userdata('logged_in');
            $data['username'] = $session_data['username'];
            $data['firstname'] = $session_data['firstname'];
            $data['rolecode'] = $session_data['rolecode'];
			/* $timezone = $session_data['timezone'];
			date_default_timezone_set($timezone); */
         
        } else {
            //If no session, redirect to login page
            /*redirect('login', 'refresh');*/
            redirect('administrator', 'refresh');
        }
    }
	
    public function index()
	{
		if ($this->session->userdata('logged_in'))		
		{  
			$session_data = $this->session->userdata('logged_in');
			$data['rolecode'] = $session_data['rolecode'];
			$data['usertype'] = $session_data['rolecode'];
			$data['roles'] = $session_data['roles'];
			$data['title'] = "Users";
			$slang="english";
			$this->lang->load($slang, $slang);
			$data['lang']=$this->lang->language; 
			
			//echo "aaaaaaaaa";exit;
			$this->load->view('admin/brand/brand_list', $data);
		} 
		else 
		{
			//If no session, redirect to login page
			/*redirect('login', 'refresh');*/
            redirect('administrator', 'refresh');
		}
	}
	
	public function brandlist(){
		if ($this->session->userdata('logged_in')) 
        {
			$session_data=$this->session->userdata('logged_in');
			$usertype = $session_data['rolecode'];
			$userid = $session_data['id'];
			$slang="english";
		    $this->lang->load($slang, $slang);
		    $data['lang']=$this->lang->language; 
    	    $aColumns = array( 'wc_brands.brand_id','wc_brands.brand_name','wc_clients.client_name','wc_brands.status');
         
			/* Indexed column (used for fast and accurate table cardinality) */
			$sIndexColumn = "brand_id";
         
			/* DB table to use */
			$sTable = "wc_brands";

            /*
			 * Paging
			 */
			$sLimit = "";
			if ( isset( $_GET['start'] ) && $_GET['length'] != '-1' )
			{
				$sLimit = "LIMIT ".intval( $_GET['start'] ).", ".
					intval( $_GET['length'] );
			}


    
     
			$sOrder = "";
			if ( isset( $_GET['order'] ) )
			{
				$sOrder = "ORDER BY  ";
			
						$sOrder .= $aColumns[0]."
							".($_GET['order'][0]['dir']==='desc' ? 'asc' : 'desc') .", ";
			   
				 
				$sOrder = substr_replace( $sOrder, "", -2 );
				if ( $sOrder == "ORDER BY" )
				{
					$sOrder = "";
				}
			}
	

        
        $commn="1=1 and wc_brands.deleted='0' ";
        
		
		//echo $_REQUEST['columns'][1]['search']['value']; exit();
		
		if(!empty($_GET['columns'][1]['search']['value']) ){  
	
		$commn.=" AND wc_brands.brand_name  LIKE '".$_GET['columns'][1]['search']['value']."%' ";
		}

        $sWhere="where $commn";
         
		
		if (isset($_GET['search']['value']) && $_GET['search']['value'] != "" )
		{
			 $sWhere = "WHERE $commn   AND (";
			for ( $i=0 ; $i<count($aColumns) ; $i++ )
			{
				
				/*if ( isset($_GET['columns'][$i]['searchable']) && $_GET['columns'][$i]['searchable'] == "true" )
				{*/
					$sWhere .= $aColumns[$i]." LIKE '%".( $_GET['search']['value'] )."%' OR ";
				/*}*/
			}
			$sWhere = substr_replace( $sWhere, "", -3 );
			$sWhere .= ')';
		}
		
		
       $join=' INNER JOIN wc_clients ON wc_brands.client_id=wc_clients.client_id';
        /*
         * SQL queries
         * Get data to display
         */
        $sQuery = "
            SELECT SQL_CALC_FOUND_ROWS ".str_replace(" , ", " ", implode(", ", $aColumns))."
            FROM   $sTable $join
            $sWhere
            $sOrder
            $sLimit";

       //echo  $sQuery; exit();

        $rResult = $this->db->query($sQuery);
        $rResult = $rResult->result_array();
        //$rResult = mysqli_query( $sQuery) or fatal_error( 'MySQL Error: ' . mysqli_errno() );

        
        /* Data set length after filtering */
        //$sQuery = "SELECT FOUND_ROWS()";
        //$rResultFilterTotal = mysqli_query( $sQuery) or fatal_error( 'MySQL Error: ' . mysqli_errno() );
        //$query = $this->db->query($sQuery);
        //$aResultFilterTotal = $query->result_array();
        //$aResultFilterTotal = mysqli_fetch_array($rResultFilterTotal);
       $iFilteredTotal = count( $rResult);
          
        /* Total data set length */
        $sQuery = "
            SELECT COUNT(".$sIndexColumn.") as count
            FROM   $sTable $join
            $sWhere
        ";
        //$rResultTotal = mysqli_query( $sQuery) or fatal_error( 'MySQL Error: ' . mysqli_errno() );
        //$aResultTotal = mysqli_fetch_array($rResultTotal);

        $query = $this->db->query($sQuery);
        $aResultTotal = $query->result_array();

        //echo "<pre>";
        //print_r($aResultTotal);
         $iTotal = $aResultTotal[0]['count'];
         
        /*
         * Output
         */
        $output = array(
                        "sEcho" => '',
                        "iTotalRecords" => $iFilteredTotal,
                        "iTotalDisplayRecords" => $iTotal,
                        "aaData" => array()
                        );

            $sColumns = array('client_name','brand_name','status','action');
         
            //while ( $aRow = mysqli_fetch_array( $rResult ) )
            //echo "<pre>";
           //print_r($output);
         
            foreach($rResult as $aRow)
            {
                $row = array();
            	
				$brand_id=$aRow['brand_id'];
				//$brand_type_id=$aRow['brand_type_id'];
				//$brand_type=$aRow['usertype_id'];
				//$business_id=$aRow['business_id'];
				$status=$aRow['status'];
                for ( $i=0 ; $i<count($sColumns) ; $i++ )
                {
                   /*  if ( $sColumns[$i] == "version" )
                    {
                       
                        $row[] = ($aRow[ $sColumns[$i] ]=="0") ? '-' : $aRow[ $sColumns[$i] ];
                    } */
					
            	    if($sColumns[$i] =='action')
            		{
            			

						$row[]='<a href="javascript:void(0)"  class="btn btn-success view" val="'.$brand_id.'"><i class="fas fa-eye"></i></a>&nbsp;<a href="javascript:void(0)"  class="btn btn-info edit" val="'.$brand_id.'"><i class="fas fa-edit"></i></a>&nbsp; <a href="javascript:void(0)"  class="btn btn-danger delete" val="'.$brand_id.'"><i class="fas fa-trash-alt"></i></a>
        			  ';
						
            		}
					
					
					
					
					
					
					
					
					
					else if($sColumns[$i] =='status')
            		{
            			
            			 $Active_status="";
			             $Inactive_status="";
						 
						 
					    if($status=='Active')
						{
						 $Active_status="selected";
			             $Inactive_status="";
						
						}
						else if($status=='Inactive')
						{
						 $Active_status="";
			             $Inactive_status="selected";
						 
						}
						
            			
            			 $row[]='<select name="status" id="status" class="selectpicker update_status">
								 <option value="Active@@@'.$brand_id.'"  '.$Active_status.'>Active</option>
								 <option value="Inactive@@@'.$brand_id.'"  '.$Inactive_status.'>Inactive</option>
								 </select>';
            			 
            		}
                    else if ( $sColumns[$i] != ' ' )
                    {
                        /* General output */
                        $row[] = $aRow[ $sColumns[$i] ];
                    }
                }
                $output['aaData'][] = $row;
            }
            //print_r($output);exit;
        echo json_encode( $output );
    }
	}
	
	
	public function add($brand_id=''){
		
		if ($this->session->userdata('logged_in'))		
		{  
			$session_data = $this->session->userdata('logged_in');
			$slang="english";
		    $this->lang->load($slang, $slang);
		    $data['lang']=$this->lang->language; 
			$data['title'] = "User Role";
			if($brand_id!=''){
				$brand_details=$this->db->query("select * from wc_brands where brand_id='$brand_id'")->result_array();
				if(!empty($brand_details)){
					$data['brand_id'] = $brand_details[0]['brand_id'];
					$data['client_id_sel'] = $brand_details[0]['client_id'];
					$data['category_id_sel'] = $brand_details[0]['category_id'];
					$data['category_id1_sel'] = $brand_details[0]['category_id1'];
					$data['category_id2_sel'] = $brand_details[0]['category_id2'];
					$data['brand_name'] = $brand_details[0]['brand_name'];
					$data['brand_email'] = $brand_details[0]['brand_email'];
					$data['status'] = $brand_details[0]['status'];
					
					
				}
			}else {
			        $data['brand_id'] = '';
					$data['client_id_sel'] = '';
					$data['category_id_sel'] = '';
					$data['category_id1_sel'] = '';
					$data['category_id2_sel'] = '';
					$data['brand_name'] = '';
					$data['brand_email'] = '';
					$data['status'] = '';
					
			}
			$this->load->view('admin/brand/brand_management', $data);
		} 
		else 
		{
			//If no session, redirect to login page
			/*redirect('login', 'refresh');*/
            redirect('administrator', 'refresh');
		}
	}
	
	
		
	  public function checkexistemail()
{			// checking the email  exist  validation in add form
	
    $brand_email = $this->input->post('brand_email');
    $sql = "SELECT brand_id,brand_email FROM wc_brands WHERE brand_email = '$brand_email'";
    $query = $this->db->query($sql);
    if( $query->num_rows() > 0 ){
        echo 'false';
    } else {
        echo 'true';
    }
	
}

   public function checkexistmobile()
{			// checking the email  exist  validation in add form
	
    $client_phone = $this->input->post('client_phone');
    $sql = "SELECT brand_id,client_phone FROM wc_brands WHERE client_phone = '$client_phone'";
    $query = $this->db->query($sql);
    if( $query->num_rows() > 0 ){
        echo 'false';
    } else {
        echo 'true';
    }
	
}
	
	public function insert_brand(){
		
		if ($this->session->userdata('logged_in')) {
			$session_data = $this->session->userdata('logged_in');
			$created_date = date('Y-m-d');
			$userid=$session_data['id'];
			$brand_id=$this->input->post('brand_id');
			$client_id=$this->input->post('client_id');
			$brand_name=$this->input->post('brand_name');

			$category_id=$this->input->post('category_id');
			$category1_id=$this->input->post('category1_id');
			$category2_id=$this->input->post('category2_id');
			
			if($brand_id!='')
			{
				
				  $data = array(
				    'brand_id'=>$this->input->post('brand_id'),
					'client_id'=>$this->input->post('client_id'),

					'category_id'=>$this->input->post('category_id'),
					'category_id1'=>$this->input->post('category_id1'),
					'category_id2'=>$this->input->post('category_id2'),

					'brand_name'=>$this->input->post('brand_name'),
					'brand_email'=>$this->input->post('brand_email'),
					'status'=>$this->input->post('status'),
					);
					$client_query= $this->db->query("SELECT * FROM wc_brands WHERE brand_id='$brand_id'");
                $client_rs= $client_query->result_array(); 

                $oldclient_id=$client_rs[0]['client_id'];
                $oldbrand_name=preg_replace('/[^a-zA-Z0-9_.]/', '_',$client_rs[0]['brand_name']);

                if($oldclient_id==$client_id){

                	$client_query1= $this->db->query("SELECT * FROM wc_clients WHERE client_id='$client_id'");
	                $client_rs1= $client_query1->result_array(); 

	                $client_name=preg_replace('/[^a-zA-Z0-9_.]/', '_',$client_rs1[0]['client_name']);
                	/*$old_brand_name=$_SERVER['DOCUMENT_ROOT']."/"."WeCreate/upload/".$client_id."-".$client_name."/".$brand_id."-".$oldbrand_name;
                	$new_brand_name=$_SERVER['DOCUMENT_ROOT']."/"."WeCreate/upload/".$client_id."-".$client_name."/".$brand_id."-".$brand_name;*/


                	$old_brand_name=$_SERVER['DOCUMENT_ROOT']."/".FOLDER_NAME."/upload/".$client_id."-".$client_name."/".$brand_id."-".$oldbrand_name;
                	$new_brand_name=$_SERVER['DOCUMENT_ROOT']."/".FOLDER_NAME."/upload/".$client_id."-".$client_name."/".$brand_id."-".$brand_name;




                	if(rename( $old_brand_name, $new_brand_name)){ 
	                   // echo "Successfully Renamed $old_client_name to $new_client_name" ;
	                }else{
	                    //echo "A File With The Same Name Already Exists" ;
	                }
                }
                if($oldclient_id!=$client_id){
                	$client_query1= $this->db->query("SELECT * FROM wc_clients WHERE client_id='$oldclient_id'");
	                $client_rs1= $client_query1->result_array();
	                $oldclient_name=preg_replace('/[^a-zA-Z0-9_.]/', '_',$client_rs1[0]['client_name']);

	                $new_client_query= $this->db->query("SELECT * FROM wc_clients WHERE client_id='$client_id'");
	                $new_client_rs= $new_client_query->result_array(); 
	                $newclient_name=preg_replace('/[^a-zA-Z0-9_.]/', '_',$new_client_rs[0]['client_name']);


	                // echo $_SERVER['DOCUMENT_ROOT']."WeCreate/upload/".$client_id."-".$client_rs[0]['client_name'];
	                /*$old_client_name=$_SERVER['DOCUMENT_ROOT']."/"."WeCreate/upload/".$oldclient_id."-".$oldclient_name;
	                $new_client_name=$_SERVER['DOCUMENT_ROOT']."/"."WeCreate/upload/".$client_id."-".$newclient_name;*/

	                $old_client_name=$_SERVER['DOCUMENT_ROOT']."/".FOLDER_NAME."/upload/".$oldclient_id."-".$oldclient_name;
	                $new_client_name=$_SERVER['DOCUMENT_ROOT']."/".FOLDER_NAME."/upload/".$client_id."-".$newclient_name;

	                if(rename( $old_client_name, $new_client_name)){ 
	                    /*$old_brand_name=$_SERVER['DOCUMENT_ROOT']."/"."WeCreate/upload/".$client_id."-".$newclient_name."/".$brand_id."-".$oldbrand_name;
	                	$new_brand_name=$_SERVER['DOCUMENT_ROOT']."/"."WeCreate/upload/".$client_id."-".$newclient_name."/".$brand_id."-".$brand_name;*/

	                	$old_brand_name=$_SERVER['DOCUMENT_ROOT']."/".FOLDER_NAME."/upload/".$client_id."-".$newclient_name."/".$brand_id."-".$oldbrand_name;
	                	$new_brand_name=$_SERVER['DOCUMENT_ROOT']."/".FOLDER_NAME."/upload/".$client_id."-".$newclient_name."/".$brand_id."-".$brand_name;

	                	if(rename( $old_brand_name, $new_brand_name)){ 
		                   // echo "Successfully Renamed $old_client_name to $new_client_name" ;
		                }else{
		                    //echo "A File With The Same Name Already Exists" ;
		                }
	                }else{
	                    //echo "A File With The Same Name Already Exists" ;
	                }
                }
               
				  $updateuserrole = $this->Brand_model->brand_update($data,$brand_id);
			      echo $brand_id;
				
			}else {
				
				
				
				    $data = array(
				    'brand_id'=>$this->input->post('brand_id'),
					'client_id'=>$this->input->post('client_id'),

					'category_id'=>$this->input->post('category_id'),
					'category_id1'=>$this->input->post('category_id1'),
					'category_id2'=>$this->input->post('category_id2'),



					'brand_name'=>$this->input->post('brand_name'),
					'brand_email'=>$this->input->post('brand_email'),
					'status'=>$this->input->post('status'),
					);
					$this->Brand_model->brand_insert($data);
					$brand_id=$this->db->insert_id();
					
					
					echo $brand_id;
				}
			
		}else {
		
            /*redirect('login', 'refresh');*/
            redirect('administrator', 'refresh');
       
		}  
	}
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
	public function change_password(){
		
		if ($this->session->userdata('logged_in'))		
		{  
			$session_data = $this->session->userdata('logged_in');
			$slang="english";
		    $this->lang->load($slang, $slang);
		    $data['lang']=$this->lang->language; 
			
			$brand_id = $session_data['id'];
			$data['brand_id'] = $brand_id;
			$data['title'] = "User Role";
			if($brand_id!=''){
				$brand_details=$this->db->query("select * from wc_brands where brand_id='$brand_id'")->result_array();
				if(!empty($brand_details)){}
			}else {}
			$this->load->view('admin/brand/brand_password', $data);
		} 
		else 
		{
			//If no session, redirect to login page
			/*redirect('login', 'refresh');*/
            redirect('administrator', 'refresh');
		}
	}
	
	public function update_change_password()			//for displaying the data of the staff into the table
   {
	
	       $brand_id=$this->input->post('brand_id');
	          $data = array(
				'brand_password' => sha1($this->input->post('brand_password'))
                 );
				//print_r($staffdata);
				$updateuserrole = $this->Brand_model->brand_update($data, $brand_id);
			    echo $brand_id;
				
				
				//redirect('manage-employee');
   }
   
 
	public function deletebrand()
    {
		if ($this->session->userdata('logged_in')) 
		{
			$brand_id = $this->input->post('brand_id');
		
			$this->db->query("delete from wc_brands where brand_id='$brand_id'");

			//$this->db->query("update wc_brands set deleted ='1',status='Inactive' where brand_id ='$brand_id'");
			$this->session->set_flashdata('added', '<div align="left" style="color:green; font-size:15px; border:1px #398AB9 solid;">
		    Service Deleted Successfully</div>');

			//redirect('users');
		} 
		else 
		{
			//If no session, redirect to login page
			/*redirect('login', 'refresh');*/
            redirect('administrator', 'refresh');
		}
	}
	
	
	
	
	public function view($brand_id=''){
		
		if ($this->session->userdata('logged_in'))		
		{  
			$session_data = $this->session->userdata('logged_in');
			$slang="english";
		    $this->lang->load($slang, $slang);
		    $data['lang']=$this->lang->language; 
			$data['title'] = "User Role";
			if($brand_id!=''){
				$brand_details=$this->db->query("select * from wc_brands where brand_id='$brand_id'")->result_array();
				if(!empty($brand_details)){
					$data['brand_id'] = $brand_details[0]['brand_id'];
					$data['client_id_sel'] = $brand_details[0]['client_id'];
					$data['category_id_sel'] = $brand_details[0]['category_id'];
					$data['category_id1_sel'] = $brand_details[0]['category_id1'];
					$data['category_id2_sel'] = $brand_details[0]['category_id2'];
					$data['brand_name'] = $brand_details[0]['brand_name'];
					$data['brand_email'] = $brand_details[0]['brand_email'];
					$data['status'] = $brand_details[0]['status'];
					
					
				}
			}else {
			        $data['brand_id'] = '';
					$data['client_id_sel'] = '';
					$data['category_id_sel'] = '';
					$data['category_id1_sel'] = '';
					$data['category_id2_sel'] = '';
					$data['brand_name'] = '';
					$data['brand_email'] = '';
					$data['status'] = '';
					
			}
			$this->load->view('admin/brand/brand_view', $data);
		} 
		else 
		{
			//If no session, redirect to login page
			/*redirect('login', 'refresh');*/
            redirect('administrator', 'refresh');
		}
	}
public function updatestatus()
{				//for email exist validation in edit customer form 
	
	
  $id=trim($this->input->get('id'));
  $status=trim($this->input->get('status'));
  //echo "update wc_brands set status ='$status' where brand_id ='$id'";
  $this->db->query("update wc_brands set status ='$status' where brand_id ='$id'");


		
}
















	public function showCategoryDiv()			
   {
		$client_id=$this->input->post('client_id');
		$category_id_sel=$this->input->post('category_id');
		$category_id1_sel=$this->input->post('category_id1');
		$category_id2_sel=$this->input->post('category_id2');
	
	       ?>

	       <?php

	       $clients_details=$this->db->query("select * from wc_category where client_id='$client_id' and deleted='0' and status='Active' ")->result_array();


	      if(count($clients_details)>0)
	       {

	       

	       ?>

	       <div class="form-group">
					    <label for="inputEmail3"  class="col-sm-2 control-label">Category
                        <!-- <span style="color:#F00">*</span> -->
                        </label>
						<div class="col-sm-8">
						    	<!-- class="form-control" -->
								<select name="category_id" id="category_id"  style="width: 100%" >
								 <option value=''>Select</option>
								<?php
								
								if(!empty($clients_details))
								{
								foreach($clients_details as $key => $clientsdetails)
								{
								$category_id=$clientsdetails['category_id'];
								$category_name=$clientsdetails['category_name'];
								?>
								<option value="<?php echo $category_id;?>" <?php if($category_id_sel==$category_id) { ?> selected="selected" <?php } ?>  ><?php echo $category_name;?></option>
								<?php

								}
								}
								?>
								
								 </select>
						</div>
						
					</div>
					
			<?php

			}
			else
			{
				?>
				<input  type='hidden'   id="category_id" name="category_id" value='<?php echo $category_id_sel;?>'>

				<?php
			}



			if(count($clients_details)>1)
	       {

	       ?>		



					<div class="form-group">
					    <label for="inputEmail3"  class="col-sm-2 control-label">Brand Level 1
                        <!-- <span style="color:#F00">*</span> -->
                        </label>
						<div class="col-sm-8">
						    	<!-- class="form-control" -->
								<select name="category_id1" id="category_id1"  style="width: 100%" >
								 <option value=''>Select</option>
								<?php
								if(!empty($clients_details))
								{
								foreach($clients_details as $key => $clientsdetails)
								{
								$category_id=$clientsdetails['category_id'];
								$category_name=$clientsdetails['category_name'];
								?>
								<option value="<?php echo $category_id;?>" <?php if($category_id1_sel==$category_id) { ?> selected="selected" <?php } ?>  ><?php echo $category_name;?></option>
								<?php

								}
								}
								?>
								
								 </select>
						</div>
						
					</div>

					<?php

			}
			else
			{
				?>
				<input  type='hidden'  id="category_id1" name="category_id1" value='<?php echo $category_id1_sel;?>'>

				<?php
			}
			




			if(count($clients_details)>2)
	       {

	       ?>		


					<div class="form-group">
					    <label for="inputEmail3"  class="col-sm-2 control-label">Brand level 2
                        <!-- <span style="color:#F00">*</span> -->
                        </label>
						<div class="col-sm-8">
						    	<!-- class="form-control" -->
								<select name="category_id2" id="category_id2"  style="width: 100%" >
								 <option value=''>Select</option>
								<?php
								if(!empty($clients_details))
								{
								foreach($clients_details as $key => $clientsdetails)
								{
								$category_id=$clientsdetails['category_id'];
								$category_name=$clientsdetails['category_name'];
								?>
								<option value="<?php echo $category_id;?>" <?php if($category_id2_sel==$category_id) { ?> selected="selected" <?php } ?>  ><?php echo $category_name;?></option>
								<?php

								}
								}
								?>
								
								 </select>
						</div>
						
						</div>


						<?php

			}
			else
			{
				?>
				<input  type='hidden'   id="category_id2" name="category_id2" value='<?php echo $category_id2_sel;?>'>

				<?php
			}
			 

	       ?>		


	       <?php
				
				
				//redirect('manage-employee');
   }













	
	
}
?>