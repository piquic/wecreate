<?php 
class uploadimages extends CI_Controller{
    
    function __construct(){
        parent::__construct();
        $this->load->library(['session']); 
        $this->load->helper(['url','file','form']); 
        $this->load->model('uploadbrief_model'); //load model upload 
        $this->load->model('Dashboard_model'); //load model upload 
        $this->load->library('upload'); //load library upload 
    }

    public function index(){

        if ($this->session->userdata('front_logged_in')) {
        $session_data = $this->session->userdata('front_logged_in');
        $data['user_id'] = $session_data['user_id'];
        $data['user_name'] = $session_data['user_name'];
		$type1 = $session_data['user_type_id'];
		
		$brand_access=$session_data['brand_access'];

        
        $data['dashboradinfo']=$this->Dashboard_model->briefListDetails();
		$data['brandsinfo'] = $this->Dashboard_model->getBrandtlist();
		$data['briefinreview']= $this->Dashboard_model->getbriefinreview($brand_access);
		$data['briefrejected']= $this->Dashboard_model->getbriefrejected($brand_access);
		$data['workinprogress']= $this->Dashboard_model->getworkinprogress($brand_access);
		$data['proffing_pending']= $this->Dashboard_model->getproffing_pending($brand_access);
		$data['revision_work']= $this->Dashboard_model->getrevision_work($brand_access);
		$data['work_complete']= $this->Dashboard_model->getwork_complete($brand_access);
		$data['feedback_pending']= $this->Dashboard_model->getfeedback_pending($brand_access);
        $data['title'] = "upload images";
        $this->load->view('front/uploadimages',$data);    
        }
        else
        {
        $data['user_id'] = '';
        $data['user_name'] = '';
        $user_id='';
        redirect('login', 'refresh');
        }       
    }

}

?>