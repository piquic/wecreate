<?php
class Myaccount extends CI_Controller {

	public function __construct() {
		parent::__construct();
		$this->load->library(['session']); 
		$this->load->model('myAccount_model');
		$this->load->helper('url_helper');
	}

	public function index(){


		if ($this->session->userdata('front_logged_in')) {
            $session_data = $this->session->userdata('front_logged_in');
            $data['user_id'] = $session_data['user_id'];
            $data['user_name'] = $session_data['user_name'];
            $user_id = $session_data['user_id'];

            $data['user_details'] = $this->myAccount_model->get_my_data($data['user_id']);

			$this->load->view('front/myaccount', $data);		
         
        } else {
        	redirect('/');
        }
		
	}
}

?>