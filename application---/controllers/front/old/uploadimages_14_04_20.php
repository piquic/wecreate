<?php 
class Uploadimages extends CI_Controller{
    
    function __construct(){
        parent::__construct();
        $this->load->library(['session']); 
        $this->load->helper(['url','file','form']); 
        $this->load->model('uploadbrief_model'); //load model upload 
        $this->load->model('Dashboard_model'); //load model upload
        $this->load->model('Uploadimages_model'); //load model upload		
        $this->load->library('upload'); //load library upload 
         $this->load->library('email');
    }

    public function index($brief_id){

        if ($this->session->userdata('front_logged_in')) {
        $session_data = $this->session->userdata('front_logged_in');
        $data['user_id'] = $session_data['user_id'];
        $data['user_name'] = $session_data['user_name'];
		$type1 = $session_data['user_type_id'];
		$brand_access=$session_data['brand_access'];
        $data['brief_id'] = $brief_id;
        $data['title'] = "upload images";
        $this->load->view('front/uploadimages',$data);    
        }
        else
        {
        $data['user_id'] = '';
        $data['user_name'] = '';
        $user_id='';
        redirect('login', 'refresh');
        }       
    }
	
	
	
	
	
	public function upload_files_breif_old(){
    if ($this->session->userdata('front_logged_in')) {
    $session_data = $this->session->userdata('front_logged_in');
    $data['user_id'] = $session_data['user_id'];
    $data['user_name'] = $session_data['user_name'];
    $user_id=$session_data['user_id'];
        //echo "<pre>";
		 $brief_id=$_POST["brief_id"];
		if(isset($_POST['image_id'])) 
		{
		$image_id=$_POST["image_id"];	
		}
		
		   //$checkquerys = $this->db->query("select * from wc_users where  user_id='$user_id'")->result_array();
		   $checkquerys = $this->db->query("SELECT * FROM `wc_brief` WHERE `brief_id` ='$brief_id'")->result_array();
		   $brand_id=$checkquerys[0]['brand_id'];
           $brief_title=$checkquerys[0]['brief_title'];
            $brief_name=str_replace(" ","_",$brief_title);
            $checkquerys = $this->db->query("select wc_brands.*,wc_clients.client_name from wc_brands inner join wc_clients on wc_brands.client_id=wc_clients.client_id where  wc_brands.brand_id='$brand_id'")->result_array();
            $brand_id=$checkquerys[0]['brand_id'];
            $brand_name=str_replace(" ","_",$checkquerys[0]['brand_name']);
            $client_id=$checkquerys[0]['client_id'];
            $client_name=str_replace(" ","_",$checkquerys[0]['client_name']);
			
			$dst = "upload/".$client_id."-".$client_name."/".$brand_id."-".$brand_name."/".$brief_id."-".$brief_name;  
	$client_folder=$client_id."-".$client_name;
	$brand_folder=$brand_id."-".$brand_name;
	$brief_folder=$brief_id."-".$brief_name;
	
	if (!is_dir("./upload/".$client_folder)) {
    mkdir("./upload/".$client_folder, 0777, true);
    }	
	if (!is_dir("./upload/".$client_folder."/".$brand_folder)) {
    mkdir("./upload/".$client_folder."/".$brand_folder, 0777, true);
    }	
	if (!is_dir("./upload/".$client_folder."/".$brand_folder."/".$brief_folder)) {
    mkdir("./upload/".$client_folder."/".$brand_folder."/".$brief_folder, 0777, true);
    }	
    $brief_path="./upload/".$client_folder."/".$brand_folder."/".$brief_folder;
    $timestamp=time();
    //if (!is_dir("./brief_upload/".$brief_id)) {
    //mkdir("./brief_upload/".$brief_id, 0777, true);
    //}
	
    $file=$_FILES["image"]["name"];
	$ext = pathinfo($file, PATHINFO_EXTENSION);
	$tm=time();
    $file = $tm.".".$ext;
	
	if(isset($_POST['image_id'])) 
		{
	$data['viewfilesinfo'] = $this->Uploadimages_model->insertimages1($file,$brief_id,$image_id);
		}
	else {
	//$data['viewfilesinfo'] = $this->Uploadimages_model->insertimages($file,$brief_id);
	$lastid=$this->Uploadimages_model->insertimages($file,$brief_id);
	
    if (!is_dir($brief_path."/".$lastid)) {
    mkdir($brief_path."/".$lastid, 0777, true);
    }
	if (!is_dir($brief_path."/".$lastid."/Revision1")) {
    mkdir($brief_path."/".$lastid."/Revision1", 0777, true);
    }			
	}
	
    $dir =$brief_path."/".$lastid."/Revision1/";
	
    if($_FILES["image"]["type"]=='image/jpeg'|| $_FILES["image"]["type"]=='image/jpg' || $_FILES["image"]["type"]=='video/webm' || $_FILES["image"]["type"]=='video/mp4' || $_FILES["image"]["type"]=='video/3gpp' || $_FILES["image"]["type"]=='video/x-sgi-movie' || $_FILES["image"]["type"]=='video/x-msvideo' || $_FILES["image"]["type"]=='video/mpeg' || $_FILES["image"]["type"]=='video/x-ms-wmv' )
    {
       move_uploaded_file($_FILES["image"]["tmp_name"], $dir. $file);
    }
    else
    {
		if($_FILES["image"]["type"]=='image/gif' || $_FILES["image"]["type"]=='image/png' || $_FILES["image"]["type"]=='image/bmp')
		{
        $img = $_FILES['image']['tmp_name'];
		$file_image=substr($file,4);
        //////////////////////////Convert JPG start//////////////////////////////////////
        $file=$this->convertToJpeg($dir,$img,$file_image);
       //////////////////////////Convert JPG end//////////////////////////////////////
		}
    }
	if($_FILES["image"]["type"]=='image/gif' || $_FILES["image"]["type"]=='image/png' || $_FILES["image"]["type"]=='image/bmp' || $_FILES["image"]["type"]=='image/jpg' || $_FILES["image"]["type"]=='image/jpeg'){
 //$file_image=$timestamp.pathinfo($_FILES["image"]["name"], PATHINFO_FILENAME);
 $file_image=pathinfo($_FILES["image"]["name"], PATHINFO_FILENAME);
 
 $new_thumb = $brief_path."/".$lastid."/Revision1/thumb_".$file;
 $file_pat = $brief_path."/".$lastid."/Revision1/".$file;
 $this->convertToThumb($new_thumb,$brief_id,$file_pat);
    //echo ltrim($file);
	}
	else
	{
	   
   $file_image=pathinfo($_FILES["image"]["name"], PATHINFO_FILENAME);
   $new_thumb = $brief_path."/".$lastid."/Revision1/"."thumb_".$file;
   $file_pat = "./assets/img/video_thumbnail.jpg";
   $this->convertToThumb($new_thumb,$brief_id,$file_pat);	   
	}
	
	
	
	
	 //$this->load->view('front/viewfiles_search',$data);
	//echo insert_sql;


     $update_sql = "UPDATE `wc_brief` SET `status`='6' WHERE brief_id='$brief_id' ";
     $this->db->query($update_sql);



    }
    else
    {
    $data['user_id'] = '';
    $data['user_name'] = '';
    $user_id='';
    echo "sessionout";
    }
	}
	
	public function upload_files_breif_ooo(){
    if ($this->session->userdata('front_logged_in')) {
      $session_data = $this->session->userdata('front_logged_in');
      $data['user_id'] = $session_data['user_id'];
      $data['user_name'] = $session_data['user_name'];
      $user_id=$session_data['user_id'];
      //echo "<pre>";
      $brief_id=$_POST["brief_id"];
      if(isset($_POST['image_id'])) 
        {
        $image_id=$_POST["image_id"]; 
        } 
      //$checkquerys = $this->db->query("select * from wc_users where  user_id='$user_id'")->result_array();
      $checkquerys = $this->db->query("SELECT * FROM `wc_brief` WHERE `brief_id` ='$brief_id'")->result_array();
      $brand_id=$checkquerys[0]['brand_id'];
	  $brief_title=$checkquerys[0]['brief_title'];
      $brief_name=str_replace(" ","_",$brief_title);
      $checkquerys = $this->db->query("select wc_brands.*,wc_clients.client_name from wc_brands inner join wc_clients on wc_brands.client_id=wc_clients.client_id where  wc_brands.brand_id='$brand_id'")->result_array();
      $brand_id=$checkquerys[0]['brand_id'];
      $brand_name=str_replace(" ","_",$checkquerys[0]['brand_name']);
      $client_id=$checkquerys[0]['client_id'];
      $client_name=str_replace(" ","_",$checkquerys[0]['client_name']);
      $dst = "upload/".$client_id."-".$client_name."/".$brand_id."-".$brand_name."/".$brief_id."-".$brief_name;  
      $client_folder=$client_id."-".$client_name;
      $brand_folder=$brand_id."-".$brand_name;
      $brief_folder=$brief_id."-".$brief_name;
      if (!is_dir("./upload/".$client_folder)) {
        mkdir("./upload/".$client_folder, 0777, true);
      } 
      if (!is_dir("./upload/".$client_folder."/".$brand_folder)) {
        mkdir("./upload/".$client_folder."/".$brand_folder, 0777, true);
      } 
      if (!is_dir("./upload/".$client_folder."/".$brand_folder."/".$brief_folder)) {
        mkdir("./upload/".$client_folder."/".$brand_folder."/".$brief_folder, 0777, true);
      } 
      $brief_path="./upload/".$client_folder."/".$brand_folder."/".$brief_folder;
      $timestamp=time();
      $file=$_FILES["image"]["name"];
      $ext = pathinfo($file, PATHINFO_EXTENSION);
      $tm=time();
      if($_FILES["image"]["type"]=='image/gif' || $_FILES["image"]["type"]=='image/png' || $_FILES["image"]["type"]=='image/bmp')
        {
          $file = $tm.".jpg";
        } 
        else {    
          $file = $tm.".".$ext;
        }     
      if(isset($_POST['image_id'])) 
        {
          $data['viewfilesinfo'] = $this->Uploadimages_model->insertimages1($file,$brief_id,$image_id);
        }
      else 
      {
        //$data['viewfilesinfo'] = $this->Uploadimages_model->insertimages($file,$brief_id);
        $lastid=$this->Uploadimages_model->insertimages($file,$brief_id);
        if (!is_dir($brief_path."/".$lastid)) {
          mkdir($brief_path."/".$lastid, 0777, true);
        }
        if (!is_dir($brief_path."/".$lastid."/Revision1")) {
          mkdir($brief_path."/".$lastid."/Revision1", 0777, true);
        }     
      }
      $dir =$brief_path."/".$lastid."/Revision1/";
      if($_FILES["image"]["type"]=='image/jpeg'|| $_FILES["image"]["type"]=='image/jpg' || $_FILES["image"]["type"]=='video/webm' || $_FILES["image"]["type"]=='video/mp4' || $_FILES["image"]["type"]=='video/3gpp' || $_FILES["image"]["type"]=='video/x-sgi-movie' || $_FILES["image"]["type"]=='video/x-msvideo' || $_FILES["image"]["type"]=='video/mpeg' || $_FILES["image"]["type"]=='video/x-ms-wmv' )
      {
         move_uploaded_file($_FILES["image"]["tmp_name"], $dir. $file);
         if($_FILES["image"]["type"]=='image/gif' || $_FILES["image"]["type"]=='image/png' || $_FILES["image"]["type"]=='image/bmp' || $_FILES["image"]["type"]=='image/jpg' || $_FILES["image"]["type"]=='image/jpeg'){
         //$file_image=$timestamp.pathinfo($_FILES["image"]["name"], PATHINFO_FILENAME);
         $file_image=pathinfo($_FILES["image"]["name"], PATHINFO_FILENAME);
         $new_thumb = $brief_path."/".$lastid."/Revision1/thumb_".$file;
         $file_pat = $brief_path."/".$lastid."/Revision1/".$file;
         $this->convertToThumb($new_thumb,$brief_id,$file_pat);
          //echo ltrim($file);
      }
      else
      {
         $file_image=pathinfo($_FILES["image"]["name"], PATHINFO_FILENAME);
         $new_thumb = $brief_path."/".$lastid."/Revision1/"."thumb_".$file;
         $file_pat = "./assets/img/video_thumbnail.jpg";
         $this->convertToThumb($new_thumb,$brief_id,$file_pat);    
      }    
      }
      else
      {
        if($_FILES["image"]["type"]=='image/gif' || $_FILES["image"]["type"]=='image/png' || $_FILES["image"]["type"]=='image/bmp')
        {
        $img = $_FILES['image']['tmp_name'];
        $file_image=substr($file,0,-4);
        //////////////////////////Convert JPG start//////////////////////////////////////
        $file_to_convert=$this->convertToJpeg($dir,$img,$file_image);
         //////////////////////////Convert JPG end//////////////////////////////////////
         $new_thumb = $brief_path."/".$lastid."/Revision1/thumb_".$file_image.".jpg";
         $file_pat = $brief_path."/".$lastid."/Revision1/".$file_image.".jpg";
         $this->convertToThumb($new_thumb,$brief_id,$file_pat);
        }
      }
       //$this->load->view('front/viewfiles_search',$data);
      //echo insert_sql;
       $update_sql = "UPDATE `wc_brief` SET `status`='6' WHERE brief_id='$brief_id' ";
       $this->db->query($update_sql);



       /*********************************Send mail*************************************/
   
    $query=$this->db->query("select * from  wc_settings  WHERE setting_key='MAIL_SMTP_USER' ");
    $userDetails= $query->result_array();
    $MAIL_SMTP_USER=$userDetails[0]['setting_value'];

    $query=$this->db->query("select * from  wc_settings  WHERE setting_key='MAIL_SMTP_PASSWORD' ");
    $userDetails= $query->result_array();
    $MAIL_SMTP_PASSWORD=$userDetails[0]['setting_value'];

    $query=$this->db->query("select * from  wc_settings  WHERE setting_key='MAIL_BODY_NOTIFY_FILEUPLOAD' ");
    $userDetails= $query->result_array();
    $MAIL_BODY=$userDetails[0]['setting_value'];


    $checkquerys = $this->db->query("select * from wc_brief where  brief_id='$brief_id' ")->result_array();
    $brief_id=$checkquerys[0]['brief_id'];
    //$rejected_reasons=$checkquerys[0]['rejected_reasons'];
    $added_by_user_id=$checkquerys[0]['added_by_user_id'];





    $checkquerys = $this->db->query("select * from wc_users where  user_id='$added_by_user_id' ")->result_array();
    $brand_manager_email=$checkquerys[0]['user_email'];
    $brand_manager_name=$checkquerys[0]['user_name'];

    $subject='Notification mail for Card - '.$brief_id;

    //$unique_user_id=time()."-".$user_id;
    //$activation_link=base_url()."activate-registration/".base64_encode($unique_user_id);

        
        //Load email library
        $this->load->library('email');

        $config = array();
        //$config['protocol'] = 'smtp';
        $config['protocol']     = 'mail';
        $config['smtp_host'] = 'localhost';
        $config['smtp_user'] = $MAIL_SMTP_USER;
        $config['smtp_pass'] = $MAIL_SMTP_PASSWORD;
        $config['smtp_port'] = 25;




        $this->email->initialize($config);
        $this->email->set_newline("\r\n");

        
        $from_email = "demo.intexom@gmail.com";
        $to_email = $brand_manager_email;

        $date_time=date("d/m/y");
        $copy_right= date("Y");
        //echo $message=$MAIL_BODY;
        $find_arr = array("##BM_NAME##","##BRIEF_ID##","##IMAGE_ID##","##COPY_RIGHT##");
        $replace_arr = array($brand_manager_name,$brief_id,$lastid,$copy_right);
        $message=str_replace($find_arr,$replace_arr,$MAIL_BODY);

        //echo $message;exit;



        $this->email->from($from_email, 'Piquic');
        $this->email->to($to_email);
        $this->email->cc('poulami@mokshaproductions.in');
        $this->email->bcc('pranav@mokshaproductions.in');
        $this->email->subject($subject);
        $this->email->message($message);
        //Send mail
        if($this->email->send())
        {

            //echo "mail sent";exit;
            $this->session->set_flashdata("email_sent","Congragulation Email Send Successfully.");
        }
        
        else
        {
            //echo $this->email->print_debugger();
            //echo "mail not sent";exit;
            $this->session->set_flashdata("email_sent","You have encountered an error");
        }
        

    //exit;

/************************************************************************/











    }
    else
    {
    $data['user_id'] = '';
    $data['user_name'] = '';
    $user_id='';
    echo "sessionout";
    }
  }

  public function upload_files_breif(){
    if ($this->session->userdata('front_logged_in')) {
      $session_data = $this->session->userdata('front_logged_in');
      $data['user_id'] = $session_data['user_id'];
      $data['user_name'] = $session_data['user_name'];
      $user_id=$session_data['user_id'];
      //echo "<pre>";
      $brief_id=$_POST["brief_id"];
      if(isset($_POST['image_id'])) 
        {
        $image_id=$_POST["image_id"]; 
        } 
      //$checkquerys = $this->db->query("select * from wc_users where  user_id='$user_id'")->result_array();
      $checkquerys = $this->db->query("SELECT * FROM `wc_brief` WHERE `brief_id` ='$brief_id'")->result_array();
      $brand_id=$checkquerys[0]['brand_id'];
	  $brief_title=$checkquerys[0]['brief_title'];
            $brief_name=str_replace(" ","_",$brief_title);
      $checkquerys = $this->db->query("select wc_brands.*,wc_clients.client_name from wc_brands inner join wc_clients on wc_brands.client_id=wc_clients.client_id where  wc_brands.brand_id='$brand_id'")->result_array();
      $brand_id=$checkquerys[0]['brand_id'];
      $brand_name=str_replace(" ","_",$checkquerys[0]['brand_name']);
      $client_id=$checkquerys[0]['client_id'];
      $client_name=str_replace(" ","_",$checkquerys[0]['client_name']);
      $dst = "upload/".$client_id."-".$client_name."/".$brand_id."-".$brand_name."/".$brief_id."-".$brief_name;  
      $client_folder=$client_id."-".$client_name;
      $brand_folder=$brand_id."-".$brand_name;
      $brief_folder=$brief_id."-".$brief_name;
      if (!is_dir("./upload/".$client_folder)) {
        mkdir("./upload/".$client_folder, 0777, true);
      } 
      if (!is_dir("./upload/".$client_folder."/".$brand_folder)) {
        mkdir("./upload/".$client_folder."/".$brand_folder, 0777, true);
      } 
      if (!is_dir("./upload/".$client_folder."/".$brand_folder."/".$brief_folder)) {
        mkdir("./upload/".$client_folder."/".$brand_folder."/".$brief_folder, 0777, true);
      } 
      $brief_path="./upload/".$client_folder."/".$brand_folder."/".$brief_folder;
      $timestamp=time();
      $file=$_FILES["image"]["name"];
      $ext = pathinfo($file, PATHINFO_EXTENSION);
      $tm=time();
      if($_FILES["image"]["type"]=='image/gif' || $_FILES["image"]["type"]=='image/png' || $_FILES["image"]["type"]=='image/bmp')
        {
          $file = $tm.".jpg";
        } 
        else {    
          $file = $tm.".".$ext;
        }     
      if(isset($_POST['image_id'])) 
        {
          $data['viewfilesinfo'] = $this->Uploadimages_model->insertimages1($file,$brief_id,$image_id);
          
          $image_id=$_POST['image_id'];
           $wc_brief_sql_ver = "SELECT version_num,img_status FROM `wc_image_upload` where brief_id='$brief_id' and parent_img=$image_id and parent_img!=image_id ORDER BY `version_num` DESC";
           $wc_brief_query_ver = $this->db->query($wc_brief_sql_ver);
           $wc_brief_result_ver=$wc_brief_query_ver->result_array();
           $num=sizeof($wc_brief_result_ver);
           if($num>0)
            //if(!empty($wc_brief_result_ver))
           {
            $max_ver=$wc_brief_result_ver[0]['version_num'];
           }
           else
           {
             $max_ver=1;
           }
           $count=$max_ver;
           $revision="Revision".$count;
          
          if (!is_dir($brief_path."/".$image_id)) {
          mkdir($brief_path."/".$image_id, 0777, true);
        }
        if (!is_dir($brief_path."/".$image_id."/".$revision)) {
          mkdir($brief_path."/".$image_id."/".$revision, 0777, true);
        } 
          
          $lastid=$image_id;
        }
      else 
      {
        //$data['viewfilesinfo'] = $this->Uploadimages_model->insertimages($file,$brief_id);
        $lastid=$this->Uploadimages_model->insertimages($file,$brief_id);
        if (!is_dir($brief_path."/".$lastid)) {
          mkdir($brief_path."/".$lastid, 0777, true);
        }
        if (!is_dir($brief_path."/".$lastid."/Revision1")) {
          mkdir($brief_path."/".$lastid."/Revision1", 0777, true);
        } 
        $revision="Revision1";
        
        
      }
  
      $dir =$brief_path."/".$lastid."/".$revision."/"; 
     //echo  $dir =$brief_path."/".$image_id."/".$revision."/"; exit;


      if($_FILES["image"]["type"]=='image/jpeg'|| $_FILES["image"]["type"]=='image/jpg' || $_FILES["image"]["type"]=='video/webm' || $_FILES["image"]["type"]=='video/mp4' || $_FILES["image"]["type"]=='video/3gpp' || $_FILES["image"]["type"]=='video/x-sgi-movie' || $_FILES["image"]["type"]=='video/x-msvideo' || $_FILES["image"]["type"]=='video/mpeg' || $_FILES["image"]["type"]=='video/x-ms-wmv' )
      {
         move_uploaded_file($_FILES["image"]["tmp_name"], $dir. $file);
         if($_FILES["image"]["type"]=='image/gif' || $_FILES["image"]["type"]=='image/png' || $_FILES["image"]["type"]=='image/bmp' || $_FILES["image"]["type"]=='image/jpg' || $_FILES["image"]["type"]=='image/jpeg'){
         //$file_image=$timestamp.pathinfo($_FILES["image"]["name"], PATHINFO_FILENAME);
         $file_image=pathinfo($_FILES["image"]["name"], PATHINFO_FILENAME);
         $new_thumb = $brief_path."/".$lastid."/".$revision."/thumb_".$file;
         $file_pat = $brief_path."/".$lastid."/".$revision."/".$file;
         $this->convertToThumb($new_thumb,$brief_id,$file_pat);
          //echo ltrim($file);
      }
      else
      {
         $file_image=pathinfo($_FILES["image"]["name"], PATHINFO_FILENAME);
         $new_thumb = $brief_path."/".$lastid."/".$revision."/"."thumb_".$file;
         $file_pat = "./assets/img/video_thumbnail.jpg";
         $this->convertToThumb($new_thumb,$brief_id,$file_pat);    
      }    
      }
      else
      {
        if($_FILES["image"]["type"]=='image/gif' || $_FILES["image"]["type"]=='image/png' || $_FILES["image"]["type"]=='image/bmp')
        {
        $img = $_FILES['image']['tmp_name'];
        $file_image=substr($file,0,-4);
        //////////////////////////Convert JPG start//////////////////////////////////////
        $file_to_convert=$this->convertToJpeg($dir,$img,$file_image);
         //////////////////////////Convert JPG end//////////////////////////////////////
         $new_thumb = $brief_path."/".$lastid."/".$revision."/thumb_".$file_image.".jpg";
         $file_pat = $brief_path."/".$lastid."/".$revision."/".$file_image.".jpg";
         $this->convertToThumb($new_thumb,$brief_id,$file_pat);
        }
      }
       //$this->load->view('front/viewfiles_search',$data);
      //echo insert_sql;
       $update_sql = "UPDATE `wc_brief` SET `status`='6' WHERE brief_id='$brief_id' ";
       $this->db->query($update_sql);




       /*********************************Send mail*************************************/
   
    $query=$this->db->query("select * from  wc_settings  WHERE setting_key='MAIL_SMTP_USER' ");
    $userDetails= $query->result_array();
    $MAIL_SMTP_USER=$userDetails[0]['setting_value'];

    $query=$this->db->query("select * from  wc_settings  WHERE setting_key='MAIL_SMTP_PASSWORD' ");
    $userDetails= $query->result_array();
    $MAIL_SMTP_PASSWORD=$userDetails[0]['setting_value'];

    $query=$this->db->query("select * from  wc_settings  WHERE setting_key='MAIL_BODY_NOTIFY_FILEUPLOAD' ");
    $userDetails= $query->result_array();
    $MAIL_BODY=$userDetails[0]['setting_value'];


    $checkquerys = $this->db->query("select * from wc_brief where  brief_id='$brief_id' ")->result_array();
    $brief_id=$checkquerys[0]['brief_id'];
    //$rejected_reasons=$checkquerys[0]['rejected_reasons'];
    $added_by_user_id=$checkquerys[0]['added_by_user_id'];





    $checkquerys = $this->db->query("select * from wc_users where  user_id='$added_by_user_id' ")->result_array();
    $brand_manager_email=$checkquerys[0]['user_email'];
    $brand_manager_name=$checkquerys[0]['user_name'];

    $subject='Notification mail for Card - '.$brief_id;

    //$unique_user_id=time()."-".$user_id;
    //$activation_link=base_url()."activate-registration/".base64_encode($unique_user_id);

        
        //Load email library
        $this->load->library('email');

        $config = array();
        //$config['protocol'] = 'smtp';
        $config['protocol']     = 'mail';
        $config['smtp_host'] = 'localhost';
        $config['smtp_user'] = $MAIL_SMTP_USER;
        $config['smtp_pass'] = $MAIL_SMTP_PASSWORD;
        $config['smtp_port'] = 25;




        $this->email->initialize($config);
        $this->email->set_newline("\r\n");

        
        $from_email = "demo.intexom@gmail.com";
        $to_email = $brand_manager_email;

        $date_time=date("d/m/y");
        $copy_right= date("Y");
        //echo $message=$MAIL_BODY;
        $find_arr = array("##BM_NAME##","##BRIEF_ID##","##IMAGE_ID##","##COPY_RIGHT##");
        $replace_arr = array($brand_manager_name,$brief_id,$lastid,$copy_right);
        $message=str_replace($find_arr,$replace_arr,$MAIL_BODY);

        //echo $message;exit;



        $this->email->from($from_email, 'Piquic');
        $this->email->to($to_email);
        $this->email->cc('poulami@mokshaproductions.in');
        $this->email->bcc('pranav@mokshaproductions.in');
        $this->email->subject($subject);
        $this->email->message($message);
        //Send mail
        if($this->email->send())
        {

            //echo "mail sent";exit;
            $this->session->set_flashdata("email_sent","Congragulation Email Send Successfully.");
        }
        
        else
        {
            //echo $this->email->print_debugger();
            //echo "mail not sent";exit;
            $this->session->set_flashdata("email_sent","You have encountered an error");
        }
        

    //exit;

/************************************************************************/



    }
    else
    {
    $data['user_id'] = '';
    $data['user_name'] = '';
    $user_id='';
    echo "sessionout";
    }
  }
	

public function convertToThumb($new_thumb,$brief_id,$file_path) {

  
  if(!file_exists($new_thumb)){
  $width = "200";
  $height = "150";
  $quality = 90;
  $img = $file_path;

  //Generate Thumbnail Images

  $file = $img;
  $dest = $new_thumb;
  $height = 150;
  $width = 200;
  $output_format = "jpg";
  $output_quality = 90;
  //$bg_color = array(255, 255, 255);
  $bg_color = array(0, 0, 0);


  //Justify the Image format and create a GD resource
  $image_info = getimagesize($file);
  list($cur_width, $cur_height, $cur_type, $cur_attr) = getimagesize($file);




  $image_type = $image_info[2];
  switch($image_type){
    case IMAGETYPE_JPEG:
    $image = imagecreatefromjpeg($file);
    break;
    case IMAGETYPE_GIF:
    $image = imagecreatefromgif($file);
    break;
    case IMAGETYPE_GIF:
    $image = imagecreatefrompng($file);
    break;
    default:
    /////////////////changes//////////////////////////
    die("notsupport");
  }

 //if($cur_width>$cur_height){
  //$degrees = -90;
  //$image = imagerotate($image, $degrees, 0);
  //}

  $image_width = imagesx($image);
  $image_height = imagesy($image);




  //echo $file;

  //Get The Calculations
  // Calculate the size of the Image
  //If Image width is bigger than the Thumbnail Width
  if($image_width>$image_height){





//echo "hoko";
//echo $image_width.">".$image_height;

        //Set Image Width to Thumbnail Width
    $new["width"] = $width;
        //Calculate Height according to width
    $new["height"] = ($new["width"]/$image_width)*$image_height;

        //If Resulting height is bigger than the thumbnail Height
    if($new["height"]>$height){

            //Set the image Height to THUmbnail Height
      $new["height"] = $height;
            //Recalculate width according to height of the thumbnail
      $new["width"] = ($new["height"]/$image_height)*$image_width;

    }

  }else{
//echo "moko";
    $new["height"] = $height;
    $new["width"] = ($new["height"]/$image_height)*$image_width;

    if($new["width"]>$width){

      $new["width"] = $width;
      $new["height"] = ($new["width"]/$image_width)*$image_height;

    }

  }

    //Calculate the image position based on the difference between the dimensons of the new image and thumbnail
  $x = ($width-$new["width"])/2;
  $y = ($height-$new["height"])/2;

  $calc =  array_merge($new, array("x"=>$x,"y"=>$y));


  // End Calculate The Image



  //Create an Empty image
  $canvas = imagecreatetruecolor($width, $height);

    //Load Background color
  $color = imagecolorallocate($canvas,
    $bg_color[0],
    $bg_color[1],
    $bg_color[2]
  );

    //FIll the Image with the Background color
  imagefilledrectangle(
    $canvas,
    0,
    0,
    $width,
    $height,
    $color
  );

    //The REAL Magic
  imagecopyresampled(
    $canvas,
    $image,
    $calc["x"],
    $calc["y"],
    0,
    0,
    $calc["width"],
    $calc["height"],
    $image_width,
    $image_height
  );

// Create Output Image

  $image = $canvas;
  $format = $output_format;
  $quality = $output_quality;



  switch($format){
    case "jpg":
    imagejpeg($image, $dest, $quality);
    break;
    case "gif":
    imagegif($image, $dest);
    break;
    case "png":
            //Png Quality is measured from 1 to 9
    imagepng($image, $dest, round(($quality/100)*9) );
    break;
  }

  //unlink($img);

}
else{
  echo "file is exist.";
}


}

public function convertToJpeg($dir,$img,$file_image) {

        $dst = $dir . $file_image;

        if (($img_info = getimagesize($img)) === FALSE)
        die("Image not found or not an image");

        $width = $img_info[0];
        $height = $img_info[1];

        switch ($img_info[2]) {
        case IMAGETYPE_GIF  : $src = imagecreatefromgif($img);  break;
        case IMAGETYPE_JPEG : $src = imagecreatefromjpeg($img); break;
        case IMAGETYPE_PNG  : $src = imagecreatefrompng($img);  break;
        default : die("Unknown filetype");
        }

        $tmp = imagecreatetruecolor($width, $height);

        $file =$dst.".jpg";

        imagecopyresampled($tmp, $src, 0, 0, 0, 0, $width, $height, $width, $height);
        imagejpeg($tmp, $file);
        return $file_image.".jpg";

}
	
	
	
	
	
	
	
	
	
	
	

}

?>