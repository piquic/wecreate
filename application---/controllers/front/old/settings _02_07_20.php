<?php 
class Settings extends CI_Controller{
 
    function __construct(){
        parent::__construct();
        $this->load->library(['session']); 
        $this->load->helper(['url','file','form']); 
        $this->load->model('Settings_model'); //load model upload 
        $this->load->model('Dashboard_model'); //load model upload 
        $this->load->model('Client_model');
        $this->load->library('upload'); //load library upload 
    }

    public function index(){

        if ($this->session->userdata('front_logged_in')) {
            $session_data = $this->session->userdata('front_logged_in');
            $data['user_id'] = $session_data['user_id'];
            $data['user_type_id'] = $session_data['user_type_id'];
            $data['user_name'] = $session_data['user_name'];
    		$type1 = $session_data['user_type_id'];
    		if($type1==2){
                $data['title'] = "Settings";
                $data['product_details']=$this->Settings_model->get_product_list();
                $this->load->view('front/settings',$data);
            }
            else{
                redirect('login', 'refresh');
            }
           
            // $this->load->view('front/dashboard_new',$data);
        }
        else{
            $data['user_id'] = '';
            $data['user_type_id'] = '';
            $data['user_name'] = '';
            $user_id='';
            redirect('login', 'refresh');
        }       
    }
    public function brief_permission(){
        $client_id = $this->input->post('client_id');
        $brief_permission = $this->input->post('brief_permission');
        $user_type_id = $this->input->post('user_type_id');
        $brand_id = $this->input->post('brand_id');

        $set_con = array('brief_permission' => $brief_permission, 'brand_id' => $brand_id);
        $where_con= array('client_id'=>$client_id,'user_type_id'=>$user_type_id);
        $this->db->where($where_con);
        $report = $this->db->update('wc_users', $set_con);
        print_r($report);
    }
    public function add_product()
    {
        //$brand_id=$_REQUEST['brand_id'];
        $product_name = $this->input->post('product_name');
        $product_description = $this->input->post('product_description');
        $product_price = $this->input->post('product_price');
        $productdata = array(
        'product_name'=>$product_name,
        'product_dest'=>$product_description,
        'product_price'=>$product_price,
        'status'=>'Active',
        'deleted'=>0,
        
        );
        $this->Settings_model->add_product($productdata);
        $data['product_details']=$this->Settings_model->get_product_list();
        $this->load->view('front/settings_list', $data);  
        //$brief_id=$this->db->insert_id();
    }
    public function update_product()
    {
        //$brand_id=$_REQUEST['brand_id'];
        $product_id = $this->input->post('product_id');
        $product_name = $this->input->post('product_name');
        $product_description = $this->input->post('product_description');
        $product_price = $this->input->post('product_price');
        $productdata = array(
        'product_name'=>$product_name,
        'product_dest'=>$product_description,
        'product_price'=>$product_price,
        'status'=>'Active',
        'deleted'=>0,
        
        );
        $this->Settings_model->update_product($productdata,$product_id);
        $data['product_details']=$this->Settings_model->get_product_list();
        $this->load->view('front/settings_list', $data);  
        //$brief_id=$this->db->insert_id();
    }
    public function delete_product()
    {
        $product_id=$this->input->post('product_id');
        $this->Settings_model->delete_product($product_id);
        $data['product_details']=$this->Settings_model->get_product_list();
        $this->load->view('front/settings_list', $data); 
          
    }
    public function update_project_desc()
    {
        //$brand_id=$_REQUEST['brand_id'];
     
        $client_id = $this->input->post('client_id');
        
        $data = array(
        'project_description'=>$this->input->post('project_description'),
        );
        //print_r($data);
        echo $result= $this->Client_model->client_update($data,$client_id);
        //$brief_id=$this->db->insert_id();
    }
    public function update_notifications()
    {
        $user_id = $this->input->post('user_id');
        $status = $this->input->post('status');
        $settingdata = array(
        'admin_mail_notifications'=>$status,
          
        );
       echo $this->Settings_model->update_notifications($settingdata,$user_id);
        
        
    }



}

?>