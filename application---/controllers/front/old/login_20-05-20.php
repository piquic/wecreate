<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
class Login extends CI_Controller
{
    public function __construct()
    {
		
        parent::__construct();
        $this->load->library('pagination');
        //$this->load->helper('url');
        $this->load->model('Login_model');
        $this->load->model('Dashboard_model');
        $this->load->library('pagination');
		$this->load->database();		
		$this->load->library('Upload');
		$this->load->helper('security');	
		$this->load->library('Form_validation');
		$this->load->helper(array('form', 'html', 'file'));
		$this->load->library('pagination');
		$this->load->library('email');
        $this->load->library('userlib');
        $this->load->helper(array('cookie', 'url')); 
		$this->load->library('facebook'); 


  }


    public function index()
    {
    
       
 
    	 if ($this->session->userdata('front_logged_in')) {
            $session_data = $this->session->userdata('front_logged_in');
            $data['user_id'] = $session_data['user_id'];
            $data['user_type_id'] = $session_data['user_type_id'];
            $data['user_name'] = $session_data['user_name'];

			redirect('dashboard');
			
         
            } else {
                
            $data['user_id'] = '';
            $data['user_type_id'] = '';
            $data['user_name'] = '';

			$this->load->view('front/login', $data);   
        }


		
    }


    public function check_login()
    {
    	$this->load->helper('cookie'); 

    	//echo "<pre>";
    	//print_r($this->input->post());



    	 $email=$this->input->post('email');
	     $password=$this->input->post('password');
	     $remember_me=$this->input->post('remember_me');
	    


    	$data['user_email']=htmlspecialchars($email);  
	    $data['user_password']=sha1(htmlspecialchars($password)); 
	    
	    // $password=sha1(htmlspecialchars($_POST['password']));  
	    //print_r($data);

	     

	    //set_cookie('cookie_user_name',$email,'662400'); 
	    //set_cookie('cookie_user_password',$password,'662400'); 


			$cookie = array(
			'name'   => 'cookie_user_name',
			'value'  => $email,
			'expire' => '862400',
			//'domain' => 'localhost',
			//'path'   => '/',
			//'prefix' => 'myprefix_',
			//'secure' => TRUE
			'secure' => false
			);

			$this->input->set_cookie($cookie);

			$cookie = array(
			'name'   => 'cookie_user_password',
			'value'  => $password,
			'expire' => '662400',
			//'domain' => 'localhost',
			//'path'   => '/',
			//'prefix' => 'myprefix_',
			'secure' => TRUE
			//'secure' => false
			);

			$this->input->set_cookie($cookie);


			 
           //echo $remember_me;
			$cookie = array(
			'name'   => 'cookie_remember_me',
			'value'  => $remember_me,
			'expire' => '662400',
			//'expire' => time()-60*60*24*30,
			//'domain' => 'localhost',
			//'path'   => '/',
			//'prefix' => 'myprefix_',
			//'secure' => TRUE
			'secure' => false
			);

			$this->input->set_cookie($cookie);

			//delete_cookie("cookie_user_name");
			//delete_cookie("cookie_user_password");





	         $this->output->disable_cache();



	  
	    $result=$this->Login_model->islogin($data);  
	       
	    //print_r($result);
	   
	        if ($result==0) 
            {
             	//$this->form_validation->set_message('check_database', '<font style="color:#ffff">Invalid username or password</font>');
                echo $result;   
            }
            else if ($result[0]->status=='Inactive') 
            {
             	//$this->form_validation->set_message('check_database', '<font style="color:#ffff">Invalid username or password</font>');
                echo "Inactive";   
            }
            else
            {
            	$sess_array = array();

            	  
                foreach ($result as $row) 
                {
                	$sess_array = array(
					    'id' => $row->user_id,
						'user_id' => $row->user_id,
						'user_type_id' => $row->user_type_id,
						'brand_access' => $row->brand_id,
						'client_access' => $row->client_id,
						'user_name' => $row->user_name,
						
                	);


					/**/







				
				}
				$this->session->set_userdata('front_logged_in', $sess_array);




				//$this->delete_temp_folderCron();

                //redirect('home', 'refresh');
				echo  "success";
            	
            }
    }










	public function checkexistemail()
	{			// checking the email  exist  validation in add form
		
	    // $user_email = $this->input->post('uemail');
	    // $sql = "SELECT user_id,user_email FROM wc_users WHERE user_email = '$user_email'";
	    // $query = $this->db->query($sql);
	    // if( $query->num_rows() > 0 ){
	    //     echo 'false';
	    // } else {
	    //     echo 'true';
	    // }
		if($this->input->post('uemail'))
		{
			$user_email = $this->input->post('uemail');
			$sql = "SELECT user_id,user_email FROM wc_users WHERE user_email = '$user_email'";
			$query = $this->db->query($sql);
			if( $query->num_rows() > 0 ){
				echo 'false';
			} else {
				echo 'true';
			}
		}
		if($this->input->post('femail'))
		{
			$user_email = $this->input->post('femail');
			$sql = "SELECT user_id,user_email FROM wc_users WHERE user_email = '$user_email'";
//print_r($sql);
			$query = $this->db->query($sql);
			if( $query->num_rows() ==1 ){
				echo 'true';
			} else {
				echo 'false';
			}
		}
		
	}

	public function checkexistmobile()
	{			// checking the email  exist  validation in add form
		
	    $user_mobile = $this->input->post('umobile');
	    $sql = "SELECT user_id,user_mobile FROM wc_users WHERE user_mobile = '$user_mobile'";
	    $query = $this->db->query($sql);
	    if( $query->num_rows() > 0 ){
	        echo 'false';
	    } else {
	        echo 'true';
	    }
		
	}
    public function login()
    {
       // echo "qqqqqqqq";exit;
		
		if ($this->session->userdata('front_logged_in')) 
        {
            //echo 'welcome to NC';
			redirect('home', 'refresh');
        } 
        else 
        {
            $this->load->helper(array('form'));
            $this->load->view('front/signin');
			
        }
    }

    function getRealIpAddr()
{
    if (!empty($_SERVER['HTTP_CLIENT_IP']))   //check ip from share internet
    {
      $ip=$_SERVER['HTTP_CLIENT_IP'];
    }
    elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR']))   //to check ip is pass from proxy
    {
      $ip=$_SERVER['HTTP_X_FORWARDED_FOR'];
    }
    else
    {
      $ip=$_SERVER['REMOTE_ADDR'];
    }

    //echo $ip='103.100.37.37';
    return $ip;;
}


    public function create_user()
    {


			$xml = simplexml_load_file("http://www.geoplugin.net/xml.gp?ip=".$this->getRealIpAddr());
			$countryName= $xml->geoplugin_countryName ;
			$currencyCode= $xml->geoplugin_currencyCode ;
			$currencySymbol= $xml->geoplugin_currencySymbol ;
			if($currencyCode!='')
			{
				$user_currency=$currencyCode;
				//$user_currency='USD';
				$user_currency='INR';
			}
			else
			{
				//$user_currency='USD';
				$user_currency='INR';
			}

			$data = array(
					'user_name'=>$this->input->post('uname'),
					'user_company'=>$this->input->post('cname'),
					'user_email'=>$this->input->post('uemail'),
					'user_password'=>sha1($this->input->post('upassword')),
					'user_mobile'=>$this->input->post('umobile'),
					'user_currency'=>$user_currency,
					/*'user_status'=>"Enable",*/
					'user_status'=>"Disable",
					'deleted'=>"0",
					);

					$this->Login_model->add_user($data);
					$user_id=$this->db->insert_id();




/*********************************Send mail*************************************/

	$checkquerys = $this->db->query("select * from wc_users where  user_id='$user_id' ")->result_array();
         // $user_id=$checkquerys[0]['user_id'];
         // $user_email=$checkquerys[0]['user_email'];
         // $user_name=$checkquerys[0]['user_name'];
         $client_id=$checkquerys[0]['client_id'];

	$query=$this->db->query("select * from  wc_settings  WHERE setting_key='MAIL_SMTP_USER' ");
	$userDetails= $query->result_array();
	$MAIL_SMTP_USER=$userDetails[0]['setting_value'];

	$query=$this->db->query("select * from  wc_settings  WHERE setting_key='MAIL_SMTP_PASSWORD' ");
	$userDetails= $query->result_array();
	$MAIL_SMTP_PASSWORD=$userDetails[0]['setting_value'];

	$query=$this->db->query("select * from  wc_settings  WHERE setting_key='MAIL_FROM' ");
    $userDetails= $query->result_array();
    $MAIL_FROM=$userDetails[0]['setting_value'];


    $query=$this->db->query("select * from  wc_settings  WHERE setting_key='MAIL_FROM_TEXT' ");
    $userDetails= $query->result_array();
    $MAIL_FROM_TEXT=$userDetails[0]['setting_value'];

	$query=$this->db->query("select * from  wc_settings  WHERE setting_key='MAIL_BODY_REGISTRATION' ");
	$userDetails= $query->result_array();
	$MAIL_BODY=$userDetails[0]['setting_value'];


	$uname=$this->input->post('uname');
	$uemail=$this->input->post('uemail');
	$subject=$MAIL_FROM_TEXT.' - Registration activation mail';

	$unique_user_id=time()."-".$user_id;
	$activation_link=base_url()."activate-registration/".base64_encode($unique_user_id);

		
		//Load email library
		$this->load->library('email');

		$config = array();
		//$config['protocol'] = 'smtp';
		$config['protocol']     = 'mail';
		$config['smtp_host'] = 'localhost';
		$config['smtp_user'] = $MAIL_SMTP_USER;
		$config['smtp_pass'] = $MAIL_SMTP_PASSWORD;
		$config['smtp_port'] = 25;




		$this->email->initialize($config);
		$this->email->set_newline("\r\n");
		if($client_id!='') {
                    $checkquerys = $this->db->query("select * from wc_clients where client_id='$client_id' ")->result_array();

                    $client_image=$checkquerys[0]['client_image'];
                    $wekan_client_id=$checkquerys[0]['wekan_client_id'];

                    $client_logo=base_url()."/upload_client_logo/".$client_image;
                    }
                    else
                    {
                    $client_logo=base_url()."/website-assets/images/logo.png";
                    }
                    $client_logo="http://style.piquic.com/WeCreate//website-assets/images/logo.png";
		
		// $from_email = "demo.intexom@gmail.com";
		$from_email = $MAIL_FROM;
		$to_email = $uemail;

		$date_time=date("d/m/y");
		 $copy_right=$MAIL_FROM_TEXT." - ".date("Y");
		//echo $message=$MAIL_BODY;
		$find_arr = array("##CLIENT_LOGO##","##USER_NAME##","##USER_EMAIL##","##USER_DATE##","##ACTIVATION_LINK##","##COPYRIGHT##");
		$replace_arr = array($client_logo,$uname,$uemail,$date_time,$activation_link,$copy_right);
		$message=str_replace($find_arr,$replace_arr,$MAIL_BODY);

		//echo $message;exit;



		$this->email->from($from_email,$MAIL_FROM_TEXT);
		$this->email->to($to_email);
		$this->email->cc('poulami@mokshaproductions.in');
        $this->email->bcc('pranav@mokshaproductions.in');
        $this->email->bcc('raja.priya@mokshaproductions.in');
		$this->email->subject($subject);
		$this->email->message($message);
		//Send mail
		if($this->email->send())
		{

			//echo "mail sent";exit;
			$this->session->set_flashdata("email_sent","Congragulation Email Send Successfully.");
		}
		
		else
		{
			//echo $this->email->print_debugger();
			//echo "mail not sent";exit;
			$this->session->set_flashdata("email_sent","You have encountered an error");
		}
		

	//exit;

/************************************************************************/









			echo $user_id;






    }


    

    


    public function setTypeSession()
    {
    	
        if ($this->session->userdata('front_logged_in')) 
        {
            $type1 = $this->input->post('type1');
			$this->session->set_userdata('type1', $type1);
		    echo "success";
        } 
        
    }


        public function check_database($password)
    {
        if ($this->session->userdata('logged_in')) 
        {
            redirect('home', 'refresh');
        } 
        else 
        {
            $username = $this->input->post('email');
			
			
       
        
            $result = $this->Login_model->loginfront($username, $password);
            if ($result) 
            {
                $sess_array = array();
                foreach ($result as $row) 
                {
				
				    $sess_array = array(
					    'id' => $row->user_id,
						'user_id' => $row->user_id,
						'email' => $row->user_email,
						'username' => $row->user_name
						
						
                	);
				
				}
				
				//print_r($sess_array);exit;
                    $this->session->set_userdata('front_logged_in', $sess_array);
					return TRUE;
            }
            else
            {
            	$this->form_validation->set_message('check_database', '<font style="color:#ffff">Invalid username or password</font>');
                return false;
            }
	
        }
    }

      public function logout()
    {




		@session_destroy();
        $this->session->unset_userdata('front_logged_in');
        $this->session->unset_userdata('wekan_integration');
        redirect('login', 'refresh');
    }
	
    public function forgotpassword()
	{			// checking the email  exist  validation in add form
		
	    $user_email = $this->input->post('femail');
	    $sql = "SELECT * FROM wc_users WHERE user_email = '$user_email'";
	    $query = $this->db->query($sql);
	    if( $query->num_rows() ==1 ){
	    	
	    	$userDetails= $query->result_array();
	    	$user_id=$userDetails[0]['user_id'];
			$user_name=ucfirst($userDetails[0]['user_name']);
			$user_email1=$userDetails[0]['user_email'];
			$user_email_value=explode("@",$user_email1);
			$user_email=substr_replace($user_email_value[0], "*****", 2).'@'.$user_email_value[1];
			$copy_right= date("Y");
			$url=base_url()."reset_password/".$user_id."/".time();



         	$client_id=$userDetails[0]['client_id'];
			//print_r($url);
			
			//echo base64_encode("http://localhost/piquic_client_new/reset_password");	       	

			/*********************************Send mail*************************************/

			$query=$this->db->query("select * from wc_settings WHERE setting_key='MAIL_SMTP_USER' ");
			$userDetails= $query->result_array();
			$MAIL_SMTP_USER=$userDetails[0]['setting_value'];

			$query=$this->db->query("select * from wc_settings WHERE setting_key='MAIL_SMTP_PASSWORD' ");
			$userDetails= $query->result_array();
			$MAIL_SMTP_PASSWORD=$userDetails[0]['setting_value'];

			$query=$this->db->query("select * from  wc_settings  WHERE setting_key='MAIL_FROM' ");
	        $userDetails= $query->result_array();
	        $MAIL_FROM=$userDetails[0]['setting_value'];


	        $query=$this->db->query("select * from  wc_settings  WHERE setting_key='MAIL_FROM_TEXT' ");
	        $userDetails= $query->result_array();
	        $MAIL_FROM_TEXT=$userDetails[0]['setting_value'];

			$query=$this->db->query("select * from wc_settings WHERE setting_key='MAIL_BODY_PASSWORD' ");
			$userDetails= $query->result_array();
			$MAIL_BODY_PASSWORD=$userDetails[0]['setting_value'];


			//Load email library
			$this->load->library('email');

			$config = array();
			//$config['protocol'] = 'smtp';
			$config['protocol'] = 'mail';
			$config['smtp_host'] = 'localhost';
			$config['smtp_user'] = $MAIL_SMTP_USER;
			$config['smtp_pass'] = $MAIL_SMTP_PASSWORD;
			$config['smtp_port'] = 25;

			$subject=$MAIL_FROM_TEXT.'   Account - Password Reset';


			$this->email->initialize($config);
			$this->email->set_newline("\r\n");


			// $from_email = "demo.intexom@gmail.com";
			  if($client_id!='') {
                    $checkquerys = $this->db->query("select * from wc_clients where client_id='$client_id' ")->result_array();

                    $client_image=$checkquerys[0]['client_image'];
                    $wekan_client_id=$checkquerys[0]['wekan_client_id'];

                    $client_logo=base_url()."/upload_client_logo/".$client_image;
                    }
                    else
                    {
                    $client_logo=base_url()."/website-assets/images/logo.png";
                    }
                    $client_logo="http://style.piquic.com/WeCreate//website-assets/images/logo.png";

        //$from_email = "demo.intexom@gmail.com";
       $from_email = $MAIL_FROM;
       $to_email = $user_email1;

        $date_time=date("d/m/y");
        $copy_right=$MAIL_FROM_TEXT." - ".date("Y");
			//$message=$MAIL_BODY_PASSWORD;
			$find_arr = array("##CLIENT_LOGO##","##USER_NAME##","##USER_EMAIL##",'##URL##','##COPY_RIGHT##');
			$replace_arr = array($client_logo,$user_name,$user_email,$url,$copy_right);
			$message=str_replace($find_arr,$replace_arr,$MAIL_BODY_PASSWORD);

			// echo $message;exit;
			$this->email->from($from_email, $MAIL_FROM_TEXT);
			$this->email->to($to_email);
			$this->email->cc('poulami@mokshaproductions.in');
			$this->email->cc('pranav@mokshaproductions.in');
			$this->email->bcc('raja.priya@mokshaproductions.in');
			$this->email->subject($subject);
			$this->email->message($message);
			//Send mail
			if($this->email->send())
			$this->session->set_flashdata("email_sent","Congragulation Email Send Successfully.");
			else
			$this->session->set_flashdata("email_sent","You have encountered an error");

			//exit;

			/************************************************************************/
	    } else {
	        echo 'flase';
	    }
		
	}
	public function reset_password($user_id,$time){
		echo "aaa";exit;
		$data['uid']= $user_id;
		//print_r($data);
		$u_time = $time;// fetching time variable from URL
		//echo $u_time = md5($u_time);
		$cur_time = time(); //fetching current time to check with GET variable's time
		//print_r($cur_time - $u_time);
		if ($cur_time - $u_time < 10800) 
		{
		  $this->load->view('front/reset_password',$data);  
		}
		else
		{
		  redirect('expired');
		}
		  
		//$this->load->view('front/reset_password',$data);    
	}

	public function reset_password_update(){
		$user_id = $this->input->post('uid');
		$user_password=sha1($this->input->post('upassword'));

		$status=$this->Login_model->rest_password_update($user_id,$user_password);

		//$this->load->view('front/home',$data);    
		//updated by rajendra			
		$sql = "SELECT * FROM wc_users WHERE user_id = '$user_id'";
	    $query = $this->db->query($sql);
	    if( $query->num_rows() ==1 ){
	    	
	    	$userDetails= $query->result_array();
	    	$user_id=$userDetails[0]['user_id'];
			$user_name=ucfirst($userDetails[0]['user_name']);
			$user_email1=$userDetails[0]['user_email'];
			$user_email_value=explode("@",$user_email1);
			$user_email=substr_replace($user_email_value[0], "*****", 2).'@'.$user_email_value[1];
			$copy_right= date("Y");
			$url=base_url()."reset_password/".$user_id."/".time();
			$client_id=$userDetails[0]['client_id'];
			//print_r($url);
			
			//echo base64_encode("http://localhost/piquic_client_new/reset_password");	       	

			/*********************************Send mail*************************************/

			$query=$this->db->query("select * from wc_settings WHERE setting_key='MAIL_SMTP_USER' ");
			$userDetails= $query->result_array();
			$MAIL_SMTP_USER=$userDetails[0]['setting_value'];

			$query=$this->db->query("select * from wc_settings WHERE setting_key='MAIL_SMTP_PASSWORD' ");
			$userDetails= $query->result_array();
			$MAIL_SMTP_PASSWORD=$userDetails[0]['setting_value'];


			$query=$this->db->query("select * from  wc_settings  WHERE setting_key='MAIL_FROM' ");
	        $userDetails= $query->result_array();
	        $MAIL_FROM=$userDetails[0]['setting_value'];


	        $query=$this->db->query("select * from  wc_settings  WHERE setting_key='MAIL_FROM_TEXT' ");
	        $userDetails= $query->result_array();
	        $MAIL_FROM_TEXT=$userDetails[0]['setting_value'];

			$query=$this->db->query("select * from wc_settings WHERE setting_key='MAIL_BODY_PASSWORD_SUCCESS' ");
			$userDetails= $query->result_array();
			$MAIL_BODY_PASSWORD_SUCCESS=$userDetails[0]['setting_value'];
			//Load email library
			$this->load->library('email');
			$config = array();
			//$config['protocol'] = 'smtp';
			$config['protocol'] = 'mail';
			$config['smtp_host'] = 'localhost';
			$config['smtp_user'] = $MAIL_SMTP_USER;
			$config['smtp_pass'] = $MAIL_SMTP_PASSWORD;
			$config['smtp_port'] = 25;


			$subject=$MAIL_FROM_TEXT.'   Account - Password Reset';


			$this->email->initialize($config);
			$this->email->set_newline("\r\n");
			// $from_email = "demo.intexom@gmail.com";
			 if($client_id!='') {
                    $checkquerys = $this->db->query("select * from wc_clients where client_id='$client_id' ")->result_array();

                    $client_image=$checkquerys[0]['client_image'];
                    $wekan_client_id=$checkquerys[0]['wekan_client_id'];

                    $client_logo=base_url()."/upload_client_logo/".$client_image;
                    }
                    else
                    {
                    $client_logo=base_url()."/website-assets/images/logo.png";
                    }
                    $client_logo="http://style.piquic.com/WeCreate//website-assets/images/logo.png";

        //$from_email = "demo.intexom@gmail.com";
       $from_email = $MAIL_FROM;
        $to_email = $user_email1;

        $date_time=date("d/m/y");
        $copy_right=$MAIL_FROM_TEXT." - ".date("Y");
			$find_arr = array("##CLIENT_LOGO##","##USER_NAME##","##USER_EMAIL##",'##COPY_RIGHT##');
			$replace_arr = array($client_logo,$user_name,$user_email,$copy_right);
			$message=str_replace($find_arr,$replace_arr,$MAIL_BODY_PASSWORD_SUCCESS);
			// echo $message;exit;
			$this->email->from($from_email,$MAIL_FROM_TEXT);
			$this->email->to($to_email);
			$this->email->cc('poulami@mokshaproductions.in');
			$this->email->cc('pranav@mokshaproductions.in');
			$this->email->bcc('raja.priya@mokshaproductions.in');
			$this->email->subject($subject);
			$this->email->message($message);
			//Send mail
			if($this->email->send())
			$this->session->set_flashdata("email_sent","Congragulation Email Send Successfully.");
			else
			$this->session->set_flashdata("email_sent","You have encountered an error");

			//exit;

			/************************************************************************/
	    } else {
	        echo 'flase';
	    }								
		//update by rajendra	
	}


	public function expired(){
	
			$this->load->view('front/expired');
      
		}












    public function uploadimage(){


    	if ($this->session->userdata('front_logged_in')) {
            $session_data = $this->session->userdata('front_logged_in');
            $data['user_id'] = $session_data['user_id'];
            $data['user_type_id'] = $session_data['user_type_id'];
            $data['user_name'] = $session_data['user_name'];
            
			$id='3';
			$data['pagecontents'] = $this->Login_model->get_page($id);
			$this->load->view('front/uploadimage', $data);    
			
         
        } else {
        	//echo "aaaaaa";exit;
            //If no session, redirect to login page
            redirect('login', 'refresh');
        }
			
			
			
			
		}



   /* public function uploadfile(){

    	if ($this->session->userdata('front_logged_in')) {
            $session_data = $this->session->userdata('front_logged_in');
            $data['user_id'] = $session_data['user_id'];
            $data['user_type_id'] = $session_data['user_type_id'];
            $data['user_name'] = $session_data['user_name'];
            $folderName=$data['user_id'];


            
			    $fileName = $_FILES["upld_zip"]["name"]; // The file name
				$fileTmpLoc = $_FILES["upld_zip"]["tmp_name"]; // File in the PHP tmp folder
				$fileType = $_FILES["upld_zip"]["type"]; // The type of file it is
				$fileSize = $_FILES["upld_zip"]["size"]; // File size in bytes
				$fileErrorMsg = $_FILES["upld_zip"]["error"]; // 0 for false... and 1 for true
				if (!$fileTmpLoc) { // if file not chosen
				echo "ERROR: Please select a file to upload.";
				exit();
				}
                
                $folder_path="zip/$folderName";
				if (!file_exists($folder_path)) {
				mkdir($folder_path, 0777, true);
				}
				if(move_uploaded_file($fileTmpLoc, "$folder_path/$fileName")){
				echo "ZIP file uploaded successfully!";

				} else {
				echo "ZIP file upload failed!";
				} 
			
         
        } else {
        	//echo "aaaaaa";exit;
            //If no session, redirect to login page
            redirect('login', 'refresh');
        }
			
			
			
				
		}*/


   
    /**/



    public function activateregistration($unique_user_id){


  

            $unique_user_id=base64_decode($unique_user_id);
            $unique_user_id_arr=explode("-",$unique_user_id);
            $user_id=trim($unique_user_id_arr[1]);

            $this->session->set_userdata('activate_user_id', $user_id);
            
			
			$query=$this->db->query("UPDATE `wc_users` SET `user_status`='Enable' WHERE user_id='$user_id' ");
         
        
			redirect('home', 'refresh');
			
			
			
		}



   
        public function pageContent($id){


           // echo "111111";exit;

			/*if ($this->session->userdata('front_logged_in')) {
			$session_data = $this->session->userdata('front_logged_in');
			$data['user_id'] = $session_data['user_id'];
            $data['user_type_id'] = $session_data['user_type_id'];
			$data['user_name'] = $session_data['user_name'];*/

			//$id='3';

			$msg= $this->uri->segment(4);

			if($msg=='success')
			{
				$data['success_msg'] ="Mail Sent  Successfully!."; 
			}
			else if($msg=='error')
			{
				$data['error_msg'] ="Mail Not Sent!."; 
			}
			

			

			


            $data['pagecontents'] = $this->Login_model->get_page($id);
			$this->load->view('front/page_content', $data);    


			/*} else {
			//echo "aaaaaa";exit;
			//If no session, redirect to login page
			redirect('login', 'refresh');
			}
			*/
			
			
		}

	public function sendContactUs()
	{
		$page_id = $this->input->post('page_id');
		$page_name = $this->input->post('page_name');


		$contact_name = $this->input->post('contact_name');
		$contact_email = $this->input->post('contact_email');
		$contact_description = $this->input->post('contact_description');


		       	

			/*********************************Send mail*************************************/

			$query=$this->db->query("select * from wc_settings WHERE setting_key='MAIL_SMTP_USER' ");
			$userDetails= $query->result_array();
			$MAIL_SMTP_USER=$userDetails[0]['setting_value'];

			$query=$this->db->query("select * from wc_settings WHERE setting_key='MAIL_SMTP_PASSWORD' ");
			$userDetails= $query->result_array();
			$MAIL_SMTP_PASSWORD=$userDetails[0]['setting_value'];

			$query=$this->db->query("select * from wc_settings WHERE setting_key='MAIL_BODY_CONTACTUS' ");
			$userDetails= $query->result_array();
			$MAIL_BODY_CONTACTUS=$userDetails[0]['setting_value'];
			//Load email library
			$this->load->library('email');
			$config = array();
			//$config['protocol'] = 'smtp';
			$config['protocol'] = 'mail';
			$config['smtp_host'] = 'localhost';
			$config['smtp_user'] = $MAIL_SMTP_USER;
			$config['smtp_pass'] = $MAIL_SMTP_PASSWORD;
			$config['smtp_port'] = 25;
			$this->email->initialize($config);
			$this->email->set_newline("\r\n");
			// $from_email = "demo.intexom@gmail.com";
			$from_email = $MAIL_SMTP_USER;
			$to_email = $contact_email;
			$message=$MAIL_BODY_CONTACTUS;
			$find_arr = array("##CONTACT_NAME##","##CONTACT_EMAIL##","##CONTACT_DESC##",'##COPY_RIGHT##');
			$copy_right= date("Y");
			$subject="Contact Us";
			$replace_arr = array($contact_name,$contact_email,$contact_description,$copy_right);
			$message=str_replace($find_arr,$replace_arr,$MAIL_BODY_CONTACTUS);
			// echo $message;exit;
			$this->email->from($from_email, 'Piquic Collab');
			$this->email->to($to_email);
			$this->email->cc('poulami@mokshaproductions.in');
			$this->email->cc('pranav@mokshaproductions.in');
			$this->email->bcc('raja.priya@mokshaproductions.in');
			$this->email->subject($subject);
			$this->email->message($message);
			//Send mail
			if($this->email->send())
			{
				//$this->session->set_flashdata("email_sent","Congragulation Email Send Successfully.");
				redirect('page-content/'.$page_id.'/'.$page_name.'/success');
			}
			else
			{
				//$this->session->set_flashdata("email_sent","You have encountered an error");
				redirect('page-content/'.$page_id.'/'.$page_name.'/error');
			}
			
			//exit;

		

	}



		































	

}

?>