<?php 
class Feedback extends CI_Controller{
    
    function __construct(){
        parent::__construct();
        $this->load->library(['session']); 
        $this->load->helper(['url','file','form']); 
        $this->load->model('uploadbrief_model'); //load model upload 
        $this->load->model('Dashboard_model'); //load model upload 
		$this->load->model('Feedback_model'); //load model upload
        $this->load->model('home_model'); //load model upload
        $this->load->library('upload'); //load library upload 
        $this->load->library('email');
    }

    public function index($brief_id){

        if ($this->session->userdata('front_logged_in')) {
        $session_data = $this->session->userdata('front_logged_in');
        $data['user_id'] = $session_data['user_id'];
        $data['user_name'] = $session_data['user_name'];
		$type1 = $session_data['user_type_id'];
		
		$data['brief_id'] = $brief_id;
        $id='2';
        $data['pagecontents'] = $this->home_model->get_page($id);
		
        $data['title'] = "upload images";
        $this->load->view('front/feedback',$data);    
        }
        else
        {
        $data['user_id'] = '';
        $data['user_name'] = '';
        $user_id='';
        redirect('login', 'refresh');
        }       
    }
    
     public function feedback_thankyou(){

       
        
        $data['title'] = "Feedback Thankyou";
        $this->load->view('front/feedback_thankyou',$data);    
          
    }

        public function feedback_submit($unique_feedback_id){

        $unique_feedback_id_str=base64_decode($unique_feedback_id);
        $unique_feedback_id_arr=explode("-",$unique_feedback_id_str);

        $time=$unique_feedback_id_arr[0];
        $brief_id=$unique_feedback_id_arr[1];
        $user_id=$unique_feedback_id_arr[2];


        $checkquerys = $this->db->query("select * from wc_users where  user_id='$user_id' ")->result_array();
        $user_id=$checkquerys[0]['user_id'];
        $user_email=$checkquerys[0]['user_email'];
        $user_name=$checkquerys[0]['user_name'];
        $user_type_id=$checkquerys[0]['user_type_id'];


        
        
         $data['user_id'] = $user_id;
        $data['user_name'] = $user_name;
        $data['type1'] = $user_type_id;
        $data['page_type'] = 'feedback_submit';
        




        
        $data['brief_id'] = $brief_id;
        $id='2';
        $data['pagecontents'] = $this->home_model->get_page($id);
        
        $data['title'] = "upload images";
        $this->load->view('front/feedback',$data);    
        
    }



public function insertFeedback(){
        /*if ($this->session->userdata('front_logged_in')) {
        $session_data = $this->session->userdata('front_logged_in');
        $data['user_id'] = $session_data['user_id'];
        $data['user_type_id'] = $session_data['user_type_id'];
        $data['user_name'] = $session_data['user_name'];
        $user_name = $session_data['user_name'];
        $user_id=$data['user_id'];
        $folderName=$data['user_id'];*/

        /*$arr=$this->input->post();


        echo "<pre>";
        print_r($arr);exit;*/
        $user_id = $this->input->post('user_id');
        $unique_id=time().rand().$user_id;
        $brief_id = $this->input->post('brief_id');
        $feedback_type_id_arr = $this->input->post('feedback_type_id');
        $review_arr = $this->input->post('review');
        /*$review_reason_arr = $this->input->post('review_reason');*/

        $review_reason = $this->input->post('review_reason');


        

        $checkquerys = $this->db->query("select * from wc_users where  user_id='$user_id' ")->result_array();
        $brand_id=$checkquerys[0]['brand_id'];

       foreach($feedback_type_id_arr as $key => $value)
       {
        $feedback_type_id=$value;
        $review=$review_arr[$key];
        //$review_reason=$review_reason_arr[$key];

        $data = array(
        'unique_id'=>$unique_id,
        'brief_id'=>$brief_id,
        'brand_id'=>$brand_id,
        'user_id'=>$user_id,
        'feedback_type_id'=>$feedback_type_id,
        'feedback_rating'=>$review,
        'feedback_reason'=>$review_reason,
        'feedback_datetime'=>date("Y-m-d H:i:s")
        );
        $this->Feedback_model->feedback_insert($data);
        $feedback_id=$this->db->insert_id();


       }
        

        

        $this->db->query("update wc_brief set status ='7' where brief_id ='$brief_id'");





    //}

}








	
	public function getfilessort()
	{
	 $brief_id=$_POST['brief_id'];
     $image_status=$_POST['status'];	
	 $data['viewfilesinfo'] = $this->Viewfiles_model->viewfilessearch($image_status,$brief_id);	
     $this->load->view('front/viewfiles_search',$data);
	
		
		
	}
	
	public function changefilestatus()
	{
	  $brief_id=$_POST['brief_id'];	
	 $image_id=$_POST['image_id'];
     $status=$_POST['status'];	
	 $data['viewfilesinfo'] = $this->Viewfiles_model->changefilestatus($image_id,$status,$brief_id);
	 $this->load->view('front/viewfiles_search',$data);
	
		
	}



    public function sendMailForFeedback($brief_id)
    {


    // $brief_id=$_POST['brief_id'];



     $this->db->query("update wc_brief set status ='5' where brief_id ='$brief_id'");
        
     $checkquerys = $this->db->query("select * from wc_brief where  brief_id='$brief_id' ")->result_array();
     $brand_id=$checkquerys[0]['brand_id'];
     $added_by_user_id=$checkquerys[0]['added_by_user_id'];


     $checkquerys = $this->db->query("select * from wc_users where  user_id='$added_by_user_id' ")->result_array();
     $user_id=$checkquerys[0]['user_id'];
     $user_email=$checkquerys[0]['user_email'];
     $user_name=$checkquerys[0]['user_name'];
     $client_id=$checkquerys[0]['client_id'];



/*********************************Send mail*************************************/

    $query=$this->db->query("select * from  wc_settings  WHERE setting_key='MAIL_SMTP_USER' ");
    $userDetails= $query->result_array();
    $MAIL_SMTP_USER=$userDetails[0]['setting_value'];

    $query=$this->db->query("select * from  wc_settings  WHERE setting_key='MAIL_SMTP_PASSWORD' ");
    $userDetails= $query->result_array();
    $MAIL_SMTP_PASSWORD=$userDetails[0]['setting_value'];

    $query=$this->db->query("select * from  wc_settings  WHERE setting_key='MAIL_BODY_FEEDBACK' ");
    $userDetails= $query->result_array();
    $MAIL_BODY=$userDetails[0]['setting_value'];


    $query=$this->db->query("select * from  wc_settings  WHERE setting_key='MAIL_FROM_TEXT' ");
    $userDetails= $query->result_array();
    $MAIL_FROM_TEXT=$userDetails[0]['setting_value'];

    $query=$this->db->query("select * from  wc_settings  WHERE setting_key='MAIL_SMTP_PASSWORD' ");
    $userDetails= $query->result_array();
    $MAIL_SMTP_PASSWORD=$userDetails[0]['setting_value'];


    $uname=$user_name;
    $uemail=$user_email;
    //$subject=$MAIL_FROM_TEXT.' - Feedback mail';
    $subject=$MAIL_FROM_TEXT.' - Feedback Mail(#'.$brief_id.')';

    $unique_feedback_id=time()."-".$brief_id."-".$user_id;
    $feedback_link=base_url()."feedback-submit/".base64_encode($unique_feedback_id);

        
        //Load email library
        $this->load->library('email');

        $config = array();
        //$config['protocol'] = 'smtp';
        $config['protocol']     = 'mail';
        $config['smtp_host'] = 'localhost';
        $config['smtp_user'] = $MAIL_SMTP_USER;
        $config['smtp_pass'] = $MAIL_SMTP_PASSWORD;
        $config['smtp_port'] = 25;




        $this->email->initialize($config);
        $this->email->set_newline("\r\n");

        if($client_id!='') {
                    $checkquerys = $this->db->query("select * from wc_clients where client_id='$client_id' ")->result_array();

                    $client_image=$checkquerys[0]['client_image'];
                    $wekan_client_id=$checkquerys[0]['wekan_client_id'];

                    $client_logo=base_url()."/upload_client_logo/".$client_image;
                    }
                    else
                    {
                    $client_logo=base_url()."/website-assets/images/logo.png";
                    }
                    $client_logo="http://style.piquic.com/WeCreate//website-assets/images/logo.png";


             $website_url=base_url();
                     $dashboard_url=base_url()."dashboard";         

        //$from_email = "demo.intexom@gmail.com";
       $from_email = $MAIL_FROM;
        $to_email = $uemail;

        $date_time=date("d/m/y");
        //$copy_right=$MAIL_FROM_TEXT." - ".date("Y");
         $copy_right=$MAIL_FROM_TEXT;
         $feedback_link="";
         $project_name="#".$brief_id." - ".$brief_title;
        //echo $message=$MAIL_BODY;
        /*$find_arr = array("##CLIENT_LOGO##","##BM_NAME##","##FEEDBACK_SUBMIT_LINK##","##COPY_RIGHT##");
        $replace_arr = array($client_logo,$uname,$feedback_link,$copy_right);*/

        $find_arr = array("##WEBSITE_URL##","##PROJECT_NAME##","##DASHBOARD_LINK##","##COPY_RIGHT##","##FEEDBACK_SUBMIT_LINK##");
        $replace_arr = array($website_url,$project_name,$dashboard_url,$copy_right,$feedback_link);

        $message=str_replace($find_arr,$replace_arr,$MAIL_BODY);

        //echo $message;exit;



        $this->email->from($from_email,$MAIL_FROM_TEXT);
        $this->email->to($to_email);
        if($_SERVER['HTTP_HOST']!='collab.piquic.com')
                    {
                        $this->email->cc(array('poulami@mokshaproductions.in','pranav@mokshaproductions.in','raja.priya@mokshaproductions.in','rajendra.prasad@mokshaproductions.in'));
                    }
        $this->email->subject($subject);
        $this->email->message($message);
        //Send mail
        if($this->email->send())
        {

            //echo "mail sent";exit;
            $this->session->set_flashdata("email_sent","Congratulation Email Send Successfully.");
        }
        
        else
        {
            //echo $this->email->print_debugger();
            //echo "mail not sent";exit;
            $this->session->set_flashdata("email_sent","You have encountered an error");
        }
        

    //exit;

/************************************************************************/





    
        
        
    }


    function getFeedbackContent()
    {


         $feedback_rating_id = $this->input->post('feedback_rating_id');
          $feedback_rating = $this->input->post('feedback_rating');
           $feedback_type_id = $this->input->post('feedback_type_id');


            $feedback_rating_sql = "SELECT * FROM `wc_feedback_content` where feedback_type_id='$feedback_type_id' and feedback_rating='$feedback_rating' and deleted ='0'  order by feedback_content_id asc ";

                                    $feedback_rating_query = $this->db->query($feedback_rating_sql);
                                    $feedback_rating_result=$feedback_rating_query->result_array();


                                    if(!empty($feedback_rating_result))
                                    {
                                    foreach($feedback_rating_result as $key => $value)
                                    {
                                    $feedback_content_id=$value['feedback_content_id'];
                                    $feedback_type_id=$value['feedback_type_id'];
                                    $feedback_content=$value['feedback_content'];
                                    

                                    ?>
                                    <span id="qty_<?php echo $feedback_content_id;?>" class="btn btn-outline-wc qty_btn_<?php echo $feedback_type_id;?> " onclick="setReviewReason('<?php echo $feedback_content;?>','<?php echo $feedback_type_id;?>','<?php echo $feedback_content_id;?>');"><?php echo $feedback_content;?></span>

                                    <?php
                                    }
                                    }
    }
    
	

}

?>