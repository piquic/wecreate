<?php 
class Proofing extends CI_Controller{
    
    function __construct(){
        parent::__construct();
        $this->load->library(['session']); 
        $this->load->helper(['url','file','form']); 
        $this->load->model('Proofing_model'); //load model upload 
        $this->load->model('home_model'); //load model upload 
        $this->load->library('upload'); //load library upload 
        $this->load->library('email');
        $this->load->model('Feedback_model'); //load model upload
    }

    public function index($brief_id){

        if ($this->session->userdata('front_logged_in')) {
            $session_data = $this->session->userdata('front_logged_in');
            $data['user_id'] = $session_data['user_id'];
            $data['user_name'] = $session_data['user_name'];
            $user_id = $session_data['user_id'];
            $user_type_id=$session_data['user_type_id'];

            $data['brief_id'] = $brief_id;
            $data['title'] = "Proofing";
            $checkquerys = $this->db->query("SELECT * FROM `wc_brief` WHERE `brief_id` ='$brief_id' and status='6'");
            $count=$checkquerys->num_rows();
            if($count>=1){
                $this->load->view('front/proofing',$data);   
            }else{
                redirect('login', 'refresh');
            }
        }
        else
        {
            $data['user_id'] = '';
            $data['user_name'] = '';
            $user_id='';
            redirect('login', 'refresh');
        }
    }

    public function update_status()
    {

        if ($this->session->userdata('front_logged_in')) {
            $session_data = $this->session->userdata('front_logged_in');
            $data['user_id'] = $session_data['user_id'];
            $data['user_name'] = $session_data['user_name'];
            $user_id = $session_data['user_id'];
            $user_type_id=$session_data['user_type_id'];

            $data['title'] = "Proofing";
            $image_id=$_POST['image_id'];
            $brief_id=$_POST['brief_id'];
            $img_status=$_POST['img_status'];

            // $array = array('image_id' => $image_id, 'brief_id' => $brief_id);
            // $img_status1= array('img_status' => $img_status);
            // $this->db->where($array);
            // $report = $this->db->update('wc_image_upload', $img_status1);
            // $sql = $this->db->last_query();

            if($img_status=="1"){
                $status_query1= $this->db->query("SELECT * FROM wc_image_upload WHERE brief_id='$brief_id' and image_id='$image_id'");
                $status_result1= $status_query1->result_array();
                $parent_img=$status_result1[0]['parent_img'];

                $array = array('image_id' => $image_id, 'brief_id' => $brief_id);
                $img_status1= array('img_status' => $img_status);
                $this->db->where($array);
                $report = $this->db->update('wc_image_upload', $img_status1);
                $sql = $this->db->last_query();

                

                $status_query= $this->db->query("SELECT * FROM wc_image_upload WHERE brief_id='$brief_id' and image_id IN (SELECT MAX(image_id) FROM wc_image_upload  GROUP BY parent_img )");
                $status_result= $status_query->result_array();
                //print_r($status_result);
                $status_count=$status_query->num_rows();
                $status_count1=0;
                if($status_count>="1"){
                    foreach ($status_result as $status_result_key => $status_result_value) {
                        $image_id1= $status_result_value['image_id'];
                        $status_query1= $this->db->query("SELECT * FROM `wc_image_upload` where brief_id='$brief_id' and image_id='$image_id1'");
                        $status_result1= $status_query1->result_array();
                        //print_r($status_result1);
                        $img_status1=$status_result1[0]['img_status'];
                        if($img_status1=="1"){
                            $status_count1++;  
                        }
                    }
                }
                if($status_count==$status_count1)
                {
                    $array = array('brief_id' => $brief_id);
                    $img_status1= array('status' => "5");
                    $this->db->where($array);
                    $report = $this->db->update('wc_brief', $img_status1);
                    $sql = $this->db->last_query();

                ///////////////////////////////////////////////////////////////////////////

             
                    
                 $checkquerys = $this->db->query("select * from wc_brief where  brief_id='$brief_id' ")->result_array();
                 $brand_id=$checkquerys[0]['brand_id'];
                 $added_by_user_id=$checkquerys[0]['added_by_user_id'];


                 $checkquerys = $this->db->query("select * from wc_users where  user_id='$added_by_user_id' ")->result_array();
                 $user_id=$checkquerys[0]['user_id'];
                 $user_email=$checkquerys[0]['user_email'];
                 $user_name=$checkquerys[0]['user_name'];




            /*********************************Send mail*************************************/

                $query=$this->db->query("select * from  wc_settings  WHERE setting_key='MAIL_SMTP_USER' ");
                $userDetails= $query->result_array();
                $MAIL_SMTP_USER=$userDetails[0]['setting_value'];

                $query=$this->db->query("select * from  wc_settings  WHERE setting_key='MAIL_SMTP_PASSWORD' ");
                $userDetails= $query->result_array();
                $MAIL_SMTP_PASSWORD=$userDetails[0]['setting_value'];

                $query=$this->db->query("select * from  wc_settings  WHERE setting_key='MAIL_BODY_FEEDBACK' ");
                $userDetails= $query->result_array();
                $MAIL_BODY=$userDetails[0]['setting_value'];


                $uname=$user_name;
                $uemail=$user_email;
                $subject='Piquic- Feedback mail';

                $unique_feedback_id=time()."-".$brief_id."-".$user_id;
                $feedback_link=base_url()."feedback-submit/".base64_encode($unique_feedback_id);

                    
                    //Load email library
                    $this->load->library('email');

                    $config = array();
                    //$config['protocol'] = 'smtp';
                    $config['protocol']     = 'mail';
                    $config['smtp_host'] = 'localhost';
                    $config['smtp_user'] = $MAIL_SMTP_USER;
                    $config['smtp_pass'] = $MAIL_SMTP_PASSWORD;
                    $config['smtp_port'] = 25;




                    $this->email->initialize($config);
                    $this->email->set_newline("\r\n");

                    
                    $from_email = "demo.intexom@gmail.com";
                    $to_email = $uemail;

                    $date_time=date("d/m/y");
                    $copy_right= date("Y");
                    //echo $message=$MAIL_BODY;
                    $find_arr = array("##BM_NAME##","##FEEDBACK_SUBMIT_LINK##","##COPY_RIGHT##");
                    $replace_arr = array($uname,$feedback_link,$copy_right);
                    $message=str_replace($find_arr,$replace_arr,$MAIL_BODY);

                    //echo $message;exit;



                    $this->email->from($from_email, 'Piquic');
                    $this->email->to($to_email);
                    $this->email->cc('poulami@mokshaproductions.in');
                    $this->email->bcc('pranav@mokshaproductions.in');
                    $this->email->bcc('raja.priya@mokshaproductions.in');
                    $this->email->subject($subject);
                    $this->email->message($message);
                    //Send mail
                    if($this->email->send())
                    {

                        //echo "mail sent";exit;
                        $this->session->set_flashdata("email_sent","Congragulation Email Send Successfully.");

                    }
                    
                    else
                    {
                        //echo $this->email->print_debugger();
                        //echo "mail not sent";exit;
                        $this->session->set_flashdata("email_sent","You have encountered an error");
                    }
                   
                    $report="0";
                //exit;

                } 

                echo $report;

            }
            else{

                $array = array('image_id' => $image_id, 'brief_id' => $brief_id);
                $img_status1= array('img_status' => $img_status);
                $this->db->where($array);
                $report = $this->db->update('wc_image_upload', $img_status1);
                $sql = $this->db->last_query();

                $wc_brief_sql_ver = "SELECT version_num FROM `wc_image_upload` where brief_id='$brief_id' and image_id='$image_id'";
                $wc_brief_query_ver = $this->db->query($wc_brief_sql_ver);
                $wc_brief_result_ver=$wc_brief_query_ver->result_array();
                $num=sizeof($wc_brief_result_ver);
                if($num>0){
                    $max_ver=$wc_brief_result_ver[0]['version_num'];
                }else{
                    $max_ver=1;
                }
                echo $max_ver;
            }
            
            // echo $report;
        }
        else
        {
            $data['user_id'] = '';
            $data['user_name'] = '';
            $user_id='';
            redirect('login', 'refresh');
        }

       
    }


    public function use_img()
    {

        if ($this->session->userdata('front_logged_in')) {
            $session_data = $this->session->userdata('front_logged_in');
            $data['user_id'] = $session_data['user_id'];
            $data['user_name'] = $session_data['user_name'];
            $user_id = $session_data['user_id'];
            $user_type_id=$session_data['user_type_id'];

           // $data['brief_id'] = $brief_id;
            $data['title'] = "Proofing";
            $brief_id=$_POST['brief_id'];
            
            $image_id=$_POST['image_id'];
            $img_status=$_POST['img_status'];

            //$checkquerys = $this->db->query("select * from wc_users where  user_id='$user_id' ")->result_array();
            // $checkquerys = $this->db->query("SELECT * FROM `wc_brief` WHERE `brief_id` ='$brief_id'")->result_array();
            // $brand_id=$checkquerys[0]['brand_id'];

            // $checkquerys = $this->db->query("select wc_brands.*,wc_clients.client_name from wc_brands inner join wc_clients on wc_brands.client_id=wc_clients.client_id where  wc_brands.brand_id='$brand_id' ")->result_array();
            // $brand_id=$checkquerys[0]['brand_id'];
            // $brand_name=preg_replace('/[^a-zA-Z0-9_.]/', '_',$checkquerys[0]['brand_name']);
            // $client_id=$checkquerys[0]['client_id'];
            // $client_name=preg_replace('/[^a-zA-Z0-9_.]/', '_',$checkquerys[0]['client_name']);
            $checkquerys = $this->db->query("select * from wc_brief where brief_id='$brief_id' ")->result_array();
            $added_by_user_id=$checkquerys[0]['added_by_user_id'];
            $brand_id=$checkquerys[0]['brand_id'];
            $brief_title=$checkquerys[0]['brief_title'];
            $brief_name=preg_replace('/[^a-zA-Z0-9_.]/', '_',$brief_title);

            /*$checkquerys = $this->db->query("select wc_brands.*,wc_clients.client_name from wc_brands inner join wc_clients on wc_brands.client_id=wc_clients.client_id where wc_brands.brand_id='$brand_id' ")->result_array();*/

            $checkquerys = $this->db->query("select wc_brands.*,wc_clients.client_name from wc_users
            inner join wc_brands on wc_users.brand_id=wc_brands.brand_id
            inner join wc_clients on wc_brands.client_id=wc_clients.client_id
            where wc_users.user_id='$added_by_user_id' ")->result_array();

            $brand_id=$checkquerys[0]['brand_id'];
            $brand_name=preg_replace('/[^a-zA-Z0-9_.]/', '_',$checkquerys[0]['brand_name']);
            $client_id=$checkquerys[0]['client_id'];
            $client_name=preg_replace('/[^a-zA-Z0-9_.]/', '_',$checkquerys[0]['client_name']);

            $checkquerys1 = $this->db->query("select * from wc_image_upload where  image_id='$image_id' ")->result_array();
            $version_num=$checkquerys1[0]['version_num'];
            $parent_img=$checkquerys1[0]['parent_img'];
            $file_type=$checkquerys1[0]['file_type'];
            if(!empty($_POST['image_path'])){
               $image_path=$_POST['image_path']; 
            }
            else{
                $image_path=$checkquerys1[0]['image_path'];
            }

            // $checkquerys2 = $this->db->query("select * from wc_revision where  revision_id='$revision_id' ")->result_array();
            // $revision_name=$checkquerys2[0]['revision_name'];

            $dst = "upload/".$client_id."-".$client_name."/".$brand_id."-".$brand_name."/".$brief_id."-".$brief_name."/".$parent_img."/Revision".$version_num;  
            $html="";

             $image_org="upload/".$client_id."-".$client_name."/".$brand_id."-".$brand_name."/".$brief_id."-".$brief_name."/".$parent_img."/Revision".$version_num."/".$image_path;


            $mime = mime_content_type($dst."/".$image_path);
            if(strstr($mime, "video/")){
            /*$div_con='<video id="video_'.$image_id.'" width="100%" height="100%" controls>
                       <source src="'.base_url().$image_org.'" type="video/mp4">Your browser does not support the video tag.
                            </video>';
*/

            $div_con='<div id="video_id"></div>';

            }else if(strstr($mime, "image/")){
            $div_con="<img class='border w-100 img_download' src='".base_url().$dst."/".$image_path."'>";
            }

            $select_value="";


            $html.="<div class='col-12 col-sm-12 col-md-12' >
                        <div class='p-4'>
                           
                        <div id='compare_img'>
                            ".$div_con."
                            </div>
                            ";
            if($img_status=='1')
            {
            
                $html.="<button type='submit' class='btn btn-wc btn-block m-2 px-5'>&emsp;&emsp;Approved&nbsp;<i class='fas fa-check'></i>&emsp;&emsp;</button>";
            }else{
                $html.=" <div class='row' id='update_approve'><div class='col-12 col-sm-12 col-md-12'>
                    <div class='d-flex justify-content-center flex-wrap'>
                       ";
                        if($img_status=='0' && ($user_type_id=='1'||$user_type_id=='2'||$user_type_id=='4')){
                            $html.="<button type='submit' class='btn btn-outline-wc btn-lg m-2 px-5' onclick='updateApprove(".$image_id.",".$brief_id.")'>&emsp;&emsp;Approve&emsp;&emsp;</button>";
                            $html.="<button type='submit' class='btn btn-outline-wc btn-lg m-2 px-5' onclick='review(".$image_id.",".$brief_id.",\"".$image_path."\",\"".$select_value."\",\"2\",\"".$file_type."\")'>&emsp;&emsp;Review&emsp;&emsp;</button>";
                        }
                       
                    // if($img_status=='4' && ($user_type_id=='1'||$user_type_id=='2'||$user_type_id=='3')){
                    //      $html.="<button type='submit' class='btn btn-outline-wc btn-lg m-2 px-5' onclick='updateApprove(".$image_id.",".$brief_id.")'>&emsp;&emsp;Approve&emsp;&emsp;</button>";
                    // }
                   
                    // if($img_status=='0' && ($user_type_id=='1'||$user_type_id=='2'||$user_type_id=='4')){
                    //      $html.="<button type='submit' class='btn btn-outline-wc btn-lg m-2 px-5' onclick='review(".$image_id.",".$brief_id.",\"".$image_path."\")'>&emsp;&emsp;Review&emsp;&emsp;</button>";
                    // }
                    $html.="
                    </div></div>
                </div>
                ";
            }
              $html.="
                    </div>
                            </div>";
            echo $html;
           
        }
        else
        {
            $data['user_id'] = '';
            $data['user_name'] = '';
            $user_id='';
            redirect('login', 'refresh');
        }
        
    }

    public function review()
    {
         if ($this->session->userdata('front_logged_in')) {
            $session_data = $this->session->userdata('front_logged_in');
            $data['user_id'] = $session_data['user_id'];
            $data['user_name'] = $session_data['user_name'];
            $user_id = $session_data['user_id'];
            $user_type_id=$session_data['user_type_id'];

            //$data['brief_id'] = $brief_id;
            $data['title'] = "Proofing";
            $brief_id=$_POST['brief_id'];
            $image_path=$_POST['image_path'];
            $image_id=$_POST['image_id'];

            $query= $this->db->query("SELECT * FROM wc_image_upload WHERE image_id='$image_id'");
            $rs= $query->result_array();
            //echo $rs[0]['img_status'];
             if(empty($_POST['image_path'])){
               $image_path=$rs[0]['image_path'];
            }
            else{
               $image_path=$_POST['image_path'];
            }
            if($rs[0]['img_status']==0)
            {
                $array = array('image_id' => $image_id, 'brief_id' => $brief_id);
                $img_status1= array('img_status' => '2');
                $this->db->where($array);
                $report = $this->db->update('wc_image_upload', $img_status1);
            }
            $wc_brief_sql = "SELECT * FROM `wc_brief` where brief_id='$brief_id'";
            $wc_brief_query = $this->db->query($wc_brief_sql);

            $wc_brief_result=$wc_brief_query->result_array();
            //print_r($wc_brief_result);
            // $wc_brief_qrydata = mysqli_fetch_array($wc_brief_query);
            $brief_title=$wc_brief_result[0]['brief_title'];

            $image_upload_query1= $this->db->query("SELECT * FROM wc_image_upload WHERE image_id='$image_id'");
            $image_upload_rs1= $image_upload_query1->result_array();
            $image_upload_rs1[0]['img_status'];
            if($image_upload_rs1[0]['img_status']==2 && ($user_type_id=='1'||$user_type_id=='2'||$user_type_id=='4')){ 
                $img_id= 'imgtag';
            } 
            else{
                $img_id= '';
            }
            

            //$checkquerys = $this->db->query("select * from wc_users where  user_id='$user_id' ")->result_array();
            //  $checkquerys = $this->db->query("SELECT * FROM `wc_brief` WHERE `brief_id` ='$brief_id'")->result_array();
            // $brand_id=$checkquerys[0]['brand_id'];

            // $checkquerys = $this->db->query("select wc_brands.*,wc_clients.client_name from wc_brands inner join wc_clients on wc_brands.client_id=wc_clients.client_id where  wc_brands.brand_id='$brand_id' ")->result_array();
            // $brand_id=$checkquerys[0]['brand_id'];
            // $brand_name=preg_replace('/[^a-zA-Z0-9_.]/', '_',$checkquerys[0]['brand_name']);
            // $client_id=$checkquerys[0]['client_id'];
            // $client_name=preg_replace('/[^a-zA-Z0-9_.]/', '_',$checkquerys[0]['client_name']);

            $checkquerys = $this->db->query("select * from wc_brief where brief_id='$brief_id' ")->result_array();
            $added_by_user_id=$checkquerys[0]['added_by_user_id'];
            $brand_id=$checkquerys[0]['brand_id'];
            $brief_title=$checkquerys[0]['brief_title'];
            $brief_name=preg_replace('/[^a-zA-Z0-9_.]/', '_',$brief_title);

            /*$checkquerys = $this->db->query("select wc_brands.*,wc_clients.client_name from wc_brands inner join wc_clients on wc_brands.client_id=wc_clients.client_id where wc_brands.brand_id='$brand_id' ")->result_array();*/

            $checkquerys = $this->db->query("select wc_brands.*,wc_clients.client_name from wc_users
            inner join wc_brands on wc_users.brand_id=wc_brands.brand_id
            inner join wc_clients on wc_brands.client_id=wc_clients.client_id
            where wc_users.user_id='$added_by_user_id' ")->result_array();

            $brand_id=$checkquerys[0]['brand_id'];
            $brand_name=preg_replace('/[^a-zA-Z0-9_.]/', '_',$checkquerys[0]['brand_name']);
            $client_id=$checkquerys[0]['client_id'];
            $client_name=preg_replace('/[^a-zA-Z0-9_.]/', '_',$checkquerys[0]['client_name']);

            $checkquerys1 = $this->db->query("select * from wc_image_upload where  image_id='$image_id' ")->result_array();
            $parent_img=$checkquerys1[0]['parent_img'];
            $version_num=$checkquerys1[0]['version_num'];
            $file_type=$checkquerys1[0]['file_type'];


            $dst = "upload/".$client_id."-".$client_name."/".$brand_id."-".$brand_name."/".$brief_id."-".$brief_name."/".$parent_img."/Revision".$version_num;  
            $html=""; 
        


            $image_org="upload/".$client_id."-".$client_name."/".$brand_id."-".$brand_name."/".$brief_id."-".$brief_name."/".$image_id."/Revision".$version_num."/".$image_path;
            $mime = mime_content_type($dst."/".$image_path);
            if(strstr($mime, "video/")){
            /*$div_con='<video id="video_'.$image_id.'" width="100%" height="100%" controls>
                       <source src="'.base_url().$image_org.'" type="video/mp4">Your browser does not support the video tag.
                            </video>';*/

             $div_con='<div id="video_id"></div>';               
            }
            else if(strstr($mime, "image/")){
            $div_con="<img class='border w-100 img_download' id='".$img_id."' src='".base_url().$dst."/".$image_path."'>";
            }
            else
            {
                $div_con="";
            }
            // $query= $this->db->query("SELECT * FROM wc_image_annotation WHERE image_id='$image_id' and img_an_status='0'");
            // $count=$query->num_rows();

if($file_type=="1")
{

     $html.="<div class='col-12 col-sm-12 col-md-12'  id='img_div1'>
                        <div class='p-4' > ".$div_con;


}
else
{

     $html.="<div class='col-12 col-sm-12 col-md-9'  id='img_div1'>
                        <div class='p-4' > <div id='compare_img'>".$div_con."</div>
                             <div id='tagbox' class='tagbox'></div>
                            ";


}


           



                        if($image_upload_rs1[0]['img_status']==3)
                        {
                            $html.="<div class='bg-light w-100 text-center p-2 lead'>&emsp;&emsp;Under Revision&nbsp;<i class='fas fa-sync-alt'></i>&emsp;&emsp;</div>";
                        }
                        //  if($image_upload_rs1[0]['img_status']==4)
                        // {
                        //      $html.=" <div class='row' id='update_approve'><div class='col-12 col-sm-12 col-md-12'>
                        //         <div class='d-flex justify-content-center flex-wrap'>
                        //            ";
                        //         if($user_type_id=='1'||$user_type_id=='2'||$user_type_id=='3'){
                        //              $html.="<button type='submit' class='btn btn-outline-wc btn-lg m-2 px-5' onclick='updateApprove(".$image_id.",".$brief_id.")'>&emsp;&emsp;Approve&emsp;&emsp;</button>";
                        //         }
                               
                              
                        //         $html.="
                        //         </div></div>
                        //     </div>
                        //     </div>
                        //             </div>";
                        // }   
                          $html.=" <div class='row d-none' id='update_approve'>
                                        <div class='col-12 col-sm-12 col-md-12'>
                                        <div class='d-flex justify-content-center flex-wrap'>
                                           ";
                                            if($image_upload_rs1[0]['img_status']==2 && ($user_type_id=='1'||$user_type_id=='2'||$user_type_id=='4')){
                                                $html.="<button type='submit' class='btn btn-outline-wc btn-lg m-2 px-5' onclick='updateApprove(".$image_id.",".$brief_id.")'>&emsp;&emsp;Approve&emsp;&emsp;</button>";
                                               
                                            }
                                           
                                        // if($img_status=='4' && ($user_type_id=='1'||$user_type_id=='2'||$user_type_id=='3')){
                                        //      $html.="<button type='submit' class='btn btn-outline-wc btn-lg m-2 px-5' onclick='updateApprove(".$image_id.",".$brief_id.")'>&emsp;&emsp;Approve&emsp;&emsp;</button>";
                                        // }
                                       
                                        // if($img_status=='0' && ($user_type_id=='1'||$user_type_id=='2'||$user_type_id=='4')){
                                        //      $html.="<button type='submit' class='btn btn-outline-wc btn-lg m-2 px-5' onclick='review(".$image_id.",".$brief_id.",\"".$image_path."\")'>&emsp;&emsp;Review&emsp;&emsp;</button>";
                                        // }
                                        $html.="
                                        </div></div>
                                    </div>
                                    ";

                    if($file_type=='1')
                    {
                        $revision_list_style="style='display:none;'";
                    }
                    else
                    {
                        $revision_list_style="style='display:block;'";
                    }


                       $html.=  "
                           
                        </div>
                    </div>
               <div class='col-12 col-sm-12 col-md-3' id='revision_list' ".$revision_list_style."   >
                        <div class='py-4 w-100'>
                            <p class='lead' id='notes'></p>
                                <div class='list-id' id='list-id' > 
                                </div> 
                                <div id='sendRevision' class='upload-btn-wrapper'>";
                if($image_upload_rs1[0]['img_status']==2 && ($user_type_id=='1'||$user_type_id=='2'||$user_type_id=='4'))
                {
                    $html.="    <button type='submit' class='btn btn-outline-wc btn-block px-5 mb-2' onclick='sendRevision(\"".$image_id."\",\"".$brief_id."\",\"".$file_type."\")'>&emsp;&emsp;Send Revision&emsp;&emsp;</button>

                            <button type='submit' class='btn btn-outline-danger btn-block px-5' onclick='reviewCancel(\"".$image_id."\",\"".$brief_id."\")'>&emsp;&emsp;Cancel&emsp;&emsp;</button>";
                }
                else if($image_upload_rs1[0]['img_status']==3 && ($user_type_id=='1'||$user_type_id=='2'||$user_type_id=='3')){
                    $html.="<form action='' method='post' enctype='multipart/form-data' id='upldForm' name='upldForm".$image_id."'>
                                <input type='hidden' name='brief_id' id='brief_id' value='".$brief_id."'>
                                <input type='hidden' name='image_id' id='image_id' value='".$image_id."'>
                                <button type='submit' class='btn btn-outline-wc w-100 px-5 mb-2'>&emsp;&emsp;<i class='fas fa-upload'></i> Upload Revised File</button>
                                <input type='file' class='upload_revised_file_ooo' onchange='uploadFile()' accept='image/jpg,image/jpeg,image/png,video/mp4' name='files'  id='files'/>
                            </form>";
                }
                // else{
                //    $html.="eroor";
                // }
                // else if($rs[0]['img_status']==3)
                // {
                //     $html.="<button type='submit' class='btn btn-outline-wc btn-block px-5 mb-2'>&emsp;&emsp;".$brief_title ."Sent&nbsp;<i class='fas fa-sync-alt'></i>&emsp;&emsp;</button>";
                // }
                            $html.="
                            </div>
                        </div>

<div class='alert alert-danger text-center lead d-none' id='imageDanger' role='alert'><i class='far fa-thumbs-down fa-2x'></i><br><span id='imagetxtDanger'></span></div>


                    </div>

                    ";

            echo $html;
           
        }
        else
        {
            $data['user_id'] = '';
            $data['user_name'] = '';
            $user_id='';
            redirect('login', 'refresh');
        }

        
    }
    public function annotation_list()
    {
         if ($this->session->userdata('front_logged_in')) {
            $session_data = $this->session->userdata('front_logged_in');
            $data['user_id'] = $session_data['user_id'];
            $data['user_name'] = $session_data['user_name'];
            $user_id = $session_data['user_id'];
            $user_type_id=$session_data['user_type_id'];

            //$data['brief_id'] = $brief_id;
            $data['title'] = "Proofing";
            $image_id=$_POST['image_id'];
           // print_r($_POST['select_value']);
            if(isset($_POST['select_value'])=="change")
            {
                $query= $this->db->query("SELECT * FROM wc_image_annotation WHERE image_id='$image_id' and (img_an_status='0' || img_an_status='1')");
                $rs= $query->result_array();
               /// print_r("SELECT * FROM wc_image_annotation WHERE image_id='$image_id' and (img_an_status='0' || img_an_status='1')");
            }
            else if(!isset($_POST['select_value'])){
                $query= $this->db->query("SELECT * FROM wc_image_annotation WHERE image_id='$image_id' and img_an_status='0'");
                $rs= $query->result_array();
               
            }
            // $query= $this->db->query("SELECT * FROM wc_image_annotation WHERE image_id='$image_id' and img_an_status='0'");
            // $rs= $query->result_array();
            //print_r($rs);


            $query1= $this->db->query("SELECT * FROM wc_image_upload WHERE image_id='$image_id'");
            $rs1= $query1->result_array();
            //echo $rs1[0]['img_status'];

            $count=$query->num_rows();
            $data['boxes'] = '';
            $data['lists'] = '';
            $data['notes'] = "Notes - Revision".$rs1[0]['version_num'] ;
            $data['send_revision']="";
            $i=1;
            if($count>0)
            {
                if ($rs){
                    foreach ($rs as $rs_key => $rs_value) {

                        $file_type=$rs_value['file_type'];
                        if($file_type=='1')
                        {
                            $style_display="visibility: hidden;";
                        }
                        else
                        {
                             $style_display="";
                        }
                     //echo $rs_value['id'];
                   
                    // do{
                    //  echo $rs_value['id'];
                        $data['boxes'] .= '<div class="tagview" style="left:' . $rs_value['position_left'] . 'px;top:' . $rs_value['position_top'] . 'px;" >';
                        $data['boxes'] .= '<div class="square"></div>';
                        $data['boxes'] .= '<div class="person popup" style="left:' . $rs_value['position_left'] . 'px;top:' . $rs_value['position_top']  . 'px;" id="view_'.$rs_value['id'].'">' . $rs_value['descr'] . '</div>';
                        $data['boxes'] .= '</div>';

                        $data['lists'] .= '<p id="'.$rs_value['id'].'">
                        
                       
                        <a class="name" id="span_name' . $rs_value['id'] . '" style="line-height:30px;">' . $rs_value['descr'] . '</a>
                        <textarea class="form-control input-sm d-none" type="text" id="name'.$rs_value['id'].'" name="name" >'. $rs_value['descr'] . '</textarea>
                        ';
                        $data['lists'] .='';
                        if($rs1[0]['img_status']=='2' && $rs_value['img_an_status']=='0' && ($user_type_id=='1'||$user_type_id=='2'||$user_type_id=='4')){
                        $data['lists'] .='
                        <button type="button" id="editBtn'.$rs_value['id'].'" class="btn btn-sm btn-default" onclick="edit_button('.$rs_value['id'].')" style="background: none;     font-size: 20px;  '.$style_display.' ">
                        <i class="far fa-edit text-wc"></i>
                        </button> 

                        <button type="button" id="updateBtn'.$rs_value['id'].'" class="btn btn-sm btn-default d-none" onclick="update_button('.$rs_value['image_id'].','.$rs_value['id'].','. $rs_value['position_left'].','. $rs_value['position_top'].')" style="background: none;     font-size: 20px;"><i class="far fa-check-circle text-success"></i></button> 


                        <button type="button" id="cancleBtn'.$rs_value['id'].'" class="btn btn-sm btn-default d-none" onclick="cancle_button('.$rs_value['id'].')" style="background: none;     font-size: 20px;"><i class="far fa-times-circle text-danger" aria-hidden="true"></i></button> 

                        <button type="button" class="btn btn-sm btn-default " id="delete_'.$rs_value['id'].'" onclick="remove1('.$rs_value['id'].')" style="background: none;     font-size: 20px; '.$style_display.' ">
                        <i class="far fa-trash-alt text-danger"></i>
                        </button> 

                        <button type="button" id="remove_'.$rs_value['id'].'" class="btn btn-sm btn-default d-none" onclick="remove('.$rs_value['id'].','.$rs_value['image_id'].')" style="background: none;     font-size: 20px;"><i class="far fa-check-circle text-success"></i></button>

                            <button type="button" id="cancleBtn1'.$rs_value['id'].'" class="btn btn-sm btn-default d-none" onclick="cancle_button1('.$rs_value['id'].')" style="background: none;     font-size: 20px;"><i class="far fa-times-circle text-danger" aria-hidden="true"></i></button> 
                            ';
                        }
                       // echo $rs_value['img_an_status'];
                        if($user_type_id=='1'||$user_type_id=='2'||$user_type_id=='3'){
                            if($rs_value['img_an_status']=='0'){
                                $data['send_revision']='1';
                            }
                            else{
                                $data['send_revision']='0';
                            }
                        }
                        else if($user_type_id=='1'||$user_type_id=='2'||$user_type_id=='4'){
                           if($rs_value['img_an_status']=='1'){
                                $data['send_revision']='0';
                            }
                            else{
                                $data['send_revision']='1';
                            }
                        }

                        $data['lists'] .= ' 
                        </p>';
                       $i++;
                    // }while($rs = $query->result_array());
                    }
                }
            }
            else{
                if($user_type_id=='1'||$user_type_id=='2'||$user_type_id=='4'){

                    if($rs1[0]['file_type']=='1')
                    {
                        $data['lists'] .= '<span class="lead">Drag drop dustom text/HTML on video to add the revision ..</span>';

                    }
                    else
                    {
                        $data['lists'] .= '<span class="lead">Click on the image to add the revision ..</span>';

                    }

                    



                    $data['send_revision']='0';
                }
            }

           echo json_encode( $data );
           
        }
        else
        {
            $data['user_id'] = '';
            $data['user_name'] = '';
            $user_id='';
            redirect('login', 'refresh');
        }
    }
    public function annotation_status()
    {
        $image_id=$_POST['image_id'];
        $array = array('image_id' => $image_id);
        $img_status1= array('img_an_status' => '1');
        $this->db->where($array);
        $report = $this->db->update('wc_image_annotation', $img_status1);
        $sql = $this->db->last_query();
        echo $report; 
    }
    public function annotation_crud()
    {
         if ($this->session->userdata('front_logged_in')) {
            $session_data = $this->session->userdata('front_logged_in');
            $data['user_id'] = $session_data['user_id'];
            $data['user_name'] = $session_data['user_name'];
            $user_id = $session_data['user_id'];
            $user_type_id=$session_data['user_type_id'];            
            // $data['brief_id'] = $brief_id;
            $data['title'] = "Proofing";
            if( !empty( $_POST['type'] ) && $_POST['type'] == "insert" )
            {
                $image_id = $_POST['image_id']; 
                $descr = $_POST['descr'];
                $position_left = $_POST['position_left'];
                $position_top = $_POST['position_top'];
               
                $create_date=date('Y/m/d');

                $checkquerys1 = $this->db->query("select * from wc_image_upload where  image_id='$image_id' ")->result_array();
                $version_num=$checkquerys1[0]['version_num'];


                $sql = "INSERT INTO wc_image_annotation(`user_id`,`image_id`,`revision_id`,`descr`,  `position_left`, `position_top`, `create_date`)VALUES('$user_id','$image_id','$version_num','$descr','$position_left','$position_top','$create_date');";

                // print_r($sql);
                $qry =$this->db->query($sql);
            }

            if( !empty( $_POST['type'] ) && $_POST['type'] == "remove")
            {
              $id = $_POST['id'];
              $sql = "DELETE FROM wc_image_annotation WHERE id = '".$id."'";
              $qry =$this->db->query($sql);
            }
            if( !empty( $_POST['type'] ) && $_POST['type'] == "update")
            {
              

              $id = $_POST['id']; 
              $image_id = $_POST['image_id']; 
              $descr = $_POST['descr'];
              $position_left = $_POST['position_left'];
              $position_top = $_POST['position_top'];

              $sql = "UPDATE wc_image_annotation SET descr='$descr' WHERE id = $id && position_left=$position_left && position_top=$position_top";
             // print_r("UPDATE gd_image_annotation SET descr='$descr' WHERE id = $id && position_left=$position_left && position_top=$position_top");
               $qry =$this->db->query($sql);
            }

        }
        else
        {
            $data['user_id'] = '';
            $data['user_name'] = '';
            $user_id='';
            redirect('login', 'refresh');
        }
        
    }
    public function select_revision()
    {
        $brief_id=$_POST['brief_id'];
        $image_id=$_POST['image_id'];
        $wc_brief_sql="SELECT * FROM wc_image_upload WHERE brief_id='$brief_id' and image_id='$image_id'";

        $wc_brief_query = $this->db->query($wc_brief_sql);
        $wc_brief_result=$wc_brief_query->result_array();
        $parent_img= $wc_brief_result[0]['parent_img'];



        $wc_brief_sql1 = "SELECT * FROM `wc_image_upload` where brief_id='$brief_id' and parent_img='$parent_img' order by version_num desc";
       // print_r($wc_brief_sql1);
        $wc_brief_query1 = $this->db->query($wc_brief_sql1);

        $wc_brief_result1=$wc_brief_query1->result_array();
        //print_r($wc_brief_result1);
        $html="";
        $html.="<select class='custom-select img_filter' onchange='revision_filter(\"".$brief_id."\",this.value)'>";
        foreach($wc_brief_result1 as $key => $value){ 
            $image_id=$value['image_id'];
            $version_num=$value['version_num'];
                            
            $html.="<option  value='".$image_id."'> Revision". $version_num."</option>";
        }
        $html.="</select> ";
         echo $html;
    }
    public function filter_status()
    {
        $filter_status= $_POST['filter_status'];
        $brief_id=$_POST['brief_id'];
        $select_value="";

        //echo "<pre>";
       

        $checkquerys = $this->db->query("select * from wc_brief where brief_id='$brief_id' ")->result_array();
        $added_by_user_id=$checkquerys[0]['added_by_user_id'];
        $brand_id=$checkquerys[0]['brand_id'];
        $brief_title=$checkquerys[0]['brief_title'];
        $brief_name=preg_replace('/[^a-zA-Z0-9_.]/', '_',$brief_title);

        /*$checkquerys = $this->db->query("select wc_brands.*,wc_clients.client_name from wc_brands inner join wc_clients on wc_brands.client_id=wc_clients.client_id where wc_brands.brand_id='$brand_id' ")->result_array();*/

        $checkquerys = $this->db->query("select wc_brands.*,wc_clients.client_name from wc_users
        inner join wc_brands on wc_users.brand_id=wc_brands.brand_id
        inner join wc_clients on wc_brands.client_id=wc_clients.client_id
        where wc_users.user_id='$added_by_user_id' ")->result_array();

        $brand_id=$checkquerys[0]['brand_id'];
        $brand_name=preg_replace('/[^a-zA-Z0-9_.]/', '_',$checkquerys[0]['brand_name']);
        $client_id=$checkquerys[0]['client_id'];
        $client_name=preg_replace('/[^a-zA-Z0-9_.]/', '_',$checkquerys[0]['client_name']);


        //$dst = "upload/".$client_id."-".$client_name."/".$brand_id."-".$brand_name."/".$brief_id."-brief";  
        // $files_sql = "SELECT * FROM `wc_image_upload` where brief_id='$brief_id' group by parent_img";
        if($filter_status=="all")
        {
            $files_sql="SELECT * FROM wc_image_upload WHERE brief_id='$brief_id' and image_id IN (SELECT MAX(image_id) FROM wc_image_upload  GROUP BY parent_img )order by parent_img asc";
        }
        else{
            /*$files_sql="SELECT * FROM wc_image_upload WHERE brief_id='$brief_id' and img_status='$filter_status' and image_id IN (SELECT MAX(image_id) FROM wc_image_upload  GROUP BY parent_img )order by parent_img asc";*/

            if($filter_status=='2')
            {

                $files_sql="SELECT * FROM wc_image_upload WHERE brief_id='$brief_id' and img_status IN ('2','3','4') and image_id IN (SELECT MAX(image_id) FROM wc_image_upload  GROUP BY parent_img )order by parent_img asc";

            }
            else
            {
                $files_sql="SELECT * FROM wc_image_upload WHERE brief_id='$brief_id' and img_status IN ('$filter_status') and image_id IN (SELECT MAX(image_id) FROM wc_image_upload  GROUP BY parent_img )order by parent_img asc";

            }

            
        }
        $files_query = $this->db->query($files_sql);
        $files_result=$files_query->result_array();
        $file_count=$files_query->num_rows();
        //print_r($file_count);
        $html="";
        if($file_count>='1'){
            foreach($files_result as $keybill => $files_result1) {

                $img_status=$files_result1['img_status'];
                $image_path=$files_result1['image_path'];
                $img_title=$files_result1['img_title'];
                $image_id=$files_result1['image_id'];
                
                $checkquerys1 = $this->db->query("select * from wc_image_upload where  image_id='$image_id' ")->result_array();
                $parent_img=$checkquerys1[0]['parent_img'];
                $version_num=$checkquerys1[0]['version_num'];
                $file_type=$checkquerys1[0]['file_type'];


                $dst = "upload/".$client_id."-".$client_name."/".$brand_id."-".$brand_name."/".$brief_id."-".$brief_name."/".$parent_img."/Revision".$version_num;  
                //$files_name=explode('.', $image_path);
                // $img_status=$files_result1['status'];
                //print_r($files_name[0]);
                $image_name= explode(".", $files_result1['image_path']);
                $html.="<div class='col-12 col-sm-12 col-md-3 p-2'>
                            <div class='card'>
                                <div class='thumb-div' style='background-image: url(".base_url().$dst."/thumb_".$image_path.");' onclick='useimg(\"".$brief_id."\",\"".$image_path."\",\"".$image_id."\",\"".$img_status."\",\"".$select_value."\",\"".$file_type."\",\"".$version_num."\")'></div>
                                <!--<img id='temp_image_download".$image_id."' class='card-img-top img_download1' src='".base_url().$dst."/thumb_".$image_path."' alt='".$img_title."' onclick='useimg(\"".$brief_id."\",\"".$image_path."\",\"".$image_id."\",\"".$img_status."\",\"".$select_value."\",\"".$file_type."\",\"".$version_num."\")'>-->
                                  <div class='card-body'>
                                        <div class='d-flex justify-content-between'>
                                            <span class='lead' style='width: 70%; white-space:nowrap; overflow:hidden; text-overflow: ellipsis;'>".$image_name[0]."</span>";
                                            if($img_status=="1") {
                                               $html.=" <i class='fas fa-circle text-wc fa-2x'></i>";
                                            }elseif($img_status=="2") {
                                               $html.=" <i class='fas fa-sync-alt fa-2x'></i>";
                                            }elseif($img_status=="3"){
                                               $html.=" <i class='fas fa-circle text-danger fa-2x'></i>";
                                            }
                                $html.="
                                        </div>
                                    </div>
                            </div>
                        </div>";           
            }
                               
            echo $html;
        }
        else{
            $html.="<span class='lead'>No Result</span>";
             echo $html;
        }

    }  
        public function compare_button()
    {
        $brief_id=$_POST['brief_id'];
        $image_id=$_POST['image_id'];
        $wc_brief_sql="SELECT * FROM wc_image_upload WHERE brief_id='$brief_id' and image_id='$image_id'";

        $wc_brief_query = $this->db->query($wc_brief_sql);
        $wc_brief_result=$wc_brief_query->result_array();
        $parent_img= $wc_brief_result[0]['parent_img'];



        $wc_brief_sql1 = "SELECT * FROM `wc_image_upload` where brief_id='$brief_id' and parent_img='$parent_img'";
       // print_r($wc_brief_sql1);
        $wc_brief_query1 = $this->db->query($wc_brief_sql1);

        $wc_brief_result1=$wc_brief_query1->result_array();
        //print_r($wc_brief_result1);
        $html="";
        $count=0;


         $checkquerys = $this->db->query("select * from wc_brief where brief_id='$brief_id' ")->result_array();
            $added_by_user_id=$checkquerys[0]['added_by_user_id'];
            $brand_id=$checkquerys[0]['brand_id'];
            $brief_title=$checkquerys[0]['brief_title'];
            $brief_name=preg_replace('/[^a-zA-Z0-9_.]/', '_',$brief_title);

            $checkquerys = $this->db->query("select wc_brands.*,wc_clients.client_name from wc_users
            inner join wc_brands on wc_users.brand_id=wc_brands.brand_id
            inner join wc_clients on wc_brands.client_id=wc_clients.client_id
            where wc_users.user_id='$added_by_user_id' ")->result_array();

            $brand_id=$checkquerys[0]['brand_id'];
            $brand_name=preg_replace('/[^a-zA-Z0-9_.]/', '_',$checkquerys[0]['brand_name']);
            $client_id=$checkquerys[0]['client_id'];
            $client_name=preg_replace('/[^a-zA-Z0-9_.]/', '_',$checkquerys[0]['client_name']);

            // $checkquerys1 = $this->db->query("select * from wc_image_upload where  image_id='$image_id' ")->result_array();
            // $version_num=$checkquerys1[0]['version_num'];
            // $parent_img=$checkquerys1[0]['parent_img'];
        foreach($wc_brief_result1 as $key => $value){ 
            $image_id=$value['image_id'];
            $version_num=$value['version_num'];
            $image_path=$value['image_path'];
            $count=$count+1;
            $dst = "upload/".$client_id."-".$client_name."/".$brand_id."-".$brand_name."/".$brief_id."-".$brief_name."/".$parent_img;
            $image_org=$dst."/Revision".$version_num."/".$image_path;
        }
        
       $mime = mime_content_type($image_org);
        if(strstr($mime, "image/")){
            if($count>'1')
                {

                 /*echo $html="<div class='p-1'>
                                <button type='button' class='btn btn-outline-wc' onclick='compare(\"".$brief_id."\",\"".$parent_img."\")'>Compare</button>
                            </div>";*/

                echo $html="<button type='button' class='btn btn-outline-wc compare_button' onclick='compare(\"".$brief_id."\",\"".$parent_img."\",\"".$image_id."\")'>Compare</button>
                            <button type='button' class='btn btn-outline-wc reset_button d-none' onclick='compare_reset(\"".$brief_id."\",\"".$parent_img."\",\"".$image_id."\")'>Exit</button>
                 ";
                }
        }
      
    }
    public function compare()
    {
        if ($this->session->userdata('front_logged_in')) {
            $session_data = $this->session->userdata('front_logged_in');
            $data['user_id'] = $session_data['user_id'];
            $data['user_name'] = $session_data['user_name'];
            $user_id = $session_data['user_id'];
            $user_type_id=$session_data['user_type_id'];

           // $data['brief_id'] = $brief_id;
            $data['title'] = "Proofing";
            $brief_id=$_POST['brief_id'];
            
             if(!empty($_POST['parent_img'])){            
                $parent_img=$_POST['parent_img'];
            }
            else if(empty($_POST['parent_img'])){
                $image_id=$_POST['image_id'];
                $wc_brief_sql="SELECT * FROM wc_image_upload WHERE brief_id='$brief_id' and image_id='$image_id'";

                $wc_brief_query = $this->db->query($wc_brief_sql);
                $wc_brief_result=$wc_brief_query->result_array();
                $parent_img= $wc_brief_result[0]['parent_img'];
            }
            $img_filter_value=$_POST['img_filter_value'];
            $checkquerys = $this->db->query("select * from wc_brief where brief_id='$brief_id' ")->result_array();
            $added_by_user_id=$checkquerys[0]['added_by_user_id'];
            $brand_id=$checkquerys[0]['brand_id'];
            $brief_title=$checkquerys[0]['brief_title'];
            $brief_name=preg_replace('/[^a-zA-Z0-9_.]/', '_',$brief_title);

            $checkquerys = $this->db->query("select wc_brands.*,wc_clients.client_name from wc_users
            inner join wc_brands on wc_users.brand_id=wc_brands.brand_id
            inner join wc_clients on wc_brands.client_id=wc_clients.client_id
            where wc_users.user_id='$added_by_user_id' ")->result_array();

            $brand_id=$checkquerys[0]['brand_id'];
            $brand_name=preg_replace('/[^a-zA-Z0-9_.]/', '_',$checkquerys[0]['brand_name']);
            $client_id=$checkquerys[0]['client_id'];
            $client_name=preg_replace('/[^a-zA-Z0-9_.]/', '_',$checkquerys[0]['client_name']);

            // $checkquerys1 = $this->db->query("select * from wc_image_upload where  image_id='$image_id' ")->result_array();
            // $version_num=$checkquerys1[0]['version_num'];
            // $parent_img=$checkquerys1[0]['parent_img'];
            

            $dst = "upload/".$client_id."-".$client_name."/".$brand_id."-".$brand_name."/".$brief_id."-".$brief_name."/".$parent_img;  
            $html="";

            $div_con="";

            
            $div_con.="<div data-type='data-type-image'>";


            $wc_brief_sql1 = "SELECT * FROM `wc_image_upload` where brief_id='$brief_id' and parent_img='$parent_img' order by image_id desc limit 1";
           // print_r($wc_brief_sql1);
            $wc_brief_query1 = $this->db->query($wc_brief_sql1);

            $wc_brief_result1=$wc_brief_query1->result_array();
            $image_path=$wc_brief_result1[0]['image_path'];   
            $version_num=$wc_brief_result1[0]['version_num'];
            $image_org=$dst."/Revision".$version_num."/".$image_path;
            $mime = mime_content_type($image_org);
                
            
                 
           $wc_brief_sql1 = "SELECT * FROM `wc_image_upload` where brief_id='$brief_id' and image_id='$img_filter_value'";
           // print_r($wc_brief_sql1);
            $wc_brief_query1 = $this->db->query($wc_brief_sql1);

            $wc_brief_result1=$wc_brief_query1->result_array();
            $image_path1=$wc_brief_result1[0]['image_path'];   
            $version_num1=$wc_brief_result1[0]['version_num'];
            $image_org1=$dst."/Revision".$version_num1."/".$image_path1;

            if($version_num!=$version_num1){
              
                $mime = mime_content_type($image_org1);
                $div_con.="<div data-type='after'>";
            
                if(strstr($mime, "video/")){
                    $div_con.='<video id="video_'.$image_id.'" width="100%" height="100%" controls>
                               <source src="'.base_url().$image_org.'" type="video/mp4">Your browser does not support the video tag.
                                    </video>';
                }else if(strstr($mime, "image/")){
                    $div_con.="<img class='border img_download'  src='".base_url().$image_org."'>";
                }
                
                    $div_con.="</div>";
                $div_con.="<div data-type='before'>";
                
                if(strstr($mime, "video/")){
                    $div_con.='<video id="video_'.$image_id.'" width="100%" height="100%" controls>
                               <source src="'.base_url().$image_org1.'" type="video/mp4">Your browser does not support the video tag.
                                    </video>';
                }else if(strstr($mime, "image/")){
                    $div_con.="<img class='border img_download'  src='".base_url().$image_org1."'>";
                }
                
                    $div_con.="</div>";
                
               
            }
            else{
                $wc_brief_sql1 = "SELECT * FROM `wc_image_upload` where brief_id='$brief_id' and parent_img='$parent_img' order by image_id desc limit 2";
               //print_r($wc_brief_sql1);
                $wc_brief_query1 = $this->db->query($wc_brief_sql1);

                $wc_brief_result1=$wc_brief_query1->result_array();
                //print_r($wc_brief_result1);
                $i=1;
               
                foreach($wc_brief_result1 as $key => $value){ 
                   
                    $version_num3=$value['version_num'];
                    $image_path3=$value['image_path'];            
                    $image_org3=$dst."/Revision".$version_num3."/".$image_path3;
                    $mime = mime_content_type($image_org);
                    if($i=='2')
                    {
                        $div_con.="<div data-type='before'>";
                    }
                    else if($i=='1'){
                        $div_con.="<div data-type='after'>";
                    }
                    if(strstr($mime, "video/")){
                        $div_con.='<video id="video_'.$image_id.'" width="100%" height="100%" controls>
                                   <source src="'.base_url().$image_org3.'" type="video/mp4">Your browser does not support the video tag.
                                        </video>';
                    }else if(strstr($mime, "image/")){
                        $div_con.="<img class='border img_download'  src='".base_url().$image_org3."'>";
                    }
                    $i++;
                    $div_con.="</div>";
                                  
                              
                }
                
                
            
            }
            
            $div_con.='</div>';


            
            echo($div_con);
           
        }
        else
        {
            $data['user_id'] = '';
            $data['user_name'] = '';
            $user_id='';
            redirect('login', 'refresh');
        }
    }
    public function download_image()
    {
        
        if ($this->session->userdata('front_logged_in')) {
            $session_data = $this->session->userdata('front_logged_in');
            $data['user_id'] = $session_data['user_id'];
            $data['user_name'] = $session_data['user_name'];
            $user_id = $session_data['user_id'];
            $user_type_id=$session_data['user_type_id'];
            $download_value=$_POST['download_value'];

            if($download_value=="download_single"){
                $image_src=$_POST['image_src'];
                $image_src=explode("upload",$image_src);
                
                $image_src="./upload".str_replace("thumb_","",$image_src[1]);
                //
                $dir =  microtime();
                if (!file_exists($dir)) {
                    $save_to =$dir."/WE_CREATE_IMAGES".$dir."/";
                    mkdir($dir."/WE_CREATE_IMAGES".$dir, 0777, true);
                    
                }
                $zip_file ='WE_CREATE_IMAGES'.time().'.zip';
                // Get real path for our folder
                $rootPath = realpath($dir);

                // Initialize archive object
                $zip = new ZipArchive();
                $zip->open($zip_file, ZipArchive::CREATE | ZipArchive::OVERWRITE);

                // Create recursive directory iterator
                /** @var SplFileInfo[] $files */
                $files = new RecursiveIteratorIterator(
                    new RecursiveDirectoryIterator($rootPath),
                    RecursiveIteratorIterator::LEAVES_ONLY
                );
            
                $image_name1=explode("/",$image_src);
                $image_name=$save_to.end($image_name1);
                // print_r("array------".$image_name1);
                // print_r("image_name ---------------".$image_name);
                if(!copy($image_src,$image_name)){
                      // echo "failed to copy ".$image_name;
                }
                else{
                     // echo "copied $image_name into ".$image_name;
                }
       
                // exit();
                foreach ($files as $name => $file)
                {
                    // Skip directories (they would be added automatically)
                    if (!$file->isDir())
                    {
                        // Get real and relative path for current file
                        $filePath = $file->getRealPath();
                        $relativePath = substr($filePath, strlen($rootPath) + 1);

                        // Add current file to archive
                        $zip->addFile($filePath, $relativePath);
                    }
                }
      
                //exit();
                //Zip archive will be created only after closing object
                $zip->close();
                 //It it's a file.
                 
                if(file_exists($dir))
                {
                    $this->deleteAll($dir);
                    echo $zip_file;
                }
                else{
                    echo $dir;
                }
            }
            else if($download_value=="download_all")
            {
                $image_src=$_POST['image_src']; 
               
               //  $image_name1=explode("/",$image_src);
               // print_r($image_name1);
               //  $image_folder="./".$image_name1[4]."/".$image_name1[5]."/".$image_name1[6]."/".$image_name1[7]."/";
               //   //print_r($image_folder);
                $dir =  microtime();
                if (!file_exists($dir)) {
                    $save_to =$dir."/WE_CREATE_IMAGES".$dir."/";
                    mkdir($dir."/WE_CREATE_IMAGES".$dir, 0777, true);
                    
                }
                $zip_file ='WE_CREATE_IMAGES'.time().'.zip';
                // Get real path for our folder
                $rootPath = realpath($dir);

                // Initialize archive object
                $zip = new ZipArchive();
                $zip->open($zip_file, ZipArchive::CREATE | ZipArchive::OVERWRITE);

                // Create recursive directory iterator
                /** @var SplFileInfo[] $files */
                $files = new RecursiveIteratorIterator(
                    new RecursiveDirectoryIterator($rootPath),
                    RecursiveIteratorIterator::LEAVES_ONLY
                );
                $downloadimgid_array=explode(",",$image_src);
               // print_r($downloadimgid_array);
                foreach ($downloadimgid_array as $downloadimgid_array_key => $downloadimgid_array_value) {
                    $downloadimgid_array_value=explode("upload",$downloadimgid_array_value);
                    $downloadimgid_array_value="./upload".$downloadimgid_array_value[1];
                    $image_src1 =str_replace('thumb_', '',$downloadimgid_array_value);
                    //echo "image src   ". $image_src1;
                    //echo "<br>";
                    $image_name1=explode("/",$image_src1);
                    //print_r($image_name1);echo "<br>";
                    $image_name=$save_to.end($image_name1);
                    //print_r("image name ----- ".$image_name);echo "<br>";
                    if(!copy($image_src1,$image_name)){
                          // echo "failed to copy ".$image_name;
                    }
                    else{
                         // echo "copied $image_name into ".$image_name;
                    }
                }
                // $sku_thumb_images = glob($image_folder."*.{png,PNG,jpg,JPG}",GLOB_BRACE);
               
                // //     print_r($sku_thumb_images);
                // foreach($sku_thumb_images as $image){ 
                //    // print_r($image);
                //     $image_name1=explode("/",$image);
                //     $image_name=$save_to.end($image_name1);
                //     //print_r($image_name1);
                //     if(!copy($image,$image_name)){
                //           // echo "failed to copy ".$image_name;
                //     }
                //     else{
                //          // echo "copied $image_name into ".$image_name;
                //     }
                // }
                // exit();
                foreach ($files as $name => $file)
                {
                    // Skip directories (they would be added automatically)
                    if (!$file->isDir())
                    {
                        // Get real and relative path for current file
                        $filePath = $file->getRealPath();
                        $relativePath = substr($filePath, strlen($rootPath) + 1);

                        // Add current file to archive
                        $zip->addFile($filePath, $relativePath);
                    }
                }
          
                //exit();
                //Zip archive will be created only after closing object
                $zip->close();
                 //It it's a file.
                     
                if(file_exists($dir))
                {
                    $this->deleteAll($dir);
                    echo $zip_file;
                }
                else{
                    echo $dir;
                }
            }
        }
        else
        {
            $data['user_id'] = '';
            $data['user_name'] = '';
            $user_id='';
            redirect('login', 'refresh');
        }
        
       
    }
    public function deleteAll($str) {
        //It it's a file.
        if (is_file($str)) {
            //Attempt to delete it.
            return unlink($str);
        }
        //If it's a directory.
        elseif (is_dir($str)) {
            //Get a list of the files in this directory.
            $scan = glob(rtrim($str,'/').'/*');
            //Loop through the list of files.
            foreach($scan as $index=>$path) {
                //Call our recursive function.
               $this-> deleteAll($path);
            }
            //Remove the directory itself.
            return @rmdir($str);
        }
    }
public function upload_files_image(){
        if ($this->session->userdata('front_logged_in')) {
            $session_data = $this->session->userdata('front_logged_in');
            $data['user_id'] = $session_data['user_id'];
            $data['user_name'] = $session_data['user_name'];
            $user_id=$session_data['user_id'];
            //echo "<pre>";
            $brief_id=$_POST["brief_id"];
            if(isset($_POST['image_id'])){
                $image_id=$_POST["image_id"];   
            }
            
           
            $checkquerys = $this->db->query("select * from wc_brief where brief_id='$brief_id' ")->result_array();
            $added_by_user_id=$checkquerys[0]['added_by_user_id'];
            $brand_id=$checkquerys[0]['brand_id'];
            $brief_title=$checkquerys[0]['brief_title'];
            $brief_name=preg_replace('/[^a-zA-Z0-9_.]/', '_',$brief_title);


            $checkquerys = $this->db->query("select wc_brands.*,wc_clients.client_name from wc_users
            inner join wc_brands on wc_users.brand_id=wc_brands.brand_id
            inner join wc_clients on wc_brands.client_id=wc_clients.client_id
            where wc_users.user_id='$added_by_user_id' ")->result_array();

            $brand_id=$checkquerys[0]['brand_id'];
            $brand_name=preg_replace('/[^a-zA-Z0-9_.]/', '_',$checkquerys[0]['brand_name']);
            $client_id=$checkquerys[0]['client_id'];
            $client_name=preg_replace('/[^a-zA-Z0-9_.]/', '_',$checkquerys[0]['client_name']);

            $dst = "upload/".$client_id."-".$client_name."/".$brand_id."-".$brand_name."/".$brief_id."-".$brief_name;  
            $client_folder=$client_id."-".$client_name;
            $brand_folder=$brand_id."-".$brand_name;
            $brief_folder=$brief_id."-".$brief_name;
            if (!is_dir("./upload/".$client_folder)) {
                mkdir("./upload/".$client_folder, 0777, true);
            }   
            if (!is_dir("./upload/".$client_folder."/".$brand_folder)) {
                mkdir("./upload/".$client_folder."/".$brand_folder, 0777, true);
            }   
            if (!is_dir("./upload/".$client_folder."/".$brand_folder."/".$brief_folder)) {
                mkdir("./upload/".$client_folder."/".$brand_folder."/".$brief_folder, 0777, true);
            }   
            $brief_path="./upload/".$client_folder."/".$brand_folder."/".$brief_folder;
            $timestamp=time();
            //if (!is_dir("./brief_upload/".$brief_id)) {
            //mkdir("./brief_upload/".$brief_id, 0777, true);
            //}

            $file=$_FILES["image"]["name"];
            $ext = pathinfo($file, PATHINFO_EXTENSION);
            $file_name=preg_replace('/[^a-zA-Z0-9_.]/', '_',pathinfo($file, PATHINFO_FILENAME));
            $tm=time();
            //$file = $tm.".".$ext;
           // $file=$tm.".jpg";
            $file = $file_name."_".$tm.".".$ext;
           

            if($_FILES["image"]["type"]=='image/gif' || $_FILES["image"]["type"]=='image/png' || $_FILES["image"]["type"]=='image/bmp' || $_FILES["image"]["type"]=='image/jpg' || $_FILES["image"]["type"]=='image/jpeg')
            {
            $file_type="0";
            }
            elseif($_FILES["image"]["type"]=='video/webm' || $_FILES["image"]["type"]=='video/mp4' || $_FILES["image"]["type"]=='video/3gpp' || $_FILES["image"]["type"]=='video/x-sgi-movie' || $_FILES["image"]["type"]=='video/x-msvideo' || $_FILES["image"]["type"]=='video/mpeg' || $_FILES["image"]["type"]=='video/x-ms-wmv' )
            {

            $file_type="1";

            }






            if(isset($_POST['image_id'])) 
            {
                //  $data['viewfilesinfo'] = $this->Proofing_model->insertimages1($file,$brief_id,$image_id);
                $lastid= $this->Proofing_model->insertimages1($file,$file_type,$brief_id,$image_id);
            }
            else {
                //$data['viewfilesinfo'] = $this->Proofing_model->insertimages($file,$brief_id);
                $lastid=$this->Proofing_model->insertimages($file,$file_type,$brief_id);
            }
            if (!is_dir($brief_path."/".$lastid)) {
                mkdir($brief_path."/".$lastid, 0777, true);
            }
            // echo $lastid;exit();
            $wc_brief_sql_ver = "SELECT version_num,img_status FROM `wc_image_upload` where brief_id='$brief_id' and parent_img='$lastid' ORDER BY `version_num` DESC";
            $wc_brief_query_ver = $this->db->query($wc_brief_sql_ver);
            $wc_brief_result_ver=$wc_brief_query_ver->result_array();
            $num=sizeof($wc_brief_result_ver);
            if($num>0){
                $max_ver=$wc_brief_result_ver[0]['version_num'];
            }else{
                $max_ver=1;
            }

            //$max_ver=$max_ver+1;

            if (!is_dir($brief_path."/".$lastid."/Revision".$max_ver."")) {
                mkdir($brief_path."/".$lastid."/Revision".$max_ver."", 0777, true);
            }     
            $dir =$brief_path."/".$lastid."/Revision".$max_ver."/";

            if($_FILES["image"]["type"]=='image/jpeg'|| $_FILES["image"]["type"]=='image/jpg' || $_FILES["image"]["type"]=='video/webm' || $_FILES["image"]["type"]=='video/mp4' || $_FILES["image"]["type"]=='video/3gpp' || $_FILES["image"]["type"]=='video/x-sgi-movie' || $_FILES["image"]["type"]=='video/x-msvideo' || $_FILES["image"]["type"]=='video/mpeg' || $_FILES["image"]["type"]=='video/x-ms-wmv' ||$_FILES["image"]["type"]=='image/gif' || $_FILES["image"]["type"]=='image/png' || $_FILES["image"]["type"]=='image/bmp'){
                move_uploaded_file($_FILES["image"]["tmp_name"], $dir. $file);
            }
            if($_FILES["image"]["type"]=='image/gif' || $_FILES["image"]["type"]=='image/png' || $_FILES["image"]["type"]=='image/bmp' || $_FILES["image"]["type"]=='image/jpg' || $_FILES["image"]["type"]=='image/jpeg'){
                //$file_image=$timestamp.pathinfo($_FILES["image"]["name"], PATHINFO_FILENAME);
                $file_image=pathinfo($_FILES["image"]["name"], PATHINFO_FILENAME);

                $new_thumb = $brief_path."/".$lastid."/Revision".$max_ver."/thumb_".$file;
                $file_pat = $brief_path."/".$lastid."/Revision".$max_ver."/".$file;
                $this->convertToThumb($new_thumb,$brief_id,$file_pat);
                //echo ltrim($file);

                // $new_thumb = $brief_path."/".$lastid."/Revision".$max_ver."/thumb_".$tm.".jpg";
                // copy($file_pat,$new_thumb);

                
            }else{

                $file_image=pathinfo($_FILES["image"]["name"], PATHINFO_FILENAME);
                $new_thumb = $brief_path."/".$lastid."/Revision".$max_ver."/"."thumb_".$file;
                $file_pat = "./assets/img/video_thumbnail.jpg";
                $this->convertToThumb($new_thumb,$brief_id,$file_pat);


                $file_pat = "./assets/img/video_thumbnail.jpg";
                // $new_thumb = $brief_path."/".$lastid."/Revision".$max_ver."/thumb_".$tm.".jpg";
                // copy($file_pat,$new_thumb);



            }

            $update_sql = "UPDATE `wc_brief` SET `status`='6' WHERE brief_id='$brief_id' ";
            $this->db->query($update_sql);

            echo "success";

        }else{
            $data['user_id'] = '';
            $data['user_name'] = '';
            $user_id='';
            echo "sessionout";
        }
    }

function convertToThumb($targetFile, $brief_id, $originalFile) {

  $newHeight = 400;

  $info = getimagesize($originalFile);
  $mime = $info['mime'];

  switch ($mime) {
    case 'image/jpeg':
    $image_create_func = 'imagecreatefromjpeg';
    $image_save_func = 'imagejpeg';
    $new_image_ext = 'jpg';
    break;

    case 'image/png':
    $image_create_func = 'imagecreatefrompng';
    $image_save_func = 'imagepng';
    $new_image_ext = 'png';
    break;

    // case 'image/gif':
    // $image_create_func = 'imagecreatefromgif';
    // $image_save_func = 'imagegif';
    // $new_image_ext = 'gif';
    // break;

    default: 
    throw new Exception('Unknown image type.');
  }

  $img = $image_create_func($originalFile);
  list($width, $height) = getimagesize($originalFile);

  $newWidth = ($width / $height) * $newHeight;
  $tmp = imagecreatetruecolor($newWidth, $newHeight);
  imagecopyresampled($tmp, $img, 0, 0, 0, 0, $newWidth, $newHeight, $width, $height);

  if (file_exists($targetFile)) {
    unlink($targetFile);
  }

  $image_save_func($tmp, "$targetFile");
}







public function getAllUser() {



    $status_query= $this->db->query("SELECT * FROM `wc_users` where 1=1 ");
                $status_result= $status_query->result_array();
                //print_r($status_result);
                $row_cnt=$status_query->num_rows();
                if($row_cnt>=1){
                    foreach ($status_result as $i => $row) {
                        $user_id = $row['user_id'];

                        if($row["user_type_id"]=="4")
                        {
                        $role="admin";
                        }
                        else
                        {
                        $role="user";
                        }


                        $userresult[$i]['name']=$row['user_name'];
                        $userresult[$i]['mail']=$row['user_email'];
                        $userresult[$i]['registrationDate']='';
                        $userresult[$i]['passwd']=sha1($row['user_password']);
                        $userresult[$i]['role']=$role;
                        $userresult[$i]['active']="1";
                        $userresult[$i]['lastLogin']="";
                        $userresult[$i]['color']="597081";
   
                        
                    }
                }





$returnArr["user-increment"] = $row_cnt;

$returnArr["user"] = $userresult;


/*  echo "<pre>";
print_r($returnArr);
exit;*/


header('Content-type: application/json; charset=UTF-8');
echo json_encode($returnArr, JSON_PRETTY_PRINT | JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES);


}








public function getAllUserById($user_id) {



    $status_query= $this->db->query("SELECT * FROM `wc_users` where user_id='$user_id' ");
                $status_result= $status_query->result_array();
                //print_r($status_result);
                $row_cnt=$status_query->num_rows();
                if($row_cnt>=1){
                    foreach ($status_result as $i => $row) {
                        $user_id = $row['user_id'];

                        if($row["user_type_id"]=="4")
                        {
                        $role="admin";
                        }
                        else
                        {
                        $role="user";
                        }


                        $userresult[$i]['name']=$row['user_name'];
                        $userresult[$i]['mail']=$row['user_email'];
                        $userresult[$i]['registrationDate']='';
                        $userresult[$i]['passwd']=sha1($row['user_password']);
                        $userresult[$i]['role']=$role;
                        $userresult[$i]['active']="1";
                        $userresult[$i]['lastLogin']="";
                        $userresult[$i]['color']="597081";
   
                        
                    }
                }




return $userresult;

}




public function getAllVideo($brief_id) {


    $checkquerys = $this->db->query("SELECT * FROM `wc_brief` WHERE `brief_id` ='$brief_id'")->result_array();
    $brand_id=$checkquerys[0]['brand_id'];
    $brief_title=$checkquerys[0]['brief_title'];
    $brief_name=preg_replace('/[^a-zA-Z0-9_.]/', '_',$brief_title);
    $checkquerys = $this->db->query("select wc_brands.*,wc_clients.client_name from wc_brands inner join wc_clients on wc_brands.client_id=wc_clients.client_id where  wc_brands.brand_id='$brand_id'")->result_array();
    $brand_id=$checkquerys[0]['brand_id'];
    $brand_name=preg_replace('/[^a-zA-Z0-9_.]/', '_',$checkquerys[0]['brand_name']);
    $client_id=$checkquerys[0]['client_id'];
    $client_name=preg_replace('/[^a-zA-Z0-9_.]/', '_',$checkquerys[0]['client_name']);

    $dst = "upload/".$client_id."-".$client_name."/".$brand_id."-".$brand_name."/".$brief_id."-".$brief_name;  
    $client_folder=$client_id."-".$client_name;
    $brand_folder=$brand_id."-".$brand_name;
    $brief_folder=$brief_id."-".$brief_name;

    $brief_path=base_url()."upload/".$client_folder."/".$brand_folder."/".$brief_folder;



    /*$status_query= $this->db->query("SELECT * FROM `wc_image_upload` where brief_id='$brief_id' and file_type='1'   ");*/
    $status_query= $this->db->query("SELECT * FROM `wc_image_upload` where brief_id='$brief_id'    ");
                $status_result= $status_query->result_array();
                //print_r($status_result);
                $row_cnt=$status_query->num_rows();
                if($row_cnt>=1){
                    foreach ($status_result as $i => $row) {
                    $image_id = $row['image_id'];
                    $version_num = $row['version_num'];
                    $parent_img = $row['parent_img'];




                    $userresult[$i]['name']=$row['img_title'];
                    $userresult[$i]['creator']="admin";
                    $userresult[$i]['creatorId']=$row['added_by'];
                    $userresult[$i]['created']=$row['added_on'];
                    $userresult[$i]['description']='';
                    $userresult[$i]['src']=$brief_path."/".$parent_img."/Revision".$version_num."/".$row['image_path'];
                    $userresult[$i]['attributes']=array();
                    $userresult[$i]['type']="video";
                    $userresult[$i]['thumb']=$brief_path."/".$parent_img."/Revision".$version_num."/thumb_".$row['image_path'];

                   
   
                        
                    }
                }




$returnArr["resources-increment"] = $row_cnt;

$returnArr["resources"] = $userresult;


/*  echo "<pre>";
print_r($returnArr);
exit;*/


header('Content-type: application/json; charset=UTF-8');
echo json_encode($returnArr, JSON_PRETTY_PRINT | JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES);

}




public function getAllVideoCount($brief_id) {


    $checkquerys = $this->db->query("SELECT * FROM `wc_brief` WHERE `brief_id` ='$brief_id'")->result_array();
    $brand_id=$checkquerys[0]['brand_id'];
    $brief_title=$checkquerys[0]['brief_title'];
    $brief_name=preg_replace('/[^a-zA-Z0-9_.]/', '_',$brief_title);
    $checkquerys = $this->db->query("select wc_brands.*,wc_clients.client_name from wc_brands inner join wc_clients on wc_brands.client_id=wc_clients.client_id where  wc_brands.brand_id='$brand_id'")->result_array();
    $brand_id=$checkquerys[0]['brand_id'];
    $brand_name=preg_replace('/[^a-zA-Z0-9_.]/', '_',$checkquerys[0]['brand_name']);
    $client_id=$checkquerys[0]['client_id'];
    $client_name=preg_replace('/[^a-zA-Z0-9_.]/', '_',$checkquerys[0]['client_name']);

    $dst = "upload/".$client_id."-".$client_name."/".$brand_id."-".$brand_name."/".$brief_id."-".$brief_name;  
    $client_folder=$client_id."-".$client_name;
    $brand_folder=$brand_id."-".$brand_name;
    $brief_folder=$brief_id."-".$brief_name;

    $brief_path=base_url()."upload/".$client_folder."/".$brand_folder."/".$brief_folder;



    /*$status_query= $this->db->query("SELECT * FROM `wc_image_upload` where brief_id='$brief_id' and file_type='1'   ");*/
    $status_query= $this->db->query("SELECT * FROM `wc_image_upload` where brief_id='$brief_id'    ");
                $status_result= $status_query->result_array();
                //print_r($status_result);
                $row_cnt=$status_query->num_rows();
                if($row_cnt>=1){
                    $i=1;
                    foreach ($status_result as $k => $row) {
                        $image_id = $row['image_id'];


                        $userresult[$image_id]="./".$image_id;

                        if($i==$row_cnt)
                        {
                        $image_last_id = $row['image_id'];
                        }
                        $i++;
                        
                    }
                }




$returnArr["hypervideo-increment"] = $image_last_id;

$returnArr["hypervideos"] = $userresult;


/*  echo "<pre>";
print_r($returnArr);
exit;*/


header('Content-type: application/json; charset=UTF-8');
echo json_encode($returnArr, JSON_PRETTY_PRINT | JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES);

}







public function getAllVideoDetail($brief_id,$image_id) {



        $checkquerys = $this->db->query("SELECT * FROM `wc_brief` WHERE `brief_id` ='$brief_id'")->result_array();
    $brand_id=$checkquerys[0]['brand_id'];
    $brief_title=$checkquerys[0]['brief_title'];
    $brief_name=preg_replace('/[^a-zA-Z0-9_.]/', '_',$brief_title);
    $checkquerys = $this->db->query("select wc_brands.*,wc_clients.client_name from wc_brands inner join wc_clients on wc_brands.client_id=wc_clients.client_id where  wc_brands.brand_id='$brand_id'")->result_array();
    $brand_id=$checkquerys[0]['brand_id'];
    $brand_name=preg_replace('/[^a-zA-Z0-9_.]/', '_',$checkquerys[0]['brand_name']);
    $client_id=$checkquerys[0]['client_id'];
    $client_name=preg_replace('/[^a-zA-Z0-9_.]/', '_',$checkquerys[0]['client_name']);

    $dst = "upload/".$client_id."-".$client_name."/".$brand_id."-".$brand_name."/".$brief_id."-".$brief_name;  
    $client_folder=$client_id."-".$client_name;
    $brand_folder=$brand_id."-".$brand_name;
    $brief_folder=$brief_id."-".$brief_name;

    $brief_path=base_url()."upload/".$client_folder."/".$brand_folder."/".$brief_folder;


    $checkquerys = $this->db->query("SELECT * FROM `wc_image_upload` where image_id='$image_id' and `brief_id` ='$brief_id'")->result_array();

    $row=$checkquerys[0];


    $image_id = $row['image_id'];
    $version_num = $row['version_num'];
    $parent_img = $row['parent_img'];




    $returnArr["meta"]["name"] = $row['img_title'];
    $returnArr["meta"]["description"] = $row['img_title'];
    $returnArr["meta"]["thumb"] = $brief_path."/".$image_id."/Revision".$version_num."/thumb_".$row['image_path'];
    $returnArr["meta"]["creator"] = "admin";
    $returnArr["meta"]["creatorId"] = $row['added_by'];
    $returnArr["meta"]["created"] = $row['added_on'];
    $returnArr["meta"]["lastchanged"] = $row['image_id'];




    $returnArr["config"]["slidingMode"] = "adjust";
    $returnArr["config"]["slidingTrigger"] = "key";
    $returnArr["config"]["autohideControls"] = false;
    $returnArr["config"]["captionsVisible"] = false;
    $returnArr["config"]["clipTimeVisible"] = false;
    $returnArr["config"]["hidden"] = $row['image_id'];
    $returnArr["config"]["layoutArea"]["areaTop"] = array();
    $returnArr["config"]["layoutArea"]["areaBottom"] = array();
    $returnArr["config"]["layoutArea"]["areaLeft"] = array();
    $returnArr["config"]["layoutArea"]["areaRight"] = array();



    $returnArr["clips"]["resourceId"] = "adjust";
    $returnArr["clips"]["src"] = "key";
    $returnArr["clips"]["duration"] = 0;
    $returnArr["clips"]["start"] = 0;
    $returnArr["clips"]["end"] = 0;
    $returnArr["clips"]["in"] = 0;
    $returnArr["clips"]["out"] = 0;

    $returnArr["clips"]["resourceId"] = $row['image_id'];
    $returnArr["clips"]["src"] = $brief_path."/".$parent_img."/Revision".$version_num."/".$row['image_path'];
    $returnArr["clips"]["duration"] = 0;
    $returnArr["clips"]["start"] = 0;
    $returnArr["clips"]["end"] = 0;
    $returnArr["clips"]["in"] = 0;
    $returnArr["clips"]["out"] = 0;


    $returnArr["globalEvents"]["onReady"] = '';
    $returnArr["globalEvents"]["onPlay"] = '';
    $returnArr["globalEvents"]["onPause"] = '';
    $returnArr["globalEvents"]["onEnded"] = '';

    $returnArr["customCSS"]="";

    //$returnArr["contents"]=array();















    $contentsArr1['@context'] = array( 
    "http://www.w3.org/ns/anno.jsonld", 
     array( 
        "frametrail" => "http://frametrail.org/ns/"
    ) 
    ); 

    $contentsArr1['creator'] = array( 
    "nickname" => "admin",  
    "type" => "Person",
    "id" => "1", 

    ); 
    $contentsArr1["created"]="Wed Apr 15 2020 12:35:39 GMT+0530 (India Standard Time)";
    $contentsArr1["type"]="Annotation";
    $contentsArr1["frametrail:type"]="Overlay";
    $contentsArr1["frametrail:tags"]=array();




    $contentsArr1['target'] = array( 
    "type" => "Video",  
    "source" => "1_1586933555_poulami.mp4",
    "selector" => array( 
        "conformsTo" => "http://www.w3.org/TR/media-frags/",  
        "type" => "FragmentSelector", 
        "value" => "t=2.512510402699279,3.6908441780731405&xywh=percent:31.769758371856916,52.41426465798388,30,30"
    ) 

    ); 


    $contentsArr1['body'] = array( 
    "type" => "TextualBody", 
    "frametrail:type" => "text", 
    "format" => "text/html", 
    "value" => "", 
    "frametrail:name" => "Custom Text/HTML",  


    ); 
    $contentsArr1['frametrail:events'] = array();

    $contentsArr1['frametrail:attributes'] = array( 
    "text" => '&lt;p&gt;&lt;b&gt;&lt;span style=\"color: rgb(255, 0, 150); font-size: 50px;\"&gt;aaaa&lt;/span&gt;&lt;/b&gt;&lt;/p&gt;', 



    ); 

    /////////////////////////////////////////////////////////////////////
    $contentsArr2['@context'] = array( 
    "http://www.w3.org/ns/anno.jsonld", 
     array( 
        "frametrail" => "http://frametrail.org/ns/"
    ) 
    ); 

    $contentsArr2['creator'] = array( 
    "nickname" => "admin",  
    "type" => "Person",
    "id" => "1", 

    ); 
    $contentsArr2["created"]="Wed Apr 15 2020 12:35:39 GMT+0530 (India Standard Time)";
    $contentsArr2["type"]="Annotation";
    $contentsArr2["frametrail:type"]="Overlay";
    $contentsArr2["frametrail:tags"]=array();




    $contentsArr2['target'] = array( 
    "type" => "Video",  
    "source" => "1_1586933555_poulami.mp4",
    "selector" => array( 
        "conformsTo" => "http://www.w3.org/TR/media-frags/",  
        "type" => "FragmentSelector", 
        "value" => "t=4.595946836014365,5.797758282272914&xywh=percent:49.5961375680302,60.32147285848638,30.2,30"
    ) 

    ); 


    $contentsArr2['body'] = array( 
    "type" => "TextualBody", 
    "frametrail:type" => "text", 
    "format" => "text/html", 
    "value" => "", 
    "frametrail:name" => "Custom Text/HTML",  


    ); 
    $contentsArr2['frametrail:events'] = array();

    $contentsArr2['frametrail:attributes'] = array( 
    "text" => '&lt;p&gt;&lt;b&gt;&lt;span style=\"color: rgb(39, 188, 127);\"&gt;bbbbbbbb&lt;/span&gt;&lt;/b&gt;&lt;b&gt;&lt;/b&gt;&lt;/p&gt;', 



    ); 


/*    $returnArr["contents"] = array( 


    $contentsArr1,
    $contentsArr2 
     
    ); 
*/

    
//echo "SELECT * FROM `wc_image_annotation` where image_id='$image_id'  and file_type='1'   ";

        $status_query= $this->db->query("SELECT * FROM `wc_image_annotation` where image_id='$image_id'  and file_type='1'   ");
        $status_result= $status_query->result_array();
        //print_r($status_result);
        $row_cnt=$status_query->num_rows();
        if($row_cnt>=1){
        foreach ($status_result as $i => $row) {
            $annotation_id = $row['id'];
            $image_id = $row['image_id'];
           


            $revision_id = $row['revision_id'];
            $target_selector = $row['target_selector'];
            $frametrail_attributes = $row['frametrail_attributes'];


            $returnArr["contents"][$i]['@context'] = array( 
            "http://www.w3.org/ns/anno.jsonld", 
            array( 
            "frametrail" => "http://frametrail.org/ns/"
            ) 
            ); 

            $returnArr["contents"][$i]['information'] = array( 
            "brief_id" => $brief_id,  
            "image_id" => $image_id,
            "annotation_id" => $annotation_id, 

            ); 

            $returnArr["contents"][$i]['creator'] = array( 
            "nickname" => "admin",  
            "type" => "Person",
            "id" => "1", 

            ); 
            $returnArr["contents"][$i]["created"]="Wed Apr 15 2020 12:35:39 GMT+0530 (India Standard Time)";
            $returnArr["contents"][$i]["type"]="Annotation";
            $returnArr["contents"][$i]["frametrail:type"]="Overlay";
            $returnArr["contents"][$i]["frametrail:tags"]=array();




            $returnArr["contents"][$i]['target'] = array( 
            "type" => "Video",  
            "source" => "1_1586933555_poulami.mp4",
            "selector" => array( 
            "conformsTo" => "http://www.w3.org/TR/media-frags/",  
            "type" => "FragmentSelector", 
            "value" => $target_selector
            ) 

            ); 


            $returnArr["contents"][$i]['body'] = array( 
            "type" => "TextualBody", 
            "frametrail:type" => "text", 
            "format" => "text/html", 
            "value" => "", 
            "frametrail:name" => "Custom Text/HTML",  


            ); 
            $returnArr["contents"][$i]['frametrail:events'] = array();

            $returnArr["contents"][$i]['frametrail:attributes'] = array( 
            "text" => $frametrail_attributes, 



            ); 


        }
        }






    /*{
            
            
            "body": {
                "type": "TextualBody",
                "frametrail:type": "text",
                "format": "text/html",
                "value": "",
                "frametrail:name": "Custom Text/HTML"
            },
            "frametrail:events": {},
            "frametrail:attributes": {
                "text": "&lt;p&gt;&lt;b&gt;&lt;span style=\"color: rgb(255, 0, 150); font-size: 50px;\"&gt;aaaa&lt;/span&gt;&lt;/b&gt;&lt;/p&gt;"
            }
        },
    */


   /*   echo "<pre>";
    print_r($returnArr);
    exit;*/


    header('Content-type: application/json; charset=UTF-8');
    echo json_encode($returnArr, JSON_PRETTY_PRINT | JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES);



    

}











public function getAllVideoAnnotation($brief_id,$image_id) {



        $checkquerys = $this->db->query("SELECT * FROM `wc_brief` WHERE `brief_id` ='$brief_id'")->result_array();
    $brand_id=$checkquerys[0]['brand_id'];
    $brief_title=$checkquerys[0]['brief_title'];
    $brief_name=preg_replace('/[^a-zA-Z0-9_.]/', '_',$brief_title);
    $checkquerys = $this->db->query("select wc_brands.*,wc_clients.client_name from wc_brands inner join wc_clients on wc_brands.client_id=wc_clients.client_id where  wc_brands.brand_id='$brand_id'")->result_array();
    $brand_id=$checkquerys[0]['brand_id'];
    $brand_name=preg_replace('/[^a-zA-Z0-9_.]/', '_',$checkquerys[0]['brand_name']);
    $client_id=$checkquerys[0]['client_id'];
    $client_name=preg_replace('/[^a-zA-Z0-9_.]/', '_',$checkquerys[0]['client_name']);

    $dst = "upload/".$client_id."-".$client_name."/".$brand_id."-".$brand_name."/".$brief_id."-".$brief_name;  
    $client_folder=$client_id."-".$client_name;
    $brand_folder=$brand_id."-".$brand_name;
    $brief_folder=$brief_id."-".$brief_name;

    $brief_path=base_url()."upload/".$client_folder."/".$brand_folder."/".$brief_folder;


    $checkquerys = $this->db->query("SELECT * FROM `wc_image_upload` where image_id='$image_id' and `brief_id` ='$brief_id'")->result_array();

    $row=$checkquerys[0];


    $image_id = $row['image_id'];

$returnArr["mainAnnotation"]="1";

$returnArr["annotationfiles"]["1"]["name"]="main";
$returnArr["annotationfiles"]["1"]["description"]="";
$returnArr["annotationfiles"]["1"]["created"]=$row['added_on'];
$returnArr["annotationfiles"]["1"]["lastchanged"]=$row['added_on'];
$returnArr["annotationfiles"]["1"]["hidden"]="";
$returnArr["annotationfiles"]["1"]["owner"]="admin";
$returnArr["annotationfiles"]["1"]["ownerId"]=1;



/*  echo "<pre>";
print_r($returnArr);
exit;*/


header('Content-type: application/json; charset=UTF-8');
echo json_encode($returnArr, JSON_PRETTY_PRINT | JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES);



    

}





public function getAllVideoAnnotationId($brief_id,$image_id) {



        $checkquerys = $this->db->query("SELECT * FROM `wc_brief` WHERE `brief_id` ='$brief_id'")->result_array();
    $brand_id=$checkquerys[0]['brand_id'];
    $brief_title=$checkquerys[0]['brief_title'];
    $brief_name=preg_replace('/[^a-zA-Z0-9_.]/', '_',$brief_title);
    $checkquerys = $this->db->query("select wc_brands.*,wc_clients.client_name from wc_brands inner join wc_clients on wc_brands.client_id=wc_clients.client_id where  wc_brands.brand_id='$brand_id'")->result_array();
    $brand_id=$checkquerys[0]['brand_id'];
    $brand_name=preg_replace('/[^a-zA-Z0-9_.]/', '_',$checkquerys[0]['brand_name']);
    $client_id=$checkquerys[0]['client_id'];
    $client_name=preg_replace('/[^a-zA-Z0-9_.]/', '_',$checkquerys[0]['client_name']);

    $dst = "upload/".$client_id."-".$client_name."/".$brand_id."-".$brand_name."/".$brief_id."-".$brief_name;  
    $client_folder=$client_id."-".$client_name;
    $brand_folder=$brand_id."-".$brand_name;
    $brief_folder=$brief_id."-".$brief_name;

    $brief_path=base_url()."upload/".$client_folder."/".$brand_folder."/".$brief_folder;


    $checkquerys = $this->db->query("SELECT * FROM `wc_image_upload` where image_id='$image_id' and `brief_id` ='$brief_id'")->result_array();

    $row=$checkquerys[0];

$image_id = $row['image_id'];

$returnArr=[];

/*  echo "<pre>";
print_r($returnArr);
exit;*/


header('Content-type: application/json; charset=UTF-8');
echo json_encode($returnArr, JSON_PRETTY_PRINT | JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES);


    

}




public function insertAllVideoAnnotationId() {


/*$session_data = $this->session->userdata('front_logged_in');
$user_id = $session_data['user_id'];
$user_type_id=$session_data['user_type_id'];*/




$user_id=$this->input->post('user_id');
 $image_id=$this->input->post('image_id');
$arr_count=$this->input->post('arr_count');
//$target_selector_arr=$this->input->post('target_selector');
/*$frametrail_attributes_arr=$this->input->post('frametrail_attributes');*/


//$target_selector_arr=$_POST['target_selector'];
//$frametrail_attributes_arr=$_POST['frametrail_attributes'];


$target_selector_arr=$this->input->post('target_selector');
$frametrail_attributes_arr=$this->input->post('frametrail_attributes');

//echo "SELECT * FROM `wc_image_upload` WHERE `image_id` ='$image_id'";exit;

$checkquerys = $this->db->query("SELECT * FROM `wc_image_upload` WHERE `image_id` ='$image_id'")->result_array();
$brand_id=$checkquerys[0]['brief_id'];
$img_title=$checkquerys[0]['img_title'];
$image_path=$checkquerys[0]['image_path'];
$file_type=$checkquerys[0]['file_type'];
$brand_id=$checkquerys[0]['brand_id'];
$brief_id=$checkquerys[0]['brief_id'];
$revision_id=$checkquerys[0]['revision_id'];
$parent_img=$checkquerys[0]['parent_img'];
$version_num=$checkquerys[0]['version_num'];
$added_on=$checkquerys[0]['added_on'];
$added_by=$checkquerys[0]['added_by'];



$sql = "DELETE FROM wc_image_annotation WHERE image_id = '".$image_id."'  and file_type='1' ";
$qry =$this->db->query($sql);

for($i=0;$i<$arr_count;$i++)
{

    $target_selector=base64_decode($target_selector_arr[$i]);
    $frametrail_attributes=base64_decode($frametrail_attributes_arr[$i]);




    $file_type="1";
    //$descr_text=(str_replace(array('&lt;','&gt;','&lt','&gt'),array('<','>','<','>'),$frametrail_attributes));
     $descr_text=(str_replace(array('&lt;','&gt;','&lt','&gt','&amp;nbsp;'),array('<','>','<','>',' '),$frametrail_attributes));


    $descr=strip_tags($descr_text);
    $position_left="";
    $position_top="";
    $create_date=date('Y/m/d');

    if($descr!='')
    {
    $sql = "INSERT INTO wc_image_annotation(`user_id`,`image_id`,`revision_id`,`file_type`,`descr`,  `position_left`, `position_top`,`target_selector`,`frametrail_attributes`, `create_date`)VALUES('$user_id','$image_id','$version_num','$file_type','$descr','$position_left','$position_top','$target_selector','$frametrail_attributes','$create_date');";

    // print_r($sql);
    $qry =$this->db->query($sql);

    }





}


return $sql;
        

    

}

public function deleteAnnotation()
{


$id=$this->input->post('annotation_id');  

$sql = "DELETE FROM wc_image_annotation WHERE id = '".$id."'  and file_type='1' ";
$qry =$this->db->query($sql);
echo "success";
}


public function get_status()
    {

        $session_data = $this->session->userdata('front_logged_in');
        $data['user_id'] = $session_data['user_id'];
        $data['user_name'] = $session_data['user_name'];
        $user_id = $session_data['user_id'];
        $user_type_id=$session_data['user_type_id'];

        
        $brief_id=$this->input->post('brief_id');
        $image_id=$this->input->post('image_id');


        $status_query1= $this->db->query("SELECT * FROM wc_image_upload WHERE brief_id='$brief_id' and image_id='$image_id'");
        $status_result1= $status_query1->result_array();
        $img_status=$status_result1[0]['img_status'];
        $file_type=$status_result1[0]['file_type'];

        echo $img_status."@@@".$file_type;

  }



public function get_file_type()
    {

        $session_data = $this->session->userdata('front_logged_in');
        $data['user_id'] = $session_data['user_id'];
        $data['user_name'] = $session_data['user_name'];
        $user_id = $session_data['user_id'];
        $user_type_id=$session_data['user_type_id'];

        
        $brief_id=$this->input->post('brief_id');
        $image_id=$this->input->post('image_id');


        $status_query1= $this->db->query("SELECT * FROM wc_image_upload WHERE brief_id='$brief_id' and image_id='$image_id'");
        $status_result1= $status_query1->result_array();
        $file_type=$status_result1[0]['file_type'];

        echo $file_type;

  }
















}

?>