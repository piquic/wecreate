<?php 
class Proofing extends CI_Controller{
    
    function __construct(){
        parent::__construct();
        $this->load->library(['session']); 
        $this->load->helper(['url','file','form']); 
        $this->load->model('Proofing_model'); //load model upload 
        $this->load->model('home_model'); //load model upload 
        $this->load->library('upload'); //load library upload 
        $this->load->library('email');
        $this->load->model('Feedback_model'); //load model upload
    }

    public function index($brief_id){

        if ($this->session->userdata('front_logged_in')) {
            $session_data = $this->session->userdata('front_logged_in');
            $data['user_id'] = $session_data['user_id'];
            $data['user_name'] = $session_data['user_name'];
            $user_id = $session_data['user_id'];
            $user_type_id=$session_data['user_type_id'];

            $data['brief_id'] = $brief_id;
            $data['title'] = "Proofing";
            $checkquerys = $this->db->query("SELECT * FROM `wc_brief` WHERE `brief_id` ='$brief_id' and status='6'");
            $count=$checkquerys->num_rows();
            if($count>=1){
                $this->load->view('front/proofing',$data);   
            }else{
                redirect('login', 'refresh');
            }
        }
        else
        {
            $data['user_id'] = '';
            $data['user_name'] = '';
            $user_id='';
            redirect('login', 'refresh');
        }
    }

    public function update_status()
    {

        if ($this->session->userdata('front_logged_in')) {
            $session_data = $this->session->userdata('front_logged_in');
            $data['user_id'] = $session_data['user_id'];
            $data['user_name'] = $session_data['user_name'];
            $user_id = $session_data['user_id'];
            $user_type_id=$session_data['user_type_id'];

            $data['title'] = "Proofing";
            $image_id=$_POST['image_id'];
            $brief_id=$_POST['brief_id'];
            $img_status=$_POST['img_status'];

            // $array = array('image_id' => $image_id, 'brief_id' => $brief_id);
            // $img_status1= array('img_status' => $img_status);
            // $this->db->where($array);
            // $report = $this->db->update('wc_image_upload', $img_status1);
            // $sql = $this->db->last_query();

            if($img_status=="1"){
                $status_query1= $this->db->query("SELECT * FROM wc_image_upload WHERE brief_id='$brief_id' and image_id='$image_id'");
                $status_result1= $status_query1->result_array();
                $parent_img=$status_result1[0]['parent_img'];

                $array = array('image_id' => $image_id, 'brief_id' => $brief_id);
                $img_status1= array('img_status' => $img_status);
                $this->db->where($array);
                $report = $this->db->update('wc_image_upload', $img_status1);
                $sql = $this->db->last_query();

                

                $status_query= $this->db->query("SELECT * FROM wc_image_upload WHERE brief_id='$brief_id' and image_id IN (SELECT MAX(image_id) FROM wc_image_upload  GROUP BY parent_img )");
                $status_result= $status_query->result_array();
                //print_r($status_result);
                $status_count=$status_query->num_rows();
                $status_count1=0;
                if($status_count>="1"){
                    foreach ($status_result as $status_result_key => $status_result_value) {
                        $image_id1= $status_result_value['image_id'];
                        $status_query1= $this->db->query("SELECT * FROM `wc_image_upload` where brief_id='$brief_id' and image_id='$image_id1'");
                        $status_result1= $status_query1->result_array();
                        //print_r($status_result1);
                        $img_status1=$status_result1[0]['img_status'];
                        if($img_status1=="1"){
                            $status_count1++;  
                        }
                    }
                }
                if($status_count==$status_count1)
                {
                    $array = array('brief_id' => $brief_id);
                    $img_status1= array('status' => "5");
                    $this->db->where($array);
                    $report = $this->db->update('wc_brief', $img_status1);
                    $sql = $this->db->last_query();

                ///////////////////////////////////////////////////////////////////////////

             
                    
                 $checkquerys = $this->db->query("select * from wc_brief where  brief_id='$brief_id' ")->result_array();
                 $brand_id=$checkquerys[0]['brand_id'];
                 $added_by_user_id=$checkquerys[0]['added_by_user_id'];


                 $checkquerys = $this->db->query("select * from wc_users where  user_id='$added_by_user_id' ")->result_array();
                 $user_id=$checkquerys[0]['user_id'];
                 $user_email=$checkquerys[0]['user_email'];
                 $user_name=$checkquerys[0]['user_name'];




            /*********************************Send mail*************************************/

                $query=$this->db->query("select * from  wc_settings  WHERE setting_key='MAIL_SMTP_USER' ");
                $userDetails= $query->result_array();
                $MAIL_SMTP_USER=$userDetails[0]['setting_value'];

                $query=$this->db->query("select * from  wc_settings  WHERE setting_key='MAIL_SMTP_PASSWORD' ");
                $userDetails= $query->result_array();
                $MAIL_SMTP_PASSWORD=$userDetails[0]['setting_value'];

                $query=$this->db->query("select * from  wc_settings  WHERE setting_key='MAIL_BODY_FEEDBACK' ");
                $userDetails= $query->result_array();
                $MAIL_BODY=$userDetails[0]['setting_value'];


                $uname=$user_name;
                $uemail=$user_email;
                $subject='Piquic- Feedback mail';

                $unique_feedback_id=time()."-".$brief_id."-".$user_id;
                $feedback_link=base_url()."feedback-submit/".base64_encode($unique_feedback_id);

                    
                    //Load email library
                    $this->load->library('email');

                    $config = array();
                    //$config['protocol'] = 'smtp';
                    $config['protocol']     = 'mail';
                    $config['smtp_host'] = 'localhost';
                    $config['smtp_user'] = $MAIL_SMTP_USER;
                    $config['smtp_pass'] = $MAIL_SMTP_PASSWORD;
                    $config['smtp_port'] = 25;




                    $this->email->initialize($config);
                    $this->email->set_newline("\r\n");

                    
                    $from_email = "demo.intexom@gmail.com";
                    $to_email = $uemail;

                    $date_time=date("d/m/y");
                    $copy_right= date("Y");
                    //echo $message=$MAIL_BODY;
                    $find_arr = array("##BM_NAME##","##FEEDBACK_SUBMIT_LINK##","##COPY_RIGHT##");
                    $replace_arr = array($uname,$feedback_link,$copy_right);
                    $message=str_replace($find_arr,$replace_arr,$MAIL_BODY);

                    //echo $message;exit;



                    $this->email->from($from_email, 'Piquic');
                    $this->email->to($to_email);
                    $this->email->cc('poulami@mokshaproductions.in');
                    $this->email->bcc('pranav@mokshaproductions.in');
                    $this->email->bcc('raja.priya@mokshaproductions.in');
                    $this->email->subject($subject);
                    $this->email->message($message);
                    //Send mail
                    if($this->email->send())
                    {

                        //echo "mail sent";exit;
                        $this->session->set_flashdata("email_sent","Congragulation Email Send Successfully.");

                    }
                    
                    else
                    {
                        //echo $this->email->print_debugger();
                        //echo "mail not sent";exit;
                        $this->session->set_flashdata("email_sent","You have encountered an error");
                    }
                   
                    $report="0";
                //exit;

                } 

                echo $report;

            }
            else{

                $array = array('image_id' => $image_id, 'brief_id' => $brief_id);
                $img_status1= array('img_status' => $img_status);
                $this->db->where($array);
                $report = $this->db->update('wc_image_upload', $img_status1);
                $sql = $this->db->last_query();

                $wc_brief_sql_ver = "SELECT version_num FROM `wc_image_upload` where brief_id='$brief_id' and image_id='$image_id'";
                $wc_brief_query_ver = $this->db->query($wc_brief_sql_ver);
                $wc_brief_result_ver=$wc_brief_query_ver->result_array();
                $num=sizeof($wc_brief_result_ver);
                if($num>0){
                    $max_ver=$wc_brief_result_ver[0]['version_num'];
                }else{
                    $max_ver=1;
                }
                echo $max_ver;
            }
            
            // echo $report;
        }
        else
        {
            $data['user_id'] = '';
            $data['user_name'] = '';
            $user_id='';
            redirect('login', 'refresh');
        }

       
    }


    public function use_img()
    {

        if ($this->session->userdata('front_logged_in')) {
            $session_data = $this->session->userdata('front_logged_in');
            $data['user_id'] = $session_data['user_id'];
            $data['user_name'] = $session_data['user_name'];
            $user_id = $session_data['user_id'];
            $user_type_id=$session_data['user_type_id'];

           // $data['brief_id'] = $brief_id;
            $data['title'] = "Proofing";
            $brief_id=$_POST['brief_id'];
            
            $image_id=$_POST['image_id'];
            $img_status=$_POST['img_status'];

            //$checkquerys = $this->db->query("select * from wc_users where  user_id='$user_id' ")->result_array();
            // $checkquerys = $this->db->query("SELECT * FROM `wc_brief` WHERE `brief_id` ='$brief_id'")->result_array();
            // $brand_id=$checkquerys[0]['brand_id'];

            // $checkquerys = $this->db->query("select wc_brands.*,wc_clients.client_name from wc_brands inner join wc_clients on wc_brands.client_id=wc_clients.client_id where  wc_brands.brand_id='$brand_id' ")->result_array();
            // $brand_id=$checkquerys[0]['brand_id'];
            // $brand_name=str_replace(" ","_",$checkquerys[0]['brand_name']);
            // $client_id=$checkquerys[0]['client_id'];
            // $client_name=str_replace(" ","_",$checkquerys[0]['client_name']);
            $checkquerys = $this->db->query("select * from wc_brief where brief_id='$brief_id' ")->result_array();
            $added_by_user_id=$checkquerys[0]['added_by_user_id'];
            $brand_id=$checkquerys[0]['brand_id'];
            $brief_title=$checkquerys[0]['brief_title'];
            $brief_name=str_replace(" ","_",$brief_title);

            /*$checkquerys = $this->db->query("select wc_brands.*,wc_clients.client_name from wc_brands inner join wc_clients on wc_brands.client_id=wc_clients.client_id where wc_brands.brand_id='$brand_id' ")->result_array();*/

            $checkquerys = $this->db->query("select wc_brands.*,wc_clients.client_name from wc_users
            inner join wc_brands on wc_users.brand_id=wc_brands.brand_id
            inner join wc_clients on wc_brands.client_id=wc_clients.client_id
            where wc_users.user_id='$added_by_user_id' ")->result_array();

            $brand_id=$checkquerys[0]['brand_id'];
            $brand_name=str_replace(" ","_",$checkquerys[0]['brand_name']);
            $client_id=$checkquerys[0]['client_id'];
            $client_name=str_replace(" ","_",$checkquerys[0]['client_name']);

            $checkquerys1 = $this->db->query("select * from wc_image_upload where  image_id='$image_id' ")->result_array();
            $version_num=$checkquerys1[0]['version_num'];
            $parent_img=$checkquerys1[0]['parent_img'];
            if(!empty($_POST['image_path'])){
               $image_path=$_POST['image_path']; 
            }
            else{
                $image_path=$checkquerys1[0]['image_path'];
            }

            // $checkquerys2 = $this->db->query("select * from wc_revision where  revision_id='$revision_id' ")->result_array();
            // $revision_name=$checkquerys2[0]['revision_name'];

            $dst = "upload/".$client_id."-".$client_name."/".$brand_id."-".$brand_name."/".$brief_id."-".$brief_name."/".$parent_img."/Revision".$version_num;  
            $html="";

             $image_org="upload/".$client_id."-".$client_name."/".$brand_id."-".$brand_name."/".$brief_id."-".$brief_name."/".$parent_img."/Revision".$version_num."/".$image_path;


            $mime = mime_content_type($dst."/".$image_path);
            if(strstr($mime, "video/")){
            $div_con='<video id="video_'.$image_id.'" width="100%" height="100%" controls>
                       <source src="'.base_url().$image_org.'" type="video/mp4">Your browser does not support the video tag.
                            </video>';
            }else if(strstr($mime, "image/")){
            $div_con="<img class='border w-100 img_download' src='".base_url().$dst."/".$image_path."'>";
            }

            $select_value="";


            $html.="<div class='col-12 col-sm-12 col-md-12' >
                        <div class='p-4'>
                           

                            ".$div_con."
                            ";
            if($img_status=='1')
            {
            
                $html.="<button type='submit' class='btn btn-wc btn-block m-2 px-5'>&emsp;&emsp;Approved&nbsp;<i class='fas fa-check'></i>&emsp;&emsp;</button>";
            }else{
                $html.=" <div class='row' id='update_approve'><div class='col-12 col-sm-12 col-md-12'>
                    <div class='d-flex justify-content-center flex-wrap'>
                       ";
                        if($img_status=='0' && ($user_type_id=='1'||$user_type_id=='2'||$user_type_id=='4')){
                            $html.="<button type='submit' class='btn btn-outline-wc btn-lg m-2 px-5' onclick='updateApprove(".$image_id.",".$brief_id.")'>&emsp;&emsp;Approve&emsp;&emsp;</button>";
                            $html.="<button type='submit' class='btn btn-outline-wc btn-lg m-2 px-5' onclick='review(".$image_id.",".$brief_id.",\"".$image_path."\",\"".$select_value."\")'>&emsp;&emsp;Review&emsp;&emsp;</button>";
                        }
                       
                    // if($img_status=='4' && ($user_type_id=='1'||$user_type_id=='2'||$user_type_id=='3')){
                    //      $html.="<button type='submit' class='btn btn-outline-wc btn-lg m-2 px-5' onclick='updateApprove(".$image_id.",".$brief_id.")'>&emsp;&emsp;Approve&emsp;&emsp;</button>";
                    // }
                   
                    // if($img_status=='0' && ($user_type_id=='1'||$user_type_id=='2'||$user_type_id=='4')){
                    //      $html.="<button type='submit' class='btn btn-outline-wc btn-lg m-2 px-5' onclick='review(".$image_id.",".$brief_id.",\"".$image_path."\")'>&emsp;&emsp;Review&emsp;&emsp;</button>";
                    // }
                    $html.="
                    </div></div>
                </div>
                ";
            }
              $html.="
                    </div>
                            </div>";
            echo $html;
           
        }
        else
        {
            $data['user_id'] = '';
            $data['user_name'] = '';
            $user_id='';
            redirect('login', 'refresh');
        }
        
    }

    public function review()
    {
         if ($this->session->userdata('front_logged_in')) {
            $session_data = $this->session->userdata('front_logged_in');
            $data['user_id'] = $session_data['user_id'];
            $data['user_name'] = $session_data['user_name'];
            $user_id = $session_data['user_id'];
            $user_type_id=$session_data['user_type_id'];

            //$data['brief_id'] = $brief_id;
            $data['title'] = "Proofing";
            $brief_id=$_POST['brief_id'];
            $image_path=$_POST['image_path'];
            $image_id=$_POST['image_id'];

            $query= $this->db->query("SELECT * FROM wc_image_upload WHERE image_id='$image_id'");
            $rs= $query->result_array();
            //echo $rs[0]['img_status'];
             if(empty($_POST['image_path'])){
               $image_path=$rs[0]['image_path'];
            }
            else{
               $image_path=$_POST['image_path'];
            }
            if($rs[0]['img_status']==0)
            {
                $array = array('image_id' => $image_id, 'brief_id' => $brief_id);
                $img_status1= array('img_status' => '2');
                $this->db->where($array);
                $report = $this->db->update('wc_image_upload', $img_status1);
            }
            $wc_brief_sql = "SELECT * FROM `wc_brief` where brief_id='$brief_id'";
            $wc_brief_query = $this->db->query($wc_brief_sql);

            $wc_brief_result=$wc_brief_query->result_array();
            //print_r($wc_brief_result);
            // $wc_brief_qrydata = mysqli_fetch_array($wc_brief_query);
            $brief_title=$wc_brief_result[0]['brief_title'];

            $image_upload_query1= $this->db->query("SELECT * FROM wc_image_upload WHERE image_id='$image_id'");
            $image_upload_rs1= $image_upload_query1->result_array();
            $image_upload_rs1[0]['img_status'];
            if($image_upload_rs1[0]['img_status']==2 && ($user_type_id=='1'||$user_type_id=='2'||$user_type_id=='4')){ 
                $img_id= 'imgtag';
            } 
            else{
                $img_id= '';
            }
            

            //$checkquerys = $this->db->query("select * from wc_users where  user_id='$user_id' ")->result_array();
            //  $checkquerys = $this->db->query("SELECT * FROM `wc_brief` WHERE `brief_id` ='$brief_id'")->result_array();
            // $brand_id=$checkquerys[0]['brand_id'];

            // $checkquerys = $this->db->query("select wc_brands.*,wc_clients.client_name from wc_brands inner join wc_clients on wc_brands.client_id=wc_clients.client_id where  wc_brands.brand_id='$brand_id' ")->result_array();
            // $brand_id=$checkquerys[0]['brand_id'];
            // $brand_name=str_replace(" ","_",$checkquerys[0]['brand_name']);
            // $client_id=$checkquerys[0]['client_id'];
            // $client_name=str_replace(" ","_",$checkquerys[0]['client_name']);

            $checkquerys = $this->db->query("select * from wc_brief where brief_id='$brief_id' ")->result_array();
            $added_by_user_id=$checkquerys[0]['added_by_user_id'];
            $brand_id=$checkquerys[0]['brand_id'];
            $brief_title=$checkquerys[0]['brief_title'];
            $brief_name=str_replace(" ","_",$brief_title);

            /*$checkquerys = $this->db->query("select wc_brands.*,wc_clients.client_name from wc_brands inner join wc_clients on wc_brands.client_id=wc_clients.client_id where wc_brands.brand_id='$brand_id' ")->result_array();*/

            $checkquerys = $this->db->query("select wc_brands.*,wc_clients.client_name from wc_users
            inner join wc_brands on wc_users.brand_id=wc_brands.brand_id
            inner join wc_clients on wc_brands.client_id=wc_clients.client_id
            where wc_users.user_id='$added_by_user_id' ")->result_array();

            $brand_id=$checkquerys[0]['brand_id'];
            $brand_name=str_replace(" ","_",$checkquerys[0]['brand_name']);
            $client_id=$checkquerys[0]['client_id'];
            $client_name=str_replace(" ","_",$checkquerys[0]['client_name']);

            $checkquerys1 = $this->db->query("select * from wc_image_upload where  image_id='$image_id' ")->result_array();
            $parent_img=$checkquerys1[0]['parent_img'];
            $version_num=$checkquerys1[0]['version_num'];


            $dst = "upload/".$client_id."-".$client_name."/".$brand_id."-".$brand_name."/".$brief_id."-".$brief_name."/".$parent_img."/Revision".$version_num;  
            $html=""; 
        


            $image_org="upload/".$client_id."-".$client_name."/".$brand_id."-".$brand_name."/".$brief_id."-".$brief_name."/".$image_id."/Revision".$version_num."/".$image_path;
            $mime = mime_content_type($dst."/".$image_path);
            if(strstr($mime, "video/")){
            $div_con='<video id="video_'.$image_id.'" width="100%" height="100%" controls>
                       <source src="'.base_url().$image_org.'" type="video/mp4">Your browser does not support the video tag.
                            </video>';
            }
            else if(strstr($mime, "image/")){
            $div_con="<img class='border w-100 img_download' id='".$img_id."' src='".base_url().$dst."/".$image_path."'>";
            }
            else
            {
                $div_con="";
            }
            // $query= $this->db->query("SELECT * FROM wc_image_annotation WHERE image_id='$image_id' and img_an_status='0'");
            // $count=$query->num_rows();


            $html.="<div class='col-12 col-sm-12 col-md-9'  id='img_div1'>
                        <div class='p-4' >
                            
                           ".$div_con."
                             <div id='tagbox' class='tagbox'></div>
                            ";

                        if($image_upload_rs1[0]['img_status']==3)
                        {
                            $html.="<div class='bg-light w-100 text-center p-2 lead'>&emsp;&emsp;Under Revision&nbsp;<i class='fas fa-sync-alt'></i>&emsp;&emsp;</div>";
                        }
                        //  if($image_upload_rs1[0]['img_status']==4)
                        // {
                        //      $html.=" <div class='row' id='update_approve'><div class='col-12 col-sm-12 col-md-12'>
                        //         <div class='d-flex justify-content-center flex-wrap'>
                        //            ";
                        //         if($user_type_id=='1'||$user_type_id=='2'||$user_type_id=='3'){
                        //              $html.="<button type='submit' class='btn btn-outline-wc btn-lg m-2 px-5' onclick='updateApprove(".$image_id.",".$brief_id.")'>&emsp;&emsp;Approve&emsp;&emsp;</button>";
                        //         }
                               
                              
                        //         $html.="
                        //         </div></div>
                        //     </div>
                        //     </div>
                        //             </div>";
                        // }   
                          $html.=" <div class='row d-none' id='update_approve'>
                                        <div class='col-12 col-sm-12 col-md-12'>
                                        <div class='d-flex justify-content-center flex-wrap'>
                                           ";
                                            if($image_upload_rs1[0]['img_status']==2 && ($user_type_id=='1'||$user_type_id=='2'||$user_type_id=='4')){
                                                $html.="<button type='submit' class='btn btn-outline-wc btn-lg m-2 px-5' onclick='updateApprove(".$image_id.",".$brief_id.")'>&emsp;&emsp;Approve&emsp;&emsp;</button>";
                                               
                                            }
                                           
                                        // if($img_status=='4' && ($user_type_id=='1'||$user_type_id=='2'||$user_type_id=='3')){
                                        //      $html.="<button type='submit' class='btn btn-outline-wc btn-lg m-2 px-5' onclick='updateApprove(".$image_id.",".$brief_id.")'>&emsp;&emsp;Approve&emsp;&emsp;</button>";
                                        // }
                                       
                                        // if($img_status=='0' && ($user_type_id=='1'||$user_type_id=='2'||$user_type_id=='4')){
                                        //      $html.="<button type='submit' class='btn btn-outline-wc btn-lg m-2 px-5' onclick='review(".$image_id.",".$brief_id.",\"".$image_path."\")'>&emsp;&emsp;Review&emsp;&emsp;</button>";
                                        // }
                                        $html.="
                                        </div></div>
                                    </div>
                                    ";
                       $html.=  "
                           
                        </div>
                    </div>
                    <div class='col-12 col-sm-12 col-md-3' id='revision_list'>
                        <div class='py-4 p-1 w-100'>
                            <p class='lead' id='notes'></p>
                                <div class='list-id' id='list-id' > 
                                </div> 
                                <div id='sendRevision' class='upload-btn-wrapper'>";
                if($image_upload_rs1[0]['img_status']==2 && ($user_type_id=='1'||$user_type_id=='2'||$user_type_id=='4'))
                {
                    $html.="    <button type='submit' class='btn btn-outline-wc btn-block px-5 mb-2' onclick='sendRevision(\"".$image_id."\",\"".$brief_id."\")'>&emsp;&emsp;Send Revision&emsp;&emsp;</button>

                            <button type='submit' class='btn btn-outline-danger btn-block px-5' onclick='reviewCancel(\"".$image_id."\",\"".$brief_id."\")'>&emsp;&emsp;Cancel&emsp;&emsp;</button>";
                }
                else if($image_upload_rs1[0]['img_status']==3 && ($user_type_id=='1'||$user_type_id=='2'||$user_type_id=='3')){
                    $html.="<form action='' method='post' enctype='multipart/form-data' id='upldForm' name='upldForm".$image_id."'>
                                <input type='hidden' name='brief_id' id='brief_id' value='".$brief_id."'>
                                <input type='hidden' name='image_id' id='image_id' value='".$image_id."'>
                                <button type='submit' class='btn btn-outline-wc w-100 px-5 mb-2'>&emsp;&emsp;<i class='fas fa-upload'></i> Upload Revised File</button>
                                <input type='file' class='upload_revised_file' name='files'  id='files'/>
                            </form>";
                }
                // else{
                //    $html.="eroor";
                // }
                // else if($rs[0]['img_status']==3)
                // {
                //     $html.="<button type='submit' class='btn btn-outline-wc btn-block px-5 mb-2'>&emsp;&emsp;".$brief_title ."Sent&nbsp;<i class='fas fa-sync-alt'></i>&emsp;&emsp;</button>";
                // }
                            $html.="
                            </div>
                        </div>
                    </div>
                    ";

            echo $html;
           
        }
        else
        {
            $data['user_id'] = '';
            $data['user_name'] = '';
            $user_id='';
            redirect('login', 'refresh');
        }

        
    }
    public function annotation_list()
    {
         if ($this->session->userdata('front_logged_in')) {
            $session_data = $this->session->userdata('front_logged_in');
            $data['user_id'] = $session_data['user_id'];
            $data['user_name'] = $session_data['user_name'];
            $user_id = $session_data['user_id'];
            $user_type_id=$session_data['user_type_id'];

            //$data['brief_id'] = $brief_id;
            $data['title'] = "Proofing";
            $image_id=$_POST['image_id'];
           // print_r($_POST['select_value']);
            if(isset($_POST['select_value'])=="change")
            {
                $query= $this->db->query("SELECT * FROM wc_image_annotation WHERE image_id='$image_id' and (img_an_status='0' || img_an_status='1')");
                $rs= $query->result_array();
               /// print_r("SELECT * FROM wc_image_annotation WHERE image_id='$image_id' and (img_an_status='0' || img_an_status='1')");
            }
            else if(!isset($_POST['select_value'])){
                $query= $this->db->query("SELECT * FROM wc_image_annotation WHERE image_id='$image_id' and img_an_status='0'");
                $rs= $query->result_array();
               
            }
            // $query= $this->db->query("SELECT * FROM wc_image_annotation WHERE image_id='$image_id' and img_an_status='0'");
            // $rs= $query->result_array();
            //print_r($rs);


            $query1= $this->db->query("SELECT * FROM wc_image_upload WHERE image_id='$image_id'");
            $rs1= $query1->result_array();
            //echo $rs1[0]['img_status'];

            $count=$query->num_rows();
            $data['boxes'] = '';
            $data['lists'] = '';
            $data['notes'] = "Notes - Revision".$rs1[0]['version_num'] ;
            $data['send_revision']="";
            $i=1;
            if($count>0)
            {
                if ($rs){
                    foreach ($rs as $rs_key => $rs_value) {
                     //echo $rs_value['id'];
                   
                    // do{
                    //  echo $rs_value['id'];
                        $data['boxes'] .= '<div class="tagview" style="left:' . $rs_value['position_left'] . 'px;top:' . $rs_value['position_top'] . 'px;" >';
                        $data['boxes'] .= '<div class="square"></div>';
                        $data['boxes'] .= '<div class="person popup" style="left:' . $rs_value['position_left'] . 'px;top:' . $rs_value['position_top']  . 'px;" id="view_'.$rs_value['id'].'">' . $rs_value['descr'] . '</div>';
                        $data['boxes'] .= '</div>';

                        $data['lists'] .= '<p id="'.$rs_value['id'].'">
                        
                       
                        <a class="name" id="span_name' . $rs_value['id'] . '">' . $rs_value['descr'] . '</a>
                        <textarea class="form-control input-sm d-none" type="text" id="name'.$rs_value['id'].'" name="name" >'. $rs_value['descr'] . '</textarea>
                        ';
                        $data['lists'] .='';
                        if($rs1[0]['img_status']=='2' && $rs_value['img_an_status']=='0' && ($user_type_id=='1'||$user_type_id=='2'||$user_type_id=='4')){
                        $data['lists'] .='
                        <button type="button" id="editBtn'.$rs_value['id'].'" class="btn btn-sm btn-default" onclick="edit_button('.$rs_value['id'].')" style="background: none;     font-size: 20px;">
                        <i class="far fa-edit text-wc"></i>
                        </button> 

                        <button type="button" id="updateBtn'.$rs_value['id'].'" class="btn btn-sm btn-default d-none" onclick="update_button('.$rs_value['image_id'].','.$rs_value['id'].','. $rs_value['position_left'].','. $rs_value['position_top'].')" style="background: none;     font-size: 20px;"><i class="far fa-check-circle text-success"></i></button> 


                        <button type="button" id="cancleBtn'.$rs_value['id'].'" class="btn btn-sm btn-default d-none" onclick="cancle_button('.$rs_value['id'].')" style="background: none;     font-size: 20px;"><i class="far fa-times-circle text-danger" aria-hidden="true"></i></button> 

                        <button type="button" class="btn btn-sm btn-default " id="delete_'.$rs_value['id'].'" onclick="remove1('.$rs_value['id'].')" style="background: none;     font-size: 20px;">
                        <i class="far fa-trash-alt text-danger"></i>
                        </button> 

                        <button type="button" id="remove_'.$rs_value['id'].'" class="btn btn-sm btn-default d-none" onclick="remove('.$rs_value['id'].','.$rs_value['image_id'].')" style="background: none;     font-size: 20px;"><i class="far fa-check-circle text-success"></i></button>

                            <button type="button" id="cancleBtn1'.$rs_value['id'].'" class="btn btn-sm btn-default d-none" onclick="cancle_button1('.$rs_value['id'].')" style="background: none;     font-size: 20px;"><i class="far fa-times-circle text-danger" aria-hidden="true"></i></button> 
                            ';
                        }
                       // echo $rs_value['img_an_status'];
                        if($user_type_id=='1'||$user_type_id=='2'||$user_type_id=='3'){
                            if($rs_value['img_an_status']=='0'){
                                $data['send_revision']='1';
                            }
                            else{
                                $data['send_revision']='0';
                            }
                        }
                        else if($user_type_id=='1'||$user_type_id=='2'||$user_type_id=='4'){
                           if($rs_value['img_an_status']=='1'){
                                $data['send_revision']='0';
                            }
                            else{
                                $data['send_revision']='1';
                            }
                        }

                        $data['lists'] .= ' 
                        </p>';
                       $i++;
                    // }while($rs = $query->result_array());
                    }
                }
            }
            else{
                if($user_type_id=='1'||$user_type_id=='2'||$user_type_id=='4'){
                    $data['lists'] .= '<span class="lead">Click on the image to add the revision ..</span>';
                    $data['send_revision']='0';
                }
            }

           echo json_encode( $data );
           
        }
        else
        {
            $data['user_id'] = '';
            $data['user_name'] = '';
            $user_id='';
            redirect('login', 'refresh');
        }
    }
    public function annotation_status()
    {
        $image_id=$_POST['image_id'];
        $array = array('image_id' => $image_id);
        $img_status1= array('img_an_status' => '1');
        $this->db->where($array);
        $report = $this->db->update('wc_image_annotation', $img_status1);
        $sql = $this->db->last_query();
        echo $report; 
    }
    public function annotation_crud()
    {
         if ($this->session->userdata('front_logged_in')) {
            $session_data = $this->session->userdata('front_logged_in');
            $data['user_id'] = $session_data['user_id'];
            $data['user_name'] = $session_data['user_name'];
            $user_id = $session_data['user_id'];
            $user_type_id=$session_data['user_type_id'];            
            // $data['brief_id'] = $brief_id;
            $data['title'] = "Proofing";
            if( !empty( $_POST['type'] ) && $_POST['type'] == "insert" )
            {
                $image_id = $_POST['image_id']; 
                $descr = $_POST['descr'];
                $position_left = $_POST['position_left'];
                $position_top = $_POST['position_top'];
               
                $create_date=date('Y/m/d');

                $checkquerys1 = $this->db->query("select * from wc_image_upload where  image_id='$image_id' ")->result_array();
                $version_num=$checkquerys1[0]['version_num'];


                $sql = "INSERT INTO wc_image_annotation(`user_id`,`image_id`,`revision_id`,`descr`,  `position_left`, `position_top`, `create_date`)VALUES('$user_id','$image_id','$version_num','$descr','$position_left','$position_top','$create_date');";

                // print_r($sql);
                $qry =$this->db->query($sql);
            }

            if( !empty( $_POST['type'] ) && $_POST['type'] == "remove")
            {
              $id = $_POST['id'];
              $sql = "DELETE FROM wc_image_annotation WHERE id = '".$id."'";
              $qry =$this->db->query($sql);
            }
            if( !empty( $_POST['type'] ) && $_POST['type'] == "update")
            {
              

              $id = $_POST['id']; 
              $image_id = $_POST['image_id']; 
              $descr = $_POST['descr'];
              $position_left = $_POST['position_left'];
              $position_top = $_POST['position_top'];

              $sql = "UPDATE wc_image_annotation SET descr='$descr' WHERE id = $id && position_left=$position_left && position_top=$position_top";
             // print_r("UPDATE gd_image_annotation SET descr='$descr' WHERE id = $id && position_left=$position_left && position_top=$position_top");
               $qry =$this->db->query($sql);
            }

        }
        else
        {
            $data['user_id'] = '';
            $data['user_name'] = '';
            $user_id='';
            redirect('login', 'refresh');
        }
        
    }
    public function select_revision()
    {
        $brief_id=$_POST['brief_id'];
        $image_id=$_POST['image_id'];
        $wc_brief_sql="SELECT * FROM wc_image_upload WHERE brief_id='$brief_id' and image_id='$image_id'";

        $wc_brief_query = $this->db->query($wc_brief_sql);
        $wc_brief_result=$wc_brief_query->result_array();
        $parent_img= $wc_brief_result[0]['parent_img'];



        $wc_brief_sql1 = "SELECT * FROM `wc_image_upload` where brief_id='$brief_id' and parent_img='$parent_img' order by version_num desc";
       // print_r($wc_brief_sql1);
        $wc_brief_query1 = $this->db->query($wc_brief_sql1);

        $wc_brief_result1=$wc_brief_query1->result_array();
        //print_r($wc_brief_result1);
        $html="";
        $html.="<select class='custom-select img_filter' onchange='revision_filter(\"".$brief_id."\",this.value)'>";
        foreach($wc_brief_result1 as $key => $value){ 
            $image_id=$value['image_id'];
            $version_num=$value['version_num'];
                            
            $html.="<option  value='".$image_id."'> Revision". $version_num."</option>";
        }
        $html.="</select> ";
         echo $html;
    }
    public function filter_status()
    {
        $filter_status= $_POST['filter_status'];
        $brief_id=$_POST['brief_id'];
        $select_value="";

        //echo "<pre>";
       

        $checkquerys = $this->db->query("select * from wc_brief where brief_id='$brief_id' ")->result_array();
        $added_by_user_id=$checkquerys[0]['added_by_user_id'];
        $brand_id=$checkquerys[0]['brand_id'];
        $brief_title=$checkquerys[0]['brief_title'];
        $brief_name=str_replace(" ","_",$brief_title);

        /*$checkquerys = $this->db->query("select wc_brands.*,wc_clients.client_name from wc_brands inner join wc_clients on wc_brands.client_id=wc_clients.client_id where wc_brands.brand_id='$brand_id' ")->result_array();*/

        $checkquerys = $this->db->query("select wc_brands.*,wc_clients.client_name from wc_users
        inner join wc_brands on wc_users.brand_id=wc_brands.brand_id
        inner join wc_clients on wc_brands.client_id=wc_clients.client_id
        where wc_users.user_id='$added_by_user_id' ")->result_array();

        $brand_id=$checkquerys[0]['brand_id'];
        $brand_name=str_replace(" ","_",$checkquerys[0]['brand_name']);
        $client_id=$checkquerys[0]['client_id'];
        $client_name=str_replace(" ","_",$checkquerys[0]['client_name']);


        //$dst = "upload/".$client_id."-".$client_name."/".$brand_id."-".$brand_name."/".$brief_id."-brief";  
        // $files_sql = "SELECT * FROM `wc_image_upload` where brief_id='$brief_id' group by parent_img";
        if($filter_status=="all")
        {
            $files_sql="SELECT * FROM wc_image_upload WHERE brief_id='$brief_id' and image_id IN (SELECT MAX(image_id) FROM wc_image_upload  GROUP BY parent_img )order by parent_img asc";
        }
        else{
            $files_sql="SELECT * FROM wc_image_upload WHERE brief_id='$brief_id' and img_status='$filter_status' and image_id IN (SELECT MAX(image_id) FROM wc_image_upload  GROUP BY parent_img )order by parent_img asc";
        }
        $files_query = $this->db->query($files_sql);
        $files_result=$files_query->result_array();
        $file_count=$files_query->num_rows();
        //print_r($file_count);
        $html="";
        if($file_count>='1'){
            foreach($files_result as $keybill => $files_result1) {

                $img_status=$files_result1['img_status'];
                $image_path=$files_result1['image_path'];
                $img_title=$files_result1['img_title'];
                $image_id=$files_result1['image_id'];
                
                $checkquerys1 = $this->db->query("select * from wc_image_upload where  image_id='$image_id' ")->result_array();
                $parent_img=$checkquerys1[0]['parent_img'];
                $version_num=$checkquerys1[0]['version_num'];


                $dst = "upload/".$client_id."-".$client_name."/".$brand_id."-".$brand_name."/".$brief_id."-".$brief_name."/".$parent_img."/Revision".$version_num;  
                //$files_name=explode('.', $image_path);
                // $img_status=$files_result1['status'];
                //print_r($files_name[0]);
            
                $html.="<div class='col-12 col-sm-12 col-md-3 p-2'>
                            <div class='card'>
                                <img class='card-img-top img_download1' src='".base_url().$dst."/thumb_".$image_path."' alt='".$img_title."' onclick='useimg(\"".$brief_id."\",\"".$image_path."\",\"".$image_id."\",\"".$img_status."\",\"".$select_value."\")'>
                                  <div class='card-body'>
                                        <div class='d-flex justify-content-between'>
                                            <span class='lead'>".$img_title."</span>";
                                            if($img_status=="1") {
                                               $html.=" <i class='fas fa-circle text-wc fa-2x'></i>";
                                            }elseif($img_status=="2") {
                                               $html.=" <i class='fas fa-sync-alt fa-2x'></i>";
                                            }elseif($img_status=="3"){
                                               $html.=" <i class='fas fa-circle text-danger fa-2x'></i>";
                                            }
                                $html.="
                                        </div>
                                    </div>
                            </div>
                        </div>";           
            }
                               
            echo $html;
        }
        else{
            $html.="<span class='lead'>No Result</span>";
             echo $html;
        }

    }  
    public function download_image()
    {
        
        if ($this->session->userdata('front_logged_in')) {
            $session_data = $this->session->userdata('front_logged_in');
            $data['user_id'] = $session_data['user_id'];
            $data['user_name'] = $session_data['user_name'];
            $user_id = $session_data['user_id'];
            $user_type_id=$session_data['user_type_id'];
            $download_value=$_POST['download_value'];

            if($download_value=="download_single"){
                $image_src=$_POST['image_src'];
                $image_src=explode("upload",$image_src);
                
                $image_src="./upload".$image_src[1];
                //
                $dir =  microtime();
                if (!file_exists($dir)) {
                    $save_to =$dir."/WE_CREATE_IMAGES".$dir."/";
                    mkdir($dir."/WE_CREATE_IMAGES".$dir, 0777, true);
                    
                }
                $zip_file ='WE_CREATE_IMAGES'.time().'.zip';
                // Get real path for our folder
                $rootPath = realpath($dir);

                // Initialize archive object
                $zip = new ZipArchive();
                $zip->open($zip_file, ZipArchive::CREATE | ZipArchive::OVERWRITE);

                // Create recursive directory iterator
                /** @var SplFileInfo[] $files */
                $files = new RecursiveIteratorIterator(
                    new RecursiveDirectoryIterator($rootPath),
                    RecursiveIteratorIterator::LEAVES_ONLY
                );
            
                $image_name1=explode("/",$image_src);
                $image_name=$save_to.end($image_name1);
                // print_r("array------".$image_name1);
                // print_r("image_name ---------------".$image_name);
                if(!copy($image_src,$image_name)){
                      // echo "failed to copy ".$image_name;
                }
                else{
                     // echo "copied $image_name into ".$image_name;
                }
       
                // exit();
                foreach ($files as $name => $file)
                {
                    // Skip directories (they would be added automatically)
                    if (!$file->isDir())
                    {
                        // Get real and relative path for current file
                        $filePath = $file->getRealPath();
                        $relativePath = substr($filePath, strlen($rootPath) + 1);

                        // Add current file to archive
                        $zip->addFile($filePath, $relativePath);
                    }
                }
      
                //exit();
                //Zip archive will be created only after closing object
                $zip->close();
                 //It it's a file.
                 
                if(file_exists($dir))
                {
                    $this->deleteAll($dir);
                    echo $zip_file;
                }
                else{
                    echo $dir;
                }
            }
            else if($download_value=="download_all")
            {
               $image_src=$_POST['image_src'];
               
               //  $image_name1=explode("/",$image_src);
               // print_r($image_name1);
               //  $image_folder="./".$image_name1[4]."/".$image_name1[5]."/".$image_name1[6]."/".$image_name1[7]."/";
               //   //print_r($image_folder);
                $dir =  microtime();
                if (!file_exists($dir)) {
                    $save_to =$dir."/WE_CREATE_IMAGES".$dir."/";
                    mkdir($dir."/WE_CREATE_IMAGES".$dir, 0777, true);
                    
                }
                $zip_file ='WE_CREATE_IMAGES'.time().'.zip';
                // Get real path for our folder
                $rootPath = realpath($dir);

                // Initialize archive object
                $zip = new ZipArchive();
                $zip->open($zip_file, ZipArchive::CREATE | ZipArchive::OVERWRITE);

                // Create recursive directory iterator
                /** @var SplFileInfo[] $files */
                $files = new RecursiveIteratorIterator(
                    new RecursiveDirectoryIterator($rootPath),
                    RecursiveIteratorIterator::LEAVES_ONLY
                );
                $downloadimgid_array=explode(",",$image_src);
               // print_r($downloadimgid_array);
                foreach ($downloadimgid_array as $downloadimgid_array_key => $downloadimgid_array_value) {
                    $downloadimgid_array_value=explode("upload",$downloadimgid_array_value);
                    $downloadimgid_array_value="./upload".$downloadimgid_array_value[1];
                    $image_src1 =str_replace('thumb_', '',$downloadimgid_array_value);
                    //echo "image src   ". $image_src1;
                    //echo "<br>";
                    $image_name1=explode("/",$image_src1);
                    //print_r($image_name1);echo "<br>";
                    $image_name=$save_to.end($image_name1);
                    //print_r("image name ----- ".$image_name);echo "<br>";
                    if(!copy($image_src1,$image_name)){
                          // echo "failed to copy ".$image_name;
                    }
                    else{
                         // echo "copied $image_name into ".$image_name;
                    }
                }
                // $sku_thumb_images = glob($image_folder."*.{png,PNG,jpg,JPG}",GLOB_BRACE);
               
                // //     print_r($sku_thumb_images);
                // foreach($sku_thumb_images as $image){ 
                //    // print_r($image);
                //     $image_name1=explode("/",$image);
                //     $image_name=$save_to.end($image_name1);
                //     //print_r($image_name1);
                //     if(!copy($image,$image_name)){
                //           // echo "failed to copy ".$image_name;
                //     }
                //     else{
                //          // echo "copied $image_name into ".$image_name;
                //     }
                // }
                // exit();
                foreach ($files as $name => $file)
                {
                    // Skip directories (they would be added automatically)
                    if (!$file->isDir())
                    {
                        // Get real and relative path for current file
                        $filePath = $file->getRealPath();
                        $relativePath = substr($filePath, strlen($rootPath) + 1);

                        // Add current file to archive
                        $zip->addFile($filePath, $relativePath);
                    }
                }
          
                //exit();
                //Zip archive will be created only after closing object
                $zip->close();
                 //It it's a file.
                     
                if(file_exists($dir))
                {
                    $this->deleteAll($dir);
                    echo $zip_file;
                }
                else{
                    echo $dir;
                }
            }
        }
        else
        {
            $data['user_id'] = '';
            $data['user_name'] = '';
            $user_id='';
            redirect('login', 'refresh');
        }
        
       
    }
    public function deleteAll($str) {
        //It it's a file.
        if (is_file($str)) {
            //Attempt to delete it.
            return unlink($str);
        }
        //If it's a directory.
        elseif (is_dir($str)) {
            //Get a list of the files in this directory.
            $scan = glob(rtrim($str,'/').'/*');
            //Loop through the list of files.
            foreach($scan as $index=>$path) {
                //Call our recursive function.
               $this-> deleteAll($path);
            }
            //Remove the directory itself.
            return @rmdir($str);
        }
    }

    public function upload_files_breif(){
        if ($this->session->userdata('front_logged_in')) {
            $session_data = $this->session->userdata('front_logged_in');
            $data['user_id'] = $session_data['user_id'];
            $data['user_name'] = $session_data['user_name'];
            $user_id=$session_data['user_id'];
            //echo "<pre>";
            $brief_id=$_POST["brief_id"];
            if(isset($_POST['image_id'])){
                $image_id=$_POST["image_id"];   
            }
            
            //$checkquerys = $this->db->query("select * from wc_users where  user_id='$user_id'")->result_array();
            // $checkquerys = $this->db->query("SELECT * FROM `wc_brief` WHERE `brief_id` ='$brief_id'")->result_array();
            // $brand_id=$checkquerys[0]['brand_id'];

            // $checkquerys = $this->db->query("select wc_brands.*,wc_clients.client_name from wc_brands inner join wc_clients on wc_brands.client_id=wc_clients.client_id where  wc_brands.brand_id='$brand_id'")->result_array();
            // $brand_id=$checkquerys[0]['brand_id'];
            // $brand_name=str_replace(" ","_",$checkquerys[0]['brand_name']);
            // $client_id=$checkquerys[0]['client_id'];
            // $client_name=str_replace(" ","_",$checkquerys[0]['client_name']);

            // $dst = "upload/".$client_id."-".$client_name."/".$brand_id."-".$brand_name."/".$brief_id."-brief";  
            // $client_folder=$client_id."-".$client_name;
            // $brand_folder=$brand_id."-".$brand_name;
            // $brief_folder=$brief_id."-brief";
            $checkquerys = $this->db->query("select * from wc_brief where brief_id='$brief_id' ")->result_array();
            $added_by_user_id=$checkquerys[0]['added_by_user_id'];
            $brand_id=$checkquerys[0]['brand_id'];
            $brief_title=$checkquerys[0]['brief_title'];
            $brief_name=str_replace(" ","_",$brief_title);

            /*$checkquerys = $this->db->query("select wc_brands.*,wc_clients.client_name from wc_brands inner join wc_clients on wc_brands.client_id=wc_clients.client_id where wc_brands.brand_id='$brand_id' ")->result_array();*/

            $checkquerys = $this->db->query("select wc_brands.*,wc_clients.client_name from wc_users
            inner join wc_brands on wc_users.brand_id=wc_brands.brand_id
            inner join wc_clients on wc_brands.client_id=wc_clients.client_id
            where wc_users.user_id='$added_by_user_id' ")->result_array();

            $brand_id=$checkquerys[0]['brand_id'];
            $brand_name=str_replace(" ","_",$checkquerys[0]['brand_name']);
            $client_id=$checkquerys[0]['client_id'];
            $client_name=str_replace(" ","_",$checkquerys[0]['client_name']);

            $dst = "upload/".$client_id."-".$client_name."/".$brand_id."-".$brand_name."/".$brief_id."-".$brief_name;  
            $client_folder=$client_id."-".$client_name;
            $brand_folder=$brand_id."-".$brand_name;
            $brief_folder=$brief_id."-".$brief_name;
            if (!is_dir("./upload/".$client_folder)) {
                mkdir("./upload/".$client_folder, 0777, true);
            }   
            if (!is_dir("./upload/".$client_folder."/".$brand_folder)) {
                mkdir("./upload/".$client_folder."/".$brand_folder, 0777, true);
            }   
            if (!is_dir("./upload/".$client_folder."/".$brand_folder."/".$brief_folder)) {
                mkdir("./upload/".$client_folder."/".$brand_folder."/".$brief_folder, 0777, true);
            }   
            $brief_path="./upload/".$client_folder."/".$brand_folder."/".$brief_folder;
            $timestamp=time();
            //if (!is_dir("./brief_upload/".$brief_id)) {
            //mkdir("./brief_upload/".$brief_id, 0777, true);
            //}

            $file=$_FILES["image"]["name"];
            $ext = pathinfo($file, PATHINFO_EXTENSION);
            $tm=time();
            //$file = $tm.".".$ext;
            $file=$tm.".jpg";
            if(isset($_POST['image_id'])) 
            {
                //  $data['viewfilesinfo'] = $this->Proofing_model->insertimages1($file,$brief_id,$image_id);
                $lastid= $this->Proofing_model->insertimages1($file,$brief_id,$image_id);
            }
            else {
                //$data['viewfilesinfo'] = $this->Proofing_model->insertimages($file,$brief_id);
                $lastid=$this->Proofing_model->insertimages($file,$brief_id);
            }
            if (!is_dir($brief_path."/".$lastid)) {
                mkdir($brief_path."/".$lastid, 0777, true);
            }
            // echo $lastid;exit();
            $wc_brief_sql_ver = "SELECT version_num,img_status FROM `wc_image_upload` where brief_id='$brief_id' and parent_img='$lastid' ORDER BY `version_num` DESC";
            $wc_brief_query_ver = $this->db->query($wc_brief_sql_ver);
            $wc_brief_result_ver=$wc_brief_query_ver->result_array();
            $num=sizeof($wc_brief_result_ver);
            if($num>0){
                $max_ver=$wc_brief_result_ver[0]['version_num'];
            }else{
                $max_ver=1;
            }

            //$max_ver=$max_ver+1;

            if (!is_dir($brief_path."/".$lastid."/Revision".$max_ver."")) {
                mkdir($brief_path."/".$lastid."/Revision".$max_ver."", 0777, true);
            }     
            $dir =$brief_path."/".$lastid."/Revision".$max_ver."/";

            if($_FILES["image"]["type"]=='image/jpeg'|| $_FILES["image"]["type"]=='image/jpg' || $_FILES["image"]["type"]=='video/webm' || $_FILES["image"]["type"]=='video/mp4' || $_FILES["image"]["type"]=='video/3gpp' || $_FILES["image"]["type"]=='video/x-sgi-movie' || $_FILES["image"]["type"]=='video/x-msvideo' || $_FILES["image"]["type"]=='video/mpeg' || $_FILES["image"]["type"]=='video/x-ms-wmv' ){
                move_uploaded_file($_FILES["image"]["tmp_name"], $dir. $file);
            }else{
                if($_FILES["image"]["type"]=='image/gif' || $_FILES["image"]["type"]=='image/png' || $_FILES["image"]["type"]=='image/bmp'){
                    $img = $_FILES['image']['tmp_name'];
                    // $file_image=substr($file,4);
                    $file_image=$file;
                    //////////////////////////Convert JPG start//////////////////////////////////////
                    $file=$this->convertToJpeg($dir,$img,$file_image);
                    //////////////////////////Convert JPG end//////////////////////////////////////
                }
            }
            if($_FILES["image"]["type"]=='image/gif' || $_FILES["image"]["type"]=='image/png' || $_FILES["image"]["type"]=='image/bmp' || $_FILES["image"]["type"]=='image/jpg' || $_FILES["image"]["type"]=='image/jpeg'){
                //$file_image=$timestamp.pathinfo($_FILES["image"]["name"], PATHINFO_FILENAME);
                $file_image=pathinfo($_FILES["image"]["name"], PATHINFO_FILENAME);

                $new_thumb = $brief_path."/".$lastid."/Revision".$max_ver."/thumb_".$file;
                $file_pat = $brief_path."/".$lastid."/Revision".$max_ver."/".$file;
                $this->convertToThumb($new_thumb,$brief_id,$file_pat);
                //echo ltrim($file);
            }else{

                $file_image=pathinfo($_FILES["image"]["name"], PATHINFO_FILENAME);
                $new_thumb = $brief_path."/".$lastid."/Revision".$max_ver."/"."thumb_".$file;
                $file_pat = "./assets/img/video_thumbnail.jpg";
                $this->convertToThumb($new_thumb,$brief_id,$file_pat);      
            }

            $update_sql = "UPDATE `wc_brief` SET `status`='6' WHERE brief_id='$brief_id' ";
            $this->db->query($update_sql);

            echo "success";

        }else{
            $data['user_id'] = '';
            $data['user_name'] = '';
            $user_id='';
            echo "sessionout";
        }
    }


    public function convertToThumb($new_thumb,$brief_id,$file_path) {

        if(!file_exists($new_thumb)){
            $width = "200";
            $height = "150";
            $quality = 90;
            $img = $file_path;

            //Generate Thumbnail Images

            $file = $img;
            $dest = $new_thumb;
            $height = 150;
            $width = 200;
            $output_format = "jpg";
            $output_quality = 90;
            //$bg_color = array(255, 255, 255);
            $bg_color = array(0, 0, 0);

            //Justify the Image format and create a GD resource
            $image_info = getimagesize($file);
            list($cur_width, $cur_height, $cur_type, $cur_attr) = getimagesize($file);

            $image_type = $image_info[2];
            switch($image_type){
                case IMAGETYPE_JPEG:
                    $image = imagecreatefromjpeg($file);
                break;
                case IMAGETYPE_GIF:
                    $image = imagecreatefromgif($file);
                break;
                case IMAGETYPE_GIF:
                    $image = imagecreatefrompng($file);
                break;
                default:
            /////////////////changes//////////////////////////
                die("notsupport");
            }

            //if($cur_width>$cur_height){
            //$degrees = -90;
            //$image = imagerotate($image, $degrees, 0);
            //}

            $image_width = imagesx($image);
            $image_height = imagesy($image);

            //echo $file;

            //Get The Calculations
            // Calculate the size of the Image
            //If Image width is bigger than the Thumbnail Width
            if($image_width>$image_height){
                //echo "hoko";
                //echo $image_width.">".$image_height;

                //Set Image Width to Thumbnail Width
                $new["width"] = $width;
                //Calculate Height according to width
                $new["height"] = ($new["width"]/$image_width)*$image_height;

                //If Resulting height is bigger than the thumbnail Height
                if($new["height"]>$height){

                    //Set the image Height to THUmbnail Height
                    $new["height"] = $height;
                    //Recalculate width according to height of the thumbnail
                    $new["width"] = ($new["height"]/$image_height)*$image_width;

                }

            }else{
                //echo "moko";
                $new["height"] = $height;
                $new["width"] = ($new["height"]/$image_height)*$image_width;

                if($new["width"]>$width){

                    $new["width"] = $width;
                    $new["height"] = ($new["width"]/$image_width)*$image_height;

                }

            }

            //Calculate the image position based on the difference between the dimensons of the new image and thumbnail
            $x = ($width-$new["width"])/2;
            $y = ($height-$new["height"])/2;

            $calc =  array_merge($new, array("x"=>$x,"y"=>$y));

            // End Calculate The Image

            //Create an Empty image
            $canvas = imagecreatetruecolor($width, $height);

            //Load Background color
            $color = imagecolorallocate($canvas,$bg_color[0],$bg_color[1],$bg_color[2]);

            //FIll the Image with the Background color
            imagefilledrectangle($canvas,0,0,$width,$height,$color);

            //The REAL Magic
            imagecopyresampled($canvas,$image,$calc["x"],$calc["y"],0,0,$calc["width"],$calc["height"],$image_width,$image_height);

            // Create Output Image

            $image = $canvas;
            $format = $output_format;
            $quality = $output_quality;



            switch($format){
                case "jpg":
                    imagejpeg($image, $dest, $quality);
                break;
                case "gif":
                    imagegif($image, $dest);
                break;
                case "png":
                //Png Quality is measured from 1 to 9
                    imagepng($image, $dest, round(($quality/100)*9) );
                break;
            }
            //unlink($img);
        }
        else{
            echo "file is exist.";
        }


    }

    public function convertToJpeg($dir,$img,$file_image) {

        $dst = $dir . $file_image;

        if (($img_info = getimagesize($img)) === FALSE)
        die("Image not found or not an image");

        $width = $img_info[0];
        $height = $img_info[1];

        switch ($img_info[2]) {
            case IMAGETYPE_GIF  : $src = imagecreatefromgif($img);  break;
            case IMAGETYPE_JPEG : $src = imagecreatefromjpeg($img); break;
            case IMAGETYPE_PNG  : $src = imagecreatefrompng($img);  break;
            default : die("Unknown filetype");
        }

        $tmp = imagecreatetruecolor($width, $height);

        $file =$dst.".jpg";

        imagecopyresampled($tmp, $src, 0, 0, 0, 0, $width, $height, $width, $height);
        imagejpeg($tmp, $file);
        return $file_image.".jpg";

    }

}

?>