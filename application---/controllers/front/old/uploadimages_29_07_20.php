<?php 
class Uploadimages extends CI_Controller{
    
    function __construct(){
        parent::__construct();
        $this->load->library(['session']); 
        $this->load->helper(['url','file','form']); 
        $this->load->model('uploadbrief_model'); //load model upload 
        $this->load->model('Dashboard_model'); //load model upload
        $this->load->model('Uploadimages_model'); //load model upload   
        $this->load->library('upload'); //load library upload 
         $this->load->library('email');
    } 

    public function index($brief_id){

        if ($this->session->userdata('front_logged_in')) {
        $session_data = $this->session->userdata('front_logged_in');
        $data['user_id'] = $session_data['user_id'];
        $data['user_name'] = $session_data['user_name'];
    $type1 = $session_data['user_type_id'];
    $brand_access=$session_data['brand_access'];
        $data['brief_id'] = $brief_id;
        $data['title'] = "upload images";
        $this->load->view('front/uploadimages',$data);    
        }
        else
        {
        $data['user_id'] = '';
        $data['user_name'] = '';
        $user_id='';
        redirect('login', 'refresh');
        }       
    }
  
  
  
  



  public function upload_files_image(){
    if ($this->session->userdata('front_logged_in')) {

      $session_data = $this->session->userdata('front_logged_in');
      $data['user_id'] = $session_data['user_id'];
      $data['user_name'] = $session_data['user_name'];
      $user_id=$session_data['user_id'];
      //echo "<pre>";
      $brief_id=preg_replace('/[^a-zA-Z0-9_ -]/s','',$_POST["brief_id"]);
      $image_num=preg_replace('/[^a-zA-Z0-9_ -]/s','',$_POST["image_num"])+1;
      $image_tot_num=preg_replace('/[^a-zA-Z0-9_ -]/s','',$_POST['image_tot_num']);
      $image_id=''; 
      if(isset($_POST['image_id'])) 
        {
        $image_id=preg_replace('/[^a-zA-Z0-9_ -]/s','',$_POST["image_id"]); 
        } 
        //echo "imge id---------".$image_id;exit();
      $checkquerys = $this->db->query("select * from wc_brief where brief_id='$brief_id' ")->result_array();
            $added_by_user_id=$checkquerys[0]['added_by_user_id'];
            $brand_id=$checkquerys[0]['brand_id'];
            $brief_title=$checkquerys[0]['brief_title'];
            $brief_name=preg_replace('/[^a-zA-Z0-9_.]/', '_',$brief_title);

            /*$checkquerys = $this->db->query("select wc_brands.*,wc_clients.client_name from wc_brands inner join wc_clients on wc_brands.client_id=wc_clients.client_id where wc_brands.brand_id='$brand_id' ")->result_array();*/

            /*$checkquerys = $this->db->query("select wc_brands.*,wc_clients.client_name from wc_users
            inner join wc_brands on wc_users.brand_id=wc_brands.brand_id
            inner join wc_clients on wc_brands.client_id=wc_clients.client_id
            where wc_users.user_id='$added_by_user_id' ")->result_array();*/


        $checkquerys = $this->db->query("select wc_brands.*,wc_clients.client_name from wc_brands
        inner join wc_clients on wc_brands.client_id=wc_clients.client_id
        where wc_brands.brand_id='$brand_id' ")->result_array();

            $brand_id=$checkquerys[0]['brand_id'];
            $brand_name=preg_replace('/[^a-zA-Z0-9_.]/', '_',$checkquerys[0]['brand_name']);
            $client_id=$checkquerys[0]['client_id'];
            $client_name=preg_replace('/[^a-zA-Z0-9_.]/', '_',$checkquerys[0]['client_name']);

            $dst = "upload/".$client_id."-".$client_name."/".$brand_id."-".$brand_name."/".$brief_id."-".$brief_name;  
            $client_folder=$client_id."-".$client_name;
            $brand_folder=$brand_id."-".$brand_name;
            $brief_folder=$brief_id."-".$brief_name;
            if (!is_dir("./upload/".$client_folder)) {
                mkdir("./upload/".$client_folder, 0777, true);
            }   
            if (!is_dir("./upload/".$client_folder."/".$brand_folder)) {
                mkdir("./upload/".$client_folder."/".$brand_folder, 0777, true);
            }   
            if (!is_dir("./upload/".$client_folder."/".$brand_folder."/".$brief_folder)) {
                mkdir("./upload/".$client_folder."/".$brand_folder."/".$brief_folder, 0777, true);
            }   
            $brief_path="./upload/".$client_folder."/".$brand_folder."/".$brief_folder;
            $timestamp=time();
            //if (!is_dir("./brief_upload/".$brief_id)) {
            //mkdir("./brief_upload/".$brief_id, 0777, true);
            //}



              if ((exif_imagetype($_FILES["image"]["tmp_name"]) == IMAGETYPE_JPEG) && (imagecreatefromjpeg( $_FILES["image"]["tmp_name"] ) !== false ))
            {
             //echo 'The picture is a valid jpg<br>';
            }
            elseif ((exif_imagetype($_FILES["image"]["tmp_name"]) == IMAGETYPE_PNG) && (imagecreatefrompng( $_FILES["image"]["tmp_name"] ) !== false ))
            {
             //echo 'The picture is a valid png<br>';
            }
            elseif (is_file($_FILES["image"]["tmp_name"]) && (0 === strpos(mime_content_type($_FILES["image"]["tmp_name"]), 'video/')))
            {
                //echo 'The picture is a valid mp4<br>';
            }
            else
            {
                 //echo 'notvalid';
                 /* echo "File ".$image_num." of ".$image_tot_num." not uploading because it has invalid property.@@@@@".$image_num."@@@@@".$image_tot_num."@@@@@notvalid";*/
                  echo "Files may contain some invalid property so it will skip that file!.@@@@@".$image_num."@@@@@".$image_tot_num."@@@@@notvalid@@@@@".$image_id;
                 exit;
            }

            

            $file=$_FILES["image"]["name"];
            $ext = pathinfo($file, PATHINFO_EXTENSION);
            $file_name=preg_replace('/[^a-zA-Z0-9_.]/', '_',pathinfo($file, PATHINFO_FILENAME));
            $tm=time();
            //$file = $tm.".".$ext;
           // $file=$tm.".jpg";
            $file = $file_name."_".$tm.".".$ext;
            
            if($_FILES["image"]["type"]=='image/gif' || $_FILES["image"]["type"]=='image/png' || $_FILES["image"]["type"]=='image/bmp' || $_FILES["image"]["type"]=='image/jpg' || $_FILES["image"]["type"]=='image/jpeg')
            {
            $file_type="0";
            }
            elseif($_FILES["image"]["type"]=='video/webm' || $_FILES["image"]["type"]=='video/mp4' || $_FILES["image"]["type"]=='video/3gpp' || $_FILES["image"]["type"]=='video/x-sgi-movie' || $_FILES["image"]["type"]=='video/x-msvideo' || $_FILES["image"]["type"]=='video/mpeg' || $_FILES["image"]["type"]=='video/x-ms-wmv' )
            {

            $file_type="1";

            }
            
            if(isset($_POST['image_id'])) 
            {
                //  $data['viewfilesinfo'] = $this->Proofing_model->insertimages1($file,$brief_id,$image_id);
                $lastid= $this->Uploadimages_model->insertimages1($file,$file_type,$brief_id,$image_id);
            }
            else {
                //$data['viewfilesinfo'] = $this->Proofing_model->insertimages($file,$brief_id);
                $lastid=$this->Uploadimages_model->insertimages($file,$file_type,$brief_id);
            }
            if (!is_dir($brief_path."/".$lastid)) {
                mkdir($brief_path."/".$lastid, 0777, true);
            }
            // echo $lastid;exit();
            $wc_brief_sql_ver = "SELECT version_num,img_status FROM `wc_image_upload` where brief_id='$brief_id' and parent_img='$lastid' ORDER BY `version_num` DESC";
            $wc_brief_query_ver = $this->db->query($wc_brief_sql_ver);
            $wc_brief_result_ver=$wc_brief_query_ver->result_array();
            $num=sizeof($wc_brief_result_ver);
            if($num>0){
                $max_ver=$wc_brief_result_ver[0]['version_num'];
            }else{
                $max_ver=1;
            }

            //$max_ver=$max_ver+1;

            if (!is_dir($brief_path."/".$lastid."/Revision".$max_ver."")) {
                mkdir($brief_path."/".$lastid."/Revision".$max_ver."", 0777, true);
            }     
            $dir =$brief_path."/".$lastid."/Revision".$max_ver."/";

            if($_FILES["image"]["type"]=='image/jpeg'|| $_FILES["image"]["type"]=='image/jpg' || $_FILES["image"]["type"]=='video/webm' || $_FILES["image"]["type"]=='video/mp4' || $_FILES["image"]["type"]=='video/3gpp' || $_FILES["image"]["type"]=='video/x-sgi-movie' || $_FILES["image"]["type"]=='video/x-msvideo' || $_FILES["image"]["type"]=='video/mpeg' || $_FILES["image"]["type"]=='video/x-ms-wmv' ||$_FILES["image"]["type"]=='image/gif' || $_FILES["image"]["type"]=='image/png' || $_FILES["image"]["type"]=='image/bmp'){
                move_uploaded_file($_FILES["image"]["tmp_name"], $dir. $file);
            }
            if($_FILES["image"]["type"]=='image/gif' || $_FILES["image"]["type"]=='image/png' || $_FILES["image"]["type"]=='image/bmp' || $_FILES["image"]["type"]=='image/jpg' || $_FILES["image"]["type"]=='image/jpeg'){
                //$file_image=$timestamp.pathinfo($_FILES["image"]["name"], PATHINFO_FILENAME);
                $file_image=pathinfo($_FILES["image"]["name"], PATHINFO_FILENAME);

                $new_thumb = $brief_path."/".$lastid."/Revision".$max_ver."/thumb_".$file;
                $org_img = $brief_path."/".$lastid."/Revision".$max_ver."/org_".$file;
                $file_pat = $brief_path."/".$lastid."/Revision".$max_ver."/".$file;
                $this->convertToThumb($new_thumb,$brief_id,$file_pat);

                $this->reduceSize($org_img,$brief_id,$file_pat);
                //echo ltrim($file);

                // $new_thumb = $brief_path."/".$lastid."/Revision".$max_ver."/thumb_".$tm.".jpg";
                // copy($file_pat,$new_thumb);

                
            }else{

                $file_image=pathinfo($_FILES["image"]["name"], PATHINFO_FILENAME);
                $new_thumb = $brief_path."/".$lastid."/Revision".$max_ver."/"."thumb_".$file;
                $org_img = $brief_path."/".$lastid."/Revision".$max_ver."/"."org_".$file;
                $file_pat = "./assets/img/video_thumbnail.jpg";
                $this->convertToThumb($new_thumb,$brief_id,$file_pat);

                $this->reduceSize($org_img,$brief_id,$file_pat);


                // $file_pat = "./assets/img/video_thumbnail.jpg";
                // $new_thumb = $brief_path."/".$lastid."/Revision".$max_ver."/thumb_".$tm.".jpg";
                // copy($file_pat,$new_thumb);



            }
       //$this->load->view('front/viewfiles_search',$data);
      //echo insert_sql;
       $update_sql = "UPDATE `wc_brief` SET `status`='6' WHERE brief_id='$brief_id' ";
       $this->db->query($update_sql);
     //echo "image ".$image_num." of ".$image_tot_num." uploaded successfully";
    /* echo "image ".$image_num." of ".$image_tot_num." uploaded successfully@@@@@".$image_num."@@@@@".$image_tot_num;*/

     echo "image ".$image_num." of ".$image_tot_num." uploaded successfully@@@@@".$image_num."@@@@@".$image_tot_num."@@@@@valid@@@@@".$image_id;

     if($image_num==$image_tot_num) {


 /*********************************Send mail*************************************/
   
    $query=$this->db->query("select * from  wc_settings  WHERE setting_key='MAIL_SMTP_USER' ");
    $userDetails= $query->result_array();
    $MAIL_SMTP_USER=$userDetails[0]['setting_value'];

    $query=$this->db->query("select * from  wc_settings  WHERE setting_key='MAIL_SMTP_PASSWORD' ");
    $userDetails= $query->result_array();
    $MAIL_SMTP_PASSWORD=$userDetails[0]['setting_value'];

    /*$query=$this->db->query("select * from  wc_settings  WHERE setting_key='MAIL_BODY_NOTIFY_FILEUPLOAD' ");
    $userDetails= $query->result_array();
    $MAIL_BODY=$userDetails[0]['setting_value'];*/

     if(isset($_POST['image_id'])) 
        {

            $query=$this->db->query("select * from  wc_settings  WHERE setting_key='MAIL_BODY_UPLOAD_REVISED_FILE' ");
            $userDetails= $query->result_array();
            $MAIL_BODY=$userDetails[0]['setting_value'];

        }
        else
        {

            $query=$this->db->query("select * from  wc_settings  WHERE setting_key='MAIL_BODY_NOTIFY_FILEUPLOAD' ");
            $userDetails= $query->result_array();
            $MAIL_BODY=$userDetails[0]['setting_value'];

        }


    $query=$this->db->query("select * from  wc_settings  WHERE setting_key='MAIL_FROM' ");
    $userDetails= $query->result_array();
    $MAIL_FROM=$userDetails[0]['setting_value'];


    $query=$this->db->query("select * from  wc_settings  WHERE setting_key='MAIL_FROM_TEXT' ");
    $userDetails= $query->result_array();
    $MAIL_FROM_TEXT=$userDetails[0]['setting_value'];

    $checkquerys = $this->db->query("select * from wc_brief where  brief_id='$brief_id' ")->result_array();
    $brief_id=$checkquerys[0]['brief_id'];
    //$rejected_reasons=$checkquerys[0]['rejected_reasons'];
    $added_by_user_id=$checkquerys[0]['added_by_user_id'];
    $brief_title=$checkquerys[0]['brief_title'];
    $brand_id=$checkquerys[0]['brand_id'];





    $checkquerys = $this->db->query("select * from wc_users where  user_id='$added_by_user_id' ")->result_array();
    $brand_manager_email=$checkquerys[0]['user_email'];
    $brand_manager_name=$checkquerys[0]['user_name'];
    $client_id=$checkquerys[0]['client_id'];

    $to_email_all_bm[]="";
    if($brand_id!='')
    {

//echo "select * from wc_users where  client_id='$client_id' and brand_id like '%,".$brand_id.",%' and user_type_id='4' ";
    $checkquerys_mail = $this->db->query("select * from wc_users where  client_id='$client_id' and brand_id like '%,".$brand_id.",%' and user_type_id='4' ")->result_array();


    if(!empty($checkquerys_mail))
    {
    foreach($checkquerys_mail as $key => $value)
    {

    $to_email_all_bm[]=$value['user_email'];

    }

    }




    }
 $checkquerys_clientmail= $this->db->query("SELECT * FROM `wc_clients` WHERE `client_id`='$client_id'")->result_array();
         $client_email=$checkquerys_clientmail[0]['client_email'];
         array_push($to_email_all_bm,$client_email);



    //$subject=$MAIL_FROM_TEXT.' - Notification mail for Card - '.$brief_id;
   // $subject=$MAIL_FROM_TEXT.' - New Files Uploaded (#'.$brief_id.')';

    if(isset($_POST['image_id'])) 
        {

            $subject=$MAIL_FROM_TEXT.' - New Revised Files Uploaded (#'.$brief_id.')';

        }
        else
        {

            $subject=$MAIL_FROM_TEXT.' - New Files Uploaded (#'.$brief_id.')';

        }
   // $uemail=$brand_manager_email;
    //$unique_user_id=time()."-".$user_id;
    //$activation_link=base_url()."activate-registration/".base64_encode($unique_user_id);

        
        //Load email library
        $this->load->library('email');

        $config = array();
        //$config['protocol'] = 'smtp';
        $config['protocol']     = 'mail';
        $config['smtp_host'] = 'localhost';
        $config['smtp_user'] = $MAIL_SMTP_USER;
        $config['smtp_pass'] = $MAIL_SMTP_PASSWORD;
        $config['smtp_port'] = 25;




        $this->email->initialize($config);
        $this->email->set_newline("\r\n");
         /*if($client_id!='') {
                    $checkquerys = $this->db->query("select * from wc_clients where client_id='$client_id' ")->result_array();

                    $client_image=$checkquerys[0]['client_image'];
                    $wekan_client_id=$checkquerys[0]['wekan_client_id'];

                    $client_logo=base_url()."/upload_client_logo/".$client_image;
                    }
                    else
                    {
                    $client_logo=base_url()."/website-assets/images/logo.png";
                    }
                    $client_logo="http://style.piquic.com/WeCreate//website-assets/images/logo.png";*/

        $website_url=base_url();
             $dashboard_url=base_url()."dashboard";            
        
        // $from_email = "demo.intexom@gmail.com";
        $from_email = $MAIL_FROM;
        //$to_email = $brand_manager_email;
        $to_email = $to_email_all_bm;

        $date_time=date("d/m/y");
        /*$copy_right=$MAIL_FROM_TEXT." - ".date("Y");*/
        $copy_right=$MAIL_FROM_TEXT;
        $project_name="#".$brief_id." - ".$brief_title;
        //echo $message=$MAIL_BODY;
       /* $find_arr = array("##CLIENT_LOGO##","##BM_NAME##","##BRIEF_ID##","##IMAGE_ID##","##COPY_RIGHT##");
        $replace_arr = array($client_logo,$brand_manager_name,$brief_id,$lastid,$copy_right);*/

        $find_arr = array("##WEBSITE_URL##","##PROJECT_NAME##","##DASHBOARD_LINK##","##COPY_RIGHT##");
            $replace_arr = array($website_url,$project_name,$dashboard_url,$copy_right);


        $message=str_replace($find_arr,$replace_arr,$MAIL_BODY);

        //echo $message;exit;



        $this->email->from($from_email, $MAIL_FROM_TEXT);
        $this->email->to($to_email);
        if($_SERVER['HTTP_HOST']!='collab.piquic.com')
                    {
                        $this->email->cc(array('poulami@mokshaproductions.in','pranav@mokshaproductions.in','raja.priya@mokshaproductions.in','rajendra.prasad@mokshaproductions.in'));
                    }
       
         //$this->email->bcc('$client_email');
        //$this->email->bcc('pranav@mokshaproductions.in');
        $this->email->subject($subject);
        $this->email->message($message);
        //Send mail
       /* if($this->email->send())
        {

            //echo "mail sent";exit;
            $this->session->set_flashdata("email_sent","Congragulation Email Send Successfully.");
        }
        
        else
        {
            //echo $this->email->print_debugger();
            //echo "mail not sent";exit;
            $this->session->set_flashdata("email_sent","You have encountered an error");
        }
        */

    //exit;

/************************************************************************/

    }

    }
    else
    {
    $data['user_id'] = '';
    $data['user_name'] = '';
    $user_id='';
    echo "sessionout";
    }
  }













function convertToThumb($targetFile, $brief_id, $originalFile) {

  $newHeight = 400;
  $newWidth = 400;

  $info = getimagesize($originalFile);
  $mime = $info['mime'];

  switch ($mime) {
    case 'image/jpeg':
    $image_create_func = 'imagecreatefromjpeg';
    $image_save_func = 'imagejpeg';
    $new_image_ext = 'jpg';
    break;

    case 'image/png':
    $image_create_func = 'imagecreatefrompng';
    $image_save_func = 'imagepng';
    $new_image_ext = 'png';
    break;

    // case 'image/gif':
    // $image_create_func = 'imagecreatefromgif';
    // $image_save_func = 'imagegif';
    // $new_image_ext = 'gif';
    // break;

    default: 
    throw new Exception('Unknown image type.');
  }

  $img = $image_create_func($originalFile);
  list($width, $height) = getimagesize($originalFile);

  if ($width < 400 && $height < 400) {
    $newWidth = $width; 
    $newHeight = $height;
  } else {
    if ($width > $height) {
      $newWidth = ($width / $height) * $newHeight;
    } else {
      $newHeight = $newWidth / ($width / $height);
    }
  }
  

  $tmp = imagecreatetruecolor($newWidth, $newHeight);
  imagecopyresampled($tmp, $img, 0, 0, 0, 0, $newWidth, $newHeight, $width, $height);

  if (file_exists($targetFile)) {
    unlink($targetFile);
  }

  $image_save_func($tmp, "$targetFile");
}

function reduceSize($targetFile, $brief_id, $originalFile) {

  $info = getimagesize($originalFile);
  $mime = $info['mime'];

  switch ($mime) {
    case 'image/jpeg':
    $image_create_func = 'imagecreatefromjpeg';
    $image_save_func = 'imagejpeg';
    $new_image_ext = 'jpg';
    break;

    case 'image/png':
    $image_create_func = 'imagecreatefrompng';
    $image_save_func = 'imagepng';
    $new_image_ext = 'png';
    break;

    // case 'image/gif':
    // $image_create_func = 'imagecreatefromgif';
    // $image_save_func = 'imagegif';
    // $new_image_ext = 'gif';
    // break;

    default: 
    throw new Exception('Unknown image type.');
  }

  $img = $image_create_func($originalFile);
  list($width, $height) = getimagesize($originalFile);

  // $newWidth = ($width / $height) * $newHeight;

  $newWidth = $width;
  $newHeight = $height;

  $tmp = imagecreatetruecolor($newWidth, $newHeight);
  imagecopyresampled($tmp, $img, 0, 0, 0, 0, $newWidth, $newHeight, $width, $height);

  if (file_exists($targetFile)) {
    unlink($targetFile);
  }

  $image_save_func($tmp, "$targetFile");
}



  
  
  
  
  

function getVideoInformation($videoPath)
{
$movie = new ffmpeg_movie($videoPath,false);

$this->videoDuration = $movie->getDuration();
$this->frameCount = $movie->getFrameCount();
$this->frameRate = $movie->getFrameRate();
$this->videoTitle = $movie->getTitle();
$this->author = $movie->getAuthor() ;
$this->copyright = $movie->getCopyright();
$this->frameHeight = $movie->getFrameHeight();
$this->frameWidth = $movie->getFrameWidth();
$this->pixelFormat = $movie->getPixelFormat();
$this->bitRate = $movie->getVideoBitRate();
$this->videoCodec = $movie->getVideoCodec();
$this->audioCodec = $movie->getAudioCodec();
$this->hasAudio = $movie->hasAudio();
$this->audSampleRate = $movie->getAudioSampleRate();
$this->audBitRate = $movie->getAudioBitRate();


}


function getAudioInformation($videoPath)
{
$movie = new ffmpeg_movie($videoPath,false);

$this->audioDuration = $movie->getDuration();
$this->frameCount = $movie->getFrameCount();
$this->frameRate = $movie->getFrameRate();
$this->audioTitle = $movie->getTitle();
$this->author = $movie->getAuthor() ;
$this->copyright = $movie->getCopyright();
$this->artist = $movie->getArtist();
$this->track = $movie->getTrackNumber();
$this->bitRate = $movie->getBitRate();
$this->audioChannels = $movie->getAudioChannels();
$this->audioCodec = $movie->getAudioCodec();
$this->audSampleRate = $movie->getAudioSampleRate();
$this->audBitRate = $movie->getAudioBitRate();

}

function getThumbImage($videoPath,$thumb_file_name,$thumb_videoPath)
{
$movie = new ffmpeg_movie($videoPath,false);
$this->videoDuration = $movie->getDuration();
$this->frameCount = $movie->getFrameCount();
$this->frameRate = $movie->getFrameRate();
$this->videoTitle = $movie->getTitle();
$this->author = $movie->getAuthor() ;
$this->copyright = $movie->getCopyright();
$this->frameHeight = $movie->getFrameHeight();
$this->frameWidth = $movie->getFrameWidth();

$capPos = ceil($this->frameCount/4);

if($this->frameWidth>120)
{
$cropWidth = ceil(($this->frameWidth-120)/2);
}
else
{
$cropWidth =0;
}
if($this->frameHeight>90)
{
$cropHeight = ceil(($this->frameHeight-90)/2);
}
else
{
$cropHeight = 0;
}
if($cropWidth%2!=0)
{
$cropWidth = $cropWidth-1;
}
if($cropHeight%2!=0)
{
$cropHeight = $cropHeight-1;
}

$frameObject = $movie->getFrame($capPos);


if($frameObject)
{
/*$imageName = "tmb_vid_1212.jpg";
$tmbPath = "/home/home_Dir/public_html/uploads/thumb/".$imageName;*/
$imageName = $thumb_file_name;
$tmbPath = $thumb_videoPath.$imageName;
$frameObject->resize(120,90,0,0,0,0);
imagejpeg($frameObject->toGDImage(),$tmbPath);
}
else
{
$imageName="";
}


return $imageName;

}

public function uploadfile(){

        if ($this->session->userdata('front_logged_in')) {
            //echo "aaaaaa";exit;



            $session_data = $this->session->userdata('front_logged_in');
            $data['user_id'] = $session_data['user_id'];
            $data['user_type_id'] = $session_data['user_type_id'];
            $data['user_name'] = $session_data['user_name'];
            $folderName=$data['user_id'];
            $user_id=$data['user_id'];  

            
            $brief_id = $this->input->post('brief_id');
            $temp_folder = $this->input->post('temp_folder');  


                $fileName = $_FILES["upld_img"]["name"]; // The file name
                $fileTmpLoc = $_FILES["upld_img"]["tmp_name"]; // File in the PHP tmp folder
                $fileType = $_FILES["upld_img"]["type"]; // The type of file it is
                $fileSize = $_FILES["upld_img"]["size"]; // File size in bytes
                $fileErrorMsg = $_FILES["upld_img"]["error"]; // 0 for false... and 1 for true
                if (!$fileTmpLoc) { // if file not chosen
                echo "ERROR: Please select a file to upload.";
                exit();
                }



            $checkquerys = $this->db->query("SELECT * FROM `wc_brief` WHERE `brief_id` ='$brief_id'")->result_array();
            $brand_id=$checkquerys[0]['brand_id'];
            $brief_title=$checkquerys[0]['brief_title'];
            $brief_name=preg_replace('/[^a-zA-Z0-9_.]/', '_',$brief_title);
            $checkquerys = $this->db->query("select wc_brands.*,wc_clients.client_name from wc_brands inner join wc_clients on wc_brands.client_id=wc_clients.client_id where  wc_brands.brand_id='$brand_id'")->result_array();
            $brand_id=$checkquerys[0]['brand_id'];
            $brand_name=preg_replace('/[^a-zA-Z0-9_.]/', '_',$checkquerys[0]['brand_name']);
            $client_id=$checkquerys[0]['client_id'];
            $client_name=preg_replace('/[^a-zA-Z0-9_.]/', '_',$checkquerys[0]['client_name']);
            $dst = "upload/".$client_id."-".$client_name."/".$brand_id."-".$brand_name."/".$brief_id."-".$brief_name;  
            $client_folder=$client_id."-".$client_name;
            $brand_folder=$brand_id."-".$brand_name;
            $brief_folder=$brief_id."-".$brief_name;
            if (!is_dir("./upload/".$client_folder)) {
            mkdir("./upload/".$client_folder, 0777, true);
            } 
            if (!is_dir("./upload/".$client_folder."/".$brand_folder)) {
            mkdir("./upload/".$client_folder."/".$brand_folder, 0777, true);
            } 
            if (!is_dir("./upload/".$client_folder."/".$brand_folder."/".$brief_folder)) {
            mkdir("./upload/".$client_folder."/".$brand_folder."/".$brief_folder, 0777, true);
            } 
            $brief_path="./upload/".$client_folder."/".$brand_folder."/".$brief_folder;
            $timestamp=time();









                /*$folder_path="temp_brief_upload/".$temp_folder;
                if (!file_exists($folder_path)) {
                mkdir($folder_path, 0777, true);
                }
                if(move_uploaded_file($fileTmpLoc, "$folder_path/$fileName")){
                
                echo $fileName."@@@@@DOC file uploaded successfully!";

                } else {
                echo "DOC file upload failed!";
                }  */
      

        }

         else {
            //echo "aaaaaa";exit;
            //If no session, redirect to login page
            redirect('login', 'refresh');
        }
            
            
            
                
        }

  
  
  
  

}

?>