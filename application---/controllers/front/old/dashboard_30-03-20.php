<?php 
class Dashboard extends CI_Controller{
    
    function __construct(){
        parent::__construct();
        $this->load->library(['session']); 
        $this->load->helper(['url','file','form']); 
        $this->load->model('uploadbrief_model'); //load model upload 
        $this->load->model('Dashboard_model'); //load model upload 
        $this->load->library('upload'); //load library upload 
    }

    public function index(){

        if ($this->session->userdata('front_logged_in')) {
        $session_data = $this->session->userdata('front_logged_in');
        $data['user_id'] = $session_data['user_id'];
        $data['user_type_id'] = $session_data['user_type_id'];
        $data['user_name'] = $session_data['user_name'];
		$type1 = $session_data['user_type_id'];
		
		$brand_access=$session_data['brand_access'];

        $account_id='';
        $search_key='';
        $data['dashboradinfo']=$this->Dashboard_model->briefListDetails();
		$data['brandsinfo'] = $this->Dashboard_model->getBrandtlist();

		$data['briefinreview']= $this->Dashboard_model->getbriefinreview($account_id,$brand_access,$search_key);
		$data['briefrejected']= $this->Dashboard_model->getbriefrejected($account_id,$brand_access,$search_key);
		$data['workinprogress']= $this->Dashboard_model->getworkinprogress($account_id,$brand_access,$search_key);
		$data['proffing_pending']= $this->Dashboard_model->getproffing_pending($account_id,$brand_access,$search_key);
		$data['revision_work']= $this->Dashboard_model->getrevision_work($account_id,$brand_access,$search_key);
		$data['work_complete']= $this->Dashboard_model->getwork_complete($account_id,$brand_access,$search_key);
		$data['feedback_pending']= $this->Dashboard_model->getfeedback_pending($account_id,$brand_access,$search_key);
        $data['title'] = "upload images";
        $this->load->view('front/dashboard',$data);    
        }
        else
        {
        $data['user_id'] = '';
        $data['user_type_id'] = '';
        $data['user_name'] = '';
        $user_id='';
        redirect('login', 'refresh');
        }       
    }
public function getbir()
{
	//$brand_id=$_REQUEST['brand_id'];
    $brand_id = $this->input->post('brand_id');
    $account_id = $this->input->post('account_id');
    $search_key = $this->input->post('search_key');
	echo $data['briefinreview']= $this->Dashboard_model->getbriefinreview($account_id,$brand_id,$search_key);
}
public function getbr()
{
	$brand_id = $this->input->post('brand_id');
    $account_id = $this->input->post('account_id');
    $search_key = $this->input->post('search_key');
	echo $data['briefinreview']= $this->Dashboard_model->getbriefrejected($account_id,$brand_id,$search_key);
}
public function getwip()
{
	$brand_id = $this->input->post('brand_id');
    $account_id = $this->input->post('account_id');
    $search_key = $this->input->post('search_key');
	echo $data['briefinreview']= $this->Dashboard_model->getworkinprogress($account_id,$brand_id,$search_key);
}



public function getpp()
{
	$brand_id = $this->input->post('brand_id');
    $account_id = $this->input->post('account_id');
    $search_key = $this->input->post('search_key');
	echo $data['briefinreview']= $this->Dashboard_model->getproffing_pending($account_id,$brand_id,$search_key);
}


public function getrw()
{
	$brand_id = $this->input->post('brand_id');
    $account_id = $this->input->post('account_id');
    $search_key = $this->input->post('search_key');
	echo $data['briefinreview']= $this->Dashboard_model->getrevision_work($account_id,$brand_id,$search_key);
}

public function getwc()
{
	$brand_id = $this->input->post('brand_id');
    $account_id = $this->input->post('account_id');
    $search_key = $this->input->post('search_key');
	echo $data['briefinreview']= $this->Dashboard_model->getwork_complete($account_id,$brand_id,$search_key);
}

public function getfp()
{
	$brand_id = $this->input->post('brand_id');
    $account_id = $this->input->post('account_id');
    $search_key = $this->input->post('search_key');
	echo $data['briefinreview']= $this->Dashboard_model->getfeedback_pending($account_id,$brand_id,$search_key);
}





        
public function getbriefsort()
{	
$session_data = $this->session->userdata('front_logged_in');
        $data['user_id'] = $session_data['user_id'];
        $data['user_type_id'] = $session_data['user_type_id'];
        $data['user_name'] = $session_data['user_name'];
		$type1 = $session_data['user_type_id'];
		$brand_access=$session_data['brand_access'];

        

	     //if(isset($this->input->post('myCheckboxes'))) {
			$brief_status = $this->input->post('myCheckboxes');
			$brief_status =str_replace(',','-',$brief_status);
			
			//}	
            $brand_id=$this->input->post('brand_id');
            $account_id=$this->input->post('account_id');
            $search_key=$this->input->post('search_key');
   //$briefinfo=$this->Dashboard_model->briefListDetails1($brief_status,$brand_id);
  $data['dashboradinfo'] =$this->Dashboard_model->briefListDetails1($brief_status,$brand_id,$account_id,$search_key);	
   $this->load->view('front/dashboard_search',$data);  	
	
	/*$html="";
	$html.="<table id='tblbrf' class='table lead'>
								<thead>
									<tr class='table-success border-bottom'>
										<th scope='col'>ID</th>
										<th scope='col'>Title</th>
										<th scope='col'>Brief</th>
										<th scope='col'>Due Date</th>
										<th scope='col'>Status</th>
									</tr>
								</thead>
								<tbody>";
								 if(!empty($briefinfo))
						  {
							  $i=1;
							foreach($briefinfo as $keybill => $brief)
												{ 
												$brief_id=$brief['brief_id'];
												$brief_title=$brief['brief_title'];
												$brief_due_date=$brief['brief_due_date'];
												$brief_statue=$brief['status'];
												$rejected_reasons=$brief['rejected_reasons'];
													
								
									$html.="<tr class='border-bottom'>
										<th scope='row'>". $i ."</th>
										<td>".$brief_title."<br><br>";
										if($brief_statue!=4) { 
										$html.="<a class='btn btn-outline-wc' href=''>View Files</a>";
										}
										$html.="</td>";
										
									 if($brief_statue==4) { 
										$html.="<td class='border-0'><i class="fas fa-circle"></i></td>";
									 } else { 
										$html.="<td><span class='sts-ico-s'>PP</span></td>";
										 } 
										$html.="<td>". date("d M Y", strtotime($brief_due_date))."<br>".date("h:i", strtotime($brief_due_date))." </td>";
										 if($brief_statue==0) { 
										$html.="<td class='border-0'>Brief in Review<br><br>
											<select class='custom-select'>
												<option value='approve'>Approve</option>
												<option value='reject' selected>Reject</option>
											</select>
										</td>";
										 } elseif($brief_statue==1) { 
										$html.="<td>Work in Progress</td>";						
										 } elseif($brief_statue==2) { 
										$html.="<td>Work Completed</td>";	
										 } elseif($brief_statue==3) { 
										$html.="<td>Revision Work<br><br><a class='btn btn-outline-wc' href=''>Open Proofing</a></td>";
										} elseif($brief_statue==4) { 
										$html.="<td>Brief Rejected </td>";
										 } elseif($brief_statue==6) { 
										$html.="<td>Proofing Pending </td>";
										 } else { 
										$html.="<td>Feedback Pending</td>";
										 } 
									$html.="</tr>";
									 if($brief_statue==4) {
									$html.="<tr class='border-bottom'>										
										<th class='border-0' scope='row'></th>
										<td class='border-0' colspan='4'>
											<div class='d-flex justify-content-between flex-wrap'>
												<div class='border px-3 mt-3 w-50 text-justify'>
													<span class='b-text bg-white p-2'>REASON FOR REJECTION</span><br>
													<span>".$rejected_reasons."</span>
												</div>

												<div class='px-3 py-5 text-center text-wc'>
													<a class='text-wc' href='#'><span class='m-3 px-3 py-4 border rounded-circle'><i class='fas fa-download fa-2x'></i></span><br><br>Download<br>Rejected Brief</a>
												</div>";
												 if($type1==4){
												$html.="<div class='px-3 py-5 text-center text-wc'>
													<a class='text-wc; href='#'><span class='m-3 px-3 py-4 border rounded-circle'><i class='fas fa-upload fa-2x'></i></span><br><br>Re-Upload<br>Brief</a>
												</div>";
												 } 
											$html.="</div>
										</td>
									</tr>";
									
												 } 
									
						  $i++;
						} 
						
						} 
								$html.="</tbody></table>";
		echo $html;*/
}








 function getBrandDropDown()
    {

    $session_data = $this->session->userdata('front_logged_in');
    $data['user_id'] = $session_data['user_id'];
    $data['user_type_id'] = $session_data['user_type_id'];
    $data['user_type_id'] = $session_data['user_type_id'];
    $data['user_name'] = $session_data['user_name'];
    $user_name = $session_data['user_name'];
    $user_id=$data['user_id'];
    $user_type_id = $session_data['user_type_id'];
    


    $account_id = $this->input->post('account_id');
    /*$brand_id_sel= $this->input->post('brand_id_sel');
    //echo "select * from wc_users where  user_id='$account_id' ";
    if($account_id!='')
    {
        $checkquerys = $this->db->query("select * from wc_users where  user_id='$account_id' ")->result_array();
        $brand_id_str=trim($checkquerys[0]['brand_id'],",");
       // $user_type_id=$checkquerys[0]['user_type_id'];

    }
    else
    {
        $brand_id_str='';
    }*/
    

   // echo $brand_id_sel;exit;
    $sql="select * from wc_brands where status='Active' ";

    if($account_id!='')
    {
    $sql.=" and  client_id='".$account_id."' " ;
    }
   // echo $sql;

    ?>
    <select class="custom-select" name="brand_id" id="brand_id" onchange="sortbrief()">
    <option value="">Select Brand</option>
    <?php

    



    $brand_type_details=$this->db->query($sql)->result_array();
    if(!empty($brand_type_details))
    {
    foreach($brand_type_details as $key => $brandstypedetails)
    {
    $brand_id=$brandstypedetails['brand_id'];
    $brand_name=$brandstypedetails['brand_name'];

    ?>
     <option value="<?php echo $brand_id;?>"  <?php if($user_type_id=='4') { echo 'selected'; } ?>    ><?php echo ucfirst($brand_name);?></option> 
    <!-- <option value="<?php echo $brand_id;?>"        ><?php echo ucfirst($brand_name);?></option> -->
    <?php

    }
    }
    ?>

    </select>
    <?php



    





    }





    public function upload_file(){
        
         
        if(!empty($_REQUEST['folder_name']))
        {
            $dir = $_REQUEST['folder_name']."/";
        }
        else
        {
            $brief_id = $_REQUEST['brief_id'];
            $dir = "brief_upload/".$brief_id."/";
        }
        $file=$_FILES["doc_file"]["name"];
        move_uploaded_file($_FILES["doc_file"]["tmp_name"], $dir. $file);


        //$brief_id=trim($this->input->get('brief_id'));
        //echo "update wc_brief set breif_new_doc ='$file' where brief_id ='$brief_id'";
       $this->db->query("update wc_brief set breif_new_doc ='$file' where brief_id ='$brief_id'");

        echo ltrim($file);
    }

























public function checkexistsku()
{           // checking the email  exist  validation in add form
    
    if ($this->session->userdata('front_logged_in')) {
    $session_data = $this->session->userdata('front_logged_in');
    $data['user_id'] = $session_data['user_id'];
        $data['user_type_id'] = $session_data['user_type_id'];
    $data['user_name'] = $session_data['user_name'];

    $user_id=$session_data['user_id'];;
    }
    else
    {
    $data['user_id'] = '';
        $data['user_type_id'] = '';
    $data['user_name'] = '';
    $user_id='';
    }


    $txtSKUNew = $this->input->post('txtSKUNew');
    $sql = "SELECT * FROM tbl_upload_img WHERE user_id='$user_id' and zipfile_name = '$txtSKUNew' ";
    $query = $this->db->query($sql);
    if( $query->num_rows() > 0 ){
        echo 'false';
    } else {
        echo 'true';
    }
    
}   





public function uploadimage(){


        if ($this->session->userdata('front_logged_in')) {
            $session_data = $this->session->userdata('front_logged_in');
            $data['user_id'] = $session_data['user_id'];
        $data['user_type_id'] = $session_data['user_type_id'];
            $data['user_name'] = $session_data['user_name'];
            
            $id='4';
            $data['pagecontents'] = $this->Dashboard_model->get_page($id);
            $this->load->view('front/uploadimage');    
            
         
        } else {
            //echo "aaaaaa";exit;
            //If no session, redirect to login page
            //redirect('login', 'refresh');
            $session_data = $this->session->userdata('front_logged_in');
            $data['user_id'] = $session_data['user_id'];
        $data['user_type_id'] = $session_data['user_type_id'];
            $data['user_name'] = $session_data['user_name'];
            
            $id='3';
            $data['pagecontents'] = $this->Dashboard_model->get_page($id);
            $this->load->view('front/uploadimage');    
        }
            
            
            
            
        }



    public function uploadfile_old1(){

        if ($this->session->userdata('front_logged_in')) {
            $session_data = $this->session->userdata('front_logged_in');
            $data['user_id'] = $session_data['user_id'];
        $data['user_type_id'] = $session_data['user_type_id'];
            $data['user_name'] = $session_data['user_name'];
            $folderName=$data['user_id'];
            $user_id=$data['user_id'];


            
                $fileName = $_FILES["upld_zip"]["name"]; // The file name
                $fileTmpLoc = $_FILES["upld_zip"]["tmp_name"]; // File in the PHP tmp folder
                $fileType = $_FILES["upld_zip"]["type"]; // The type of file it is
                $fileSize = $_FILES["upld_zip"]["size"]; // File size in bytes
                $fileErrorMsg = $_FILES["upld_zip"]["error"]; // 0 for false... and 1 for true
                if (!$fileTmpLoc) { // if file not chosen
                echo "ERROR: Please select a file to upload.";
                exit();
                }
                
                $folder_path="zip/$folderName";
                if (!file_exists($folder_path)) {
                mkdir($folder_path, 0777, true);
                }
                if(move_uploaded_file($fileTmpLoc, "$folder_path/$fileName")){
                echo "ZIP file uploaded successfully!";

                } else {
                echo "ZIP file upload failed!";
                } 
            
         
        } else {
            //echo "aaaaaa";exit;
            //If no session, redirect to login page
            redirect('login', 'refresh');
        }
            
            
            
                
        }


      public function uploadfile_old(){

        if ($this->session->userdata('front_logged_in')) {
            $session_data = $this->session->userdata('front_logged_in');
            $data['user_id'] = $session_data['user_id'];
        $data['user_type_id'] = $session_data['user_type_id'];
            $data['user_name'] = $session_data['user_name'];
            $folderName=$data['user_id'];
            $user_id=$data['user_id'];


            
                $fileName = $_FILES["upld_zip"]["name"]; // The file name
                $fileTmpLoc = $_FILES["upld_zip"]["tmp_name"]; // File in the PHP tmp folder
                $fileType = $_FILES["upld_zip"]["type"]; // The type of file it is
                $fileSize = $_FILES["upld_zip"]["size"]; // File size in bytes
                $fileErrorMsg = $_FILES["upld_zip"]["error"]; // 0 for false... and 1 for true
                if (!$fileTmpLoc) { // if file not chosen
                echo "ERROR: Please select a file to upload.";
                exit();
                }
                
                $folder_path="zip/$folderName";
                if (!file_exists($folder_path)) {
                mkdir($folder_path, 0777, true);
                }
                if(move_uploaded_file($fileTmpLoc, "$folder_path/$fileName")){
                //$file_nm=$this->input->post('file_nm');
                    //$current_path="zip/$folderName/$file_nm";
                    $current_path="$folder_path/$fileName";
                    $unzip = new ZipArchive;
                    $out = $unzip->open($current_path);
                    if ($out === TRUE) {
                    //echo getcwd();
                    //$unzip->extractTo(getcwd());
                    $unzip->extractTo('zip/'.$folderName.'/');
                    $unzip->close();
                    //echo 'File unzipped';
                    }
                    else{
                        //echo 'File not unzipped';
                    }
                    
                 $unziped_folder=preg_replace('/.[^.]*$/', '', $current_path);
                    
                    $dir = "./".$unziped_folder."/";
                    if (is_dir($unziped_folder)) {
    if ($dh = opendir($unziped_folder)) {
        $i=0;
        while (($file = readdir($dh)) !== false) {
            //if($i>1)
            //{
            //echo $file;
            $file_image=pathinfo($file, PATHINFO_FILENAME);
            //////////////////////////Convert JPG start//////////////////////////////////////
            $file=$dir.$file;
            if(is_dir($file)) {
              //echo ("$file is a directory");
            } else {
            $file=$this->convertToJpeg($dir,$file,$file_image);
            }
            //////////////////////////Convert JPG end//////////////////////////////////////
            }
       // $i++;
        //}
        closedir($dh);
    }
    if (!is_dir("./".$unziped_folder."/thumb/")) {
        @mkdir("./".$unziped_folder."/thumb/", 0777, true);
        }
    
    if ($dh = opendir($unziped_folder)) {
        $i=0;
        while (($file = readdir($dh)) !== false) {
            //if($i>1)
            //{
           //echo $file;
            $file_image=pathinfo($file, PATHINFO_FILENAME);
            //////////////////////////////////////CREATE TEMP THUMB///////////////////////////////////////
            $file1=$dir.$file;
            if(is_dir($file1)) {
              //echo ("$file is a directory");
            } else {
        $new_thumb = "./".$unziped_folder."/thumb/"."temp_thumb_".$file_image.".jpg";
        $file_pat = "./".$unziped_folder."/".$file;
        $this->convertToThumb($new_thumb,$user_id,$file_pat);
        //////////////////////////////////////////////////////////////////////////////////// 
            
            }
            
            }
        //$i++;
        //}
        closedir($dh);
    }
        
    
    
    
}
                
                    
                    
                echo $fileName."@@@@@ZIP file uploaded successfully!";

                } else {
                echo "ZIP file upload failed!";
                }  
            
         
        } else {
            //echo "aaaaaa";exit;
            //If no session, redirect to login page
            redirect('login', 'refresh');
        }
            
            
            
                
        }



public function uploadfile(){

        if ($this->session->userdata('front_logged_in')) {
            $session_data = $this->session->userdata('front_logged_in');
            $data['user_id'] = $session_data['user_id'];
        $data['user_type_id'] = $session_data['user_type_id'];
            $data['user_name'] = $session_data['user_name'];
            $folderName=$data['user_id'];
            $user_id=$data['user_id'];            
                $fileName = $_FILES["upld_zip"]["name"]; // The file name
                $fileTmpLoc = $_FILES["upld_zip"]["tmp_name"]; // File in the PHP tmp folder
                $fileType = $_FILES["upld_zip"]["type"]; // The type of file it is
                $fileSize = $_FILES["upld_zip"]["size"]; // File size in bytes
                $fileErrorMsg = $_FILES["upld_zip"]["error"]; // 0 for false... and 1 for true
                if (!$fileTmpLoc) { // if file not chosen
                echo "ERROR: Please select a file to upload.";
                exit();
                }
                $folder_path="zip/$folderName";
                if (!file_exists($folder_path)) {
                mkdir($folder_path, 0777, true);
                }
                if(move_uploaded_file($fileTmpLoc, "$folder_path/$fileName")){
                //$file_nm=$this->input->post('file_nm');
                    //$current_path="zip/$folderName/$file_nm";
                    $current_path="$folder_path/$fileName";
                    $unzip = new ZipArchive;
                    $out = $unzip->open($current_path);
                    if ($out === TRUE) {
                    //echo getcwd();
                    //$unzip->extractTo(getcwd());
                    
                    $path = 'zip/'.$folderName.'/'.substr($fileName,0,-4);
                    $unzip->extractTo($path);

                    //$unzip->extractTo('zip/'.$folderName.'/');
                    $unzip->close();
                    //echo 'File unzipped';
                    }
                    else{
                        //echo 'File not unzipped';
                    }
                    
                 $unziped_folder=preg_replace('/.[^.]*$/', '', $current_path);
                    
                    $dir = "./".$unziped_folder."/";
                    if (is_dir($unziped_folder)) {
    if ($dh = opendir($unziped_folder)) {
        while (($file = readdir($dh)) !== false) {
                        if($file!='.' && $file!='..' && $file!='thumb')
                        {
                            if (is_dir($unziped_folder."/".$file)){
                               //echo $file."  is a folder<br>";
                               if (!is_dir("./".$unziped_folder."/".$file."/thumb/")) {
                                @mkdir("./".$unziped_folder."/".$file."/thumb/", 0777, true);
                                }
                                $current_path1=$current_path."/".$file;
                                $unziped_folder1=preg_replace('/.[^.]*$/', '', $current_path1);
                                $unziped_folder1=$unziped_folder1."/".$file;
                                $dir1 = "./".$unziped_folder1."/".$file."/";
                                if ($dh1 = opendir($unziped_folder1)) {
                                    while (($file1 = readdir($dh1)) !== false) {
                                        if($file1!='.' && $file1!='..' && $file1!='thumb')
                                        {
                                        $file_image1=pathinfo($file1, PATHINFO_FILENAME);
                                        //////////////////////////////////////CREATE TEMP THUMB///////////////////////////////////////
                                        //$file1=$dir1.$file1;

                                        if(is_dir($file1)) {
                                        } else {
                                    $new_thumb1 = "./".$unziped_folder1."/thumb/"."temp_thumb_".$file_image1.".jpg";
                                     $file_pat1 = "./".$unziped_folder1."/".$file1;
                                    $this->convertToThumb($new_thumb1,$user_id,$file_pat1);
                                    ////////////////////////////////////////////////////////////////////////////////////                                    
                                        }
                                        }
                                        }
                                    closedir($dh1);
                                }   
                                else{
                                    echo "folder cant open";
                                }
                            }
                            else
                            {                               
                            //echo $file."  is a file<br>";
                            $file_image=pathinfo($file, PATHINFO_FILENAME);
                                //////////////////////////Convert JPG start//////////////////////////////////////
                                $file=$dir.$file;
                                $file=$this->convertToJpeg($dir,$file,$file_image);
                                //////////////////////////Convert JPG end//////////////////////////////////////
                                if (!is_dir("./".$unziped_folder."/thumb/")) {
                                @mkdir("./".$unziped_folder."/thumb/", 0777, true);
                                }
                                $new_thumb = "./".$unziped_folder."/thumb/"."temp_thumb_".$file_image.".jpg";
                                $file_pat = "./".$unziped_folder."/".$file;
                                $this->convertToThumb($new_thumb,$user_id,$file_pat);                   
                            }   
                        }
            }
        closedir($dh);
    }
}
               echo $fileName."@@@@@ZIP file uploaded successfully!";

                } else {
                echo "ZIP file upload failed!";
                }  
        } else {
            //echo "aaaaaa";exit;
            //If no session, redirect to login page
            redirect('login', 'refresh');
        }
            
            
            
                
        }













   public function moveunzipfile_old(){


        if ($this->session->userdata('front_logged_in')) {
        $session_data = $this->session->userdata('front_logged_in');
        $data['user_id'] = $session_data['user_id'];
        $data['user_type_id'] = $session_data['user_type_id'];
        $data['user_name'] = $session_data['user_name'];
        $user_name = $session_data['user_name'];
        $user_id=$data['user_id'];
        $folderName=$data['user_id'];

        $file_nm=$this->input->post('file_nm');
        $current_path="zip/".$user_id."/";

        $dir = "zip/".$user_id."/";
        // Open a directory, and read its contents
        if (is_dir($dir)){
        if ($dh = opendir($dir)){
        while (($file = readdir($dh)) !== false){
       
        if($file!='.' && $file!='..')
        {
           //echo "filename:" . $file . "<br>";
            $sku_folder=$file;
            
            $data = array(
            'user_id'=>$user_id,
            'zipfile_name'=>$sku_folder,
            'client_name'=>$user_name,
             );

            $this->uploadbrief_model->upload_img_insert($data);
            $upimg_id=$this->db->insert_id();

                $src = "zip/".$user_id."/".$sku_folder; 

                $dst = "upload/".$user_id."/".$sku_folder; 


                if (!file_exists($dst)) {
                mkdir($dst, 0777, true);
                }

                $this->custom_copy($src, $dst); 
                



 ///////////////////////////////////////////////////////////////////////////////////////////    

                // /$directories = glob('upload/'.$user_id.'/'.$sku_folder . '/*' , GLOB_ONLYDIR);
                
                // echo "upload/".$user_id."/".$sku_folder."/*.*";
                $directories = glob("upload/".$user_id."/".$sku_folder."/*.*", GLOB_BRACE);

                 //echo "<pre>";

               // print_r( $directories);
              


               

                foreach($directories as $file)
                {
                //echo "---".$dir."---";


                $ext = pathinfo($file, PATHINFO_EXTENSION);

                //echo $file."-----";
                $arr=explode("/",$file); 



                $dir = "./".$arr[0]."/".$arr[1]."/".$arr[2]."/";

                $img = $file;

                $file_image=pathinfo($arr[3], PATHINFO_FILENAME);

                if(strtoupper($ext)=='PNG')
                {
                //////////////////////////Convert JPG start//////////////////////////////////////

                $this->convertToJpeg($dir,$img,$file_image);
                unlink($file);
                }


                $file_image=pathinfo($arr[3], PATHINFO_FILENAME);
                $new_thumb = "./".$arr[0]."/".$arr[1]."/".$arr[2]."/"."thumb_".$file_image.".jpg";
                $file_p = "./".$arr[0]."/".$arr[1]."/".$arr[2]."/".$file_image.".jpg";

                $this->convertToThumb($new_thumb,$user_id,$file_p);







                



                }   




                //////////////////////////////////////////////////////////////////








            
        ////////////////////////////////////////////////////////////////////////////

                $dir_sub = "zip/".$user_id."/".$sku_folder."/";
                // Open a directory, and read its contents
                if (is_dir($dir_sub)){
                if ($dh_sub = opendir($dir_sub)){
                while (($file_sub = readdir($dh_sub)) !== false){

                if($file_sub!='.' && $file_sub!='..')
                {
                //echo "filename:" . $file_sub . "<br>";
                $sku_file=$file_sub;

                ////////////////////////////////////////////////////////////////////////////


                    $img_name_check=strtoupper($file_sub);

                    if(strstr($img_name_check,'FRONT'))
                    {
                    $img_ang='Front';
                    }
                    elseif(strstr($img_name_check,'BACK'))
                    {
                    $img_ang='Back';
                    }
                    elseif(strstr($img_name_check,'LEFT'))
                    {
                    $img_ang='LeftSide';
                    }
                    elseif(strstr($img_name_check,'RIGHT'))
                    {
                    $img_ang='RightSide';
                    }
                    elseif(strstr($img_name_check,'TAG'))
                    {
                    $img_ang='Tag';
                    }
                    else
                    {
                    $img_ang='';
                    }


                    $ext_item = pathinfo($file_sub, PATHINFO_EXTENSION);
                    if(strtoupper($ext_item)=='PNG')
                    {
                    $file_image_item=str_replace("png","jpg",str_replace("PNG","JPG",$file_sub));
                    }
                    else
                    {
                    $file_image_item=$file_sub;
                    }



                    $data = array(
                    //'user_id'=>$user_id,
                    'upimg_id'=>$upimg_id,
                    'img_name'=>$file_image_item,
                    'img_ang'=>$img_ang,
                    );

                    $checkquerys = $this->db->query("select id from tbl_upload_images where upimg_id='$upimg_id' and img_name='$file'  and upimg_id='$upimg_id'")->result_array();
                    if(empty($checkquerys))
                    {
                    $this->uploadbrief_model->upload_images_insert($data);
                    $id=$this->db->insert_id();
                    }
                    else
                    {
                    $id=$checkquerys[0]['id'];
                    }



                ////////////////////////////////////////////////////////////////////////////



                }
                }
                closedir($dh_sub);
                }
                } 


           $this->delete_directory($src); 

          ////////////////////////////////////////////////////////////////////////////



       


        }
        }
        closedir($dh);
        }
        }








        
                
        }

    }





public function moveunzipfile(){
        if ($this->session->userdata('front_logged_in')) {
        $session_data = $this->session->userdata('front_logged_in');
        $data['user_id'] = $session_data['user_id'];
        $data['user_type_id'] = $session_data['user_type_id'];
        $data['user_name'] = $session_data['user_name'];
        $user_name = $session_data['user_name'];
        $user_id=$data['user_id'];
        $folderName=$data['user_id'];
        $file_nm=$this->input->post('file_nm');
        $current_path="zip/".$user_id."/";
        $dir = "zip/".$user_id."/";
        // Open a directory, and read its contents
        if (is_dir($dir)){
        if ($dh = opendir($dir)){
        while (($file = readdir($dh)) !== false){     
        if($file!='.' && $file!='..')
        {
              $src = "zip/".$user_id."/".$file;
              
               if((is_dir($src))&&(count(glob("$src/*"))===0))
                {
                    //echo count(glob("$src/*"))."<br>";
                    //$this->delete_directory( $src ); 
                    
                }
            
           //echo "filename:" . $file . "<br>";
            $sku_folder=$file;
            $data = array(
            'user_id'=>$user_id,
            'zipfile_name'=>$sku_folder,
            'client_name'=>$user_name,
             );
            $this->uploadbrief_model->upload_img_insert($data);
            $upimg_id=$this->db->insert_id();
                $src = "zip/".$user_id."/".$sku_folder; 
                $dst = "upload/".$user_id."/".$sku_folder; 
                
                if (!file_exists($dst)) {
                mkdir($dst, 0777, true);
                }
                $this->custom_copy($src, $dst); 
 ///////////////////////////////////////////////////////////////////////////////////////////    

                // /$directories = glob('upload/'.$user_id.'/'.$sku_folder . '/*' , GLOB_ONLYDIR);
                
                // echo "upload/".$user_id."/".$sku_folder."/*.*";
                $directories = glob("upload/".$user_id."/".$sku_folder."/*.*", GLOB_BRACE);

                 //echo "<pre>";

               //print_r( $directories);
     

                foreach($directories as $file)
                {
                //echo "---".$dir."---";


                $ext = pathinfo($file, PATHINFO_EXTENSION);

                //echo $file."-----";
                $arr=explode("/",$file); 



                $dir = "./".$arr[0]."/".$arr[1]."/".$arr[2]."/";

                $img = $file;

                $file_image=pathinfo($arr[3], PATHINFO_FILENAME);

                if(strtoupper($ext)=='PNG')
                {
                //////////////////////////Convert JPG start//////////////////////////////////////

                $this->convertToJpeg($dir,$img,$file_image);
                unlink($file);
                }


                $file_image=pathinfo($arr[3], PATHINFO_FILENAME);
                $new_thumb = "./".$arr[0]."/".$arr[1]."/".$arr[2]."/"."thumb_".$file_image.".jpg";
                $file_p = "./".$arr[0]."/".$arr[1]."/".$arr[2]."/".$file_image.".jpg";
                $this->convertToThumb($new_thumb,$user_id,$file_p);
                }   
                ///////////////////////////////////////////////////////////////
                ////////////////////////////////////////////////////////////////////////////

                $dir_sub = "zip/".$user_id."/".$sku_folder."/";
                // Open a directory, and read its contents
                if (is_dir($dir_sub)){
                if ($dh_sub = opendir($dir_sub)){
                while (($file_sub = readdir($dh_sub)) !== false){

                if($file_sub!='.' && $file_sub!='..')
                {
                //echo "filename:" . $file_sub . "<br>";
                $sku_file=$file_sub;
                ////////////////////////////////////////////////////////////////////////////
                    $img_name_check=strtoupper($file_sub);

                    if(strstr($img_name_check,'FRONT'))
                    {
                    $img_ang='Front';
                    }
                    elseif(strstr($img_name_check,'BACK'))
                    {
                    $img_ang='Back';
                    }
                    elseif(strstr($img_name_check,'LEFT'))
                    {
                    $img_ang='LeftSide';
                    }
                    elseif(strstr($img_name_check,'RIGHT'))
                    {
                    $img_ang='RightSide';
                    }
                    elseif(strstr($img_name_check,'TAG'))
                    {
                    $img_ang='Tag';
                    }
                    else
                    {
                    $img_ang='';
                    }


                    $ext_item = pathinfo($file_sub, PATHINFO_EXTENSION);
                    if(strtoupper($ext_item)=='PNG')
                    {
                    $file_image_item=str_replace("png","jpg",str_replace("PNG","JPG",$file_sub));
                    }
                    else
                    {
                    $file_image_item=$file_sub;
                    }
                    $data = array(
                    //'user_id'=>$user_id,
                    'upimg_id'=>$upimg_id,
                    'img_name'=>$file_image_item,
                    'img_ang'=>$img_ang,
                    );

                    $checkquerys = $this->db->query("select id from tbl_upload_images where upimg_id='$upimg_id' and img_name='$file'  and upimg_id='$upimg_id'")->result_array();
                    if(empty($checkquerys))
                    {
                    $this->uploadbrief_model->upload_images_insert($data);
                    $id=$this->db->insert_id();
                    }
                    else
                    {
                    $id=$checkquerys[0]['id'];
                    }
                ////////////////////////////////////////////////////////////////////////////
                }
                }
                closedir($dh_sub);
                }
                } 


           //$this->delete_directory($src); 

          ////////////////////////////////////////////////////////////////////////////
        }
        }
        }
        closedir($dh);
        $dir = "zip/".$user_id."/";
        // Open a directory, and read its contents
        if (is_dir($dir)){
        if ($dh = opendir($dir)){
        while (($file = readdir($dh)) !== false){     
        if($file!='.' && $file!='..')
        {
            $src = "zip/".$user_id."/".$file;           
            $this->delete_directory($src); 
        }
        }
        }
        closedir($dh);
        }
        
        
        }       
        }
    }
















function custom_copy($src, $dst) {  
  
    // open the source directory 
    $dir = @opendir($src);  
  
    // Make the destination directory if not exist 
    @mkdir($dst);  
  
    // Loop through the files in source directory 
    while( $file = readdir($dir) ) {  
  
        if (( $file != '.' ) && ( $file != '..' )) {  
            if ( is_dir($src . '/' . $file) )  
            {  
  
                // Recursively calling custom copy function 
                // for sub directory  
                custom_copy($src . '/' . $file, $dst . '/' . $file);  
  
            }  
            else {  
                copy($src . '/' . $file, $dst . '/' . $file);  
            }  
        }  
    }  
  
    closedir($dir); 
} 

public function delete_directory( $dir )
{
        if( is_dir( $dir ) )
        {
        $files = glob( $dir . '*', GLOB_MARK ); //GLOB_MARK adds a slash to directories returned

        foreach( $files as $file )
        {
        $this->delete_directory( $file );      
        }

        @rmdir( $dir );
        } 
        elseif( is_file( $dir ) ) 
        {
        unlink( $dir );  
        }
}
   
    public function moveunzipfile_old1(){


        if ($this->session->userdata('front_logged_in')) {
        $session_data = $this->session->userdata('front_logged_in');
        $data['user_id'] = $session_data['user_id'];
        $data['user_type_id'] = $session_data['user_type_id'];
        $data['user_name'] = $session_data['user_name'];
        $user_name = $session_data['user_name'];
        $user_id=$data['user_id'];
        $folderName=$data['user_id'];

        $file_nm=$this->input->post('file_nm');
        $current_path="zip/$folderName/$file_nm";

        $unzip = new ZipArchive;
        $out = $unzip->open($current_path);
        if ($out === TRUE) {

        //echo getcwd();
        //$unzip->extractTo(getcwd());
        $unzip->extractTo('zip/'.$folderName.'/');
        $unzip->close();
        //echo 'File unzipped';


        //////////////////////////////////////////////////////////////////
        $directories = glob('zip/'.$folderName . '/*' , GLOB_ONLYDIR);

        //echo "<pre>";
        //print_r( $directories);exit;

        foreach($directories as $dir)
        {

        $dir_path=str_replace('zip/'.$folderName.'/', '', $dir);




        $data = array(
        'user_id'=>$user_id,
        'zipfile_name'=>$dir_path,
        'client_name'=>$user_name,

        );

        $checkquerys = $this->db->query("select upimg_id from tbl_upload_img where zipfile_name='$dir_path' and user_id='$user_id' ")->result_array();
        if(empty($checkquerys))
        {
        $this->uploadbrief_model->upload_img_insert($data);
        $upimg_id=$this->db->insert_id();
        }
        else
        {
        $upimg_id=$checkquerys[0]['upimg_id'];
        }




        $sub_path='zip/'.$folderName . '/'.$dir_path . '/';

        $files = array_diff(scandir($sub_path), array('.', '..'));

        /* echo "<pre>";
        print_r( $files);
        exit;*/



        foreach($files as $file)
        {



        $img_name_check=strtoupper($file);

        if(strstr($img_name_check,'FRONT'))
        {
        $img_ang='Front';
        }
        elseif(strstr($img_name_check,'BACK'))
        {
        $img_ang='Back';
        }
        elseif(strstr($img_name_check,'LEFT'))
        {
        $img_ang='LeftSide';
        }
        elseif(strstr($img_name_check,'RIGHT'))
        {
        $img_ang='RightSide';
        }
        elseif(strstr($img_name_check,'TAG'))
        {
        $img_ang='Tag';
        }
        else
        {
        $img_ang='';
        }


        $ext_item = pathinfo($file, PATHINFO_EXTENSION);
        if(strtoupper($ext_item)=='PNG')
        {
        $file_image_item=str_replace("png","jpg",str_replace("PNG","JPG",$file));
        }
        else
        {
        $file_image_item=$file;
        }



        $data = array(
        //'user_id'=>$user_id,
        'upimg_id'=>$upimg_id,
        'img_name'=>$file_image_item,
        'img_ang'=>$img_ang,
        );

        $checkquerys = $this->db->query("select id from tbl_upload_images where upimg_id='$upimg_id' and img_name='$file'  and upimg_id='$upimg_id'")->result_array();
        if(empty($checkquerys))
        {
        $this->uploadbrief_model->upload_images_insert($data);
        $id=$this->db->insert_id();
        }
        else
        {
        $id=$checkquerys[0]['id'];
        }






        }



        }

        ///////////////////////////////////////////////////////////////////////////////////////////
        $current_path="zip/$folderName/$file_nm";


        $folder_path_target="upload/$folderName";
        if (!file_exists($folder_path_target)) {
        mkdir($folder_path_target, 0777, true);
        }
        $target_path=$folder_path_target."/$file_nm";
        if(copy($current_path, $target_path)){
        echo "ZIP file moved successfully!";


        $unzip = new ZipArchive;
        $out = $unzip->open($target_path);
        if ($out === TRUE) {

        //echo getcwd();
        //$unzip->extractTo(getcwd());
        $unzip->extractTo('upload/'.$folderName);
        $unzip->close();
        //echo 'File unzipped';
        //unlink($current_user_path);

        unlink($current_path);
        unlink($target_path);




        $directories = glob('zip/'.$folderName . '/*' , GLOB_ONLYDIR);

        //echo "<pre>";
        //print_r( $directories);exit;

        foreach($directories as $dir)
        {
        //echo "---".$dir."---";

        $files = glob($dir.'/*'); // get all file names
        foreach($files as $file){ // iterate files
        if(is_file($file))
        unlink($file); // delete file
        }

        rmdir($dir);

        }

        rmdir('zip/'.$folderName);



        } else {
        echo 'Error';
        }





        } else {
        echo "ZIP file moved failed!";
        }






        /////////////////////////////////////////////////////////////////////////////////////////// 

        $directories = glob('upload/'.$folderName . '/*' , GLOB_ONLYDIR);


        //echo "<pre>";

        //print_r( $directories);exit;

        foreach($directories as $dir)
        {
        //echo "---".$dir."---";

        $files = glob($dir.'/*'); // get all file names
        foreach($files as $file){ // iterate files

        $ext = pathinfo($file, PATHINFO_EXTENSION);

        //echo $file."-----";
        $arr=explode("/",$file); 



        $dir = "./".$arr[0]."/".$arr[1]."/".$arr[2]."/";

        $img = $file;

        $file_image=pathinfo($arr[3], PATHINFO_FILENAME);

        if(strtoupper($ext)=='PNG')
        {



        //////////////////////////Convert JPG start//////////////////////////////////////

        $this->convertToJpeg($dir,$img,$file_image);
        unlink($file);






        }


        $file_image=pathinfo($arr[3], PATHINFO_FILENAME);
        $new_thumb = "./".$arr[0]."/".$arr[1]."/".$arr[2]."/"."thumb_".$file_image.".jpg";
        $file_p = "./".$arr[0]."/".$arr[1]."/".$arr[2]."/".$file_image.".jpg";

        $this->convertToThumb($new_thumb,$user_id,$file_p);







        }



        }   




        //////////////////////////////////////////////////////////////////
        } else {
        echo 'Error';
        }







        } else {
        //echo "aaaaaa";exit;
        //If no session, redirect to login page
        redirect('login', 'refresh');
        }

            
                
        }


public function moveunzipfileCopy(){


        if ($this->session->userdata('front_logged_in')) {
        $session_data = $this->session->userdata('front_logged_in');
        $data['user_id'] = $session_data['user_id'];
        $data['user_type_id'] = $session_data['user_type_id'];
        $data['user_name'] = $session_data['user_name'];
        $user_name = $session_data['user_name'];
        $user_id=$data['user_id'];
        $folderName=$data['user_id'];

        $file_nm=$this->input->post('file_nm');
        $current_path="zip/$folderName/$file_nm";

        $unzip = new ZipArchive;
        $out = $unzip->open($current_path);
        if ($out === TRUE) {

        //echo getcwd();
        //$unzip->extractTo(getcwd());
        $unzip->extractTo('zip/'.$folderName.'/');
        $unzip->close();
        echo 'File unzipped';


        //////////////////////////////////////////////////////////////////
        $directories = glob('zip/'.$folderName . '/*' , GLOB_ONLYDIR);

        //echo "<pre>";
        //print_r( $directories);exit;

        foreach($directories as $dir)
        {

        $dir_path=str_replace('zip/'.$folderName.'/', '', $dir);


        $data = array(
        'user_id'=>$user_id,
        'zipfile_name'=>$dir_path,
        'client_name'=>$user_name,

        );

        $checkquerys = $this->db->query("select upimg_id from tbl_upload_img where zipfile_name='$dir_path' and user_id='$user_id' ")->result_array();
        if(empty($checkquerys))
        {
        $this->uploadbrief_model->upload_img_insert($data);
        $upimg_id=$this->db->insert_id();
        }
        else
        {
        $upimg_id=$checkquerys[0]['upimg_id'];
        }




        $sub_path='zip/'.$folderName . '/'.$dir_path . '/';

        $files = array_diff(scandir($sub_path), array('.', '..'));

        /* echo "<pre>";
        print_r( $files);
        exit;*/



        foreach($files as $file)
        {



        $img_name_check=strtoupper($file);

        if(strstr($img_name_check,'FRONT'))
        {
        $img_ang='Front';
        }
        elseif(strstr($img_name_check,'BACK'))
        {
        $img_ang='Back';
        }
        elseif(strstr($img_name_check,'LEFT'))
        {
        $img_ang='LeftSide';
        }
        elseif(strstr($img_name_check,'RIGHT'))
        {
        $img_ang='RightSide';
        }
        elseif(strstr($img_name_check,'TAG'))
        {
        $img_ang='Tag';
        }
        else
        {
        $img_ang='';
        }


        $ext_item = pathinfo($file, PATHINFO_EXTENSION);
        if(strtoupper($ext_item)=='PNG')
        {
        $file_image_item=str_replace("png","jpg",str_replace("PNG","JPG",$file));
        }
        else
        {
        $file_image_item=$file;
        }



        $data = array(
        //'user_id'=>$user_id,
        'upimg_id'=>$upimg_id,
        'img_name'=>$file_image_item,
        'img_ang'=>$img_ang,
        );

        $checkquerys = $this->db->query("select id from tbl_upload_images where upimg_id='$upimg_id' and img_name='$file'  and upimg_id='$upimg_id'")->result_array();
        if(empty($checkquerys))
        {
        $this->uploadbrief_model->upload_images_insert($data);
        $id=$this->db->insert_id();
        }
        else
        {
        $id=$checkquerys[0]['id'];
        }






        }



        }

        ///////////////////////////////////////////////////////////////////////////////////////////
        $current_path="zip/$folderName/$file_nm";


        $folder_path_target="upload/$folderName";
        if (!file_exists($folder_path_target)) {
        mkdir($folder_path_target, 0777, true);
        }
        $target_path=$folder_path_target."/$file_nm";
        if(copy($current_path, $target_path)){
        echo "ZIP file moved successfully!";


        $unzip = new ZipArchive;
        $out = $unzip->open($target_path);
        if ($out === TRUE) {

        //echo getcwd();
        //$unzip->extractTo(getcwd());
        $unzip->extractTo('upload/'.$folderName);
        $unzip->close();
        //echo 'File unzipped';
        //unlink($current_user_path);

        unlink($current_path);
        unlink($target_path);




        $directories = glob('zip/'.$folderName . '/*' , GLOB_ONLYDIR);

        //echo "<pre>";
        //print_r( $directories);exit;

        foreach($directories as $dir)
        {
        //echo "---".$dir."---";

        $files = glob($dir.'/*'); // get all file names
        foreach($files as $file){ // iterate files
        if(is_file($file))
        unlink($file); // delete file
        }

        rmdir($dir);

        }

        rmdir('zip/'.$folderName);



        } else {
        echo 'Error';
        }





        } else {
        echo "ZIP file moved failed!";
        }






        /////////////////////////////////////////////////////////////////////////////////////////// 

        $directories = glob('upload/'.$folderName . '/*' , GLOB_ONLYDIR);


        //echo "<pre>";

        //print_r( $directories);exit;

        foreach($directories as $dir)
        {
        //echo "---".$dir."---";

        $files = glob($dir.'/*'); // get all file names
        foreach($files as $file){ // iterate files

        $ext = pathinfo($file, PATHINFO_EXTENSION);

        //echo $file."-----";
        $arr=explode("/",$file); 



        $dir = "./".$arr[0]."/".$arr[1]."/".$arr[2]."/";

        $img = $file;

        $file_image=pathinfo($arr[3], PATHINFO_FILENAME);

        if(strtoupper($ext)=='PNG')
        {



        //////////////////////////Convert JPG start//////////////////////////////////////

        $this->convertToJpeg($dir,$img,$file_image);
        unlink($file);






        }


        $file_image=pathinfo($arr[3], PATHINFO_FILENAME);
        $new_thumb = "./".$arr[0]."/".$arr[1]."/".$arr[2]."/"."thumb_".$file_image.".jpg";
        $file_p = "./".$arr[0]."/".$arr[1]."/".$arr[2]."/".$file_image.".jpg";

        $this->convertToThumb($new_thumb,$user_id,$file_p);







        }



        } 




        //////////////////////////////////////////////////////////////////
        } else {
        echo 'Error';
        }







        } else {
        //echo "aaaaaa";exit;
        //If no session, redirect to login page
        redirect('login', 'refresh');
        }

      
        
    }




    
    
    function openZIPFolder_old()
    {
    
    $str='';
    $path = urldecode( $this->input->post('path'));
    $foldername= urldecode( $this->input->post('foldername'));
    
    $skufolderfile_nm_field= trim(urldecode( $this->input->post('skufolderfile_nm')),",");
    $skufolderfile_nm_arr=explode(",",$skufolderfile_nm_field);

    if( file_exists( $path)) {
    if( $path[ strlen( $path ) - 1 ] ==  '/' )
    $folder = $path;
    else
    $folder = $path . '/';

    $path=$path.'/'.$foldername;
    $dir = opendir( $path );
    $i = 0;
    while(( $file = readdir( $dir ) ) != false )
    {
        $imgid = $foldername.'_'.$i;
    //echo $file;
    if($file!='.' && $file!='..' && $file!='thumb')
    {
     
     $files[] = $file;

      //$inside_path=

     $str.='<div class="col-12 col-sm-12 col-md-4 col-lg-4 col-xl-4">
            <script type="text/javascript">
            var img = document.getElementById("img_'.$imgid.'");
            var width = img.naturalWidth;
            var height = img.naturalHeight;
            // console.log(width);
            // console.log(height);

            if (width > height){
                $("#rtImg_'.$imgid.'").removeClass("d-none");
            } else {
                $("#orgImg_'.$imgid.'").removeClass("d-none");
            }
            </script>
            <div id="orgImg_'.$imgid.'" class="w-100 d-none">
                <img id="img_'.$imgid.'" class="w-100 mb-3" src="'.$path.'/thumb/temp_thumb_'.$file.'" alt="'.$file.'">
            </div>

            <div id="rtImg_'.$imgid.'" class="img-box d-none">
                <img id="img_'.$imgid.'" class="img mb-3" src="'.$path.'/thumb/temp_thumb_'.$file.'" alt="'.$file.'">
            </div>
            </div>';


    //$str.=' <div class="col-12 col-sm-12 col-md-4 col-lg-4 col-xl-4"> <img class="w-100 mb-3" src="'.$path.'/'.$file.'"> </div>';
     }
     $i++;
    }

    echo $str;
     //echo "<pre>";
    // print_r($files);
    //return $files;
    closedir( $dir );
    }


    }


    function openZIPFolder()
    {
    
    $str='';
    $path = urldecode( $this->input->post('path'));
    $foldername= urldecode( $this->input->post('foldername'));
    
    $skufolderfile_nm_field= trim(urldecode( $this->input->post('skufolderfile_nm')),",");
    $skufolderfile_nm_arr=explode(",",$skufolderfile_nm_field);

    if( file_exists( $path)) {
    if( $path[ strlen( $path ) - 1 ] ==  '/' )
    $folder = $path;
    else
    $folder = $path . '/';

    $path=$path.'/'.$foldername;
    $dir = opendir( $path );
    $i = 0;
    while(( $file = readdir( $dir ) ) != false )
    {
        $imgid = $foldername.'_'.$i;
    //echo $file;
        
    if($file!='.' && $file!='..' && $file!='thumb')
    {
    
    if (is_dir($path."/".$file)){
            //echo $file."  is a folder<hr><br>";
            
                $path1=$path.'/'.$file;
                $dir1 = opendir( $path1 );
                $j = 0;
                while(( $file1 = readdir( $dir1 ) ) != false )
                {
                    $imgid1 = $foldername.'_'.$i.'_'.$j;
                //echo $file;
                    
                if($file1!='.' && $file1!='..' && $file1!='thumb')
                {
                   //echo $file1;
                           $files1[] = $file1;

                              //$inside_path=

                             $str.='<div class="col-12 col-sm-12 col-md-4 col-lg-4 col-xl-4">
                                    <script type="text/javascript">
                                    var img = document.getElementById("img_'.$imgid1.'");
                                    var width = img.naturalWidth;
                                    var height = img.naturalHeight;
                                    // console.log(width);
                                    // console.log(height);

                                    if (width > height){
                                        $("#rtImg_'.$imgid1.'").removeClass("d-none");
                                    } else {
                                        $("#orgImg_'.$imgid1.'").removeClass("d-none");
                                    }
                                    </script>
                                    <div id="orgImg_'.$imgid1.'" class="w-100 d-none">
                                        <img id="img_'.$imgid1.'" class="w-100 mb-3" src="'.$path1.'/thumb/temp_thumb_'.$file1.'" alt="'.$file1.'">
                                    </div>

                                    <div id="rtImg_'.$imgid1.'" class="img-box d-none">
                                        <img id="img_'.$imgid1.'" class="img mb-3" src="'.$path1.'/thumb/temp_thumb_'.$file1.'" alt="'.$file1.'">
                                    </div>
                                    </div>';

                   
                   
                   
                }
                $j++;
                }
            
        }
    else    
    {
     $files[] = $file;

      //$inside_path=

     $str.='<div class="col-12 col-sm-12 col-md-4 col-lg-4 col-xl-4">
            <script type="text/javascript">
            var img = document.getElementById("img_'.$imgid.'");
            var width = img.naturalWidth;
            var height = img.naturalHeight;
            // console.log(width);
            // console.log(height);

            if (width > height){
                $("#rtImg_'.$imgid.'").removeClass("d-none");
            } else {
                $("#orgImg_'.$imgid.'").removeClass("d-none");
            }
            </script>
            <div id="orgImg_'.$imgid.'" class="w-100 d-none">
                <img id="img_'.$imgid.'" class="w-100 mb-3" src="'.$path.'/thumb/temp_thumb_'.$file.'" alt="'.$file.'">
            </div>

            <div id="rtImg_'.$imgid.'" class="img-box d-none">
                <img id="img_'.$imgid.'" class="img mb-3" src="'.$path.'/thumb/temp_thumb_'.$file.'" alt="'.$file.'">
            </div>
            </div>';


    //$str.=' <div class="col-12 col-sm-12 col-md-4 col-lg-4 col-xl-4"> <img class="w-100 mb-3" src="'.$path.'/'.$file.'"> </div>';
     }
    }
     $i++;
    }

    echo $str;
     //echo "<pre>";
    // print_r($files);
    //return $files;
    closedir( $dir );
    }


    }




    function removeZIPFolder_old()
    {
    
    $str='';
    $path = urldecode( $this->input->post('path'));
    $foldername= urldecode( $this->input->post('foldername')).".zip";
    $zipfile= $path."/".$foldername;
    $skufolderfile_nm_field= trim(urldecode( $this->input->post('skufolderfile_nm')),",");
    $skufolderfile_nm_arr=explode(",",$skufolderfile_nm_field);

    if( file_exists($zipfile)) {
    if(is_file($zipfile)){
        //Use the unlink function to delete the file.
        //unlink($zipfile);
        if(unlink($zipfile))
        {
          echo ("$foldername successfully removed");
        }
        else
        {
          echo ("$foldername couldn't be removed"); 
        }
    }
    }
    
    $str='';
    $path = urldecode( $this->input->post('path'));
    $foldername= urldecode( $this->input->post('foldername'));
    //echo $path."/".$foldername;
    $skufolderfile_nm_field= trim(urldecode( $this->input->post('skufolderfile_nm')),",");
    $skufolderfile_nm_arr=explode(",",$skufolderfile_nm_field);

    

    $thumbpath = $path."/".$foldername."/thumb";
    $this->removeThumbSKUFolder($thumbpath);


    $path=$path."/".$foldername;
    if( file_exists($path)) {
        
        
        $files = glob($path . '/*');
 
//Loop through the file list.
foreach($files as $file){
    //Make sure that this is a file and not a directory.
    if(is_file($file)){
        //Use the unlink function to delete the file.
        unlink($file);
    }
}
        
        if(rmdir($path))
        {
          //echo ("$foldername successfully removed");
        }
        else
        {
          //echo ("$foldername couldn't be removed"); 
        }
        
    }
    else
    {
        echo "error";
    }
    
    
    

    }
    
 function removeZIPFolder()
    {
    
    $str='';
    $path = urldecode( $this->input->post('path'));
    $foldername= urldecode( $this->input->post('foldername')).".zip";
    $foldername1= urldecode( $this->input->post('foldername'));
    $folpath=$path."/".$foldername1;
    $zipfile= $path."/".$foldername;
    $skufolderfile_nm_field= trim(urldecode( $this->input->post('skufolderfile_nm')),",");
    $skufolderfile_nm_arr=explode(",",$skufolderfile_nm_field);

    if( file_exists($zipfile)) {
    if(is_file($zipfile)){
        //Use the unlink function to delete the file.
        //unlink($zipfile);
        if(unlink($zipfile))
        {
          echo ("$foldername successfully removed");
        }
        else
        {
          echo ("$foldername couldn't be removed"); 
        }
    }
    }
    
    if (is_dir($folpath)) {
        $thumbpath = $folpath."/thumb";
        if(is_dir($thumbpath)) {
        $this->removeThumbSKUFolder($thumbpath);
        }
    $objects = scandir($folpath);
    foreach ($objects as $object) {
      if ($object != "." && $object != "..") {
        if (filetype($folpath."/".$object) == "dir")
        {
            $thumbpath1 = $folpath."/".$object."/thumb";
            if(is_dir($thumbpath1)) {
            $this->removeThumbSKUFolder($thumbpath1);
            }
            $folpath1=$folpath."/".$object;
           $objects1 = scandir($folpath1);
           foreach ($objects1 as $object1) {
                if ($object1 != "." && $object1 != "..") {
                    if (filetype($folpath1."/".$object1) == "dir")
                    {
                        rmdir($folpath1."/".$object1);
                    }
                    else{
                        unlink   ($folpath1."/".$object1);
                    }
                    
                }
                    reset($objects1);
                    if(is_dir($folpath."/".$object)) {
                    @rmdir($folpath."/".$object); 
                    }
      }
    }
    else{
        unlink($folpath."/".$object);
    }
    reset($objects);
    @rmdir($folpath);
  }
    
    
    }
    }
    /*
    $str='';
    $path = urldecode( $this->input->post('path'));
    $foldername= urldecode( $this->input->post('foldername'));
    //echo $path."/".$foldername;
    $skufolderfile_nm_field= trim(urldecode( $this->input->post('skufolderfile_nm')),",");
    $skufolderfile_nm_arr=explode(",",$skufolderfile_nm_field);

    

    $thumbpath = $path."/".$foldername."/thumb";
    $this->removeThumbSKUFolder($thumbpath);


    $path=$path."/".$foldername;
    if( file_exists($path)) {
        
        
        $files = glob($path . '/*');
 
//Loop through the file list.
foreach($files as $file){
    //Make sure that this is a file and not a directory.
    if(is_file($file)){
        //Use the unlink function to delete the file.
        unlink($file);
    }
}   
        if(rmdir($path))
        {
          //echo ("$foldername successfully removed");
        }
        else
        {
          //echo ("$foldername couldn't be removed"); 
        }
        
    }
    else
    {
        echo "error";
    }
    
    */
    
    
    }


public function convertToThumb($new_thumb,$user_id,$file_path) {

  if(!file_exists($new_thumb)){
  $width = "400";
  $height = "600";
  $quality = 90;
  $img = $file_path;

  //Generate Thumbnail Images

  $file = $img;
  $dest = $new_thumb;
  $height = 600;
  $width = 400;
  $output_format = "jpg";
  $output_quality = 90;
  $bg_color = array(255, 255, 255);


  //Justify the Image format and create a GD resource
  /*$image_info = getimagesize($file);
  list($cur_width, $cur_height, $cur_type, $cur_attr) = getimagesize($file);*/
   $image_info = @getimagesize($file);
  list($cur_width, $cur_height, $cur_type, $cur_attr) = @getimagesize($file);




  $image_type = $image_info[2];
  switch($image_type){
    case IMAGETYPE_JPEG:
    $image = imagecreatefromjpeg($file);
    break;
    case IMAGETYPE_GIF:
    $image = imagecreatefromgif($file);
    break;
    case IMAGETYPE_GIF:
    $image = imagecreatefrompng($file);
    break;
    default:
    die("Image Format not Suppoted");
  }

 if($cur_width>$cur_height){
  $degrees = -90;
  $image = imagerotate($image, $degrees, 0);
  }

  $image_width = imagesx($image);
  $image_height = imagesy($image);




  //echo $file;

  //Get The Calculations
  // Calculate the size of the Image
  //If Image width is bigger than the Thumbnail Width
  if($image_width>$image_height){





//echo "hoko";
//echo $image_width.">".$image_height;

        //Set Image Width to Thumbnail Width
    $new["width"] = $width;
        //Calculate Height according to width
    $new["height"] = ($new["width"]/$image_width)*$image_height;

        //If Resulting height is bigger than the thumbnail Height
    if($new["height"]>$height){

            //Set the image Height to THUmbnail Height
      $new["height"] = $height;
            //Recalculate width according to height of the thumbnail
      $new["width"] = ($new["height"]/$image_height)*$image_width;

    }

  }else{
//echo "moko";
    $new["height"] = $height;
    $new["width"] = ($new["height"]/$image_height)*$image_width;

    if($new["width"]>$width){

      $new["width"] = $width;
      $new["height"] = ($new["width"]/$image_width)*$image_height;

    }

  }

    //Calculate the image position based on the difference between the dimensons of the new image and thumbnail
  $x = ($width-$new["width"])/2;
  $y = ($height-$new["height"])/2;

  $calc =  array_merge($new, array("x"=>$x,"y"=>$y));


  // End Calculate The Image



  //Create an Empty image
  $canvas = imagecreatetruecolor($width, $height);

    //Load Background color
  $color = imagecolorallocate($canvas,
    $bg_color[0],
    $bg_color[1],
    $bg_color[2]
  );

    //FIll the Image with the Background color
  imagefilledrectangle(
    $canvas,
    0,
    0,
    $width,
    $height,
    $color
  );

    //The REAL Magic
  imagecopyresampled(
    $canvas,
    $image,
    $calc["x"],
    $calc["y"],
    0,
    0,
    $calc["width"],
    $calc["height"],
    $image_width,
    $image_height
  );

// Create Output Image

  $image = $canvas;
  $format = $output_format;
  $quality = $output_quality;



  switch($format){
    case "jpg":
    imagejpeg($image, $dest, $quality);
    break;
    case "gif":
    imagegif($image, $dest);
    break;
    case "png":
            //Png Quality is measured from 1 to 9
    imagepng($image, $dest, round(($quality/100)*9) );
    break;
  }

  //unlink($img);

}
else{
  // echo "file is exist.";
}




}

public function convertToJpeg($dir,$img,$file_image) {

        $dst = $dir . $file_image;

        if (($img_info = getimagesize($img)) === FALSE)
        die("Image not found or not an image");

        $width = $img_info[0];
        $height = $img_info[1];

        switch ($img_info[2]) {
        case IMAGETYPE_GIF  : $src = imagecreatefromgif($img);  break;
        case IMAGETYPE_JPEG : $src = imagecreatefromjpeg($img); break;
        case IMAGETYPE_PNG  : $src = imagecreatefrompng($img);  break;
        default : die("Unknown filetype");
        }

        $tmp = imagecreatetruecolor($width, $height);

        $file =$dst.".jpg";

        imagecopyresampled($tmp, $src, 0, 0, 0, 0, $width, $height, $width, $height);
        imagejpeg($tmp, $file);
        return $file_image.".jpg";

}



    public function insertSKUFromSidebar(){

     if ($this->session->userdata('front_logged_in')) {
    $session_data = $this->session->userdata('front_logged_in');
    $data['user_id'] = $session_data['user_id'];
        $data['user_type_id'] = $session_data['user_type_id'];
    $data['user_name'] = $session_data['user_name'];

    $user_id=$session_data['user_id'];
    $user_name=$session_data['user_name'];
    }
    else
    {
    $data['user_id'] = '';
        $data['user_type_id'] = '';
    $data['user_name'] = '';
    $user_id='';
    $user_name='';
    }

    $txtSKUNew=$this->input->post('txtSKUNew');
    $pimg=$this->input->post('pimg');


    $data = array(
    'user_id'=>$user_id,
    'zipfile_name'=>$txtSKUNew,
    'client_name'=>$user_name,

    );

    $checkquerys = $this->db->query("select upimg_id from tbl_upload_img where zipfile_name='$txtSKUNew' and user_id='$user_id' ")->result_array();
    if(empty($checkquerys))
    {
    $this->uploadbrief_model->upload_img_insert($data);
    $upimg_id=$this->db->insert_id();
    }
    else
    {
    $upimg_id=$checkquerys[0]['upimg_id'];
    }



    $allImage=trim($pimg,",");
    $allImageArr=explode(",",$allImage);

    foreach($allImageArr as $key => $file)
    {

        if (!is_dir("./upload/".$user_id."/".$txtSKUNew)) {
       mkdir("./upload/".$user_id."/".$txtSKUNew, 0777, true);
       }

        $source_path="./temp_upload/".$user_id."/".$file;
        $destination_path="./upload/".$user_id."/".$txtSKUNew."/".$file;
        copy($source_path, $destination_path);
        unlink($source_path);

        $thumb_source_path="./temp_upload/".$user_id."/thumb_".$file;
        $thumb_destination_path="./upload/".$user_id."/".$txtSKUNew."/thumb_".$file;
        copy($thumb_source_path, $thumb_destination_path);
        unlink($thumb_source_path);


            $img_name_check=strtoupper($file);

            if(strstr($img_name_check,'FRONT'))
            {
            $img_ang='Front';
            }
            elseif(strstr($img_name_check,'BACK'))
            {
            $img_ang='Back';
            }
            elseif(strstr($img_name_check,'LEFT'))
            {
            $img_ang='LeftSide';
            }
            elseif(strstr($img_name_check,'RIGHT'))
            {
            $img_ang='RightSide';
            }
            elseif(strstr($img_name_check,'TAG'))
            {
            $img_ang='Tag';
            }
            else
            {
            $img_ang='';
            }

        $data = array(
            //'user_id'=>$user_id,
          'upimg_id'=>$upimg_id,
          'img_name'=>$file,
          'img_ang'=>$img_ang,
          );

                   $checkquerys = $this->db->query("select id from tbl_upload_images where upimg_id='$upimg_id' and img_name='$file'  and upimg_id='$upimg_id'")->result_array();
                    if(empty($checkquerys))
                    {
                    $this->uploadbrief_model->upload_images_insert($data);
                    $id=$this->db->insert_id();
                    }
                    else
                    {
                     $id=$checkquerys[0]['id'];
                    }
    }


   echo "success";

  }










































public function create()
    {
        $file_name = $this->input->post('file_nm');     
        $uid = $this->input->post('uid');

        $data = array(
            'user_id'   => $uid,
            'zipfile_name'  => $file_name,              
        );
        $this->load->model('Upload_image');
        $insert = $this->upload_image->createData($data);
        echo json_encode($insert);
    }




    public function uploadSKUFolder(){

      
      if ($this->session->userdata('front_logged_in')) {
            $session_data = $this->session->userdata('front_logged_in');
            $data['user_id'] = $session_data['user_id'];
        $data['user_type_id'] = $session_data['user_type_id'];
            $data['user_name'] = $session_data['user_name'];
            $user_id=$data['user_id'];
            $folderName=$data['user_id'];

            $upld_skufolder_name_path=$_POST['upld_skufolder_name'];

            $upld_skufolder_name_path_arr=explode("/",$upld_skufolder_name_path);

            $path_count=count($upld_skufolder_name_path_arr);

            if($path_count==3)
            {
              $upld_skufolder_name=$upld_skufolder_name_path_arr[1];
            }
            elseif($path_count==2)
            {
              $upld_skufolder_name=$upld_skufolder_name_path_arr[0];
            }
            //echo $path_count;exit;

            //echo "<pre>";
            //print_r($_FILES);exit;



            $fileName = $_FILES["upld_skufolder"]["name"]; // The file name
            $fileTmpLoc = $_FILES["upld_skufolder"]["tmp_name"]; // File in the PHP tmp folder
            $fileType = $_FILES["upld_skufolder"]["type"]; // The type of file it is
            $fileSize = $_FILES["upld_skufolder"]["size"]; // File size in bytes
            $fileErrorMsg = $_FILES["upld_skufolder"]["error"]; // 0 for false... and 1 for true

            //echo $fileType;
            //exit;
            $ext = strtoupper(pathinfo($fileName, PATHINFO_EXTENSION));

            // if($fileType!='image/jpeg')
            // {
            //  echo "not_image";   
            //  exit;
            // }

            if($ext=='JPG' || $ext=='JPEG' || $ext=='PNG') { }
            else { echo "not_image"; exit; }

            if (!$fileTmpLoc) { // if file not chosen
            echo "ERROR: Please select a file to upload.";
            exit();
            }

            $folder_path="temp_folder_upload/".$user_id."/".$upld_skufolder_name."/";
            if (!file_exists($folder_path)) {
            @mkdir($folder_path, 0777, true);
            }


          // if($_FILES["upld_skufolder"]["type"]=='image/jpeg'|| $_FILES["upld_skufolder"]["type"]=='image/jpg')
            if($ext=='JPG' || $ext=='JPEG' )
          {
          $file=$_FILES["upld_skufolder"]["name"];
          move_uploaded_file($fileTmpLoc, $folder_path."/".$fileName);
          }
          else
          {

            $dir = "./temp_folder_upload/".$user_id."/".$upld_skufolder_name."/";

            $img = $_FILES['upld_skufolder']['tmp_name'];

            $file_image=pathinfo($_FILES["upld_skufolder"]["name"], PATHINFO_FILENAME);


            //////////////////////////Convert JPG start//////////////////////////////////////

            $file=$this->convertToJpeg($dir,$img,$file_image);

            //////////////////////////Convert JPG end//////////////////////////////////////

          }


        //////////////////////////////////////CREATE TEMP THUMB///////////////////////////////////////

        if (!is_dir("./temp_folder_upload/".$user_id."/".$upld_skufolder_name."/thumb/")) {
        @mkdir("./temp_folder_upload/".$user_id."/".$upld_skufolder_name."/thumb/", 0777, true);
        }
  
        $file_image=pathinfo($_FILES["upld_skufolder"]["name"], PATHINFO_FILENAME);
        $new_thumb = "./temp_folder_upload/".$user_id."/".$upld_skufolder_name."/thumb/"."temp_thumb_".$file_image.".jpg";

        $file_pat = "./temp_folder_upload/".$user_id."/".$upld_skufolder_name."/".$file;
        $this->convertToThumb($new_thumb,$user_id,$file_pat);
        ////////////////////////////////////////////////////////////////////////////////////  
           

            echo $upld_skufolder_name."@@@@@Folder uploaded successfully!";



            } else {
            //echo "aaaaaa";exit;
            //If no session, redirect to login page
            redirect('login', 'refresh');
            }


        
    }





  public function moveSkuFolderUpload(){



  if ($this->session->userdata('front_logged_in')) {
  $session_data = $this->session->userdata('front_logged_in');
  $data['user_id'] = $session_data['user_id'];
        $data['user_type_id'] = $session_data['user_type_id'];
  $data['user_name'] = $session_data['user_name'];
  $user_name = $session_data['user_name'];
  $user_id=$data['user_id'];
  $folderName=$data['user_id'];

   $skufolderfile_nm=$this->input->post('skufolderfile_nm');
  $skufolderfile_nm_arr=explode(",",$skufolderfile_nm);

//echo "<pre>";
//print_r($skufolderfile_nm_arr);
  if(!empty($skufolderfile_nm_arr))
  {
    foreach($skufolderfile_nm_arr as $value)
    {
      if($value!='')
      {

      $skufolderfile_nm=$value;


       $file_pat = "temp_folder_upload/".$user_id."/".$skufolderfile_nm."/thumb";
        $this->removeThumbSKUFolder($file_pat);




      $target_path="temp_folder_upload/".$user_id."/".$skufolderfile_nm."/";

      $destination_path="upload/".$user_id."/".$skufolderfile_nm."/";




      $data = array(
      'user_id'=>$user_id,
      'zipfile_name'=>$skufolderfile_nm,
      'client_name'=>$user_name,

      );

      $checkquerys = $this->db->query("select upimg_id from tbl_upload_img where zipfile_name='$skufolderfile_nm' and user_id='$user_id' ")->result_array();
      if(empty($checkquerys))
      {
      $this->uploadbrief_model->upload_img_insert($data);
      $upimg_id=$this->db->insert_id();
      }
      else
      {
      $upimg_id=$checkquerys[0]['upimg_id'];
      }






        if (!file_exists($destination_path)) {
        mkdir($destination_path, 0777, true);
        }


        // Open a directory, and read its contents
        if (is_dir($target_path)){
        if ($dh = opendir($target_path)){
        while (($file = readdir($dh)) !== false){
        echo "filename:" . $file . "<br>";
       /* if($file!='.' && $file!='..')
        {
        copy($target_path.$file, $destination_path.$file);
        unlink($target_path.$file);



        $file_image=pathinfo($file, PATHINFO_FILENAME);
        $new_thumb = "./upload/".$user_id."/".$skufolderfile_nm."/"."thumb_".$file_image.".jpg";

        $file_pat = "./upload/".$user_id."/".$skufolderfile_nm."/".$file;
        $this->convertToThumb($new_thumb,$user_id,$file_pat);




        $img_name_check=strtoupper($file);

        if(strstr($img_name_check,'FRONT'))
        {
        $img_ang='Front';
        }
        elseif(strstr($img_name_check,'BACK'))
        {
        $img_ang='Back';
        }
        elseif(strstr($img_name_check,'LEFT'))
        {
        $img_ang='LeftSide';
        }
        elseif(strstr($img_name_check,'RIGHT'))
        {
        $img_ang='RightSide';
        }
        elseif(strstr($img_name_check,'TAG'))
        {
        $img_ang='Tag';
        }
        else
        {
        $img_ang='';
        }


        $ext_item = pathinfo($file, PATHINFO_EXTENSION);
        if(strtoupper($ext_item)=='PNG')
        {
        $file_image_item=str_replace("png","jpg",str_replace("PNG","JPG",$file));
        }
        else
        {
        $file_image_item=$file;
        }



        $data = array(
        //'user_id'=>$user_id,
        'upimg_id'=>$upimg_id,
        'img_name'=>$file_image_item,
        'img_ang'=>$img_ang,
        );

        $checkquerys = $this->db->query("select id from tbl_upload_images where upimg_id='$upimg_id' and img_name='$file'  and upimg_id='$upimg_id'")->result_array();
        if(empty($checkquerys))
        {
        $this->uploadbrief_model->upload_images_insert($data);
        $id=$this->db->insert_id();
        }
        else
        {
        $id=$checkquerys[0]['id'];
        }








        }*/


        if($file!='.' && $file!='..')
        {
        if(copy($target_path.$file, $destination_path.$file))
        {
        unlink($target_path.$file);
        $file_image=pathinfo($file, PATHINFO_FILENAME);
        $new_thumb = "./upload/".$user_id."/".$skufolderfile_nm."/"."thumb_".$file_image.".jpg";

        $file_pat = "./upload/".$user_id."/".$skufolderfile_nm."/".$file;
        $a = getimagesize($file_pat);
            $image_type = $a[2];
            
            if(in_array($image_type , array(IMAGETYPE_GIF , IMAGETYPE_JPEG ,IMAGETYPE_PNG , IMAGETYPE_BMP)))
            {
              $this->convertToThumb($new_thumb,$user_id,$file_pat);
              $insertinidb=1;
            }
            else
            {
                unlink($file_pat);
            }
        
        }
        $img_name_check=strtoupper($file);

        if(strstr($img_name_check,'FRONT'))
        {
        $img_ang='Front';
        }
        elseif(strstr($img_name_check,'BACK'))
        {
        $img_ang='Back';
        }
        elseif(strstr($img_name_check,'LEFT'))
        {
        $img_ang='LeftSide';
        }
        elseif(strstr($img_name_check,'RIGHT'))
        {
        $img_ang='RightSide';
        }
        elseif(strstr($img_name_check,'TAG'))
        {
        $img_ang='Tag';
        }
        else
        {
        $img_ang='';
        }


        $ext_item = pathinfo($file, PATHINFO_EXTENSION);
        if(strtoupper($ext_item)=='PNG')
        {
        $file_image_item=str_replace("png","jpg",str_replace("PNG","JPG",$file));
        }
        else
        {
        $file_image_item=$file;
        }



        $data = array(
        //'user_id'=>$user_id,
        'upimg_id'=>$upimg_id,
        'img_name'=>$file_image_item,
        'img_ang'=>$img_ang,
        );
        if(isset($insertinidb))
        {
        $checkquerys = $this->db->query("select id from tbl_upload_images where upimg_id='$upimg_id' and img_name='$file'  and upimg_id='$upimg_id'")->result_array();
        if(empty($checkquerys))
        {
        $this->uploadbrief_model->upload_images_insert($data);
        $id=$this->db->insert_id();
        }
        else
        {
        $id=$checkquerys[0]['id'];
        }
        }
        }


        }
        closedir($dh);
        }
        }

        rmdir($target_path);
        rmdir("temp_folder_upload/".$user_id."/");

        /*if(copy($target_path, $destination_path)){

        }*/



      }


    }

  }


  




 






  }
  else {
  //echo "aaaaaa";exit;
  //If no session, redirect to login page
  redirect('login', 'refresh');
  }






  }








    function create_tree($path,$skufolderfile_nm_arr) {

   //public $files;
   //private $folder;

   $files = array();  
    
    if( file_exists( $path)) {
      if( $path[ strlen( $path ) - 1 ] ==  '/' )
        $folder = $path;
      else
        $folder = $path . '/';
      
      $dir = opendir( $path );
      while(( $file = readdir( $dir ) ) != false )
      {

       //echo $file;
        if(in_array($file, $skufolderfile_nm_arr))
        {
          echo $file;
          $files[] = $file;
        }
        
      }

      return $files;
      closedir( $dir );
    }

      
    if( count( $files ) > 2 ) { /* First 2 entries are . and ..  -skip them */
      natcasesort( $files );
      $list = '<ul class="filetree" style="display: none;">';
      // Group folders first
      foreach( $files as $file ) {
        if( file_exists( $folder . $file ) && $file != '.' && $file != '..' && is_dir( $folder . $file )) {
          $list .= '<li class="folder collapsed"><a href="#" rel="' . htmlentities( $folder . $file ) . '/">' . htmlentities( $file ) . '</a></li>';
        }
      }
      // Group all files
      foreach( $files as $file ) {
        if( file_exists( $folder . $file ) && $file != '.' && $file != '..' && !is_dir( $folder . $file )) {
          $ext = preg_replace('/^.*\./', '', $file);
          $list .= '<li class="file ext_' . $ext . '"><a href="#" rel="' . htmlentities( $folder . $file ) . '">' . htmlentities( $file ) . '</a></li>';
        }
      }
      $list .= '</ul>'; 
      return $list;
    }
  }



    function Foldertree()
    {
   

    $path = urldecode( $this->input->post('dir'));
    $skufolderfile_nm_field= trim(urldecode( $this->input->post('skufolderfile_nm_field')),",");
    $skufolderfile_nm_arr=explode(",",$skufolderfile_nm_field);
    echo $this->create_tree($path,$skufolderfile_nm_arr);


    }


 
    function openSKUFolder()
    {
    
    $str='';
    $path = urldecode( $this->input->post('path'));
    $foldername= urldecode( $this->input->post('foldername'));
    
    $skufolderfile_nm_field= trim(urldecode( $this->input->post('skufolderfile_nm')),",");
    $skufolderfile_nm_arr=explode(",",$skufolderfile_nm_field);

    if( file_exists( $path)) {
    if( $path[ strlen( $path ) - 1 ] ==  '/' )
    $folder = $path;
    else
    $folder = $path . '/';

    $path=$path.'/'.$foldername;
    $dir = opendir( $path );
    $i = 0;
    while(( $file = readdir( $dir ) ) != false )
    {
        $imgid = $foldername.'_'.$i;
    //echo $file;
    if($file!='.' && $file!='..' && $file!='thumb')
    {
     
     $files[] = $file;

      //$inside_path=
/*
     $str.='<div class="col-12 col-sm-12 col-md-4 col-lg-4 col-xl-4">
            <script type="text/javascript">
            var img = document.getElementById("img_'.$imgid.'");
            var width = img.naturalWidth;
            var height = img.naturalHeight;
            // console.log(width);
            // console.log(height);

            if (width > height){
                $("#rtImg_'.$imgid.'").removeClass("d-none");
            } else {
                $("#orgImg_'.$imgid.'").removeClass("d-none");
            }
            </script>
            <div id="orgImg_'.$imgid.'" class="w-100 d-none">
                <img id="img_'.$imgid.'" class="w-100 mb-3" src="'.$path.'/thumb/temp_thumb_'.$file.'" alt="'.$file.'">
            </div>

            <div id="rtImg_'.$imgid.'" class="img-box d-none">
                <img id="img_'.$imgid.'" class="img mb-3" src="'.$path.'/thumb/temp_thumb_'.$file.'" alt="'.$file.'">
            </div>
            </div>';*/

      $filepath=$path.'/'.$file;
    $a = getimagesize($filepath);
    $image_type = $a[2];
if(in_array($image_type , array(IMAGETYPE_GIF , IMAGETYPE_JPEG ,IMAGETYPE_PNG , IMAGETYPE_BMP)))
    {
     $str.='<div class="col-12 col-sm-12 col-md-4 col-lg-4 col-xl-4">
            <script type="text/javascript">
            var img = document.getElementById("img_'.$imgid.'");
            var width = img.naturalWidth;
            var height = img.naturalHeight;
            // console.log(width);
            // console.log(height);

            if (width > height){
                $("#rtImg_'.$imgid.'").removeClass("d-none");
            } else {
                $("#orgImg_'.$imgid.'").removeClass("d-none");
            }
            </script>
            <div id="orgImg_'.$imgid.'" class="w-100 d-none">
                <img id="img_'.$imgid.'" class="w-100 mb-3" src="'.$path.'/thumb/temp_thumb_'.$file.'" alt="'.$file.'">
            </div>

            <div id="rtImg_'.$imgid.'" class="img-box d-none">
                <img id="img_'.$imgid.'" class="img mb-3" src="'.$path.'/thumb/temp_thumb_'.$file.'" alt="'.$file.'">
            </div>
            </div>';

    }      


    //$str.=' <div class="col-12 col-sm-12 col-md-4 col-lg-4 col-xl-4"> <img class="w-100 mb-3" src="'.$path.'/'.$file.'"> </div>';
     }
     $i++;
    }

    echo $str;
     //echo "<pre>";
    // print_r($files);
    //return $files;
    closedir( $dir );
    }


    }

    //remove folder rajendra
    
    function removeSKUFolder()
    {
    
    $str='';
    $path = urldecode( $this->input->post('path'));
    $foldername= urldecode( $this->input->post('foldername'));
    //echo $path."/".$foldername;
    $skufolderfile_nm_field= trim(urldecode( $this->input->post('skufolderfile_nm')),",");
    $skufolderfile_nm_arr=explode(",",$skufolderfile_nm_field);

    

    $thumbpath = $path."/".$foldername."/thumb";
    $this->removeThumbSKUFolder($thumbpath);


    $path=$path."/".$foldername;
    if( file_exists($path)) {
        
        
        $files = glob($path . '/*');
 
//Loop through the file list.
foreach($files as $file){
    //Make sure that this is a file and not a directory.
    if(is_file($file)){
        //Use the unlink function to delete the file.
        unlink($file);
    }
}
        
        if(rmdir($path))
        {
          echo ("$foldername successfully removed");
        }
        else
        {
          echo ("$foldername couldn't be removed"); 
        }
        
    }
    else
    {
        echo "error";
    }
    
    }
    //remove folder ends rajendra



    public function removeThumbSKUFolder( $dir )
{
        if( is_dir( $dir ) )
        {
        $files = glob( $dir . '*', GLOB_MARK ); //GLOB_MARK adds a slash to directories returned

        foreach( $files as $file )
        {
        $this->delete_directory( $file );      
        }

        @rmdir( $dir );
        } 
        elseif( is_file( $dir ) ) 
        {
        unlink( $dir );  
        }
}

    

        //logincheck rajendra
    function logincheck()
    {
       $session_data = $this->session->userdata('front_logged_in');
        $data['user_id'] = $session_data['user_id'];
        $data['user_type_id'] = $session_data['user_type_id'];
        $data['user_name'] = $session_data['user_name'];
        $user_name = $session_data['user_name'];
        $user_id=$data['user_id'];
       
        if($user_id=='') { echo "sessionout"; } else { echo "sessionin"; }
    }   
    //logincheck end rajendra
    

    
    function checkSKULengthExistOrNotFolder()
    {

        $session_data = $this->session->userdata('front_logged_in');
        $data['user_id'] = $session_data['user_id'];
        $data['user_type_id'] = $session_data['user_type_id'];
        $data['user_name'] = $session_data['user_name'];
        $user_name = $session_data['user_name'];
        $user_id=$data['user_id'];
       
       if($user_id!='')
       { 

       $skufolder = trim( $this->input->post('skufolderfile_nm'),",");

       $skufolder_arr = explode(",",$skufolder);



      $skufolder_arr_temp=$skufolder_arr;
       if(!empty($skufolder_arr))
       {
        $sku_str="";
        foreach($skufolder_arr as $value)
        {
              $skufolder=$value;


           
            $check_path = glob("temp_folder_upload/".$user_id."/".$skufolder."/*.{jpg,jpeg,png,gif,JPG,JPEG}", GLOB_BRACE);
            $directories = glob("temp_folder_upload/".$user_id."/".$skufolder."/*.*", GLOB_BRACE);
            if(count($check_path)!=count($directories))
            {
             echo 'not_image';

              /////////////////////remove dir////////////////////////////////

            if(!empty($skufolder_arr_temp))
            {
            foreach($skufolder_arr as $value)
            {
                 $skufolder=$value;

                 $src = "zip/".$user_id."/".$skufolder; 
                  $this->delete_directory($src); 


            }
            }
             ///////////////////////////////////////////////////
            exit;
            }
           else if(strlen($skufolder)<=2)
            {
             echo 'less_strlen';

               /////////////////////remove dir////////////////////////////////

            if(!empty($skufolder_arr_temp))
            {
            foreach($skufolder_arr as $value)
            {
                $skufolder=$value;
                $src = "temp_folder_upload/".$user_id."/".$skufolder; 
                $this->delete_directory($src); 

                            /*$dirsku = "zip/".$user_id."/".$skufolder."/";
                            // Open a directory, and read its contents
                            if (is_dir($dirsku)){

                            if ($dhsku = opendir($dirsku)){
                            while (($filesku = readdir($dhsku)) !== false){
                               // echo "filesku:" . $filesku . "<br>";
                                if($filesku!='.' && $filesku!='..')
                                {
                                    
                                    unlink($dirsku.$filesku);
                                }
                            
                            
                            }
                            closedir($dhsku);
                             }
                            }
                            rmdir($dirsku);*/
                            

            }
            }
             ///////////////////////////////////////////////////

             exit;
            }
            else
            {
              $sku_str.="'".$skufolder."',";
            }

        }
       }
       

       $sku_str=trim($sku_str,",");


     // echo "select * from tbl_upload_img  where zipfile_name IN (".$skufolder.") and user_id='$user_id' ";

       //exit;
        if($sku_str!='')
        {
        $checkquerys = $this->db->query("select * from tbl_upload_img  where zipfile_name IN (".$sku_str.") and user_id='$user_id' ")->result_array();
        if(!empty($checkquerys))
        {
        echo 'exist';
        exit;
        }
        else
        {
        echo 'notexist';
        exit;
        }
        }

    }
    else
    {
        echo "sessionout";
    }




    }













    function generateSKUForExistFolder()
    {

        $session_data = $this->session->userdata('front_logged_in');
        $data['user_id'] = $session_data['user_id'];
        $data['user_type_id'] = $session_data['user_type_id'];
        $data['user_name'] = $session_data['user_name'];
        $user_name = $session_data['user_name'];
        $user_id=$data['user_id'];
        

       $skufolder = trim( $this->input->post('skufolderfile_nm'),",");

       $skufolder_arr = explode(",",$skufolder);

       if(!empty($skufolder_arr))
       {
        $sku_str="";
        foreach($skufolder_arr as $value)
        {


            $skufolder=$value;
            

            $checkquerys = $this->db->query("select * from tbl_upload_img  where user_id='$user_id' and zipfile_name like '%".$skufolder."_%' order by upimg_id desc ")->result_array();
            if(empty($checkquerys))
            {
            $newskufolder=$skufolder."_1";
            }
            else
            {
            $ctn=count($checkquerys)+1;

            $newskufolder=$skufolder."_".$ctn;
            }

            //echo $newskufolder;

            $src_path = "temp_folder_upload/".$user_id."/".$skufolder; 
            $des_path = "temp_folder_upload/".$user_id."/".$newskufolder; 

            rename($src_path,$des_path);

            $sku_str.=$newskufolder.",";
           

        }
       }
       

       echo $sku_str=",".trim($sku_str,",").",";


      



    }



    function deleteCancelSKUForExistFolder()
    {

        $session_data = $this->session->userdata('front_logged_in');
        $data['user_id'] = $session_data['user_id'];
        $data['user_type_id'] = $session_data['user_type_id'];
        $data['user_name'] = $session_data['user_name'];
        $user_name = $session_data['user_name'];
        $user_id=$data['user_id'];
        

        $file_nm = $this->input->post('file_nm');

        

        $dir = "temp_folder_upload/".$user_id."/";
        $sku_str="";
        // Open a directory, and read its contents
        if (is_dir($dir)){
            if ($dh = opendir($dir)){
                while (($file = readdir($dh)) !== false){

                    if($file!='.' && $file!='..')
                    {
                    //echo "filename:" . $file . "<br>";
                    $skufolder=$file;

                        $src = "temp_folder_upload/".$user_id."/".$skufolder; 
                        $this->delete_directory($src); 

                    }

                }

            }

        }


  echo "success";
       

    }





























function checkSKULenExistMoveunzipfile_old()
    {

        $session_data = $this->session->userdata('front_logged_in');
        $data['user_id'] = $session_data['user_id'];
        $data['user_type_id'] = $session_data['user_type_id'];
        $data['user_name'] = $session_data['user_name'];
        $user_name = $session_data['user_name'];
        $user_id=$data['user_id'];

        if($user_id!='')
        {
        
        $file_nm = trim( $this->input->post('file_nm'));
        $current_path="zip/".$user_id."/".$file_nm;
        $thumb_path=substr($current_path,0,-4)."/thumb";
        //$thumb_path=glob($thumb_path.'/*');  
        //$src = "zip/".$user_id."/".$skufolder; 
        $this->delete_directory($thumb_path); 

        
        
        


     //////////////////////////unzipped///////////////////////////////// not required bcos its already unzipped
        
        if(file_exists($current_path))
        {

        /*
        $unzip = new ZipArchive;
        $out = $unzip->open($current_path);
        if ($out === TRUE) {

        //echo getcwd();
        //$unzip->extractTo(getcwd());
        $unzip->extractTo('zip/'.$user_id.'/');
        $unzip->close();
        //echo 'File unzipped';
        }
        */
        unlink($current_path);
        }

        
     //////////////////////////unzipped/////////////////////////////////

        $dir = "zip/".$user_id."/";
        // Open a directory, and read its contents
        if (is_dir($dir)){

        if ($dh = opendir($dir)){
            $skufolder_arr=array();
        while (($file = readdir($dh)) !== false){
        //echo "filename:" . $file . "<br>";
            if($file!='.' && $file!='..')
            {
            $skufolder_arr[]=$file;
            }
        }
        closedir($dh);
        }
        }

//echo "<pre>";
//print_r($skufolder_arr);
        $skufolder_arr_temp=$skufolder_arr;
       $check= '';
       $sku_str="";
       if(!empty($skufolder_arr))
       {
        
        foreach($skufolder_arr as $value)
        {
            $skufolder=$value;
            
            //echo "zip/".$user_id."/".$skufolder."/*.*";
            $check_path = glob("zip/".$user_id."/".$skufolder."/*.{jpg,jpeg,png,gif,JPG,JPEG}", GLOB_BRACE);
            $directories = glob("zip/".$user_id."/".$skufolder."/*.*", GLOB_BRACE);
            //echo count($check_path)."--".count($directories);exit;
            if(count($check_path)!=count($directories))
            {
             echo 'not_image';

              /////////////////////remove dir////////////////////////////////

            if(!empty($skufolder_arr_temp))
            {
            foreach($skufolder_arr as $value)
            {
                 $skufolder=$value;

                 $src = "zip/".$user_id."/".$skufolder; 
                  $this->delete_directory($src); 


            }
            }
             ///////////////////////////////////////////////////
            exit;
            }
            else if(strlen($skufolder)<=2)
            {
             echo 'less_strlen';

              /////////////////////remove dir////////////////////////////////

            if(!empty($skufolder_arr_temp))
            {
            foreach($skufolder_arr as $value)
            {
                 $skufolder=$value;

                 $src = "zip/".$user_id."/".$skufolder; 
                  $this->delete_directory($src); 


                            /*$dirsku = "zip/".$user_id."/".$skufolder."/";
                            // Open a directory, and read its contents
                            if (is_dir($dirsku)){

                            if ($dhsku = opendir($dirsku)){
                            while (($filesku = readdir($dhsku)) !== false){
                               // echo "filesku:" . $filesku . "<br>";
                                if($filesku!='.' && $filesku!='..')
                                {
                                    
                                    unlink($dirsku.$filesku);
                                }
                            
                            
                            }
                            closedir($dhsku);
                             }
                            }
                            rmdir($dirsku);*/
                            

            }
            }
             ///////////////////////////////////////////////////


             exit;
            }
            else
            {
              $sku_str.="'".$skufolder."',";
            }

        }
       }





       

       $sku_str=trim($sku_str,",");

        if($sku_str!='')
        {

        //echo "select * from tbl_upload_img  where zipfile_name IN (".$sku_str.") and user_id='$user_id' ";

        //exit;
        $checkquerys = $this->db->query("select * from tbl_upload_img  where zipfile_name IN (".$sku_str.") and user_id='$user_id' ")->result_array();
        if(!empty($checkquerys))
        {
        echo 'exist';
        exit;
        }
        else
        {
        echo 'notexist';
        exit;
        }

        }

    }
    else
    {
        echo "sessionout";
    }


    }


function checkSKULenExistMoveunzipfile_old1()
    {

        $session_data = $this->session->userdata('front_logged_in');
        $data['user_id'] = $session_data['user_id'];
        $data['user_type_id'] = $session_data['user_type_id'];
        $data['user_name'] = $session_data['user_name'];
        $user_name = $session_data['user_name'];
        $user_id=$data['user_id'];

        if($user_id!='')
        {
        
        $file_nm = trim( $this->input->post('file_nm'));
        $current_path="zip/".$user_id."/".$file_nm;


     //////////////////////////unzipped/////////////////////////////////
        if(file_exists($current_path))
        {

        
        $unzip = new ZipArchive;
        $out = $unzip->open($current_path);
        if ($out === TRUE) {

        //echo getcwd();
        //$unzip->extractTo(getcwd());
        $unzip->extractTo('zip/'.$user_id.'/');
        $unzip->close();
        //echo 'File unzipped';
        }
        unlink($current_path);
        }

        
     //////////////////////////unzipped/////////////////////////////////

        $dir = "zip/".$user_id."/";
        // Open a directory, and read its contents
        if (is_dir($dir)){

        if ($dh = opendir($dir)){
            $skufolder_arr=array();
        while (($file = readdir($dh)) !== false){
        //echo "filename:" . $file . "<br>";
            if($file!='.' && $file!='..')
            {
            $skufolder_arr[]=$file;
            }
        }
        closedir($dh);
        }
        }

//echo "<pre>";
//print_r($skufolder_arr);
        $skufolder_arr_temp=$skufolder_arr;
       $check= '';
       $sku_str="";
       if(!empty($skufolder_arr))
       {
        
        foreach($skufolder_arr as $value)
        {
            $skufolder=$value;
            
            //echo "zip/".$user_id."/".$skufolder."/*.*";
            $check_path = glob("zip/".$user_id."/".$skufolder."/*.{jpg,jpeg,png,gif,JPG,JPEG}", GLOB_BRACE);
            $directories = glob("zip/".$user_id."/".$skufolder."/*.*", GLOB_BRACE);
            //echo count($check_path)."--".count($directories);exit;
            if(count($check_path)!=count($directories))
            {
             echo 'not_image';

              /////////////////////remove dir////////////////////////////////

            if(!empty($skufolder_arr_temp))
            {
            foreach($skufolder_arr as $value)
            {
                 $skufolder=$value;

                 $src = "zip/".$user_id."/".$skufolder; 
                  $this->delete_directory($src); 


            }
            }
             ///////////////////////////////////////////////////
            exit;
            }
            else if(strlen($skufolder)<=2)
            {
             echo 'less_strlen';

              /////////////////////remove dir////////////////////////////////

            if(!empty($skufolder_arr_temp))
            {
            foreach($skufolder_arr as $value)
            {
                 $skufolder=$value;

                 $src = "zip/".$user_id."/".$skufolder; 
                  $this->delete_directory($src); 


                            /*$dirsku = "zip/".$user_id."/".$skufolder."/";
                            // Open a directory, and read its contents
                            if (is_dir($dirsku)){

                            if ($dhsku = opendir($dirsku)){
                            while (($filesku = readdir($dhsku)) !== false){
                               // echo "filesku:" . $filesku . "<br>";
                                if($filesku!='.' && $filesku!='..')
                                {
                                    
                                    unlink($dirsku.$filesku);
                                }
                            
                            
                            }
                            closedir($dhsku);
                             }
                            }
                            rmdir($dirsku);*/
                            

            }
            }
             ///////////////////////////////////////////////////


             exit;
            }
            else
            {
              $sku_str.="'".$skufolder."',";
            }

        }
       }





       

       $sku_str=trim($sku_str,",");

        if($sku_str!='')
        {

        //echo "select * from tbl_upload_img  where zipfile_name IN (".$sku_str.") and user_id='$user_id' ";

        //exit;
        $checkquerys = $this->db->query("select * from tbl_upload_img  where zipfile_name IN (".$sku_str.") and user_id='$user_id' ")->result_array();
        if(!empty($checkquerys))
        {
        echo 'exist';
        exit;
        }
        else
        {
        echo 'notexist';
        exit;
        }

        }

    }
    else
    {
        echo "sessionout";
    }


    }


  function checkSKULenExistMoveunzipfile()
    {
        $session_data = $this->session->userdata('front_logged_in');
        $data['user_id'] = $session_data['user_id'];
        $data['user_type_id'] = $session_data['user_type_id'];
        $data['user_name'] = $session_data['user_name'];
        $user_name = $session_data['user_name'];
        $user_id=$data['user_id'];
        if($user_id!='')
        {
        $file_nm = trim( $this->input->post('file_nm'));
        $current_path="zip/".$user_id."/".$file_nm;
        $thumb_path=substr($current_path,0,-4)."/thumb";
        $folder_path=substr($current_path,0,-4);
        $folder_name=substr($file_nm,0,-4);
        //$thumb_path=glob($thumb_path.'/*');  
        //$src = "zip/".$user_id."/".$skufolder; 
        
        // future works
                    //$path = 'zip/'.$folderName.'/'.substr($fileName,0,-4);
                    //$unzip->extractTo($path);
        
        //
        if (is_dir($thumb_path)){
        $this->delete_directory($thumb_path); 
        }

                if (is_dir($folder_path)){

                    if ($dh = opendir($folder_path)){
                    while (($file = readdir($dh)) !== false){
                        if($file!='.' && $file!='..')
                        {
                            
                        $src_path = "zip/".$user_id."/".$folder_name."/".$file; 
                        $des_path = "zip/".$user_id."/".$file; 
                        if (is_dir($src_path)){
                            if (is_dir($des_path)){
                                echo "pls change the zip file name and upload";
                                 //$des_path = "zip/".$user_id."/".$file."_files";
                                
                                    $thumb_path=$src_path."/thumb"; 
                                    $this->delete_directory($thumb_path);
                                    //echo $des_path;
                                    $des_path1 ="zip/".$user_id."/".$folder_name."/".$file."_renamed"; 
                                    if(rename($src_path,$des_path1))
                                    {
                                       if(rename($des_path1,"zip/".$user_id."/".$file."ren"))
                                       {
                                           //echo "done";
                                           //$this->delete_directory("zip/".$user_id."/".$folder_name);
                                           $single_file=1;
                                       }
                                    }
                                
                            }
                            else
                                {
                        rename($src_path,$des_path);
                        $thumb_path=$des_path."/thumb"; 
                        $this->delete_directory($thumb_path); 
                        $folderpath_del=1;
                            }
                            
                        }
                        else
                        {
                            
                        }   
                        }
                    }
                    }
                    closedir($dh);
                    //rmdir($folder_path);
                    if( isset( $folderpath_del ) ) 
                    {
                    $this->delete_directory($folder_path); 
                    }
                    if( isset( $single_file ) ) 
                    {
                    //$this->delete_directory($folder_path); 
                    }
                    }
                    else
                    {
                    echo "its a file single folder";    
                    }
     //////////////////////////unzipped///////////////////////////////// not required bcos its already unzipped
        
        if(file_exists($current_path))
        {

        /*
        $unzip = new ZipArchive;
        $out = $unzip->open($current_path);
        if ($out === TRUE) {

        //echo getcwd();
        //$unzip->extractTo(getcwd());
        $unzip->extractTo('zip/'.$user_id.'/');
        $unzip->close();
        //echo 'File unzipped';
        }
        */
        unlink($current_path);
        }

        
     //////////////////////////unzipped/////////////////////////////////

        $dir = "zip/".$user_id."/";
        // Open a directory, and read its contents
        if (is_dir($dir)){

        if ($dh = opendir($dir)){
            $skufolder_arr=array();
        while (($file = readdir($dh)) !== false){
        //echo "filename:" . $file . "<br>";
            if($file!='.' && $file!='..')
            {
            $skufolder_arr[]=$file;
            }
        }
        closedir($dh);
        }
        }

//echo "<pre>";
print_r($skufolder_arr);
        $skufolder_arr_temp=$skufolder_arr;
       $check= '';
       $sku_str="";
       if(!empty($skufolder_arr))
       {
        
        foreach($skufolder_arr as $value)
        {
            $skufolder=$value;
            
            //echo "zip/".$user_id."/".$skufolder."/*.*";
            $check_path = glob("zip/".$user_id."/".$skufolder."/*.{jpg,jpeg,png,gif,JPG,JPEG}", GLOB_BRACE);
            $directories = glob("zip/".$user_id."/".$skufolder."/*.*", GLOB_BRACE);
            //echo count($check_path)."--".count($directories);exit;
            if(count($check_path)!=count($directories))
            {
             echo 'not_image';

              /////////////////////remove dir////////////////////////////////

            if(!empty($skufolder_arr_temp))
            {
            foreach($skufolder_arr as $value)
            {
                 $skufolder=$value;

                 $src = "zip/".$user_id."/".$skufolder; 
                  $this->delete_directory($src); 


            }
            }
             ///////////////////////////////////////////////////
            exit;
            }
            else if(strlen($skufolder)<=2)
            {
             echo 'less_strlen';

              /////////////////////remove dir////////////////////////////////

            if(!empty($skufolder_arr_temp))
            {
            foreach($skufolder_arr as $value)
            {
                 $skufolder=$value;

                 $src = "zip/".$user_id."/".$skufolder; 
                  $this->delete_directory($src); 


                            /*$dirsku = "zip/".$user_id."/".$skufolder."/";
                            // Open a directory, and read its contents
                            if (is_dir($dirsku)){

                            if ($dhsku = opendir($dirsku)){
                            while (($filesku = readdir($dhsku)) !== false){
                               // echo "filesku:" . $filesku . "<br>";
                                if($filesku!='.' && $filesku!='..')
                                {
                                    
                                    unlink($dirsku.$filesku);
                                }
                            
                            
                            }
                            closedir($dhsku);
                             }
                            }
                            rmdir($dirsku);*/
                            

            }
            }
             ///////////////////////////////////////////////////


             exit;
            }
            else
            {
              $sku_str.="'".$skufolder."',";
            }

        }
       }
       $sku_str=trim($sku_str,",");
       //echo $sku_str;

        if($sku_str!='')
        {

        //echo "select * from tbl_upload_img  where zipfile_name IN (".$sku_str.") and user_id='$user_id' ";

        //exit;
        $checkquerys = $this->db->query("select * from tbl_upload_img  where zipfile_name IN (".$sku_str.") and user_id='$user_id' ")->result_array();
        if(!empty($checkquerys))
        {
        echo 'exist';
        exit;
        }
        else
        {
        echo 'notexist';
        exit;
        }

        }

    }
    else
    {
        echo "sessionout";
    }


    }





    function generateSKUForExistZip()
    {

        $session_data = $this->session->userdata('front_logged_in');
        $data['user_id'] = $session_data['user_id'];
        $data['user_type_id'] = $session_data['user_type_id'];
        $data['user_name'] = $session_data['user_name'];
        $user_name = $session_data['user_name'];
        $user_id=$data['user_id'];
        

        $file_nm = $this->input->post('file_nm');

        $current_path="zip/".$user_id."/";

        $dir = "zip/".$user_id."/";
         $sku_str="";
        // Open a directory, and read its contents
        if (is_dir($dir)){
        if ($dh = opendir($dir)){
        while (($file = readdir($dh)) !== false){

            if($file!='.' && $file!='..')
            {
            //echo "filename:" . $file . "<br>";
            $skufolder=$file;

            $checkquerys = $this->db->query("select * from tbl_upload_img  where user_id='$user_id' and zipfile_name like '%".$skufolder."_%' order by upimg_id desc ")->result_array();
            if(empty($checkquerys))
            {
            $newskufolder=$skufolder."_1";
            }
            else
            {
            $ctn=count($checkquerys)+1;

            $newskufolder=$skufolder."_".$ctn;
            }

            //echo $newskufolder;

            $src_path = "zip/".$user_id."/".$skufolder; 
            $des_path = "zip/".$user_id."/".$newskufolder; 

            rename($src_path,$des_path);

            $sku_str.=$newskufolder.",";

            }

          }
         }
        }

       

  echo $sku_str;

      



    }









    function deleteCancelSKUForExistZip()
    {

        $session_data = $this->session->userdata('front_logged_in');
        $data['user_id'] = $session_data['user_id'];
        $data['user_type_id'] = $session_data['user_type_id'];
        $data['user_name'] = $session_data['user_name'];
        $user_name = $session_data['user_name'];
        $user_id=$data['user_id'];
        

        $file_nm = $this->input->post('file_nm');

        

        $dir = "zip/".$user_id."/";
        $sku_str="";
        // Open a directory, and read its contents
        if (is_dir($dir)){
            if ($dh = opendir($dir)){
                while (($file = readdir($dh)) !== false){

                    if($file!='.' && $file!='..')
                    {
                    //echo "filename:" . $file . "<br>";
                    $skufolder=$file;

                        $src = "zip/".$user_id."/".$skufolder; 
                        $this->delete_directory($src); 

                    }

                }

            }

        }


  echo "success";
       

    } 

}

?>