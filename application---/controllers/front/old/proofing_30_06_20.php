<?php  
error_reporting(0); 
class Proofing extends CI_Controller{
    
    function __construct(){ 
        parent::__construct();
        $this->load->library(['session']); 
        $this->load->helper(['url','file','form']); 
        $this->load->model('Proofing_model'); //load model upload 
        $this->load->model('home_model'); //load model upload 
        $this->load->library('upload'); //load library upload 
        $this->load->library('email');
        $this->load->model('Feedback_model'); //load model upload
    }
 
    public function index($brief_id){

        if ($this->session->userdata('front_logged_in')) {
            $session_data = $this->session->userdata('front_logged_in');
            $data['user_id'] = $session_data['user_id'];
            $data['user_name'] = $session_data['user_name'];
            $user_id = $session_data['user_id'];
            $user_type_id=$session_data['user_type_id'];

            $data['brief_id'] = $brief_id;
            $data['title'] = "Proofing";
            $checkquerys = $this->db->query("SELECT * FROM `wc_brief` WHERE `brief_id` ='$brief_id' and status='6'");
            $count=$checkquerys->num_rows();
            if($count>=1){
                $this->load->view('front/proofing',$data);   
            }else{
                redirect('login', 'refresh');
            }
        }
        else
        {
            $data['user_id'] = '';
            $data['user_name'] = '';
            $user_id='';
            redirect('login', 'refresh');
        }
    }


    
public function comments_update_wekan($wekan_unique_id,$annot_details){

    $session_data = $this->session->userdata('front_logged_in');
    $user_id = $session_data['user_id'];
    $user_type_id = $session_data['user_type_id'];
    $user_name = $session_data['user_name'];
    $client_access = $session_data['client_access'];


    $query=$this->db->query("select * from wc_settings WHERE setting_key='WEKAN_LINK' ");
    $userDetails= $query->result_array();
    $WEKAN_LINK=$userDetails[0]['setting_value'];

    $query=$this->db->query("select * from wc_settings WHERE setting_key='WEKAN_USERNAME' ");
    $userDetails= $query->result_array();
    $WEKAN_USERNAME=$userDetails[0]['setting_value'];


    $query=$this->db->query("select * from wc_settings WHERE setting_key='WEKAN_PASSWORD' ");
    $userDetails= $query->result_array();
    $WEKAN_PASSWORD=$userDetails[0]['setting_value'];

    

    /*$checkquerys = $this->db->query("select * from wc_clients where  client_id='$client_access' ")->result_array();
    $WEKAN_BOARD_ID=$checkquerys[0]['wekan_client_id'];

    $checkquerys = $this->db->query("select * from wc_client_wekan_list_relation where wekan_board_list_id='1' and  client_id='$client_access' ")->result_array();
    $WEKAN_LIST_ID_PENDING=$checkquerys[0]['wekan_list_id'];

    $checkquerys = $this->db->query("select * from wc_client_wekan_list_relation where wekan_board_list_id='3' and  client_id='$client_access' ")->result_array();
    $WEKAN_LIST_ID_ONGOING=$checkquerys[0]['wekan_list_id'];

    $checkquerys = $this->db->query("select * from wc_client_wekan_list_relation where wekan_board_list_id='2' and  client_id='$client_access' ")->result_array();
    $WEKAN_LIST_ID_REJECTED=$checkquerys[0]['wekan_list_id'];*/


    $checkquerys = $this->db->query("select * from wc_clients where  client_id='$client_access' ")->result_array();
    if(isset($checkquerys[0]['wekan_client_id']))
    {
        $WEKAN_BOARD_ID=$checkquerys[0]['wekan_client_id'];
    }
    

    $checkquerys = $this->db->query("select * from wc_client_wekan_list_relation where wekan_board_list_id='1' and  client_id='$client_access' ")->result_array();
   
    if(isset($checkquerys[0]['wekan_list_id']))
    {
         $WEKAN_LIST_ID_PENDING=$checkquerys[0]['wekan_list_id'];
    }

    $checkquerys = $this->db->query("select * from wc_client_wekan_list_relation where wekan_board_list_id='3' and  client_id='$client_access' ")->result_array();
   
    if(isset($checkquerys[0]['wekan_list_id']))
    {
          $WEKAN_LIST_ID_ONGOING=$checkquerys[0]['wekan_list_id'];
    }

    $checkquerys = $this->db->query("select * from wc_client_wekan_list_relation where wekan_board_list_id='2' and  client_id='$client_access' ")->result_array();
    
    if(isset($checkquerys[0]['wekan_list_id']))
    {
         $WEKAN_LIST_ID_REJECTED=$checkquerys[0]['wekan_list_id'];
    }



   if ($this->session->userdata('wekan_integration')) {

        $wekan_session_data = $this->session->userdata('wekan_integration');
        $wekan_authorId = $wekan_session_data['wekan_authorId'];
        $wekan_token = $wekan_session_data['wekan_token'];
        $wekan_tokenExpires = $wekan_session_data['wekan_tokenExpires'];
    }
    else
    {

        $curl = curl_init();
        curl_setopt_array($curl, array(
        CURLOPT_URL => $WEKAN_LINK."users/login",
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_ENCODING => "",
        CURLOPT_MAXREDIRS => 10,
        CURLOPT_TIMEOUT => 30,
        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
        CURLOPT_CUSTOMREQUEST => "POST",
        CURLOPT_POSTFIELDS => "username=".$WEKAN_USERNAME."&password=".$WEKAN_PASSWORD."",
        CURLOPT_HTTPHEADER => array(
        "cache-control: no-cache",
        "content-type: application/x-www-form-urlencoded"
        ),
        ));
        $response = curl_exec($curl);
        $err = curl_error($curl);

        curl_close($curl);

        if (!$err)
        {
        //var_dump($response);



        $res_arr=json_decode($response);

        //echo "<pre>";
        //print_r($res_arr);
        $id=$res_arr->id;
        $token=$res_arr->token;
        $tokenExpires=$res_arr->tokenExpires;


        $sess_array = array(
        'wekan_authorId' => $id,
        'wekan_token' => $token,
        'wekan_tokenExpires' => $tokenExpires
        );
        $this->session->set_userdata('wekan_integration', $sess_array);


        $wekan_session_data = $this->session->userdata('wekan_integration');
        $wekan_authorId = $wekan_session_data['wekan_authorId'];
        $wekan_token = $wekan_session_data['wekan_token'];
        $wekan_tokenExpires = $wekan_session_data['wekan_tokenExpires'];

        }

    }

/*echo $wekan_token."----".$WEKAN_LINK."api/boards/".$WEKAN_BOARD_ID."/lists/".$WEKAN_LIST_ID_PENDING."/cards/".$wekan_unique_id;
echo "<br>";
echo "listId=".$WEKAN_NEW_STATUS."";*/



    $ch = curl_init();

    curl_setopt($ch, CURLOPT_URL,$WEKAN_LINK."api/boards/".$WEKAN_BOARD_ID."/cards/".$wekan_unique_id."/comments");
    curl_setopt($ch, CURLOPT_HTTPHEADER, array(
    'Content-Type: application/x-www-form-urlencoded',
    'Authorization: Bearer ' . $wekan_token
    ));
    curl_setopt($ch, CURLOPT_POST, 1);
    curl_setopt($ch, CURLOPT_POSTFIELDS,
    "authorId=".$wekan_authorId."&comment=".$annot_details );
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

    $server_output1 = curl_exec($ch);

    curl_close ($ch);
    $id_arr1=json_decode($server_output1);


    

}








    public function update_status()
    {

        if ($this->session->userdata('front_logged_in')) {
            $session_data = $this->session->userdata('front_logged_in');
            $data['user_id'] = $session_data['user_id'];
            $data['user_name'] = $session_data['user_name'];
            $user_id = $session_data['user_id'];
            $user_type_id=$session_data['user_type_id'];

            $data['title'] = "Proofing";
            $image_id=preg_replace('/[^a-zA-Z0-9_ -]/s','',$_POST['image_id']);
            $brief_id=preg_replace('/[^a-zA-Z0-9_ -]/s','',$_POST['brief_id']);
            $img_status=preg_replace('/[^a-zA-Z0-9_ -]/s','',$_POST['img_status']);


            // $array = array('image_id' => $image_id, 'brief_id' => $brief_id);
            // $img_status1= array('img_status' => $img_status);
            // $this->db->where($array);
            // $report = $this->db->update('wc_image_upload', $img_status1);
            // $sql = $this->db->last_query();

            if($img_status=="1"){
                $status_query1= $this->db->query("SELECT * FROM wc_image_upload WHERE brief_id='$brief_id' and image_id='$image_id'");
                $status_result1= $status_query1->result_array();
                $parent_img=$status_result1[0]['parent_img'];

                $array = array('image_id' => $image_id, 'brief_id' => $brief_id);
                $img_status1= array('img_status' => $img_status);
                $this->db->where($array);
                $report = $this->db->update('wc_image_upload', $img_status1);
                $sql = $this->db->last_query();


                echo $report;

            }
            else{

                /*$array = array('image_id' => $image_id, 'brief_id' => $brief_id);
                $img_status1= array('img_status' => $img_status);
                $this->db->where($array);
                $report = $this->db->update('wc_image_upload', $img_status1);*/

                 $array = array('image_id' => $image_id, 'brief_id' => $brief_id);
                if($img_status=="0"){
                    $img_status1= array('img_status' => $img_status);
                }
                if($img_status=="2"){
                    $img_status1= array('img_status' => $img_status);
                }
                if($img_status=="3"){
                    $img_status1= array('img_status' => $img_status,'cron_mail_status' => '0');
                }
                if($img_status=="6"){
                    $reason_qc=$_POST['reason_qc'];
                    $img_status1= array('img_status' => $img_status,'reason_qc' => $reason_qc);
                }
                $this->db->where($array);
                $report = $this->db->update('wc_image_upload', $img_status1);
                $sql = $this->db->last_query();
             

                $wc_brief_sql_ver = "SELECT version_num FROM `wc_image_upload` where brief_id='$brief_id' and image_id='$image_id'";
                $wc_brief_query_ver = $this->db->query($wc_brief_sql_ver);
                $wc_brief_result_ver=$wc_brief_query_ver->result_array();
                $num=sizeof($wc_brief_result_ver);
                if($num>0){
                    $max_ver=$wc_brief_result_ver[0]['version_num'];
                }else{
                    $max_ver=1;
                }
                echo $max_ver;
                // email to pm to reupload images


                
                
                // end of email to pm to reupload images




            if($img_status=='3')
            {


          ///////////////////////////////////////////New cone///////////////////////////////////////////

            $status_query1= $this->db->query("SELECT * FROM wc_image_upload WHERE brief_id='$brief_id' and image_id='$image_id'");
            $status_result1= $status_query1->result_array();
            $img_title=$status_result1[0]['img_title'];
            $version_num=$status_result1[0]['version_num'];
             $image_path=$status_result1[0]['image_path'];
             $added_by=$status_result1[0]['added_by'];


            /*$annot_details=$image_id." - ".$img_title." - Revision".$version_num." - <br>";*/
            $annot_details=$image_id." - ".$image_path." - ".$img_title." - Revision".$version_num." - <br>";

            $annotation_query= $this->db->query("SELECT * FROM wc_image_annotation WHERE image_id='$image_id' ");
            $annotation_result= $annotation_query->result_array();
            if(!empty($annotation_result))
            {
                foreach ($annotation_result as $annotation_key => $annotation_value) 
                {
                     $descr= $annotation_value['descr'];

                     //$annot_details.=$descr." - ok <br>";
                     $annot_details.=$descr." - Pending <br>";
                }
            }

             $checkquerys = $this->db->query("select * from wc_brief where  brief_id='$brief_id' ")->result_array();
            $wekan_unique_id=$checkquerys[0]['wekan_unique_id'];
            $this->comments_update_wekan($wekan_unique_id,$annot_details);   

            //////////////////////////////////////////////////////////////////////////////////////


            /////////////////////////////////Send Mail Start//////////////////////////////////////

                   
            /*********************************Send mail*************************************/

                    $query=$this->db->query("select * from  wc_settings  WHERE setting_key='MAIL_SMTP_USER' ");
                    $userDetails= $query->result_array();
                    $MAIL_SMTP_USER=$userDetails[0]['setting_value'];

                    $query=$this->db->query("select * from  wc_settings  WHERE setting_key='MAIL_SMTP_PASSWORD' ");
                    $userDetails= $query->result_array();
                    $MAIL_SMTP_PASSWORD=$userDetails[0]['setting_value'];

                    $query=$this->db->query("select * from  wc_settings  WHERE setting_key='MAIL_BODY_SEND_FOR_REVISION' ");
                    $userDetails= $query->result_array();
                    $MAIL_BODY=$userDetails[0]['setting_value'];

                    $query=$this->db->query("select * from  wc_settings  WHERE setting_key='MAIL_FROM' ");
                    $userDetails= $query->result_array();
                    $MAIL_FROM=$userDetails[0]['setting_value'];


                    $query=$this->db->query("select * from  wc_settings  WHERE setting_key='MAIL_FROM_TEXT' ");
                    $userDetails= $query->result_array();
                    $MAIL_FROM_TEXT=$userDetails[0]['setting_value'];

                    $checkquerys = $this->db->query("select * from wc_brief where  brief_id='$brief_id' ")->result_array();
                    $brief_id=$checkquerys[0]['brief_id'];
                    //$rejected_reasons=$checkquerys[0]['rejected_reasons'];
                    $added_by_user_id=$checkquerys[0]['added_by_user_id'];
                    $brief_title=$checkquerys[0]['brief_title'];

                    





                    $checkquerys = $this->db->query("select * from wc_users where  user_id='$added_by_user_id' ")->result_array();
                    $project_manager_email=$checkquerys[0]['user_email'];
                    $project_manager_name=$checkquerys[0]['user_name'];
                    $client_id=$checkquerys[0]['client_id'];


                    $checkquerys_mail = $this->db->query("select * from wc_users where  client_id='$client_id' and user_type_id='3' ")->result_array();


                    if(!empty($checkquerys_mail))
                    {
                    foreach($checkquerys_mail as $key => $value)
                    {

                    $to_email_all[]=$value['user_email'];

                    }

                    }
        
                    $checkquerys_clientmail= $this->db->query("SELECT * FROM `wc_clients` WHERE `client_id`='$client_id'")->result_array();
                    $client_email=$checkquerys_clientmail[0]['client_email'];
                    array_push($to_email_all,$client_email);

                    /*$subject=$MAIL_FROM_TEXT.' - Notification mail for Card - '.$brief_id;*/
                    $subject=$MAIL_FROM_TEXT.' - Send Revision (#'.$brief_id.')';
                    // $uemail=$brand_manager_email;
                    //$unique_user_id=time()."-".$user_id;
                    //$activation_link=base_url()."activate-registration/".base64_encode($unique_user_id);


                    //Load email library
                    $this->load->library('email');

                    $config = array();
                    //$config['protocol'] = 'smtp';
                    $config['protocol']     = 'mail';
                    $config['smtp_host'] = 'localhost';
                    $config['smtp_user'] = $MAIL_SMTP_USER;
                    $config['smtp_pass'] = $MAIL_SMTP_PASSWORD;
                    $config['smtp_port'] = 25;




                    $this->email->initialize($config);
                    $this->email->set_newline("\r\n");
                    /*if($client_id!='') {
                    $checkquerys = $this->db->query("select * from wc_clients where client_id='$client_id' ")->result_array();

                    $client_image=$checkquerys[0]['client_image'];
                    $wekan_client_id=$checkquerys[0]['wekan_client_id'];

                    $client_logo=base_url()."/upload_client_logo/".$client_image;
                    }
                    else
                    {
                    $client_logo=base_url()."/website-assets/images/logo.png";
                    }
                    $client_logo="http://style.piquic.com/WeCreate//website-assets/images/logo.png";*/

                    $website_url=base_url();
                    $dashboard_url=base_url()."dashboard"; 

                    // $from_email = "demo.intexom@gmail.com";
                    $from_email = $MAIL_FROM;
                    $to_email = $to_email_all;

                    $date_time=date("d/m/y");
                    /*$copy_right=$MAIL_FROM_TEXT." - ".date("Y");*/
                    $copy_right=$MAIL_FROM_TEXT;
                    $project_name="#".$brief_id." - ".$brief_title;
                    //echo $message=$MAIL_BODY;
                    /*$find_arr = array("##CLIENT_LOGO##","##PM_NAME##","##BRIEF_ID##","##IMAGE_ID##","##COPY_RIGHT##");
                    $replace_arr = array($client_logo,$project_manager_name,$brief_id,$image_id,$copy_right);*/

                    $find_arr = array("##WEBSITE_URL##","##PROJECT_NAME##","##DASHBOARD_LINK##","##COPY_RIGHT##");
                    $replace_arr = array($website_url,$project_name,$dashboard_url,$copy_right);

                    $message=str_replace($find_arr,$replace_arr,$MAIL_BODY);

                    //echo $message;exit;



                    $this->email->from($from_email, $MAIL_FROM_TEXT);
                    $this->email->to($to_email);
                     if($_SERVER['HTTP_HOST']!='collab.piquic.com')
                    {
                        $this->email->cc(array('poulami@mokshaproductions.in','pranav@mokshaproductions.in','raja.priya@mokshaproductions.in','rajendra.prasad@mokshaproductions.in'));
                    }
                    //$this->email->bcc('pranav@mokshaproductions.in');
                    $this->email->subject($subject);
                    $this->email->message($message);
                    //Send mail
                    /*if($this->email->send())
                    {

                    //echo "mail sent";exit;
                    $this->session->set_flashdata("email_sent","Congragulation Email Send Successfully.");
                    }

                    else
                    {
                    //echo $this->email->print_debugger();
                    //echo "mail not sent";exit;
                    $this->session->set_flashdata("email_sent","You have encountered an error");
                    }
                    */






                     //////////////////////////////////////Send Mail End/////////////////////////////

                   /************************************************************************/



            }





                
            }
            
            // echo $report;
        }
        else
        {
            $data['user_id'] = '';
            $data['user_name'] = '';
            $user_id='';
            redirect('login', 'refresh');
        }

       
    }


    public function use_img()
    {

        if ($this->session->userdata('front_logged_in')) {
            $session_data = $this->session->userdata('front_logged_in');
            $data['user_id'] = $session_data['user_id'];
            $data['user_name'] = $session_data['user_name'];
            $user_id = $session_data['user_id'];
            $user_type_id=$session_data['user_type_id'];

           // $data['brief_id'] = $brief_id;
            $data['title'] = "Proofing";
            $brief_id=preg_replace('/[^a-zA-Z0-9_ -]/s','',$_POST['brief_id']);
            
            $image_id=preg_replace('/[^a-zA-Z0-9_ -]/s','',$_POST['image_id']);
            $img_status=preg_replace('/[^a-zA-Z0-9_ -]/s','',$_POST['img_status']);

            //$checkquerys = $this->db->query("select * from wc_users where  user_id='$user_id' ")->result_array();
            // $checkquerys = $this->db->query("SELECT * FROM `wc_brief` WHERE `brief_id` ='$brief_id'")->result_array();
            // $brand_id=$checkquerys[0]['brand_id'];

            // $checkquerys = $this->db->query("select wc_brands.*,wc_clients.client_name from wc_brands inner join wc_clients on wc_brands.client_id=wc_clients.client_id where  wc_brands.brand_id='$brand_id' ")->result_array();
            // $brand_id=$checkquerys[0]['brand_id'];
            // $brand_name=preg_replace('/[^a-zA-Z0-9_.]/', '_',$checkquerys[0]['brand_name']);
            // $client_id=$checkquerys[0]['client_id'];
            // $client_name=preg_replace('/[^a-zA-Z0-9_.]/', '_',$checkquerys[0]['client_name']);
            $checkquerys = $this->db->query("select * from wc_brief where brief_id='$brief_id' ")->result_array();
            $added_by_user_id=$checkquerys[0]['added_by_user_id'];
            $brand_id=$checkquerys[0]['brand_id'];
            $brief_title=$checkquerys[0]['brief_title'];
            $brief_name=preg_replace('/[^a-zA-Z0-9_.]/', '_',$brief_title);

            /*$checkquerys = $this->db->query("select wc_brands.*,wc_clients.client_name from wc_brands inner join wc_clients on wc_brands.client_id=wc_clients.client_id where wc_brands.brand_id='$brand_id' ")->result_array();*/

            /*$checkquerys = $this->db->query("select wc_brands.*,wc_clients.client_name from wc_users
            inner join wc_brands on wc_users.brand_id=wc_brands.brand_id
            inner join wc_clients on wc_brands.client_id=wc_clients.client_id
            where wc_users.user_id='$added_by_user_id' ")->result_array();*/

             $checkquerys = $this->db->query("select wc_brands.*,wc_clients.client_name from wc_brands
            inner join wc_clients on wc_brands.client_id=wc_clients.client_id
            where wc_brands.brand_id='$brand_id' ")->result_array();

            $brand_id=$checkquerys[0]['brand_id'];
            $brand_name=preg_replace('/[^a-zA-Z0-9_.]/', '_',$checkquerys[0]['brand_name']);
            $client_id=$checkquerys[0]['client_id'];
            $client_name=preg_replace('/[^a-zA-Z0-9_.]/', '_',$checkquerys[0]['client_name']);

            $checkquerys1 = $this->db->query("select * from wc_image_upload where  image_id='$image_id' ")->result_array();
            $version_num=$checkquerys1[0]['version_num'];
            $parent_img=$checkquerys1[0]['parent_img'];
            $file_type=$checkquerys1[0]['file_type'];
            if(!empty($_POST['image_path'])){
               $image_path=$checkquerys1[0]['image_path']; 
            }
            else{
                $image_path=$checkquerys1[0]['image_path'];
            }

            // $checkquerys2 = $this->db->query("select * from wc_revision where  revision_id='$revision_id' ")->result_array();
            // $revision_name=$checkquerys2[0]['revision_name'];

            $dst = "upload/".$client_id."-".$client_name."/".$brand_id."-".$brand_name."/".$brief_id."-".$brief_name."/".$parent_img."/Revision".$version_num;  
            $html="";

             $image_org="upload/".$client_id."-".$client_name."/".$brand_id."-".$brand_name."/".$brief_id."-".$brief_name."/".$parent_img."/Revision".$version_num."/".$image_path;


            $mime = mime_content_type($dst."/".$image_path);
            if(strstr($mime, "video/")){
            /*$div_con='<video id="video_'.$image_id.'" width="100%" height="100%" controls>
                       <source src="'.base_url().$image_org.'" type="video/mp4">Your browser does not support the video tag.
                            </video>';
*/

            $div_con='<div id="video_id"></div>';

            }else if(strstr($mime, "image/")){
            $div_con="<img class='img_download' style='max-width: 100%;' src='".base_url().$dst."/org_".$image_path."'>";
            }

            $select_value="";


            $html.="<div class='col-12 col-sm-12 col-md-12' >
                        <div class='p-4'>
                           <div class='w-100 text-center border'>
                        <div id='compare_img' class='m-auto'>
                            ".$div_con."
                            </div></div>
                            ";
            if($img_status=='1')
            {
            
                $html.="<button type='submit' class='btn btn-wc btn-block m-2 px-5'>Approved&nbsp;<i class='fas fa-check'></i></button>";
            }else{
                $html.=" <div class='row' id='update_approve'><div class='col-12 col-sm-12 col-md-12'>
                    <div class='d-flex justify-content-center flex-wrap'>
                       ";
                        if($img_status=='0' && ($user_type_id=='1'||$user_type_id=='2'||$user_type_id=='4')){
                            $html.="<button type='submit' class='btn btn-outline-wc btn-lg m-2 px-5' onclick='updateApprove(".$image_id.",".$brief_id.")'>Approve</button>";
                            $html.="<button type='submit' class='btn btn-outline-wc btn-lg m-2 px-5' onclick='review(".$image_id.",".$brief_id.",\"".$image_path."\",\"".$select_value."\",\"2\",\"".$file_type."\")'>Review</button>";
                        }
                       
                    // if($img_status=='4' && ($user_type_id=='1'||$user_type_id=='2'||$user_type_id=='3')){
                    //      $html.="<button type='submit' class='btn btn-outline-wc btn-lg m-2 px-5' onclick='updateApprove(".$image_id.",".$brief_id.")'>Approve</button>";
                    // }
                   
                    // if($img_status=='0' && ($user_type_id=='1'||$user_type_id=='2'||$user_type_id=='4')){
                    //      $html.="<button type='submit' class='btn btn-outline-wc btn-lg m-2 px-5' onclick='review(".$image_id.",".$brief_id.",\"".$image_path."\")'>Review</button>";
                    // }
                    $html.="
                    </div></div>
                </div>
                ";
            }
              $html.="
                    </div>
                            </div>";
            echo $html;
           
        }
        else
        {
            $data['user_id'] = '';
            $data['user_name'] = '';
            $user_id='';
            redirect('login', 'refresh');
        }
        
    }

    public function review()
    {
         if ($this->session->userdata('front_logged_in')) {
            $session_data = $this->session->userdata('front_logged_in');
            $data['user_id'] = $session_data['user_id'];
            $data['user_name'] = $session_data['user_name'];
            $user_id = $session_data['user_id'];
            $user_type_id=$session_data['user_type_id'];

            //$data['brief_id'] = $brief_id;
            $data['title'] = "Proofing";
            $brief_id=preg_replace('/[^a-zA-Z0-9_ -]/s','',$_POST['brief_id']);
            $image_path=preg_replace('/[^a-zA-Z0-9_ -]/s','',$_POST['image_path']);
            $image_id=preg_replace('/[^a-zA-Z0-9_ -]/s','',$_POST['image_id']);

            $query= $this->db->query("SELECT * FROM wc_image_upload WHERE image_id='$image_id'");
            $rs= $query->result_array();
            //echo $rs[0]['img_status'];
             if(empty($_POST['image_path'])){
               $image_path=$rs[0]['image_path'];
            }
            else{
               $image_path=$rs[0]['image_path'];
            }
            if($rs[0]['img_status']==0)
            {
                $array = array('image_id' => $image_id, 'brief_id' => $brief_id);
                $img_status1= array('img_status' => '2');
                $this->db->where($array);
                $report = $this->db->update('wc_image_upload', $img_status1);
            }
            $wc_brief_sql = "SELECT * FROM `wc_brief` where brief_id='$brief_id'";
            $wc_brief_query = $this->db->query($wc_brief_sql);

            $wc_brief_result=$wc_brief_query->result_array();
            //print_r($wc_brief_result);
            // $wc_brief_qrydata = mysqli_fetch_array($wc_brief_query);
            $brief_title=$wc_brief_result[0]['brief_title'];

            $image_upload_query1= $this->db->query("SELECT * FROM wc_image_upload WHERE image_id='$image_id'");
            $image_upload_rs1= $image_upload_query1->result_array();
            $image_upload_rs1[0]['img_status'];
           

            //$checkquerys = $this->db->query("select * from wc_users where  user_id='$user_id' ")->result_array();
            //  $checkquerys = $this->db->query("SELECT * FROM `wc_brief` WHERE `brief_id` ='$brief_id'")->result_array();
            // $brand_id=$checkquerys[0]['brand_id'];

            // $checkquerys = $this->db->query("select wc_brands.*,wc_clients.client_name from wc_brands inner join wc_clients on wc_brands.client_id=wc_clients.client_id where  wc_brands.brand_id='$brand_id' ")->result_array();
            // $brand_id=$checkquerys[0]['brand_id'];
            // $brand_name=preg_replace('/[^a-zA-Z0-9_.]/', '_',$checkquerys[0]['brand_name']);
            // $client_id=$checkquerys[0]['client_id'];
            // $client_name=preg_replace('/[^a-zA-Z0-9_.]/', '_',$checkquerys[0]['client_name']);

            $checkquerys = $this->db->query("select * from wc_brief where brief_id='$brief_id' ")->result_array();
            $added_by_user_id=$checkquerys[0]['added_by_user_id'];
            $brand_id=$checkquerys[0]['brand_id'];
            $brief_title=$checkquerys[0]['brief_title'];
            $brief_name=preg_replace('/[^a-zA-Z0-9_.]/', '_',$brief_title);

            /*$checkquerys = $this->db->query("select wc_brands.*,wc_clients.client_name from wc_brands inner join wc_clients on wc_brands.client_id=wc_clients.client_id where wc_brands.brand_id='$brand_id' ")->result_array();*/

            /*$checkquerys = $this->db->query("select wc_brands.*,wc_clients.client_name from wc_users
            inner join wc_brands on wc_users.brand_id=wc_brands.brand_id
            inner join wc_clients on wc_brands.client_id=wc_clients.client_id
            where wc_users.user_id='$added_by_user_id' ")->result_array();*/
             $checkquerys = $this->db->query("select wc_brands.*,wc_clients.client_name from wc_brands
            inner join wc_clients on wc_brands.client_id=wc_clients.client_id
            where wc_brands.brand_id='$brand_id' ")->result_array();

            $brand_id=$checkquerys[0]['brand_id'];
            $brand_name=preg_replace('/[^a-zA-Z0-9_.]/', '_',$checkquerys[0]['brand_name']);
            $client_id=$checkquerys[0]['client_id'];
            $client_name=preg_replace('/[^a-zA-Z0-9_.]/', '_',$checkquerys[0]['client_name']);

            $checkquerys1 = $this->db->query("select * from wc_image_upload where  image_id='$image_id' ")->result_array();
            $parent_img=$checkquerys1[0]['parent_img'];
            $version_num=$checkquerys1[0]['version_num'];
            $img_path=$checkquerys1[0]['image_path'];
            $file_type=$checkquerys1[0]['file_type'];


            $dst = "upload/".$client_id."-".$client_name."/".$brand_id."-".$brand_name."/".$brief_id."-".$brief_name."/".$parent_img."/Revision".$version_num;  
            $html=""; 
        


            $image_org="upload/".$client_id."-".$client_name."/".$brand_id."-".$brand_name."/".$brief_id."-".$brief_name."/".$image_id."/Revision".$version_num."/".$image_path;
            $mime = mime_content_type($dst."/".$image_path);
            
            $query1= $this->db->query("SELECT * FROM wc_image_annotation WHERE image_id='$image_id'");
            // $reasonquerys = $query1->result_array();
            // print_r($reasonquerys);
            $annotation_count=$query1->num_rows();
          //  echo $version_num;
            if($version_num==1)
            {
              if($image_upload_rs1[0]['img_status']==2 && ($user_type_id=='1'||$user_type_id=='2'||$user_type_id=='4')){ 
                $img_id= 'imgtag';
                } 
                else{
                    $img_id='temp_image_download'.$image_id;
                }  
            }
            elseif($version_num>1 && $annotation_count>=1 &&($image_upload_rs1[0]['img_status']==2 && ($user_type_id=='1'||$user_type_id=='2'||$user_type_id=='4')) ){
                $img_id= 'imgtag';
            }
            else{
                $img_id='temp_image_download'.$image_id;
            }

            if(strstr($mime, "video/")){
            /*$div_con='<video id="video_'.$image_id.'" width="100%" height="100%" controls>
                       <source src="'.base_url().$image_org.'" type="video/mp4">Your browser does not support the video tag.
                            </video>';*/

             $div_con='<div id="video_id"></div>';               
            }
            else if(strstr($mime, "image/")){
            $div_con="<img class='img_download' style='max-width: 100%;' id='".$img_id."' src='".base_url().$dst."/org_".$image_path."'>";
            }
            else
            {
                $div_con="";
            }
            // $query= $this->db->query("SELECT * FROM wc_image_annotation WHERE image_id='$image_id' and img_an_status='0'");
            // $count=$query->num_rows();

if($file_type=="1")
{

     $html.="<div class='col-12 col-sm-12 col-md-12' >
                        <div class='p-4' id='img_div1'>
                           <div class='w-100 text-center border'> ".$div_con."</div>";


}
else
{
    /*class='col-12 col-sm-12 col-md-9'*/
     $html.="
                <div class='col-12 col-sm-12 col-md-9 imgdivscrl'>
                        <div class='p-4 imgdiv' id='img_div1'><div class='w-100 text-center border'><div id='compare_img' class='m-auto'>".$div_con."</div></div>
                             <div id='tagbox' class='tagbox'></div>
                            ";


}


           



                        if($image_upload_rs1[0]['img_status']==3)
                        {
                            $html.="<div class='bg-light w-100 text-center p-2 lead'>Under Revision&nbsp;<i class='fas fa-sync-alt'></i></div>";
                        }
                        //  if($image_upload_rs1[0]['img_status']==4)
                        // {
                        //      $html.=" <div class='row' id='update_approve'><div class='col-12 col-sm-12 col-md-12'>
                        //         <div class='d-flex justify-content-center flex-wrap'>
                        //            ";
                        //         if($user_type_id=='1'||$user_type_id=='2'||$user_type_id=='3'){
                        //              $html.="<button type='submit' class='btn btn-outline-wc btn-lg m-2 px-5' onclick='updateApprove(".$image_id.",".$brief_id.")'>Approve</button>";
                        //         }
                               
                              
                        //         $html.="
                        //         </div></div>
                        //     </div>
                        //     </div>
                        //             </div>";
                        // }   
                          $html.=" <div class='row d-none' id='update_approve'>
                                        <div class='col-12 col-sm-12 col-md-12'>
                                        <div class='d-flex justify-content-center flex-wrap'>
                                           ";
                                            if($image_upload_rs1[0]['img_status']==2 && ($user_type_id=='1'||$user_type_id=='2'||$user_type_id=='4')){
                                                $html.="<button type='submit' class='btn btn-outline-wc btn-lg m-2 px-5' onclick='updateApprove(".$image_id.",".$brief_id.")'>Approve</button>";
                                                if($version_num>1){   
                                                    $html.="<button type='submit' class='btn btn-outline-wc btn-lg m-2 px-5' onclick='review_img_tag(\"".$image_id."\")' id='review_img_tag'>Review</button>";
                                                }
                                                if($image_id==$parent_img && $version_num==1){   
                                                    $html.="<button type='submit' class='btn btn-outline-wc btn-lg m-2 px-5' onclick='review_img_tag(\"".$image_id."\")' id='review_img_tag'>Review</button>";
                                                }
                                            }
                                            // if($version_num==1 && ($user_type_id=='5' || $user_type_id=='6')){   
                                            //         $html.="<button type='submit' class='btn btn-outline-wc btn-lg m-2 px-5' onclick='review_img_tag(\"".$image_id."\")' id='review_img_tag'>Review</button>";
                                            // }
                                        // if($img_status=='4' && ($user_type_id=='1'||$user_type_id=='2'||$user_type_id=='3')){
                                        //      $html.="<button type='submit' class='btn btn-outline-wc btn-lg m-2 px-5' onclick='updateApprove(".$image_id.",".$brief_id.")'>Approve</button>";
                                        // }
                                       
                                        // if($img_status=='0' && ($user_type_id=='1'||$user_type_id=='2'||$user_type_id=='4')){
                                        //      $html.="<button type='submit' class='btn btn-outline-wc btn-lg m-2 px-5' onclick='review(".$image_id.",".$brief_id.",\"".$image_path."\")'>Review</button>";
                                        // }
                                        $html.="
                                        </div></div>
                                    
                                    ";

                    if($file_type=='1')
                    {
                        $revision_list_style="style='display:none;'";
                    }
                    else
                    {
                        $revision_list_style="style='display:block;'";
                    }


                    // $image_name=  $files_result1['image_path'];
                    // substr($image_name[0], 0, -11)

                    // $img_ext = pathinfo($img_path)['extension'];
                    // $img_nm  = substr($img_path, 0, strrpos($img_path, '.'));

                    /*$img_ext = pathinfo($img_path)['extension'];
                    $img_name  = preg_replace('/.[^.]*$/', '', $img_path);
                    $img_name  = substr($img_name, 0, -11);
                    $img_nm=$img_name.".".$img_ext;*/

                     if($file_type!='1')
                    {
                    $img_ext = pathinfo($img_path)['extension'];
                    $img_name  = preg_replace('/.[^.]*$/', '', $img_path);
                    $img_name  = substr($img_name, 0, -11);
                    $img_nm=$img_name.".".$img_ext;
                    }
                    else
                    {
                    $img_nm='';
                    }
                    if($user_type_id=='5' || $user_type_id=='6'){
                        //  echo $user_type_id;
                        $img_upload_button="Send for QC";
                    
                    }else{
                        //  echo $user_type_id;
                        $img_upload_button="Upload";
                    
                    }
                    if($user_type_id=='3' && ($image_upload_rs1[0]['img_status']==5 || $image_upload_rs1[0]['img_status']==6)){
                        $d_none1="d-none";
                    }else{
                        $d_none1="";
                    }
                    //echo $image_upload_rs1[0]['img_status'];
                       $html.=  "
                           
                        </div>
                    </div>
                </div>
               <div class='col-12 col-sm-12 col-md-3' id='revision_list' ".$revision_list_style."   >
                        <div class='py-4 w-100'>
                            <p class='lead pb-3 text-wc ' id='img_nm'>".$img_nm."</p>
                            <p class='lead ".$d_none1."' id='notes'></p>
                                <div class='list-id pt-3 ".$d_none1."' id='list-id' > 
                                </div> 
                                <div id='sendRevision' class='upload-btn-wrapper'>";
                if($image_upload_rs1[0]['img_status']==2 && ($user_type_id=='1'||$user_type_id=='2'||$user_type_id=='4'))
                {
                    $html.="    <button type='submit' class='btn btn-outline-wc btn-block px-5 mb-2' onclick='sendRevision(\"".$image_id."\",\"".$brief_id."\",\"".$file_type."\")'>Send Revision</button>

                            <button type='submit' class='btn btn-outline-danger btn-block px-5' onclick='reviewCancel(\"".$image_id."\",\"".$brief_id."\",\"".$file_type."\")'>Cancel</button>";
                }
                else if($image_upload_rs1[0]['img_status']==3 && ($user_type_id=='1'||$user_type_id=='2'||$user_type_id=='3'|| $user_type_id=='5'|| $user_type_id=='6')){
                    $html.="<form action='' method='post' enctype='multipart/form-data' id='upldForm' name='upldForm".$image_id."'>
                                <input type='hidden' name='brief_id' id='brief_id' value='".$brief_id."'>
                                <input type='hidden' name='image_id' id='image_id' value='".$image_id."'>
                                
                                <label for='files' class='btn btn-outline-wc w-100 px-5 mb-2'><i class='fas fa-upload'></i>&nbsp;Upload Revised File</label>
                                
                               <input type='file' class='upload_revised_file_ooo d-none' accept='image/jpg,image/jpeg,image/png,video/mp4' name='files'  id='files' onchange='readURL(this)'/>
                            </form>
                            <div id='preview' class='w-100 d-block'>

                            </div>
                            <div class='alert alert-success text-center lead d-none' id='imageSuccess' role='alert'><i class='far fa-thumbs-up fa-2x'></i><br><span id='imagetxtDanger'></span></div>    
                            <div class='alert alert-danger text-center lead d-none' id='imageDanger' role='alert'><i class='far fa-thumbs-down fa-2x'></i><br><span id='imagetxtDanger'>Please Upload Image/Video File Only</span></div>
                             <button type='submit' class='btn btn-outline-wc btn-lg m-2 px-5 w-100 d-none' id='img_upload' onclick='uploadFile()'>".$img_upload_button."</button>


                            ";
                } else if($image_upload_rs1[0]['img_status']==5 && $user_type_id=='3'){
                   // echo "ahddfk;ljasd";
                   
                    $html.=" <div class='d-flex align-items-start flex-column' style='height: 525px;'>
                                <div class='mb-auto w-100'>
                                    <div class='d-flex justify-content-start' id='qc_button'>
                                        <div class='py-2 px-1'><p class='lead'>Select:</p></div>

                                        <div class='p-1'>
                                            <button type='button'  id='qc_passed_button' class='btn btn-outline-wc rounded-pill' onclick='qc_passed(\"".$brief_id."\",\"".$image_id."\")'>QC Passed</button>
                                        </div>

                                        <div class='p-1' >
                                            <button type='button' id='qc_failed_button' class='btn btn-outline-danger rounded-pill' onclick='qc_failed(\"".$brief_id."\",\"".$image_id."\")'>QC Failed</button>
                                            </div>
                                    </div>

                                    <div class='p-1 d-none' id='qc_failed_div'>
                                        <p>Reason for QC Failed:</p>
                                        <textarea id='reason_qc' rows='4' name='reason_qc' class='form-control rounded mb-2'></textarea>

                                        <button type='submit' class='btn btn-outline-danger btn-block px-5 mb-2' onclick='qc_failed_Confirm(\"".$brief_id."\",\"".$image_id."\")'>Confirm</button>
                                        <button type='submit' class='btn btn-outline-wc btn-block px-5 mb-2' onclick='qc_failed_cancel(\"".$brief_id."\",\"".$image_id."\")'>Cancel</button>
                                    </div>
                                </div>

                                <div class='d-none' id='qc_failed_upload'>
                                    <form action='' method='post' enctype='multipart/form-data' id='upldForm' name='upldForm".$image_id."'>
                                        <input type='hidden' name='brief_id' id='brief_id' value='".$brief_id."'>
                                        <input type='hidden' name='image_id' id='image_id' value='".$image_id."'>
                                        
                                        <label for='files' class='btn btn-outline-wc w-100 px-5 mb-2'><i class='fas fa-upload'></i>&nbsp;Upload Revised File</label>
                                        
                                       <input type='file' class='upload_revised_file_ooo d-none' accept='image/jpg,image/jpeg,image/png,video/mp4' name='files'  id='files' onchange='readURL(this)'/>
                                    </form>
                                    <div id='preview' class='w-100 d-block'>

                                    </div>
                                    <div class='alert alert-success text-center lead d-none' id='imageSuccess' role='alert'><i class='far fa-thumbs-up fa-2x'></i><br><span>File Successfully Uploaded</span></div>    
                                    <div class='alert alert-danger text-center lead d-none' id='imageDanger' role='alert'><i class='far fa-thumbs-down fa-2x'></i><br><span>Please Upload Image/Video File Only</span></div>
                                    <button type='submit' class='btn btn-outline-wc btn-lg m-2 px-5 w-100 d-none' id='img_upload' onclick='uploadFile()'>".$img_upload_button."</button>
                                </div>
                            </div>";
                        }
                 else if($image_upload_rs1[0]['img_status']==6 &&  ($user_type_id=='3' || $user_type_id=='5'|| $user_type_id=='6')){
                   // echo "ahddfk;ljasd";
                   
                    $html.="
                    <div class='py-4 p-1 w-100'>
                            <div class='d-flex align-items-start flex-column' style='height: 525px;'>
                                <div class='mb-auto w-100'>
                                    <div class='p-1'><button type='button' id='qc_failed_button' class='btn btn-danger rounded-pill'>QC Failed</button></div>

                                    <div class='p-1'> 
                                        <p class='lead  mb-2 p-2'>Reason for QC Failed:</p> 
                                        <p class='border rounded mb-2 p-2' >".$image_upload_rs1[0]['reason_qc']."</p>
                                    </div>
                                </div>

                               <div class='w-100' id='qc_failed_upload'>
                                    <form action='' method='post' enctype='multipart/form-data' id='upldForm' name='upldForm".$image_id."'>
                                        <input type='hidden' name='brief_id' id='brief_id' value='".$brief_id."'>
                                        <input type='hidden' name='image_id' id='image_id' value='".$image_id."'>
                                        
                                        <label for='files' class='btn btn-outline-wc w-100 px-5 mb-2'><i class='fas fa-upload'></i>&nbsp;Upload Revised File</label>
                                        
                                       <input type='file' class='upload_revised_file_ooo d-none' accept='image/jpg,image/jpeg,image/png,video/mp4' name='files'  id='files' onchange='readURL(this)'/>
                                    </form>
                                    <div id='preview' class='w-100 d-block'>

                                    </div>
                                    <div class='alert alert-success text-center lead d-none' id='imageSuccess' role='alert'><i class='far fa-thumbs-up fa-2x'></i><br><span>File Successfully Uploaded</span></div>    
                                    <div class='alert alert-danger text-center lead d-none' id='imageDanger' role='alert'><i class='far fa-thumbs-down fa-2x'></i><br><span>Please Upload Image/Video File Only</span></div>
                                    <button type='submit' class='btn btn-outline-wc btn-lg m-2 px-5 w-100 d-none' id='img_upload' onclick='uploadFile()'>".$img_upload_button."</button>
                                </div>
                            </div>
                        </div>
                    ";
                        }
                
                
                // }
                // else if($rs[0]['img_status']==3)
                // {
                //     $html.="<button type='submit' class='btn btn-outline-wc btn-block px-5 mb-2'>".$brief_title ."Sent&nbsp;<i class='fas fa-sync-alt'></i></button>";
                // }

                 /*<div class='alert alert-danger text-center lead d-none' id='imageDanger' role='alert'><i class='far fa-thumbs-down fa-2x'></i><span id='imagetxtDanger'></span></div>*/
                            $html.="
                            </div>
                        </div>
<div class='alert alert-danger text-center lead d-none' id='imageDanger' role='alert'><i class='far fa-thumbs-down'></i>&nbsp;<span id='imagetxtDanger'></span></div>



                    </div>

                    ";

            echo $html;
           
        }
        else
        {
            $data['user_id'] = '';
            $data['user_name'] = '';
            $user_id='';
            redirect('login', 'refresh');
        }

        
    }
    public function annotation_list()
    {
         if ($this->session->userdata('front_logged_in')) {
            $session_data = $this->session->userdata('front_logged_in');
            $data['user_id'] = $session_data['user_id'];
            $data['user_name'] = $session_data['user_name'];
            $user_id = $session_data['user_id'];
            $user_type_id=$session_data['user_type_id'];

            //$data['brief_id'] = $brief_id;
            //$change="change";
            $data['title'] = "Proofing";
            $image_id=preg_replace('/[^a-zA-Z0-9_ -]/s','',$_POST['image_id']);
           // print_r($_POST['select_value']);
            if(isset($_POST['select_value'])=='change')
            {
                $query= $this->db->query("SELECT * FROM wc_image_annotation WHERE image_id='$image_id' and (img_an_status='0' || img_an_status='1')");
                $rs= $query->result_array();
               /// print_r("SELECT * FROM wc_image_annotation WHERE image_id='$image_id' and (img_an_status='0' || img_an_status='1')");
            }
            else if(!isset($_POST['select_value'])){
                $query= $this->db->query("SELECT * FROM wc_image_annotation WHERE image_id='$image_id' and img_an_status='0'");
                $rs= $query->result_array();
               
            }
            else{
                $query= $this->db->query("SELECT * FROM wc_image_annotation WHERE image_id='$image_id' and img_an_status='0'");
                $rs= $query->result_array();
            }
            // $query= $this->db->query("SELECT * FROM wc_image_annotation WHERE image_id='$image_id' and img_an_status='0'");
            // $rs= $query->result_array();
            //print_r($rs);


            $query1= $this->db->query("SELECT * FROM wc_image_upload WHERE image_id='$image_id'");
            $rs1= $query1->result_array();
            //echo $rs1[0]['img_status'];

            $count=$query->num_rows();
            $data['boxes'] = '';
            $data['lists'] = '';
            $data['notes'] = "Notes - Revision".$rs1[0]['version_num'] ;
            $data['send_revision']="";
            $i=1;
            //if($count>0)
            if($count>0  && $rs1[0]['img_status']!=2 &&  ($user_type_id=='1' || $user_type_id=='2' || $user_type_id=='3' || $user_type_id=='5' || $user_type_id=='6'))
            {
                if ($rs){
                    foreach ($rs as $rs_key => $rs_value) {

                        $file_type=$rs_value['file_type'];
                        if($file_type=='1')
                        {
                            $style_display="visibility: hidden;";
                        }
                        else
                        {
                             $style_display="";
                        }
                         if($rs_value['apply_all']==1){
                            $text_wc = "text-wc"; 
                            $check="";
                        }else if($rs_value['apply_all']==0){
                            $text_wc = "text-wc-light";
                            $check="d-none";
                        }
                     //echo $rs_value['id'];
                   
                    // do{
                    //  echo $rs_value['id'];
                        $data['boxes'] .= '<div class="tagview" style="left:' . $rs_value['position_left'] . 'px;top:' . $rs_value['position_top'] . 'px;" >';
                        $data['boxes'] .= '<div class="square"></div>';
                        $data['boxes'] .= '<div class="person popup" style="left:' . $rs_value['position_left'] . 'px;top:' . $rs_value['position_top']  . 'px;" id="view_'.$rs_value['id'].'">' . $rs_value['descr'] . '</div>';
                        $data['boxes'] .= '</div>';

                        $data['lists'] .= '<p id="'.$rs_value['id'].'" class="d-flex justify-content-between border-wc rounded p-1 mb-3">                        
                       
                        <a class="name text-dark p-2" style="width: 70%;" id="span_name' . $rs_value['id'] . '">' . $rs_value['descr'] . '</a>
                        <textarea class="form-control input-sm d-none" style="width: 80%" type="text" id="name'.$rs_value['id'].'" name="name" >'. $rs_value['descr'] . '</textarea>
                        ';
                        $data['lists'] .='';
                        if($rs1[0]['img_status']=='2' && $rs_value['img_an_status']=='0' && ($user_type_id=='1'||$user_type_id=='2'||$user_type_id=='4')){
                        $data['lists'] .='<span>';

                        if($file_type=='0' && ($rs_value['id']==$rs_value['parent_id']))
                        {
                           
                        $data['lists'] .='
                        <button type="button" id="editBtn'.$rs_value['id'].'" class="btn btn-default btn-lg p-0 edit_button'. $image_id.'" onclick="edit_button('.$rs_value['id'].')"'.$style_display.' " title="Edit">
                        <i class="far fa-edit text-wc"></i>
                        </button> 
                        
                        <button type="button" id="updateBtn'.$rs_value['id'].'" class="btn btn-default btn-lg p-0 d-none" onclick="update_button('.$rs_value['image_id'].','.$rs_value['id'].','. $rs_value['position_left'].','. $rs_value['position_top'].')" title="Update"><i class="far fa-check-circle text-wc"></i></button> 

                        <button type="button" id="cancleBtn'.$rs_value['id'].'" class="btn btn-default btn-lg p-0 d-none" onclick="cancle_button('.$rs_value['id'].')" title="Cancel"><i class="far fa-times-circle text-wc" aria-hidden="true"></i></button> 

                        <button type="button" class="btn btn-default btn-lg p-0 applyall_button'. $image_id.'" id="annotation_all'.$rs_value['id'].'"  onclick=annotation_all(\''.$image_id.'\',\''.$rs_value['id'].'\',\''.$rs_value['apply_all'].'\')><i class="fab fa-buffer '. $text_wc.'"></i><span class="bg-wc text-white rounded-circle on_ico '. $check.'  ">on</span></button>
                            
                        <button type="button" class="btn btn-default btn-lg p-0 remove_button'. $image_id.'" id="delete_'.$rs_value['id'].'" onclick="remove1('.$rs_value['id'].')" style="'.$style_display.' " title="Delete">
                        <i class="far fa-trash-alt text-wc"></i>
                        </button> 

                        <button type="button" id="remove_'.$rs_value['id'].'" class="btn btn-default btn-lg p-0 d-none" onclick="remove('.$rs_value['id'].','.$rs_value['image_id'].')" title="Delete Me"><i class="far fa-check-circle text-wc"></i></button>

                        <button type="button" id="cancleBtn1'.$rs_value['id'].'" class="btn btn-default btn-lg p-0 d-none" onclick="cancle_button1('.$rs_value['id'].')"title=" Don\'t Delete Me"><i class="far fa-times-circle text-wc" ></i></button> 
                        ';
                             }
                        $data['lists'] .='
                        </span>
                       
                            ';
                      
                        }
                       // echo $rs_value['img_an_status'];
                        if($user_type_id=='1'||$user_type_id=='2'||$user_type_id=='3'){
                            if($rs_value['img_an_status']=='0'){
                                $data['send_revision']='1';
                            }
                            else{
                                $data['send_revision']='0';
                            }
                        }
                        else if($user_type_id=='1'||$user_type_id=='2'||$user_type_id=='4'){
                           if($rs_value['img_an_status']=='1'){
                                $data['send_revision']='0';
                            }
                            else{
                                $data['send_revision']='1';
                            }
                        }

                        $data['lists'] .= ' 
                        </p>';
                       $i++;
                    // }while($rs = $query->result_array());
                    }
                }
            }
            else if($count>0  &&  ($user_type_id=='4'))
            {
                if ($rs){
                    foreach ($rs as $rs_key => $rs_value) {

                        $file_type=$rs_value['file_type'];
                        if($file_type=='1')
                        {
                            $style_display="visibility: hidden;";
                        }
                        else
                        {
                             $style_display="";
                        }
                         if($rs_value['apply_all']==1){
                            $text_wc = "text-wc"; 
                            $check="";
                        }else if($rs_value['apply_all']==0){
                            $text_wc = "text-wc-light";
                            $check="d-none";
                        }
                     //echo $rs_value['id'];
                   
                    // do{
                    //  echo $rs_value['id'];
                        $data['boxes'] .= '<div class="tagview" style="left:' . $rs_value['position_left'] . 'px;top:' . $rs_value['position_top'] . 'px;" >';
                        $data['boxes'] .= '<div class="square"></div>';
                        $data['boxes'] .= '<div class="person popup" style="left:' . $rs_value['position_left'] . 'px;top:' . $rs_value['position_top']  . 'px;" id="view_'.$rs_value['id'].'">' . $rs_value['descr'] . '</div>';
                        $data['boxes'] .= '</div>';

                        $data['lists'] .= '<p id="'.$rs_value['id'].'" class="d-flex justify-content-between border-wc rounded p-1 mb-3">                        
                       
                        <a class="name text-dark p-2" style="width: 70%;" id="span_name' . $rs_value['id'] . '">' . $rs_value['descr'] . '</a>
                        <textarea class="form-control input-sm d-none" style="width: 80%" type="text" id="name'.$rs_value['id'].'" name="name" >'. $rs_value['descr'] . '</textarea>
                        ';
                        $data['lists'] .='';
                        if($rs1[0]['img_status']=='2' && $rs_value['img_an_status']=='0' && ($user_type_id=='1'||$user_type_id=='2'||$user_type_id=='4')){
                        $data['lists'] .='<span>';

                        if($file_type=='0' && ($rs_value['id']==$rs_value['parent_id']))
                        {
                           
                        $data['lists'] .='
                        <button type="button" id="editBtn'.$rs_value['id'].'" class="btn btn-default btn-lg p-0 edit_button'. $image_id.'" onclick="edit_button('.$rs_value['id'].')"'.$style_display.' " title="Edit">
                        <i class="far fa-edit text-wc"></i>
                        </button> 
                        
                        <button type="button" id="updateBtn'.$rs_value['id'].'" class="btn btn-default btn-lg p-0 d-none" onclick="update_button('.$rs_value['image_id'].','.$rs_value['id'].','. $rs_value['position_left'].','. $rs_value['position_top'].')" title="Update"><i class="far fa-check-circle text-wc"></i></button> 

                        <button type="button" id="cancleBtn'.$rs_value['id'].'" class="btn btn-default btn-lg p-0 d-none" onclick="cancle_button('.$rs_value['id'].')" title="Cancel"><i class="far fa-times-circle text-wc" aria-hidden="true"></i></button> 

                        <button type="button" class="btn btn-default btn-lg p-0 applyall_button'. $image_id.'" id="annotation_all'.$rs_value['id'].'"  onclick=annotation_all(\''.$image_id.'\',\''.$rs_value['id'].'\',\''.$rs_value['apply_all'].'\')><i class="fab fa-buffer '. $text_wc.'"></i><span class="bg-wc text-white rounded-circle on_ico '. $check.'  ">on</span></button>
                            
                        <button type="button" class="btn btn-default btn-lg p-0 remove_button'. $image_id.'" id="delete_'.$rs_value['id'].'" onclick="remove1('.$rs_value['id'].')" style="'.$style_display.' " title="Delete">
                        <i class="far fa-trash-alt text-wc"></i>
                        </button> 

                        <button type="button" id="remove_'.$rs_value['id'].'" class="btn btn-default btn-lg p-0 d-none" onclick="remove('.$rs_value['id'].','.$rs_value['image_id'].')" title="Delete Me"><i class="far fa-check-circle text-wc"></i></button>

                        <button type="button" id="cancleBtn1'.$rs_value['id'].'" class="btn btn-default btn-lg p-0 d-none" onclick="cancle_button1('.$rs_value['id'].')"title=" Don\'t Delete Me"><i class="far fa-times-circle text-wc" ></i></button> 
                        ';
                             }
                        $data['lists'] .='
                        </span>
                       
                            ';
                      
                        }
                       // echo $rs_value['img_an_status'];
                        if($user_type_id=='1'||$user_type_id=='2'||$user_type_id=='3'){
                            if($rs_value['img_an_status']=='0'){
                                $data['send_revision']='1';
                            }
                            else{
                                $data['send_revision']='0';
                            }
                        }
                        else if($user_type_id=='1'||$user_type_id=='2'||$user_type_id=='4'){
                           if($rs_value['img_an_status']=='1'){
                                $data['send_revision']='0';
                            }
                            else{
                                $data['send_revision']='1';
                            }
                        }

                        $data['lists'] .= ' 
                        </p>';
                       $i++;
                    // }while($rs = $query->result_array());
                    }
                }
            }
            else{
                if($user_type_id=='1'||$user_type_id=='2'||$user_type_id=='4'){

                    if($rs1[0]['file_type']=='1')
                    {
                        $data['lists'] .= '<span class="lead">Drag drop dustom text/HTML on video to add the revision ..</span>';

                    }
                    else
                    {
                        $data['lists'] .= '<span class="lead">Click on the image to add the revision ..</span>';

                    }

                    



                    $data['send_revision']='0';
                }
                else if($user_type_id=='3'){
                    $data['lists'] .= '<span class="lead">BM Reviewing the Images... </span>';
                }
            }

           echo json_encode( $data );
           
        }
        else
        {
            $data['user_id'] = '';
            $data['user_name'] = '';
            $user_id='';
            redirect('login', 'refresh');
        }
    }
    public function annotation_all(){
        $brief_id = preg_replace('/[^a-zA-Z0-9_ -]/s','',$_POST['brief_id']); 
       
        $image_id=preg_replace('/[^a-zA-Z0-9_ -]/s','',$_POST['image_id']);
        $annotation_id=preg_replace('/[^a-zA-Z0-9_ -]/s','',$_POST['annotation_id']);
        $apply_all=$_POST['apply_all'];
        $checkquerys1=$this->db->query("select * from wc_image_annotation where  id='$annotation_id' and img_an_status='0'")->result_array();
       // print_r($checkquerys1);
        $annotation_desc = $checkquerys1[0]['descr'];
        $position_left = $checkquerys1[0]['position_left'];
        $position_top = $checkquerys1[0]['position_top'];
        $user_id=$checkquerys1[0]['user_id'];
        $parent_id=$checkquerys1[0]['parent_id']; 
        $create_date=date('Y/m/d');
        $array_image_id= "";
        //exit();
        if($apply_all==0){
            $files_sql="SELECT * FROM wc_image_upload WHERE brief_id='$brief_id' and img_status IN ('0','2') and  image_id IN (SELECT MAX(image_id) FROM wc_image_upload  GROUP BY parent_img )order by parent_img asc";
            $files_query = $this->db->query($files_sql);
            $files_result=$files_query->result_array();
            //print_r($files_result);
            foreach ($files_result as $key => $value) {

                $image_id1=$value['image_id'];
                $array_image_id.=$image_id1.",";
                $version_num=$value['version_num'];
    
                if($image_id!=$image_id1){
                    $sql = "INSERT INTO wc_image_annotation(`user_id`,`image_id`,`revision_id`,`descr`,  `position_left`, `position_top`,`parent_id`,`create_date`,`apply_all`)VALUES('$user_id','$image_id1','$version_num','$annotation_desc','$position_left','$position_top','$annotation_id','$create_date','1');";
                    // $annotation_id=$this->db->insert_id();
                    $qry =$this->db->query($sql);
                    
                  
                    $array = array('image_id' => $image_id1, 'brief_id' => $brief_id);
                    $img_status1= array('img_status' => '2');
                    $this->db->where($array);
                    $report = $this->db->update('wc_image_upload', $img_status1);
                 //  print_r($sql);
                }
                else if($image_id==$image_id1){
                    $sql = "UPDATE wc_image_annotation SET apply_all='1' WHERE id = $annotation_id and  img_an_status='0'";
                   //  print_r($sql);
                    $qry =$this->db->query($sql);
                }
                
            }
        }
        else if($apply_all==1){
            $files_sql="SELECT * FROM wc_image_upload WHERE brief_id='$brief_id' and img_status IN ('0','2') and image_id IN (SELECT MAX(image_id) FROM wc_image_upload  GROUP BY parent_img )order by parent_img asc";
            $files_query = $this->db->query($files_sql);
            $files_result=$files_query->result_array();
            //print_r($files_result);

            foreach ($files_result as $key => $value) {

                $image_id1=$value['image_id'];
                $array_image_id.=$image_id1.",";
                $version_num=$value['version_num'];
    
                if($image_id!=$image_id1){
                      $sql = "DELETE FROM wc_image_annotation WHERE image_id = '".$image_id1."'";
                      $qry =$this->db->query($sql);
                  
                }else if($image_id==$image_id1){
                    
                    $sql = "UPDATE wc_image_annotation SET apply_all='0' WHERE id = $annotation_id and img_an_status='0'";
                    $qry =$this->db->query($sql);
                }
                
            }
        }
       
        //if()
    }
    public function notes_update(){
        $brief_id =preg_replace('/[^a-zA-Z0-9_ -]/s','',$_POST['brief_id']); 
       $files_sql="SELECT * FROM wc_image_upload WHERE brief_id='$brief_id' and img_status IN ('0','2')  and  image_id IN (SELECT MAX(image_id) FROM wc_image_upload  GROUP BY parent_img )order by parent_img asc";
        $files_query = $this->db->query($files_sql);
        $files_result=$files_query->result_array();
       /* echo "<pre>";
        print_r($files_result);*/
        $array_image_id="";
        foreach ($files_result as $key => $value) {

            $image_id1=$value['image_id'];
            $array_image_id.=$image_id1.",";
        }
        $array_image_id=rtrim($array_image_id, ',');
        //print_r($array_image_id);
        
        $checkquerys1 = $this->db->query("SELECT * from wc_image_annotation where  image_id IN ($array_image_id) and apply_all='1' and img_an_status='0' GROUP by parent_id")->result_array();
           // print_r("SELECT * from wc_image_annotation where  image_id IN $array_image_id and apply_all='1' GROUP by parent_id");
           // echo "<pre>";
          // print_r($checkquerys1);
        $descr='';

    if(!empty($checkquerys1)){ 
        foreach ($checkquerys1 as $key => $value) {
        $descr.=$value['descr']."\n";
      }
      
       $html="";
       $html="<div class='w-100 p-3'>
               <h4>Notes:</h4>
                <textarea class='form-control' rows='10'>".$descr."</textarea> 
               </div>";
       echo $html;
     }
     else{
        echo "";
     }
    }
    public function annotation_status()
    {
        $image_id=preg_replace('/[^a-zA-Z0-9_ -]/s','',$_POST['image_id']);
        $array = array('image_id' => $image_id);
        $img_status1= array('img_an_status' => '1');
        $this->db->where($array);
        $report = $this->db->update('wc_image_annotation', $img_status1);
        $sql = $this->db->last_query();
        echo $report; 
    }
    public function annotation_crud()
    {
         if ($this->session->userdata('front_logged_in')) {
            $session_data = $this->session->userdata('front_logged_in');
            $data['user_id'] = $session_data['user_id'];
            $data['user_name'] = $session_data['user_name'];
            $user_id = $session_data['user_id'];
            $user_type_id=$session_data['user_type_id'];            
            // $data['brief_id'] = $brief_id;
            $data['title'] = "Proofing";
            if( !empty( $_POST['type'] ) && $_POST['type'] == "insert" )
            {
                $image_id = preg_replace('/[^a-zA-Z0-9_ -]/s','',$_POST['image_id']); 
                $descr = addslashes($_POST['descr']);
                $position_left = $_POST['position_left'];
                $position_top = $_POST['position_top'];
               
                $create_date=date('Y/m/d');

                $checkquerys1 = $this->db->query("select * from wc_image_upload where  image_id='$image_id' ")->result_array();
                $version_num=$checkquerys1[0]['version_num'];


                $sql = "INSERT INTO wc_image_annotation(`user_id`,`image_id`,`revision_id`,`descr`,  `position_left`, `position_top`, `create_date`)VALUES('$user_id','$image_id','$version_num','$descr','$position_left','$position_top','$create_date');";

                // print_r($sql);
                $qry =$this->db->query($sql);
                 $annotation_id=$this->db->insert_id();
                $sql = "UPDATE wc_image_annotation SET parent_id='$annotation_id' WHERE id = $annotation_id ";
                // print_r("UPDATE gd_image_annotation SET descr='$descr' WHERE id = $id && position_left=$position_left && position_top=$position_top");
                $qry =$this->db->query($sql);
            }

            if( !empty( $_POST['type'] ) && $_POST['type'] == "remove")
            {
              $id =preg_replace('/[^a-zA-Z0-9_ -]/s','',$_POST['id']);
              $checkquerys1 = $this->db->query("select * from wc_image_annotation where  id='$id' ")->result_array();
              $parent_id=$checkquerys1[0]['parent_id']; 
              $sql = "DELETE FROM wc_image_annotation WHERE parent_id = '".$parent_id."'";
              $qry =$this->db->query($sql);
            }
            if( !empty( $_POST['type'] ) && $_POST['type'] == "update")
            {
              

              $id =preg_replace('/[^a-zA-Z0-9_ -]/s','', $_POST['id']); 
              $image_id =preg_replace('/[^a-zA-Z0-9_ -]/s','', $_POST['image_id']); 
              $descr = $_POST['descr'];
              $position_left = $_POST['position_left'];
              $position_top = $_POST['position_top'];

              $checkquerys1 = $this->db->query("select * from wc_image_annotation where  id='$id' ")->result_array();
              $parent_id=$checkquerys1[0]['parent_id']; 
              $sql = "UPDATE wc_image_annotation SET descr='$descr' WHERE parent_id=$parent_id && position_left=$position_left && position_top=$position_top";
             // print_r("UPDATE gd_image_annotation SET descr='$descr' WHERE id = $id && position_left=$position_left && position_top=$position_top");
               $qry =$this->db->query($sql);
            }

        }
        else
        {
            $data['user_id'] = '';
            $data['user_name'] = '';
            $user_id='';
            redirect('login', 'refresh');
        }
        
    }
    public function select_revision()
    {
        $brief_id=preg_replace('/[^a-zA-Z0-9_ -]/s','',$_POST['brief_id']);
        $image_id=preg_replace('/[^a-zA-Z0-9_ -]/s','',$_POST['image_id']);
        $wc_brief_sql="SELECT * FROM wc_image_upload WHERE brief_id='$brief_id' and image_id='$image_id'";

        $wc_brief_query = $this->db->query($wc_brief_sql);
        $wc_brief_result=$wc_brief_query->result_array();
        $parent_img= $wc_brief_result[0]['parent_img'];



        $wc_brief_sql1 = "SELECT * FROM `wc_image_upload` where brief_id='$brief_id' and parent_img='$parent_img' order by version_num desc";
       // print_r($wc_brief_sql1);
        $wc_brief_query1 = $this->db->query($wc_brief_sql1);

        $wc_brief_result1=$wc_brief_query1->result_array();
        //print_r($wc_brief_result1);
        $html="";
        $html.="<select class='custom-select custom-select-sm img_filter' onchange='revision_filter(\"".$brief_id."\",this.value)'>";
        foreach($wc_brief_result1 as $key => $value){ 
            $image_id=$value['image_id'];
            $version_num=$value['version_num'];
                            
            $html.="<option  value='".$image_id."'> Revision". $version_num."</option>";
        }
        $html.="</select> ";
         echo $html;
    }

    public function filter_status()
    {
        $filter_status = preg_replace('/[^a-zA-Z0-9_ -]/s','', $_POST['filter_status']);
        $get_image_id =preg_replace('/[^a-zA-Z0-9_ -]/s','', $_POST['image_id']);
        $brief_id = preg_replace('/[^a-zA-Z0-9_ -]/s','',$_POST['brief_id']);
        $select_value="";

        //echo "<pre>";
       

        $checkquerys = $this->db->query("select * from wc_brief where brief_id='$brief_id' ")->result_array();
        $added_by_user_id=$checkquerys[0]['added_by_user_id'];
        $brand_id=$checkquerys[0]['brand_id'];
        $brief_title=$checkquerys[0]['brief_title'];
        $brief_name=preg_replace('/[^a-zA-Z0-9_.]/', '_',$brief_title);

    

         $checkquerys = $this->db->query("select wc_brands.*,wc_clients.client_name from wc_brands
            inner join wc_clients on wc_brands.client_id=wc_clients.client_id
            where wc_brands.brand_id='$brand_id' ")->result_array();

        $brand_id=$checkquerys[0]['brand_id'];
        $brand_name=preg_replace('/[^a-zA-Z0-9_.]/', '_',$checkquerys[0]['brand_name']);
        $client_id=$checkquerys[0]['client_id'];
        $client_name=preg_replace('/[^a-zA-Z0-9_.]/', '_',$checkquerys[0]['client_name']);


        //$dst = "upload/".$client_id."-".$client_name."/".$brand_id."-".$brand_name."/".$brief_id."-brief";  
        // $files_sql = "SELECT * FROM `wc_image_upload` where brief_id='$brief_id' group by parent_img";
        if($filter_status=="all")
        {
            $files_sql="SELECT * FROM wc_image_upload WHERE brief_id='$brief_id' and image_id IN (SELECT MAX(image_id) FROM wc_image_upload  GROUP BY parent_img )order by parent_img asc";
        }
        else{
            /*$files_sql="SELECT * FROM wc_image_upload WHERE brief_id='$brief_id' and img_status='$filter_status' and image_id IN (SELECT MAX(image_id) FROM wc_image_upload  GROUP BY parent_img )order by parent_img asc";*/

            if($filter_status=="0") {
               /*$files_sql= "SELECT * from wc_image_upload left join wc_image_annotation on wc_image_annotation.image_id=wc_image_upload.image_id where wc_image_upload.brief_id='$brief_id'  and wc_image_annotation.image_id IS NULL OR wc_image_upload.image_id IS NULL GROUP BY wc_image_upload.parent_img";*/

               $files_sql="select wc_image_upload.* from wc_image_upload where brief_id='$brief_id' and img_status in('2','0') GROUP BY wc_image_upload.parent_img";
            }
        
            if($filter_status=='2')
            {

               /* $files_sql="SELECT * from  wc_image_annotation join wc_image_upload on wc_image_annotation.image_id=wc_image_upload.image_id  where wc_image_annotation.img_an_status='0' and wc_image_upload.brief_id='$brief_id' and wc_image_upload.img_status!='4' GROUP BY wc_image_upload.parent_img";*/

               $files_sql="select wc_image_upload.* from wc_image_upload where brief_id='$brief_id' and img_status='3' GROUP BY wc_image_upload.parent_img";

            }
            if($filter_status=='5')
            {

               /* $files_sql="SELECT * from  wc_image_annotation join wc_image_upload on wc_image_annotation.image_id=wc_image_upload.image_id  where wc_image_annotation.img_an_status='0' and wc_image_upload.brief_id='$brief_id' and wc_image_upload.img_status!='4' GROUP BY wc_image_upload.parent_img";*/

                $files_sql="select wc_image_upload.* from wc_image_upload where brief_id='$brief_id' and img_status in('5','6') GROUP BY wc_image_upload.parent_img";
            }
            if($filter_status=='1')
            {
                /*$files_sql="select * from wc_image_upload where brief_id='$brief_id' and img_status='1' and image_id IN (SELECT MAX(image_id) FROM wc_image_upload  GROUP BY parent_img )";*/

                $files_sql="select wc_image_upload.* from wc_image_upload where brief_id='$brief_id' and img_status='1' GROUP BY wc_image_upload.parent_img";


            }
            else
            {
                /*$files_sql="select * from wc_image_upload where brief_id='$brief_id' and img_status='1' and image_id IN (SELECT MAX(image_id) FROM wc_image_upload  GROUP BY parent_img )";*/

               $files_sql="select wc_image_upload.* from wc_image_upload where brief_id='$brief_id' and img_status='$filter_status' GROUP BY wc_image_upload.parent_img";


            }
            
        }
        $files_query = $this->db->query($files_sql);
        $files_result=$files_query->result_array();
        $file_count=$files_query->num_rows();
        //print_r($file_count);
        $html="";
        if($file_count>='1'){
            foreach($files_result as $keybill => $files_result1) {

               $img_status=$files_result1['img_status'];
                $image_path=$files_result1['image_path'];
                $img_title=$files_result1['img_title'];
                $image_id=$files_result1['image_id'];
                 if($get_image_id==$image_id){
                    $slctcard = "border-wc";
                    $selected="bg-wc-light";
                    $txtslct = "text-wc font-weight-bold";
                }
                else{
                    $slctcard = "";
                    $selected="";
                    $txtslct = "";
                }
                $checkquerys1 = $this->db->query("select * from wc_image_upload where  image_id='$image_id' ")->result_array();
                $parent_img=$checkquerys1[0]['parent_img'];
                $version_num=$checkquerys1[0]['version_num'];
                $file_type=$checkquerys1[0]['file_type'];


                $dst = "upload/".$client_id."-".$client_name."/".$brand_id."-".$brand_name."/".$brief_id."-".$brief_name."/".$parent_img."/Revision".$version_num;  
                //$files_name=explode('.', $image_path);
                // $img_status=$files_result1['status'];
                //print_r($files_name[0]);
                $image_name= explode(".", $files_result1['image_path']);

                 $query1= $this->db->query("SELECT * FROM wc_image_annotation WHERE image_id='$image_id'");
                $annotation_count=$query1->num_rows();
                $html.="<div class='col-12 col-sm-12 col-md-6 col-lg-3 p-2'>
                            <div class='card ".$slctcard."'>
                                <div class='thumb-div' style='background-image: url(".base_url().$dst."/thumb_".$image_path.") !important;' onclick='useimg(\"".$brief_id."\",\"".$image_path."\",\"".$image_id."\",\"".$img_status."\",\"".$select_value."\",\"".$file_type."\",\"".$version_num."\")'></div>
                                <img id='temp_image_download".$image_id."' class='card-img-top img_download1 d-none' src='".base_url().$dst."/thumb_".$image_path."' alt='".$img_title."' onclick='useimg(\"".$brief_id."\",\"".$image_path."\",\"".$image_id."\",\"".$img_status."\",\"".$select_value."\",\"".$file_type."\",\"".$version_num."\")'> 
                                  <div class='card-body ".$selected."'>
                                        <div class='d-flex justify-content-between'>
                                            <span class='lead ".$txtslct."' style='width: 70%; white-space:nowrap; overflow:hidden; text-overflow: ellipsis;'>".substr($image_name[0], 0, -11)."</span><div id='icon_update_".$image_id."'>";
                                            if($img_status=="1") {
                                               $html.=" <i data-toggle='tooltip' data-placement='bottom' title='Approved' class='fas fa-circle text-wc fa-2x'></i>";
                                            }elseif($img_status=="2" ||$img_status=="0") {
                                                    $html.=" <i data-toggle='tooltip' data-placement='bottom' title='Approval pending' class='fas fa-circle text-secondary fa-2x'></i>";
                                               
                                               
                                            }elseif($img_status=="3"){
                                               $html.=" <i data-toggle='tooltip' data-placement='bottom' title='to be revised' class='fas fa-circle text-danger fa-2x'></i>";
                                            } elseif($img_status=='5' ||$img_status=="6"){
                                                 $html.=" <i data-toggle='tooltip' data-placement='bottom' title='Sent for QC' class='fas fa-circle text-warning fa-2x'></i>";
                                                
                                            }
                                $html.="</div>
                                        </div>
                                    </div>
                            </div>
                        </div>";           
            }
                               
            echo $html;
        }
        else{
            $html.="<span class='lead'>No Result</span>";
             echo $html;
        }

    }  

    public function icon_update(){
        $img_status=preg_replace('/[^a-zA-Z0-9_ -]/s','',$_POST['img_status']);
        $image_id=preg_replace('/[^a-zA-Z0-9_ -]/s','',$_POST['image_id']);
        $wc_brief_sql="SELECT * FROM wc_image_upload WHERE image_id='$image_id'";

        $wc_brief_query = $this->db->query($wc_brief_sql);
        $wc_brief_result=$wc_brief_query->result_array();
        $version_num= $wc_brief_result[0]['version_num'];
         $query1= $this->db->query("SELECT * FROM wc_image_annotation WHERE image_id='$image_id'");
                $annotation_count=$query1->num_rows();
        $html="";
        if($img_status=="1") {
           $html=" <i data-toggle='tooltip' data-placement='bottom' title='Approved' class='fas fa-circle text-wc fa-2x'></i>";
        }elseif($img_status=="2") {
            if($version_num>'1'  && $annotation_count==0){
                $html=" <i data-toggle='tooltip' data-placement='bottom' title='Approval pending' class='fas fa-circle text-secondary fa-2x'></i>";
            }else{
                $html=" <i data-toggle='tooltip' data-placement='bottom' title='to be revised' class='fas fa-circle text-wc fa-2x'></i>";
            } 
           
        }elseif($img_status=="3"){
           $html=" <i data-toggle='tooltip' data-placement='bottom' title='to be revised' class='fas fa-circle text-wc fa-2x'></i>";
        }
        echo $html;
    }

    public function compare_button()
    {
        $brief_id=preg_replace('/[^a-zA-Z0-9_ -]/s','',$_POST['brief_id']);
        $image_id=preg_replace('/[^a-zA-Z0-9_ -]/s','',$_POST['image_id']);
        $wc_brief_sql="SELECT * FROM wc_image_upload WHERE brief_id='$brief_id' and image_id='$image_id'";

        $wc_brief_query = $this->db->query($wc_brief_sql);
        $wc_brief_result=$wc_brief_query->result_array();
        $parent_img= $wc_brief_result[0]['parent_img'];



        $wc_brief_sql1 = "SELECT * FROM `wc_image_upload` where brief_id='$brief_id' and parent_img='$parent_img'";
       // print_r($wc_brief_sql1);
        $wc_brief_query1 = $this->db->query($wc_brief_sql1);

        $wc_brief_result1=$wc_brief_query1->result_array();
        //print_r($wc_brief_result1);
        $html="";
        $count=0;


         $checkquerys = $this->db->query("select * from wc_brief where brief_id='$brief_id' ")->result_array();
            $added_by_user_id=$checkquerys[0]['added_by_user_id'];
            $brand_id=$checkquerys[0]['brand_id'];
            $brief_title=$checkquerys[0]['brief_title'];
            $brief_name=preg_replace('/[^a-zA-Z0-9_.]/', '_',$brief_title);

            /*$checkquerys = $this->db->query("select wc_brands.*,wc_clients.client_name from wc_users
            inner join wc_brands on wc_users.brand_id=wc_brands.brand_id
            inner join wc_clients on wc_brands.client_id=wc_clients.client_id
            where wc_users.user_id='$added_by_user_id' ")->result_array();*/

            $checkquerys = $this->db->query("select wc_brands.*,wc_clients.client_name from wc_brands
            inner join wc_clients on wc_brands.client_id=wc_clients.client_id
            where wc_brands.brand_id='$brand_id' ")->result_array();

            $brand_id=$checkquerys[0]['brand_id'];
            $brand_name=preg_replace('/[^a-zA-Z0-9_.]/', '_',$checkquerys[0]['brand_name']);
            $client_id=$checkquerys[0]['client_id'];
            $client_name=preg_replace('/[^a-zA-Z0-9_.]/', '_',$checkquerys[0]['client_name']);

            // $checkquerys1 = $this->db->query("select * from wc_image_upload where  image_id='$image_id' ")->result_array();
            // $version_num=$checkquerys1[0]['version_num'];
            // $parent_img=$checkquerys1[0]['parent_img'];
        foreach($wc_brief_result1 as $key => $value){ 
            $image_id=$value['image_id'];
            $version_num=$value['version_num'];
            $image_path=$value['image_path'];
            $count=$count+1;
            $dst = "upload/".$client_id."-".$client_name."/".$brand_id."-".$brand_name."/".$brief_id."-".$brief_name."/".$parent_img;
            $image_org=$dst."/Revision".$version_num."/".$image_path;
        }
        
       $mime = mime_content_type($image_org);
        if(strstr($mime, "image/")){
            if($count>'1')
                {

                 /*echo $html="<div class='p-1'>
                                <button type='button' class='btn btn-outline-wc' onclick='compare(\"".$brief_id."\",\"".$parent_img."\")'>Compare</button>
                            </div>";*/

                echo $html="<button type='button' class='btn btn-outline-wc btn-sm compare_button' onclick='compare(\"".$brief_id."\",\"".$parent_img."\",\"".$image_id."\")'>Compare</button>
                            <button type='button' class='btn btn-outline-wc btn-sm reset_button d-none' onclick='compare_reset(\"".$brief_id."\",\"".$parent_img."\",\"".$image_id."\")'>Exit</button>
                 ";
                }
        }
      
    }
    public function compare()
    {
        if ($this->session->userdata('front_logged_in')) {
            $session_data = $this->session->userdata('front_logged_in');
            $data['user_id'] = $session_data['user_id'];
            $data['user_name'] = $session_data['user_name'];
            $user_id = $session_data['user_id'];
            $user_type_id=$session_data['user_type_id'];

           // $data['brief_id'] = $brief_id;
            $data['title'] = "Proofing";
            $brief_id=preg_replace('/[^a-zA-Z0-9_ -]/s','',$_POST['brief_id']);
            
             if(!empty(preg_replace('/[^a-zA-Z0-9_ -]/s','',$_POST['parent_img']))){            
                $parent_img=preg_replace('/[^a-zA-Z0-9_ -]/s','',$_POST['parent_img']);
            }
            else if(empty(preg_replace('/[^a-zA-Z0-9_ -]/s','',$_POST['parent_img']))){
                $image_id=preg_replace('/[^a-zA-Z0-9_ -]/s','',$_POST['image_id']);
                $wc_brief_sql="SELECT * FROM wc_image_upload WHERE brief_id='$brief_id' and image_id='$image_id'";

                $wc_brief_query = $this->db->query($wc_brief_sql);
                $wc_brief_result=$wc_brief_query->result_array();
                $parent_img= $wc_brief_result[0]['parent_img'];
            }
            $img_filter_value=preg_replace('/[^a-zA-Z0-9_ -]/s','',$_POST['img_filter_value']);
            $checkquerys = $this->db->query("select * from wc_brief where brief_id='$brief_id' ")->result_array();
            $added_by_user_id=$checkquerys[0]['added_by_user_id'];
            $brand_id=$checkquerys[0]['brand_id'];
            $brief_title=$checkquerys[0]['brief_title'];
            $brief_name=preg_replace('/[^a-zA-Z0-9_.]/', '_',$brief_title);

            /*$checkquerys = $this->db->query("select wc_brands.*,wc_clients.client_name from wc_users
            inner join wc_brands on wc_users.brand_id=wc_brands.brand_id
            inner join wc_clients on wc_brands.client_id=wc_clients.client_id
            where wc_users.user_id='$added_by_user_id' ")->result_array();*/


            $checkquerys = $this->db->query("select wc_brands.*,wc_clients.client_name from wc_brands
            inner join wc_clients on wc_brands.client_id=wc_clients.client_id
            where wc_brands.brand_id='$brand_id' ")->result_array();

            $brand_id=$checkquerys[0]['brand_id'];
            $brand_name=preg_replace('/[^a-zA-Z0-9_.]/', '_',$checkquerys[0]['brand_name']);
            $client_id=$checkquerys[0]['client_id'];
            $client_name=preg_replace('/[^a-zA-Z0-9_.]/', '_',$checkquerys[0]['client_name']);

            // $checkquerys1 = $this->db->query("select * from wc_image_upload where  image_id='$image_id' ")->result_array();
            // $version_num=$checkquerys1[0]['version_num'];
            // $parent_img=$checkquerys1[0]['parent_img'];
            

            $dst = "upload/".$client_id."-".$client_name."/".$brand_id."-".$brand_name."/".$brief_id."-".$brief_name."/".$parent_img;  
            $html="";

            $div_con="";

            
            $div_con.="<div data-type='data-type-image'>";


            $wc_brief_sql1 = "SELECT * FROM `wc_image_upload` where brief_id='$brief_id' and parent_img='$parent_img' order by image_id desc limit 1";
           // print_r($wc_brief_sql1);
            $wc_brief_query1 = $this->db->query($wc_brief_sql1);

            $wc_brief_result1=$wc_brief_query1->result_array();
            $image_path=$wc_brief_result1[0]['image_path'];   
            $version_num=$wc_brief_result1[0]['version_num'];
            $image_org=$dst."/Revision".$version_num."/org_".$image_path;
            $mime = mime_content_type($image_org);
                
            
                 
           $wc_brief_sql1 = "SELECT * FROM `wc_image_upload` where brief_id='$brief_id' and image_id='$img_filter_value'";
           // print_r($wc_brief_sql1);
            $wc_brief_query1 = $this->db->query($wc_brief_sql1);

            $wc_brief_result1=$wc_brief_query1->result_array();
            $image_path1=$wc_brief_result1[0]['image_path']; 
            $image_id=$wc_brief_result1[0]['image_id'];   
            $version_num1=$wc_brief_result1[0]['version_num'];
            $image_org1=$dst."/Revision".$version_num1."/org_".$image_path1;

            if($version_num!=$version_num1){
                $sen_num=$version_num1-1;

                $wc_brief_sql1 = "SELECT * FROM `wc_image_upload` where brief_id='$brief_id' and parent_img='$parent_img'  and  version_num='$sen_num' order by image_id  limit 1";
               // print_r($wc_brief_sql1);
                $wc_brief_query1 = $this->db->query($wc_brief_sql1);

                $wc_brief_result1=$wc_brief_query1->result_array();
                $image_path=$wc_brief_result1[0]['image_path'];   
                $version_num=$wc_brief_result1[0]['version_num'];
                $image_id1=$wc_brief_result1[0]['image_id'];   
                $image_org=$dst."/Revision".$version_num."/org_".$image_path;
                $mime = mime_content_type($image_org1);
                $div_con.="<div data-type='after'>";
            
                if(strstr($mime, "video/")){
                    $div_con.='<video id="video_'.$image_id1.'" width="100%" height="100%" controls>
                               <source src="'.base_url().$image_org1.'" type="video/mp4">Your browser does not support the video tag.
                                    </video>';
                }else if(strstr($mime, "image/")){
                    $div_con.="<img class='img_download after' id='image_".$image_id1."'  src='".base_url().$image_org1."'>";
                }
                
                    $div_con.="</div>";
                $div_con.="<div data-type='before'>";
                
                if(strstr($mime, "video/")){
                    $div_con.='<video id="video_'.$image_id.'" width="100%" height="100%" controls>
                               <source src="'.base_url().$image_org.'" type="video/mp4">Your browser does not support the video tag.
                                    </video>';
                }else if(strstr($mime, "image/")){
                    $div_con.="<img class='img_download before' id='image_".$image_id."' src='".base_url().$image_org."'>";
                }
                
                    $div_con.="</div>";
                
               
            }
            else{
                $wc_brief_sql1 = "SELECT * FROM `wc_image_upload` where brief_id='$brief_id' and parent_img='$parent_img' order by image_id desc limit 2";
               //print_r($wc_brief_sql1);
                $wc_brief_query1 = $this->db->query($wc_brief_sql1);

                $wc_brief_result1=$wc_brief_query1->result_array();
                //print_r($wc_brief_result1);
                $i=1;
               
                foreach($wc_brief_result1 as $key => $value){ 
                   $image_id=$value['image_id'];   
                    $version_num3=$value['version_num'];
                    $image_path3=$value['image_path'];            
                    $image_org3=$dst."/Revision".$version_num3."/org_".$image_path3;
                    $mime = mime_content_type($image_org3);
                    if($i=='2')
                    {
                        $div_con.="<div data-type='before'>";
                        $class_type="before";
                    }
                    else if($i=='1'){
                        $div_con.="<div data-type='after'>";
                         $class_type="after";
                    }
                    if(strstr($mime, "video/")){
                        $div_con.='<video id="video_'.$image_id.'" width="100%" height="100%" controls>
                                   <source src="'.base_url().$image_org3.'" type="video/mp4">Your browser does not support the video tag.
                                        </video>';
                    }else if(strstr($mime, "image/")){
                        $div_con.="<img class='img_download ".$class_type."' id='image_".$image_id."'  src='".base_url().$image_org3."'>";
                    }
                    $i++;
                    $div_con.="</div>";
                                  
                              
                }
                
                
            
            }
            
            $div_con.='</div>';


            
            echo($div_con);
           
        }
        else
        {
            $data['user_id'] = '';
            $data['user_name'] = '';
            $user_id='';
            redirect('login', 'refresh');
        }
    }
    public function download_image()
    {
        
        if ($this->session->userdata('front_logged_in')) {
            $session_data = $this->session->userdata('front_logged_in');
            $data['user_id'] = $session_data['user_id'];
            $data['user_name'] = $session_data['user_name'];
            $user_id = $session_data['user_id'];
            $user_type_id=$session_data['user_type_id'];
            $download_value=$_POST['download_value'];

            if($download_value=="download_single"){
               $image_src=$_POST['image_src'];
                $image_src=explode("upload",$image_src);
                
                $image_src="./upload".str_replace("thumb_","",$image_src[1]);
                //
                $dir =  microtime();
                if (!file_exists($dir)) {
                    $save_to =$dir."/Image_download".$dir."/";
                    mkdir($dir."/Image_download".$dir, 0777, true);
                    
                } 
                $zip_file ='Image_download'.time().'.zip';
                // Get real path for our folder
                $rootPath = realpath($dir);

                // Initialize archive object
                $zip = new ZipArchive();
                $zip->open($zip_file, ZipArchive::CREATE | ZipArchive::OVERWRITE);

                // Create recursive directory iterator
                /** @var SplFileInfo[] $files */
                $files = new RecursiveIteratorIterator(
                    new RecursiveDirectoryIterator($rootPath),
                    RecursiveIteratorIterator::LEAVES_ONLY
                );
            
                $image_name1=explode("/",$image_src);
                $image_name=$save_to.end($image_name1);
                // print_r("array------".$image_name1);
                // print_r("image_name ---------------".$image_name);
                if(!copy($image_src,$image_name)){
                      // echo "failed to copy ".$image_name;
                }
                else{
                     // echo "copied $image_name into ".$image_name;
                }
       
                // exit();
                foreach ($files as $name => $file)
                {
                    // Skip directories (they would be added automatically)
                    if (!$file->isDir())
                    {
                        // Get real and relative path for current file
                        $filePath = $file->getRealPath();
                        $relativePath = substr($filePath, strlen($rootPath) + 1);

                        // Add current file to archive
                        $zip->addFile($filePath, $relativePath);
                    }
                }
      
                //exit();
                //Zip archive will be created only after closing object
                $zip->close();
                 //It it's a file.
                 
                if(file_exists($dir))
                {
                    $this->deleteAll($dir);
                    echo $zip_file;
                }
                else{
                    echo $dir;
                }
            }
            else if($download_value=="download_all")
            {
                $image_src=$_POST['image_src']; 
               
               //  $image_name1=explode("/",$image_src);
               // print_r($image_name1);
               //  $image_folder="./".$image_name1[4]."/".$image_name1[5]."/".$image_name1[6]."/".$image_name1[7]."/";
               //   //print_r($image_folder);
                $dir =  microtime();
                if (!file_exists($dir)) {
                    $save_to =$dir."/Image_download".$dir."/";
                    mkdir($dir."/Image_download".$dir, 0777, true);
                    
                }
                $zip_file ='Image_download'.time().'.zip';
                // Get real path for our folder
                $rootPath = realpath($dir);

                // Initialize archive object
                $zip = new ZipArchive();
                $zip->open($zip_file, ZipArchive::CREATE | ZipArchive::OVERWRITE);

                // Create recursive directory iterator
                /** @var SplFileInfo[] $files */
                $files = new RecursiveIteratorIterator(
                    new RecursiveDirectoryIterator($rootPath),
                    RecursiveIteratorIterator::LEAVES_ONLY
                );
                $downloadimgid_array=explode(",",$image_src);
               // print_r($downloadimgid_array);
                foreach ($downloadimgid_array as $downloadimgid_array_key => $downloadimgid_array_value) {
                    $downloadimgid_array_value=explode("upload",$downloadimgid_array_value);
                    $downloadimgid_array_value="./upload".$downloadimgid_array_value[1];
                    $image_src1 =str_replace('thumb_', '',$downloadimgid_array_value);
                    //echo "image src   ". $image_src1;
                    //echo "<br>";
                    $image_name1=explode("/",$image_src1);
                    //print_r($image_name1);echo "<br>";
                    $image_name=$save_to.end($image_name1);
                    //print_r("image name ----- ".$image_name);echo "<br>";
                    if(!copy($image_src1,$image_name)){
                          // echo "failed to copy ".$image_name;
                    }
                    else{
                         // echo "copied $image_name into ".$image_name;
                    }
                }
                // $sku_thumb_images = glob($image_folder."*.{png,PNG,jpg,JPG}",GLOB_BRACE);
               
                // //     print_r($sku_thumb_images);
                // foreach($sku_thumb_images as $image){ 
                //    // print_r($image);
                //     $image_name1=explode("/",$image);
                //     $image_name=$save_to.end($image_name1);
                //     //print_r($image_name1);
                //     if(!copy($image,$image_name)){
                //           // echo "failed to copy ".$image_name;
                //     }
                //     else{
                //          // echo "copied $image_name into ".$image_name;
                //     }
                // }
                // exit();
                foreach ($files as $name => $file)
                {
                    // Skip directories (they would be added automatically)
                    if (!$file->isDir())
                    {
                        // Get real and relative path for current file
                        $filePath = $file->getRealPath();
                        $relativePath = substr($filePath, strlen($rootPath) + 1);

                        // Add current file to archive
                        $zip->addFile($filePath, $relativePath);
                    }
                }
          
                //exit();
                //Zip archive will be created only after closing object
                $zip->close();
                 //It it's a file.
                     
                if(file_exists($dir))
                {
                    $this->deleteAll($dir);
                    echo $zip_file;
                }
                else{
                    echo $dir;
                }
            }
        }
        else
        {
            $data['user_id'] = '';
            $data['user_name'] = '';
            $user_id='';
            redirect('login', 'refresh');
        }
        
       
    }
    public function deleteAll($str) {
        //It it's a file.
        if (is_file($str)) {
            //Attempt to delete it.
            return unlink($str);
        }
        //If it's a directory.
        elseif (is_dir($str)) {
            //Get a list of the files in this directory.
            $scan = glob(rtrim($str,'/').'/*');
            //Loop through the list of files.
            foreach($scan as $index=>$path) {
                //Call our recursive function.
               $this-> deleteAll($path);
            }
            //Remove the directory itself.
            return @rmdir($str);
        }
    }
public function upload_files_image(){
        if ($this->session->userdata('front_logged_in')) {
            $session_data = $this->session->userdata('front_logged_in');
            $data['user_id'] = $session_data['user_id'];
            $data['user_name'] = $session_data['user_name'];
            $user_id=$session_data['user_id'];
            //echo "<pre>";
            $brief_id=$_POST["brief_id"];
            if(isset($_POST['image_id'])){
                $image_id=preg_replace('/[^a-zA-Z0-9_ -]/s','',$_POST["image_id"]);   
            }
            
           
            $checkquerys = $this->db->query("select * from wc_brief where brief_id='$brief_id' ")->result_array();
            $added_by_user_id=$checkquerys[0]['added_by_user_id'];
            $brand_id=$checkquerys[0]['brand_id'];
            $brief_title=$checkquerys[0]['brief_title'];
            $brief_name=preg_replace('/[^a-zA-Z0-9_.]/', '_',$brief_title);


            /*$checkquerys = $this->db->query("select wc_brands.*,wc_clients.client_name from wc_users
            inner join wc_brands on wc_users.brand_id=wc_brands.brand_id
            inner join wc_clients on wc_brands.client_id=wc_clients.client_id
            where wc_users.user_id='$added_by_user_id' ")->result_array();*/
             $checkquerys = $this->db->query("select wc_brands.*,wc_clients.client_name from wc_brands
            inner join wc_clients on wc_brands.client_id=wc_clients.client_id
            where wc_brands.brand_id='$brand_id' ")->result_array();

            $brand_id=$checkquerys[0]['brand_id'];
            $brand_name=preg_replace('/[^a-zA-Z0-9_.]/', '_',$checkquerys[0]['brand_name']);
            $client_id=$checkquerys[0]['client_id'];
            $client_name=preg_replace('/[^a-zA-Z0-9_.]/', '_',$checkquerys[0]['client_name']);

            $dst = "upload/".$client_id."-".$client_name."/".$brand_id."-".$brand_name."/".$brief_id."-".$brief_name;  
            $client_folder=$client_id."-".$client_name;
            $brand_folder=$brand_id."-".$brand_name;
            $brief_folder=$brief_id."-".$brief_name;
            if (!is_dir("./upload/".$client_folder)) {
                mkdir("./upload/".$client_folder, 0777, true);
            }   
            if (!is_dir("./upload/".$client_folder."/".$brand_folder)) {
                mkdir("./upload/".$client_folder."/".$brand_folder, 0777, true);
            }   
            if (!is_dir("./upload/".$client_folder."/".$brand_folder."/".$brief_folder)) {
                mkdir("./upload/".$client_folder."/".$brand_folder."/".$brief_folder, 0777, true);
            }   
            $brief_path="./upload/".$client_folder."/".$brand_folder."/".$brief_folder;
            $timestamp=time();
            //if (!is_dir("./brief_upload/".$brief_id)) {
            //mkdir("./brief_upload/".$brief_id, 0777, true);
            //}



              if ((exif_imagetype($_FILES["image"]["tmp_name"]) == IMAGETYPE_JPEG) && (imagecreatefromjpeg( $_FILES["image"]["tmp_name"] ) !== false ))
            {
             //echo 'The picture is a valid jpg<br>';
            }
            elseif ((exif_imagetype($_FILES["image"]["tmp_name"]) == IMAGETYPE_PNG) && (imagecreatefrompng( $_FILES["image"]["tmp_name"] ) !== false ))
            {
             //echo 'The picture is a valid png<br>';
            }
            elseif (is_file($_FILES["image"]["tmp_name"]) && (0 === strpos(mime_content_type($_FILES["image"]["tmp_name"]), 'video/')))
            {
                //echo 'The picture is a valid mp4<br>';
            }
            else
            {
                 //echo 'notvalid';
                 /* echo "File ".$image_num." of ".$image_tot_num." not uploading because it has invalid property.@@@@@".$image_num."@@@@@".$image_tot_num."@@@@@notvalid";*/
                  echo "notvalid";
                 exit;
            }














            $file=$_FILES["image"]["name"];
            $ext = pathinfo($file, PATHINFO_EXTENSION);
            $file_name=preg_replace('/[^a-zA-Z0-9_.]/', '_',pathinfo($file, PATHINFO_FILENAME));
            $tm=time();
            //$file = $tm.".".$ext;
           // $file=$tm.".jpg";
            $file = $file_name."_".$tm.".".$ext;
           

            if($_FILES["image"]["type"]=='image/gif' || $_FILES["image"]["type"]=='image/png' || $_FILES["image"]["type"]=='image/bmp' || $_FILES["image"]["type"]=='image/jpg' || $_FILES["image"]["type"]=='image/jpeg')
            {
            $file_type="0";
            }
            elseif($_FILES["image"]["type"]=='video/webm' || $_FILES["image"]["type"]=='video/mp4' || $_FILES["image"]["type"]=='video/3gpp' || $_FILES["image"]["type"]=='video/x-sgi-movie' || $_FILES["image"]["type"]=='video/x-msvideo' || $_FILES["image"]["type"]=='video/mpeg' || $_FILES["image"]["type"]=='video/x-ms-wmv' )
            {

            $file_type="1";

            }






            if(isset($_POST['image_id'])) 
            {
                //  $data['viewfilesinfo'] = $this->Proofing_model->insertimages1($file,$brief_id,$image_id);
                $lastid= $this->Proofing_model->insertimages1($file,$file_type,$brief_id,$image_id);
            }
            else {
                //$data['viewfilesinfo'] = $this->Proofing_model->insertimages($file,$brief_id);
                $lastid=$this->Proofing_model->insertimages($file,$file_type,$brief_id);
            }
            if (!is_dir($brief_path."/".$lastid)) {
                mkdir($brief_path."/".$lastid, 0777, true);
            }
            // echo $lastid;exit();
            $wc_brief_sql_ver = "SELECT version_num,img_status FROM `wc_image_upload` where brief_id='$brief_id' and parent_img='$lastid' ORDER BY `version_num` DESC";
            $wc_brief_query_ver = $this->db->query($wc_brief_sql_ver);
            $wc_brief_result_ver=$wc_brief_query_ver->result_array();
            $num=sizeof($wc_brief_result_ver);
            if($num>0){
                $max_ver=$wc_brief_result_ver[0]['version_num'];
            }else{
                $max_ver=1;
            }

            //$max_ver=$max_ver+1;

            if (!is_dir($brief_path."/".$lastid."/Revision".$max_ver."")) {
                mkdir($brief_path."/".$lastid."/Revision".$max_ver."", 0777, true);
            }     
            $dir =$brief_path."/".$lastid."/Revision".$max_ver."/";

            if($_FILES["image"]["type"]=='image/jpeg'|| $_FILES["image"]["type"]=='image/jpg' || $_FILES["image"]["type"]=='video/webm' || $_FILES["image"]["type"]=='video/mp4' || $_FILES["image"]["type"]=='video/3gpp' || $_FILES["image"]["type"]=='video/x-sgi-movie' || $_FILES["image"]["type"]=='video/x-msvideo' || $_FILES["image"]["type"]=='video/mpeg' || $_FILES["image"]["type"]=='video/x-ms-wmv' ||$_FILES["image"]["type"]=='image/gif' || $_FILES["image"]["type"]=='image/png' || $_FILES["image"]["type"]=='image/bmp'){
                move_uploaded_file($_FILES["image"]["tmp_name"], $dir. $file);
            }
            if($_FILES["image"]["type"]=='image/gif' || $_FILES["image"]["type"]=='image/png' || $_FILES["image"]["type"]=='image/bmp' || $_FILES["image"]["type"]=='image/jpg' || $_FILES["image"]["type"]=='image/jpeg'){
                //$file_image=$timestamp.pathinfo($_FILES["image"]["name"], PATHINFO_FILENAME);
                $file_image=pathinfo($_FILES["image"]["name"], PATHINFO_FILENAME);

                $new_thumb = $brief_path."/".$lastid."/Revision".$max_ver."/thumb_".$file;
                $org_img = $brief_path."/".$lastid."/Revision".$max_ver."/org_".$file;
                $file_pat = $brief_path."/".$lastid."/Revision".$max_ver."/".$file;
                $this->convertToThumb($new_thumb,$brief_id,$file_pat);

                $this->reduceSize($org_img,$brief_id,$file_pat);
                //echo ltrim($file);

                // $new_thumb = $brief_path."/".$lastid."/Revision".$max_ver."/thumb_".$tm.".jpg";
                // copy($file_pat,$new_thumb);

                
            }else{

                $file_image=pathinfo($_FILES["image"]["name"], PATHINFO_FILENAME);
                $new_thumb = $brief_path."/".$lastid."/Revision".$max_ver."/"."thumb_".$file;
                $org_img = $brief_path."/".$lastid."/Revision".$max_ver."/"."org_".$file;
                $file_pat = "./assets/img/video_thumbnail.jpg";
                $this->convertToThumb($new_thumb,$brief_id,$file_pat);

                $this->convertToThumb($org_img,$brief_id,$file_pat);


                $file_pat = "./assets/img/video_thumbnail.jpg";
                // $new_thumb = $brief_path."/".$lastid."/Revision".$max_ver."/thumb_".$tm.".jpg";
                // copy($file_pat,$new_thumb);



            }
            
            //email function to bm
            

            
            
            
            
            
            
            
            
            //end of email function to bm
            

            $update_sql = "UPDATE `wc_brief` SET `status`='6' WHERE brief_id='$brief_id' ";
            $this->db->query($update_sql);

            echo "success";



              /*********************************Send mail*************************************/
   
    $query=$this->db->query("select * from  wc_settings  WHERE setting_key='MAIL_SMTP_USER' ");
    $userDetails= $query->result_array();
    $MAIL_SMTP_USER=$userDetails[0]['setting_value'];

    $query=$this->db->query("select * from  wc_settings  WHERE setting_key='MAIL_SMTP_PASSWORD' ");
    $userDetails= $query->result_array();
    $MAIL_SMTP_PASSWORD=$userDetails[0]['setting_value'];

    $query=$this->db->query("select * from  wc_settings  WHERE setting_key='MAIL_BODY_UPLOAD_REVISED_FILE' ");
    $userDetails= $query->result_array();
    $MAIL_BODY=$userDetails[0]['setting_value'];

    $query=$this->db->query("select * from  wc_settings  WHERE setting_key='MAIL_FROM' ");
    $userDetails= $query->result_array();
    $MAIL_FROM=$userDetails[0]['setting_value'];


    $query=$this->db->query("select * from  wc_settings  WHERE setting_key='MAIL_FROM_TEXT' ");
    $userDetails= $query->result_array();
    $MAIL_FROM_TEXT=$userDetails[0]['setting_value'];

    $checkquerys = $this->db->query("select * from wc_brief where  brief_id='$brief_id' ")->result_array();
    $brief_id=$checkquerys[0]['brief_id'];
    //$rejected_reasons=$checkquerys[0]['rejected_reasons'];
    $added_by_user_id=$checkquerys[0]['added_by_user_id'];
    $brief_title=$checkquerys[0]['brief_title'];
    $brand_id=$checkquerys[0]['brand_id'];





    $checkquerys = $this->db->query("select * from wc_users where  user_id='$added_by_user_id' ")->result_array();
    $brand_manager_email=$checkquerys[0]['user_email'];
    $brand_manager_name=$checkquerys[0]['user_name'];
    $client_id=$checkquerys[0]['client_id'];



        $to_email_all_bm[]="";
         if($brand_id!='')
        {

            
            $checkquerys_mail = $this->db->query("select * from wc_users where  client_id='$client_id' and brand_id like '%,".$brand_id.",%' and user_type_id='4' ")->result_array();


            if(!empty($checkquerys_mail))
            {
            foreach($checkquerys_mail as $key => $value)
            {

            $to_email_all_bm[]=$value['user_email'];

            }

            }




        }

        $checkquerys_clientmail= $this->db->query("SELECT * FROM `wc_clients` WHERE `client_id`='$client_id'")->result_array();
        $client_email=$checkquerys_clientmail[0]['client_email'];
        array_push($to_email_all_bm,$client_email);
        
    //$subject=$MAIL_FROM_TEXT.' - Notification mail for Card - '.$brief_id;
     $subject=$MAIL_FROM_TEXT.' - New Revised Files Uploaded (#'.$brief_id.')';
   // $uemail=$brand_manager_email;
    //$unique_user_id=time()."-".$user_id;
    //$activation_link=base_url()."activate-registration/".base64_encode($unique_user_id);

        
        //Load email library
        $this->load->library('email');

        $config = array();
        //$config['protocol'] = 'smtp';
        $config['protocol']     = 'mail';
        $config['smtp_host'] = 'localhost';
        $config['smtp_user'] = $MAIL_SMTP_USER;
        $config['smtp_pass'] = $MAIL_SMTP_PASSWORD;
        $config['smtp_port'] = 25;




        $this->email->initialize($config);
        $this->email->set_newline("\r\n");
        /* if($client_id!='') {
                    $checkquerys = $this->db->query("select * from wc_clients where client_id='$client_id' ")->result_array();

                    $client_image=$checkquerys[0]['client_image'];
                    $wekan_client_id=$checkquerys[0]['wekan_client_id'];

                    $client_logo=base_url()."/upload_client_logo/".$client_image;
                    }
                    else
                    {
                    $client_logo=base_url()."/website-assets/images/logo.png";
                    }
                    $client_logo="http://style.piquic.com/WeCreate//website-assets/images/logo.png";*/

         $website_url=base_url();
         $dashboard_url=base_url()."dashboard";              
        
        // $from_email = "demo.intexom@gmail.com";
        $from_email = $MAIL_FROM;
        //$to_email = $brand_manager_email;
        $to_email = $to_email_all_bm;

        $date_time=date("d/m/y");
        /*$copy_right=$MAIL_FROM_TEXT." - ".date("Y");*/
        $copy_right=$MAIL_FROM_TEXT;
        $project_name="#".$brief_id." - ".$brief_title;
        //echo $message=$MAIL_BODY;
       /* $find_arr = array("##CLIENT_LOGO##","##BM_NAME##","##BRIEF_ID##","##IMAGE_ID##","##COPY_RIGHT##");
        $replace_arr = array($client_logo,$brand_manager_name,$brief_id,$lastid,$copy_right);*/
        $find_arr = array("##WEBSITE_URL##","##PROJECT_NAME##","##DASHBOARD_LINK##","##COPY_RIGHT##");
        $replace_arr = array($website_url,$project_name,$dashboard_url,$copy_right);
        $message=str_replace($find_arr,$replace_arr,$MAIL_BODY);

        //echo $message;exit;



        $this->email->from($from_email, $MAIL_FROM_TEXT);
        $this->email->to($to_email);
         if($_SERVER['HTTP_HOST']!='collab.piquic.com')
                    {
                        $this->email->cc(array('poulami@mokshaproductions.in','pranav@mokshaproductions.in','raja.priya@mokshaproductions.in','rajendra.prasad@mokshaproductions.in'));
                    }
        //$this->email->bcc('raja.priya@mokshaproductions.in');
        //$this->email->bcc('rajendra.prasad@mokshaproductions.in');
        $this->email->subject($subject);
        $this->email->message($message);
        //Send mail
        /*if($this->email->send())
        {

            //echo "mail sent";exit;
            $this->session->set_flashdata("email_sent","Congragulation Email Send Successfully.");
        }
        
        else
        {
            //echo $this->email->print_debugger();
            //echo "mail not sent";exit;
            $this->session->set_flashdata("email_sent","You have encountered an error");
        }*/
        

    //exit;

/************************************************************************/


            

        }else{
            $data['user_id'] = '';
            $data['user_name'] = '';
            $user_id='';
            echo "sessionout";
        }
    }

function convertToThumb($targetFile, $brief_id, $originalFile) {

  $newHeight = 400;

  $info = getimagesize($originalFile);
  $mime = $info['mime'];

  switch ($mime) {
    case 'image/jpeg':
    $image_create_func = 'imagecreatefromjpeg';
    $image_save_func = 'imagejpeg';
    $new_image_ext = 'jpg';
    break;

    case 'image/png':
    $image_create_func = 'imagecreatefrompng';
    $image_save_func = 'imagepng';
    $new_image_ext = 'png';
    break;

    // case 'image/gif':
    // $image_create_func = 'imagecreatefromgif';
    // $image_save_func = 'imagegif';
    // $new_image_ext = 'gif';
    // break;

    default: 
    throw new Exception('Unknown image type.');
  }

  $img = $image_create_func($originalFile);
  list($width, $height) = getimagesize($originalFile);

  $newWidth = ($width / $height) * $newHeight;
  $tmp = imagecreatetruecolor($newWidth, $newHeight);
  imagecopyresampled($tmp, $img, 0, 0, 0, 0, $newWidth, $newHeight, $width, $height);

  if (file_exists($targetFile)) {
    unlink($targetFile);
  }

  $image_save_func($tmp, "$targetFile");
}

function reduceSize($targetFile, $brief_id, $originalFile) {

  $info = getimagesize($originalFile);
  $mime = $info['mime'];

  switch ($mime) {
    case 'image/jpeg':
    $image_create_func = 'imagecreatefromjpeg';
    $image_save_func = 'imagejpeg';
    $new_image_ext = 'jpg';
    break;

    case 'image/png':
    $image_create_func = 'imagecreatefrompng';
    $image_save_func = 'imagepng';
    $new_image_ext = 'png';
    break;

    // case 'image/gif':
    // $image_create_func = 'imagecreatefromgif';
    // $image_save_func = 'imagegif';
    // $new_image_ext = 'gif';
    // break;

    default: 
    throw new Exception('Unknown image type.');
  }

  $img = $image_create_func($originalFile);
  list($width, $height) = getimagesize($originalFile);

  $newWidth = $width;
  $newHeight = $height;

  $tmp = imagecreatetruecolor($newWidth, $newHeight);
  imagecopyresampled($tmp, $img, 0, 0, 0, 0, $newWidth, $newHeight, $width, $height);

  if (file_exists($targetFile)) {
    unlink($targetFile);
  }

  $image_save_func($tmp, "$targetFile");
}





public function getAllUser() {



    $status_query= $this->db->query("SELECT * FROM `wc_users` where 1=1 ");
                $status_result= $status_query->result_array();
                //print_r($status_result);
                $row_cnt=$status_query->num_rows();
                if($row_cnt>=1){
                    foreach ($status_result as $i => $row) {
                        $user_id = $row['user_id'];

                        if($row["user_type_id"]=="4")
                        {
                        $role="admin";
                        }
                        else
                        {
                        $role="user";
                        }


                        $userresult[$i]['name']=$row['user_name'];
                        $userresult[$i]['mail']=$row['user_email'];
                        $userresult[$i]['registrationDate']='';
                        $userresult[$i]['passwd']=sha1($row['user_password']);
                        $userresult[$i]['role']=$role;
                        $userresult[$i]['active']="1";
                        $userresult[$i]['lastLogin']="";
                        $userresult[$i]['color']="597081";
   
                        
                    }
                }





$returnArr["user-increment"] = $row_cnt;

$returnArr["user"] = $userresult;


/*  echo "<pre>";
print_r($returnArr);
exit;*/


header('Content-type: application/json; charset=UTF-8');
echo json_encode($returnArr, JSON_PRETTY_PRINT | JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES);


}








public function getAllUserById($user_id) {



    $status_query= $this->db->query("SELECT * FROM `wc_users` where user_id='$user_id' ");
                $status_result= $status_query->result_array();
                //print_r($status_result);
                $row_cnt=$status_query->num_rows();
                if($row_cnt>=1){
                    foreach ($status_result as $i => $row) {
                        $user_id = $row['user_id'];

                        if($row["user_type_id"]=="4")
                        {
                        $role="admin";
                        }
                        else
                        {
                        $role="user";
                        }


                        $userresult[$i]['name']=$row['user_name'];
                        $userresult[$i]['mail']=$row['user_email'];
                        $userresult[$i]['registrationDate']='';
                        $userresult[$i]['passwd']=sha1($row['user_password']);
                        $userresult[$i]['role']=$role;
                        $userresult[$i]['active']="1";
                        $userresult[$i]['lastLogin']="";
                        $userresult[$i]['color']="597081";
   
                        
                    }
                }




return $userresult;

}




public function getAllVideo($brief_id) {


    $checkquerys = $this->db->query("SELECT * FROM `wc_brief` WHERE `brief_id` ='$brief_id'")->result_array();
    $brand_id=$checkquerys[0]['brand_id'];
    $brief_title=$checkquerys[0]['brief_title'];
    $brief_name=preg_replace('/[^a-zA-Z0-9_.]/', '_',$brief_title);
    $checkquerys = $this->db->query("select wc_brands.*,wc_clients.client_name from wc_brands inner join wc_clients on wc_brands.client_id=wc_clients.client_id where  wc_brands.brand_id='$brand_id'")->result_array();
    $brand_id=$checkquerys[0]['brand_id'];
    $brand_name=preg_replace('/[^a-zA-Z0-9_.]/', '_',$checkquerys[0]['brand_name']);
    $client_id=$checkquerys[0]['client_id'];
    $client_name=preg_replace('/[^a-zA-Z0-9_.]/', '_',$checkquerys[0]['client_name']);

    $dst = "upload/".$client_id."-".$client_name."/".$brand_id."-".$brand_name."/".$brief_id."-".$brief_name;  
    $client_folder=$client_id."-".$client_name;
    $brand_folder=$brand_id."-".$brand_name;
    $brief_folder=$brief_id."-".$brief_name;

    $brief_path=base_url()."upload/".$client_folder."/".$brand_folder."/".$brief_folder;



    /*$status_query= $this->db->query("SELECT * FROM `wc_image_upload` where brief_id='$brief_id' and file_type='1'   ");*/
    $status_query= $this->db->query("SELECT * FROM `wc_image_upload` where brief_id='$brief_id'    ");
                $status_result= $status_query->result_array();
                //print_r($status_result);
                $row_cnt=$status_query->num_rows();
                if($row_cnt>=1){
                    foreach ($status_result as $i => $row) {
                    $image_id = $row['image_id'];
                    $version_num = $row['version_num'];
                    $parent_img = $row['parent_img'];




                    $userresult[$i]['name']=$row['img_title'];
                    $userresult[$i]['creator']="admin";
                    $userresult[$i]['creatorId']=$row['added_by'];
                    $userresult[$i]['created']=$row['added_on'];
                    $userresult[$i]['description']='';
                    $userresult[$i]['src']=$brief_path."/".$parent_img."/Revision".$version_num."/".$row['image_path'];
                    $userresult[$i]['attributes']=array();
                    $userresult[$i]['type']="video";
                    $userresult[$i]['thumb']=$brief_path."/".$parent_img."/Revision".$version_num."/thumb_".$row['image_path'];

                   
   
                        
                    }
                }




$returnArr["resources-increment"] = $row_cnt;

$returnArr["resources"] = $userresult;


/*  echo "<pre>";
print_r($returnArr);
exit;*/


header('Content-type: application/json; charset=UTF-8');
echo json_encode($returnArr, JSON_PRETTY_PRINT | JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES);

}




public function getAllVideoCount($brief_id) {


    $checkquerys = $this->db->query("SELECT * FROM `wc_brief` WHERE `brief_id` ='$brief_id'")->result_array();
    $brand_id=$checkquerys[0]['brand_id'];
    $brief_title=$checkquerys[0]['brief_title'];
    $brief_name=preg_replace('/[^a-zA-Z0-9_.]/', '_',$brief_title);
    $checkquerys = $this->db->query("select wc_brands.*,wc_clients.client_name from wc_brands inner join wc_clients on wc_brands.client_id=wc_clients.client_id where  wc_brands.brand_id='$brand_id'")->result_array();
    $brand_id=$checkquerys[0]['brand_id'];
    $brand_name=preg_replace('/[^a-zA-Z0-9_.]/', '_',$checkquerys[0]['brand_name']);
    $client_id=$checkquerys[0]['client_id'];
    $client_name=preg_replace('/[^a-zA-Z0-9_.]/', '_',$checkquerys[0]['client_name']);

    $dst = "upload/".$client_id."-".$client_name."/".$brand_id."-".$brand_name."/".$brief_id."-".$brief_name;  
    $client_folder=$client_id."-".$client_name;
    $brand_folder=$brand_id."-".$brand_name;
    $brief_folder=$brief_id."-".$brief_name;

    $brief_path=base_url()."upload/".$client_folder."/".$brand_folder."/".$brief_folder;



    /*$status_query= $this->db->query("SELECT * FROM `wc_image_upload` where brief_id='$brief_id' and file_type='1'   ");*/
    $status_query= $this->db->query("SELECT * FROM `wc_image_upload` where brief_id='$brief_id'    ");
                $status_result= $status_query->result_array();
                //print_r($status_result);
                $row_cnt=$status_query->num_rows();
                if($row_cnt>=1){
                    $i=1;
                    foreach ($status_result as $k => $row) {
                        $image_id = $row['image_id'];


                        $userresult[$image_id]="./".$image_id;

                        if($i==$row_cnt)
                        {
                        $image_last_id = $row['image_id'];
                        }
                        $i++;
                        
                    }
                }




$returnArr["hypervideo-increment"] = $image_last_id;

$returnArr["hypervideos"] = $userresult;


/*  echo "<pre>";
print_r($returnArr);
exit;*/


header('Content-type: application/json; charset=UTF-8');
echo json_encode($returnArr, JSON_PRETTY_PRINT | JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES);

}







public function getAllVideoDetail($brief_id,$image_id) {



        $checkquerys = $this->db->query("SELECT * FROM `wc_brief` WHERE `brief_id` ='$brief_id'")->result_array();
    $brand_id=$checkquerys[0]['brand_id'];
    $brief_title=$checkquerys[0]['brief_title'];
    $brief_name=preg_replace('/[^a-zA-Z0-9_.]/', '_',$brief_title);
    $checkquerys = $this->db->query("select wc_brands.*,wc_clients.client_name from wc_brands inner join wc_clients on wc_brands.client_id=wc_clients.client_id where  wc_brands.brand_id='$brand_id'")->result_array();
    $brand_id=$checkquerys[0]['brand_id'];
    $brand_name=preg_replace('/[^a-zA-Z0-9_.]/', '_',$checkquerys[0]['brand_name']);
    $client_id=$checkquerys[0]['client_id'];
    $client_name=preg_replace('/[^a-zA-Z0-9_.]/', '_',$checkquerys[0]['client_name']);

    $dst = "upload/".$client_id."-".$client_name."/".$brand_id."-".$brand_name."/".$brief_id."-".$brief_name;  
    $client_folder=$client_id."-".$client_name;
    $brand_folder=$brand_id."-".$brand_name;
    $brief_folder=$brief_id."-".$brief_name;

    $brief_path=base_url()."upload/".$client_folder."/".$brand_folder."/".$brief_folder;


    $checkquerys = $this->db->query("SELECT * FROM `wc_image_upload` where image_id='$image_id' and `brief_id` ='$brief_id'")->result_array();

    $row=$checkquerys[0];


    $image_id = $row['image_id'];
    $version_num = $row['version_num'];
    $parent_img = $row['parent_img'];




    $returnArr["meta"]["name"] = $row['img_title'];
    $returnArr["meta"]["description"] = $row['img_title'];
    $returnArr["meta"]["thumb"] = $brief_path."/".$image_id."/Revision".$version_num."/thumb_".$row['image_path'];
    $returnArr["meta"]["creator"] = "admin";
    $returnArr["meta"]["creatorId"] = $row['added_by'];
    $returnArr["meta"]["created"] = $row['added_on'];
    $returnArr["meta"]["lastchanged"] = $row['image_id'];




    $returnArr["config"]["slidingMode"] = "adjust";
    $returnArr["config"]["slidingTrigger"] = "key";
    $returnArr["config"]["autohideControls"] = false;
    $returnArr["config"]["captionsVisible"] = false;
    $returnArr["config"]["clipTimeVisible"] = false;
    $returnArr["config"]["hidden"] = $row['image_id'];
    $returnArr["config"]["layoutArea"]["areaTop"] = array();
    $returnArr["config"]["layoutArea"]["areaBottom"] = array();
    $returnArr["config"]["layoutArea"]["areaLeft"] = array();
    $returnArr["config"]["layoutArea"]["areaRight"] = array();



    $returnArr["clips"]["resourceId"] = "adjust";
    $returnArr["clips"]["src"] = "key";
    $returnArr["clips"]["duration"] = 0;
    $returnArr["clips"]["start"] = 0;
    $returnArr["clips"]["end"] = 0;
    $returnArr["clips"]["in"] = 0;
    $returnArr["clips"]["out"] = 0;

    $returnArr["clips"]["resourceId"] = $row['image_id'];
    $returnArr["clips"]["src"] = $brief_path."/".$parent_img."/Revision".$version_num."/".$row['image_path'];
    $returnArr["clips"]["duration"] = 0;
    $returnArr["clips"]["start"] = 0;
    $returnArr["clips"]["end"] = 0;
    $returnArr["clips"]["in"] = 0;
    $returnArr["clips"]["out"] = 0;


    $returnArr["globalEvents"]["onReady"] = '';
    $returnArr["globalEvents"]["onPlay"] = '';
    $returnArr["globalEvents"]["onPause"] = '';
    $returnArr["globalEvents"]["onEnded"] = '';

    $returnArr["customCSS"]="";

    //$returnArr["contents"]=array();















    $contentsArr1['@context'] = array( 
    "http://www.w3.org/ns/anno.jsonld", 
     array( 
        "frametrail" => "http://frametrail.org/ns/"
    ) 
    ); 

    $contentsArr1['creator'] = array( 
    "nickname" => "admin",  
    "type" => "Person",
    "id" => "1", 

    ); 
    $contentsArr1["created"]="Wed Apr 15 2020 12:35:39 GMT+0530 (India Standard Time)";
    $contentsArr1["type"]="Annotation";
    $contentsArr1["frametrail:type"]="Overlay";
    $contentsArr1["frametrail:tags"]=array();




    $contentsArr1['target'] = array( 
    "type" => "Video",  
    "source" => "1_1586933555_poulami.mp4",
    "selector" => array( 
        "conformsTo" => "http://www.w3.org/TR/media-frags/",  
        "type" => "FragmentSelector", 
        "value" => "t=2.512510402699279,3.6908441780731405&xywh=percent:31.769758371856916,52.41426465798388,30,30"
    ) 

    ); 


    $contentsArr1['body'] = array( 
    "type" => "TextualBody", 
    "frametrail:type" => "text", 
    "format" => "text/html", 
    "value" => "", 
    "frametrail:name" => "Custom Text/HTML",  


    ); 
    $contentsArr1['frametrail:events'] = array();

    $contentsArr1['frametrail:attributes'] = array( 
    "text" => '&lt;p&gt;&lt;b&gt;&lt;span style=\"color: rgb(255, 0, 150); font-size: 50px;\"&gt;aaaa&lt;/span&gt;&lt;/b&gt;&lt;/p&gt;', 



    ); 

    /////////////////////////////////////////////////////////////////////
    $contentsArr2['@context'] = array( 
    "http://www.w3.org/ns/anno.jsonld", 
     array( 
        "frametrail" => "http://frametrail.org/ns/"
    ) 
    ); 

    $contentsArr2['creator'] = array( 
    "nickname" => "admin",  
    "type" => "Person",
    "id" => "1", 

    ); 
    $contentsArr2["created"]="Wed Apr 15 2020 12:35:39 GMT+0530 (India Standard Time)";
    $contentsArr2["type"]="Annotation";
    $contentsArr2["frametrail:type"]="Overlay";
    $contentsArr2["frametrail:tags"]=array();




    $contentsArr2['target'] = array( 
    "type" => "Video",  
    "source" => "1_1586933555_poulami.mp4",
    "selector" => array( 
        "conformsTo" => "http://www.w3.org/TR/media-frags/",  
        "type" => "FragmentSelector", 
        "value" => "t=4.595946836014365,5.797758282272914&xywh=percent:49.5961375680302,60.32147285848638,30.2,30"
    ) 

    ); 


    $contentsArr2['body'] = array( 
    "type" => "TextualBody", 
    "frametrail:type" => "text", 
    "format" => "text/html", 
    "value" => "", 
    "frametrail:name" => "Custom Text/HTML",  


    ); 
    $contentsArr2['frametrail:events'] = array();

    $contentsArr2['frametrail:attributes'] = array( 
    "text" => '&lt;p&gt;&lt;b&gt;&lt;span style=\"color: rgb(39, 188, 127);\"&gt;bbbbbbbb&lt;/span&gt;&lt;/b&gt;&lt;b&gt;&lt;/b&gt;&lt;/p&gt;', 



    ); 


/*    $returnArr["contents"] = array( 


    $contentsArr1,
    $contentsArr2 
     
    ); 
*/

    
//echo "SELECT * FROM `wc_image_annotation` where image_id='$image_id'  and file_type='1'   ";

        $status_query= $this->db->query("SELECT * FROM `wc_image_annotation` where image_id='$image_id'  and file_type='1'   ");
        $status_result= $status_query->result_array();
        //print_r($status_result);
        $row_cnt=$status_query->num_rows();
        if($row_cnt>=1){
        foreach ($status_result as $i => $row) {
            $annotation_id = $row['id'];
            $image_id = $row['image_id'];
           


            $revision_id = $row['revision_id'];
            $target_selector = $row['target_selector'];
            $frametrail_attributes = $row['frametrail_attributes'];


            $returnArr["contents"][$i]['@context'] = array( 
            "http://www.w3.org/ns/anno.jsonld", 
            array( 
            "frametrail" => "http://frametrail.org/ns/"
            ) 
            ); 

            $returnArr["contents"][$i]['information'] = array( 
            "brief_id" => $brief_id,  
            "image_id" => $image_id,
            "annotation_id" => $annotation_id, 

            ); 

            $returnArr["contents"][$i]['creator'] = array( 
            "nickname" => "admin",  
            "type" => "Person",
            "id" => "1", 

            ); 
            $returnArr["contents"][$i]["created"]="Wed Apr 15 2020 12:35:39 GMT+0530 (India Standard Time)";
            $returnArr["contents"][$i]["type"]="Annotation";
            $returnArr["contents"][$i]["frametrail:type"]="Overlay";
            $returnArr["contents"][$i]["frametrail:tags"]=array();




            $returnArr["contents"][$i]['target'] = array( 
            "type" => "Video",  
            "source" => "1_1586933555_poulami.mp4",
            "selector" => array( 
            "conformsTo" => "http://www.w3.org/TR/media-frags/",  
            "type" => "FragmentSelector", 
            "value" => $target_selector
            ) 

            ); 


            $returnArr["contents"][$i]['body'] = array( 
            "type" => "TextualBody", 
            "frametrail:type" => "text", 
            "format" => "text/html", 
            "value" => "", 
            "frametrail:name" => "Custom Text/HTML",  


            ); 
            $returnArr["contents"][$i]['frametrail:events'] = array();

            $returnArr["contents"][$i]['frametrail:attributes'] = array( 
            "text" => $frametrail_attributes, 



            ); 


        }
        }






    /*{
            
            
            "body": {
                "type": "TextualBody",
                "frametrail:type": "text",
                "format": "text/html",
                "value": "",
                "frametrail:name": "Custom Text/HTML"
            },
            "frametrail:events": {},
            "frametrail:attributes": {
                "text": "&lt;p&gt;&lt;b&gt;&lt;span style=\"color: rgb(255, 0, 150); font-size: 50px;\"&gt;aaaa&lt;/span&gt;&lt;/b&gt;&lt;/p&gt;"
            }
        },
    */


   /*   echo "<pre>";
    print_r($returnArr);
    exit;*/


    header('Content-type: application/json; charset=UTF-8');
    echo json_encode($returnArr, JSON_PRETTY_PRINT | JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES);



    

}











public function getAllVideoAnnotation($brief_id,$image_id) {



        $checkquerys = $this->db->query("SELECT * FROM `wc_brief` WHERE `brief_id` ='$brief_id'")->result_array();
    $brand_id=$checkquerys[0]['brand_id'];
    $brief_title=$checkquerys[0]['brief_title'];
    $brief_name=preg_replace('/[^a-zA-Z0-9_.]/', '_',$brief_title);
    $checkquerys = $this->db->query("select wc_brands.*,wc_clients.client_name from wc_brands inner join wc_clients on wc_brands.client_id=wc_clients.client_id where  wc_brands.brand_id='$brand_id'")->result_array();
    $brand_id=$checkquerys[0]['brand_id'];
    $brand_name=preg_replace('/[^a-zA-Z0-9_.]/', '_',$checkquerys[0]['brand_name']);
    $client_id=$checkquerys[0]['client_id'];
    $client_name=preg_replace('/[^a-zA-Z0-9_.]/', '_',$checkquerys[0]['client_name']);

    $dst = "upload/".$client_id."-".$client_name."/".$brand_id."-".$brand_name."/".$brief_id."-".$brief_name;  
    $client_folder=$client_id."-".$client_name;
    $brand_folder=$brand_id."-".$brand_name;
    $brief_folder=$brief_id."-".$brief_name;

    $brief_path=base_url()."upload/".$client_folder."/".$brand_folder."/".$brief_folder;


    $checkquerys = $this->db->query("SELECT * FROM `wc_image_upload` where image_id='$image_id' and `brief_id` ='$brief_id'")->result_array();

    $row=$checkquerys[0];


    $image_id = $row['image_id'];

$returnArr["mainAnnotation"]="1";

$returnArr["annotationfiles"]["1"]["name"]="main";
$returnArr["annotationfiles"]["1"]["description"]="";
$returnArr["annotationfiles"]["1"]["created"]=$row['added_on'];
$returnArr["annotationfiles"]["1"]["lastchanged"]=$row['added_on'];
$returnArr["annotationfiles"]["1"]["hidden"]="";
$returnArr["annotationfiles"]["1"]["owner"]="admin";
$returnArr["annotationfiles"]["1"]["ownerId"]=1;



/*  echo "<pre>";
print_r($returnArr);
exit;*/


header('Content-type: application/json; charset=UTF-8');
echo json_encode($returnArr, JSON_PRETTY_PRINT | JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES);



    

}





public function getAllVideoAnnotationId($brief_id,$image_id) {



        $checkquerys = $this->db->query("SELECT * FROM `wc_brief` WHERE `brief_id` ='$brief_id'")->result_array();
    $brand_id=$checkquerys[0]['brand_id'];
    $brief_title=$checkquerys[0]['brief_title'];
    $brief_name=preg_replace('/[^a-zA-Z0-9_.]/', '_',$brief_title);
    $checkquerys = $this->db->query("select wc_brands.*,wc_clients.client_name from wc_brands inner join wc_clients on wc_brands.client_id=wc_clients.client_id where  wc_brands.brand_id='$brand_id'")->result_array();
    $brand_id=$checkquerys[0]['brand_id'];
    $brand_name=preg_replace('/[^a-zA-Z0-9_.]/', '_',$checkquerys[0]['brand_name']);
    $client_id=$checkquerys[0]['client_id'];
    $client_name=preg_replace('/[^a-zA-Z0-9_.]/', '_',$checkquerys[0]['client_name']);

    $dst = "upload/".$client_id."-".$client_name."/".$brand_id."-".$brand_name."/".$brief_id."-".$brief_name;  
    $client_folder=$client_id."-".$client_name;
    $brand_folder=$brand_id."-".$brand_name;
    $brief_folder=$brief_id."-".$brief_name;

    $brief_path=base_url()."upload/".$client_folder."/".$brand_folder."/".$brief_folder;


    $checkquerys = $this->db->query("SELECT * FROM `wc_image_upload` where image_id='$image_id' and `brief_id` ='$brief_id'")->result_array();

    $row=$checkquerys[0];

$image_id = $row['image_id'];

$returnArr=[];

/*  echo "<pre>";
print_r($returnArr);
exit;*/


header('Content-type: application/json; charset=UTF-8');
echo json_encode($returnArr, JSON_PRETTY_PRINT | JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES);


    

}




public function insertAllVideoAnnotationId() {


/*$session_data = $this->session->userdata('front_logged_in');
$user_id = $session_data['user_id'];
$user_type_id=$session_data['user_type_id'];*/




$user_id=$this->input->post('user_id');
 $image_id=$this->input->post('image_id');
$arr_count=$this->input->post('arr_count');
//$target_selector_arr=$this->input->post('target_selector');
/*$frametrail_attributes_arr=$this->input->post('frametrail_attributes');*/


//$target_selector_arr=$_POST['target_selector'];
//$frametrail_attributes_arr=$_POST['frametrail_attributes'];


$target_selector_arr=$this->input->post('target_selector');
$frametrail_attributes_arr=$this->input->post('frametrail_attributes');

//echo "SELECT * FROM `wc_image_upload` WHERE `image_id` ='$image_id'";exit;

$checkquerys = $this->db->query("SELECT * FROM `wc_image_upload` WHERE `image_id` ='$image_id'")->result_array();
$brand_id=$checkquerys[0]['brief_id'];
$img_title=$checkquerys[0]['img_title'];
$image_path=$checkquerys[0]['image_path'];
$file_type=$checkquerys[0]['file_type'];
$brand_id=$checkquerys[0]['brand_id'];
$brief_id=$checkquerys[0]['brief_id'];
$revision_id=$checkquerys[0]['revision_id'];
$parent_img=$checkquerys[0]['parent_img'];
$version_num=$checkquerys[0]['version_num'];
$added_on=$checkquerys[0]['added_on'];
$added_by=$checkquerys[0]['added_by'];



$sql = "DELETE FROM wc_image_annotation WHERE image_id = '".$image_id."'  and file_type='1' ";
$qry =$this->db->query($sql);

for($i=0;$i<$arr_count;$i++)
{

    $target_selector=base64_decode($target_selector_arr[$i]);
    $frametrail_attributes=base64_decode($frametrail_attributes_arr[$i]);
    $frametrail_attributes=addslashes($frametrail_attributes);




    $file_type="1";
    //$descr_text=(str_replace(array('&lt;','&gt;','&lt','&gt'),array('<','>','<','>'),$frametrail_attributes));
     $descr_text=(str_replace(array('&lt;','&gt;','&lt','&gt','&amp;nbsp;'),array('<','>','<','>',' '),$frametrail_attributes));
     $descr_text=addslashes($descr_text);


    $descr=strip_tags($descr_text);
    $position_left="";
    $position_top="";
    $create_date=date('Y/m/d');

    if($descr!='')
    {
    $sql = "INSERT INTO wc_image_annotation(`user_id`,`image_id`,`revision_id`,`file_type`,`descr`,  `position_left`, `position_top`,`target_selector`,`frametrail_attributes`, `create_date`)VALUES('$user_id','$image_id','$version_num','$file_type','$descr','$position_left','$position_top','$target_selector','$frametrail_attributes','$create_date');";

    // print_r($sql);
    $qry =$this->db->query($sql);

    }





}


return $sql;
        

    

}

public function deleteAnnotation()
{


$id=$this->input->post('annotation_id');  

$sql = "DELETE FROM wc_image_annotation WHERE id = '".$id."'  and file_type='1' ";
$qry =$this->db->query($sql);
echo "success";
}


public function get_status()
    {

        $session_data = $this->session->userdata('front_logged_in');
        $data['user_id'] = $session_data['user_id'];
        $data['user_name'] = $session_data['user_name'];
        $user_id = $session_data['user_id'];
        $user_type_id=$session_data['user_type_id'];

        
        $brief_id=$this->input->post('brief_id');
        $image_id=$this->input->post('image_id');


        $status_query1= $this->db->query("SELECT * FROM wc_image_upload WHERE brief_id='$brief_id' and image_id='$image_id'");
        $status_result1= $status_query1->result_array();
        $img_status=$status_result1[0]['img_status'];
        $file_type=$status_result1[0]['file_type'];

        echo $img_status."@@@".$file_type;

  }



public function get_file_type()
    {

        $session_data = $this->session->userdata('front_logged_in');
        $data['user_id'] = $session_data['user_id'];
        $data['user_name'] = $session_data['user_name'];
        $user_id = $session_data['user_id'];
        $user_type_id=$session_data['user_type_id'];

        
        $brief_id=$this->input->post('brief_id');
        $image_id=$this->input->post('image_id');


        $status_query1= $this->db->query("SELECT * FROM wc_image_upload WHERE brief_id='$brief_id' and image_id='$image_id'");
        $status_result1= $status_query1->result_array();
        $file_type=$status_result1[0]['file_type'];

        echo $file_type;

  }





public function update_wekan($wekan_unique_id,$status){

    $session_data = $this->session->userdata('front_logged_in');
    $user_id = $session_data['user_id'];
    $user_type_id = $session_data['user_type_id'];
    $user_name = $session_data['user_name'];
    $client_access = $session_data['client_access'];


    $query=$this->db->query("select * from wc_settings WHERE setting_key='WEKAN_LINK' ");
    $userDetails= $query->result_array();
    $WEKAN_LINK=$userDetails[0]['setting_value'];

    $query=$this->db->query("select * from wc_settings WHERE setting_key='WEKAN_USERNAME' ");
    $userDetails= $query->result_array();
    $WEKAN_USERNAME=$userDetails[0]['setting_value'];


    $query=$this->db->query("select * from wc_settings WHERE setting_key='WEKAN_PASSWORD' ");
    $userDetails= $query->result_array();
    $WEKAN_PASSWORD=$userDetails[0]['setting_value'];

/*$query=$this->db->query("select * from wc_settings WHERE setting_key='WEKAN_BOARD_ID' ");
$userDetails= $query->result_array();
$WEKAN_BOARD_ID=$userDetails[0]['setting_value'];

$query=$this->db->query("select * from wc_settings WHERE setting_key='WEKAN_LIST_ID_PENDING' ");
$userDetails= $query->result_array();
$WEKAN_LIST_ID_PENDING=$userDetails[0]['setting_value'];


$query=$this->db->query("select * from wc_settings WHERE setting_key='WEKAN_LIST_ID_REJECTED' ");
$userDetails= $query->result_array();
$WEKAN_LIST_ID_REJECTED=$userDetails[0]['setting_value'];*/

/*$checkquerys = $this->db->query("select * from wc_clients where  client_id='$client_access' ")->result_array();
$WEKAN_BOARD_ID=$checkquerys[0]['wekan_client_id'];
$WEKAN_LIST_ID_PENDING=$checkquerys[0]['wekan_pending_id'];
$WEKAN_LIST_ID_ONGOING=$checkquerys[0]['wekan_ongoing_id'];
$WEKAN_LIST_ID_REJECTED=$checkquerys[0]['wekan_rejecting_id'];*/



$checkquerys = $this->db->query("select * from wc_clients where  client_id='$client_access' ")->result_array();
$WEKAN_BOARD_ID=$checkquerys[0]['wekan_client_id'];

$checkquerys = $this->db->query("select * from wc_client_wekan_list_relation where wekan_board_list_id='1' and  client_id='$client_access' ")->result_array();
$WEKAN_LIST_ID_PENDING=$checkquerys[0]['wekan_list_id'];

$checkquerys = $this->db->query("select * from wc_client_wekan_list_relation where wekan_board_list_id='3' and  client_id='$client_access' ")->result_array();
$WEKAN_LIST_ID_ONGOING=$checkquerys[0]['wekan_list_id'];

$checkquerys = $this->db->query("select * from wc_client_wekan_list_relation where wekan_board_list_id='2' and  client_id='$client_access' ")->result_array();
$WEKAN_LIST_ID_REJECTED=$checkquerys[0]['wekan_list_id'];

$checkquerys = $this->db->query("select * from wc_client_wekan_list_relation where wekan_board_list_id='4' and  client_id='$client_access' ")->result_array();
$WEKAN_LIST_ID_TASKCOMPLETED=$checkquerys[0]['wekan_list_id'];




 if ($this->session->userdata('wekan_integration')) {

        $wekan_session_data = $this->session->userdata('wekan_integration');
        $wekan_authorId = $wekan_session_data['wekan_authorId'];
        $wekan_token = $wekan_session_data['wekan_token'];
        $wekan_tokenExpires = $wekan_session_data['wekan_tokenExpires'];
    }
    else
    {

        $curl = curl_init();
        curl_setopt_array($curl, array(
        CURLOPT_URL => $WEKAN_LINK."users/login",
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_ENCODING => "",
        CURLOPT_MAXREDIRS => 10,
        CURLOPT_TIMEOUT => 30,
        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
        CURLOPT_CUSTOMREQUEST => "POST",
        CURLOPT_POSTFIELDS => "username=".$WEKAN_USERNAME."&password=".$WEKAN_PASSWORD."",
        CURLOPT_HTTPHEADER => array(
        "cache-control: no-cache",
        "content-type: application/x-www-form-urlencoded"
        ),
        ));
        $response = curl_exec($curl);
        $err = curl_error($curl);

        curl_close($curl);

        if (!$err)
        {
        //var_dump($response);



        $res_arr=json_decode($response);

        //echo "<pre>";
        //print_r($res_arr);
        $id=$res_arr->id;
        $token=$res_arr->token;
        $tokenExpires=$res_arr->tokenExpires;


        $sess_array = array(
        'wekan_authorId' => $id,
        'wekan_token' => $token,
        'wekan_tokenExpires' => $tokenExpires
        );
        $this->session->set_userdata('wekan_integration', $sess_array);


        $wekan_session_data = $this->session->userdata('wekan_integration');
        $wekan_authorId = $wekan_session_data['wekan_authorId'];
        $wekan_token = $wekan_session_data['wekan_token'];
        $wekan_tokenExpires = $wekan_session_data['wekan_tokenExpires'];

        }

    }




if($status==1)
{
    $task="Ongoing task";
}
elseif($status==4)
{
    $task="Rejected Brief";
}
elseif($status==5)
{
    $task="Task Completed";
}

$ch = curl_init();

curl_setopt($ch, CURLOPT_URL,$WEKAN_LINK."api/boards/".$WEKAN_BOARD_ID."/cards/".$wekan_unique_id."/comments");
curl_setopt($ch, CURLOPT_HTTPHEADER, array(
    'Content-Type: application/x-www-form-urlencoded',
    'Authorization: Bearer ' . $wekan_token
));
curl_setopt($ch, CURLOPT_POST, 1);
curl_setopt($ch, CURLOPT_POSTFIELDS,
    "authorId=".$wekan_authorId."&comment=".$user_name." moved this card from Ongoing Tasks to ".$task );
curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

$server_output1 = curl_exec($ch);

curl_close ($ch);
$id_arr1=json_decode($server_output1);







$ch = curl_init();

curl_setopt($ch, CURLOPT_URL,$WEKAN_LINK."api/boards/".$WEKAN_BOARD_ID."/lists/".$WEKAN_LIST_ID_ONGOING."/cards/".$wekan_unique_id);
curl_setopt($ch, CURLOPT_HTTPHEADER, array(
    'Content-Type: application/x-www-form-urlencoded',
    'Authorization: Bearer ' . $wekan_token
));
//curl_setopt($ch, CURLOPT_POST, 1);
curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "PUT");
curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
curl_setopt($ch, CURLOPT_POSTFIELDS,
    "listId=".$WEKAN_LIST_ID_TASKCOMPLETED."");

$server_output = curl_exec($ch);

curl_close ($ch);



}



      public function mark_completed(){
        $brief_id=$this->input->post('brief_id');
        
        $array = array('brief_id' => $brief_id);
        $img_status1= array('status' => "5",'cron_mail_status' => '0');
        $this->db->where($array);
        $report = $this->db->update('wc_brief', $img_status1);
        $sql = $this->db->last_query();

        ///////////////////////////////////////////////////////////////////////////
         $checkquerys = $this->db->query("select * from wc_brief where  brief_id='$brief_id' ")->result_array();
         $brand_id=$checkquerys[0]['brand_id'];
         $added_by_user_id=$checkquerys[0]['added_by_user_id'];
         $brief_title=$checkquerys[0]['brief_title'];
         $brand_id=$checkquerys[0]['brand_id'];
         

         //////////////////////////////////////////////////////////////////////    
         $wekan_unique_id=$checkquerys[0]['wekan_unique_id'];
         $status="5";
         $this->update_wekan($wekan_unique_id,$status); 
          //////////////////////////////////////////////////////////////////////  


         $checkquerys = $this->db->query("select * from wc_users where  user_id='$added_by_user_id' ")->result_array();
         $user_id=$checkquerys[0]['user_id'];
         $user_email=$checkquerys[0]['user_email'];
         $user_name=$checkquerys[0]['user_name'];
         $client_id=$checkquerys[0]['client_id'];



        if($brand_id!='')
        {

            
            $checkquerys_mail = $this->db->query("select * from wc_users where  client_id='$client_id' and brand_id like '%,".$brand_id.",%' and user_type_id='4' ")->result_array();


            if(!empty($checkquerys_mail))
            {
            foreach($checkquerys_mail as $key => $value)
            {

            $to_email_all_bm[]=$value['user_email'];

            }

            }




        }

        $checkquerys_clientmail= $this->db->query("SELECT * FROM `wc_clients` WHERE `client_id`='$client_id'")->result_array();
        $client_email=$checkquerys_clientmail[0]['client_email'];
        array_push($to_email_all_bm,$client_email);



        /*********************************Send mail*************************************/

        $query=$this->db->query("select * from  wc_settings  WHERE setting_key='MAIL_SMTP_USER' ");
        $userDetails= $query->result_array();
        $MAIL_SMTP_USER=$userDetails[0]['setting_value'];

        $query=$this->db->query("select * from  wc_settings  WHERE setting_key='MAIL_FROM' ");
        $userDetails= $query->result_array();
        $MAIL_FROM=$userDetails[0]['setting_value'];


        $query=$this->db->query("select * from  wc_settings  WHERE setting_key='MAIL_FROM_TEXT' ");
        $userDetails= $query->result_array();
        $MAIL_FROM_TEXT=$userDetails[0]['setting_value'];

        $query=$this->db->query("select * from  wc_settings  WHERE setting_key='MAIL_SMTP_PASSWORD' ");
        $userDetails= $query->result_array();
        $MAIL_SMTP_PASSWORD=$userDetails[0]['setting_value'];

        $query=$this->db->query("select * from  wc_settings  WHERE setting_key='MAIL_BODY_FEEDBACK' ");
        $userDetails= $query->result_array();
        $MAIL_BODY=$userDetails[0]['setting_value'];


        $uname=$user_name;
        $uemail=$user_email;
        $subject=$MAIL_FROM_TEXT.' - Feedback Mail(#'.$brief_id.')';

        $unique_feedback_id=time()."-".$brief_id."-".$user_id;
        $feedback_link=base_url()."feedback-submit/".base64_encode($unique_feedback_id);

        
        //Load email library
        $this->load->library('email');

        $config = array();
        //$config['protocol'] = 'smtp';
        $config['protocol']     = 'mail';
        $config['smtp_host'] = 'localhost';
        $config['smtp_user'] = $MAIL_SMTP_USER;
        $config['smtp_pass'] = $MAIL_SMTP_PASSWORD;
        $config['smtp_port'] = 25;




        $this->email->initialize($config);
        $this->email->set_newline("\r\n");
/*
        if($client_id!='') {
                    $checkquerys = $this->db->query("select * from wc_clients where client_id='$client_id' ")->result_array();

                    $client_image=$checkquerys[0]['client_image'];
                    $wekan_client_id=$checkquerys[0]['wekan_client_id'];

                    $client_logo=base_url()."/upload_client_logo/".$client_image;
                    }
                    else
                    {
                    $client_logo=base_url()."/website-assets/images/logo.png";
                    }
                    $client_logo="http://style.piquic.com/WeCreate//website-assets/images/logo.png";*/

         $website_url=base_url();
             $dashboard_url=base_url()."dashboard";             

        //$from_email = "demo.intexom@gmail.com";
       $from_email = $MAIL_FROM;
        //$to_email = $uemail;
        $to_email = $to_email_all_bm;

        $date_time=date("d/m/y");
        /*$copy_right=$MAIL_FROM_TEXT." - ".date("Y");*/
         $copy_right=$MAIL_FROM_TEXT;
         $feedback_link="";
         $project_name="#".$brief_id." - ".$brief_title;
        //echo $message=$MAIL_BODY;
        /*$find_arr = array("##CLIENT_LOGO##","##BM_NAME##","##FEEDBACK_SUBMIT_LINK##","##COPY_RIGHT##");
        $replace_arr = array($client_logo,$uname,$feedback_link,$copy_right);*/
         $find_arr = array("##WEBSITE_URL##","##PROJECT_NAME##","##DASHBOARD_LINK##","##COPY_RIGHT##","##FEEDBACK_SUBMIT_LINK##");
            $replace_arr = array($website_url,$project_name,$dashboard_url,$copy_right,$feedback_link);

        $message=str_replace($find_arr,$replace_arr,$MAIL_BODY);

        //echo $message;exit;

 

        $this->email->from($from_email,$MAIL_FROM_TEXT);
        $this->email->to($to_email);
        if($_SERVER['HTTP_HOST']!='collab.piquic.com')
                    {
                        $this->email->cc(array('poulami@mokshaproductions.in','pranav@mokshaproductions.in','raja.priya@mokshaproductions.in','rajendra.prasad@mokshaproductions.in'));
                    }
       // $this->email->bcc('pranav@mokshaproductions.in');
        //$this->email->bcc('raja.priya@mokshaproductions.in');
        $this->email->subject($subject);
        $this->email->message($message);
        //Send mail
        if($this->email->send())
        {

            //echo "mail sent";exit;
            $this->session->set_flashdata("email_sent","Congragulation Email Send Successfully.");

        }
        
        else
        {
            //echo $this->email->print_debugger();
            //echo "mail not sent";exit;
            $this->session->set_flashdata("email_sent","You have encountered an error");
        }


/////////////////////////////////////Mail send end//////////////////////////////////////////////

        echo "success";
        //exit;
    }

}

?>