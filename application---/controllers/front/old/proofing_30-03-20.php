<?php 
class Proofing extends CI_Controller{
    
    function __construct(){
        parent::__construct();
        $this->load->library(['session']); 
        $this->load->helper(['url','file','form']); 
        $this->load->model('uploadbrief_model'); //load model upload 
        $this->load->model('home_model'); //load model upload 
        $this->load->library('upload'); //load library upload 
    }

    public function index($brief_id){

        if ($this->session->userdata('front_logged_in')) {
            $session_data = $this->session->userdata('front_logged_in');
            $data['user_id'] = $session_data['user_id'];
            $data['user_name'] = $session_data['user_name'];
            $user_id = $session_data['user_id'];
            $user_type_id=$session_data['user_type_id'];

            $data['brief_id'] = $brief_id;
            $data['title'] = "Proofing";
            $this->load->view('front/proofing',$data);    
        }
        else
        {
            $data['user_id'] = '';
            $data['user_name'] = '';
            $user_id='';
            redirect('login', 'refresh');
        }


// $data['title'] = "upload images";
// $this->load->view('front/proofing',$data);    
       
    }

    public function update_status()
    {

        if ($this->session->userdata('front_logged_in')) {
            $session_data = $this->session->userdata('front_logged_in');
            $data['user_id'] = $session_data['user_id'];
            $data['user_name'] = $session_data['user_name'];
            $user_id = $session_data['user_id'];
            $user_type_id=$session_data['user_type_id'];

           // $data['brief_id'] = $brief_id;
            $data['title'] = "Proofing";
            $image_id=$_POST['image_id'];
            $brief_id=$_POST['brief_id'];
            $img_status=$_POST['img_status'];
            $array = array('image_id' => $image_id, 'brief_id' => $brief_id);
            $img_status1= array('img_status' => $img_status);
            $this->db->where($array);
            $report = $this->db->update('wc_image_upload', $img_status1);
           $sql = $this->db->last_query();
         //  print_r($sql);
            // $folder_qry = mysqli_query($con, "UPDATE `wc_image_upload` SET `img_status`='$img_status' WHERE `brief_id`='$brief_id' and `image_id`='$image_id'"); 
            echo $report;  
        }
        else
        {
            $data['user_id'] = '';
            $data['user_name'] = '';
            $user_id='';
            redirect('login', 'refresh');
        }

       
    }


    public function use_img()
    {

        if ($this->session->userdata('front_logged_in')) {
            $session_data = $this->session->userdata('front_logged_in');
            $data['user_id'] = $session_data['user_id'];
            $data['user_name'] = $session_data['user_name'];
            $user_id = $session_data['user_id'];
            $user_type_id=$session_data['user_type_id'];

           // $data['brief_id'] = $brief_id;
            $data['title'] = "Proofing";
            $brief_id=$_POST['brief_id'];
            $image_path=$_POST['image_path'];
            $image_id=$_POST['image_id'];
            $img_status=$_POST['img_status'];
            $html="";


            $html.="<div class='col-12 col-sm-12 col-md-12'>
                            <div class='p-4'>
                            <img class='border w-100' src='".base_url()."brief_upload/".$brief_id."/image/".$image_path."'>
                            ";
            if($img_status=='1')
            {
            
                $html.="<button type='submit' class='btn btn-wc btn-block m-2 px-5'>&emsp;&emsp;Approved&nbsp;<i class='fas fa-check'></i>&emsp;&emsp;</button>";
            }else if($img_status=='0'){
                $html.=" <div class='row' id='update_approve'><div class='col-12 col-sm-12 col-md-12'>
                    <div class='d-flex justify-content-center flex-wrap'>
                       ";
                    if($user_type_id=='1'||$user_type_id=='2'||$user_type_id=='3'){
                         $html.="<button type='submit' class='btn btn-outline-wc btn-lg m-2 px-5' onclick='updateApprove(".$image_id.",".$brief_id.")'>&emsp;&emsp;Approve&emsp;&emsp;</button>";
                    }
                   
                    if($user_type_id=='1'||$user_type_id=='2'||$user_type_id=='4'){
                         $html.="<button type='submit' class='btn btn-outline-wc btn-lg m-2 px-5' onclick='review(".$image_id.",".$brief_id.",\"".$image_path."\")'>&emsp;&emsp;Review&emsp;&emsp;</button>";
                    }
                    $html.="
                    </div></div>
                </div>
                </div>
                        </div>";
            }
            echo $html;
           
        }
        else
        {
            $data['user_id'] = '';
            $data['user_name'] = '';
            $user_id='';
            redirect('login', 'refresh');
        }
        
    }

    public function review()
    {
         if ($this->session->userdata('front_logged_in')) {
            $session_data = $this->session->userdata('front_logged_in');
            $data['user_id'] = $session_data['user_id'];
            $data['user_name'] = $session_data['user_name'];
            $user_id = $session_data['user_id'];
            $user_type_id=$session_data['user_type_id'];

            //$data['brief_id'] = $brief_id;
            $data['title'] = "Proofing";
            $brief_id=$_POST['brief_id'];
            $image_path=$_POST['image_path'];
            $image_id=$_POST['image_id'];

            $query= $this->db->query("SELECT * FROM wc_image_upload WHERE image_id='$image_id'");
            $rs= $query->result_array();
            //echo $rs[0]['img_status'];
            if($rs[0]['img_status']==0)
            {
                $array = array('image_id' => $image_id, 'brief_id' => $brief_id);
                $img_status1= array('img_status' => '2');
                $this->db->where($array);
                $report = $this->db->update('wc_image_upload', $img_status1);
            }
            $wc_brief_sql = "SELECT * FROM `wc_brief` where brief_id='$brief_id'";
            $wc_brief_query = $this->db->query($wc_brief_sql);

            $wc_brief_result=$wc_brief_query->result_array();
            //print_r($wc_brief_result);
            // $wc_brief_qrydata = mysqli_fetch_array($wc_brief_query);
            $brief_title=$wc_brief_result[0]['brief_title'];

            $query1= $this->db->query("SELECT * FROM wc_image_upload WHERE image_id='$image_id'");
            $rs1= $query1->result_array();

            if($rs1[0]['img_status']==2 && ($user_type_id=='1'||$user_type_id=='2'||$user_type_id=='4')){ 
                $img_id= 'imgtag';
            } 
            else{
                $img_id= '';
            }
            $html=""; 
            $html.="<div class='col-12 col-sm-12 col-md-9' >
                        <div class='p-4' >
                            <img class='border w-100' id='".$img_id."' src='".base_url()."brief_upload/".$brief_id."/image/".$image_path."'>";
                        if($rs1[0]['img_status']==3)
                        {
                            $html.="<div class='bg-light w-100 text-center p-2 lead'>&emsp;&emsp;Under Revision&nbsp;<i class='fas fa-sync-alt'></i>&emsp;&emsp;</div>";
                        }
                         if($rs1[0]['img_status']==3)
                        {
                             $html.=" <div class='row' id='update_approve'><div class='col-12 col-sm-12 col-md-12'>
                                <div class='d-flex justify-content-center flex-wrap'>
                                   ";
                                if($user_type_id=='1'||$user_type_id=='2'||$user_type_id=='3'){
                                     $html.="<button type='submit' class='btn btn-outline-wc btn-lg m-2 px-5' onclick='updateApprove(".$image_id.",".$brief_id.")'>&emsp;&emsp;Approve&emsp;&emsp;</button>";
                                }
                               
                              
                                $html.="
                                </div></div>
                            </div>
                            </div>
                                    </div>";
                        }   
                       $html.=  "
                            <div id='tagbox' class='tagbox'></div>
                        </div>
                    </div>
                    <div class='col-12 col-sm-12 col-md-3'>
                        <div class='py-4 p-1 w-100'>
                            <p class='lead'>Notes - ".$brief_title."</p>
                                <div class='taglist' id='taglist' > 
                                    <table class='table table-striped'>
                                        <thead>
                                            <tr>
                                                <th>ID</th>
                                                <th>Annotation</th>
                                                <th>Action</th>
                                            </tr>
                                        </thead>

                                        <tbody id='list-id' class='list-id'>

                                        </tbody>
                                    </table>
                                </div> 
                            <div id='sendRevision'>";
                if($rs1[0]['img_status']==2 && ($user_type_id=='1'||$user_type_id=='2'||$user_type_id=='4'))
                {
                    $html.="        <button type='submit' class='btn btn-outline-wc btn-block px-5 mb-2' onclick='sendRevision(\"".$image_id."\",\"".$brief_id."\")'>&emsp;&emsp;Send Revision&emsp;&emsp;</button>

                            <button type='submit' class='btn btn-outline-danger btn-block px-5'>&emsp;&emsp;Cancel&emsp;&emsp;</button>";
                }
                // else if($rs[0]['img_status']==3)
                // {
                //     $html.="<button type='submit' class='btn btn-outline-wc btn-block px-5 mb-2'>&emsp;&emsp;".$brief_title ."Sent&nbsp;<i class='fas fa-sync-alt'></i>&emsp;&emsp;</button>";
                // }
                            $html.="
                            </div>
                        </div>
                    </div>
                    ";

            echo $html;
           
        }
        else
        {
            $data['user_id'] = '';
            $data['user_name'] = '';
            $user_id='';
            redirect('login', 'refresh');
        }

        
    }
    public function annotation_list()
    {
         if ($this->session->userdata('front_logged_in')) {
            $session_data = $this->session->userdata('front_logged_in');
            $data['user_id'] = $session_data['user_id'];
            $data['user_name'] = $session_data['user_name'];
            $user_id = $session_data['user_id'];
            $user_type_id=$session_data['user_type_id'];

            //$data['brief_id'] = $brief_id;
            $data['title'] = "Proofing";
            $image_id=$_POST['image_id'];
        
            $query= $this->db->query("SELECT * FROM wc_image_annotation WHERE image_id='$image_id'");
            $rs= $query->result_array();
            //print_r($rs);


            $query1= $this->db->query("SELECT * FROM wc_image_upload WHERE image_id='$image_id'");
            $rs1= $query1->result_array();
            //echo $rs1[0]['img_status'];

            $count=$query->num_rows();
            $data['boxes'] = '';
            $data['lists'] = '';
            $i=1;
            if($count>0)
            {
                if ($rs){
                    foreach ($rs as $rs_key => $rs_value) {
                     //echo $rs_value['id'];
                   
                    // do{
                    //  echo $rs_value['id'];
                        $data['boxes'] .= '<div class="tagview" style="left:' . $rs_value['position_left'] . 'px;top:' . $rs_value['position_top'] . 'px;" >';
                        $data['boxes'] .= '<div class="square"></div>';
                        $data['boxes'] .= '<div class="person popup" style="left:' . $rs_value['position_left'] . 'px;top:' . $rs_value['position_top']  . 'px;" id="view_'.$rs_value['id'].'">' . $rs_value['descr'] . '</div>';
                        $data['boxes'] .= '</div>';

                        $data['lists'] .= '<tr id="'.$rs_value['id'].'">
                        <td>'.$i.'</td>
                        <td>
                        <a class="name" id="span_name' . $rs_value['id'] . '">' . $rs_value['descr'] . '</a>
                        <textarea class="form-control input-sm d-none" type="text" id="name'.$rs_value['id'].'" name="name" >'. $rs_value['descr'] . '</textarea>
                        </td>';
                        $data['lists'] .='<td>';
                        if($rs1[0]['img_status']=='2' && ($user_type_id=='1'||$user_type_id=='2'||$user_type_id=='4')){
                        $data['lists'] .='
                        <button type="button" id="editBtn'.$rs_value['id'].'" class="btn btn-sm btn-default" onclick="edit_button('.$rs_value['id'].')" style="background: none;     font-size: 20px;">
                        <i class="far fa-edit text-wc"></i>
                        </button> 

                        <button type="button" id="updateBtn'.$rs_value['id'].'" class="btn btn-sm btn-default d-none" onclick="update_button('.$rs_value['image_id'].','.$rs_value['id'].','. $rs_value['position_left'].','. $rs_value['position_top'].')" style="background: none;     font-size: 20px;"><i class="far fa-check-circle text-success"></i></button> 


                        <button type="button" id="cancleBtn'.$rs_value['id'].'" class="btn btn-sm btn-default d-none" onclick="cancle_button('.$rs_value['id'].')" style="background: none;     font-size: 20px;"><i class="far fa-times-circle text-danger" aria-hidden="true"></i></button> 

                        <button type="button" class="btn btn-sm btn-default " id="delete_'.$rs_value['id'].'" onclick="remove1('.$rs_value['id'].')" style="background: none;     font-size: 20px;">
                        <i class="far fa-trash-alt text-danger"></i>
                        </button> 

                        <button type="button" id="remove_'.$rs_value['id'].'" class="btn btn-sm btn-default d-none" onclick="remove('.$rs_value['id'].','.$rs_value['image_id'].')" style="background: none;     font-size: 20px;"><i class="far fa-check-circle text-success"></i></button>

                            <button type="button" id="cancleBtn1'.$rs_value['id'].'" class="btn btn-sm btn-default d-none" onclick="cancle_button1('.$rs_value['id'].')" style="background: none;     font-size: 20px;"><i class="far fa-times-circle text-danger" aria-hidden="true"></i></button> 
                            ';
                        }
                        $data['lists'] .= ' </td>
                        </tr>';
                       $i++;
                    // }while($rs = $query->result_array());
                    }
                }
            }
            else{
                $data['lists'] .= '<tr><td colspan="5">Annotation Is Not Found...</td></tr>';
            }

           echo json_encode( $data );
           
        }
        else
        {
            $data['user_id'] = '';
            $data['user_name'] = '';
            $user_id='';
            redirect('login', 'refresh');
        }
        

    }
    public function annotation_crud()
    {
         if ($this->session->userdata('front_logged_in')) {
            $session_data = $this->session->userdata('front_logged_in');
            $data['user_id'] = $session_data['user_id'];
            $data['user_name'] = $session_data['user_name'];
            $user_id = $session_data['user_id'];
            $user_type_id=$session_data['user_type_id'];            
            // $data['brief_id'] = $brief_id;
            $data['title'] = "Proofing";
            if( !empty( $_POST['type'] ) && $_POST['type'] == "insert" )
            {
                $image_id = $_POST['image_id'];  
                // $sku_no=substr($id, 7);

                $descr = $_POST['descr'];
                $position_left = $_POST['position_left'];
                $position_top = $_POST['position_top'];
               
                $create_date=date('Y/m/d');

                $sql = "INSERT INTO wc_image_annotation(`user_id`,`image_id`,`descr`,  `position_left`, `position_top`, `create_date`)VALUES('$user_id','$image_id','$descr','$position_left','$position_top','$create_date');";

                // print_r($sql);
                $qry =$this->db->query($sql);
            }

            if( !empty( $_POST['type'] ) && $_POST['type'] == "remove")
            {
              $id = $_POST['id'];
              $sql = "DELETE FROM wc_image_annotation WHERE id = '".$id."'";
              $qry =$this->db->query($sql);
            }
            if( !empty( $_POST['type'] ) && $_POST['type'] == "update")
            {
              

              $id = $_POST['id']; 
              $image_id = $_POST['image_id']; 
              $descr = $_POST['descr'];
              $position_left = $_POST['position_left'];
              $position_top = $_POST['position_top'];

              $sql = "UPDATE wc_image_annotation SET descr='$descr' WHERE id = $id && position_left=$position_left && position_top=$position_top";
             // print_r("UPDATE gd_image_annotation SET descr='$descr' WHERE id = $id && position_left=$position_left && position_top=$position_top");
               $qry =$this->db->query($sql);
            }

        }
        else
        {
            $data['user_id'] = '';
            $data['user_name'] = '';
            $user_id='';
            redirect('login', 'refresh');
        }
        
    }



}

?>