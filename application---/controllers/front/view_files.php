<?php 
class View_files extends CI_Controller{
    
    function __construct(){
        parent::__construct();
        $this->load->library(['session']); 
        $this->load->helper(['url','file','form']); 
        $this->load->model('uploadbrief_model'); //load model upload 
        $this->load->model('Dashboard_model'); //load model upload 
		$this->load->model('Viewfiles_model'); //load model upload
        $this->load->library('upload'); //load library upload 
        $this->load->library('zip');
    }

    public function index($brief_id){

        if ($this->session->userdata('front_logged_in')) {
        $session_data = $this->session->userdata('front_logged_in');
        $data['user_id'] = $session_data['user_id'];
        $data['user_name'] = $session_data['user_name'];
		$type1 = $session_data['user_type_id'];
		$this->session->set_userdata('previous_url', "");
		
		$data['brief_id'] = $brief_id;
		
        $data['title'] = "upload images";
        $data['viewfilesinfo'] = $this->Viewfiles_model->viewfiles($brief_id);	
    
        $this->load->view('front/view_files',$data);    
        }
        else
        {
        $data['user_id'] = '';
        $data['user_name'] = '';
        $user_id='';
        $this->session->set_userdata('previous_url', current_url());
        redirect('login', 'refresh');
        }       
    
						
	}
	
public function getfilessort()
{
	 $brief_id=preg_replace('/[^a-zA-Z0-9_ -]/s','',$_POST['brief_id']);
     $image_status=preg_replace('/[^a-zA-Z0-9_ -]/s','',$_POST['status']);	
     $data['brief_id']=$brief_id;
	 $data['viewfilesinfo'] = $this->Viewfiles_model->viewfilessearch($image_status,$brief_id);	
     $this->load->view('front/viewfiles_search',$data);
}


public function downloadall(){
 $brief_id=addslashes($this->input->post('brief_id'));
	 $checkquerys = $this->db->query("select * from wc_brief where  brief_id='$brief_id' ")->result_array();
	$added_by_user_id=$checkquerys[0]['added_by_user_id'];
	$brand_id=$checkquerys[0]['brand_id'];
	$brief_title=$checkquerys[0]['brief_title'];
	$brief_name=preg_replace('/[^a-zA-Z0-9_.]/', '_',$brief_title);


	$checkquerys = $this->db->query("select wc_brands.*,wc_clients.client_name from wc_brands
    inner join wc_clients on wc_brands.client_id=wc_clients.client_id
    where wc_brands.brand_id='$brand_id' ")->result_array();

	$brand_id=$checkquerys[0]['brand_id'];
	$brand_name=preg_replace('/[^a-zA-Z0-9_.]/', '_',$checkquerys[0]['brand_name']);
	$client_id=$checkquerys[0]['client_id'];
	$client_name=preg_replace('/[^a-zA-Z0-9_.]/', '_',$checkquerys[0]['client_name']);
	$client_folder=$client_id."-".$client_name;
	$brand_folder=$brand_id."-".$brand_name;
	$brief_folder=$brief_id."-".$brief_name;

	$brief_path="./upload/".$client_folder."/".$brand_folder."/".$brief_folder;
$query=$this->db->query("select image_id from  wc_image_upload WHERE `wc_image_upload`.`brief_id` = $brief_id");   
				$img_list=$query->result_array();
				$i=0;
				$image_id="";
				foreach ($img_list as &$value) {
					 $image_id.=$img_list[$i]['image_id'].",";
					$i++;
				}
				$image_id=substr($image_id,0,-1);
				//echo $image_id;
//print_r($img_list);

          $img_list=explode(",",$image_id);
		  //print_r($img_list);
			 $dir ="downloads/".microtime();
            if (!file_exists($dir)) {
                $save_to =$dir."/Images_download".$dir."/";
                mkdir($dir."/Images_download".$dir, 0777, true);
                
            } 
            $zip_file ='downloads/Images_download'.time().'.zip';
			foreach ($img_list as &$value) {
				$query=$this->db->query("select * from  wc_image_upload WHERE `wc_image_upload`.`image_id` = $value");   
				$row=$query->result_array();
				$image_path=$row[0]['image_path'];
				$brief_id=$row[0]['brief_id'];
				$parent_id=$row[0]['parent_img']; 
		   
		   		$brief_path="./upload/".$client_folder."/".$brand_folder."/".$brief_folder;	   
				$revision_sql="SELECT * FROM `wc_revision` where image_id='$value' ORDER BY `wc_revision`.`revision_id` DESC LIMIT 1";
				$wc_rev_query = $this->db->query($revision_sql);
				$wc_rev_result=$wc_rev_query->result_array();
				@$rev_folder=$wc_rev_result[0]['revision_name'];


				$revision_id=$row[0]['version_num'];
				$image_id=$parent_img=$row[0]['parent_img'];
				$rev_folder="Revision".$revision_id;
										 

				$directory=$brief_path."/".$parent_id.'/'.$rev_folder.'/'.$image_path;
				 $image_name=$save_to.$image_path;
				
				if (!copy($directory, $image_name)) {
					echo "failed to copy $file_path...\n";
				}
			}

		
			// $files =glob($imgdirname."*.{zip,ZIP}", GLOB_BRACE);	
			// foreach($files as $zip_file_path){
			// 	//unlink($zip_file_path);
			// } 


			// Create new zip class 
			$rootPath = realpath($dir);

            // Initialize archive object
            $zip = new ZipArchive();
            $zip->open($zip_file, ZipArchive::CREATE | ZipArchive::OVERWRITE);

            // Create recursive directory iterator
            /** @var SplFileInfo[] $files */
          $files = new RecursiveIteratorIterator(
                new RecursiveDirectoryIterator($rootPath),
                RecursiveIteratorIterator::LEAVES_ONLY
            );
            foreach ($files as $name => $file)
            {
                // Skip directories (they would be added automatically)
                if (!$file->isDir())
                {
                    // Get real and relative path for current file
                    $filePath = $file->getRealPath();
                    $relativePath = substr($filePath, strlen($rootPath) + 1);

                    // Add current file to archive
                    $zip->addFile($filePath, $relativePath);
                }
            }
            $zip->close();
             //It it's a file.
             
            if(file_exists($dir))
            {
                $this->deleteAll($dir);
                echo $zip_file;
            }
            else{
                echo $dir;
            }



}



public function downloadall_old(){
$brief_id=$_POST['brief_id'];
$brief_id=$brief_id;
	$checkquerys = $this->db->query("select * from wc_brief where  brief_id='$brief_id' ")->result_array();
	$added_by_user_id=$checkquerys[0]['added_by_user_id'];
	$brand_id=$checkquerys[0]['brand_id'];
	$brief_title=$checkquerys[0]['brief_title'];
	$brief_name=preg_replace('/[^a-zA-Z0-9_.]/', '_',$brief_title);


	$checkquerys = $this->db->query("select wc_brands.*,wc_clients.client_name from wc_brands
    inner join wc_clients on wc_brands.client_id=wc_clients.client_id
    where wc_brands.brand_id='$brand_id' ")->result_array();

	$brand_id=$checkquerys[0]['brand_id'];
	$brand_name=preg_replace('/[^a-zA-Z0-9_.]/', '_',$checkquerys[0]['brand_name']);
	$client_id=$checkquerys[0]['client_id'];
	$client_name=preg_replace('/[^a-zA-Z0-9_.]/', '_',$checkquerys[0]['client_name']);
	$client_folder=$client_id."-".$client_name;
	$brand_folder=$brand_id."-".$brand_name;
	$brief_folder=$brief_id."-".$brief_name;

	$brief_path="./upload/".$client_folder."/".$brand_folder."/".$brief_folder;

          $img_list=explode(",",$image_id);
			 $dir =  microtime();
            if (!file_exists($dir)) {
                $save_to =$dir."/Images_download".$dir."/";
                mkdir($dir."/Images_download".$dir, 0777, true);
                
            } 
            $zip_file ='Images_download'.time().'.zip';
			foreach ($img_list as &$value) {
				$query=$this->db->query("select * from  wc_image_upload WHERE `wc_image_upload`.`image_id` = $value");   
				$row=$query->result_array();
				$image_path=$row[0]['image_path'];
				$brief_id=$row[0]['brief_id'];
				$parent_id=$row[0]['parent_img'];
		   
		   		$brief_path="./upload/".$client_folder."/".$brand_folder."/".$brief_folder;	   
				$revision_sql="SELECT * FROM `wc_revision` where image_id='$value' ORDER BY `wc_revision`.`revision_id` DESC LIMIT 1";
				$wc_rev_query = $this->db->query($revision_sql);
				$wc_rev_result=$wc_rev_query->result_array();
				@$rev_folder=$wc_rev_result[0]['revision_name'];


				$revision_id=$row[0]['version_num'];
				$image_id=$parent_img=$row[0]['parent_img'];
				$rev_folder="Revision".$revision_id;
										 

				$directory=$brief_path."/".$parent_id.'/'.$rev_folder.'/'.$image_path;
				 $image_name=$save_to.$image_path;
				
				if (!copy($directory, $image_name)) {
					echo "failed to copy $file_path...\n";
				}
			}

		
			// $files =glob($imgdirname."*.{zip,ZIP}", GLOB_BRACE);	
			// foreach($files as $zip_file_path){
			// 	//unlink($zip_file_path);
			// } 


			// Create new zip class 
			$rootPath = realpath($dir);

            // Initialize archive object
            $zip = new ZipArchive();
            $zip->open($zip_file, ZipArchive::CREATE | ZipArchive::OVERWRITE);

            // Create recursive directory iterator
            /** @var SplFileInfo[] $files */
            $files = new RecursiveIteratorIterator(
                new RecursiveDirectoryIterator($rootPath),
                RecursiveIteratorIterator::LEAVES_ONLY
            );
            foreach ($files as $name => $file)
            {
                // Skip directories (they would be added automatically)
                if (!$file->isDir())
                {
                    // Get real and relative path for current file
                    $filePath = $file->getRealPath();
                    $relativePath = substr($filePath, strlen($rootPath) + 1);

                    // Add current file to archive
                    $zip->addFile($filePath, $relativePath);
                }
            }
            $zip->close();
             //It it's a file.
             
            if(file_exists($dir))
            {
                $this->deleteAll($dir);
                echo $zip_file;
            }
            else{
                echo $dir;
            }



}


	
public function changefilestatus(){
	$brief_id=preg_replace('/[^a-zA-Z0-9_ -]/s','',$_POST['brief_id']);	
	$image_id=preg_replace('/[^a-zA-Z0-9_ -]/s','',$_POST['image_id']);
	$status=preg_replace('/[^a-zA-Z0-9_ -]/s','',$_POST['status']);	
	$brief_id=$brief_id;
	$checkquerys = $this->db->query("select * from wc_brief where  brief_id='$brief_id' ")->result_array();
	$added_by_user_id=$checkquerys[0]['added_by_user_id'];
	$brand_id=$checkquerys[0]['brand_id'];
	$brief_title=$checkquerys[0]['brief_title'];
	$brief_name=preg_replace('/[^a-zA-Z0-9_.]/', '_',$brief_title);


	$checkquerys = $this->db->query("select wc_brands.*,wc_clients.client_name from wc_brands
    inner join wc_clients on wc_brands.client_id=wc_clients.client_id
    where wc_brands.brand_id='$brand_id' ")->result_array();

	$brand_id=$checkquerys[0]['brand_id'];
	$brand_name=preg_replace('/[^a-zA-Z0-9_.]/', '_',$checkquerys[0]['brand_name']);
	$client_id=$checkquerys[0]['client_id'];
	$client_name=preg_replace('/[^a-zA-Z0-9_.]/', '_',$checkquerys[0]['client_name']);
	$client_folder=$client_id."-".$client_name;
	$brand_folder=$brand_id."-".$brand_name;
	$brief_folder=$brief_id."-".$brief_name;

	$brief_path="./upload/".$client_folder."/".$brand_folder."/".$brief_folder;
 
 	if(!empty($status)){
	 
		if($status==5){	
			$img_list=explode(",",$image_id);
			foreach ($img_list as &$value) {
				$query=$this->db->query("select * from  wc_image_upload WHERE `wc_image_upload`.`image_id` = $value");   
				$row=$query->result_array();
				$del=$row[0]['image_path'];
				$brief_id=$row[0]['brief_id'];
				$parent_id=$row[0]['parent_img'];
				$version=$row[0]['version_num'];
			
				if($version>1){
					$query_all=$this->db->query("select * from  wc_image_upload WHERE `wc_image_upload`.`Parent_img` = $parent_id");   
					$row_all=$query_all->result_array();
					foreach($row_all as $keybill => $delimages){
						$image_id_all=$delimages['image_id'];
						$del_all=$delimages['image_path'];
						$brief_id_all=$delimages['brief_id'];
						$parent_id_all=$delimages['parent_img'];
						$version_all=$delimages['version_num'];
						$rev_folder_all="Revision".$version_all;

						// echo $imgdirname=$brief_path."/".$parent_id_all."/".$rev_folder_all;
						echo $imgdirname=$brief_path."/".$parent_id_all;
						if(is_dir($imgdirname)){
					        $this->deleteAll($imgdirname);
					        
					        $array = array('parent_img' => $parent_id);
							$this->db->where($array); 
							$result=$this->db->delete('wc_image_upload');	
					    }
					    else{
					        echo "Directory is not available";
					    }
					}
				}
				else 
				{
					$rev_folder="Revision".$version;
					// echo $imgdirname=$brief_path."/".$parent_id."/".$rev_folder;
					$imgdirname=$brief_path."/".$parent_id;
					if(is_dir($imgdirname)){
				        $this->deleteAll($imgdirname);
				        
					    $array = array('parent_img' => $parent_id);
						$this->db->where($array); 
						$result=$this->db->delete('wc_image_upload');	
				    }
				    else{
				        echo "Directory is not available";
				    }
						
					
				}
				$brief_query=$this->db->query("select * from  wc_image_upload WHERE brief_id=$brief_id");   
				$brief_row=$brief_query->result_array();
				$count=$brief_query->num_rows();
				if($count==0)
				{

			        $array = array('brief_id' => $brief_id);
			        $img_status1= array('status' => "1");
			        $this->db->where($array);
			        $report = $this->db->update('wc_brief', $img_status1);
				}
			}
			 $data['brief_id']=$brief_id;
			$data['viewfilesinfo'] = $this->Viewfiles_model->viewfiles($brief_id);	

   	 		$this->load->view('front/viewfiles_search',$data);  
		}
		if($status==4){
			$img_list=explode(",",$image_id);
			 $dir =  microtime();
            if (!file_exists($dir)) {
                $save_to =$dir."/Images_download".$dir."/";
                mkdir($dir."/Images_download".$dir, 0777, true);
                
            } 
            $zip_file ='Images_download'.time().'.zip';
			foreach ($img_list as &$value) {
				$query=$this->db->query("select * from  wc_image_upload WHERE `wc_image_upload`.`image_id` = $value");   
				$row=$query->result_array();
				$image_path=$row[0]['image_path'];
				$brief_id=$row[0]['brief_id'];
				$parent_id=$row[0]['parent_img'];
		   
		   		$brief_path="./upload/".$client_folder."/".$brand_folder."/".$brief_folder;	   
				$revision_sql="SELECT * FROM `wc_revision` where image_id='$value' ORDER BY `wc_revision`.`revision_id` DESC LIMIT 1";
				$wc_rev_query = $this->db->query($revision_sql);
				$wc_rev_result=$wc_rev_query->result_array();
				@$rev_folder=$wc_rev_result[0]['revision_name'];


				$revision_id=$row[0]['version_num'];
				$image_id=$parent_img=$row[0]['parent_img'];
				$rev_folder="Revision".$revision_id;
										 

				$directory=$brief_path."/".$parent_id.'/'.$rev_folder.'/'.$image_path;
				 $image_name=$save_to.$image_path;
				
				if (!copy($directory, $image_name)) {
					echo "failed to copy $file_path...\n";
				}
			}

		
			// $files =glob($imgdirname."*.{zip,ZIP}", GLOB_BRACE);	
			// foreach($files as $zip_file_path){
			// 	//unlink($zip_file_path);
			// } 


			// Create new zip class 
			$rootPath = realpath($dir);

            // Initialize archive object
            $zip = new ZipArchive();
            $zip->open($zip_file, ZipArchive::CREATE | ZipArchive::OVERWRITE);

            // Create recursive directory iterator
            /** @var SplFileInfo[] $files */
            $files = new RecursiveIteratorIterator(
                new RecursiveDirectoryIterator($rootPath),
                RecursiveIteratorIterator::LEAVES_ONLY
            );
            foreach ($files as $name => $file)
            {
                // Skip directories (they would be added automatically)
                if (!$file->isDir())
                {
                    // Get real and relative path for current file
                    $filePath = $file->getRealPath();
                    $relativePath = substr($filePath, strlen($rootPath) + 1);

                    // Add current file to archive
                    $zip->addFile($filePath, $relativePath);
                }
            }
            $zip->close();
             //It it's a file.
             
            if(file_exists($dir))
            {
                $this->deleteAll($dir);
                echo $zip_file;
            }
            else{
                echo $dir;
            }
			
	 	}

	 }
}
public function deleteAll($str) {
	//It it's a file.
	if (is_file($str)) {
		//Attempt to delete it.
		return unlink($str);
	}
	//If it's a directory.
	elseif (is_dir($str)) {
	//Get a list of the files in this directory.
		$scan = glob(rtrim($str,'/').'/*');
		//Loop through the list of files.
		foreach($scan as $index=>$path) {
		//Call our recursive function.
			$this-> deleteAll($path);
		}
		//Remove the directory itself.
     	return @rmdir($str);
		}
}

}

?>