<?php  
class Dashboard extends CI_Controller{
//https://wekan.github.io/api/v2.55/?javascript#get_board
	function __construct(){
		parent::__construct();
		ob_start();
		$this->load->library(['session']); 
		$this->load->helper(['url','file','form']); 
        $this->load->model('uploadbrief_model'); //load model upload 
        $this->load->model('Dashboard_model'); //load model upload 
        $this->load->library('upload'); //load library upload 
     } 

     public function index(){ 

     	if ($this->session->userdata('front_logged_in')) {
     		$session_data = $this->session->userdata('front_logged_in');
     		$data['user_id'] = $session_data['user_id'];
     		$data['user_type_id'] = $session_data['user_type_id'];
     		$data['user_name'] = $session_data['user_name'];
     		$type1 = $session_data['user_type_id'];


     		$client_access=$session_data['client_access'];
     		$brand_access=$session_data['brand_access'];
			$user_id = $session_data['user_id'];
			$checkquerys = $this->db->query("select brief_id,brand_id from wc_users where user_id='$user_id' ")->result_array();
			//$brief_access=$checkquerys[0]['brief_id'];
			$brand_access=$checkquerys[0]['brand_id'];
     		

     		$account_id='';
     		$search_key='';
     		$rowperpage = 12;
     		$lastid = "";
     		$allcount = $this->Dashboard_model->getbriefCount($lastid);


     		$data['showLimit']=$rowperpage;
     		$data['allNumRows']=$allcount;

     		$data['dashboradinfo']=$this->Dashboard_model->briefListDetails($rowperpage);

        //print_r($data['dashboradinfo']);


     		$data['brandsinfo'] = $this->Dashboard_model->getBrandtlist();

     		$data['briefinreview']= $this->Dashboard_model->getbriefinreview($account_id,$brand_access,$search_key);
     		$data['briefrejected']= $this->Dashboard_model->getbriefrejected($account_id,$brand_access,$search_key);
     		$data['workinprogress']= $this->Dashboard_model->getworkinprogress($account_id,$brand_access,$search_key);
     		$data['proffing_pending']= $this->Dashboard_model->getproffing_pending($account_id,$brand_access,$search_key);
     		$data['revision_work']= $this->Dashboard_model->getrevision_work($account_id,$brand_access,$search_key);
     		$data['work_complete']= $this->Dashboard_model->getwork_complete($account_id,$brand_access,$search_key);
     		$data['feedback_pending']= $this->Dashboard_model->getfeedback_pending($account_id,$brand_access,$search_key);
     		$data['feedback_completed']= $this->Dashboard_model->getfeedback_completed($account_id,$brand_access,$search_key);
     		$data['title'] = "upload images";
     		$this->load->view('front/dashboard',$data);
    // $this->load->view('front/dashboard_new',$data);
     	}
     	else
     	{
     		$data['user_id'] = '';
     		$data['user_type_id'] = '';
     		$data['user_name'] = '';
     		$user_id='';
     		redirect('login', 'refresh');
     	}       
     }

     public function storelog()
     {
     	$brief_id=$this->input->post('brief_id');
     	$logdate=$this->input->post('logdate');
     	$loghours=$this->input->post('loghours');
     	$logmins=$this->input->post('logmins');
     	$logdate=  date("Y-m-d", strtotime($logdate));
     	$session_data = $this->session->userdata('front_logged_in');
     	$user_id= $session_data['user_id'];
     	$user_type_id= $session_data['user_type_id'];
     	$user_name= $session_data['user_name'];

     	if(($brief_id!='')&&($user_id!='')&&($logdate!='')&&($logdate!='')&&($loghours!='')&&($logmins!=''))
     	{
     		$sql = "INSERT INTO wc_brieflogs(brief_id,user_added_by,log_date,worked_hours,worked_mins)VALUES('$brief_id','$user_id','$logdate','$loghours','$logmins')";
     		$qry =$this->db->query($sql);

     		$brief_log_id=$this->db->insert_id();

     		if($brief_log_id!='')
     		{
            //echo "<p class='w-100 text-center text-wc lead'><i class='fas fa-check-circle'></i>&nbsp;Log Saved</p>";
            //echo "<script>$('click_".$brief_id."').trigger('click');</script>";
     			echo "success";
     		}
     	}
     	else
     	{
     		echo "<p class='w-100 text-center text-danger'><i class='fas fa-info-circle'></i>&nbsp;Please Fill All Data</p>";
     	}
     }

public function sortbrand()
{
	$user_type_id=$this->input->post('user_type_id');
    $searchkey=$this->input->post('keytosearch');
	$client_id=$this->input->post('client_id');
	$lastID=$this->input->post('lastID');
	$brandid=$this->input->post('brandid');

											$brand_id_check_add="";
											//$lastID='';
											$html='';
										 $sql="select * from wc_brands where status='Active' and client_id=$client_id";

										if( ($user_type_id=='2' || $user_type_id=='3') &&  $client_id!='0')
										{
											$sql.=" and brand_id in (select brand_id from wc_brands where client_id='".$client_id."') ";
										}

										$brand_type_check=$this->db->query($sql)->result_array();
										

										foreach($brand_type_check as $key => $brandstypedetails){
											$brand_id_check_add.=$brandstypedetails['brand_id'].",";
										}


										$sql1="select * from wc_brief WHERE 1=1 and brand_id IN(".rtrim($brand_id_check_add,",").")";
										$brand_id_check_add=$this->db->query($sql1)->result_array();
										$all_count_check=count($brand_id_check_add); 


										$sql="select * from wc_brands where status='Active' and brand_name like '%$searchkey%'";
										if( ($user_type_id=='2' || $user_type_id=='3') &&  $client_id!='0')
										{
											$sql.=" and brand_id in (select brand_id from wc_brands where client_id='".$client_id."') ";
										}
										if( $user_type_id=='4' &&  $brand_id!='0' )
										{
											$sql.=" and brand_id in (".trim($brand_id,",").") ";
										}
										//echo $sql;
										$brand_type_details=$this->db->query($sql)->result_array();
										$xyz=count($brand_type_details); 
										if($xyz=='0')
										{
											echo "No Match Found";

										}
										if(!empty($brand_type_details)){
											foreach($brand_type_details as $key => $brandstypedetails){
												$brand_id=$brandstypedetails['brand_id'];
												$brand_name=$brandstypedetails['brand_name'];
												$brand_id_check=$brandstypedetails['brand_id'];
												$sql="select * from wc_brief WHERE 1=1 and brand_id IN(".trim($brand_id_check,",").")";
												$brand_type_check=$this->db->query($sql)->result_array();
												$count_check=count($brand_type_check);

												$checked="";
												if($brandid==$brand_id)
												{
													$checked='checked';
												}
												
												$html.='<div class="custom-control custom-radio">
													<input type="radio" class="custom-control-input brand_id" id="brand_'.$brand_id.'" name="brand_id[]" onchange="sortbrief('.$lastID.')" value='.$brand_id.' brand_name="'.$brand_name.'" '.$checked.'>
													<label class="custom-control-label roundCheck lead" for="brand_'.$brand_id.'">'.ucfirst($brand_name).'<span class="badge badge-wc" id="cnt_'. $brand_id.'">'.$count_check.'</span></label>
												</div>';
											 } } 
											echo $html;
	
	

}
     
     public function listalllog()
     {
     	$emp_id=$this->input->post('emp_id');
     	$logfromdate=$this->input->post('logfromdate');
     	$logtodate=$this->input->post('logtodate');
     	if(substr($emp_id,0,1)==','){ $emp_id=substr($emp_id,1); }
     	$logfromdate=  date("Y-m-d", strtotime($logfromdate));
     	$logtodate=  date("Y-m-d", strtotime($logtodate));
    //echo $sql="select wc_brieflogs.log_id,wc_brieflogs.brief_id,wc_brieflogs.worked_hours,wc_brieflogs.worked_mins,wc_brief.brief_title from wc_brieflogs INNER JOIN wc_brief WHERE wc_brieflogs.brief_id=wc_brief.brief_id and wc_brieflogs.user_added_by=$emp_id and log_date BETWEEN '$logfromdate' AND '$logtodate'";
     	if($emp_id!='')
     	{
     		if($emp_id=="all")
     		{
     			$session_data = $this->session->userdata('front_logged_in');
     			$user_id= $session_data['user_id'];
     			$checkquerys = $this->db->query("select * from wc_users where  user_id='$user_id' ")->result_array();
     			$client_id = $checkquerys[0]['client_id'];
     			$emp_id_arr=array();
     			$user_details=$this->db->query("select * from wc_users where user_type_id=6")->result_array();
     			$i=0;
     			foreach ($user_details as $row) {
     				$emp_id = $user_details[$i]['user_id'];
     				$emp_client_id = $user_details[$i]['client_id'];
     				if($emp_client_id!='0')
     				{
     					$emp_client_id_ar = explode(',',$emp_client_id);
     					if(in_array($client_id, $emp_client_id_ar))
     					{
     						array_push($emp_id_arr,$emp_id);
     					}
     				}
     				$i++;
     			}
     			$emp_id= implode(",",$emp_id_arr);

     			$log_details=$this->db->query("select wc_brieflogs.log_id,wc_brieflogs.brief_id,wc_brieflogs.log_date,wc_brieflogs.worked_hours,wc_brieflogs.worked_mins,wc_brief.brief_title from wc_brieflogs INNER JOIN wc_brief WHERE wc_brieflogs.brief_id=wc_brief.brief_id and wc_brieflogs.user_added_by IN ($emp_id) and log_date BETWEEN '$logfromdate' AND '$logtodate'
     				")->result_array();     

     		}
     		else
     		{
     			$log_details=$this->db->query("select wc_brieflogs.log_id,wc_brieflogs.brief_id,wc_brieflogs.log_date,wc_brieflogs.worked_hours,wc_brieflogs.worked_mins,wc_brief.brief_title from wc_brieflogs INNER JOIN wc_brief WHERE wc_brieflogs.brief_id=wc_brief.brief_id and wc_brieflogs.user_added_by IN ($emp_id) and log_date BETWEEN '$logfromdate' AND '$logtodate'
     				")->result_array();
     		}
     	}
/*else
{
$log_details=$this->db->query("select wc_brieflogs.log_id,wc_brieflogs.brief_id,wc_brieflogs.log_date,wc_brieflogs.worked_hours,wc_brieflogs.worked_mins,wc_brief.brief_title from wc_brieflogs INNER JOIN wc_brief WHERE wc_brieflogs.brief_id=wc_brief.brief_id and log_date BETWEEN '$logfromdate' AND '$logtodate'
")->result_array();
}  */   
$logfromdate=  date("d M Y", strtotime($logfromdate));   
$logtodate=  date("d M Y", strtotime($logtodate)); 
echo '<p class="w-100 font-weight-bold text-center text-wc">'.$logfromdate.' - '.$logtodate.'</p><div class="mb-3 scrl" style="max-height: 255px;" >';

if(!empty($log_details))
{
	$hh=0;
	$mm=0;
	foreach($log_details as $key => $viewlogdetails)
	{
		$brief_id=$viewlogdetails['brief_id'];
		$brief_title=$viewlogdetails['brief_title'];
		$worked_hours=$viewlogdetails['worked_hours'];
		$worked_mins=$viewlogdetails['worked_mins'];
		$id_log=$viewlogdetails['log_id'];
		$log_date=$viewlogdetails['log_date'];
		$hh+=$worked_hours;
		$mm+=$worked_mins;

                                    //$button='<span class="btn btn-default p-0 remove_button432" id="delete_270" onclick="removelog('.$id_log.','.$brief_id.')" title="Delete"><i class="far fa-trash-alt text-wc"></i></span>';
		?>                          

		<div class="d-flex justify-content-between flex-wrap">
			<div class="py-1 text-wc">
				<span>#P<?php echo $brief_id; ?>&nbsp;<?php echo $brief_title; ?>&nbsp;-&nbsp;( on <?php echo $log_date; ?>)</span>
			</div>

			<div class="d-flex justify-content-between">
				<span class="px-2 pt-1 text-wc"><?php echo $worked_hours.":".$worked_mins; ?></span>
				<?php //echo $button; ?>
			</div>
		</div>
		<?php
	} 
	$hrstomm=$hh*60;
	$mm+=$hrstomm;
                                //$hours = intdiv($mm, 60).' : '. ($mm % 60);
	$x= floor($mm / 60);
	$y=($mm -   floor($mm / 60) * 60);
	$hrs=str_pad($x, 2, "0", STR_PAD_LEFT);
	$mns=str_pad($y, 2, "0", STR_PAD_LEFT);
	$hours =$hrs.':'.$mns;


	?>
</div>
<hr class="border p-0 mt-0">

<div class="d-flex justify-content-between flex-wrap" style="margin-bottom:1rem">
	<div class="py-1 text-wc">
		<span>Total working Hours</span>
	</div>

	<div class="d-flex justify-content-between">
		<span class="px-2 pt-1 text-wc"><?php echo $hours; ?></span>
		<?php //echo $button; ?>
	</div>
</div>
<hr class="border p-0 mt-0">
<?php


}
else
{
	echo '<p class="w-100 font-weight-bold text-center text-wc">No logs found</p>';
}



}
public function listlog()
{
	$brief_id=$this->input->post('brief_id');
	$logdate=$this->input->post('logdate');
	$logdate=  date("Y-m-d", strtotime($logdate));
    //echo $sql="select wc_brieflogs.log_id,wc_brieflogs.brief_id,wc_brieflogs.worked_hours,wc_brieflogs.worked_mins,wc_brief.brief_title from wc_brieflogs INNER JOIN wc_brief WHERE wc_brieflogs.brief_id=wc_brief.brief_id and wc_brief.brief_id=$brief_id and log_date like '$logdate'";
	$log_details=$this->db->query("select wc_brieflogs.log_id,wc_brieflogs.brief_id,wc_brieflogs.worked_hours,wc_brieflogs.worked_mins,wc_brief.brief_title from wc_brieflogs INNER JOIN wc_brief WHERE wc_brieflogs.brief_id=wc_brief.brief_id and wc_brief.brief_id=$brief_id and log_date like '$logdate'
		")->result_array();  
	$logdate=  date("d M Y", strtotime($logdate));   
	echo '<p class="w-100 font-weight-bold text-center text-wc">'.$logdate.'</p>';
	if(!empty($log_details))
	{


		foreach($log_details as $key => $viewlogdetails)
		{
			$brief_title=$viewlogdetails['brief_title'];
			$worked_hours=$viewlogdetails['worked_hours'];
			$worked_mins=$viewlogdetails['worked_mins'];
			$id_log=$viewlogdetails['log_id'];

			$button='<button type="button" class="btn btn-default btn-lg p-0 remove_button432" id="delete_270" onclick="removelog('.$id_log.','.$brief_id.')" title="Delete">
			<i class="far fa-trash-alt text-wc"></i></button>';
            //echo "<p>#P ".$brief_id." &nbsp; ".$brief_title."&nbsp ".$worked_hours.":".$worked_mins."  ".$button."</p>";

			echo $pho='

			<div class="d-flex justify-content-between flex-wrap">
			<div class="py-1 text-wc">
			<span>#P&nbsp;'.$brief_id.'&nbsp;'.$brief_title.'</span>
			</div>

			<div class="d-flex justify-content-between">
			<span class="px-2 pt-1 text-wc">'.$worked_hours.'&nbsp;:&nbsp;'.$worked_mins.'</span>'.$button.' </div></div>';


		}
	}
	else
	{
		echo '<p class="w-100 font-weight-bold text-center text-wc">No logs found</p>';
	}
}
public function removelog()
{
	$brief_id=$this->input->post('brief_id');
	$log_id=$this->input->post('log_id');

	$array = array('log_id' => $log_id);
	$this->db->where($array); 
	$result=$this->db->delete('wc_brieflogs');


}
public function updateuser_brief()
{
	$brief_id=$this->input->post('brief_id');
	$emp_id=$this->input->post('emp_id');
	$client_id=$this->input->post('client_id');
	if($emp_id=='all')
	{
		$user_details=$this->db->query("select * from wc_users where client_id like '%,".$client_id.",%' and  user_type_id=6")->result_array();
		$i=0;
		foreach ($user_details as $row) {
			$emp_id = $user_details[$i]['user_id'];
			$emp_brief_id = $user_details[$i]['brief_id'];
			$emp_client_id = $user_details[$i]['client_id'];
			if($emp_client_id!='0')
			{
				$emp_client_id_ar = explode(',',$emp_client_id);
				if(in_array($client_id, $emp_client_id_ar))
				{
					if($emp_brief_id!='')
					{
						$emp_brief_id_ar = explode(',',$emp_brief_id);
						if(in_array($brief_id, $emp_brief_id_ar))
						{
							if (($key = array_search($brief_id, $emp_brief_id_ar)) !== false) {
            				//unset($emp_brief_id_ar[$key]);
							}

						}
						else
						{
							array_push($emp_brief_id_ar,$brief_id);
						}
						$updated_emp_brief_id=implode(",",$emp_brief_id_ar);
						$set_con1 = array('brief_id' => $updated_emp_brief_id);
						$where_con1= array('user_id'=>$emp_id);
						$this->db->where($where_con1);
						$report = $this->db->update('wc_users', $set_con1);
					}
					else
					{
						$set_con1 = array('brief_id' => $brief_id);
						$where_con1= array('user_id'=>$emp_id);
						$this->db->where($where_con1);
						$report = $this->db->update('wc_users', $set_con1);     
					}   
				}
			}                
			$i++;
		}  
	}
	elseif($emp_id=='unall')
	{
		$user_details=$this->db->query("select * from wc_users where client_id like '%,".$client_id.",%' and  user_type_id=6")->result_array();
		$i=0;
		foreach ($user_details as $row) {
			$emp_id = $user_details[$i]['user_id'];
			$emp_brief_id = $user_details[$i]['brief_id'];
			$emp_client_id = $user_details[$i]['client_id'];
			if($emp_client_id!='0')
			{
				$emp_client_id_ar = explode(',',$emp_client_id);
				if(in_array($client_id, $emp_client_id_ar))
				{
					if($emp_brief_id!='')
					{
						$emp_brief_id_ar = explode(',',$emp_brief_id);
    //print_r($emp_brief_id_ar);
						if(in_array($brief_id, $emp_brief_id_ar))
						{
							if (($key = array_search($brief_id, $emp_brief_id_ar)) !== false) {
								unset($emp_brief_id_ar[$key]);
							}

						}
						else
						{
          //array_push($emp_brief_id_ar,$brief_id);
						}
						$updated_emp_brief_id=implode(",",$emp_brief_id_ar);
						$set_con1 = array('brief_id' => $updated_emp_brief_id);
						$where_con1= array('user_id'=>$emp_id);
						$this->db->where($where_con1);
						$report = $this->db->update('wc_users', $set_con1);
					}
					else
					{
						$set_con1 = array('brief_id' => $brief_id);
						$where_con1= array('user_id'=>$emp_id);
						$this->db->where($where_con1);
						$report = $this->db->update('wc_users', $set_con1);     
					}
				}
			}                   
			$i++;
		}  
	}

	else
	{
		$user_details=$this->db->query("select * from wc_users where user_id='$emp_id'")->result_array();
		$emp_brief_id = $user_details[0]['brief_id'];
		if($emp_brief_id!='')
		{
			$emp_brief_id_ar = explode(',',$emp_brief_id);
    //print_r($emp_brief_id_ar);
			if(in_array($brief_id, $emp_brief_id_ar))
			{
				if (($key = array_search($brief_id, $emp_brief_id_ar)) !== false) {
					unset($emp_brief_id_ar[$key]);
				}

			}
			else
			{
				array_push($emp_brief_id_ar,$brief_id);
			}
			$updated_emp_brief_id=implode(",",$emp_brief_id_ar);
			$set_con1 = array('brief_id' => $updated_emp_brief_id);
			$where_con1= array('user_id'=>$emp_id);
			$this->db->where($where_con1);
			$report = $this->db->update('wc_users', $set_con1);
		}
		else
		{
			$set_con1 = array('brief_id' => $brief_id);
			$where_con1= array('user_id'=>$emp_id);
			$this->db->where($where_con1);
			$report = $this->db->update('wc_users', $set_con1);     
		}
   // echo $emp_brief_id = $user_details[0]['user_name'];
      /*if($report)
      {
          echo 'updated';
      }
      else
      {
          echo 'error';
       }*/

    }


    


//////////////////////////////////Send Mail Start///////////////////////////////////////////////

    $brief_id=$this->input->post('brief_id');
	$emp_id=$this->input->post('emp_id');
	$client_id=$this->input->post('client_id');
     
if($emp_id!='unall')
{



 

     /*********************************Send mail*************************************/

     $query=$this->db->query("select * from  wc_settings  WHERE setting_key='MAIL_SMTP_USER' ");
     $userDetails= $query->result_array();
     $MAIL_SMTP_USER=$userDetails[0]['setting_value'];

     $query=$this->db->query("select * from  wc_settings  WHERE setting_key='MAIL_SMTP_PASSWORD' ");
     $userDetails= $query->result_array();
     $MAIL_SMTP_PASSWORD=$userDetails[0]['setting_value'];

     $query=$this->db->query("select * from  wc_settings  WHERE setting_key='MAIL_BODY_ASSIGNED_BRIEF' ");
     $userDetails= $query->result_array();
     $MAIL_BODY=$userDetails[0]['setting_value'];

     $query=$this->db->query("select * from  wc_settings  WHERE setting_key='MAIL_FROM_TEXT' ");
     $userDetails= $query->result_array();
     $MAIL_FROM_TEXT=$userDetails[0]['setting_value'];

     $query=$this->db->query("select * from  wc_settings  WHERE setting_key='MAIL_FROM' ");
     $userDetails= $query->result_array();
     $MAIL_FROM=$userDetails[0]['setting_value'];



     $checkquerys = $this->db->query("select * from wc_brief where  brief_id='$brief_id' ")->result_array();
     $brief_id=$checkquerys[0]['brief_id'];
         //$rejected_reasons=$checkquerys[0]['rejected_reasons'];
     $added_by_user_id=$checkquerys[0]['added_by_user_id'];
     $brief_title=$checkquerys[0]['brief_title'];





     $checkquerys = $this->db->query("select * from wc_users where  user_id='$added_by_user_id' ")->result_array();
     $brand_manager_email=$checkquerys[0]['user_email'];
     $brand_manager_name=$checkquerys[0]['user_name'];
     $client_id=$checkquerys[0]['client_id'];

     $subject=$MAIL_FROM_TEXT.' - Brief Assigned(#'.$brief_id.')';

	 $to_email_all_cr=array();

    
	if($emp_id=='all')
		{
				//echo "select * from wc_users where client_id like '%,".$client_id.",%'and user_type_id='6' ";
				$checkquerys_mail = $this->db->query("select * from wc_users where client_id like '%,".$client_id.",%' and user_type_id='6' ")->result_array();

				//echo "<pre>";
				//print_r($checkquerys_mail);



				if(!empty($checkquerys_mail))
				{
				foreach($checkquerys_mail as $key => $value)
				{

				$to_email_all_cr[]=$value['user_email'];

				}

				}

		}
		else
		{
			  $user_details=$this->db->query("select * from wc_users where user_id='$emp_id'")->result_array();
		      $brief_id_arr=explode(",",$user_details[0]['brief_id']);
		      $user_email=$user_details[0]['user_email'];
		      if(in_array($brief_id, $brief_id_arr))
		      {
		      	$to_email_all_cr[]=$user_details[0]['user_email'];
		      }
		      



		}

	 //echo "<pre>";
     //print_r($to_email_all_cr);

     

if(!empty($to_email_all_cr))
{


             //Load email library
     $this->load->library('email');

     $config = array();
             //$config['protocol'] = 'smtp';
     $config['protocol']     = 'mail';
     $config['smtp_host'] = 'localhost';
     $config['smtp_user'] = $MAIL_SMTP_USER;
     $config['smtp_pass'] = $MAIL_SMTP_PASSWORD;
     $config['smtp_port'] = 25;




     $this->email->initialize($config);
     $this->email->set_newline("\r\n");





     $website_url=base_url();
     $dashboard_url=base_url()."dashboard";


     //$from_email = "demo.intexom@gmail.com";
     $from_email = $MAIL_FROM;
     $to_email = $to_email_all_cr;

     

     $date_time=date("d/m/y");
     
     $copy_right=$MAIL_FROM_TEXT;
     $project_name="#".$brief_id." - ".$brief_title;
            //echo $message=$MAIL_BODY;
     $find_arr = array("##WEBSITE_URL##","##PROJECT_NAME##","##DASHBOARD_LINK##","##COPY_RIGHT##");
     $replace_arr = array($website_url,$project_name,$dashboard_url,$copy_right);
     $message=str_replace($find_arr,$replace_arr,$MAIL_BODY);

            //echo print_r($to_email)."---".$from_email."----".$message;exit;



     $this->email->from($from_email, $MAIL_FROM_TEXT);
     $this->email->to($to_email);
     if($_SERVER['HTTP_HOST']!='collab.piquic.com')
     {
     	$this->email->cc(array('poulami@mokshaproductions.in','pranav@mokshaproductions.in','raja.priya@mokshaproductions.in','rajendra.prasad@mokshaproductions.in'));
     }

            //$this->email->bcc('$client_email');
             //$this->email->bcc('pranav@mokshaproductions.in');
     $this->email->subject($subject);
     $this->email->message($message);
             //Send mail
     if($this->email->send())
     {

                 //echo "mail sent";exit;
     	$this->session->set_flashdata("email_sent","Congragulation Email Send Successfully.");
     }

     else
     {
                 //echo $this->email->print_debugger();
                 //echo "mail not sent";exit;
     	$this->session->set_flashdata("email_sent","You have encountered an error");
     }

   }


       


 }


 /////////////////////////////////////Send Mail End//////////////////////////////////////////




}

public function change_worktype()
{
   $brief_id=$this->input->post('brief_id');
   $work_type=$this->input->post('status');

        $set_con1 = array('work_type' => $work_type);
        $where_con1= array('brief_id'=>$brief_id);
        $this->db->where($where_con1);
        $report = $this->db->update('wc_brief', $set_con1);


}



    

 
 public function updateuser_brief_old()
 {
 	$brief_id=$this->input->post('brief_id');
 	$emp_id=$this->input->post('emp_id');
 	$user_details=$this->db->query("select * from wc_users where user_id='$emp_id'")->result_array();
 	$emp_brief_id = $user_details[0]['brief_id'];
 	if($emp_brief_id!='')
 	{
 		$emp_brief_id_ar = explode(',',$emp_brief_id);
    //print_r($emp_brief_id_ar);
 		if(in_array($brief_id, $emp_brief_id_ar))
 		{
 			if (($key = array_search($brief_id, $emp_brief_id_ar)) !== false) {
 				unset($emp_brief_id_ar[$key]);
 			}

 		}
 		else
 		{
 			array_push($emp_brief_id_ar,$brief_id);
 		}
 		$updated_emp_brief_id=implode(",",$emp_brief_id_ar);
 		$set_con1 = array('brief_id' => $updated_emp_brief_id);
 		$where_con1= array('user_id'=>$emp_id);
 		$this->db->where($where_con1);
 		$report = $this->db->update('wc_users', $set_con1);
 	}
 	else
 	{
 		$set_con1 = array('brief_id' => $brief_id);
 		$where_con1= array('user_id'=>$emp_id);
 		$this->db->where($where_con1);
 		$report = $this->db->update('wc_users', $set_con1);     
 	}
 	echo $emp_brief_id = $user_details[0]['user_name'];
      /*if($report)
      {
          echo 'updated';
      }
      else
      {
          echo 'error';
       }*/

    }

    public function sortemp()
    {
    	$brief_id=$this->input->post('brief_id');
    	$key=$this->input->post('key');
    	$session_data = $this->session->userdata('front_logged_in');
    	$user_id= $session_data['user_id'];
    	$checkquerys = $this->db->query("select * from wc_users where  user_id='$user_id' ")->result_array();
    	$client_id = $checkquerys[0]['client_id'];

    	$user_details=$this->db->query("select * from wc_users where user_type_id=6 and user_name like '%$key%'")->result_array();     
    	$i=0;
    	$data='';
    	foreach ($user_details as $row) {

    		$emp_user_id = $user_details[$i]['user_id'];
    		$emp_brief_id = $user_details[$i]['brief_id'];
    		$emp_client_id = $user_details[$i]['client_id'];
    		if($emp_client_id!='0')
    		{
    			$emp_client_id_ar = explode(',', $emp_client_id);
    			if (in_array($client_id, $emp_client_id_ar))
    			{
    				$emp_name = $user_details[$i]['user_name'];
    				$initials='';
    				foreach (explode(' ', $emp_name) as $word)
    				{
                  // $initials .= strtoupper($word[0]);
                  
    					if(strpos($emp_name, ' ') !== false) {
    						$initials .= strtoupper($word[0]);
    					} else {
    						$initials .= strtoupper($word[0] . $word[1]);
    					}
    				}


    				if($emp_brief_id!='')
    				{
    					$emp_brief_id_ar = explode(',', $emp_brief_id);
    				}
    				else
    				{
    					$emp_brief_id_ar =array();
    				}
    				$allaoted='';
    				if (in_array($brief_id, $emp_brief_id_ar))
    				{
    					$allaoted=1;                                            
    				}
    				if($allaoted==1){ $sty="text-wc"; $checked='checked'; } else { $sty="text-white"; $checked=''; }
    				$data.='<input class="d-none" type="checkbox" id="chk_'.$emp_user_id.'" value="'.$emp_user_id.'"'.$checked.'>
    				<label class="w-100" for="chk_'.$emp_user_id.'">
    				<div id="usr_'.$emp_user_id.'" class="d-flex justify-content-between" onclick="updateuser_brief('.$emp_user_id.','.$brief_id.')">
    				<div class="py-2"><span class="mbr-ico">'.$initials.'</span>&nbsp;<span>'.$emp_name.'</span></div>
    				<div class="pt-2"><i class="fas fa-check '.$sty.'"></i></div>
    				</div>
    				</label>';
    			} }
    			$i++; }

    			echo $data;


    		}


public function price_cal(){
    $session_data = $this->session->userdata('front_logged_in');
    $user_id= $session_data['user_id'];
    $user_type_id= $session_data['user_type_id'];
    $user_name= $session_data['user_name'];


    $checkquerys_1= $this->db->query("select * from wc_users where user_id='$user_id'")->result_array();
    $add_discount=$checkquerys_1[0]['add_discount'];
    $add_extra_charges=$checkquerys_1[0]['add_extra_charges'];


    $brief_id=preg_replace('/[^a-zA-Z0-9_ -]/s','',$this->input->post('brief_id'));
    $product_id=preg_replace('/[^a-zA-Z0-9_ -]/s','',$this->input->post('product_id'));
    $txtQty=preg_replace('/[^a-zA-Z0-9_ -]/s','',$this->input->post('txtQty'));
    if(!empty($this->input->post('update_quty'))){
        $update_quty=$this->input->post('update_quty');
    }
    else{
        $update_quty="";
    }
    if($product_id!='discount' && $product_id!='extra_charges'){

        //echo "update_quty=====". $update_quty;
        $checkquerys = $this->db->query("select * from wc_product_price where product_id='$product_id' ")->result_array();
        $product_name=$checkquerys[0]['product_name'];
        $product_price=$checkquerys[0]['product_price'];
        $subtotal_price=trim($txtQty*$product_price);
        
        if(empty($update_quty)){
            $htmt="";
            $html=" <tr id='".$brief_id."_".$product_id."'>
            <td><div class='small' style='width: 90%; white-space:nowrap; overflow:hidden; text-overflow: ellipsis;'>".$product_name."</div><input type='hidden' id='product_name".$brief_id."' name='product_name".$brief_id."[]' value='".$product_name."' />
            <input type='hidden' id='product_id".$brief_id."' name='product_id".$brief_id."[]' value='".$product_id."' />
            </td>
            <td class='small text-right'>".$txtQty." <input type='hidden' id='total_qty".$brief_id."' name='total_qty".$brief_id."[]' value='".$txtQty."' step='any' min='1'/></td>
            <td class='small text-right'>Rs.".$subtotal_price." <input type='hidden' id='subtotal_price".$brief_id."' name='subtotal_price".$brief_id."[]' value='".$subtotal_price."' readonly/></td>
            <td class='small'><i class='far fa-times-circle' onclick='price_cancel(\"".$brief_id."\",\"".$product_id."\",\"".$add_discount."\",\"".$add_extra_charges ."\")'></i></td>
            </tr>"; 
            echo $html;
        }
        else if(!empty($update_quty)=="update_quty"){
            echo $subtotal_price;
        }
    }
    else if($product_id=='discount'){
         $htmt="";
            $html=" <tr id='".$brief_id."_".$product_id."'>
            <td>
                <div class='small' style='width: 90%; white-space:nowrap; overflow:hidden; text-overflow: ellipsis;'>Discount</div>
            </td>
            <td class='small text-right'>1</td>
            <td class='small text-right'>Rs.".$txtQty." <input type='hidden' id='discount".$brief_id."' name='discount".$brief_id."[]' value='".$txtQty."' readonly/></td>
            <td class='small'><i class='far fa-times-circle' onclick='price_cancel(\"".$brief_id."\",\"".$product_id."\",\"".$add_discount."\",\"".$add_extra_charges ."\")'></i></td>
            </tr>"; 
            echo $html;
    }
    else if($product_id=='extra_charges'){
         $htmt="";
            $html=" <tr id='".$brief_id."_".$product_id."'>
            <td>
                <div class='small' style='width: 90%; white-space:nowrap; overflow:hidden; text-overflow: ellipsis;'>Extra Charges</div>
            </td>
            <td class='small text-right'>1</td>
            <td class='small text-right'>Rs.".$txtQty." <input type='hidden' id='extra_charges".$brief_id."' name='extra_charges".$brief_id."[]' value='".$txtQty."' readonly/></td>
            <td class='small'><i class='far fa-times-circle' onclick='price_cancel(\"".$brief_id."\",\"".$product_id."\",\"".$add_discount."\",\"".$add_extra_charges ."\")'></i></td>
            </tr>"; 
            echo $html;
    }
}
public function view_product_add(){

    $brief_id=preg_replace('/[^a-zA-Z0-9_ -]/s','',$this->input->post('brief_id'));
    $product_id=preg_replace('/[^a-zA-Z0-9_ -]/s','',$this->input->post('product_id'));
    $product_quty=preg_replace('/[^a-zA-Z0-9_ -]/s','',$this->input->post('txtQty'));
    $txtDscnt=preg_replace('/[^a-zA-Z0-9_ -]/s','',$this->input->post('txtDscnt'));
    $txtEC=preg_replace('/[^a-zA-Z0-9_ -]/s','',$this->input->post('txtEC'));

    $session_data = $this->session->userdata('front_logged_in');
    $user_id= $session_data['user_id'];
    $user_type_id= $session_data['user_type_id'];
    $user_name= $session_data['user_name'];


    $checkquerys_1= $this->db->query("select * from wc_users where user_id='$user_id'")->result_array();
    $add_discount=$checkquerys_1[0]['add_discount'];
    $add_extra_charges=$checkquerys_1[0]['add_extra_charges'];

    if($product_id!='discount' && $product_id!='extra_charges'){
        //echo "update_quty=====". $update_quty;
        $checkquerys = $this->db->query("select * from wc_product_price where product_id='$product_id' ")->result_array();
        $product_name=$checkquerys[0]['product_name'];
        $product_price=$checkquerys[0]['product_price'];
        $subtotal_price=trim($product_quty*$product_price);
        
       
        $sql = "INSERT INTO wc_brief_product_list(`brief_id`,`product_id`,`product_quty`,`subtotal_price`)VALUES('$brief_id','$product_id','$product_quty','$subtotal_price')";
        $qry =$this->db->query($sql);

        $brief_product_id=$this->db->insert_id();

       
        $total_price=0;
        $wc_brief_product_result1 = $this->db->query("select * from wc_brief_product_list where brief_id='$brief_id' ")->result_array();
        foreach($wc_brief_product_result1 as $key => $value){ 
            $total_price+=$value['subtotal_price'];
        }
        $set_con1 = array('total_price' => $total_price,'add_discount'=>$txtDscnt,'add_extra_charges'=>$txtEC);
        $where_con1= array('brief_id'=>$brief_id);
        $this->db->where($where_con1);
        $report = $this->db->update('wc_brief', $set_con1);


            $htmt="";
            $html=" <tr id='view_".$brief_id."_".$brief_product_id."'>
            <td>
            <div class='small ' style='width: 90%; white-space:nowrap; overflow:hidden; text-overflow: ellipsis;'>".$product_name."</div>
            <input type='hidden' id='product_name".$brief_product_id."' name='product_name".$brief_product_id."' value='".$product_name."' />
            <input type='hidden' id='product_id".$brief_product_id."' name='product_id".$brief_product_id."' value='".$product_id."' />
            </td>
            <td class='small text-right'>
            <div class='small text-right' id='div_product_quty".$brief_product_id ."'>".$product_quty."</div>
             <div class='d-flex justify-content-between'>
             <input class='form-control d-none ' type='text' id='product_qty".$brief_product_id."' name='product_qty".$brief_product_id."' value='".$product_quty."' step='any' min='1' style='width: 4rem;' onkeyup='update_price_cal(\"".$brief_id."\",\"".$brief_product_id ."\",\"".$add_discount."\",\"".$add_extra_charges ."\")' value='".$product_quty."'/>
              <span class='pl-1'>
               <i class='far fa-check-circle d-none' id='update_product".$brief_product_id ."' onclick='update_product(\"".$brief_id ."\",\"".$brief_product_id ."\",\"".$add_discount."\",\"".$add_extra_charges ."\")'></i>
                <i class='far fa-times-circle d-none' id='cancel_product".$brief_product_id ."' onclick='cancel_product(\"".$brief_product_id ."\",\"".$add_discount."\",\"".$add_extra_charges ."\")'></i>
            </span>
            </div>
             <span class='small text-danger' id='errmsg_product_qty_".$brief_product_id."'></span>

            <script type='text/javascript'>
            $('#product_qty".$brief_product_id ."').keypress(function(e) {

            if (((e.which != 46 || (e.which == 46 && $(this).val() == '')) ||$(this).val().indexOf('.') != 0) && (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57))) {
            $('#errmsg_product_qty_".$brief_product_id."').html('Digits Only').show().fadeOut(1500);
            return false;
            }
            }).on('paste', function(e) {
            $('#errmsg_product_qty_".$brief_product_id."').html('Type the value').show().fadeOut(1500);
            return false;

            });
            $('#product_qty".$brief_product_id ."').on('input', function() {
            if (/^0/.test(this.value)) {
            this.value = this.value.replace(/^0/, '');
            $('#errmsg_product_qty_".$brief_product_id."').html('Invalid').show().fadeOut(1500);
            }

            }); 
            </script> 
             </td>
            <td><div class='small text-right' id='div_subtotal_price_".$brief_product_id ."'> Rs.".$subtotal_price."</div>
            <input type='hidden' class='subtotal_price_".$brief_id."' id='subtotal_price_".$brief_product_id."' name='subtotal_price_".$brief_product_id."' value='".$subtotal_price."' />
            </td>
            ";
             if($user_type_id=='1'||$user_type_id=='2'||$user_type_id=='3'){ 
     $html.=" <td>
        <i class='far fa-edit' id='edit_product".$brief_product_id ."' onclick='price_edit(\"".$brief_product_id ."\")'></i>
        
        <i class='far fa-trash-alt' id='delete_product".$brief_product_id ."' onclick='price_delete(\"".$brief_product_id ."\")' data-toggle='modal' data-target='#modal_confirm_product".$brief_product_id."'></i>

        
        <div class='modal fade' id='modal_confirm_product".$brief_product_id ."' tabindex='-1' role='dialog' aria-hidden='true'>
          <div class='modal-dialog' role='document'>
            <div class='modal-content'>
              <div class='modal-header'>
                <h5 class='modal-title text-dark' id='mod_title_".$brief_id ."'>Are you sure, you want to delete ?</h5>
                <button type='button' class='close text-danger' data-dismiss='modal' aria-label='Close'>
                  <span aria-hidden='true' >&times;</span>
                </button>
              </div>
              <div class='modal-footer'>
                <button type='button' class='btn btn-secondary' data-dismiss='modal'>Close</button>
                <button type='button' id='hideDel' class='btn btn-danger' onclick='delete_product(\"".$brief_id ."\",\"".$brief_product_id ."\",\"".$add_discount."\",\"".$add_extra_charges ."\")' data-dismiss='modal'>Delete</button>
              </div>
            </div>
          </div>
        </div>

      </td>";
    }

         $html.="</tr>"; 

    }
    else if($product_id=='discount'){
        $total_price=0;
        $wc_brief_product_result1 = $this->db->query("select * from wc_brief_product_list where brief_id='$brief_id' ")->result_array();
        foreach($wc_brief_product_result1 as $key => $value){ 
            $total_price+=$value['subtotal_price'];
        }
        $set_con1 = array('total_price' => $total_price,'add_discount'=>$txtDscnt,'add_extra_charges'=>$txtEC);
        $where_con1= array('brief_id'=>$brief_id);
        $this->db->where($where_con1);
        $report = $this->db->update('wc_brief', $set_con1);

         $htmt="";
            $html=" <tr id='view_".$brief_id."_".$product_id."'>
            <td><div class='small' style='width: 90%; white-space:nowrap; overflow:hidden; text-overflow: ellipsis;'>Discount</div>
            </td>
            <td class='small text-right'>1</td>
            <td class='small text-right'>Rs.".$product_quty." <input type='hidden' id='discount".$brief_id."' name='discount".$brief_id."[]' value='".$product_quty."' readonly/></td>";
        if($user_type_id=='1'||$user_type_id=='2'||$user_type_id=='3'){ 
             $html.=" <td>
              
                <i class='far fa-trash-alt' id='delete_product_discount".$brief_id."' onclick='price_delete(\"discount\")' data-toggle='modal' data-target='#modal_confirm_product_discount".$brief_id."'></i>

                
                <div class='modal fade' id='modal_confirm_product_discount".$brief_id."' tabindex='-1' role='dialog' aria-hidden='true'>
                  <div class='modal-dialog' role='document'>
                    <div class='modal-content'>
                      <div class='modal-header'>
                        <h5 class='modal-title text-dark' id='mod_title_".$brief_id ."'>Are you sure, you want to delete ?</h5>
                        <button type='button' class='close text-danger' data-dismiss='modal' aria-label='Close'>
                          <span aria-hidden='true' >&times;</span>
                        </button>
                      </div>
                      <div class='modal-footer'>
                        <button type='button' class='btn btn-secondary' data-dismiss='modal'>Close</button>
                        <button type='button' id='hideDel' class='btn btn-danger' onclick='delete_product(\"".$brief_id ."\",\"discount\",\"".$add_discount."\",\"".$add_extra_charges ."\")' data-dismiss='modal'>Delete</button>
                      </div>
                    </div>
                  </div>
                </div>

              </td>";
            }
            
            $html.="</tr>"; 
           
    }
    else if($product_id=='extra_charges'){
        $total_price=0;
        $wc_brief_product_result1 = $this->db->query("select * from wc_brief_product_list where brief_id='$brief_id' ")->result_array();
        foreach($wc_brief_product_result1 as $key => $value){ 
            $total_price+=$value['subtotal_price'];
        }
        $set_con1 = array('total_price' => $total_price,'add_discount'=>$txtDscnt,'add_extra_charges'=>$txtEC);
        $where_con1= array('brief_id'=>$brief_id);
        $this->db->where($where_con1);
        $report = $this->db->update('wc_brief', $set_con1);
         $htmt="";
            $html=" <tr id='view_".$brief_id."_".$product_id."'>
            <td>
                <div class='small' style='width: 90%; white-space:nowrap; overflow:hidden; text-overflow: ellipsis;'>Extra Charges</div>
            </td>
            <td class='small text-right'>1</td>
            <td class='small text-right'>Rs.".$product_quty." <input type='hidden' id='extra_charges".$brief_id."' name='extra_charges".$brief_id."[]' value='".$product_quty."' readonly/></td>";
            if($user_type_id=='1'||$user_type_id=='2'||$user_type_id=='3'){ 
             $html.=" <td>
              
                <i class='far fa-trash-alt' id='delete_product_extra_charges".$brief_id."' onclick='price_delete(\"extra_charges\")' data-toggle='modal' data-target='#modal_confirm_product_extra_charges".$brief_id."'></i>

                
                <div class='modal fade' id='modal_confirm_product_extra_charges".$brief_id."' tabindex='-1' role='dialog' aria-hidden='true'>
                  <div class='modal-dialog' role='document'>
                    <div class='modal-content'>
                      <div class='modal-header'>
                        <h5 class='modal-title text-dark' id='mod_title_".$brief_id ."'>Are you sure, you want to delete ?</h5>
                        <button type='button' class='close text-danger' data-dismiss='modal' aria-label='Close'>
                          <span aria-hidden='true' >&times;</span>
                        </button>
                      </div>
                      <div class='modal-footer'>
                        <button type='button' class='btn btn-secondary' data-dismiss='modal'>Close</button>
                        <button type='button' id='hideDel' class='btn btn-danger' onclick='delete_product(\"".$brief_id ."\",\"extra_charges\",\"".$add_discount."\",\"".$add_extra_charges ."\")' data-dismiss='modal'>Delete</button>
                      </div>
                    </div>
                  </div>
                </div>

              </td>";
              
            }
          
           $html.="</tr>"; 
    }
 

        echo $html;
    
   

}

public function update_product_list(){
    $brief_id=preg_replace('/[^a-zA-Z0-9_ -]/s','',$this->input->post('brief_id'));
    $brief_product_id=preg_replace('/[^a-zA-Z0-9_ -]/s','',$this->input->post('brief_product_id'));
    $product_qty=preg_replace('/[^a-zA-Z0-9_ -]/s','',$this->input->post('product_qty'));
    $subtotal_price=preg_replace('/[^a-zA-Z0-9_ -]/s','',$this->input->post('subtotal_price'));
    $total_price=preg_replace('/[^a-zA-Z0-9_ -]/s','',$this->input->post('total_price'));
    $txtDscnt=preg_replace('/[^a-zA-Z0-9_ -]/s','',$this->input->post('txtDscnt'));
    $txtEC=preg_replace('/[^a-zA-Z0-9_ -]/s','',$this->input->post('txtEC'));

    $set_con = array('product_quty' => $product_qty, 'subtotal_price' => $subtotal_price);
    $where_con= array('brief_product_id'=>$brief_product_id);
    $this->db->where($where_con);
    $report = $this->db->update('wc_brief_product_list', $set_con);

    $set_con1 = array('total_price' => $total_price,'add_discount'=>$txtDscnt,'add_extra_charges'=>$txtEC);
    $where_con1= array('brief_id'=>$brief_id);

    $this->db->where($where_con1);
    $report = $this->db->update('wc_brief', $set_con1);

    echo $report;
}
public function delete_product_list()
{
    $brief_id=preg_replace('/[^a-zA-Z0-9_ -]/s','',$this->input->post('brief_id'));
    $brief_product_id=preg_replace('/[^a-zA-Z0-9_ -]/s','',$this->input->post('brief_product_id'));

    $txtDscnt=preg_replace('/[^a-zA-Z0-9_ -]/s','',$this->input->post('txtDscnt'));
    $txtEC=preg_replace('/[^a-zA-Z0-9_ -]/s','',$this->input->post('txtEC'));

    $array = array('brief_id' => $brief_id,'brief_product_id'=>$brief_product_id);
    $this->db->where($array); 
    $result=$this->db->delete('wc_brief_product_list');
    $total_price=0;
    $wc_brief_product_result1 = $this->db->query("select * from wc_brief_product_list where brief_id='$brief_id' ")->result_array();
    foreach($wc_brief_product_result1 as $key => $value){ 
        $total_price+=$value['subtotal_price'];
    }

    $set_con1 = array('total_price' => $total_price,'add_discount'=>$txtDscnt,'add_extra_charges'=>$txtEC);
    $where_con1= array('brief_id'=>$brief_id);

    $this->db->where($where_con1);
    $report = $this->db->update('wc_brief', $set_con1);
    echo $total_price;

}    					public function delete_card()
    					{
    						$brief_id=$this->input->post('brief_id');
    						$brief_id=preg_replace('/[^a-zA-Z0-9_ -]/s','',$brief_id);
    						$checkquerys = $this->db->query("select * from wc_brief where brief_id='$brief_id' ")->result_array();
    						$added_by_user_id=$checkquerys[0]['added_by_user_id'];
    						$brand_id=$checkquerys[0]['brand_id'];
    						$brief_title=$checkquerys[0]['brief_title'];
    						$brief_name=preg_replace('/[^a-zA-Z0-9_.]/', '_',$brief_title);

    /*$checkquerys = $this->db->query("select wc_brands.*,wc_clients.client_name from wc_users
        inner join wc_brands on wc_users.brand_id=wc_brands.brand_id
        inner join wc_clients on wc_brands.client_id=wc_clients.client_id
        where wc_users.user_id='$added_by_user_id' ")->result_array();*/



        $checkquerys = $this->db->query("select wc_brands.*,wc_clients.client_name from wc_brands
        	inner join wc_clients on wc_brands.client_id=wc_clients.client_id
        	where wc_brands.brand_id='$brand_id' ")->result_array();

        $brand_id=$checkquerys[0]['brand_id'];
        $brand_name=preg_replace('/[^a-zA-Z0-9_.]/', '_',$checkquerys[0]['brand_name']);
        $client_id=$checkquerys[0]['client_id'];
        $client_name=preg_replace('/[^a-zA-Z0-9_.]/', '_',$checkquerys[0]['client_name']);

        $dir = "upload/".$client_id."-".$client_name."/".$brand_id."-".$brand_name."/".$brief_id."-".$brief_name;
        if(is_dir($dir))
        {
        	$this->deleteAll($dir);

        	$array = array('brief_id' => $brief_id);
        	$this->db->where($array); 
        	$result=$this->db->delete('wc_brief_records');
        	$array = array('brief_id' => $brief_id);
        	$this->db->where($array); 
        	$result=$this->db->delete('wc_brief');
        }
        else{
        	echo "Directory is not available";
        }
     }

     public function deleteAll($str) {
    //It it's a file.
     	if (is_file($str)) {
        //Attempt to delete it.
     		return unlink($str);
     	}
    //If it's a directory.
     	elseif (is_dir($str)) {
        //Get a list of the files in this directory.
     		$scan = glob(rtrim($str,'/').'/*');
        //Loop through the list of files.
     		foreach($scan as $index=>$path) {
            //Call our recursive function.
     			$this-> deleteAll($path);
     		}
        //Remove the directory itself.
     		return @rmdir($str);
     	}
     }

     public function getbir()
     {
//$brand_id=$_REQUEST['brand_id'];
     	$brand_id = preg_replace('/[^a-zA-Z0-9_ -]/s','',$this->input->post('brand_id'));
     	$account_id = preg_replace('/[^a-zA-Z0-9_ -]/s','',$this->input->post('account_id'));
     	$search_key =preg_replace('/[^a-zA-Z0-9_ -]/s','',$this->input->post('search_key'));
     	echo $data['briefinreview']= $this->Dashboard_model->getbriefinreview($account_id,$brand_id,$search_key);
     }
     public function getbr()
     {
     	$brand_id = preg_replace('/[^a-zA-Z0-9_ -]/s','',$this->input->post('brand_id'));
     	$account_id =preg_replace('/[^a-zA-Z0-9_ -]/s','',$this->input->post('account_id'));
     	$search_key = preg_replace('/[^a-zA-Z0-9_ -]/s','',$this->input->post('search_key'));
     	echo $data['briefinreview']= $this->Dashboard_model->getbriefrejected($account_id,$brand_id,$search_key);
     }
     public function getwip()
     {
     	$brand_id = preg_replace('/[^a-zA-Z0-9_ -]/s','',$this->input->post('brand_id'));
     	$account_id =preg_replace('/[^a-zA-Z0-9_ -]/s','',$this->input->post('account_id'));
     	$search_key =preg_replace('/[^a-zA-Z0-9_ -]/s','',$this->input->post('search_key'));
     	echo $data['briefinreview']= $this->Dashboard_model->getworkinprogress($account_id,$brand_id,$search_key);
     }



     public function getpp()
     {
     	$brand_id = preg_replace('/[^a-zA-Z0-9_ -]/s','',$this->input->post('brand_id'));
     	$account_id = preg_replace('/[^a-zA-Z0-9_ -]/s','',$this->input->post('account_id'));
     	$search_key = preg_replace('/[^a-zA-Z0-9_ -]/s','',$this->input->post('search_key'));
     	echo $data['briefinreview']= $this->Dashboard_model->getproffing_pending($account_id,$brand_id,$search_key);
     }


     public function getrw()
     {
     	$brand_id = preg_replace('/[^a-zA-Z0-9_ -]/s','',$this->input->post('brand_id'));
     	$account_id = preg_replace('/[^a-zA-Z0-9_ -]/s','',$this->input->post('account_id'));
     	$search_key = preg_replace('/[^a-zA-Z0-9_ -]/s','',$this->input->post('search_key'));
     	echo $data['briefinreview']= $this->Dashboard_model->getrevision_work($account_id,$brand_id,$search_key);
     }

     public function getwc()
     {
     	$brand_id = preg_replace('/[^a-zA-Z0-9_ -]/s','',$this->input->post('brand_id'));
     	$account_id = preg_replace('/[^a-zA-Z0-9_ -]/s','',$this->input->post('account_id'));
     	$search_key = preg_replace('/[^a-zA-Z0-9_ -]/s','',$this->input->post('search_key'));
     	echo $data['briefinreview']= $this->Dashboard_model->getwork_complete($account_id,$brand_id,$search_key);
     }

     public function getfp()
     {
     	$brand_id = preg_replace('/[^a-zA-Z0-9_ -]/s','',$this->input->post('brand_id'));
     	$account_id = preg_replace('/[^a-zA-Z0-9_ -]/s','',$this->input->post('account_id'));
     	$search_key = preg_replace('/[^a-zA-Z0-9_ -]/s','',$this->input->post('search_key'));
     	echo $data['briefinreview']= $this->Dashboard_model->getfeedback_pending($account_id,$brand_id,$search_key);
     }

     public function getfc()
     {
     	$brand_id = preg_replace('/[^a-zA-Z0-9_ -]/s','',$this->input->post('brand_id'));
     	$account_id = preg_replace('/[^a-zA-Z0-9_ -]/s','',$this->input->post('account_id'));
     	$search_key = preg_replace('/[^a-zA-Z0-9_ -]/s','',$this->input->post('search_key'));
     	echo $data['briefinreview']= $this->Dashboard_model->getfeedback_completed($account_id,$brand_id,$search_key);
     }





     public function getbriefsort()
     {	

     	if ($this->session->userdata('front_logged_in')) {
     		$session_data = $this->session->userdata('front_logged_in');
     		$data['user_id'] = $session_data['user_id'];
     		$data['user_type_id'] = $session_data['user_type_id'];
     		$data['user_name'] = $session_data['user_name'];
     		$type1 = $session_data['user_type_id'];

     		$session_data = $this->session->userdata('front_logged_in');
     		$data['user_id'] = $session_data['user_id'];
     		$data['user_type_id'] = $session_data['user_type_id'];
     		$data['user_name'] = $session_data['user_name'];
     		$type1 = $session_data['user_type_id'];
     		$brand_access=$session_data['brand_access'];
     		$user_id = $session_data['user_id'];
     		$checkquerys = $this->db->query("select brief_id,brand_id from wc_users where user_id='$user_id' ")->result_array();
			//$brief_access=$checkquerys[0]['brief_id'];
			$brand_access=$checkquerys[0]['brand_id'];
     		$lastID=preg_replace('/[^a-zA-Z0-9_ -]/s','',$this->input->post('lastID'));    
     		$rowperpage = 12;
     		//if(isset($this->input->post('myCheckboxes'))) {
     		$brief_status = preg_replace('/[^a-zA-Z0-9_ -]/s','',$this->input->post('status_filter'));
     		$brief_status =str_replace(',','-',$brief_status);

			//}	
     		$brand_id=preg_replace('/[^a-zA-Z0-9_ -]/s','',$this->input->post('brand_id'));
     		$brand_id =str_replace(',','-',$brand_id);
     		$account_id=preg_replace('/[^a-zA-Z0-9_ -]/s','',$this->input->post('account_id'));
     		$account_id =str_replace(',','-',$account_id);
     		$search_key=preg_replace('/[^a-zA-Z0-9_ -]/s','',$this->input->post('search_key'));
     		$allcount = $this->Dashboard_model->getbriefCount1($brief_status,$brand_id,$account_id,$search_key,$lastID);
     		$data['showLimit']=$rowperpage;
     		$data['allNumRows']=$allcount;
    		//$briefinfo=$this->Dashboard_model->briefListDetails1($brief_status,$brand_id);
     		$data['dashboradinfo'] =$this->Dashboard_model->briefListDetails1($brief_status,$brand_id,$account_id,$search_key,$lastID,$rowperpage);	
     		$this->load->view('front/dashboard_search',$data); 

     	}
     	else
     	{
     		echo "sessionout";
     	}   	
     }








     function getBrandDropDown()
     {

     	$session_data = $this->session->userdata('front_logged_in');
     	$data['user_id'] = $session_data['user_id'];
     	$data['user_type_id'] = $session_data['user_type_id'];
     	$data['user_type_id'] = $session_data['user_type_id'];
     	$data['user_name'] = $session_data['user_name'];
     	$user_name = $session_data['user_name'];
     	$user_id=$data['user_id'];
     	$user_type_id = $session_data['user_type_id'];



     	$account_id = $this->input->post('account_id');
/*$brand_id_sel= $this->input->post('brand_id_sel');
//echo "select * from wc_users where  user_id='$account_id' ";
if($account_id!='')
{
    $checkquerys = $this->db->query("select * from wc_users where  user_id='$account_id' ")->result_array();
    $brand_id_str=trim($checkquerys[0]['brand_id'],",");
   // $user_type_id=$checkquerys[0]['user_type_id'];

}
else
{
    $brand_id_str='';
 }*/


// echo $brand_id_sel;exit;
 $sql="select * from wc_brands where status='Active' ";

 if($account_id!='')
 {
 	$sql.=" and  client_id='".$account_id."' " ;
 }
// echo $sql;

 ?>
 <select class="custom-select" name="brand_id" id="brand_id" onchange="sortbrief()">
 	<option value="">Select Brand</option>
 	<?php





 	$brand_type_details=$this->db->query($sql)->result_array();
 	if(!empty($brand_type_details))
 	{
 		foreach($brand_type_details as $key => $brandstypedetails)
 		{
 			$brand_id=$brandstypedetails['brand_id'];
 			$brand_name=$brandstypedetails['brand_name'];

 			?>
 			<option value="<?php echo $brand_id;?>"  <?php if($user_type_id=='4') { echo 'selected'; } ?>    ><?php echo ucfirst($brand_name);?></option> 
 			<!-- <option value="<?php echo $brand_id;?>"        ><?php echo ucfirst($brand_name);?></option> -->
 			<?php

 		}
 	}
 	?>

 </select>
 <?php









}



function zipFolder($temp_folder) {  

 // Get real path for our folder
	$rootPath ="temp_brief_upload/".$temp_folder;

    // Initialize archive object
	$tot_file_name="temp_brief_upload/".$temp_folder."/".$temp_folder.'.zip';
	$zip = new ZipArchive();
	$zip->open($tot_file_name, ZipArchive::CREATE | ZipArchive::OVERWRITE);

    // Create recursive directory iterator

	$files = new RecursiveIteratorIterator(
		new RecursiveDirectoryIterator($rootPath),
		RecursiveIteratorIterator::LEAVES_ONLY
	);

	foreach ($files as $name => $file)
	{
    // Skip directories (they would be added automatically)
		if (!$file->isDir())
		{
    // Get real and relative path for current file
			$filePath = $file->getRealPath();
    //$filePath = "temp_brief_upload/".$temp_folder;
			$relativePath = basename(substr($filePath, strlen($rootPath)));
    //echo $relativePath = pathinfo($rootPath);

    //echo $relativePath."<br>" ;

    // Add current file to archive
			$zip->addFile($filePath, $relativePath);
		}
	}

    // Zip archive will be created only after closing object
	$zip->close();


}


public function removeFolder( $dir )
{
	if( is_dir( $dir ) )
	{
    $files = glob( $dir . '*', GLOB_MARK ); //GLOB_MARK adds a slash to directories returned

    foreach( $files as $file )
    {
    	$this->delete_directory( $file );      
    }

    @rmdir( $dir );
 } 
 elseif( is_file( $dir ) ) 
 {
 	unlink( $dir );  
 }
}
public function delete_directory( $dir )
{
	if( is_dir( $dir ) )
	{
    $files = glob( $dir . '*', GLOB_MARK ); //GLOB_MARK adds a slash to directories returned

    foreach( $files as $file )
    {
    	$this->delete_directory( $file );      
    }

    @rmdir( $dir );
 } 
 elseif( is_file( $dir ) ) 
 {
 	unlink( $dir );  
 }
}


public function getTempFolder(){

	$session_data = $this->session->userdata('front_logged_in');
	$data['user_id'] = $session_data['user_id'];
	$data['user_type_id'] = $session_data['user_type_id'];
	$data['user_type_id'] = $session_data['user_type_id'];
	$data['user_name'] = $session_data['user_name'];
	$user_name = $session_data['user_name'];
	$user_id=$data['user_id'];
	$user_type_id = $session_data['user_type_id'];

	$folder=$user_id.time();
	echo $folder;
}



public function upload_file(){

	$brief_id = $this->input->post('brief_id');
	$temp_folder = $this->input->post('folder');
	$check = $this->input->post('check');
    //$file=$_FILES["doc_file"]["name"];

    $fileName = $_FILES["doc_file"]["name"]; // The file name
    $fileTmpLoc = $_FILES["doc_file"]["tmp_name"]; // File in the PHP tmp folder
    $fileType = $_FILES["doc_file"]["type"]; // The type of file it is
    $fileSize = $_FILES["doc_file"]["size"]; // File size in bytes
    $fileErrorMsg = $_FILES["doc_file"]["error"]; // 0 for false... and 1 for true



    $folder_path="temp_brief_upload/".$temp_folder;
    if (!file_exists($folder_path)) {
    	mkdir($folder_path, 0777, true);
    }
    if(move_uploaded_file($fileTmpLoc, "$folder_path/$fileName")){

    	echo $fileName."@@@@@@@@@@@".$check;

    } else {
    	echo "0";
    }  




    





    

    /*if(!empty($_REQUEST['folder_name']))
    {
        $dir = $_REQUEST['folder_name']."/";
    }
    else
    {
        $brief_id = $_REQUEST['brief_id'];
        $dir = "brief_upload/".$brief_id."/";
    }
    $file=$_FILES["doc_file"]["name"];
    move_uploaded_file($_FILES["doc_file"]["tmp_name"], $dir. $file);


    //$brief_id=trim($this->input->get('brief_id'));
    //echo "update wc_brief set breif_new_doc ='$file' where brief_id ='$brief_id'";
    $this->db->query("update wc_brief set breif_new_doc ='$file' where brief_id ='$brief_id'");
    
    echo ltrim($file);*/
 }



 public function update_wekan($wekan_unique_id,$status){

 	$session_data = $this->session->userdata('front_logged_in');
 	$user_id = $session_data['user_id'];
 	$user_type_id = $session_data['user_type_id'];
 	$user_name = $session_data['user_name'];
 	$client_access = $session_data['client_access'];


 	$query=$this->db->query("select * from wc_settings WHERE setting_key='WEKAN_LINK' ");
 	$userDetails= $query->result_array();
 	$WEKAN_LINK=$userDetails[0]['setting_value'];

 	$query=$this->db->query("select * from wc_settings WHERE setting_key='WEKAN_USERNAME' ");
 	$userDetails= $query->result_array();
 	$WEKAN_USERNAME=$userDetails[0]['setting_value'];


 	$query=$this->db->query("select * from wc_settings WHERE setting_key='WEKAN_PASSWORD' ");
 	$userDetails= $query->result_array();
 	$WEKAN_PASSWORD=$userDetails[0]['setting_value'];

/*$query=$this->db->query("select * from wc_settings WHERE setting_key='WEKAN_BOARD_ID' ");
$userDetails= $query->result_array();
$WEKAN_BOARD_ID=$userDetails[0]['setting_value'];

$query=$this->db->query("select * from wc_settings WHERE setting_key='WEKAN_LIST_ID_PENDING' ");
$userDetails= $query->result_array();
$WEKAN_LIST_ID_PENDING=$userDetails[0]['setting_value'];


$query=$this->db->query("select * from wc_settings WHERE setting_key='WEKAN_LIST_ID_REJECTED' ");
$userDetails= $query->result_array();
$WEKAN_LIST_ID_REJECTED=$userDetails[0]['setting_value'];*/

/*$checkquerys = $this->db->query("select * from wc_clients where  client_id='$client_access' ")->result_array();
$WEKAN_BOARD_ID=$checkquerys[0]['wekan_client_id'];
$WEKAN_LIST_ID_PENDING=$checkquerys[0]['wekan_pending_id'];
$WEKAN_LIST_ID_ONGOING=$checkquerys[0]['wekan_ongoing_id'];
$WEKAN_LIST_ID_REJECTED=$checkquerys[0]['wekan_rejecting_id'];*/



$checkquerys = $this->db->query("select * from wc_clients where  client_id='$client_access' ")->result_array();
$WEKAN_BOARD_ID=$checkquerys[0]['wekan_client_id'];

$checkquerys = $this->db->query("select * from wc_client_wekan_list_relation where wekan_board_list_id='1' and  client_id='$client_access' ")->result_array();
$WEKAN_LIST_ID_PENDING=$checkquerys[0]['wekan_list_id'];

$checkquerys = $this->db->query("select * from wc_client_wekan_list_relation where wekan_board_list_id='3' and  client_id='$client_access' ")->result_array();
$WEKAN_LIST_ID_ONGOING=$checkquerys[0]['wekan_list_id'];

$checkquerys = $this->db->query("select * from wc_client_wekan_list_relation where wekan_board_list_id='2' and  client_id='$client_access' ")->result_array();
$WEKAN_LIST_ID_REJECTED=$checkquerys[0]['wekan_list_id'];






if ($this->session->userdata('wekan_integration')) {

	$wekan_session_data = $this->session->userdata('wekan_integration');
	$wekan_authorId = $wekan_session_data['wekan_authorId'];
	$wekan_token = $wekan_session_data['wekan_token'];
	$wekan_tokenExpires = $wekan_session_data['wekan_tokenExpires'];
}
else
{

	$curl = curl_init();
	curl_setopt_array($curl, array(
		CURLOPT_URL => $WEKAN_LINK."users/login",
		CURLOPT_RETURNTRANSFER => true,
		CURLOPT_ENCODING => "",
		CURLOPT_MAXREDIRS => 10,
		CURLOPT_TIMEOUT => 30,
		CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
		CURLOPT_CUSTOMREQUEST => "POST",
		CURLOPT_POSTFIELDS => "username=".$WEKAN_USERNAME."&password=".$WEKAN_PASSWORD."",
		CURLOPT_HTTPHEADER => array(
			"cache-control: no-cache",
			"content-type: application/x-www-form-urlencoded"
		),
	));
	$response = curl_exec($curl);
	$err = curl_error($curl);

	curl_close($curl);

	if (!$err)
	{
        //var_dump($response);



		$res_arr=json_decode($response);

        //echo "<pre>";
        //print_r($res_arr);
		$id=$res_arr->id;
		$token=$res_arr->token;
		$tokenExpires=$res_arr->tokenExpires;


		$sess_array = array(
			'wekan_authorId' => $id,
			'wekan_token' => $token,
			'wekan_tokenExpires' => $tokenExpires
		);
		$this->session->set_userdata('wekan_integration', $sess_array);


		$wekan_session_data = $this->session->userdata('wekan_integration');
		$wekan_authorId = $wekan_session_data['wekan_authorId'];
		$wekan_token = $wekan_session_data['wekan_token'];
		$wekan_tokenExpires = $wekan_session_data['wekan_tokenExpires'];

	}

}




if($status==1)
{
	$task="Ongoing task";
}
elseif($status==4)
{
	$task="Rejected Brief";
}

$ch = curl_init();

curl_setopt($ch, CURLOPT_URL,$WEKAN_LINK."api/boards/".$WEKAN_BOARD_ID."/cards/".$wekan_unique_id."/comments");
curl_setopt($ch, CURLOPT_HTTPHEADER, array(
	'Content-Type: application/x-www-form-urlencoded',
	'Authorization: Bearer ' . $wekan_token
));
curl_setopt($ch, CURLOPT_POST, 1);
curl_setopt($ch, CURLOPT_POSTFIELDS,
	"authorId=".$wekan_authorId."&comment=".$user_name." moved this card from Brief - Review Pending to ".$task );
curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

$server_output1 = curl_exec($ch);

curl_close ($ch);
$id_arr1=json_decode($server_output1);







$ch = curl_init();

curl_setopt($ch, CURLOPT_URL,$WEKAN_LINK."api/boards/".$WEKAN_BOARD_ID."/lists/".$WEKAN_LIST_ID_REJECTED."/cards/".$wekan_unique_id);
curl_setopt($ch, CURLOPT_HTTPHEADER, array(
	'Content-Type: application/x-www-form-urlencoded',
	'Authorization: Bearer ' . $wekan_token
));
//curl_setopt($ch, CURLOPT_POST, 1);
curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "PUT");
curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
curl_setopt($ch, CURLOPT_POSTFIELDS,
	"listId=".$WEKAN_LIST_ID_PENDING."");

$server_output = curl_exec($ch);

curl_close ($ch);



}







public function move_upload_file(){


	$session_data = $this->session->userdata('front_logged_in');
	$data['user_id'] = $session_data['user_id'];
	$data['user_type_id'] = $session_data['user_type_id'];
	$data['user_type_id'] = $session_data['user_type_id'];
	$data['user_name'] = $session_data['user_name'];
	$user_name = $session_data['user_name'];
	$user_id=$data['user_id'];





	$brief_id = $this->input->post('brief_id');
	$temp_folder = $this->input->post('temp_folder');
	$status="0";

	$checkquerys = $this->db->query("select * from wc_users where  user_id='$user_id' ")->result_array();
	$brand_id=trim($checkquerys[0]['brand_id'],",");

	$file_nm=trim($this->input->post('file_nm'),",,,");

	$file_nm_arr=explode(",,,",$file_nm);

   // echo "<pre>";
   // print_r($file_nm_arr);exit;

	$main_file_name=$temp_folder.'.zip';

	$this->zipFolder($temp_folder);


	$data = array(
		'brief_doc'=>$main_file_name,
		'last_updated_on'=>date("Y-m-d H:i:s"),
		'status'=>$status,
		'cron_mail_status'=>'0'
	);
	$this->uploadbrief_model->upload_brief_update($data,$brief_id);

	$checkquerys = $this->db->query("select * from wc_brief where  brief_id='$brief_id' ")->result_array();
	$wekan_unique_id=$checkquerys[0]['wekan_unique_id'];
	$this->update_wekan($wekan_unique_id,$status);   




	$checkquerys = $this->db->query("select * from wc_brief where  brief_id='$brief_id' ")->result_array();
	$added_by_user_id=$checkquerys[0]['added_by_user_id'];
	$brand_id=$checkquerys[0]['brand_id'];
	$brief_title=$checkquerys[0]['brief_title'];
	$brief_name=preg_replace('/[^a-zA-Z0-9_.]/', '_',$brief_title);




	for($i=0;$i<count($file_nm_arr);$i++)
	{
		$file_name=$file_nm_arr[$i];



		$data = array(
			'brief_id'=>$brief_id,
			'brief_group_id'=>$temp_folder,
			'brief_doc'=>$file_name,
			'last_updated_on'=>date("Y-m-d H:i:s"),
		);
		$this->uploadbrief_model->upload_brief_records_insert($data);
		$brief_record_id=$this->db->insert_id();



		$src = "temp_brief_upload/".$temp_folder; 
        //$dst = "brief_upload/".$brief_id; 


		$checkquerys = $this->db->query("select * from wc_brief where  brief_id='$brief_id' ")->result_array();
		$added_by_user_id=$checkquerys[0]['added_by_user_id'];
		$brand_id=$checkquerys[0]['brand_id'];
		$brief_title=$checkquerys[0]['brief_title'];
		$brief_name=preg_replace('/[^a-zA-Z0-9_.]/', '_',$brief_title);

		/*$checkquerys = $this->db->query("select wc_brands.*,wc_clients.client_name from wc_brands inner join wc_clients on wc_brands.client_id=wc_clients.client_id where  wc_brands.brand_id='$brand_id' ")->result_array();*/

       /*$checkquerys = $this->db->query("select wc_brands.*,wc_clients.client_name from wc_users 
        inner join wc_brands on wc_users.brand_id=wc_brands.brand_id  
        inner join wc_clients on wc_brands.client_id=wc_clients.client_id 
        where  wc_users.user_id='$added_by_user_id' ")->result_array();*/

        

        $checkquerys = $this->db->query("select wc_brands.*,wc_clients.client_name from wc_brands
        	inner join wc_clients on wc_brands.client_id=wc_clients.client_id
        	where wc_brands.brand_id='$brand_id' ")->result_array();

        $brand_id=$checkquerys[0]['brand_id'];
        $brand_name=preg_replace('/[^a-zA-Z0-9_.]/', '_',$checkquerys[0]['brand_name']);
        $client_id=$checkquerys[0]['client_id'];
        $client_name=preg_replace('/[^a-zA-Z0-9_.]/', '_',$checkquerys[0]['client_name']);



        //$dst = "upload/".$client_id."-".$client_name."/".$brand_id."-".$brand_name."/".$brief_id."-brief";
        $dst = "upload/".$client_id."-".$client_name."/".$brand_id."-".$brand_name."/".$brief_id."-".$brief_name;   

        if (!file_exists($dst)) {
        	mkdir($dst, 0777, true);
        }

        $src = "temp_brief_upload/".$temp_folder."/".$file_name; 
       //$dst = "brief_upload/".$brief_id."/".$file_name;
       //$dst = "upload/".$client_id."-".$client_name."/".$brand_id."-".$brand_name."/".$brief_id."-brief"."/".$file_name;  
        $dst = "upload/".$client_id."-".$client_name."/".$brand_id."-".$brand_name."/".$brief_id."-".$brief_name."/".$file_name;    

        copy($src, $dst);
        unlink($src);
        $this->removeFolder($src);


     }

     $src = "temp_brief_upload/".$temp_folder."/".$temp_folder.'.zip'; 
       // $dst = "brief_upload/".$brief_id."/".$temp_folder.'.zip'; 
         //$dst = "upload/".$client_id."-".$client_name."/".$brand_id."-".$brand_name."/".$brief_id."-brief"."/".$temp_folder.'.zip';
     $dst = "upload/".$client_id."-".$client_name."/".$brand_id."-".$brand_name."/".$brief_id."-".$brief_name."/".$temp_folder.'.zip';   

     copy($src, $dst);
     unlink($src);


     $this->removeFolder($src);

     $src = "temp_brief_upload/".$temp_folder; 
     $this->delete_directory($src); 







 //////////////////////////////////Send Mail Start///////////////////////////////////////////////

     /*********************************Send mail*************************************/

     $query=$this->db->query("select * from  wc_settings  WHERE setting_key='MAIL_SMTP_USER' ");
     $userDetails= $query->result_array();
     $MAIL_SMTP_USER=$userDetails[0]['setting_value'];

     $query=$this->db->query("select * from  wc_settings  WHERE setting_key='MAIL_SMTP_PASSWORD' ");
     $userDetails= $query->result_array();
     $MAIL_SMTP_PASSWORD=$userDetails[0]['setting_value'];

     $query=$this->db->query("select * from  wc_settings  WHERE setting_key='MAIL_BODY_ADDED_BRIEF' ");
     $userDetails= $query->result_array();
     $MAIL_BODY=$userDetails[0]['setting_value'];

     $query=$this->db->query("select * from  wc_settings  WHERE setting_key='MAIL_FROM_TEXT' ");
     $userDetails= $query->result_array();
     $MAIL_FROM_TEXT=$userDetails[0]['setting_value'];

     $query=$this->db->query("select * from  wc_settings  WHERE setting_key='MAIL_FROM' ");
     $userDetails= $query->result_array();
     $MAIL_FROM=$userDetails[0]['setting_value'];



     $checkquerys = $this->db->query("select * from wc_brief where  brief_id='$brief_id' ")->result_array();
     $brief_id=$checkquerys[0]['brief_id'];
         //$rejected_reasons=$checkquerys[0]['rejected_reasons'];
     $added_by_user_id=$checkquerys[0]['added_by_user_id'];
     $brief_title=$checkquerys[0]['brief_title'];





     $checkquerys = $this->db->query("select * from wc_users where  user_id='$added_by_user_id' ")->result_array();
     $brand_manager_email=$checkquerys[0]['user_email'];
     $brand_manager_name=$checkquerys[0]['user_name'];
     $client_id=$checkquerys[0]['client_id'];
     $subject=$MAIL_FROM_TEXT.' - Brief Re Uploaded(#'.$brief_id.')';


     $checkquerys_mail = $this->db->query("select * from wc_users where  client_id='$client_id' and user_type_id='3' ")->result_array();


     if(!empty($checkquerys_mail))
     {
     	foreach($checkquerys_mail as $key => $value)
     	{

     		$to_email_all_pm[]=$value['user_email'];

     	}

     }
     $checkquerys_clientmail= $this->db->query("SELECT * FROM `wc_clients` WHERE `client_id`='$client_id'")->result_array();
     $client_email=$checkquerys_clientmail[0]['client_email'];
     array_push($to_email_all_pm,$client_email);









         //$unique_user_id=time()."-".$user_id;
         //$activation_link=base_url()."activate-registration/".base64_encode($unique_user_id);


             //Load email library
     $this->load->library('email');

     $config = array();
             //$config['protocol'] = 'smtp';
     $config['protocol']     = 'mail';
     $config['smtp_host'] = 'localhost';
     $config['smtp_user'] = $MAIL_SMTP_USER;
     $config['smtp_pass'] = $MAIL_SMTP_PASSWORD;
     $config['smtp_port'] = 25;




     $this->email->initialize($config);
     $this->email->set_newline("\r\n");





     $website_url=base_url();
     $dashboard_url=base_url()."dashboard";


            //$from_email = "demo.intexom@gmail.com";
     $from_email = $MAIL_FROM;
            //$to_email = $to_email_all;
     $to_email = $to_email_all_pm;

     $date_time=date("d/m/y");
     /*$copy_right=$MAIL_FROM_TEXT." - ".date("Y");*/
     $copy_right=$MAIL_FROM_TEXT;
     $project_name="#".$brief_id." - ".$brief_title;
            //echo $message=$MAIL_BODY;
     $find_arr = array("##WEBSITE_URL##","##PROJECT_NAME##","##DASHBOARD_LINK##","##COPY_RIGHT##");
     $replace_arr = array($website_url,$project_name,$dashboard_url,$copy_right);
     $message=str_replace($find_arr,$replace_arr,$MAIL_BODY);

            //echo print_r($to_email)."---".$from_email."----".$message;exit;



     $this->email->from($from_email, $MAIL_FROM_TEXT);
     $this->email->to($to_email);
     if($_SERVER['HTTP_HOST']!='collab.piquic.com')
     {
     	$this->email->cc(array('poulami@mokshaproductions.in','pranav@mokshaproductions.in','raja.priya@mokshaproductions.in','rajendra.prasad@mokshaproductions.in'));
     }

            //$this->email->bcc('$client_email');
             //$this->email->bcc('pranav@mokshaproductions.in');
     $this->email->subject($subject);
     $this->email->message($message);
             //Send mail
     if($this->email->send())
     {

                 //echo "mail sent";exit;
     	$this->session->set_flashdata("email_sent","Congragulation Email Send Successfully.");
     }

     else
     {
                 //echo $this->email->print_debugger();
                 //echo "mail not sent";exit;
     	$this->session->set_flashdata("email_sent","You have encountered an error");
     }


        /////////////////////////////////////Send Mail End//////////////////////////////////////////












  }
























  public function checkexistsku()
{           // checking the email  exist  validation in add form

	if ($this->session->userdata('front_logged_in')) {
		$session_data = $this->session->userdata('front_logged_in');
		$data['user_id'] = $session_data['user_id'];
		$data['user_type_id'] = $session_data['user_type_id'];
		$data['user_name'] = $session_data['user_name'];

		$user_id=$session_data['user_id'];;
	}
	else
	{
		$data['user_id'] = '';
		$data['user_type_id'] = '';
		$data['user_name'] = '';
		$user_id='';
	}


	$txtSKUNew = $this->input->post('txtSKUNew');
	$sql = "SELECT * FROM tbl_upload_img WHERE user_id='$user_id' and zipfile_name = '$txtSKUNew' ";
	$query = $this->db->query($sql);
	if( $query->num_rows() > 0 ){
		echo 'false';
	} else {
		echo 'true';
	}

}   





public function uploadimage(){


	if ($this->session->userdata('front_logged_in')) {
		$session_data = $this->session->userdata('front_logged_in');
		$data['user_id'] = $session_data['user_id'];
		$data['user_type_id'] = $session_data['user_type_id'];
		$data['user_name'] = $session_data['user_name'];

		$id='4';
		$data['pagecontents'] = $this->Dashboard_model->get_page($id);
		$this->load->view('front/uploadimage');    


	} else {
        //echo "aaaaaa";exit;
        //If no session, redirect to login page
        //redirect('login', 'refresh');
		$session_data = $this->session->userdata('front_logged_in');
		$data['user_id'] = $session_data['user_id'];
		$data['user_type_id'] = $session_data['user_type_id'];
		$data['user_name'] = $session_data['user_name'];

		$id='3';
		$data['pagecontents'] = $this->Dashboard_model->get_page($id);
		$this->load->view('front/uploadimage');    
	}




}



public function uploadfile_old1(){

	if ($this->session->userdata('front_logged_in')) {
		$session_data = $this->session->userdata('front_logged_in');
		$data['user_id'] = $session_data['user_id'];
		$data['user_type_id'] = $session_data['user_type_id'];
		$data['user_name'] = $session_data['user_name'];
		$folderName=$data['user_id'];
		$user_id=$data['user_id'];



            $fileName = $_FILES["upld_zip"]["name"]; // The file name
            $fileTmpLoc = $_FILES["upld_zip"]["tmp_name"]; // File in the PHP tmp folder
            $fileType = $_FILES["upld_zip"]["type"]; // The type of file it is
            $fileSize = $_FILES["upld_zip"]["size"]; // File size in bytes
            $fileErrorMsg = $_FILES["upld_zip"]["error"]; // 0 for false... and 1 for true
            if (!$fileTmpLoc) { // if file not chosen
            	echo "ERROR: Please select a file to upload.";
            	exit();
            }
            
            $folder_path="zip/$folderName";
            if (!file_exists($folder_path)) {
            	mkdir($folder_path, 0777, true);
            }
            if(move_uploaded_file($fileTmpLoc, "$folder_path/$fileName")){
            	echo "ZIP file uploaded successfully!";

            } else {
            	echo "ZIP file upload failed!";
            } 


         } else {
        //echo "aaaaaa";exit;
        //If no session, redirect to login page
         	redirect('login', 'refresh');
         }




      }


      public function uploadfile_old(){

      	if ($this->session->userdata('front_logged_in')) {
      		$session_data = $this->session->userdata('front_logged_in');
      		$data['user_id'] = $session_data['user_id'];
      		$data['user_type_id'] = $session_data['user_type_id'];
      		$data['user_name'] = $session_data['user_name'];
      		$folderName=$data['user_id'];
      		$user_id=$data['user_id'];



            $fileName = $_FILES["upld_zip"]["name"]; // The file name
            $fileTmpLoc = $_FILES["upld_zip"]["tmp_name"]; // File in the PHP tmp folder
            $fileType = $_FILES["upld_zip"]["type"]; // The type of file it is
            $fileSize = $_FILES["upld_zip"]["size"]; // File size in bytes
            $fileErrorMsg = $_FILES["upld_zip"]["error"]; // 0 for false... and 1 for true
            if (!$fileTmpLoc) { // if file not chosen
            	echo "ERROR: Please select a file to upload.";
            	exit();
            }
            
            $folder_path="zip/$folderName";
            if (!file_exists($folder_path)) {
            	mkdir($folder_path, 0777, true);
            }
            if(move_uploaded_file($fileTmpLoc, "$folder_path/$fileName")){
            //$file_nm=$this->input->post('file_nm');
                //$current_path="zip/$folderName/$file_nm";
            	$current_path="$folder_path/$fileName";
            	$unzip = new ZipArchive;
            	$out = $unzip->open($current_path);
            	if ($out === TRUE) {
                //echo getcwd();
                //$unzip->extractTo(getcwd());
            		$unzip->extractTo('zip/'.$folderName.'/');
            		$unzip->close();
                //echo 'File unzipped';
            	}
            	else{
                    //echo 'File not unzipped';
            	}

            	$unziped_folder=preg_replace('/.[^.]*$/', '', $current_path);

            	$dir = "./".$unziped_folder."/";
            	if (is_dir($unziped_folder)) {
            		if ($dh = opendir($unziped_folder)) {
            			$i=0;
            			while (($file = readdir($dh)) !== false) {
        //if($i>1)
        //{
        //echo $file;
            				$file_image=pathinfo($file, PATHINFO_FILENAME);
        //////////////////////////Convert JPG start//////////////////////////////////////
            				$file=$dir.$file;
            				if(is_dir($file)) {
          //echo ("$file is a directory");
            				} else {
            					$file=$this->convertToJpeg($dir,$file,$file_image);
            				}
        //////////////////////////Convert JPG end//////////////////////////////////////
            			}
   // $i++;
    //}
            			closedir($dh);
            		}
            		if (!is_dir("./".$unziped_folder."/thumb/")) {
            			@mkdir("./".$unziped_folder."/thumb/", 0777, true);
            		}

            		if ($dh = opendir($unziped_folder)) {
            			$i=0;
            			while (($file = readdir($dh)) !== false) {
        //if($i>1)
        //{
       //echo $file;
            				$file_image=pathinfo($file, PATHINFO_FILENAME);
        //////////////////////////////////////CREATE TEMP THUMB///////////////////////////////////////
            				$file1=$dir.$file;
            				if(is_dir($file1)) {
          //echo ("$file is a directory");
            				} else {
            					$new_thumb = "./".$unziped_folder."/thumb/"."temp_thumb_".$file_image.".jpg";
            					$file_pat = "./".$unziped_folder."/".$file;
            					$this->convertToThumb($new_thumb,$user_id,$file_pat);
    //////////////////////////////////////////////////////////////////////////////////// 

            				}

            			}
    //$i++;
    //}
            			closedir($dh);
            		}




            	}



            	echo $fileName."@@@@@ZIP file uploaded successfully!";

            } else {
            	echo "ZIP file upload failed!";
            }  


         } else {
        //echo "aaaaaa";exit;
        //If no session, redirect to login page
         	redirect('login', 'refresh');
         }




      }



      public function uploadfile(){

      	if ($this->session->userdata('front_logged_in')) {
      		$session_data = $this->session->userdata('front_logged_in');
      		$data['user_id'] = $session_data['user_id'];
      		$data['user_type_id'] = $session_data['user_type_id'];
      		$data['user_name'] = $session_data['user_name'];
      		$folderName=$data['user_id'];
      		$user_id=$data['user_id'];            
            $fileName = $_FILES["upld_zip"]["name"]; // The file name
            $fileTmpLoc = $_FILES["upld_zip"]["tmp_name"]; // File in the PHP tmp folder
            $fileType = $_FILES["upld_zip"]["type"]; // The type of file it is
            $fileSize = $_FILES["upld_zip"]["size"]; // File size in bytes
            $fileErrorMsg = $_FILES["upld_zip"]["error"]; // 0 for false... and 1 for true
            if (!$fileTmpLoc) { // if file not chosen
            	echo "ERROR: Please select a file to upload.";
            	exit();
            }
            $folder_path="zip/$folderName";
            if (!file_exists($folder_path)) {
            	mkdir($folder_path, 0777, true);
            }
            if(move_uploaded_file($fileTmpLoc, "$folder_path/$fileName")){
            //$file_nm=$this->input->post('file_nm');
                //$current_path="zip/$folderName/$file_nm";
            	$current_path="$folder_path/$fileName";
            	$unzip = new ZipArchive;
            	$out = $unzip->open($current_path);
            	if ($out === TRUE) {
                //echo getcwd();
                //$unzip->extractTo(getcwd());

            		$path = 'zip/'.$folderName.'/'.substr($fileName,0,-4);
            		$unzip->extractTo($path);

                //$unzip->extractTo('zip/'.$folderName.'/');
            		$unzip->close();
                //echo 'File unzipped';
            	}
            	else{
                    //echo 'File not unzipped';
            	}

            	$unziped_folder=preg_replace('/.[^.]*$/', '', $current_path);

            	$dir = "./".$unziped_folder."/";
            	if (is_dir($unziped_folder)) {
            		if ($dh = opendir($unziped_folder)) {
            			while (($file = readdir($dh)) !== false) {
            				if($file!='.' && $file!='..' && $file!='thumb')
            				{
            					if (is_dir($unziped_folder."/".$file)){
                           //echo $file."  is a folder<br>";
            						if (!is_dir("./".$unziped_folder."/".$file."/thumb/")) {
            							@mkdir("./".$unziped_folder."/".$file."/thumb/", 0777, true);
            						}
            						$current_path1=$current_path."/".$file;
            						$unziped_folder1=preg_replace('/.[^.]*$/', '', $current_path1);
            						$unziped_folder1=$unziped_folder1."/".$file;
            						$dir1 = "./".$unziped_folder1."/".$file."/";
            						if ($dh1 = opendir($unziped_folder1)) {
            							while (($file1 = readdir($dh1)) !== false) {
            								if($file1!='.' && $file1!='..' && $file1!='thumb')
            								{
            									$file_image1=pathinfo($file1, PATHINFO_FILENAME);
                                    //////////////////////////////////////CREATE TEMP THUMB///////////////////////////////////////
                                    //$file1=$dir1.$file1;

            									if(is_dir($file1)) {
            									} else {
            										$new_thumb1 = "./".$unziped_folder1."/thumb/"."temp_thumb_".$file_image1.".jpg";
            										$file_pat1 = "./".$unziped_folder1."/".$file1;
            										$this->convertToThumb($new_thumb1,$user_id,$file_pat1);
                                ////////////////////////////////////////////////////////////////////////////////////                                    
            									}
            								}
            							}
            							closedir($dh1);
            						}   
            						else{
            							echo "folder cant open";
            						}
            					}
            					else
            					{                               
                        //echo $file."  is a file<br>";
            						$file_image=pathinfo($file, PATHINFO_FILENAME);
                            //////////////////////////Convert JPG start//////////////////////////////////////
            						$file=$dir.$file;
            						$file=$this->convertToJpeg($dir,$file,$file_image);
                            //////////////////////////Convert JPG end//////////////////////////////////////
            						if (!is_dir("./".$unziped_folder."/thumb/")) {
            							@mkdir("./".$unziped_folder."/thumb/", 0777, true);
            						}
            						$new_thumb = "./".$unziped_folder."/thumb/"."temp_thumb_".$file_image.".jpg";
            						$file_pat = "./".$unziped_folder."/".$file;
            						$this->convertToThumb($new_thumb,$user_id,$file_pat);                   
            					}   
            				}
            			}
            			closedir($dh);
            		}
            	}
            	echo $fileName."@@@@@ZIP file uploaded successfully!";

            } else {
            	echo "ZIP file upload failed!";
            }  
         } else {
        //echo "aaaaaa";exit;
        //If no session, redirect to login page
         	redirect('login', 'refresh');
         }




      }













      public function moveunzipfile_old(){


      	if ($this->session->userdata('front_logged_in')) {
      		$session_data = $this->session->userdata('front_logged_in');
      		$data['user_id'] = $session_data['user_id'];
      		$data['user_type_id'] = $session_data['user_type_id'];
      		$data['user_name'] = $session_data['user_name'];
      		$user_name = $session_data['user_name'];
      		$user_id=$data['user_id'];
      		$folderName=$data['user_id'];

      		$file_nm=$this->input->post('file_nm');
      		$current_path="zip/".$user_id."/";

      		$dir = "zip/".$user_id."/";
    // Open a directory, and read its contents
      		if (is_dir($dir)){
      			if ($dh = opendir($dir)){
      				while (($file = readdir($dh)) !== false){

      					if($file!='.' && $file!='..')
      					{
       //echo "filename:" . $file . "<br>";
      						$sku_folder=$file;

      						$data = array(
      							'user_id'=>$user_id,
      							'zipfile_name'=>$sku_folder,
      							'client_name'=>$user_name,
      						);

      						$this->uploadbrief_model->upload_img_insert($data);
      						$upimg_id=$this->db->insert_id();

      						$src = "zip/".$user_id."/".$sku_folder; 

      						$dst = "upload/".$user_id."/".$sku_folder; 


      						if (!file_exists($dst)) {
      							mkdir($dst, 0777, true);
      						}

      						$this->custom_copy($src, $dst); 




///////////////////////////////////////////////////////////////////////////////////////////    

            // /$directories = glob('upload/'.$user_id.'/'.$sku_folder . '/*' , GLOB_ONLYDIR);

            // echo "upload/".$user_id."/".$sku_folder."/*.*";
      						$directories = glob("upload/".$user_id."/".$sku_folder."/*.*", GLOB_BRACE);

             //echo "<pre>";

           // print_r( $directories);





      						foreach($directories as $file)
      						{
            //echo "---".$dir."---";


      							$ext = pathinfo($file, PATHINFO_EXTENSION);

            //echo $file."-----";
      							$arr=explode("/",$file); 



      							$dir = "./".$arr[0]."/".$arr[1]."/".$arr[2]."/";

      							$img = $file;

      							$file_image=pathinfo($arr[3], PATHINFO_FILENAME);

      							if(strtoupper($ext)=='PNG')
      							{
            //////////////////////////Convert JPG start//////////////////////////////////////

      								$this->convertToJpeg($dir,$img,$file_image);
      								unlink($file);
      							}


      							$file_image=pathinfo($arr[3], PATHINFO_FILENAME);
      							$new_thumb = "./".$arr[0]."/".$arr[1]."/".$arr[2]."/"."thumb_".$file_image.".jpg";
      							$file_p = "./".$arr[0]."/".$arr[1]."/".$arr[2]."/".$file_image.".jpg";

      							$this->convertToThumb($new_thumb,$user_id,$file_p);











      						}   




            //////////////////////////////////////////////////////////////////









    ////////////////////////////////////////////////////////////////////////////

      						$dir_sub = "zip/".$user_id."/".$sku_folder."/";
            // Open a directory, and read its contents
      						if (is_dir($dir_sub)){
      							if ($dh_sub = opendir($dir_sub)){
      								while (($file_sub = readdir($dh_sub)) !== false){

      									if($file_sub!='.' && $file_sub!='..')
      									{
            //echo "filename:" . $file_sub . "<br>";
      										$sku_file=$file_sub;

            ////////////////////////////////////////////////////////////////////////////


      										$img_name_check=strtoupper($file_sub);

      										if(strstr($img_name_check,'FRONT'))
      										{
      											$img_ang='Front';
      										}
      										elseif(strstr($img_name_check,'BACK'))
      										{
      											$img_ang='Back';
      										}
      										elseif(strstr($img_name_check,'LEFT'))
      										{
      											$img_ang='LeftSide';
      										}
      										elseif(strstr($img_name_check,'RIGHT'))
      										{
      											$img_ang='RightSide';
      										}
      										elseif(strstr($img_name_check,'TAG'))
      										{
      											$img_ang='Tag';
      										}
      										else
      										{
      											$img_ang='';
      										}


      										$ext_item = pathinfo($file_sub, PATHINFO_EXTENSION);
      										if(strtoupper($ext_item)=='PNG')
      										{
      											$file_image_item=str_replace("png","jpg",str_replace("PNG","JPG",$file_sub));
      										}
      										else
      										{
      											$file_image_item=$file_sub;
      										}



      										$data = array(
                //'user_id'=>$user_id,
      											'upimg_id'=>$upimg_id,
      											'img_name'=>$file_image_item,
      											'img_ang'=>$img_ang,
      										);

      										$checkquerys = $this->db->query("select id from tbl_upload_images where upimg_id='$upimg_id' and img_name='$file'  and upimg_id='$upimg_id'")->result_array();
      										if(empty($checkquerys))
      										{
      											$this->uploadbrief_model->upload_images_insert($data);
      											$id=$this->db->insert_id();
      										}
      										else
      										{
      											$id=$checkquerys[0]['id'];
      										}



            ////////////////////////////////////////////////////////////////////////////



      									}
      								}
      								closedir($dh_sub);
      							}
      						} 


      						$this->delete_directory($src); 

      ////////////////////////////////////////////////////////////////////////////






      					}
      				}
      				closedir($dh);
      			}
      		}










      	}

      }





      public function moveunzipfile(){
      	if ($this->session->userdata('front_logged_in')) {
      		$session_data = $this->session->userdata('front_logged_in');
      		$data['user_id'] = $session_data['user_id'];
      		$data['user_type_id'] = $session_data['user_type_id'];
      		$data['user_name'] = $session_data['user_name'];
      		$user_name = $session_data['user_name'];
      		$user_id=$data['user_id'];
      		$folderName=$data['user_id'];
      		$file_nm=$this->input->post('file_nm');
      		$current_path="zip/".$user_id."/";
      		$dir = "zip/".$user_id."/";
    // Open a directory, and read its contents
      		if (is_dir($dir)){
      			if ($dh = opendir($dir)){
      				while (($file = readdir($dh)) !== false){     
      					if($file!='.' && $file!='..')
      					{
      						$src = "zip/".$user_id."/".$file;

      						if((is_dir($src))&&(count(glob("$src/*"))===0))
      						{
                //echo count(glob("$src/*"))."<br>";
                //$this->delete_directory( $src ); 

      						}

       //echo "filename:" . $file . "<br>";
      						$sku_folder=$file;
      						$data = array(
      							'user_id'=>$user_id,
      							'zipfile_name'=>$sku_folder,
      							'client_name'=>$user_name,
      						);
      						$this->uploadbrief_model->upload_img_insert($data);
      						$upimg_id=$this->db->insert_id();
      						$src = "zip/".$user_id."/".$sku_folder; 
      						$dst = "upload/".$user_id."/".$sku_folder; 

      						if (!file_exists($dst)) {
      							mkdir($dst, 0777, true);
      						}
      						$this->custom_copy($src, $dst); 
///////////////////////////////////////////////////////////////////////////////////////////    

            // /$directories = glob('upload/'.$user_id.'/'.$sku_folder . '/*' , GLOB_ONLYDIR);

            // echo "upload/".$user_id."/".$sku_folder."/*.*";
      						$directories = glob("upload/".$user_id."/".$sku_folder."/*.*", GLOB_BRACE);

             //echo "<pre>";

           //print_r( $directories);


      						foreach($directories as $file)
      						{
            //echo "---".$dir."---";


      							$ext = pathinfo($file, PATHINFO_EXTENSION);

            //echo $file."-----";
      							$arr=explode("/",$file); 



      							$dir = "./".$arr[0]."/".$arr[1]."/".$arr[2]."/";

      							$img = $file;

      							$file_image=pathinfo($arr[3], PATHINFO_FILENAME);

      							if(strtoupper($ext)=='PNG')
      							{
            //////////////////////////Convert JPG start//////////////////////////////////////

      								$this->convertToJpeg($dir,$img,$file_image);
      								unlink($file);
      							}


      							$file_image=pathinfo($arr[3], PATHINFO_FILENAME);
      							$new_thumb = "./".$arr[0]."/".$arr[1]."/".$arr[2]."/"."thumb_".$file_image.".jpg";
      							$file_p = "./".$arr[0]."/".$arr[1]."/".$arr[2]."/".$file_image.".jpg";
      							$this->convertToThumb($new_thumb,$user_id,$file_p);
      						}   
            ///////////////////////////////////////////////////////////////
            ////////////////////////////////////////////////////////////////////////////

      						$dir_sub = "zip/".$user_id."/".$sku_folder."/";
            // Open a directory, and read its contents
      						if (is_dir($dir_sub)){
      							if ($dh_sub = opendir($dir_sub)){
      								while (($file_sub = readdir($dh_sub)) !== false){

      									if($file_sub!='.' && $file_sub!='..')
      									{
            //echo "filename:" . $file_sub . "<br>";
      										$sku_file=$file_sub;
            ////////////////////////////////////////////////////////////////////////////
      										$img_name_check=strtoupper($file_sub);

      										if(strstr($img_name_check,'FRONT'))
      										{
      											$img_ang='Front';
      										}
      										elseif(strstr($img_name_check,'BACK'))
      										{
      											$img_ang='Back';
      										}
      										elseif(strstr($img_name_check,'LEFT'))
      										{
      											$img_ang='LeftSide';
      										}
      										elseif(strstr($img_name_check,'RIGHT'))
      										{
      											$img_ang='RightSide';
      										}
      										elseif(strstr($img_name_check,'TAG'))
      										{
      											$img_ang='Tag';
      										}
      										else
      										{
      											$img_ang='';
      										}


      										$ext_item = pathinfo($file_sub, PATHINFO_EXTENSION);
      										if(strtoupper($ext_item)=='PNG')
      										{
      											$file_image_item=str_replace("png","jpg",str_replace("PNG","JPG",$file_sub));
      										}
      										else
      										{
      											$file_image_item=$file_sub;
      										}
      										$data = array(
                //'user_id'=>$user_id,
      											'upimg_id'=>$upimg_id,
      											'img_name'=>$file_image_item,
      											'img_ang'=>$img_ang,
      										);

      										$checkquerys = $this->db->query("select id from tbl_upload_images where upimg_id='$upimg_id' and img_name='$file'  and upimg_id='$upimg_id'")->result_array();
      										if(empty($checkquerys))
      										{
      											$this->uploadbrief_model->upload_images_insert($data);
      											$id=$this->db->insert_id();
      										}
      										else
      										{
      											$id=$checkquerys[0]['id'];
      										}
            ////////////////////////////////////////////////////////////////////////////
      									}
      								}
      								closedir($dh_sub);
      							}
      						} 


       //$this->delete_directory($src); 

      ////////////////////////////////////////////////////////////////////////////
      					}
      				}
      			}
      			closedir($dh);
      			$dir = "zip/".$user_id."/";
    // Open a directory, and read its contents
      			if (is_dir($dir)){
      				if ($dh = opendir($dir)){
      					while (($file = readdir($dh)) !== false){     
      						if($file!='.' && $file!='..')
      						{
      							$src = "zip/".$user_id."/".$file;           
      							$this->delete_directory($src); 
      						}
      					}
      				}
      				closedir($dh);
      			}


      		}       
      	}
      }
















      function custom_copy($src, $dst) {  

// open the source directory 
      	$dir = @opendir($src);  

// Make the destination directory if not exist 
      	@mkdir($dst);  

// Loop through the files in source directory 
      	while( $file = readdir($dir) ) {  

      		if (( $file != '.' ) && ( $file != '..' )) {  
      			if ( is_dir($src . '/' . $file) )  
      			{  

            // Recursively calling custom copy function 
            // for sub directory  
      				custom_copy($src . '/' . $file, $dst . '/' . $file);  

      			}  
      			else {  
      				copy($src . '/' . $file, $dst . '/' . $file);  
      			}  
      		}  
      	}  

      	closedir($dir); 
      } 


      public function moveunzipfile_old1(){


      	if ($this->session->userdata('front_logged_in')) {
      		$session_data = $this->session->userdata('front_logged_in');
      		$data['user_id'] = $session_data['user_id'];
      		$data['user_type_id'] = $session_data['user_type_id'];
      		$data['user_name'] = $session_data['user_name'];
      		$user_name = $session_data['user_name'];
      		$user_id=$data['user_id'];
      		$folderName=$data['user_id'];

      		$file_nm=$this->input->post('file_nm');
      		$current_path="zip/$folderName/$file_nm";

      		$unzip = new ZipArchive;
      		$out = $unzip->open($current_path);
      		if ($out === TRUE) {

    //echo getcwd();
    //$unzip->extractTo(getcwd());
      			$unzip->extractTo('zip/'.$folderName.'/');
      			$unzip->close();
    //echo 'File unzipped';


    //////////////////////////////////////////////////////////////////
      			$directories = glob('zip/'.$folderName . '/*' , GLOB_ONLYDIR);

    //echo "<pre>";
    //print_r( $directories);exit;

      			foreach($directories as $dir)
      			{

      				$dir_path=str_replace('zip/'.$folderName.'/', '', $dir);




      				$data = array(
      					'user_id'=>$user_id,
      					'zipfile_name'=>$dir_path,
      					'client_name'=>$user_name,

      				);

      				$checkquerys = $this->db->query("select upimg_id from tbl_upload_img where zipfile_name='$dir_path' and user_id='$user_id' ")->result_array();
      				if(empty($checkquerys))
      				{
      					$this->uploadbrief_model->upload_img_insert($data);
      					$upimg_id=$this->db->insert_id();
      				}
      				else
      				{
      					$upimg_id=$checkquerys[0]['upimg_id'];
      				}




      				$sub_path='zip/'.$folderName . '/'.$dir_path . '/';

      				$files = array_diff(scandir($sub_path), array('.', '..'));

    /* echo "<pre>";
    print_r( $files);
    exit;*/



    foreach($files as $file)
    {



    	$img_name_check=strtoupper($file);

    	if(strstr($img_name_check,'FRONT'))
    	{
    		$img_ang='Front';
    	}
    	elseif(strstr($img_name_check,'BACK'))
    	{
    		$img_ang='Back';
    	}
    	elseif(strstr($img_name_check,'LEFT'))
    	{
    		$img_ang='LeftSide';
    	}
    	elseif(strstr($img_name_check,'RIGHT'))
    	{
    		$img_ang='RightSide';
    	}
    	elseif(strstr($img_name_check,'TAG'))
    	{
    		$img_ang='Tag';
    	}
    	else
    	{
    		$img_ang='';
    	}


    	$ext_item = pathinfo($file, PATHINFO_EXTENSION);
    	if(strtoupper($ext_item)=='PNG')
    	{
    		$file_image_item=str_replace("png","jpg",str_replace("PNG","JPG",$file));
    	}
    	else
    	{
    		$file_image_item=$file;
    	}



    	$data = array(
    //'user_id'=>$user_id,
    		'upimg_id'=>$upimg_id,
    		'img_name'=>$file_image_item,
    		'img_ang'=>$img_ang,
    	);

    	$checkquerys = $this->db->query("select id from tbl_upload_images where upimg_id='$upimg_id' and img_name='$file'  and upimg_id='$upimg_id'")->result_array();
    	if(empty($checkquerys))
    	{
    		$this->uploadbrief_model->upload_images_insert($data);
    		$id=$this->db->insert_id();
    	}
    	else
    	{
    		$id=$checkquerys[0]['id'];
    	}






    }



 }

    ///////////////////////////////////////////////////////////////////////////////////////////
 $current_path="zip/$folderName/$file_nm";


 $folder_path_target="upload/$folderName";
 if (!file_exists($folder_path_target)) {
 	mkdir($folder_path_target, 0777, true);
 }
 $target_path=$folder_path_target."/$file_nm";
 if(copy($current_path, $target_path)){
 	echo "ZIP file moved successfully!";


 	$unzip = new ZipArchive;
 	$out = $unzip->open($target_path);
 	if ($out === TRUE) {

    //echo getcwd();
    //$unzip->extractTo(getcwd());
 		$unzip->extractTo('upload/'.$folderName);
 		$unzip->close();
    //echo 'File unzipped';
    //unlink($current_user_path);

 		unlink($current_path);
 		unlink($target_path);




 		$directories = glob('zip/'.$folderName . '/*' , GLOB_ONLYDIR);

    //echo "<pre>";
    //print_r( $directories);exit;

 		foreach($directories as $dir)
 		{
    //echo "---".$dir."---";

    $files = glob($dir.'/*'); // get all file names
    foreach($files as $file){ // iterate files
    	if(is_file($file))
    unlink($file); // delete file
}

rmdir($dir);

}

rmdir('zip/'.$folderName);



} else {
	echo 'Error';
}





} else {
	echo "ZIP file moved failed!";
}






    /////////////////////////////////////////////////////////////////////////////////////////// 

$directories = glob('upload/'.$folderName . '/*' , GLOB_ONLYDIR);


    //echo "<pre>";

    //print_r( $directories);exit;

foreach($directories as $dir)
{
    //echo "---".$dir."---";

    $files = glob($dir.'/*'); // get all file names
    foreach($files as $file){ // iterate files

    	$ext = pathinfo($file, PATHINFO_EXTENSION);

    //echo $file."-----";
    	$arr=explode("/",$file); 



    	$dir = "./".$arr[0]."/".$arr[1]."/".$arr[2]."/";

    	$img = $file;

    	$file_image=pathinfo($arr[3], PATHINFO_FILENAME);

    	if(strtoupper($ext)=='PNG')
    	{



    //////////////////////////Convert JPG start//////////////////////////////////////

    		$this->convertToJpeg($dir,$img,$file_image);
    		unlink($file);






    	}


    	$file_image=pathinfo($arr[3], PATHINFO_FILENAME);
    	$new_thumb = "./".$arr[0]."/".$arr[1]."/".$arr[2]."/"."thumb_".$file_image.".jpg";
    	$file_p = "./".$arr[0]."/".$arr[1]."/".$arr[2]."/".$file_image.".jpg";

    	$this->convertToThumb($new_thumb,$user_id,$file_p);







    }



 }   




    //////////////////////////////////////////////////////////////////
} else {
	echo 'Error';
}







} else {
    //echo "aaaaaa";exit;
    //If no session, redirect to login page
	redirect('login', 'refresh');
}



}


public function moveunzipfileCopy(){


	if ($this->session->userdata('front_logged_in')) {
		$session_data = $this->session->userdata('front_logged_in');
		$data['user_id'] = $session_data['user_id'];
		$data['user_type_id'] = $session_data['user_type_id'];
		$data['user_name'] = $session_data['user_name'];
		$user_name = $session_data['user_name'];
		$user_id=$data['user_id'];
		$folderName=$data['user_id'];

		$file_nm=$this->input->post('file_nm');
		$current_path="zip/$folderName/$file_nm";

		$unzip = new ZipArchive;
		$out = $unzip->open($current_path);
		if ($out === TRUE) {

    //echo getcwd();
    //$unzip->extractTo(getcwd());
			$unzip->extractTo('zip/'.$folderName.'/');
			$unzip->close();
			echo 'File unzipped';


    //////////////////////////////////////////////////////////////////
			$directories = glob('zip/'.$folderName . '/*' , GLOB_ONLYDIR);

    //echo "<pre>";
    //print_r( $directories);exit;

			foreach($directories as $dir)
			{

				$dir_path=str_replace('zip/'.$folderName.'/', '', $dir);


				$data = array(
					'user_id'=>$user_id,
					'zipfile_name'=>$dir_path,
					'client_name'=>$user_name,

				);

				$checkquerys = $this->db->query("select upimg_id from tbl_upload_img where zipfile_name='$dir_path' and user_id='$user_id' ")->result_array();
				if(empty($checkquerys))
				{
					$this->uploadbrief_model->upload_img_insert($data);
					$upimg_id=$this->db->insert_id();
				}
				else
				{
					$upimg_id=$checkquerys[0]['upimg_id'];
				}




				$sub_path='zip/'.$folderName . '/'.$dir_path . '/';

				$files = array_diff(scandir($sub_path), array('.', '..'));

    /* echo "<pre>";
    print_r( $files);
    exit;*/



    foreach($files as $file)
    {



    	$img_name_check=strtoupper($file);

    	if(strstr($img_name_check,'FRONT'))
    	{
    		$img_ang='Front';
    	}
    	elseif(strstr($img_name_check,'BACK'))
    	{
    		$img_ang='Back';
    	}
    	elseif(strstr($img_name_check,'LEFT'))
    	{
    		$img_ang='LeftSide';
    	}
    	elseif(strstr($img_name_check,'RIGHT'))
    	{
    		$img_ang='RightSide';
    	}
    	elseif(strstr($img_name_check,'TAG'))
    	{
    		$img_ang='Tag';
    	}
    	else
    	{
    		$img_ang='';
    	}


    	$ext_item = pathinfo($file, PATHINFO_EXTENSION);
    	if(strtoupper($ext_item)=='PNG')
    	{
    		$file_image_item=str_replace("png","jpg",str_replace("PNG","JPG",$file));
    	}
    	else
    	{
    		$file_image_item=$file;
    	}



    	$data = array(
    //'user_id'=>$user_id,
    		'upimg_id'=>$upimg_id,
    		'img_name'=>$file_image_item,
    		'img_ang'=>$img_ang,
    	);

    	$checkquerys = $this->db->query("select id from tbl_upload_images where upimg_id='$upimg_id' and img_name='$file'  and upimg_id='$upimg_id'")->result_array();
    	if(empty($checkquerys))
    	{
    		$this->uploadbrief_model->upload_images_insert($data);
    		$id=$this->db->insert_id();
    	}
    	else
    	{
    		$id=$checkquerys[0]['id'];
    	}






    }



 }

    ///////////////////////////////////////////////////////////////////////////////////////////
 $current_path="zip/$folderName/$file_nm";


 $folder_path_target="upload/$folderName";
 if (!file_exists($folder_path_target)) {
 	mkdir($folder_path_target, 0777, true);
 }
 $target_path=$folder_path_target."/$file_nm";
 if(copy($current_path, $target_path)){
 	echo "ZIP file moved successfully!";


 	$unzip = new ZipArchive;
 	$out = $unzip->open($target_path);
 	if ($out === TRUE) {

    //echo getcwd();
    //$unzip->extractTo(getcwd());
 		$unzip->extractTo('upload/'.$folderName);
 		$unzip->close();
    //echo 'File unzipped';
    //unlink($current_user_path);

 		unlink($current_path);
 		unlink($target_path);




 		$directories = glob('zip/'.$folderName . '/*' , GLOB_ONLYDIR);

    //echo "<pre>";
    //print_r( $directories);exit;

 		foreach($directories as $dir)
 		{
    //echo "---".$dir."---";

    $files = glob($dir.'/*'); // get all file names
    foreach($files as $file){ // iterate files
    	if(is_file($file))
    unlink($file); // delete file
}

rmdir($dir);

}

rmdir('zip/'.$folderName);



} else {
	echo 'Error';
}





} else {
	echo "ZIP file moved failed!";
}






    /////////////////////////////////////////////////////////////////////////////////////////// 

$directories = glob('upload/'.$folderName . '/*' , GLOB_ONLYDIR);


    //echo "<pre>";

    //print_r( $directories);exit;

foreach($directories as $dir)
{
    //echo "---".$dir."---";

    $files = glob($dir.'/*'); // get all file names
    foreach($files as $file){ // iterate files

    	$ext = pathinfo($file, PATHINFO_EXTENSION);

    //echo $file."-----";
    	$arr=explode("/",$file); 



    	$dir = "./".$arr[0]."/".$arr[1]."/".$arr[2]."/";

    	$img = $file;

    	$file_image=pathinfo($arr[3], PATHINFO_FILENAME);

    	if(strtoupper($ext)=='PNG')
    	{



    //////////////////////////Convert JPG start//////////////////////////////////////

    		$this->convertToJpeg($dir,$img,$file_image);
    		unlink($file);






    	}


    	$file_image=pathinfo($arr[3], PATHINFO_FILENAME);
    	$new_thumb = "./".$arr[0]."/".$arr[1]."/".$arr[2]."/"."thumb_".$file_image.".jpg";
    	$file_p = "./".$arr[0]."/".$arr[1]."/".$arr[2]."/".$file_image.".jpg";

    	$this->convertToThumb($new_thumb,$user_id,$file_p);







    }



 } 




    //////////////////////////////////////////////////////////////////
} else {
	echo 'Error';
}







} else {
    //echo "aaaaaa";exit;
    //If no session, redirect to login page
	redirect('login', 'refresh');
}



}






function openZIPFolder_old()
{

	$str='';
	$path = urldecode( $this->input->post('path'));
	$foldername= urldecode( $this->input->post('foldername'));

	$skufolderfile_nm_field= trim(urldecode( $this->input->post('skufolderfile_nm')),",");
	$skufolderfile_nm_arr=explode(",",$skufolderfile_nm_field);

	if( file_exists( $path)) {
		if( $path[ strlen( $path ) - 1 ] ==  '/' )
			$folder = $path;
		else
			$folder = $path . '/';

		$path=$path.'/'.$foldername;
		$dir = opendir( $path );
		$i = 0;
		while(( $file = readdir( $dir ) ) != false )
		{
			$imgid = $foldername.'_'.$i;
//echo $file;
			if($file!='.' && $file!='..' && $file!='thumb')
			{

				$files[] = $file;

  //$inside_path=

				$str.='<div class="col-12 col-sm-12 col-md-4 col-lg-4 col-xl-4">
				<script type="text/javascript">
				var img = document.getElementById("img_'.$imgid.'");
				var width = img.naturalWidth;
				var height = img.naturalHeight;
        // console.log(width);
        // console.log(height);

				if (width > height){
					$("#rtImg_'.$imgid.'").removeClass("d-none");
					} else {
						$("#orgImg_'.$imgid.'").removeClass("d-none");
					}
					</script>
					<div id="orgImg_'.$imgid.'" class="w-100 d-none">
					<img id="img_'.$imgid.'" class="w-100 mb-3" src="'.$path.'/thumb/temp_thumb_'.$file.'" alt="'.$file.'">
					</div>

					<div id="rtImg_'.$imgid.'" class="img-box d-none">
					<img id="img_'.$imgid.'" class="img mb-3" src="'.$path.'/thumb/temp_thumb_'.$file.'" alt="'.$file.'">
					</div>
					</div>';


//$str.=' <div class="col-12 col-sm-12 col-md-4 col-lg-4 col-xl-4"> <img class="w-100 mb-3" src="'.$path.'/'.$file.'"> </div>';
				}
				$i++;
			}

			echo $str;
 //echo "<pre>";
// print_r($files);
//return $files;
			closedir( $dir );
		}


	}


	function openZIPFolder()
	{

		$str='';
		$path = urldecode( $this->input->post('path'));
		$foldername= urldecode( $this->input->post('foldername'));

		$skufolderfile_nm_field= trim(urldecode( $this->input->post('skufolderfile_nm')),",");
		$skufolderfile_nm_arr=explode(",",$skufolderfile_nm_field);

		if( file_exists( $path)) {
			if( $path[ strlen( $path ) - 1 ] ==  '/' )
				$folder = $path;
			else
				$folder = $path . '/';

			$path=$path.'/'.$foldername;
			$dir = opendir( $path );
			$i = 0;
			while(( $file = readdir( $dir ) ) != false )
			{
				$imgid = $foldername.'_'.$i;
//echo $file;

				if($file!='.' && $file!='..' && $file!='thumb')
				{

					if (is_dir($path."/".$file)){
        //echo $file."  is a folder<hr><br>";

						$path1=$path.'/'.$file;
						$dir1 = opendir( $path1 );
						$j = 0;
						while(( $file1 = readdir( $dir1 ) ) != false )
						{
							$imgid1 = $foldername.'_'.$i.'_'.$j;
            //echo $file;

							if($file1!='.' && $file1!='..' && $file1!='thumb')
							{
               //echo $file1;
								$files1[] = $file1;

                          //$inside_path=

								$str.='<div class="col-12 col-sm-12 col-md-4 col-lg-4 col-xl-4">
								<script type="text/javascript">
								var img = document.getElementById("img_'.$imgid1.'");
								var width = img.naturalWidth;
								var height = img.naturalHeight;
                                // console.log(width);
                                // console.log(height);

								if (width > height){
									$("#rtImg_'.$imgid1.'").removeClass("d-none");
									} else {
										$("#orgImg_'.$imgid1.'").removeClass("d-none");
									}
									</script>
									<div id="orgImg_'.$imgid1.'" class="w-100 d-none">
									<img id="img_'.$imgid1.'" class="w-100 mb-3" src="'.$path1.'/thumb/temp_thumb_'.$file1.'" alt="'.$file1.'">
									</div>

									<div id="rtImg_'.$imgid1.'" class="img-box d-none">
									<img id="img_'.$imgid1.'" class="img mb-3" src="'.$path1.'/thumb/temp_thumb_'.$file1.'" alt="'.$file1.'">
									</div>
									</div>';




								}
								$j++;
							}

						}
						else    
						{
							$files[] = $file;

  //$inside_path=

							$str.='<div class="col-12 col-sm-12 col-md-4 col-lg-4 col-xl-4">
							<script type="text/javascript">
							var img = document.getElementById("img_'.$imgid.'");
							var width = img.naturalWidth;
							var height = img.naturalHeight;
        // console.log(width);
        // console.log(height);

							if (width > height){
								$("#rtImg_'.$imgid.'").removeClass("d-none");
								} else {
									$("#orgImg_'.$imgid.'").removeClass("d-none");
								}
								</script>
								<div id="orgImg_'.$imgid.'" class="w-100 d-none">
								<img id="img_'.$imgid.'" class="w-100 mb-3" src="'.$path.'/thumb/temp_thumb_'.$file.'" alt="'.$file.'">
								</div>

								<div id="rtImg_'.$imgid.'" class="img-box d-none">
								<img id="img_'.$imgid.'" class="img mb-3" src="'.$path.'/thumb/temp_thumb_'.$file.'" alt="'.$file.'">
								</div>
								</div>';


//$str.=' <div class="col-12 col-sm-12 col-md-4 col-lg-4 col-xl-4"> <img class="w-100 mb-3" src="'.$path.'/'.$file.'"> </div>';
							}
						}
						$i++;
					}

					echo $str;
 //echo "<pre>";
// print_r($files);
//return $files;
					closedir( $dir );
				}


			}




			function removeZIPFolder_old()
			{

				$str='';
				$path = urldecode( $this->input->post('path'));
				$foldername= urldecode( $this->input->post('foldername')).".zip";
				$zipfile= $path."/".$foldername;
				$skufolderfile_nm_field= trim(urldecode( $this->input->post('skufolderfile_nm')),",");
				$skufolderfile_nm_arr=explode(",",$skufolderfile_nm_field);

				if( file_exists($zipfile)) {
					if(is_file($zipfile)){
    //Use the unlink function to delete the file.
    //unlink($zipfile);
						if(unlink($zipfile))
						{
							echo ("$foldername successfully removed");
						}
						else
						{
							echo ("$foldername couldn't be removed"); 
						}
					}
				}

				$str='';
				$path = urldecode( $this->input->post('path'));
				$foldername= urldecode( $this->input->post('foldername'));
//echo $path."/".$foldername;
				$skufolderfile_nm_field= trim(urldecode( $this->input->post('skufolderfile_nm')),",");
				$skufolderfile_nm_arr=explode(",",$skufolderfile_nm_field);



				$thumbpath = $path."/".$foldername."/thumb";
				$this->removeThumbSKUFolder($thumbpath);


				$path=$path."/".$foldername;
				if( file_exists($path)) {


					$files = glob($path . '/*');

//Loop through the file list.
					foreach($files as $file){
//Make sure that this is a file and not a directory.
						if(is_file($file)){
    //Use the unlink function to delete the file.
							unlink($file);
						}
					}

					if(rmdir($path))
					{
      //echo ("$foldername successfully removed");
					}
					else
					{
      //echo ("$foldername couldn't be removed"); 
					}

				}
				else
				{
					echo "error";
				}




			}

			function removeZIPFolder()
			{

				$str='';
				$path = urldecode( $this->input->post('path'));
				$foldername= urldecode( $this->input->post('foldername')).".zip";
				$foldername1= urldecode( $this->input->post('foldername'));
				$folpath=$path."/".$foldername1;
				$zipfile= $path."/".$foldername;
				$skufolderfile_nm_field= trim(urldecode( $this->input->post('skufolderfile_nm')),",");
				$skufolderfile_nm_arr=explode(",",$skufolderfile_nm_field);

				if( file_exists($zipfile)) {
					if(is_file($zipfile)){
    //Use the unlink function to delete the file.
    //unlink($zipfile);
						if(unlink($zipfile))
						{
							echo ("$foldername successfully removed");
						}
						else
						{
							echo ("$foldername couldn't be removed"); 
						}
					}
				}

				if (is_dir($folpath)) {
					$thumbpath = $folpath."/thumb";
					if(is_dir($thumbpath)) {
						$this->removeThumbSKUFolder($thumbpath);
					}
					$objects = scandir($folpath);
					foreach ($objects as $object) {
						if ($object != "." && $object != "..") {
							if (filetype($folpath."/".$object) == "dir")
							{
								$thumbpath1 = $folpath."/".$object."/thumb";
								if(is_dir($thumbpath1)) {
									$this->removeThumbSKUFolder($thumbpath1);
								}
								$folpath1=$folpath."/".$object;
								$objects1 = scandir($folpath1);
								foreach ($objects1 as $object1) {
									if ($object1 != "." && $object1 != "..") {
										if (filetype($folpath1."/".$object1) == "dir")
										{
											rmdir($folpath1."/".$object1);
										}
										else{
											unlink   ($folpath1."/".$object1);
										}

									}
									reset($objects1);
									if(is_dir($folpath."/".$object)) {
										@rmdir($folpath."/".$object); 
									}
								}
							}
							else{
								unlink($folpath."/".$object);
							}
							reset($objects);
							@rmdir($folpath);
						}


					}
				}
/*
$str='';
$path = urldecode( $this->input->post('path'));
$foldername= urldecode( $this->input->post('foldername'));
//echo $path."/".$foldername;
$skufolderfile_nm_field= trim(urldecode( $this->input->post('skufolderfile_nm')),",");
$skufolderfile_nm_arr=explode(",",$skufolderfile_nm_field);



$thumbpath = $path."/".$foldername."/thumb";
$this->removeThumbSKUFolder($thumbpath);


$path=$path."/".$foldername;
if( file_exists($path)) {
    
    
    $files = glob($path . '/*');

//Loop through the file list.
foreach($files as $file){
//Make sure that this is a file and not a directory.
if(is_file($file)){
    //Use the unlink function to delete the file.
    unlink($file);
}
}   
    if(rmdir($path))
    {
      //echo ("$foldername successfully removed");
    }
    else
    {
      //echo ("$foldername couldn't be removed"); 
    }
    
}
else
{
    echo "error";
}

*/


}


public function convertToThumb($new_thumb,$user_id,$file_path) {

	if(!file_exists($new_thumb)){
		$width = "400";
		$height = "600";
		$quality = 90;
		$img = $file_path;

//Generate Thumbnail Images

		$file = $img;
		$dest = $new_thumb;
		$height = 600;
		$width = 400;
		$output_format = "jpg";
		$output_quality = 90;
		$bg_color = array(255, 255, 255);


//Justify the Image format and create a GD resource
/*$image_info = getimagesize($file);
list($cur_width, $cur_height, $cur_type, $cur_attr) = getimagesize($file);*/
$image_info = @getimagesize($file);
list($cur_width, $cur_height, $cur_type, $cur_attr) = @getimagesize($file);




$image_type = $image_info[2];
switch($image_type){
	case IMAGETYPE_JPEG:
	$image = imagecreatefromjpeg($file);
	break;
	case IMAGETYPE_GIF:
	$image = imagecreatefromgif($file);
	break;
	case IMAGETYPE_GIF:
	$image = imagecreatefrompng($file);
	break;
	default:
	die("Image Format not Suppoted");
}

if($cur_width>$cur_height){
	$degrees = -90;
	$image = imagerotate($image, $degrees, 0);
}

$image_width = imagesx($image);
$image_height = imagesy($image);




//echo $file;

//Get The Calculations
// Calculate the size of the Image
//If Image width is bigger than the Thumbnail Width
if($image_width>$image_height){





//echo "hoko";
//echo $image_width.">".$image_height;

    //Set Image Width to Thumbnail Width
	$new["width"] = $width;
    //Calculate Height according to width
	$new["height"] = ($new["width"]/$image_width)*$image_height;

    //If Resulting height is bigger than the thumbnail Height
	if($new["height"]>$height){

        //Set the image Height to THUmbnail Height
		$new["height"] = $height;
        //Recalculate width according to height of the thumbnail
		$new["width"] = ($new["height"]/$image_height)*$image_width;

	}

}else{
//echo "moko";
	$new["height"] = $height;
	$new["width"] = ($new["height"]/$image_height)*$image_width;

	if($new["width"]>$width){

		$new["width"] = $width;
		$new["height"] = ($new["width"]/$image_width)*$image_height;

	}

}

//Calculate the image position based on the difference between the dimensons of the new image and thumbnail
$x = ($width-$new["width"])/2;
$y = ($height-$new["height"])/2;

$calc =  array_merge($new, array("x"=>$x,"y"=>$y));


// End Calculate The Image



//Create an Empty image
$canvas = imagecreatetruecolor($width, $height);

//Load Background color
$color = imagecolorallocate($canvas,
	$bg_color[0],
	$bg_color[1],
	$bg_color[2]
);

//FIll the Image with the Background color
imagefilledrectangle(
	$canvas,
	0,
	0,
	$width,
	$height,
	$color
);

//The REAL Magic
imagecopyresampled(
	$canvas,
	$image,
	$calc["x"],
	$calc["y"],
	0,
	0,
	$calc["width"],
	$calc["height"],
	$image_width,
	$image_height
);

// Create Output Image

$image = $canvas;
$format = $output_format;
$quality = $output_quality;



switch($format){
	case "jpg":
	imagejpeg($image, $dest, $quality);
	break;
	case "gif":
	imagegif($image, $dest);
	break;
	case "png":
        //Png Quality is measured from 1 to 9
	imagepng($image, $dest, round(($quality/100)*9) );
	break;
}

//unlink($img);

}
else{
// echo "file is exist.";
}




}

public function convertToJpeg($dir,$img,$file_image) {

	$dst = $dir . $file_image;

	if (($img_info = getimagesize($img)) === FALSE)
		die("Image not found or not an image");

	$width = $img_info[0];
	$height = $img_info[1];

	switch ($img_info[2]) {
		case IMAGETYPE_GIF  : $src = imagecreatefromgif($img);  break;
		case IMAGETYPE_JPEG : $src = imagecreatefromjpeg($img); break;
		case IMAGETYPE_PNG  : $src = imagecreatefrompng($img);  break;
		default : die("Unknown filetype");
	}

	$tmp = imagecreatetruecolor($width, $height);

	$file =$dst.".jpg";

	imagecopyresampled($tmp, $src, 0, 0, 0, 0, $width, $height, $width, $height);
	imagejpeg($tmp, $file);
	return $file_image.".jpg";

}



public function insertSKUFromSidebar(){

	if ($this->session->userdata('front_logged_in')) {
		$session_data = $this->session->userdata('front_logged_in');
		$data['user_id'] = $session_data['user_id'];
		$data['user_type_id'] = $session_data['user_type_id'];
		$data['user_name'] = $session_data['user_name'];

		$user_id=$session_data['user_id'];
		$user_name=$session_data['user_name'];
	}
	else
	{
		$data['user_id'] = '';
		$data['user_type_id'] = '';
		$data['user_name'] = '';
		$user_id='';
		$user_name='';
	}

	$txtSKUNew=$this->input->post('txtSKUNew');
	$pimg=$this->input->post('pimg');


	$data = array(
		'user_id'=>$user_id,
		'zipfile_name'=>$txtSKUNew,
		'client_name'=>$user_name,

	);

	$checkquerys = $this->db->query("select upimg_id from tbl_upload_img where zipfile_name='$txtSKUNew' and user_id='$user_id' ")->result_array();
	if(empty($checkquerys))
	{
		$this->uploadbrief_model->upload_img_insert($data);
		$upimg_id=$this->db->insert_id();
	}
	else
	{
		$upimg_id=$checkquerys[0]['upimg_id'];
	}



	$allImage=trim($pimg,",");
	$allImageArr=explode(",",$allImage);

	foreach($allImageArr as $key => $file)
	{

		if (!is_dir("./upload/".$user_id."/".$txtSKUNew)) {
			mkdir("./upload/".$user_id."/".$txtSKUNew, 0777, true);
		}

		$source_path="./temp_upload/".$user_id."/".$file;
		$destination_path="./upload/".$user_id."/".$txtSKUNew."/".$file;
		copy($source_path, $destination_path);
		unlink($source_path);

		$thumb_source_path="./temp_upload/".$user_id."/thumb_".$file;
		$thumb_destination_path="./upload/".$user_id."/".$txtSKUNew."/thumb_".$file;
		copy($thumb_source_path, $thumb_destination_path);
		unlink($thumb_source_path);


		$img_name_check=strtoupper($file);

		if(strstr($img_name_check,'FRONT'))
		{
			$img_ang='Front';
		}
		elseif(strstr($img_name_check,'BACK'))
		{
			$img_ang='Back';
		}
		elseif(strstr($img_name_check,'LEFT'))
		{
			$img_ang='LeftSide';
		}
		elseif(strstr($img_name_check,'RIGHT'))
		{
			$img_ang='RightSide';
		}
		elseif(strstr($img_name_check,'TAG'))
		{
			$img_ang='Tag';
		}
		else
		{
			$img_ang='';
		}

		$data = array(
        //'user_id'=>$user_id,
			'upimg_id'=>$upimg_id,
			'img_name'=>$file,
			'img_ang'=>$img_ang,
		);

		$checkquerys = $this->db->query("select id from tbl_upload_images where upimg_id='$upimg_id' and img_name='$file'  and upimg_id='$upimg_id'")->result_array();
		if(empty($checkquerys))
		{
			$this->uploadbrief_model->upload_images_insert($data);
			$id=$this->db->insert_id();
		}
		else
		{
			$id=$checkquerys[0]['id'];
		}
	}


	echo "success";

}










































public function create()
{
	$file_name = $this->input->post('file_nm');     
	$uid = $this->input->post('uid');

	$data = array(
		'user_id'   => $uid,
		'zipfile_name'  => $file_name,              
	);
	$this->load->model('Upload_image');
	$insert = $this->upload_image->createData($data);
	echo json_encode($insert);
}




public function uploadSKUFolder(){


	if ($this->session->userdata('front_logged_in')) {
		$session_data = $this->session->userdata('front_logged_in');
		$data['user_id'] = $session_data['user_id'];
		$data['user_type_id'] = $session_data['user_type_id'];
		$data['user_name'] = $session_data['user_name'];
		$user_id=$data['user_id'];
		$folderName=$data['user_id'];

		$upld_skufolder_name_path=$_POST['upld_skufolder_name'];

		$upld_skufolder_name_path_arr=explode("/",$upld_skufolder_name_path);

		$path_count=count($upld_skufolder_name_path_arr);

		if($path_count==3)
		{
			$upld_skufolder_name=$upld_skufolder_name_path_arr[1];
		}
		elseif($path_count==2)
		{
			$upld_skufolder_name=$upld_skufolder_name_path_arr[0];
		}
        //echo $path_count;exit;

        //echo "<pre>";
        //print_r($_FILES);exit;



        $fileName = $_FILES["upld_skufolder"]["name"]; // The file name
        $fileTmpLoc = $_FILES["upld_skufolder"]["tmp_name"]; // File in the PHP tmp folder
        $fileType = $_FILES["upld_skufolder"]["type"]; // The type of file it is
        $fileSize = $_FILES["upld_skufolder"]["size"]; // File size in bytes
        $fileErrorMsg = $_FILES["upld_skufolder"]["error"]; // 0 for false... and 1 for true

        //echo $fileType;
        //exit;
        $ext = strtoupper(pathinfo($fileName, PATHINFO_EXTENSION));

        // if($fileType!='image/jpeg')
        // {
        //  echo "not_image";   
        //  exit;
        // }

        if($ext=='JPG' || $ext=='JPEG' || $ext=='PNG') { }
        	else { echo "not_image"; exit; }

        if (!$fileTmpLoc) { // if file not chosen
        	echo "ERROR: Please select a file to upload.";
        	exit();
        }

        $folder_path="temp_folder_upload/".$user_id."/".$upld_skufolder_name."/";
        if (!file_exists($folder_path)) {
        	@mkdir($folder_path, 0777, true);
        }


      // if($_FILES["upld_skufolder"]["type"]=='image/jpeg'|| $_FILES["upld_skufolder"]["type"]=='image/jpg')
        if($ext=='JPG' || $ext=='JPEG' )
        {
        	$file=$_FILES["upld_skufolder"]["name"];
        	move_uploaded_file($fileTmpLoc, $folder_path."/".$fileName);
        }
        else
        {

        	$dir = "./temp_folder_upload/".$user_id."/".$upld_skufolder_name."/";

        	$img = $_FILES['upld_skufolder']['tmp_name'];

        	$file_image=pathinfo($_FILES["upld_skufolder"]["name"], PATHINFO_FILENAME);


        //////////////////////////Convert JPG start//////////////////////////////////////

        	$file=$this->convertToJpeg($dir,$img,$file_image);

        //////////////////////////Convert JPG end//////////////////////////////////////

        }


    //////////////////////////////////////CREATE TEMP THUMB///////////////////////////////////////

        if (!is_dir("./temp_folder_upload/".$user_id."/".$upld_skufolder_name."/thumb/")) {
        	@mkdir("./temp_folder_upload/".$user_id."/".$upld_skufolder_name."/thumb/", 0777, true);
        }

        $file_image=pathinfo($_FILES["upld_skufolder"]["name"], PATHINFO_FILENAME);
        $new_thumb = "./temp_folder_upload/".$user_id."/".$upld_skufolder_name."/thumb/"."temp_thumb_".$file_image.".jpg";

        $file_pat = "./temp_folder_upload/".$user_id."/".$upld_skufolder_name."/".$file;
        $this->convertToThumb($new_thumb,$user_id,$file_pat);
    ////////////////////////////////////////////////////////////////////////////////////  


        echo $upld_skufolder_name."@@@@@Folder uploaded successfully!";



     } else {
        //echo "aaaaaa";exit;
        //If no session, redirect to login page
     	redirect('login', 'refresh');
     }



  }





  public function moveSkuFolderUpload(){



  	if ($this->session->userdata('front_logged_in')) {
  		$session_data = $this->session->userdata('front_logged_in');
  		$data['user_id'] = $session_data['user_id'];
  		$data['user_type_id'] = $session_data['user_type_id'];
  		$data['user_name'] = $session_data['user_name'];
  		$user_name = $session_data['user_name'];
  		$user_id=$data['user_id'];
  		$folderName=$data['user_id'];

  		$skufolderfile_nm=$this->input->post('skufolderfile_nm');
  		$skufolderfile_nm_arr=explode(",",$skufolderfile_nm);

//echo "<pre>";
//print_r($skufolderfile_nm_arr);
  		if(!empty($skufolderfile_nm_arr))
  		{
  			foreach($skufolderfile_nm_arr as $value)
  			{
  				if($value!='')
  				{

  					$skufolderfile_nm=$value;


  					$file_pat = "temp_folder_upload/".$user_id."/".$skufolderfile_nm."/thumb";
  					$this->removeThumbSKUFolder($file_pat);




  					$target_path="temp_folder_upload/".$user_id."/".$skufolderfile_nm."/";

  					$destination_path="upload/".$user_id."/".$skufolderfile_nm."/";




  					$data = array(
  						'user_id'=>$user_id,
  						'zipfile_name'=>$skufolderfile_nm,
  						'client_name'=>$user_name,

  					);

  					$checkquerys = $this->db->query("select upimg_id from tbl_upload_img where zipfile_name='$skufolderfile_nm' and user_id='$user_id' ")->result_array();
  					if(empty($checkquerys))
  					{
  						$this->uploadbrief_model->upload_img_insert($data);
  						$upimg_id=$this->db->insert_id();
  					}
  					else
  					{
  						$upimg_id=$checkquerys[0]['upimg_id'];
  					}






  					if (!file_exists($destination_path)) {
  						mkdir($destination_path, 0777, true);
  					}


    // Open a directory, and read its contents
  					if (is_dir($target_path)){
  						if ($dh = opendir($target_path)){
  							while (($file = readdir($dh)) !== false){
  								echo "filename:" . $file . "<br>";
   /* if($file!='.' && $file!='..')
    {
    copy($target_path.$file, $destination_path.$file);
    unlink($target_path.$file);



    $file_image=pathinfo($file, PATHINFO_FILENAME);
    $new_thumb = "./upload/".$user_id."/".$skufolderfile_nm."/"."thumb_".$file_image.".jpg";

    $file_pat = "./upload/".$user_id."/".$skufolderfile_nm."/".$file;
    $this->convertToThumb($new_thumb,$user_id,$file_pat);




    $img_name_check=strtoupper($file);

    if(strstr($img_name_check,'FRONT'))
    {
    $img_ang='Front';
    }
    elseif(strstr($img_name_check,'BACK'))
    {
    $img_ang='Back';
    }
    elseif(strstr($img_name_check,'LEFT'))
    {
    $img_ang='LeftSide';
    }
    elseif(strstr($img_name_check,'RIGHT'))
    {
    $img_ang='RightSide';
    }
    elseif(strstr($img_name_check,'TAG'))
    {
    $img_ang='Tag';
    }
    else
    {
    $img_ang='';
    }


    $ext_item = pathinfo($file, PATHINFO_EXTENSION);
    if(strtoupper($ext_item)=='PNG')
    {
    $file_image_item=str_replace("png","jpg",str_replace("PNG","JPG",$file));
    }
    else
    {
    $file_image_item=$file;
    }



    $data = array(
    //'user_id'=>$user_id,
    'upimg_id'=>$upimg_id,
    'img_name'=>$file_image_item,
    'img_ang'=>$img_ang,
    );

    $checkquerys = $this->db->query("select id from tbl_upload_images where upimg_id='$upimg_id' and img_name='$file'  and upimg_id='$upimg_id'")->result_array();
    if(empty($checkquerys))
    {
    $this->uploadbrief_model->upload_images_insert($data);
    $id=$this->db->insert_id();
    }
    else
    {
    $id=$checkquerys[0]['id'];
    }








 }*/


 if($file!='.' && $file!='..')
 {
 	if(copy($target_path.$file, $destination_path.$file))
 	{
 		unlink($target_path.$file);
 		$file_image=pathinfo($file, PATHINFO_FILENAME);
 		$new_thumb = "./upload/".$user_id."/".$skufolderfile_nm."/"."thumb_".$file_image.".jpg";

 		$file_pat = "./upload/".$user_id."/".$skufolderfile_nm."/".$file;
 		$a = getimagesize($file_pat);
 		$image_type = $a[2];

 		if(in_array($image_type , array(IMAGETYPE_GIF , IMAGETYPE_JPEG ,IMAGETYPE_PNG , IMAGETYPE_BMP)))
 		{
 			$this->convertToThumb($new_thumb,$user_id,$file_pat);
 			$insertinidb=1;
 		}
 		else
 		{
 			unlink($file_pat);
 		}

 	}
 	$img_name_check=strtoupper($file);

 	if(strstr($img_name_check,'FRONT'))
 	{
 		$img_ang='Front';
 	}
 	elseif(strstr($img_name_check,'BACK'))
 	{
 		$img_ang='Back';
 	}
 	elseif(strstr($img_name_check,'LEFT'))
 	{
 		$img_ang='LeftSide';
 	}
 	elseif(strstr($img_name_check,'RIGHT'))
 	{
 		$img_ang='RightSide';
 	}
 	elseif(strstr($img_name_check,'TAG'))
 	{
 		$img_ang='Tag';
 	}
 	else
 	{
 		$img_ang='';
 	}


 	$ext_item = pathinfo($file, PATHINFO_EXTENSION);
 	if(strtoupper($ext_item)=='PNG')
 	{
 		$file_image_item=str_replace("png","jpg",str_replace("PNG","JPG",$file));
 	}
 	else
 	{
 		$file_image_item=$file;
 	}



 	$data = array(
    //'user_id'=>$user_id,
 		'upimg_id'=>$upimg_id,
 		'img_name'=>$file_image_item,
 		'img_ang'=>$img_ang,
 	);
 	if(isset($insertinidb))
 	{
 		$checkquerys = $this->db->query("select id from tbl_upload_images where upimg_id='$upimg_id' and img_name='$file'  and upimg_id='$upimg_id'")->result_array();
 		if(empty($checkquerys))
 		{
 			$this->uploadbrief_model->upload_images_insert($data);
 			$id=$this->db->insert_id();
 		}
 		else
 		{
 			$id=$checkquerys[0]['id'];
 		}
 	}
 }


}
closedir($dh);
}
}

rmdir($target_path);
rmdir("temp_folder_upload/".$user_id."/");

    /*if(copy($target_path, $destination_path)){

    }*/



 }


}

}














}
else {
//echo "aaaaaa";exit;
//If no session, redirect to login page
	redirect('login', 'refresh');
}






}








function create_tree($path,$skufolderfile_nm_arr) {

//public $files;
//private $folder;

	$files = array();  

	if( file_exists( $path)) {
		if( $path[ strlen( $path ) - 1 ] ==  '/' )
			$folder = $path;
		else
			$folder = $path . '/';

		$dir = opendir( $path );
		while(( $file = readdir( $dir ) ) != false )
		{

   //echo $file;
			if(in_array($file, $skufolderfile_nm_arr))
			{
				echo $file;
				$files[] = $file;
			}

		}

		return $files;
		closedir( $dir );
	}


	if( count( $files ) > 2 ) { /* First 2 entries are . and ..  -skip them */
		natcasesort( $files );
		$list = '<ul class="filetree" style="display: none;">';
  // Group folders first
		foreach( $files as $file ) {
			if( file_exists( $folder . $file ) && $file != '.' && $file != '..' && is_dir( $folder . $file )) {
				$list .= '<li class="folder collapsed"><a href="#" rel="' . htmlentities( $folder . $file ) . '/">' . htmlentities( $file ) . '</a></li>';
			}
		}
  // Group all files
		foreach( $files as $file ) {
			if( file_exists( $folder . $file ) && $file != '.' && $file != '..' && !is_dir( $folder . $file )) {
				$ext = preg_replace('/^.*\./', '', $file);
				$list .= '<li class="file ext_' . $ext . '"><a href="#" rel="' . htmlentities( $folder . $file ) . '">' . htmlentities( $file ) . '</a></li>';
			}
		}
		$list .= '</ul>'; 
		return $list;
	}
}



function Foldertree()
{


	$path = urldecode( $this->input->post('dir'));
	$skufolderfile_nm_field= trim(urldecode( $this->input->post('skufolderfile_nm_field')),",");
	$skufolderfile_nm_arr=explode(",",$skufolderfile_nm_field);
	echo $this->create_tree($path,$skufolderfile_nm_arr);


}



function openSKUFolder()
{

	$str='';
	$path = urldecode( $this->input->post('path'));
	$foldername= urldecode( $this->input->post('foldername'));

	$skufolderfile_nm_field= trim(urldecode( $this->input->post('skufolderfile_nm')),",");
	$skufolderfile_nm_arr=explode(",",$skufolderfile_nm_field);

	if( file_exists( $path)) {
		if( $path[ strlen( $path ) - 1 ] ==  '/' )
			$folder = $path;
		else
			$folder = $path . '/';

		$path=$path.'/'.$foldername;
		$dir = opendir( $path );
		$i = 0;
		while(( $file = readdir( $dir ) ) != false )
		{
			$imgid = $foldername.'_'.$i;
//echo $file;
			if($file!='.' && $file!='..' && $file!='thumb')
			{

				$files[] = $file;

  //$inside_path=
/*
 $str.='<div class="col-12 col-sm-12 col-md-4 col-lg-4 col-xl-4">
        <script type="text/javascript">
        var img = document.getElementById("img_'.$imgid.'");
        var width = img.naturalWidth;
        var height = img.naturalHeight;
        // console.log(width);
        // console.log(height);

        if (width > height){
            $("#rtImg_'.$imgid.'").removeClass("d-none");
        } else {
            $("#orgImg_'.$imgid.'").removeClass("d-none");
        }
        </script>
        <div id="orgImg_'.$imgid.'" class="w-100 d-none">
            <img id="img_'.$imgid.'" class="w-100 mb-3" src="'.$path.'/thumb/temp_thumb_'.$file.'" alt="'.$file.'">
        </div>

        <div id="rtImg_'.$imgid.'" class="img-box d-none">
            <img id="img_'.$imgid.'" class="img mb-3" src="'.$path.'/thumb/temp_thumb_'.$file.'" alt="'.$file.'">
        </div>
        </div>';*/

        $filepath=$path.'/'.$file;
        $a = getimagesize($filepath);
        $image_type = $a[2];
        if(in_array($image_type , array(IMAGETYPE_GIF , IMAGETYPE_JPEG ,IMAGETYPE_PNG , IMAGETYPE_BMP)))
        {
        	$str.='<div class="col-12 col-sm-12 col-md-4 col-lg-4 col-xl-4">
        	<script type="text/javascript">
        	var img = document.getElementById("img_'.$imgid.'");
        	var width = img.naturalWidth;
        	var height = img.naturalHeight;
        // console.log(width);
        // console.log(height);

        	if (width > height){
        		$("#rtImg_'.$imgid.'").removeClass("d-none");
        		} else {
        			$("#orgImg_'.$imgid.'").removeClass("d-none");
        		}
        		</script>
        		<div id="orgImg_'.$imgid.'" class="w-100 d-none">
        		<img id="img_'.$imgid.'" class="w-100 mb-3" src="'.$path.'/thumb/temp_thumb_'.$file.'" alt="'.$file.'">
        		</div>

        		<div id="rtImg_'.$imgid.'" class="img-box d-none">
        		<img id="img_'.$imgid.'" class="img mb-3" src="'.$path.'/thumb/temp_thumb_'.$file.'" alt="'.$file.'">
        		</div>
        		</div>';

        	}      


//$str.=' <div class="col-12 col-sm-12 col-md-4 col-lg-4 col-xl-4"> <img class="w-100 mb-3" src="'.$path.'/'.$file.'"> </div>';
        }
        $i++;
     }

     echo $str;
 //echo "<pre>";
// print_r($files);
//return $files;
     closedir( $dir );
  }


}

//remove folder rajendra

function removeSKUFolder()
{

	$str='';
	$path = urldecode( $this->input->post('path'));
	$foldername= urldecode( $this->input->post('foldername'));
//echo $path."/".$foldername;
	$skufolderfile_nm_field= trim(urldecode( $this->input->post('skufolderfile_nm')),",");
	$skufolderfile_nm_arr=explode(",",$skufolderfile_nm_field);



	$thumbpath = $path."/".$foldername."/thumb";
	$this->removeThumbSKUFolder($thumbpath);


	$path=$path."/".$foldername;
	if( file_exists($path)) {


		$files = glob($path . '/*');

//Loop through the file list.
		foreach($files as $file){
//Make sure that this is a file and not a directory.
			if(is_file($file)){
    //Use the unlink function to delete the file.
				unlink($file);
			}
		}

		if(rmdir($path))
		{
			echo ("$foldername successfully removed");
		}
		else
		{
			echo ("$foldername couldn't be removed"); 
		}

	}
	else
	{
		echo "error";
	}

}
//remove folder ends rajendra



public function removeThumbSKUFolder( $dir )
{
	if( is_dir( $dir ) )
	{
    $files = glob( $dir . '*', GLOB_MARK ); //GLOB_MARK adds a slash to directories returned

    foreach( $files as $file )
    {
    	$this->delete_directory( $file );      
    }

    @rmdir( $dir );
 } 
 elseif( is_file( $dir ) ) 
 {
 	unlink( $dir );  
 }
}



    //logincheck rajendra
function logincheck()
{
	$session_data = $this->session->userdata('front_logged_in');
	$data['user_id'] = $session_data['user_id'];
	$data['user_type_id'] = $session_data['user_type_id'];
	$data['user_name'] = $session_data['user_name'];
	$user_name = $session_data['user_name'];
	$user_id=$data['user_id'];

	if($user_id=='') { echo "sessionout"; } else { echo "sessionin"; }
}   
//logincheck end rajendra



function checkSKULengthExistOrNotFolder()
{

	$session_data = $this->session->userdata('front_logged_in');
	$data['user_id'] = $session_data['user_id'];
	$data['user_type_id'] = $session_data['user_type_id'];
	$data['user_name'] = $session_data['user_name'];
	$user_name = $session_data['user_name'];
	$user_id=$data['user_id'];

	if($user_id!='')
	{ 

		$skufolder = trim( $this->input->post('skufolderfile_nm'),",");

		$skufolder_arr = explode(",",$skufolder);



		$skufolder_arr_temp=$skufolder_arr;
		if(!empty($skufolder_arr))
		{
			$sku_str="";
			foreach($skufolder_arr as $value)
			{
				$skufolder=$value;



				$check_path = glob("temp_folder_upload/".$user_id."/".$skufolder."/*.{jpg,jpeg,png,gif,JPG,JPEG}", GLOB_BRACE);
				$directories = glob("temp_folder_upload/".$user_id."/".$skufolder."/*.*", GLOB_BRACE);
				if(count($check_path)!=count($directories))
				{
					echo 'not_image';

          /////////////////////remove dir////////////////////////////////

					if(!empty($skufolder_arr_temp))
					{
						foreach($skufolder_arr as $value)
						{
							$skufolder=$value;

							$src = "zip/".$user_id."/".$skufolder; 
							$this->delete_directory($src); 


						}
					}
         ///////////////////////////////////////////////////
					exit;
				}
				else if(strlen($skufolder)<=2)
				{
					echo 'less_strlen';

           /////////////////////remove dir////////////////////////////////

					if(!empty($skufolder_arr_temp))
					{
						foreach($skufolder_arr as $value)
						{
							$skufolder=$value;
							$src = "temp_folder_upload/".$user_id."/".$skufolder; 
							$this->delete_directory($src); 

                        /*$dirsku = "zip/".$user_id."/".$skufolder."/";
                        // Open a directory, and read its contents
                        if (is_dir($dirsku)){

                        if ($dhsku = opendir($dirsku)){
                        while (($filesku = readdir($dhsku)) !== false){
                           // echo "filesku:" . $filesku . "<br>";
                            if($filesku!='.' && $filesku!='..')
                            {
                                
                                unlink($dirsku.$filesku);
                            }
                        
                        
                        }
                        closedir($dhsku);
                         }
                        }
                        rmdir($dirsku);*/
                        

                     }
                  }
         ///////////////////////////////////////////////////

                  exit;
               }
               else
               {
               	$sku_str.="'".$skufolder."',";
               }

            }
         }


         $sku_str=trim($sku_str,",");


 // echo "select * from tbl_upload_img  where zipfile_name IN (".$skufolder.") and user_id='$user_id' ";

   //exit;
         if($sku_str!='')
         {
         	$checkquerys = $this->db->query("select * from tbl_upload_img  where zipfile_name IN (".$sku_str.") and user_id='$user_id' ")->result_array();
         	if(!empty($checkquerys))
         	{
         		echo 'exist';
         		exit;
         	}
         	else
         	{
         		echo 'notexist';
         		exit;
         	}
         }

      }
      else
      {
      	echo "sessionout";
      }




   }













   function generateSKUForExistFolder()
   {

   	$session_data = $this->session->userdata('front_logged_in');
   	$data['user_id'] = $session_data['user_id'];
   	$data['user_type_id'] = $session_data['user_type_id'];
   	$data['user_name'] = $session_data['user_name'];
   	$user_name = $session_data['user_name'];
   	$user_id=$data['user_id'];


   	$skufolder = trim( $this->input->post('skufolderfile_nm'),",");

   	$skufolder_arr = explode(",",$skufolder);

   	if(!empty($skufolder_arr))
   	{
   		$sku_str="";
   		foreach($skufolder_arr as $value)
   		{


   			$skufolder=$value;


   			$checkquerys = $this->db->query("select * from tbl_upload_img  where user_id='$user_id' and zipfile_name like '%".$skufolder."_%' order by upimg_id desc ")->result_array();
   			if(empty($checkquerys))
   			{
   				$newskufolder=$skufolder."_1";
   			}
   			else
   			{
   				$ctn=count($checkquerys)+1;

   				$newskufolder=$skufolder."_".$ctn;
   			}

        //echo $newskufolder;

   			$src_path = "temp_folder_upload/".$user_id."/".$skufolder; 
   			$des_path = "temp_folder_upload/".$user_id."/".$newskufolder; 

   			rename($src_path,$des_path);

   			$sku_str.=$newskufolder.",";


   		}
   	}


   	echo $sku_str=",".trim($sku_str,",").",";






   }



   function deleteCancelSKUForExistFolder()
   {

   	$session_data = $this->session->userdata('front_logged_in');
   	$data['user_id'] = $session_data['user_id'];
   	$data['user_type_id'] = $session_data['user_type_id'];
   	$data['user_name'] = $session_data['user_name'];
   	$user_name = $session_data['user_name'];
   	$user_id=$data['user_id'];


   	$file_nm = $this->input->post('file_nm');



   	$dir = "temp_folder_upload/".$user_id."/";
   	$sku_str="";
    // Open a directory, and read its contents
   	if (is_dir($dir)){
   		if ($dh = opendir($dir)){
   			while (($file = readdir($dh)) !== false){

   				if($file!='.' && $file!='..')
   				{
                //echo "filename:" . $file . "<br>";
   					$skufolder=$file;

   					$src = "temp_folder_upload/".$user_id."/".$skufolder; 
   					$this->delete_directory($src); 

   				}

   			}

   		}

   	}


   	echo "success";


   }





























   function checkSKULenExistMoveunzipfile_old()
   {

   	$session_data = $this->session->userdata('front_logged_in');
   	$data['user_id'] = $session_data['user_id'];
   	$data['user_type_id'] = $session_data['user_type_id'];
   	$data['user_name'] = $session_data['user_name'];
   	$user_name = $session_data['user_name'];
   	$user_id=$data['user_id'];

   	if($user_id!='')
   	{

   		$file_nm = trim( $this->input->post('file_nm'));
   		$current_path="zip/".$user_id."/".$file_nm;
   		$thumb_path=substr($current_path,0,-4)."/thumb";
    //$thumb_path=glob($thumb_path.'/*');  
    //$src = "zip/".$user_id."/".$skufolder; 
   		$this->delete_directory($thumb_path); 






 //////////////////////////unzipped///////////////////////////////// not required bcos its already unzipped

   		if(file_exists($current_path))
   		{

    /*
    $unzip = new ZipArchive;
    $out = $unzip->open($current_path);
    if ($out === TRUE) {

    //echo getcwd();
    //$unzip->extractTo(getcwd());
    $unzip->extractTo('zip/'.$user_id.'/');
    $unzip->close();
    //echo 'File unzipped';
    }
    */
    unlink($current_path);
 }


 //////////////////////////unzipped/////////////////////////////////

 $dir = "zip/".$user_id."/";
    // Open a directory, and read its contents
 if (is_dir($dir)){

 	if ($dh = opendir($dir)){
 		$skufolder_arr=array();
 		while (($file = readdir($dh)) !== false){
    //echo "filename:" . $file . "<br>";
 			if($file!='.' && $file!='..')
 			{
 				$skufolder_arr[]=$file;
 			}
 		}
 		closedir($dh);
 	}
 }

//echo "<pre>";
//print_r($skufolder_arr);
 $skufolder_arr_temp=$skufolder_arr;
 $check= '';
 $sku_str="";
 if(!empty($skufolder_arr))
 {

 	foreach($skufolder_arr as $value)
 	{
 		$skufolder=$value;

        //echo "zip/".$user_id."/".$skufolder."/*.*";
 		$check_path = glob("zip/".$user_id."/".$skufolder."/*.{jpg,jpeg,png,gif,JPG,JPEG}", GLOB_BRACE);
 		$directories = glob("zip/".$user_id."/".$skufolder."/*.*", GLOB_BRACE);
        //echo count($check_path)."--".count($directories);exit;
 		if(count($check_path)!=count($directories))
 		{
 			echo 'not_image';

          /////////////////////remove dir////////////////////////////////

 			if(!empty($skufolder_arr_temp))
 			{
 				foreach($skufolder_arr as $value)
 				{
 					$skufolder=$value;

 					$src = "zip/".$user_id."/".$skufolder; 
 					$this->delete_directory($src); 


 				}
 			}
         ///////////////////////////////////////////////////
 			exit;
 		}
 		else if(strlen($skufolder)<=2)
 		{
 			echo 'less_strlen';

          /////////////////////remove dir////////////////////////////////

 			if(!empty($skufolder_arr_temp))
 			{
 				foreach($skufolder_arr as $value)
 				{
 					$skufolder=$value;

 					$src = "zip/".$user_id."/".$skufolder; 
 					$this->delete_directory($src); 


                        /*$dirsku = "zip/".$user_id."/".$skufolder."/";
                        // Open a directory, and read its contents
                        if (is_dir($dirsku)){

                        if ($dhsku = opendir($dirsku)){
                        while (($filesku = readdir($dhsku)) !== false){
                           // echo "filesku:" . $filesku . "<br>";
                            if($filesku!='.' && $filesku!='..')
                            {
                                
                                unlink($dirsku.$filesku);
                            }
                        
                        
                        }
                        closedir($dhsku);
                         }
                        }
                        rmdir($dirsku);*/
                        

                     }
                  }
         ///////////////////////////////////////////////////


                  exit;
               }
               else
               {
               	$sku_str.="'".$skufolder."',";
               }

            }
         }







         $sku_str=trim($sku_str,",");

         if($sku_str!='')
         {

    //echo "select * from tbl_upload_img  where zipfile_name IN (".$sku_str.") and user_id='$user_id' ";

    //exit;
         	$checkquerys = $this->db->query("select * from tbl_upload_img  where zipfile_name IN (".$sku_str.") and user_id='$user_id' ")->result_array();
         	if(!empty($checkquerys))
         	{
         		echo 'exist';
         		exit;
         	}
         	else
         	{
         		echo 'notexist';
         		exit;
         	}

         }

      }
      else
      {
      	echo "sessionout";
      }


   }


   function checkSKULenExistMoveunzipfile_old1()
   {

   	$session_data = $this->session->userdata('front_logged_in');
   	$data['user_id'] = $session_data['user_id'];
   	$data['user_type_id'] = $session_data['user_type_id'];
   	$data['user_name'] = $session_data['user_name'];
   	$user_name = $session_data['user_name'];
   	$user_id=$data['user_id'];

   	if($user_id!='')
   	{

   		$file_nm = trim( $this->input->post('file_nm'));
   		$current_path="zip/".$user_id."/".$file_nm;


 //////////////////////////unzipped/////////////////////////////////
   		if(file_exists($current_path))
   		{


   			$unzip = new ZipArchive;
   			$out = $unzip->open($current_path);
   			if ($out === TRUE) {

    //echo getcwd();
    //$unzip->extractTo(getcwd());
   				$unzip->extractTo('zip/'.$user_id.'/');
   				$unzip->close();
    //echo 'File unzipped';
   			}
   			unlink($current_path);
   		}


 //////////////////////////unzipped/////////////////////////////////

   		$dir = "zip/".$user_id."/";
    // Open a directory, and read its contents
   		if (is_dir($dir)){

   			if ($dh = opendir($dir)){
   				$skufolder_arr=array();
   				while (($file = readdir($dh)) !== false){
    //echo "filename:" . $file . "<br>";
   					if($file!='.' && $file!='..')
   					{
   						$skufolder_arr[]=$file;
   					}
   				}
   				closedir($dh);
   			}
   		}

//echo "<pre>";
//print_r($skufolder_arr);
   		$skufolder_arr_temp=$skufolder_arr;
   		$check= '';
   		$sku_str="";
   		if(!empty($skufolder_arr))
   		{

   			foreach($skufolder_arr as $value)
   			{
   				$skufolder=$value;

        //echo "zip/".$user_id."/".$skufolder."/*.*";
   				$check_path = glob("zip/".$user_id."/".$skufolder."/*.{jpg,jpeg,png,gif,JPG,JPEG}", GLOB_BRACE);
   				$directories = glob("zip/".$user_id."/".$skufolder."/*.*", GLOB_BRACE);
        //echo count($check_path)."--".count($directories);exit;
   				if(count($check_path)!=count($directories))
   				{
   					echo 'not_image';

          /////////////////////remove dir////////////////////////////////

   					if(!empty($skufolder_arr_temp))
   					{
   						foreach($skufolder_arr as $value)
   						{
   							$skufolder=$value;

   							$src = "zip/".$user_id."/".$skufolder; 
   							$this->delete_directory($src); 


   						}
   					}
         ///////////////////////////////////////////////////
   					exit;
   				}
   				else if(strlen($skufolder)<=2)
   				{
   					echo 'less_strlen';

          /////////////////////remove dir////////////////////////////////

   					if(!empty($skufolder_arr_temp))
   					{
   						foreach($skufolder_arr as $value)
   						{
   							$skufolder=$value;

   							$src = "zip/".$user_id."/".$skufolder; 
   							$this->delete_directory($src); 


                        /*$dirsku = "zip/".$user_id."/".$skufolder."/";
                        // Open a directory, and read its contents
                        if (is_dir($dirsku)){

                        if ($dhsku = opendir($dirsku)){
                        while (($filesku = readdir($dhsku)) !== false){
                           // echo "filesku:" . $filesku . "<br>";
                            if($filesku!='.' && $filesku!='..')
                            {
                                
                                unlink($dirsku.$filesku);
                            }
                        
                        
                        }
                        closedir($dhsku);
                         }
                        }
                        rmdir($dirsku);*/
                        

                     }
                  }
         ///////////////////////////////////////////////////


                  exit;
               }
               else
               {
               	$sku_str.="'".$skufolder."',";
               }

            }
         }







         $sku_str=trim($sku_str,",");

         if($sku_str!='')
         {

    //echo "select * from tbl_upload_img  where zipfile_name IN (".$sku_str.") and user_id='$user_id' ";

    //exit;
         	$checkquerys = $this->db->query("select * from tbl_upload_img  where zipfile_name IN (".$sku_str.") and user_id='$user_id' ")->result_array();
         	if(!empty($checkquerys))
         	{
         		echo 'exist';
         		exit;
         	}
         	else
         	{
         		echo 'notexist';
         		exit;
         	}

         }

      }
      else
      {
      	echo "sessionout";
      }


   }


   function checkSKULenExistMoveunzipfile()
   {
   	$session_data = $this->session->userdata('front_logged_in');
   	$data['user_id'] = $session_data['user_id'];
   	$data['user_type_id'] = $session_data['user_type_id'];
   	$data['user_name'] = $session_data['user_name'];
   	$user_name = $session_data['user_name'];
   	$user_id=$data['user_id'];
   	if($user_id!='')
   	{
   		$file_nm = trim( $this->input->post('file_nm'));
   		$current_path="zip/".$user_id."/".$file_nm;
   		$thumb_path=substr($current_path,0,-4)."/thumb";
   		$folder_path=substr($current_path,0,-4);
   		$folder_name=substr($file_nm,0,-4);
    //$thumb_path=glob($thumb_path.'/*');  
    //$src = "zip/".$user_id."/".$skufolder; 

    // future works
                //$path = 'zip/'.$folderName.'/'.substr($fileName,0,-4);
                //$unzip->extractTo($path);

    //
   		if (is_dir($thumb_path)){
   			$this->delete_directory($thumb_path); 
   		}

   		if (is_dir($folder_path)){

   			if ($dh = opendir($folder_path)){
   				while (($file = readdir($dh)) !== false){
   					if($file!='.' && $file!='..')
   					{

   						$src_path = "zip/".$user_id."/".$folder_name."/".$file; 
   						$des_path = "zip/".$user_id."/".$file; 
   						if (is_dir($src_path)){
   							if (is_dir($des_path)){
   								echo "pls change the zip file name and upload";
                             //$des_path = "zip/".$user_id."/".$file."_files";

   								$thumb_path=$src_path."/thumb"; 
   								$this->delete_directory($thumb_path);
                                //echo $des_path;
   								$des_path1 ="zip/".$user_id."/".$folder_name."/".$file."_renamed"; 
   								if(rename($src_path,$des_path1))
   								{
   									if(rename($des_path1,"zip/".$user_id."/".$file."ren"))
   									{
                                       //echo "done";
                                       //$this->delete_directory("zip/".$user_id."/".$folder_name);
   										$single_file=1;
   									}
   								}

   							}
   							else
   							{
   								rename($src_path,$des_path);
   								$thumb_path=$des_path."/thumb"; 
   								$this->delete_directory($thumb_path); 
   								$folderpath_del=1;
   							}

   						}
   						else
   						{

   						}   
   					}
   				}
   			}
   			closedir($dh);
                //rmdir($folder_path);
   			if( isset( $folderpath_del ) ) 
   			{
   				$this->delete_directory($folder_path); 
   			}
   			if( isset( $single_file ) ) 
   			{
                //$this->delete_directory($folder_path); 
   			}
   		}
   		else
   		{
   			echo "its a file single folder";    
   		}
 //////////////////////////unzipped///////////////////////////////// not required bcos its already unzipped

   		if(file_exists($current_path))
   		{

    /*
    $unzip = new ZipArchive;
    $out = $unzip->open($current_path);
    if ($out === TRUE) {

    //echo getcwd();
    //$unzip->extractTo(getcwd());
    $unzip->extractTo('zip/'.$user_id.'/');
    $unzip->close();
    //echo 'File unzipped';
    }
    */
    unlink($current_path);
 }


 //////////////////////////unzipped/////////////////////////////////

 $dir = "zip/".$user_id."/";
    // Open a directory, and read its contents
 if (is_dir($dir)){

 	if ($dh = opendir($dir)){
 		$skufolder_arr=array();
 		while (($file = readdir($dh)) !== false){
    //echo "filename:" . $file . "<br>";
 			if($file!='.' && $file!='..')
 			{
 				$skufolder_arr[]=$file;
 			}
 		}
 		closedir($dh);
 	}
 }

//echo "<pre>";
 print_r($skufolder_arr);
 $skufolder_arr_temp=$skufolder_arr;
 $check= '';
 $sku_str="";
 if(!empty($skufolder_arr))
 {

 	foreach($skufolder_arr as $value)
 	{
 		$skufolder=$value;

        //echo "zip/".$user_id."/".$skufolder."/*.*";
 		$check_path = glob("zip/".$user_id."/".$skufolder."/*.{jpg,jpeg,png,gif,JPG,JPEG}", GLOB_BRACE);
 		$directories = glob("zip/".$user_id."/".$skufolder."/*.*", GLOB_BRACE);
        //echo count($check_path)."--".count($directories);exit;
 		if(count($check_path)!=count($directories))
 		{
 			echo 'not_image';

          /////////////////////remove dir////////////////////////////////

 			if(!empty($skufolder_arr_temp))
 			{
 				foreach($skufolder_arr as $value)
 				{
 					$skufolder=$value;

 					$src = "zip/".$user_id."/".$skufolder; 
 					$this->delete_directory($src); 


 				}
 			}
         ///////////////////////////////////////////////////
 			exit;
 		}
 		else if(strlen($skufolder)<=2)
 		{
 			echo 'less_strlen';

          /////////////////////remove dir////////////////////////////////

 			if(!empty($skufolder_arr_temp))
 			{
 				foreach($skufolder_arr as $value)
 				{
 					$skufolder=$value;

 					$src = "zip/".$user_id."/".$skufolder; 
 					$this->delete_directory($src); 


                        /*$dirsku = "zip/".$user_id."/".$skufolder."/";
                        // Open a directory, and read its contents
                        if (is_dir($dirsku)){

                        if ($dhsku = opendir($dirsku)){
                        while (($filesku = readdir($dhsku)) !== false){
                           // echo "filesku:" . $filesku . "<br>";
                            if($filesku!='.' && $filesku!='..')
                            {
                                
                                unlink($dirsku.$filesku);
                            }
                        
                        
                        }
                        closedir($dhsku);
                         }
                        }
                        rmdir($dirsku);*/
                        

                     }
                  }
         ///////////////////////////////////////////////////


                  exit;
               }
               else
               {
               	$sku_str.="'".$skufolder."',";
               }

            }
         }
         $sku_str=trim($sku_str,",");
   //echo $sku_str;

         if($sku_str!='')
         {

    //echo "select * from tbl_upload_img  where zipfile_name IN (".$sku_str.") and user_id='$user_id' ";

    //exit;
         	$checkquerys = $this->db->query("select * from tbl_upload_img  where zipfile_name IN (".$sku_str.") and user_id='$user_id' ")->result_array();
         	if(!empty($checkquerys))
         	{
         		echo 'exist';
         		exit;
         	}
         	else
         	{
         		echo 'notexist';
         		exit;
         	}

         }

      }
      else
      {
      	echo "sessionout";
      }


   }





   function generateSKUForExistZip()
   {

   	$session_data = $this->session->userdata('front_logged_in');
   	$data['user_id'] = $session_data['user_id'];
   	$data['user_type_id'] = $session_data['user_type_id'];
   	$data['user_name'] = $session_data['user_name'];
   	$user_name = $session_data['user_name'];
   	$user_id=$data['user_id'];


   	$file_nm = $this->input->post('file_nm');

   	$current_path="zip/".$user_id."/";

   	$dir = "zip/".$user_id."/";
   	$sku_str="";
    // Open a directory, and read its contents
   	if (is_dir($dir)){
   		if ($dh = opendir($dir)){
   			while (($file = readdir($dh)) !== false){

   				if($file!='.' && $file!='..')
   				{
        //echo "filename:" . $file . "<br>";
   					$skufolder=$file;

   					$checkquerys = $this->db->query("select * from tbl_upload_img  where user_id='$user_id' and zipfile_name like '%".$skufolder."_%' order by upimg_id desc ")->result_array();
   					if(empty($checkquerys))
   					{
   						$newskufolder=$skufolder."_1";
   					}
   					else
   					{
   						$ctn=count($checkquerys)+1;

   						$newskufolder=$skufolder."_".$ctn;
   					}

        //echo $newskufolder;

   					$src_path = "zip/".$user_id."/".$skufolder; 
   					$des_path = "zip/".$user_id."/".$newskufolder; 

   					rename($src_path,$des_path);

   					$sku_str.=$newskufolder.",";

   				}

   			}
   		}
   	}



   	echo $sku_str;





   }









   function deleteCancelSKUForExistZip()
   {

   	$session_data = $this->session->userdata('front_logged_in');
   	$data['user_id'] = $session_data['user_id'];
   	$data['user_type_id'] = $session_data['user_type_id'];
   	$data['user_name'] = $session_data['user_name'];
   	$user_name = $session_data['user_name'];
   	$user_id=$data['user_id'];


   	$file_nm = $this->input->post('file_nm');



   	$dir = "zip/".$user_id."/";
   	$sku_str="";
    // Open a directory, and read its contents
   	if (is_dir($dir)){
   		if ($dh = opendir($dir)){
   			while (($file = readdir($dh)) !== false){

   				if($file!='.' && $file!='..')
   				{
                //echo "filename:" . $file . "<br>";
   					$skufolder=$file;

   					$src = "zip/".$user_id."/".$skufolder; 
   					$this->delete_directory($src); 

   				}

   			}

   		}

   	}


   	echo "success";


   } 

}

?>