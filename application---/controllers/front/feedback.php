<?php 
class Feedback extends CI_Controller{
    
    function __construct(){
        parent::__construct();
        $this->load->library(['session']); 
        $this->load->helper(['url','file','form']); 
        $this->load->model('uploadbrief_model'); //load model upload 
        $this->load->model('Dashboard_model'); //load model upload 
		$this->load->model('Feedback_model'); //load model upload
        $this->load->model('home_model'); //load model upload
        $this->load->library('upload'); //load library upload 
        $this->load->library('email');
    }

    public function index($brief_id){

        if ($this->session->userdata('front_logged_in')) {
        $session_data = $this->session->userdata('front_logged_in');
        $data['user_id'] = $session_data['user_id'];
        $data['user_name'] = $session_data['user_name'];
		$type1 = $session_data['user_type_id'];
		
		$data['brief_id'] = $brief_id;
        $id='2';
        $data['pagecontents'] = $this->home_model->get_page($id);
		
        $data['title'] = "upload images";
        $this->load->view('front/feedback',$data);    
        }
        else
        {
        $data['user_id'] = '';
        $data['user_name'] = '';
        $user_id='';
        redirect('login', 'refresh');
        }       
    }
    
     public function feedback_thankyou(){

       
        
        $data['title'] = "Feedback Thankyou";
        $this->load->view('front/feedback_thankyou',$data);    
          
    }

     public function feedback_submit($unique_feedback_id){

        $unique_feedback_id_str=base64_decode($unique_feedback_id);
        $unique_feedback_id_arr=explode("-",$unique_feedback_id_str);

        $time=$unique_feedback_id_arr[0];
        $brief_id=$unique_feedback_id_arr[1];
        $user_id=$unique_feedback_id_arr[2];


        $checkquerys = $this->db->query("select * from wc_users where  user_id='$user_id' ")->result_array();
        $user_id=$checkquerys[0]['user_id'];
        $user_email=$checkquerys[0]['user_email'];
        $user_name=$checkquerys[0]['user_name'];
        $user_type_id=$checkquerys[0]['user_type_id'];


        
        
         $data['user_id'] = $user_id;
        $data['user_name'] = $user_name;
        $data['type1'] = $user_type_id;
        $data['page_type'] = 'feedback_submit';
        




        
        $data['brief_id'] = $brief_id;
        $id='2';
        $data['pagecontents'] = $this->home_model->get_page($id);
        
        $data['title'] = "upload images";
        $this->load->view('front/feedback',$data);    
        
    }



public function insertFeedback(){
        /*if ($this->session->userdata('front_logged_in')) {
        $session_data = $this->session->userdata('front_logged_in');
        $data['user_id'] = $session_data['user_id'];
        $data['user_type_id'] = $session_data['user_type_id'];
        $data['user_name'] = $session_data['user_name'];
        $user_name = $session_data['user_name'];
        $user_id=$data['user_id'];
        $folderName=$data['user_id'];*/

        /*$arr=$this->input->post();


        echo "<pre>";
        print_r($arr);exit;*/
        $user_id = $this->input->post('user_id');
        $unique_id=time().rand().$user_id;
        $brief_id = $this->input->post('brief_id');
        $feedback_type_id_arr = $this->input->post('feedback_type_id');
        $review_arr = $this->input->post('review');
        /*$review_reason_arr = $this->input->post('review_reason');*/

        $review_reason = $this->input->post('review_reason');


        

        $checkquerys = $this->db->query("select * from wc_users where  user_id='$user_id' ")->result_array();
        $brand_id=$checkquerys[0]['brand_id'];

       foreach($feedback_type_id_arr as $key => $value)
       {
        $feedback_type_id=$value;
        $review=$review_arr[$key];
        //$review_reason=$review_reason_arr[$key];

        $data = array(
        'unique_id'=>$unique_id,
        'brief_id'=>$brief_id,
        'brand_id'=>$brand_id,
        'user_id'=>$user_id,
        'feedback_type_id'=>$feedback_type_id,
        'feedback_rating'=>$review,
        'feedback_reason'=>$review_reason,
        'feedback_datetime'=>date("Y-m-d H:i:s")
        );
        $this->Feedback_model->feedback_insert($data);
        $feedback_id=$this->db->insert_id();


       }
        

        

        $this->db->query("update wc_brief set status ='7' where brief_id ='$brief_id'");



        ///////////////////////////////////////////////////////////////////////////
        $checkquerys = $this->db->query("select * from wc_brief where  brief_id='$brief_id' ")->result_array();
        $brand_id=$checkquerys[0]['brand_id'];
        $added_by_user_id=$checkquerys[0]['added_by_user_id'];
        $brief_title=$checkquerys[0]['brief_title'];
        $brand_id=$checkquerys[0]['brand_id'];

        $checkquerys = $this->db->query("select * from wc_users where  user_id='$added_by_user_id' ")->result_array();
        $user_id=$checkquerys[0]['user_id'];
        $user_email=$checkquerys[0]['user_email'];
        $user_name=$checkquerys[0]['user_name'];
        $client_id=$checkquerys[0]['client_id'];



        if($brand_id!='')
        {
            $checkquerys_mail = $this->db->query("select * from wc_users where  client_id='$client_id' and brand_id like '%,".$brand_id.",%' and user_type_id='4' ")->result_array();
            if(!empty($checkquerys_mail)){
                foreach($checkquerys_mail as $key => $value){
                    $to_email_all_bm[]=$value['user_email'];
                }
            }
        }

        $checkquerys_clientmail= $this->db->query("SELECT * FROM `wc_clients` WHERE `client_id`='$client_id'")->result_array();
        $client_email=$checkquerys_clientmail[0]['client_email'];
        array_push($to_email_all_bm,$client_email);



        /*********************************Send mail*************************************/

        
        $query=$this->db->query("select * from  wc_settings  WHERE setting_key='MAIL_SMTP_USER' ");
        $userDetails= $query->result_array();
        $MAIL_SMTP_USER=$userDetails[0]['setting_value'];

        $query=$this->db->query("select * from  wc_settings  WHERE setting_key='MAIL_FROM' ");
        $userDetails= $query->result_array();
        $MAIL_FROM=$userDetails[0]['setting_value'];


        $query=$this->db->query("select * from  wc_settings  WHERE setting_key='MAIL_FROM_TEXT' ");
        $userDetails= $query->result_array();
        $MAIL_FROM_TEXT=$userDetails[0]['setting_value'];

        $query=$this->db->query("select * from  wc_settings  WHERE setting_key='MAIL_SMTP_PASSWORD' ");
        $userDetails= $query->result_array();
        $MAIL_SMTP_PASSWORD=$userDetails[0]['setting_value'];

        $query=$this->db->query("select * from  wc_settings  WHERE setting_key='MAIL_BODY_FEEDBACK_SUBMITTED' ");
        $userDetails= $query->result_array();
        $MAIL_BODY=$userDetails[0]['setting_value'];


        $uname=$user_name;
        $uemail=$user_email;
        $subject=$MAIL_FROM_TEXT.' - Feedback Submitted(#'.$brief_id.')';

        //$unique_feedback_id=time()."-".$brief_id."-".$user_id;
        //$feedback_link=base_url()."feedback-submit/".base64_encode($unique_feedback_id);



        $uploaded_images="";


        $userQuery = "select * from wc_image_upload where  brief_id='$brief_id' and version_num='1' limit 5";
        $userResult = $this->db->query($userQuery)->result_array();

        
        if(!empty($userResult)){ 
            foreach ($userResult as $keyuserRow => $userRow) {

                $image_id=$userRow['image_id'];
                $version_num=$userRow['version_num'];
                $parent_image_id=$userRow['parent_img'];
                $image_path=$userRow['image_path'];

                $checkquerys = $this->db->query("select * from wc_image_upload where brief_id='$brief_id' and parent_img='$parent_image_id' order by version_num desc ")->result_array();
                if(!empty($checkquerys)){
                    $version_num=$checkquerys[0]['version_num'];
                    $image_path=$checkquerys[0]['image_path'];
                }

                $checkquerys = $this->db->query("select * from wc_brief where  brief_id='$brief_id' ")->result_array();
                $added_by_user_id=$checkquerys[0]['added_by_user_id'];
                $brand_id=$checkquerys[0]['brand_id'];
                $brief_title=$checkquerys[0]['brief_title'];
                $brief_name=preg_replace('/[^a-zA-Z0-9_.]/', '_',$brief_title);

                $checkquerys = $this->db->query("select wc_brands.*,wc_clients.client_name from wc_brands
                inner join wc_clients on wc_brands.client_id=wc_clients.client_id
                where wc_brands.brand_id='$brand_id' ")->result_array();

                $brand_id=$checkquerys[0]['brand_id'];
                $brand_name=preg_replace('/[^a-zA-Z0-9_.]/', '_',$checkquerys[0]['brand_name']);
                $client_id=$checkquerys[0]['client_id'];
                $client_name=preg_replace('/[^a-zA-Z0-9_.]/', '_',$checkquerys[0]['client_name']);
                $client_folder=$client_id."-".$client_name;
                $brand_folder=$brand_id."-".$brand_name;
                $brief_folder=$brief_id."-".$brief_name;

                $brief_path=BASE_URL."upload/".$client_folder."/".$brand_folder."/".$brief_folder; 
                $rev_folder="Revision".$version_num;
                $image_thumb_path=$brief_path."/".$parent_image_id."/".$rev_folder."/thumb_".$image_path;
                $image_org=$brief_path."/".$image_id."/".$rev_folder."/".$image_path;
                $ext = pathinfo($image_org, PATHINFO_EXTENSION);    
                $image_name= explode(".", $image_path);

                
                $img_thumb_path = "./upload/".$client_folder."/".$brand_folder."/".$brief_folder."/".$parent_image_id."/".$rev_folder."/thumb_".$image_path;

                list($img_width, $img_height) = getimagesize($img_thumb_path);
                // echo $img_width . '-width, height-' . $img_height;

                if($img_width > $img_height) {
                    $style = "width: 150px; height: auto;";
                    $attr = "width='150' height='auto'";
                }
                else if($img_width < $img_height) {
                    $style = "width: auto; height: 150px;";
                    $attr = "width='auto' height='150'";
                } else if($img_width == $img_height) {
                    $style = "width: 150px; height: 150px;";
                    $attr = "width='150' height='150'";             
                } else {
                    $style = "width: 150px; height: 150px;";
                    $attr = "width='150' height='150'";
                }
                // exit();
                    

                $uploaded_images.='<div style="height: 150px; width: 150px; border: 1px solid #5bad9a69; margin-bottom: 20px; overflow: hidden; display: table;"> <div style="width: 100%; height: 100%; display: table-cell; vertical-align: middle;"> <img align="none" alt="'.$image_name[0].'" src="'.$image_thumb_path.'" style="-ms-interpolation-mode: bicubic; border: 0; outline: none; text-decoration: none; '.$style.'" '.$attr.'> </div> </div>';

            }
    
        }
       
        $feedback_details="";
        $feedback_type_sql = "SELECT * FROM `wc_feedback_type` where status='Active' and deleted='0'  ";

        $feedback_type_query = $this->db->query($feedback_type_sql);
        $feedback_type_result=$feedback_type_query->result_array();

        if(!empty($feedback_type_result))
        {
            foreach($feedback_type_result as $key => $value)
            {
                $feedback_type_id=$value['feedback_type_id'];
                $feedback_type=$value['feedback_type'];

                $feedback_sql = "SELECT * FROM `wc_feedback` where  brief_id='$brief_id' and feedback_type_id='$feedback_type_id'  ";

                $feedback_query = $this->db->query($feedback_sql);
                $feedback_result=$feedback_query->result_array();
                $feedback_rating=$feedback_result[0]['feedback_rating'];

                if($feedback_rating==1){ $icon_img1= BASE_URL."/website-assets/icons/angry.png"; }else{ $icon_img1=BASE_URL."/website-assets/icons/angry-blur.png"; }
                if($feedback_rating==2){ $icon_img2= BASE_URL."/website-assets/icons/frown.png"; }else{ $icon_img2=BASE_URL."/website-assets/icons/frown-blur.png"; }
                if($feedback_rating==3){ $icon_img3= BASE_URL."/website-assets/icons/meh.png"; }else{ $icon_img3=BASE_URL."/website-assets/icons/meh-blur.png"; }
                if($feedback_rating==4){ $icon_img4= BASE_URL."/website-assets/icons/smile.png"; }else{ $icon_img4=BASE_URL."/website-assets/icons/smile-blur.png"; }
                if($feedback_rating==5){ $icon_img5= BASE_URL."/website-assets/icons/laugh.png"; }else{ $icon_img5=BASE_URL."/website-assets/icons/laugh-blur.png"; }
                $feedback_details.="
                <tr>
                  <td align='left' class='mcnButtonContent' style='mso-line-height-rule: exactly; -ms-text-size-adjust: 80%; -webkit-text-size-adjust: 80%; font-family: 'Asap', Helvetica, sans-serif; font-size: 16px; padding: 0 0 2px 50px;' valign='middle'>".$feedback_type."
                  </td>
                  <td align='center' class='mcnButtonContent' style='mso-line-height-rule: exactly; -ms-text-size-adjust: 20%; -webkit-text-size-adjust: 20%; font-family: 'Asap', Helvetica, sans-serif; font-size: 18px; padding: 0 40px 2px 10px;' valign='middle'>
                   
                    <span><img src='".$icon_img1."' style='-ms-interpolation-mode: bicubic; border: 0; outline: none; text-decoration: none; height: auto; width: 30px;  margin: 0px;' width='30' height='30'></span>
                    <span><img src='".$icon_img2."' style='-ms-interpolation-mode: bicubic; border: 0; outline: none; text-decoration: none; height: auto; width: 30px;  margin: 0px;' width='30' height='30'></span>
                    <span><img src='".$icon_img3."' style='-ms-interpolation-mode: bicubic; border: 0; outline: none; text-decoration: none; height: auto; width: 30px;  margin: 0px;' width='30' height='30'></span>
                    <span><img src='".$icon_img4."' style='-ms-interpolation-mode: bicubic; border: 0; outline: none; text-decoration: none; height: auto; width: 30px;  margin: 0px;' width='30' height='30'></span>
                    <span><img src='".$icon_img5."' style='-ms-interpolation-mode: bicubic; border: 0; outline: none; text-decoration: none; height: auto; width: 30px;  margin: 0px;' width='30' height='30'></span>
                    
                  </td>
                </tr>";
            }
        }
                           
                        
       // echo $uploaded_images; exit();
        $website_url=base_url();
        $dashboard_url=base_url()."dashboard";             

        //$from_email = "demo.intexom@gmail.com";
        $from_email = $MAIL_FROM;
        //$to_email = $uemail;
        $to_email = $to_email_all_bm;

        $date_time=date("d/m/y");
        /*$copy_right=$MAIL_FROM_TEXT." - ".date("Y");*/
        $copy_right=$MAIL_FROM_TEXT;
        $feedback_link=$dashboard_url;
        $project_name="#".$brief_id." - ".$brief_title;
        //echo $message=$MAIL_BODY;
        $find_arr = array("##WEBSITE_URL##","##PROJECT_NAME##","##DASHBOARD_LINK##","##COPY_RIGHT##","##FEEDBACK_SUBMIT_LINK##","##UPLOADED_IMAGES##","##FEEDBACK_DETAILS##");
        $replace_arr = array($website_url,$project_name,$dashboard_url,$copy_right,$feedback_link,$uploaded_images,$feedback_details);
        $message=str_replace($find_arr,$replace_arr,$MAIL_BODY);

        //echo $message;exit;

        //Load email library
        $this->load->library('email');

        $config = array();
        //$config['protocol'] = 'smtp';
        $config['protocol']     = 'mail';
        $config['smtp_host'] = 'localhost';
        $config['smtp_user'] = $MAIL_SMTP_USER;
        $config['smtp_pass'] = $MAIL_SMTP_PASSWORD;
        $config['smtp_port'] = 25;

        $this->email->initialize($config);
        $this->email->set_newline("\r\n");
        $this->email->from($from_email,$MAIL_FROM_TEXT);
        $this->email->to($to_email);
        if($_SERVER['HTTP_HOST']!='collab.piquic.com'){
            $this->email->cc(array('poulami@mokshaproductions.in','pranav@mokshaproductions.in','raja.priya@mokshaproductions.in','rajendra.prasad@mokshaproductions.in'));
                        
        }
        $this->email->subject($subject);
        $this->email->message($message);
        //Send mail
        if($this->email->send()){
            $this->session->set_flashdata("email_sent","Congragulation Email Send Successfully.");
            echo "success";
        }
        else{
            $this->session->set_flashdata("email_sent","You have encountered an error");
        }
        /////////////////////////////////////Mail send end//////////////////////////////////////////////
       // 




    //}

}








	
	public function getfilessort()
	{
	 $brief_id=$_POST['brief_id'];
     $image_status=$_POST['status'];	
	 $data['viewfilesinfo'] = $this->Viewfiles_model->viewfilessearch($image_status,$brief_id);	
     $this->load->view('front/viewfiles_search',$data);
	
		
		
	}
	
	public function changefilestatus()
	{
	  $brief_id=$_POST['brief_id'];	
	 $image_id=$_POST['image_id'];
     $status=$_POST['status'];	
	 $data['viewfilesinfo'] = $this->Viewfiles_model->changefilestatus($image_id,$status,$brief_id);
	 $this->load->view('front/viewfiles_search',$data);
	
		
	}



    public function sendMailForFeedback($brief_id)
    {


    // $brief_id=$_POST['brief_id'];



     $this->db->query("update wc_brief set status ='5' where brief_id ='$brief_id'");
        
     $checkquerys = $this->db->query("select * from wc_brief where  brief_id='$brief_id' ")->result_array();
     $brand_id=$checkquerys[0]['brand_id'];
     $added_by_user_id=$checkquerys[0]['added_by_user_id'];


     $checkquerys = $this->db->query("select * from wc_users where  user_id='$added_by_user_id' ")->result_array();
     $user_id=$checkquerys[0]['user_id'];
     $user_email=$checkquerys[0]['user_email'];
     $user_name=$checkquerys[0]['user_name'];
     $client_id=$checkquerys[0]['client_id'];



/*********************************Send mail*************************************/

    $query=$this->db->query("select * from  wc_settings  WHERE setting_key='MAIL_SMTP_USER' ");
    $userDetails= $query->result_array();
    $MAIL_SMTP_USER=$userDetails[0]['setting_value'];

    $query=$this->db->query("select * from  wc_settings  WHERE setting_key='MAIL_SMTP_PASSWORD' ");
    $userDetails= $query->result_array();
    $MAIL_SMTP_PASSWORD=$userDetails[0]['setting_value'];

    $query=$this->db->query("select * from  wc_settings  WHERE setting_key='MAIL_BODY_FEEDBACK' ");
    $userDetails= $query->result_array();
    $MAIL_BODY=$userDetails[0]['setting_value'];


    $query=$this->db->query("select * from  wc_settings  WHERE setting_key='MAIL_FROM_TEXT' ");
    $userDetails= $query->result_array();
    $MAIL_FROM_TEXT=$userDetails[0]['setting_value'];

    $query=$this->db->query("select * from  wc_settings  WHERE setting_key='MAIL_SMTP_PASSWORD' ");
    $userDetails= $query->result_array();
    $MAIL_SMTP_PASSWORD=$userDetails[0]['setting_value'];


    $uname=$user_name;
    $uemail=$user_email;
    //$subject=$MAIL_FROM_TEXT.' - Feedback mail';
    $subject=$MAIL_FROM_TEXT.' - Feedback Mail(#'.$brief_id.')';

    $unique_feedback_id=time()."-".$brief_id."-".$user_id;
    $feedback_link=base_url()."feedback-submit/".base64_encode($unique_feedback_id);

        
        //Load email library
        $this->load->library('email');

        $config = array();
        //$config['protocol'] = 'smtp';
        $config['protocol']     = 'mail';
        $config['smtp_host'] = 'localhost';
        $config['smtp_user'] = $MAIL_SMTP_USER;
        $config['smtp_pass'] = $MAIL_SMTP_PASSWORD;
        $config['smtp_port'] = 25;




        $this->email->initialize($config);
        $this->email->set_newline("\r\n");

        if($client_id!='') {
                    $checkquerys = $this->db->query("select * from wc_clients where client_id='$client_id' ")->result_array();

                    $client_image=$checkquerys[0]['client_image'];
                    $wekan_client_id=$checkquerys[0]['wekan_client_id'];

                    $client_logo=base_url()."/upload_client_logo/".$client_image;
                    }
                    else
                    {
                    $client_logo=base_url()."/website-assets/images/logo.png";
                    }
                    //$client_logo="http://style.piquic.com/WeCreate//website-assets/images/logo.png";


             $website_url=base_url();
                     $dashboard_url=base_url()."dashboard";         

        //$from_email = "demo.intexom@gmail.com";
       $from_email = $MAIL_FROM;
        $to_email = $uemail;

        $date_time=date("d/m/y");
        //$copy_right=$MAIL_FROM_TEXT." - ".date("Y");
         $copy_right=$MAIL_FROM_TEXT;
         $feedback_link="";
         $project_name="#".$brief_id." - ".$brief_title;
        //echo $message=$MAIL_BODY;
        /*$find_arr = array("##CLIENT_LOGO##","##BM_NAME##","##FEEDBACK_SUBMIT_LINK##","##COPY_RIGHT##");
        $replace_arr = array($client_logo,$uname,$feedback_link,$copy_right);*/

        $find_arr = array("##WEBSITE_URL##","##PROJECT_NAME##","##DASHBOARD_LINK##","##COPY_RIGHT##","##FEEDBACK_SUBMIT_LINK##");
        $replace_arr = array($website_url,$project_name,$dashboard_url,$copy_right,$feedback_link);

        $message=str_replace($find_arr,$replace_arr,$MAIL_BODY);

        //echo $message;exit;



        $this->email->from($from_email,$MAIL_FROM_TEXT);
        $this->email->to($to_email);
        if($_SERVER['HTTP_HOST']!='collab.piquic.com')
                    {
                        $this->email->cc(array('poulami@mokshaproductions.in','pranav@mokshaproductions.in','raja.priya@mokshaproductions.in','rajendra.prasad@mokshaproductions.in'));
                    }
        $this->email->subject($subject);
        $this->email->message($message);
        //Send mail
        if($this->email->send())
        {

            //echo "mail sent";exit;
            $this->session->set_flashdata("email_sent","Congratulation Email Send Successfully.");
        }
        
        else
        {
            //echo $this->email->print_debugger();
            //echo "mail not sent";exit;
            $this->session->set_flashdata("email_sent","You have encountered an error");
        }
        

    //exit;

/************************************************************************/





    
        
        
    }


    function getFeedbackContent()
    {


         $feedback_rating_id = $this->input->post('feedback_rating_id');
          $feedback_rating = $this->input->post('feedback_rating');
           $feedback_type_id = $this->input->post('feedback_type_id');


            $feedback_rating_sql = "SELECT * FROM `wc_feedback_content` where feedback_type_id='$feedback_type_id' and feedback_rating='$feedback_rating' and deleted ='0'  order by feedback_content_id asc ";

                                    $feedback_rating_query = $this->db->query($feedback_rating_sql);
                                    $feedback_rating_result=$feedback_rating_query->result_array();


                                    if(!empty($feedback_rating_result))
                                    {
                                    foreach($feedback_rating_result as $key => $value)
                                    {
                                    $feedback_content_id=$value['feedback_content_id'];
                                    $feedback_type_id=$value['feedback_type_id'];
                                    $feedback_content=$value['feedback_content'];
                                    

                                    ?>
                                    <span id="qty_<?php echo $feedback_content_id;?>" class="btn btn-outline-wc qty_btn_<?php echo $feedback_type_id;?> " onclick="setReviewReason('<?php echo $feedback_content;?>','<?php echo $feedback_type_id;?>','<?php echo $feedback_content_id;?>');"><?php echo $feedback_content;?></span>

                                    <?php
                                    }
                                    }
    }
    
	

}

?>