<?php
class Myaccount extends CI_Controller {

	public function __construct() {
		parent::__construct();
		$this->load->library(['session']); 
		$this->load->model('myAccount_model');
		$this->load->model('User_model');
		$this->load->helper('url_helper');
	}

	public function index(){


		if ($this->session->userdata('front_logged_in')) {
            $session_data = $this->session->userdata('front_logged_in');
            $data['user_id'] = $session_data['user_id'];
            $data['user_name'] = $session_data['user_name'];
            $user_id = $session_data['user_id'];
            $this->session->set_userdata('previous_url', '');

            $data['user_details'] = $this->myAccount_model->get_my_data($data['user_id']);

			$this->load->view('front/myaccount', $data);		
         
        } else {
        	$this->session->set_userdata('previous_url', current_url());
        	redirect('/');
        }
		
	}



public function insert_member_wekan($client_id,$user_type_id,$user_name,$user_email,$user_password){


    $query=$this->db->query("select * from wc_settings WHERE setting_key='WEKAN_LINK' ");
    $userDetails= $query->result_array();
    $WEKAN_LINK=$userDetails[0]['setting_value'];

    $query=$this->db->query("select * from wc_settings WHERE setting_key='WEKAN_USERNAME' ");
    $userDetails= $query->result_array();
    $WEKAN_USERNAME=$userDetails[0]['setting_value'];


    $query=$this->db->query("select * from wc_settings WHERE setting_key='WEKAN_PASSWORD' ");
    $userDetails= $query->result_array();
    $WEKAN_PASSWORD=$userDetails[0]['setting_value'];

    

    if ($this->session->userdata('wekan_integration')) {



        $wekan_session_data = $this->session->userdata('wekan_integration');
        $wekan_authorId = $wekan_session_data['wekan_authorId'];
        $wekan_token = $wekan_session_data['wekan_token'];
        $wekan_tokenExpires = $wekan_session_data['wekan_tokenExpires'];

         //echo "<pre>";
        //print_r($wekan_session_data);exit;
    }
    else
    {

        $curl = curl_init();
        curl_setopt_array($curl, array(
        CURLOPT_URL => $WEKAN_LINK."users/login",
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_ENCODING => "",
        CURLOPT_MAXREDIRS => 10,
        CURLOPT_TIMEOUT => 30,
        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
        CURLOPT_CUSTOMREQUEST => "POST",
        CURLOPT_POSTFIELDS => "username=".$WEKAN_USERNAME."&password=".$WEKAN_PASSWORD."",
        CURLOPT_HTTPHEADER => array(
        "cache-control: no-cache",
        "content-type: application/x-www-form-urlencoded"
        ),
        ));
        $response = curl_exec($curl);
        $err = curl_error($curl);

        curl_close($curl);

        if (!$err)
        {
        //var_dump($response);



        $res_arr=json_decode($response);

        //echo "<pre>";
        //print_r($res_arr);exit;
        $id=$res_arr->id;
        $token=$res_arr->token;
        $tokenExpires=$res_arr->tokenExpires;


        $sess_array = array(
        'wekan_authorId' => $id,
        'wekan_token' => $token,
        'wekan_tokenExpires' => $tokenExpires
        );
        $this->session->set_userdata('wekan_integration', $sess_array);


        $wekan_session_data = $this->session->userdata('wekan_integration');
        $wekan_authorId = $wekan_session_data['wekan_authorId'];
        $wekan_token = $wekan_session_data['wekan_token'];
        $wekan_tokenExpires = $wekan_session_data['wekan_tokenExpires'];

       

        }

    }


   // echo $client_id,$user_name,$user_email,$user_password;exit;

      //////////////////////////////////////////insert member////////////////////////////////

    $user_name_arr=explode("@",$user_email);
    $user_name=$user_name_arr[0];

    $ch = curl_init();

    curl_setopt($ch, CURLOPT_URL,$WEKAN_LINK."api/users");
    curl_setopt($ch, CURLOPT_HTTPHEADER, array(
    'Content-Type: application/x-www-form-urlencoded',
    'Authorization: Bearer ' . $wekan_token
    ));
    curl_setopt($ch, CURLOPT_POST, 1);
    curl_setopt($ch, CURLOPT_POSTFIELDS,
    "username=".$user_email."&email=".$user_email."&password=".$user_password);

    // In real life you should use something like:
    // curl_setopt($ch, CURLOPT_POSTFIELDS, 
    //          http_build_query(array('postvar1' => 'value1')));

    // Receive server response ...
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

    $server_output = curl_exec($ch);

    curl_close ($ch);
	$id_arr=json_decode($server_output);
	//echo "<pre>";
    //print_r($id_arr);exit;

    if(isset($id_arr->_id))
    {
    	$wekan_member_id=$id_arr->_id;

    }
    else
    {
    	$wekan_member_id="0";
    }
    
	



  //////////////////////////////////////////assign member to board///////////////////////////////



    $wekan_member_assign_id="";
    $user_details=$this->db->query("select * from wc_clients where client_id='$client_id'")->result_array();
	$wekan_client_id = $user_details[0]['wekan_client_id'];

	if($wekan_client_id!='')
	{


			$isAdmin=true;
			$isNoComments=true;
			$isCommentOnly=true;
			$action="add";

			/*if($user_type_id=='1' && $user_type_id=='2' && $user_type_id=='3')
			{

			$isAdmin=true;
			$isNoComments=true;
			$isCommentOnly=true;

			}
			elseif($user_type_id=='4')
			{

			$isAdmin=true;
			$isNoComments=true;
			$isCommentOnly=true;

			}*/


			$ch = curl_init();

			//echo $WEKAN_LINK."api/boards/".$wekan_client_id."/members/".$wekan_member_id."/add";

			curl_setopt($ch, CURLOPT_URL,$WEKAN_LINK."api/boards/".$wekan_client_id."/members/".$wekan_member_id."/add");
			curl_setopt($ch, CURLOPT_HTTPHEADER, array(
			'Content-Type: application/x-www-form-urlencoded',
			'Authorization: Bearer ' . $wekan_token
			));
			curl_setopt($ch, CURLOPT_POST, 1);
			/*curl_setopt($ch, CURLOPT_POSTFIELDS,
			"action=".$action."&isAdmin=".$isAdmin."&isNoComments=".$isNoComments."&isCommentOnly=".$isCommentOnly);*/

			curl_setopt($ch, CURLOPT_POSTFIELDS,
			"action=add&isAdmin=true&isNoComments=true&isCommentOnly=true");

			// In real life you should use something like:
			// curl_setopt($ch, CURLOPT_POSTFIELDS, 
			//          http_build_query(array('postvar1' => 'value1')));

			// Receive server response ...
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

			$server_output = curl_exec($ch);

			curl_close ($ch);
			$id_arr=json_decode($server_output);
			//echo "<pre>";
			//print_r($id_arr);exit;

			if(isset($id_arr->_id))
			{
			$wekan_member_assign_id=$id_arr->_id;
			$wekan_member_assign_title=$id_arr->title;
            }
			
			
             if($user_type_id!=4)
             {

             	$curl = curl_init();

				curl_setopt_array($curl, array(
				CURLOPT_URL => $WEKAN_LINK."/api/boards/".$wekan_client_id."/members/".$wekan_member_id."",
				CURLOPT_RETURNTRANSFER => true,
				CURLOPT_ENCODING => "",
				CURLOPT_MAXREDIRS => 10,
				CURLOPT_TIMEOUT => 0,
				CURLOPT_FOLLOWLOCATION => true,
				CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
				CURLOPT_CUSTOMREQUEST => "POST",
				 CURLOPT_POSTFIELDS => "isAdmin=true&isNoComments=true&isCommentOnly=true",
				CURLOPT_HTTPHEADER => array(
				"Accept: application/json",
				"Content-Type: application/x-www-form-urlencoded",
				"Authorization: Bearer " . $wekan_token,
				"Content-Type: application/x-www-form-urlencoded"
				),
				));

				$response = curl_exec($curl);

				curl_close($curl);
				//echo $response;



             }


				






	}

    


    return $wekan_member_id;

    

}

public function update_member_wekan($wekan_member_id,$client_id,$user_type_id,$user_name,$user_email,$user_password){


    $query=$this->db->query("select * from wc_settings WHERE setting_key='WEKAN_LINK' ");
    $userDetails= $query->result_array();
    $WEKAN_LINK=$userDetails[0]['setting_value'];

    $query=$this->db->query("select * from wc_settings WHERE setting_key='WEKAN_USERNAME' ");
    $userDetails= $query->result_array();
    $WEKAN_USERNAME=$userDetails[0]['setting_value'];


    $query=$this->db->query("select * from wc_settings WHERE setting_key='WEKAN_PASSWORD' ");
    $userDetails= $query->result_array();
    $WEKAN_PASSWORD=$userDetails[0]['setting_value'];

    

    if ($this->session->userdata('wekan_integration')) {



        $wekan_session_data = $this->session->userdata('wekan_integration');
        $wekan_authorId = $wekan_session_data['wekan_authorId'];
        $wekan_token = $wekan_session_data['wekan_token'];
        $wekan_tokenExpires = $wekan_session_data['wekan_tokenExpires'];

         //echo "<pre>";
        //print_r($wekan_session_data);exit;
    }
    else
    {

        $curl = curl_init();
        curl_setopt_array($curl, array(
        CURLOPT_URL => $WEKAN_LINK."users/login",
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_ENCODING => "",
        CURLOPT_MAXREDIRS => 10,
        CURLOPT_TIMEOUT => 30,
        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
        CURLOPT_CUSTOMREQUEST => "POST",
        CURLOPT_POSTFIELDS => "username=".$WEKAN_USERNAME."&password=".$WEKAN_PASSWORD."",
        CURLOPT_HTTPHEADER => array(
        "cache-control: no-cache",
        "content-type: application/x-www-form-urlencoded"
        ),
        ));
        $response = curl_exec($curl);
        $err = curl_error($curl);

        curl_close($curl);

        if (!$err)
        {
        //var_dump($response);



        $res_arr=json_decode($response);

        //echo "<pre>";
        //print_r($res_arr);exit;
        $id=$res_arr->id;
        $token=$res_arr->token;
        $tokenExpires=$res_arr->tokenExpires;


        $sess_array = array(
        'wekan_authorId' => $id,
        'wekan_token' => $token,
        'wekan_tokenExpires' => $tokenExpires
        );
        $this->session->set_userdata('wekan_integration', $sess_array);


        $wekan_session_data = $this->session->userdata('wekan_integration');
        $wekan_authorId = $wekan_session_data['wekan_authorId'];
        $wekan_token = $wekan_session_data['wekan_token'];
        $wekan_tokenExpires = $wekan_session_data['wekan_tokenExpires'];

       

        }

    }


    /////////////////////////////////////////////////////////////////////////////////

    if($wekan_member_id!='')
    {

			

			$curl = curl_init();

			curl_setopt_array($curl, array(
			CURLOPT_URL => $WEKAN_LINK."api/users/".$wekan_member_id,
			CURLOPT_RETURNTRANSFER => true,
			CURLOPT_ENCODING => "",
			CURLOPT_MAXREDIRS => 10,
			CURLOPT_TIMEOUT => 0,
			CURLOPT_FOLLOWLOCATION => true,
			CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
			CURLOPT_CUSTOMREQUEST => "DELETE",
			CURLOPT_HTTPHEADER => array(
			"Accept: application/json",
			"Authorization: Bearer ".$wekan_token
			),
			));

			$response = curl_exec($curl);

			curl_close($curl);
			//echo $response;

    }



   // echo $client_id,$user_name,$user_email,$user_password;exit;

      //////////////////////////////////////////insert member////////////////////////////////

    $user_name_arr=explode("@",$user_email);
    $user_name=$user_name_arr[0];

    $ch = curl_init();

    curl_setopt($ch, CURLOPT_URL,$WEKAN_LINK."api/users");
    curl_setopt($ch, CURLOPT_HTTPHEADER, array(
    'Content-Type: application/x-www-form-urlencoded',
    'Authorization: Bearer ' . $wekan_token
    ));
    curl_setopt($ch, CURLOPT_POST, 1);
    curl_setopt($ch, CURLOPT_POSTFIELDS,
    "username=".$user_email."&email=".$user_email."&password=".$user_password);

    // In real life you should use something like:
    // curl_setopt($ch, CURLOPT_POSTFIELDS, 
    //          http_build_query(array('postvar1' => 'value1')));

    // Receive server response ...
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

    $server_output = curl_exec($ch);

    curl_close ($ch);
	$id_arr=json_decode($server_output);
	//echo "<pre>";
    //print_r($id_arr);exit;
	

	if(isset($id_arr->_id))
    {
    	$wekan_member_id=$id_arr->_id;

    }
    else
    {
    	$wekan_member_id="0";
    }



  //////////////////////////////////////////assign member to board///////////////////////////////




    $user_details=$this->db->query("select * from wc_clients where client_id='$client_id'")->result_array();
	$wekan_client_id = $user_details[0]['wekan_client_id'];

	if($wekan_client_id!='')
	{


			$isAdmin=true;
			$isNoComments=true;
			$isCommentOnly=true;
			$action="add";

			/*if($user_type_id=='1' && $user_type_id=='2' && $user_type_id=='3')
			{

			$isAdmin=true;
			$isNoComments=true;
			$isCommentOnly=true;

			}
			elseif($user_type_id=='4')
			{

			$isAdmin=false;
			$isNoComments=true;
			$isCommentOnly=true;

			}*/


			$ch = curl_init();

			curl_setopt($ch, CURLOPT_URL,$WEKAN_LINK."api/boards/".$wekan_client_id."/members/".$wekan_member_id."/add");
			curl_setopt($ch, CURLOPT_HTTPHEADER, array(
			'Content-Type: application/x-www-form-urlencoded',
			'Authorization: Bearer ' . $wekan_token
			));
			curl_setopt($ch, CURLOPT_POST, 1);
			curl_setopt($ch, CURLOPT_POSTFIELDS,
			"action=".$action."&isAdmin=".$isAdmin."&isNoComments=".$isNoComments."&isCommentOnly=".$isCommentOnly);

			// In real life you should use something like:
			// curl_setopt($ch, CURLOPT_POSTFIELDS, 
			//          http_build_query(array('postvar1' => 'value1')));

			// Receive server response ...
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

			$server_output = curl_exec($ch);

			curl_close ($ch);
			$id_arr=json_decode($server_output);
			//echo "<pre>";
			//print_r($id_arr);exit;

			if(isset($id_arr->_id))
			{
			$wekan_member_assign_id=$id_arr->_id;
			$wekan_member_assign_title=$id_arr->title;

			}
			




			if($user_type_id!=4)
			{

			$curl = curl_init();

			curl_setopt_array($curl, array(
			CURLOPT_URL => $WEKAN_LINK."/api/boards/".$wekan_client_id."/members/".$wekan_member_id."",
			CURLOPT_RETURNTRANSFER => true,
			CURLOPT_ENCODING => "",
			CURLOPT_MAXREDIRS => 10,
			CURLOPT_TIMEOUT => 0,
			CURLOPT_FOLLOWLOCATION => true,
			CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
			CURLOPT_CUSTOMREQUEST => "POST",
			CURLOPT_POSTFIELDS => "isAdmin=true&isNoComments=true&isCommentOnly=true",
			CURLOPT_HTTPHEADER => array(
			"Accept: application/json",
			"Content-Type: application/x-www-form-urlencoded",
			"Authorization: Bearer " . $wekan_token,
			"Content-Type: application/x-www-form-urlencoded"
			),
			));

			$response = curl_exec($curl);

			curl_close($curl);
			//echo $response;



			}







	}

    


    return $wekan_member_id;

    

}







public function update_user(){
		
		if ($this->session->userdata('front_logged_in')) {
			$session_data = $this->session->userdata('front_logged_in');
			$created_date = date('Y-m-d');
			$userid=$session_data['id'];
			$user_id=$this->input->post('user_id');
			
			if($user_id!='')
			{

				  $user_details=$this->db->query("select * from wc_users where user_id='$user_id'")->result_array();
				  $wekan_member_id = $user_details[0]['wekan_member_id'];
				  $user_email_exist = $user_details[0]['user_email'];
				  $user_password=base64_decode($user_details[0]['user_password_text']);

				  $user_type_id=$this->input->post('user_type_id');
				  $user_name=$this->input->post('user_name');
				  $user_email=$this->input->post('user_email');
				  $client_id=$this->input->post('client_id');


				  
				  	

				  	/* if($user_type_id!='4' && $user_type_id!='6')
				  	{
				  	$wekan_member_id=$this->update_member_wekan($wekan_member_id,$client_id,$user_type_id,$user_name,$user_email,$user_password);
				  	}
				  	else
				  	{*/
				  		$wekan_member_id="";

				  	/*}*/


				
				  $data = array(
				    /*'user_id'=>$this->input->post('user_id'),*/
				    'wekan_member_id'=>$wekan_member_id,
					'user_type_id'=>$this->input->post('user_type_id'),
					'client_id'=>$this->input->post('client_id'),
					'brand_id'=>$this->input->post('brand_id'),
					'user_name'=>$this->input->post('user_name'),
					//'user_address'=>$this->input->post('user_address'),
					'user_email'=>$this->input->post('user_email'),
					//'user_telephone'=>$this->input->post('user_telephone'),
					'user_mobile'=>$this->input->post('user_mobile'),
					/*'user_password'=>$this->input->post('user_password'),*/
					
					'status'=>$this->input->post('status'),
					);









				  
					
				  $updateuserrole = $this->User_model->user_update($data,$user_id);
			      echo $user_id;





				  /*********************************Send mail start*************************************/

				  if($user_email_exist!=$user_email)
				  {

                $query=$this->db->query("select * from  wc_settings  WHERE setting_key='MAIL_SMTP_USER' ");
                $userDetails= $query->result_array();
                $MAIL_SMTP_USER=$userDetails[0]['setting_value'];

                $query=$this->db->query("select * from  wc_settings  WHERE setting_key='MAIL_SMTP_PASSWORD' ");
                $userDetails= $query->result_array();
                $MAIL_SMTP_PASSWORD=$userDetails[0]['setting_value'];

                $query=$this->db->query("select * from  wc_settings  WHERE setting_key='MAIL_FROM' ");
                $userDetails= $query->result_array();
                $MAIL_FROM=$userDetails[0]['setting_value'];


                $query=$this->db->query("select * from  wc_settings  WHERE setting_key='MAIL_FROM_TEXT' ");
                $userDetails= $query->result_array();
                $MAIL_FROM_TEXT=$userDetails[0]['setting_value'];


                $query=$this->db->query("select * from wc_settings WHERE setting_key='WEKAN_URL_LINK' ");
				$settingDetails= $query->result_array();
				$WEKAN_URL_LINK=$settingDetails[0]['setting_value'];

                $query=$this->db->query("select * from  wc_settings  WHERE setting_key='MAIL_BODY_WEKAN_LOGIN' ");
                $userDetails= $query->result_array();
                $MAIL_BODY=$userDetails[0]['setting_value'];

                

                $client_id=$this->input->post('client_id');
                $user_name=$this->input->post('user_name');
                $user_email=$this->input->post('user_email');
                $user_password=$user_password;






                $checkquerys = $this->db->query("select * from wc_clients where  client_id='$client_id' ")->result_array();
                $client_name=ucfirst($checkquerys[0]['client_name']);
                $client_image=$checkquerys[0]['client_image'];
                $client_email=$checkquerys[0]['client_email'];

                //$subject=$client_name.' - Collab Login';
                  $subject=$client_name.' - Login User Email Changed';

                //$unique_user_id=time()."-".$user_id;
                //$activation_link=base_url()."activate-registration/".base64_encode($unique_user_id);

                    
                    //Load email library
                    $this->load->library('email');

                    $config = array();
                    //$config['protocol'] = 'smtp';
                    $config['protocol']     = 'mail';
                    $config['smtp_host'] = 'localhost';
                    $config['smtp_user'] = $MAIL_SMTP_USER;
                    $config['smtp_pass'] = $MAIL_SMTP_PASSWORD;
                    $config['smtp_port'] = 25;




                    $this->email->initialize($config);
                    $this->email->set_newline("\r\n");


					if($client_id!='') {
					$checkquerys = $this->db->query("select * from wc_clients where client_id='$client_id' ")->result_array();

					$client_image=$checkquerys[0]['client_image'];
					$wekan_client_id=$checkquerys[0]['wekan_client_id'];

					$client_logo=base_url()."/upload_client_logo/".$client_image;
					}
					else
					{
					$client_logo=base_url()."/website-assets/images/logo.png";
					}
					//$client_logo="http://style.piquic.com/WeCreate/website-assets/images/logo.png";

					$client_name=str_replace(" ","-",strtolower($checkquerys[0]['client_name']));


					//$wekan_link=$WEKAN_URL_LINK."b/".$wekan_client_id."/".$client_name;
					$wekan_link=$WEKAN_URL_LINK."sign-in";

					

                    
                      $website_url=base_url();
                     $dashboard_url=base_url()."dashboard";





                    $from_email = $MAIL_FROM;
                    $to_email = $user_email;

                    $date_time=date("d/m/y");
                    /*$copy_right=$MAIL_FROM_TEXT." - ".date("Y");*/
                    $copy_right=$MAIL_FROM_TEXT;
                    //echo $message=$MAIL_BODY;
                   /* $find_arr = array("##CLIENT_LOGO##","##USER_NAME##","##WEKAN_LINK##","##USER_EMAIL##","##USER_PASSWORD##","##COPY_RIGHT##");
                    $replace_arr = array($client_logo,$user_name,$wekan_link,$user_email,$user_password,$copy_right);*/

                   






                    /*$find_arr = array("##CLIENT_LOGO##","##USER_NAME##","##WEKAN_LINK##","##USER_EMAIL##","##USER_PASSWORD##","##COPY_RIGHT##","##WEBSITE_URL##","##DASHBOARD_LINK##");
                    $replace_arr = array($client_logo,$user_name,$wekan_link,$user_email,$user_password,$client_name,$copy_right,$website_url,$dashboard_url);*/

                    $find_arr = array("##WEBSITE_URL##","##CLIENT_LOGO##","##USER_NAME##","##WEKAN_LINK##","##USER_EMAIL##","##USER_PASSWORD##","##COPY_RIGHT##","##DASHBOARD_LINK##");
                    $replace_arr = array($website_url,$client_logo,$user_name,$wekan_link,$user_email,$user_password,$client_name,$copy_right,$dashboard_url);
                    $message=str_replace($find_arr,$replace_arr,$MAIL_BODY);

                    //echo $message;exit;



                    $this->email->from($from_email, $MAIL_FROM_TEXT);
                    $this->email->to($to_email);
                     if($_SERVER['HTTP_HOST']!='collab.piquic.com')
					{
						$this->email->cc(array('poulami@mokshaproductions.in','pranav@mokshaproductions.in','raja.priya@mokshaproductions.in','rajendra.prasad@mokshaproductions.in'));
					}
                    //$this->email->bcc('raja.priya@mokshaproductions.in');
                    $this->email->subject($subject);
                    $this->email->message($message);
                    //Send mail
                    if($this->email->send())
                    {

                        //echo "mail sent";exit;
                        $this->session->set_flashdata("email_sent","Congratulations Email Send Successfully.");
                    }
                    
                    else
                    {
                        //echo $this->email->print_debugger();
                        //echo "mail not sent";exit;
                        $this->session->set_flashdata("email_sent","You have encountered an error");
                    }
                }

                    /*********************************Send mail end*************************************/




				
			}
			
		}else {
		
            /*redirect('login', 'refresh');*/
            redirect('login', 'refresh');
       
		}  
	}







public function update_change_password()			//for displaying the data of the staff into the table
   {

   			$user_id=$this->input->post('user_id');

              
			$user_details=$this->db->query("select * from wc_users where user_id='$user_id'")->result_array();
			$wekan_member_id = $user_details[0]['wekan_member_id'];
			$user_type_id = $user_details[0]['user_type_id'];
			$user_name = $user_details[0]['user_name'];
			$user_email = $user_details[0]['user_email'];
			$client_id = $user_details[0]['client_id'];



			
			$user_password=$this->input->post('user_password');
			


			$wekan_member_id=$this->update_member_wekan($wekan_member_id,$client_id,$user_type_id,$user_name,$user_email,$user_password);

	
	       $user_id=$this->input->post('user_id');
	          $data = array(
	          	'wekan_member_id'=>$wekan_member_id,
				'user_password' => sha1($this->input->post('user_password')),
				'user_password_text'=>base64_encode($this->input->post('user_password')),
                 );
				//print_r($staffdata);
				$updateuserrole = $this->User_model->user_update($data, $user_id);
			    echo $user_id;


			    		    /*********************************Send mail start*************************************/

                $query=$this->db->query("select * from  wc_settings  WHERE setting_key='MAIL_SMTP_USER' ");
                $userDetails= $query->result_array();
                $MAIL_SMTP_USER=$userDetails[0]['setting_value'];

                $query=$this->db->query("select * from  wc_settings  WHERE setting_key='MAIL_SMTP_PASSWORD' ");
                $userDetails= $query->result_array();
                $MAIL_SMTP_PASSWORD=$userDetails[0]['setting_value'];

                $query=$this->db->query("select * from  wc_settings  WHERE setting_key='MAIL_FROM' ");
                $userDetails= $query->result_array();
                $MAIL_FROM=$userDetails[0]['setting_value'];


                $query=$this->db->query("select * from  wc_settings  WHERE setting_key='MAIL_FROM_TEXT' ");
                $userDetails= $query->result_array();
                $MAIL_FROM_TEXT=$userDetails[0]['setting_value'];


                $query=$this->db->query("select * from wc_settings WHERE setting_key='WEKAN_URL_LINK' ");
				$settingDetails= $query->result_array();
				$WEKAN_URL_LINK=$settingDetails[0]['setting_value'];

                /*$query=$this->db->query("select * from  wc_settings  WHERE setting_key='MAIL_BODY_WEKAN_LOGIN' ");
                $userDetails= $query->result_array();
                $MAIL_BODY=$userDetails[0]['setting_value'];*/

                $query=$this->db->query("select * from  wc_settings  WHERE setting_key='MAIL_BODY_PASSWORD_SUCCESS' ");
                $userDetails= $query->result_array();
                $MAIL_BODY=$userDetails[0]['setting_value'];


                






                $checkquerys = $this->db->query("select * from wc_clients where  client_id='$client_id' ")->result_array();
                $client_name=ucfirst($checkquerys[0]['client_name']);
                $client_image=$checkquerys[0]['client_image'];
                $client_email=$checkquerys[0]['client_email'];

                $subject=$client_name.' - Password Changed';

                //$unique_user_id=time()."-".$user_id;
                //$activation_link=base_url()."activate-registration/".base64_encode($unique_user_id);

                    
                    //Load email library
                    $this->load->library('email');

                    $config = array();
                    //$config['protocol'] = 'smtp';
                    $config['protocol']     = 'mail';
                    $config['smtp_host'] = 'localhost';
                    $config['smtp_user'] = $MAIL_SMTP_USER;
                    $config['smtp_pass'] = $MAIL_SMTP_PASSWORD;
                    $config['smtp_port'] = 25;




                    $this->email->initialize($config);
                    $this->email->set_newline("\r\n");


					if($client_id!='') {
					$checkquerys = $this->db->query("select * from wc_clients where client_id='$client_id' ")->result_array();

					$client_image=$checkquerys[0]['client_image'];
					$wekan_client_id=$checkquerys[0]['wekan_client_id'];

					$client_logo=base_url()."/upload_client_logo/".$client_image;
					}
					else
					{
					$client_logo=base_url()."/website-assets/images/logo.png";
					}
					//$client_logo="http://style.piquic.com/WeCreate//website-assets/images/logo.png";

					$client_name=str_replace(" ","-",strtolower($checkquerys[0]['client_name']));


					//$wekan_link=$WEKAN_URL_LINK."b/".$wekan_client_id."/".$client_name;
					$wekan_link=$WEKAN_URL_LINK."sign-in";

					

                    
                     $website_url=base_url();
                     $dashboard_url=base_url()."dashboard";





                    $from_email = $MAIL_FROM;
                    $to_email = $user_email;

                    $date_time=date("d/m/y");
                    $copy_right=$MAIL_FROM_TEXT." - ".date("Y");
                    //echo $message=$MAIL_BODY;
                    /*$find_arr = array("##CLIENT_LOGO##","##USER_NAME##","##WEKAN_LINK##","##USER_EMAIL##","##USER_PASSWORD##","##COPY_RIGHT##");
                    $replace_arr = array($client_logo,$user_name,$wekan_link,$user_email,$user_password,$copy_right);*/
                    $find_arr = array("##WEBSITE_URL##","##CLIENT_LOGO##","##USER_NAME##","##WEKAN_LINK##","##USER_EMAIL##","##USER_PASSWORD##","##COPY_RIGHT##","##DASHBOARD_LINK##");
                    $replace_arr = array($website_url,$client_logo,$user_name,$wekan_link,$user_email,$user_password,$client_name,$copy_right,$dashboard_url);
                    $message=str_replace($find_arr,$replace_arr,$MAIL_BODY);

                    //echo $message;exit;



                    $this->email->from($from_email, $MAIL_FROM_TEXT);
                    $this->email->to($to_email);
                     if($_SERVER['HTTP_HOST']!='collab.piquic.com')
					{
						$this->email->cc(array('poulami@mokshaproductions.in','pranav@mokshaproductions.in','raja.priya@mokshaproductions.in','rajendra.prasad@mokshaproductions.in'));
					}
                    $this->email->subject($subject);
                    $this->email->message($message);
                    //Send mail
                    if($this->email->send())
                    {

                        //echo "mail sent";exit;
                        $this->session->set_flashdata("email_sent","Congratulations Email Send Successfully.");
                    }
                    
                    else
                    {
                        //echo $this->email->print_debugger();
                        //echo "mail not sent";exit;
                        $this->session->set_flashdata("email_sent","You have encountered an error");
                    }


                    /*********************************Send mail end*************************************/
				
				
				//redirect('manage-employee');
   }













}

?>