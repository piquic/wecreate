<?php
/**For Add Edit service page START**/
$lang['Add Services'] = 'New Service';
$lang['Add Services Details'] = 'Create a new service';

$lang['Edit Services'] = 'Edit Services';
$lang['Edit Services Details'] = 'Change an existing service.';

$lang['Service Business Name']='Business Name';
$lang['Service Business Name Details']='Select relevant business name.';

$lang['Service Name']='Service Name';
$lang['Service Name Details']='Name of the Service. Service Name will be displayed in Email Confirmations, Invoices and Reports. Therefore, try to keep it short, use 2-3 words. Option: A service can be offered as a package. Example: Yoga Retreat package - 10 days. Packages must be created in Business Setup>Package Management>Add Package to activate this handy option.';

$lang['Price']='Price';
$lang['Price Details']='Sales price of the service. Format: xx.xx. Type 0 if there is no sales price.';

$lang['Required Deposit']='Deposit %';
$lang['Required Deposit Details']='Do you require a deposit to confirm the booking? Format: 1-99 digits. Type 0 if there is no requirement for a deposit.';

$lang['Time Before Booking']='Time Before Booking';
$lang['Time Before Booking Details']='This is the time it requires for a service provider to prepare for the next booking. Example: Time required for cleaning and to set-up the consulting room for next customer. Number of hours entered here will prevent a customer from booking any time spot within this period. Example: if right now is 12pm and you have 1 hour in this field, the earliest time spot customers will be able to book is 1pm';

$lang['Description']='Description';
$lang['Description Details']='Description of the service. Your customers will read this information to understand the details of the service they purchase. Therefore, provide all key information relevant for the service and customer. A wrong or unclear descriptions may lead to unhappy customers, disputes and claims.';

$lang['Service Duration']='Service Duration';
$lang['Service Duration Details']='The time it takes to perform the service. That is the duration of 1 booking interval (spot). Try to estimate the time as accurately as possible to minimise wait times for the next customer.';

$lang['Payment Mode']='Point of Payment';
$lang['Payment Mode Details']='Do you want your customers to pay prior to a booking confirmation? If yes, select "Paypal" option. Select "Offline Payment" to confirm a booking without a payment. Configure Paypal settings in Business Setup>Finance>Payment Gateways';

$lang['Minimum Booking per services']='The minimum number of bookings required to perform a service. Example: A yoga centre requiring a minimum of 10 bookings as a pre-requisite for a booking confirmation. Individual bookings for the same time-slot will be marked as a "conditional" booking  until the required minimum is met. If the required minimum is not met by the set hrs prior to the time-slot, then all conditional bookings will be cancelled and a notification is emailed.';

$lang['Maximum Booking per services']='Maximum Bookings';
$lang['Maximum Booking per services Details']='Maximum number of bookings available per slot. Example: 2 Consultants are available to service the same timeslot. Therefore, Maximum Bookings = 2';

$lang['Sunday']='Sunday';
$lang['Sunday Details']='';

$lang['Monday']='Monday';
$lang['Monday Details']='';

$lang['Tuesday']='Tuesday';
$lang['Tuesday Details']='';

$lang['Wednesday']='Wednesday';
$lang['Wednesday Details']='';

$lang['Thursday']='Thursday';
$lang['Thursday Details']='';

$lang['Friday']='Friday';
$lang['Friday Details']='';

$lang['Saturday']='Saturday';
$lang['Saturday Details']='';

$lang['Start Time']='Start Time';
$lang['Start Time Details']='';

$lang['End Time']='End Time';
$lang['End Time Details']='';

$lang['Package option']="Package option";
$lang['Select Package option']="Select Package option";
/**For Add Edit service page END**/


/**For Add Edit Booking page START**/
$lang['Add Booking'] = 'New Booking';
$lang['Add Booking Details'] = 'Create a new booking.';

$lang['Edit Booking'] = 'Edit Booking';
$lang['Edit Booking Details'] = 'Change details of an existing booking.';

$lang['Business Name'] = 'Business Name';
$lang['Business Name Details'] = 'Select relevant Business Name.';

$lang['Customer'] = 'Customer';
$lang['Customer Details'] = 'Select the Customer, or create a New Customer.';

$lang['Service'] = 'Service';
$lang['Service Details'] = 'Select a Service.';

$lang['Service Price'] = 'Service Price';
$lang['Service Price Details'] = 'Service Price is auto-populated as per the Service Configuration.';

$lang['Special Instructions'] = 'Special Instructions';
$lang['Special Instructions Details'] = 'Use this field to record Special Instructions related to the booking.';

$lang['Booking Date'] = 'Booking Date';
$lang['Booking Date Details'] = 'Select the required date for the booking. The available time-slots are displayed for selection as per the Service Configuration.';

$lang['Available Time Slots'] = 'Available Time Slots';
$lang['Available Time Slots Details'] = 'Available time-slots are displayed as per the Service Configuration.';
/**For Add Edit Booking page END**/

/**For Add Edit Business Owner page START**/
$lang['Cancellation Charges'] = 'Cancellation Charges';
$lang['Cancellation Charges Details'] = 'Do you have a minimum charge for a booking cancellation? Type 0 if there is no cancellation fee.';

$lang['Number Of Days Prior To Cancellation'] = 'Number Of Days Prior To Cancellation';
$lang['Number Of Days Prior To Cancellation Details'] = 'A cancellation could result in a lost opportunity by preventing another customer booking the same time-slot. Do you have a policy on the minimum number of days required (calculated from the booking date) for a cancellation to go through without a charge? Type 0 if there is no cancellation policy.';

$lang['Owner Business Name'] = 'Business Name';
$lang['Owner Business Name Details'] = 'The name of the business as you want it to appear on the invoices and booking confirmation emails.';

$lang['Business Type'] = 'Business Type';
$lang['Business Type Details'] = 'The type of the business is used by the system to make it more user friendly for your customers and staff.';

$lang['Contact Person Name'] = 'Contact Person Name';
$lang['Contact Person Name Details'] = 'Name of the contact person. Typically this is the owner of the business.';

$lang['Owner Email'] = 'Email';
$lang['Owner Email Details'] = 'Business email is used by the system to send-out notifications. It is also the username.';

$lang['Owner Mobile'] = 'Mobile';
$lang['Owner Mobile Details'] = '';

$lang['Owner Theme Color'] = 'Colour';
$lang['Owner Theme Color Details'] = 'Select the theme colour of the system. It will be effective from the next time you login to the system.';

$lang['Owner Address'] = 'Address';
$lang['Owner Address Details'] = 'Address of the Business. It is displayed on Invoices, and used to make the system more user friendly for your customers and staff.';

$lang['Owner City'] = 'City';
$lang['Owner City Details'] = 'Select the cities you operate the business.';

$lang['Owner State'] = 'State';
$lang['Owner State Details'] = 'Select the states you operate the business.';

$lang['Owner Country'] = 'Country';
$lang['Owner Country Details'] = 'Select the countries you operate the business.';

$lang['Owner Sales Tax'] = 'Sales Tax';
$lang['Owner Sales Tax Details'] = 'Sales Tax % is used to charge the tax on the sales price. Example: This can be the Goods & Service Tax, Value Added Tax, Sales Tax etc. Type 0 if there is no sales tax.';

$lang['Domain'] = 'Domain';
$lang['Domain Details'] = 'Leave blank if there is no domain for the business.';

$lang['ABN No'] = 'ABN No';
$lang['ABN No Details'] = 'Leave blank if there is no ABN number.';

$lang['Phone No'] = 'Phone No';
$lang['Phone No Details'] = '';

$lang['Opening Time'] = 'Opening Time';
$lang['Opening Time Details'] = 'Select the opening time of the business.';

$lang['Closing Time'] = 'Closing Time';
$lang['Closing Time Details'] = 'Select the closing time of the business.';

$lang['Upload Logo'] = 'Upload Logo';
$lang['Upload Logo Details'] = 'Upload your logo. It will be displayed on Invoices and notifications.';

$lang['User Name'] = 'User Name';
$lang['User Name Details'] = '';

$lang['Password'] = 'Password';
$lang['Password Details'] = '';

$lang['Confirm Password'] = 'Confirm password';
$lang['Confirm Password Details'] = '';

$lang['Booking Reminder On'] = 'Booking Reminder On';
$lang['Booking Reminder On Details'] = 'Do you want the system to send booking reminders via email? Select the checkbox to activate this feature.';

$lang['Payment Method'] = 'Payment Method';
$lang['Payment Method Details'] = '';

/* For Add Edit Business Owner page END*/

?>

