<?php
/**For Add Edit service page START**/

$lang['Create User']='Create New User';
$lang['Update User']='Update User';
$lang['View User']='View User';


$lang['user_name']='User Name';
$lang['user_email']='User Email';
$lang['user_password']='User Password';
$lang['user_cpassword']='User Confirm Password';
$lang['user_telephone']='User Telephone';
$lang['user_mobile']='User Mobile';
$lang['user_group']='User Group';
$lang['user_address']='User Address';
$lang['user_status']='User Status';

$lang['create_customer']='Create New Customer';
$lang['update_customer']='Update Customer';
$lang['view_customer']='View Customer';


$lang['customer_name']='Customer Name';
$lang['customer_email']='Customer Email';
$lang['customer_password']='Customer Password';
$lang['customer_cpassword']='Customer Confirm Password';
$lang['customer_telephone']='Customer Telephone';
$lang['customer_mobile']='Customer Mobile';
$lang['customer_group']='Customer Group';
$lang['customer_address']='Customer Address';
$lang['total_credits']='Customer Total Credits';
$lang['customer_status']='Customer Status';



$lang['create_entity']='Create New Store';
$lang['update_entity']='Update Store';
$lang['view_entity']='View Store';


$lang['entity_name']='Entity Name';
$lang['entity_email']='Email';
$lang['entity_telephone1']='Phone 1';
$lang['entity_telephone2']='Phone 2';
$lang['entity_telephone3']='Phone 3';
$lang['entity_mobile1']='Mobile 1';
$lang['entity_mobile2']='Mobile 2';
$lang['entity_mobile3']='Mobile 3';
$lang['entity_fax']='Fax';
$lang['entity_website']='Website';
$lang['entity_facebook']='Facebook';
$lang['entity_google']='Google';
$lang['entity_linkedin']='linkdin';
$lang['entity_twitter']='Twitter';
$lang['entity_logo']='Logo';
$lang['entity_country']='Country';
$lang['entity_state']='State';
$lang['entity_suburb']='Suburb';
$lang['entity_city']='City';
$lang['entity_postcode']='Postcode';
$lang['entity_address']='Address';
$lang['latitude']='Lattitude';
$lang['longitude']='Longitude';
$lang['status']='Status';




$lang['create_menu']='Create New Menu';
$lang['update_menu']='Update Menu';
$lang['view_menu']='View Menu';


$lang['menu_name']='Menu Name';
$lang['menu_time_from']='Level1';
$lang['menu_time_to']='Level3';
$lang['image_name']='Level3';







$lang['create_item_category']='Create New Category';
$lang['update_item_category']='Update Category';
$lang['view_item_category']='View Category';


$lang['category_level3']='Category Name';
$lang['category_level1']='Level1';
$lang['category_level2']='Level3';
$lang['category_level4']='Level3';










$lang['create_item']='Create New Item';
$lang['update_item']='Update Item';
$lang['view_item']='View Item';


$lang['item_menu']='Item Menu';
$lang['item_category']='Item Category';
$lang['item_extra']='Item Extraas';
$lang['item_option']='Item Options';
$lang['item_segment']='Item Segments';
$lang['level1segment']='Item Segments level1';
$lang['level2segment']='Item Segments level2';
$lang['level3segment']='Item Segments level3';

$lang['item_name']='Item Name';
$lang['item_name']='Item Name';
$lang['item_description']='Item Description';
$lang['item_price']='Item Price';
$lang['item_status']='Item Status';
$lang['item_image1']='Item Image1';
$lang['item_image2']='Item Image2';
$lang['item_image3']='Item Image3';
$lang['item_image4']='Item Image4';
$lang['item_image5']='Item Image5';
$lang['notes']='Allow Type';
$lang['status']='Status';




$lang['create_extras']='Create New Extras';
$lang['update_extras']='Update Extras';
$lang['view_extras']='View Extras';


$lang['extra_name']='Extras Name';
$lang['extra_price']='Extras price';
$lang['Status']='Status';






$lang['create_options']='Create New Options';
$lang['update_options']='Update Options';
$lang['view_options']='View Options';


$lang['option_name']='Options Name';
$lang['option_price']='Options price';
$lang['status']='Status';



$lang['create_segments']='Create New Segments';
$lang['update_segments']='Update Segments';
$lang['view_segments']='View Segments';


$lang['segment_name']='Segments Name';
$lang['level1segment']='Segments Level1';
$lang['level2segment']='Segments Level2';
$lang['level3segment']='Segments Level3';
$lang['level4segment']='Segments Level4';
$lang['status']='Status';





$lang['create_combos']='Create New Combos';
$lang['update_combos']='Update Combos';
$lang['view_combos']='View Combos';


$lang['combo_item']='Combos Item';
$lang['combo_name']='Combos Name';
$lang['combo_price']='Combos price';
$lang['combo_discount']='Combos Discount';
$lang['status']='Status';





$lang['create_promotionpoints']='Create New Promotionpoints';
$lang['update_promotionpoints']='Update Promotionpoints';
$lang['view_promotionpoints']='View Promotionpoints';


$lang['promotionpoint_customer_name']='Customer Name';
$lang['earned_point']='Earned Points';
$lang['share_point']='Share Points';




$lang['create_sharepoints']='Create New Sharepoints';
$lang['update_sharepoints']='Update Sharepoints';
$lang['view_sharepoints']='View Sharepoints';


$lang['sharepoint_by_customer_name']='Share By Customer Name';
$lang['sharepoint_to_customer_name']='Share To Customer Name';
$lang['share_point']='Share Points';







$lang['create_order']='Create New Order';
$lang['update_order']='Update Order';
$lang['view_order']='View Order';


$lang['order_type']='Order Type';
$lang['order_taken']='Order Taken';
$lang['customer_id']='Customer Name';
$lang['customer_mobile']='Customer Mobile';
$lang['need_date']='Pick Up Date';
$lang['payment_method']='Payment Method';
$lang['add_item']='Add Item';
$lang['add_extra']='Add Extra';
$lang['add_option']='Add Option';

$lang['total_credits']='Total Credits';
$lang['earned_point']='Earned Points';


$lang['create_banner']='Create New Banner';
$lang['update_banner']='Update Banner';
$lang['view_banner']='View Banner';
$lang['banner_name']='Banner Name';
$lang['banner_image']='Banner Image';



$lang['create_slider']='Create New Slider';
$lang['update_slider']='Update Slider';
$lang['view_slider']='View Slider';
$lang['slider_name']='Slider Name';
$lang['slider_image']='Slider Image';




























?>

