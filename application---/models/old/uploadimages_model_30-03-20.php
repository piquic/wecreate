<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Uploadimages_model extends CI_Model
{

function getbrandid($breif_id)
{
	$sql = "SELECT brand_id FROM `wc_brief` where brief_id='$brief_id'";
	$query = $this->db->query($sql);
	return $query->result_array();
}


	 
function insertimages($file,$brief_id) {
	if ($this->session->userdata('front_logged_in')) {
	$session_data = $this->session->userdata('front_logged_in');
	$user_id = $session_data['user_id'];
	$user_type_id = $session_data['user_type_id'];
	$user_name = $session_data['user_name'];
	$type1 = $session_data['user_type_id'];
	$brand_access=$session_data['brand_access'];
	$user_type_id=$type1;
	}
	else
	{
	$user_id = '';
	$user_type_id = '';
	$user_name = '';
	}
	 $file=$file;
	 $brief_id=$brief_id;
	 $status=0;
	 $parent=0;
	 $version=1;
	 //$brand_id=getbrandid($brief_id);
	 
	 $sql = "SELECT * FROM `wc_brief` where brief_id='$brief_id'";
	$query = $this->db->query($sql);
	$brand_result=$query->result_array();
	 foreach($brand_result as $keybill_sub => $brand_details)
	  {
			$brand_id=$brand_details['brand_id'];
			$brief_name=$brand_details['brief_title'];			
	  }
	 	
	$insert_sql="INSERT INTO `wc_image_upload` (`image_id`, `img_title`, `image_path`, `brand_id`, `brief_id`, `parent_img`, `version_num`, `added_on`, `added_by`, `img_status`) VALUES 
	(NULL, '$brief_name', '".$file."','".$brand_id."', '".$brief_id."', '0', '1',CURRENT_TIMESTAMP, '".$user_id."', '0')";
	$wc_image_query = $this->db->query($insert_sql);

	echo $insert_sql;
}


function insertimages1($file,$brief_id,$image_id) {
	if ($this->session->userdata('front_logged_in')) {
	$session_data = $this->session->userdata('front_logged_in');
	$user_id = $session_data['user_id'];
	$user_type_id = $session_data['user_type_id'];
	$user_name = $session_data['user_name'];
	$type1 = $session_data['user_type_id'];
	$brand_access=$session_data['brand_access'];
	$user_type_id=$type1;
	}
	else
	{
	$user_id = '';
	$user_type_id = '';
	$user_name = '';
	}
	 $file=$file;
	 $brief_id=$brief_id;
	 $image_id=$image_id;
	 $status=0;
	 $parent=0;
	 $version=1;
	 //$brand_id=getbrandid($brief_id);
	 
	 $wc_brief_sql_ver = "SELECT version_num,img_status FROM `wc_image_upload` where brief_id='$brief_id' and parent_img=$image_id ORDER BY `version_num` DESC";
	 $wc_brief_query_ver = $this->db->query($wc_brief_sql_ver);
	 $wc_brief_result_ver=$wc_brief_query_ver->result_array();
	 $num=sizeof($wc_brief_result_ver);
	 if($num>0)
	 {
	 $max_ver=$wc_brief_result_ver[0]['version_num'];
	 }
	 else
	 {
		 $max_ver=1;
	 }
	 	 
	 $max_ver=$max_ver+1;
	 
	 $sql = "SELECT * FROM `wc_brief` where brief_id='$brief_id'";
	$query = $this->db->query($sql);
	$brand_result=$query->result_array();
	 foreach($brand_result as $keybill_sub => $brand_details)
	  {
			$brand_id=$brand_details['brand_id'];
			$brief_name=$brand_details['brief_title'];			
	  }
	 	
	$insert_sql="INSERT INTO `wc_image_upload` (`image_id`, `img_title`, `image_path`, `brand_id`, `brief_id`, `parent_img`, `version_num`, `added_on`, `added_by`, `img_status`) VALUES 
	(NULL, '$brief_name', '".$file."','".$brand_id."', '".$brief_id."', '$image_id', '$max_ver',CURRENT_TIMESTAMP, '".$user_id."', '3')";
	$wc_image_query = $this->db->query($insert_sql);

	echo $insert_sql;
}






		public function checkPermission($user_type_id,$module_id,$field_name)
    {
		$sql="select ".$field_name." as field from  wc_module_permission WHERE 1=1 ";

		if($user_type_id!='')
		{
		$sql.=" and user_type_id  ='".$user_type_id."' ";
		}

		if($module_id!='')
		{
		$sql.=" and module_id  ='".$module_id."' ";
		}

     // echo $sql;
        $query = $this->db->query($sql)->result_array();

//print_r($query);

        $field_value=$query[0]['field'];

        return $field_value;
    }
    public function __construct()
    {
        $this->load->database();
        $this->load->library('session');
    }
	
	public function getOwnerlist()
    {
        $sql = 'select * from nc_user where usertype="2"';
        $query = $this->db->query($sql);
        return $query->num_rows();
    }
	
	public function getCustomerlist()
    {
        $sql = 'select * from customer_master';
        $query = $this->db->query($sql);
        return $query->num_rows();
    }	
	
	public function getServicelist()
    {
        $sql = 'select * from service_master where status="Active"';
        $query = $this->db->query($sql);
        return $query->num_rows();
    }
	
	public function getEventlist()
    {
        $sql = 'select * from event_master';
        $query = $this->db->query($sql);
        return $query->num_rows();
    }
	
	
	
	public function sgetCustomerlist($userid)
    {
        $sql = "select * from customer_master where owner_id='$userid'";
        $query = $this->db->query($sql);
        return $query->num_rows();
    }	
	
	public function sgetServicelist($userid)
    {
        $sql = "select * from service_master where owner_id='$userid' and status='Active'";
        $query = $this->db->query($sql);
        return $query->num_rows();
    }
	
	public function sgetEventlist($userid)
    {
        $sql = "select * from event_master where owner_id='$userid'";
        $query = $this->db->query($sql);
        return $query->num_rows();
    }
		public function sgetBookinglist($userid)
    {
        $sql = "select * from manual_booking where owner_id='$userid'";
        $query = $this->db->query($sql);
        return $query->num_rows();
    }
	
	function getbriefinreview($brand_id){
		if($brand_id==0)
		{
		$sql = "select * from wc_brief where status=0";
        }
		else {
		$sql = "select * from wc_brief where status=0 and brand_id=".$brand_id;
		}
		$query = $this->db->query($sql);
        return $query->num_rows();		
	}
	
	
	function getbriefrejected($brand_id){
		if($brand_id==0)
		{
		$sql = "select * from wc_brief where status=4";
        }
		else {
		$sql = "select * from wc_brief where status=4 and brand_id=".$brand_id;
		}
		$query = $this->db->query($sql);
        return $query->num_rows();		
	}
	
	
	function getworkinprogress($brand_id){
		if($brand_id==0)
		{
		$sql = "select * from wc_brief where status=1";
        }
		else {
		$sql = "select * from wc_brief where status=1 and brand_id=".$brand_id;
		}
		$query = $this->db->query($sql);
        return $query->num_rows();		
	}
	
	function getproffing_pending($brand_id){
		if($brand_id==0)
		{
		$sql = "select * from wc_brief where status=6";
        }
		else {
		$sql = "select * from wc_brief where status=6 and brand_id=".$brand_id;
		}
		$query = $this->db->query($sql);
        return $query->num_rows();		
	}
	
	function getrevision_work($brand_id){
		if($brand_id==0)
		{
		$sql = "select * from wc_brief where status=3";
        }
		else {
		$sql = "select * from wc_brief where status=3 and brand_id=".$brand_id; 
		}
		$query = $this->db->query($sql);
        return $query->num_rows();		
	}
	
	function getwork_complete($brand_id){
		if($brand_id==0)
		{
		$sql = "select * from wc_brief where status=2";
        }
		else {
		$sql = "select * from wc_brief where status=2 and brand_id=".$brand_id;
		}
		$query = $this->db->query($sql);
        return $query->num_rows();		
	}
	
	function getfeedback_pending($brand_id){
		if($brand_id==0)
		{
		$sql = "select * from wc_brief where status=5";
        }
		else {
		$sql = "select * from wc_brief where status=5 and brand_id=".$brand_id;
		}
		$query = $this->db->query($sql);
        return $query->num_rows();		
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	public function getBrandtlist_old($brand_access)
    {
		 //$brand_access=$session_data['brand_access'];
		$sql = 'select * from wc_brands where brand_id in ('.$brand_access.')';
        $query = $this->db->query($sql);
        //return $query->num_rows();
		return $query->result_array();
    }
	
	public function getBrandtlist()
    {
		 //$brand_access=$session_data['brand_access'];
		$sql = 'select * from wc_brands';
        $query = $this->db->query($sql);
        //return $query->num_rows();
		return $query->result_array();
    }
	
	
	
	
	
	
	
	
	public function getBookingweekly($date)
    {
	
	     $session_data = $this->session->userdata('logged_in');
         $rolecode = $session_data['rolecode'];          
        
		if($rolecode=='2' || $rolecode=='3')
		{
		$owner_id=$session_data['id'];
		$sql = "select * from manual_booking  WHERE (start_date) = '".$date."' AND  owner_id = '".$owner_id."' ";
		} 
		else 
        {
		$sql = "select * from manual_booking  WHERE (start_date) = '".$date."'  ";
		}
        $query = $this->db->query($sql);
        return $query->num_rows();
    }

	
	public function getBookingweek($day)
    {
	
	    $year=date("Y");
		$month=date("m");
		 $session_data = $this->session->userdata('logged_in');
         $rolecode = $session_data['rolecode'];          
        
		if($rolecode=='2' || $rolecode=='3')
		{
		$owner_id=$session_data['id'];
		
	    $sql = 'select * from manual_booking  WHERE YEAR(start_date) = '.$year.' AND MONTH(start_date) = '.$month.' AND DAY(start_date) = '.$day.'  AND  owner_id = '.$owner_id;
		} 
        else 
        {
		
        $sql = 'select * from manual_booking  WHERE YEAR(start_date) = '.$year.' AND MONTH(start_date) = '.$month.' AND DAY(start_date) = '.$day.'';
		
		}
        $query = $this->db->query($sql);
        return $query->num_rows();
    }
	
		public function getBookingmonth($year,$month)
    {
	
	     $session_data = $this->session->userdata('logged_in');
         $rolecode = $session_data['rolecode'];          
        
		if($rolecode=='2' || $rolecode=='3')
		{
		$owner_id=$session_data['id'];
		
	    $sql = 'select * from manual_booking  WHERE YEAR(start_date) = '.$year.' AND MONTH(start_date) = '.$month.'  AND  owner_id = '.$owner_id;
		} 
        else 
        {
		 $sql = 'select * from manual_booking  WHERE YEAR(start_date) = '.$year.' AND MONTH(start_date) = '.$month.' ';
		
		}
        $query = $this->db->query($sql);
        return $query->num_rows();
    }
	
	
	public function getBookingyear($year)
    {
	
	     $session_data = $this->session->userdata('logged_in');
         $rolecode = $session_data['rolecode'];          
        
		if($rolecode=='2' || $rolecode=='3')
		{
		$owner_id=$session_data['id'];
		
	    $sql = 'select * from manual_booking  WHERE YEAR(start_date) = '.$year.'   AND  owner_id = '.$owner_id;
		} 
        else 
        {
		
        $sql = 'select * from manual_booking  WHERE YEAR(start_date) = '.$year.' ';
		
		}
        $query = $this->db->query($sql);
        return $query->num_rows();
    }
	


	public function getBookinglist($owner_id="")
    {
	
	    $session_data = $this->session->userdata('logged_in');
        $rolecode = $session_data['rolecode'];          
        
		if($rolecode=='2' || $rolecode=='3')
		{
		$owner_id=$session_data['id'];
		
		$sql = 'select * from manual_booking  WHERE status="Pending"   AND  owner_id = "'.$owner_id.'" order by manual_booking_id desc  limit 7 ';
		} 
		else
		{
        $sql = 'select * from manual_booking WHERE status="Pending" order by manual_booking_id desc   limit 7';
		}
		
        $query = $this->db->query($sql);
        return $query->result();
    }
	

	public function getBookinglistwithservice($owner_id="")
    {
	
	    $session_data = $this->session->userdata('logged_in');
        $rolecode = $session_data['rolecode'];          
        
		if($rolecode=='2' || $rolecode=='3')
		{
		$owner_id=$session_data['id'];
		
		$sql = 'select manual_booking.manual_booking_id,manual_booking.unique_booking_id,manual_booking.start_date,manual_booking.status,service_master.service_title from manual_booking inner join service_master on manual_booking.service_id=service_master.service_id  WHERE manual_booking.status="Confirmed"   AND  manual_booking.owner_id = "'.$owner_id.'" order by manual_booking_id desc  limit 7 ';
		} 
		else
		{
        $sql = 'select manual_booking.manual_booking_id,manual_booking.unique_booking_id,manual_booking.start_date,manual_booking.status,service_master.service_title from manual_booking inner join service_master on manual_booking.service_id=service_master.service_id WHERE manual_booking.status="Confirmed"  order by manual_booking_id desc   limit 7';
		}
		
        $query = $this->db->query($sql);
        return $query->result();
    }
	

	public function getTodaysalecount($userid)
    {
	     $today_date=date("Y-m-d");
		 if($userid!='')
		 {
			
			$sql1='select sum(service_price) as total_price from manual_booking inner join service_master on manual_booking.service_id=service_master.service_id WHERE manual_booking.confirmed_date="'.$today_date.'" AND manual_booking.owner_id="'.$userid.'" and manual_booking.status="Confirmed" and manual_booking.unique_id="" ';
			$query1= $this->db->query($sql1)->result_array();
			if($query1['0']['total_price']!='')
			{
			    $service_price=$query1['0']['total_price'];
			}else {
				 $service_price=0;
			}
			$sql2='select service_price as course_price from manual_booking inner join service_master on manual_booking.service_id=service_master.service_id WHERE manual_booking.confirmed_date="'.$today_date.'" AND manual_booking.owner_id="'.$userid.'" and manual_booking.status="Confirmed" and manual_booking.unique_id!="" group by unique_id';
           //$sql = 'select sum(price) as total_price from manual_booking inner join service_master on manual_booking.service_id=service_master.service_id WHERE manual_booking.created_date="'.$today_date.'" AND manual_booking.owner_id="'.$userid.'" ';
		    $query2 = $this->db->query($sql2)->result();
			$course_price_array=array();
			if($query2){
				foreach($query2 as $result2){
				    $course_price_array[]=$result2->course_price;
			    }
			}
			$course_price=array_sum($course_price_array);
		    $total_price=$service_price+$course_price;
		
		}
		else
		{
			//$sql='select sum(service_price) as total_price from manual_booking inner join service_master on manual_booking.service_id=service_master.service_id WHERE manual_booking.confirmed_date="'.$today_date.'" and manual_booking.status="Confirmed" ';
		   //$sql = 'select sum(price) as total_price from manual_booking inner join service_master on manual_booking.service_id=service_master.service_id WHERE manual_booking.created_date="'.$today_date.'" ';
		
		   $sql1='select sum(service_price) as total_price from manual_booking inner join service_master on manual_booking.service_id=service_master.service_id WHERE manual_booking.confirmed_date="'.$today_date.'" and manual_booking.status="Confirmed" and manual_booking.unique_id="" ';
			$query1= $this->db->query($sql1)->result_array();
			if($query1['0']['total_price']!='')
			{
			    $service_price=$query1['0']['total_price'];
			}
			$sql2='select service_price as course_price from manual_booking inner join service_master on manual_booking.service_id=service_master.service_id WHERE manual_booking.confirmed_date="'.$today_date.'" and manual_booking.status="Confirmed" and manual_booking.unique_id!="" group by unique_id';
           //$sql = 'select sum(price) as total_price from manual_booking inner join service_master on manual_booking.service_id=service_master.service_id WHERE manual_booking.created_date="'.$today_date.'" AND manual_booking.owner_id="'.$userid.'" ';
		    $query2 = $this->db->query($sql2)->result();
			$course_price_array=array();
			if($query2){
				foreach($query2 as $result2){
				    $course_price_array[]=$result2->course_price;
			    }
			}
			$course_price=array_sum($course_price_array);
		    $total_price=$service_price+$course_price;
		
		
		}
		
		//echo $sql; exit;
        /*  $query = $this->db->query($sql)->result_array();
		 
		 if($query['0']['total_price']!='')
		 {
		 $total_price=$query['0']['total_price'];
		 }
		 else
		 {
		 $total_price="0";
		 } */
         return  $total_price;
		
		
    }
	

 }
