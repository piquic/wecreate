<?php
class Settings_model extends CI_Model{
    function get_product_list(){
        if ($this->session->userdata('front_logged_in')) {
            $session_data = $this->session->userdata('front_logged_in');
            $user_id = $session_data['user_id'];
            $user_type_id = $session_data['user_type_id'];
            $user_name = $session_data['user_name'];
            $type1 = $session_data['user_type_id'];
            $brand_access=$session_data['brand_access'];
            $client_access=$session_data['client_access'];
            $user_type_id=$type1;
        }
        else{
            $user_id = '';
            $user_type_id = '';
            $user_name = '';
        }
       
        $sql="select * from  wc_product_price";

        //echo $sql;
        $query=$this->db->query($sql);
        //return $query->result();
        return $query->result_array();
    }
    public function add_product($productdata){
        $this->db->insert('wc_product_price', $productdata);
    }
    public function update_product($productdata, $product_id){
        $this->db->where('product_id', $product_id);
        $report = $this->db->update('wc_product_price', $productdata);
        if ($report) {
            return true;
        } else {
            return false;
        }
    }
    public function delete_product($product_id){
        $array = array('product_id' => $product_id);
        $this->db->where($array); 
        $result=$this->db->delete('wc_product_price');
    }

    public function update_notifications($settingdata,$user_id){
        $this->db->where('user_id', $user_id);
        $report = $this->db->update('wc_users', $settingdata);
    //print_r($settingdata);
    if($settingdata['admin_mail_notifications']==1)
    {
        $status="Activated";
    }
    else{
        $status="DeActivated";
    }
      echo "Email Notifications susessfully ".$status;
    }



}