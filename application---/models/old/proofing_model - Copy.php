<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Proofing_model extends CI_Model
{

	function getbrandid($breif_id)
	{
		$sql = "SELECT brand_id FROM `wc_brief` where brief_id='$brief_id'";
		$query = $this->db->query($sql);
		return $query->result_array();
	}

		 
	function insertimages($file,$file_type,$brief_id) {
		if ($this->session->userdata('front_logged_in')) {
			$session_data = $this->session->userdata('front_logged_in');
			$user_id = $session_data['user_id'];
			$user_type_id = $session_data['user_type_id'];
			$user_name = $session_data['user_name'];
			$type1 = $session_data['user_type_id'];
			$brand_access=$session_data['brand_access'];

			$user_type_id=$type1;

			$file=$file;
			$brief_id=$brief_id;
			$status=0;
			$parent=0;
			$version=1;
			//$brand_id=getbrandid($brief_id);

			$sql = "SELECT * FROM `wc_brief` where brief_id='$brief_id'";
			$query = $this->db->query($sql);
			$brand_result=$query->result_array();
			foreach($brand_result as $keybill_sub => $brand_details){
				$brand_id=$brand_details['brand_id'];
				$brief_name=$brand_details['brief_title'];			
			}

			// $insert_rev_sql="INSERT INTO `wc_revision` (`revision_id`, `brief_id`, `image_id`, `user_id`, `revision_name`, `revised_file`, `revised_status`, `revision_datetime`) VALUES (NULL, '".$brief_id."', '', '".$user_id."', 'Revision1', '',  '0', 'date()')";
			// $wc_image_query = $this->db->query($insert_rev_sql);
			// $insertId_rev = $this->db->insert_id();

			$wc_brief_sql_ver = "SELECT version_num,img_status FROM `wc_image_upload` where brief_id='$brief_id' and parent_img=$image_id ORDER BY `version_num` DESC";
			$wc_brief_query_ver = $this->db->query($wc_brief_sql_ver);
			$wc_brief_result_ver=$wc_brief_query_ver->result_array();
			$num=sizeof($wc_brief_result_ver);
			if($num>0){
				$max_ver=$wc_brief_result_ver[0]['version_num'];
			}else{
				$max_ver=1;
			}

			$max_ver=$max_ver+1;

			$insert_sql="INSERT INTO `wc_image_upload` (`image_id`, `img_title`, `image_path`,`file_type`, `brand_id`, `brief_id`, `revision_id`, `parent_img`, `version_num`, `added_on`, `added_by`, `img_status`) VALUES 
			(NULL, '$brief_name', '".$file."','".$file_type."','".$brand_id."', '".$brief_id."','".$max_ver."', '0', '1',CURRENT_TIMESTAMP, '".$user_id."', '2')";
			$wc_image_query = $this->db->query($insert_sql);
			$insertId = $this->db->insert_id();
			// $insert_rev_update="UPDATE `wc_revision` SET `image_id` = '".$insertId."' WHERE `wc_revision`.`revision_id` = '".$insertId_rev."'";
			// $wc_rev_query_update = $this->db->query($insert_rev_update);
			return $insertId;

		}else{
			$user_id = '';
			$user_type_id = '';
			$user_name = '';
		}
		
	}


	function insertimages1($file,$file_type,$brief_id,$image_id) {
		if ($this->session->userdata('front_logged_in')) {
			$session_data = $this->session->userdata('front_logged_in');
			$user_id = $session_data['user_id'];
			$user_type_id = $session_data['user_type_id'];
			$user_name = $session_data['user_name'];
			$type1 = $session_data['user_type_id'];
			$brand_access=$session_data['brand_access'];
			$user_type_id=$type1;

			$file=$file;
			$brief_id=$brief_id;
			$image_id=$image_id;
			$status=0;
			$parent=0;
			$version=1;
			//$brand_id=getbrandid($brief_id);
			$wc_brief_sql_ver1 = "SELECT * FROM `wc_image_upload` where brief_id='$brief_id' and image_id='$image_id'";
			$wc_brief_query_ver1 = $this->db->query($wc_brief_sql_ver1);
			$wc_brief_result_ver1=$wc_brief_query_ver1->result_array();
			//print_r($wc_brief_result_ver1);
			$parent_img=$wc_brief_result_ver1[0]['parent_img'];
			$update_sql = "UPDATE `wc_image_upload` SET `img_status`='4' WHERE parent_img='$parent_img' ";
			$this->db->query($update_sql);


			$wc_brief_sql_ver = "SELECT version_num,img_status FROM `wc_image_upload` where brief_id='$brief_id' and parent_img=$parent_img ORDER BY `version_num` DESC";
			$wc_brief_query_ver = $this->db->query($wc_brief_sql_ver);
			$wc_brief_result_ver=$wc_brief_query_ver->result_array();
			$num=sizeof($wc_brief_result_ver);
			if($num>0){
				$max_ver=$wc_brief_result_ver[0]['version_num'];
			}else{
				$max_ver=1;
			}

			$max_ver=$max_ver+1;
			// $insert_rev_update="UPDATE `wc_image_upload` SET img_status='5' WHERE `parent_img` = '".$image_id."'";
			// $wc_rev_query_update = $this->db->query($insert_rev_update);

			$sql = "SELECT * FROM `wc_brief` where brief_id='$brief_id'";
			$query = $this->db->query($sql);
			$brand_result=$query->result_array();
			foreach($brand_result as $keybill_sub => $brand_details){
				$brand_id=$brand_details['brand_id'];
				$brief_name=$brand_details['brief_title'];			
			}

			$insert_sql="INSERT INTO `wc_image_upload` (`image_id`, `img_title`, `image_path`,`file_type`, `brand_id`, `brief_id`, `parent_img`, `version_num`, `added_on`, `added_by`, `img_status`) VALUES 
			(NULL, '$brief_name', '".$file."','".$file_type."','".$brand_id."', '".$brief_id."', '$parent_img', '$max_ver',CURRENT_TIMESTAMP, '".$user_id."', '2')";
			//print_r($insert_sql);
			$wc_image_query = $this->db->query($insert_sql);

			//$insertId = $this->db->insert_id();
			// $insert_rev_update="UPDATE `wc_revision` SET `image_id` = '".$insertId."' WHERE `wc_revision`.`revision_id` = '".$insertId_rev."'";
			// $wc_rev_query_update = $this->db->query($insert_rev_update);
			return $parent_img;
		}else{
			$user_id = '';
			$user_type_id = '';
			$user_name = '';
		}

	}


 }
