<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Maintaince_model extends CI_Model
{
    public function __construct()
    {
        $this->load->database();
        $this->load->library('session');
    }
	public function get_setting_content()
	{
		
			

		$this->db->select('*');
		$this->db->from('wc_maintaince_mode');
		$query = $this->db->get();
		//print_r($this->db->last_query());  exit();
		//print_r($query);exit();
		return $query->result_array();

		}

		public function setting_update($id,$key,$value)
	    {	
	    	$data=array('setting_value'=>$value);
			$this->db->set('setting_value',false);
	        $this->db->where('setting_id', $id);
	        $this->db->where('setting_key', $key);
	        $report = $this->db->update('wc_maintaince_mode', $data);
	        
	        if ($report) {
	            return true;
	        } else {
	            return false;
	        }
	    }


	
	
	public function upload_img_update($userdata, $upimg_id)
    {
        $this->db->where('upimg_id', $upimg_id);
        $report = $this->db->update('wc_upload_img', $userdata);
        if ($report) {
            return true;
        } else {
            return false;
        }
    }
 }
