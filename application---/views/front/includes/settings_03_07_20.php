<?php
session_start();
error_reporting(0);
 
if ($this->session->userdata('front_logged_in')) {
	$session_data = $this->session->userdata('front_logged_in');
	$user_id = $session_data['user_id'];
	$user_name = $session_data['user_name'];
	$_SESSION['user_id']=$user_id;
	$_SESSION['user_name']=$user_name;
	$type1 = $session_data['user_type_id'];
	$user_type_id=$type1;
	$brand_access=$session_data['brand_access'];
 
	$checkquerys = $this->db->query("select user_type_name from wc_users_type where user_type_id='$user_type_id' ")->result_array();
	$job_title=$checkquerys[0]['user_type_name'];
	$client_access=$session_data['client_access'];

$checkquerys = $this->db->query("select admin_mail_notifications,client_id from wc_users where user_id='$user_id' ")->result_array();
	$admin_mail_notifications=$checkquerys[0]['admin_mail_notifications'];
	$client_id=$checkquerys[0]['client_id'];

}
else
{
	$user_id = '';
	$user_name = ''; 
	$_SESSION['user_id']=$user_id;
	$_SESSION['user_name']=$user_name;
}
?>

<?php $data['page_title'] = "dashboard";

$this->load->view('front/includes/header',$data);
$this->load->view('front/includes/topnav');
?>
<style type="text/css">
	.errmsg{
		color:red;
	}
</style>
<div class="container-fluid">
	<div class="row">

		<!-- Main Body -->
		<div class="col-12 col-sm-12 col-md-12">
			<main class="p-3">

				<!-- <div class="row">
					<div class="col-12 col-sm-12 col-md-12">
						<div class="px-3">
							<a href="<?php echo base_url() ?>dashboard"><p class="text-wc lead"><i class="fas fa-arrow-alt-circle-left"></i>&nbsp;Back to Dashboard</p></a>
						</div>
					</div>
				</div> -->

				<div class="row">
					<div class="col-12 col-sm-12 col-md-12">
						<div class="px-3 text-wc">
							<h2>Settings</h2>
						</div>
					</div>
				</div>

				<!-- <hr class="text-center w-50 "> -->

				<div class="row">
					<div class="col-12 col-sm-12 col-md-12">
						<div class="p-3">
							<ul class="nav nav-tabs" id="myTab" role="tablist">
								<li class="nav-item">
									<a class="nav-link text-wc text-uppercase active" id="general-tab" data-toggle="tab" href="#general" role="tab" aria-controls="general" aria-selected="true">General</a>
								</li>
								<li class="nav-item">
									<a class="nav-link text-wc text-uppercase" id="items-tab" data-toggle="tab" href="#items" role="tab" aria-controls="items" aria-selected="false">Items</a>
								</li>
								<li class="nav-item">
									<a class="nav-link text-wc text-uppercase" id="permission-tab" data-toggle="tab" href="#permission" role="tab" aria-controls="permission" aria-selected="false">Permission</a>
								</li>
								<!-- /*<?php if($type1==2){ ?>
								<li class="nav-item">
									<a class="nav-link text-wc text-uppercase" id="notifications-tab" data-toggle="tab" href="#notifications" role="tab" aria-controls="notifications" aria-selected="false">Email Notifications</a>
								</li>
								<?php } ?>*/ -->
							</ul>
							<div class="tab-content" id="myTabContent">

								<div class="tab-pane fade show active" id="general" role="tabpanel" aria-labelledby="general-tab">
									<div class="p-4">
										<div class="custom-control custom-checkbox">
											<input type="checkbox" class="custom-control-input" id="chkActivateLink">
											<label class="custom-control-label roundCheck" for="chkActivateLink">Activate Link Sharing</label>
										</div>
									</div>
									<hr>

									<style type="text/css">
										.tox-statusbar__branding { display: none; }
									</style>

									<div class="p-4">
										<p class="h4 mb-4">Project Description</p>
										<?php 
										$checkquerys = $this->db->query("select * from wc_clients where client_id='$client_access' ")->result_array();
										
									    $project_description=$checkquerys[0]['project_description'];
									    ?>
										<div class="form-group">
											<textarea id="project_description" name="project_description"><?php echo $project_description ?></textarea>
											<span class="small text-danger" id="errmsg_project_description"></span>
											<input type="hidden" id="client_id" name="client_id" value="<?php echo $client_access ?>">
										</div>
										<button type="button" class="btn btn-wc" id="update_project_desc">Submit</button>
									</div>
								</div>

								<div class="tab-pane fade" id="items" role="tabpanel" aria-labelledby="items-tab">
									<div class="p-4">
										<div class="d-flex justify-content-between flex-wrap">
											<div class="p-1">
												<span class="text-wc lead" data-toggle="modal" data-target="#addModel"><i class="far fa-plus-square"></i>&nbsp;Add Item</span>
											</div>
											<?php 
											$checkquerys = $this->db->query("select * from wc_users where user_id='$user_id' ")->result_array();
											$add_discount=$checkquerys[0]['add_discount'];
											$add_extra_charges=$checkquerys[0]['add_extra_charges'];
											if($add_discount=='0'){ 
												$add_discount_status='1'; 
											}else if($add_discount=='1'){ 
												$add_discount_status='0'; 
											}	
											if($add_extra_charges=='0'){ 
												$add_extra_charges_status='1'; 
											}else if($add_extra_charges=='1'){ 
												$add_extra_charges_status='0'; 
											}	
											?>
											<div class="d-flex justify-content-end flex-wrap">

												<div class="p-1 pt-2 pr-2">
													<div class="custom-control custom-checkbox">
														<input type="checkbox" <?php if($add_discount=='1'){ echo "checked"; } ?> class="custom-control-input" id="chkDiscount" onclick="add_discount('<?php echo $user_id; ?>','<?php echo $add_discount_status; ?>')">
														<label class="custom-control-label" for="chkDiscount">Add Discount</label>
													</div>
												</div>

												<div class="p-1 pt-2 pr-2">
													<div class="custom-control custom-checkbox">
														<input type="checkbox" <?php if($add_extra_charges=='1'){ echo "checked"; } ?> class="custom-control-input" id="chkEC" onclick="add_extra_charges('<?php echo $user_id; ?>','<?php echo $add_extra_charges_status; ?>')">
														<label class="custom-control-label" for="chkEC">Add Extra Charge</label>
													</div>
												</div>
											</div>
										</div>
										<div class="table-responsive" id="product_details_table">
											<?php $this->load->view('front/settings_list', $product_details);   ?>
												
										</div>
									</div>
								</div>
								<div class="tab-pane fade" id="permission" role="tabpanel" aria-labelledby="general-tab">
									<div class="p-4">
										<?php 
										
										$brief_permission_details=$this->db->query("select * from wc_users where client_id=$client_id and user_type_id='3'")->result_array();
										//print_r($brief_permission_details);
										$brief_permission=$brief_permission_details[0]['brief_permission'];
									
										$user_type_id=$brief_permission_details[0]['user_type_id'];
										$brand_id=$brief_permission_details[0]['brand_id'];
										
										if($brand_id!=""){
											$brand_id_arr=explode(",",trim($brand_id,","));
										}
										else{
								   		$brand_id_arr=array();
								   	}
								   //	print_r($brand_id_arr);
										?>
										<div class="custom-control custom-checkbox">
											<input type="checkbox" class="custom-control-input" <?php if($brief_permission==1){ echo "checked"; } ?>  id="upload_brief_pm">
											<label class="custom-control-label roundCheck" for="upload_brief_pm">Enable Brief Upload for PM</label>

											<div class="<?php if($brief_permission==0){ echo "d-none"; } ?>" id="brand_list">
												<p class="h4 mb-4">Brand List</p>
												<div class="custom-control custom-checkbox">
														<input type="checkbox" class="custom-control-input" value="select_all" name="select_all" id="select_all">
														<label class="custom-control-label roundCheck" for="select_all">select_all</label>

													</div>
											<?php 


											$brand_details=$this->db->query("select * from wc_brands where deleted='0' and status='Active' and client_id='$client_id' order by  brand_name asc")->result_array();
													
											if(!empty($brand_details))
											{
												foreach($brand_details as $key => $branddetails)
												{
													$brand_id=$branddetails['brand_id'];
													$brand_name=$branddetails['brand_name'];
													?>
													<div class="custom-control custom-checkbox">
														<input type="checkbox" class="custom-control-input brand_check" value="<?php echo $brand_id;?>" name="brand_id" id="brand_id<?php echo $brand_id;?>" <?php if(in_array($brand_id, $brand_id_arr)) { ?> checked    <?php } ?>>
														<label class="custom-control-label roundCheck" for="brand_id<?php echo $brand_id;?>"><?php echo $brand_name;?></label>

													</div>
														<?php
													}
												}
												?>
											</div>

											
										</div>
										<hr>
										<button id="btn_brief_permission" type="button" class="btn btn-wc" onclick="brief_upload_permission('<?php echo $client_id; ?>','<?php echo $user_type_id; ?>')">Save</button>
									</div>
									
								</div>
								<div class="tab-pane fade" id="notifications" role="tabpanel" aria-labelledby="notifications-tab">
									<div class="p-4">
										<div class="custom-control custom-checkbox">
											<input type="checkbox" class="custom-control-input" id="chkActivatenotification" <?php if($admin_mail_notifications==1){ echo "checked"; } ?> onclick="return activate(<?php echo $user_id; ?>)">
											<label class="custom-control-label roundCheck" for="chkActivatenotification">Activate Board Email Notification</label>
										</div>
									</div>
									<hr>
								</div>

							</div>
						</div>
					</div>
				</div>
			</main>
		</div>

	</div>
</div>
<div class="modal fade" id="addModel" tabindex="-1" role="dialog" aria-labelledby="addModelLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
    
      <div class="modal-header">
        <h5 class="modal-title" id="addModelLabel">Add Product Details</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        
          <div class="form-group">
            <label for="product_name" class="col-form-label">Product Name</label>
            <input type="text" class="form-control" id="product_name" name="product_name" required >
            <span class="small text-danger" id="errmsg_product_name"></span>
          </div>
          <div class="form-group">
            <label for="product_description" class="col-form-label">Product Description</label>
            <textarea class="form-control" id="product_description" name="product_description" required ></textarea>
            <span class="small text-danger" id="errmsg_product_description"></span>
          </div>
          <div class="form-group">
            <label for="product_price" class="col-form-label">Product Price</label>
            <input type="text" class="form-control" id="product_price" name="product_price" required >
            <span class="small text-danger" id="errmsg_product_price"></span>
          </div>

       
      </div>
      <div class="modal-footer">
      	<button type="button" class="btn btn-outline-wc" id="add_product">&emsp;Add&emsp;</button>
        <button type="button" class="btn btn-outline-wc" data-dismiss="modal">&emsp;Close&emsp;</button>
        
      </div>
       
    </div>
  </div>
</div>
<script type="text/javascript">
	function brief_upload_permission(client_id,user_type_id){
		if ($('#upload_brief_pm').is(":checked")){
		  var brief_permission=1;
		}
		else{
			var brief_permission=0;
		}
		if(brief_permission==0){
			$("input[name='brand_id']").each(function(){
		   	$(".brand_check").prop('checked', false);
	  	});
		}
	 var brand_id = new Array();
		$("input[name='brand_id']:checked").each(function(){
        brand_id.push($(this).val());
        
      });
 		brand_id=brand_id.join(",");

		 $.ajax({
		    type: "POST",
		    url: "<?php echo base_url();?>front/Settings/brief_permission", 
		    dataType: 'html',
		    data: 'client_id='+client_id+"&brief_permission="+brief_permission+"&user_type_id="+user_type_id+"&brand_id="+brand_id,
		    success: function(data){
		     	$("#loader").hide();
					if(data=="1"){
						//alert(data);
						location.reload();
					}
		  	}
		  });
        
	}


	$(document).ready(function() {
		var totalCheckboxes=0;
		$("input[name='brand_id']").each(function(){
		    totalCheckboxes=totalCheckboxes+1;
	  });
	  //alert($("input[name=brand_id]:checked").length==totalCheckboxes);
		if($("input[name=brand_id]:checked").length==totalCheckboxes ){
	    $("#select_all").prop('checked', true);
	    

	  }  
	  if($("input[name=brand_id]:checked").length!=totalCheckboxes ){
	    $("#select_all").prop('checked', false);
	  } 
	  if($("input[name=brand_id]:checked").length==0){
	    $('#btn_brief_permission').prop("disabled", true);
	  } 
	  if($("input[name=brand_id]:checked").length>=1){
	  	$('#btn_brief_permission').removeAttr("disabled");
	  } 
	});
	$(".brand_check").change(function(){   
		
		var totalCheckboxes=0;
		$("input[name='brand_id']").each(function(){
		    totalCheckboxes=totalCheckboxes+1;
	  });
	  //alert($("input[name=brand_id]:checked").length==totalCheckboxes);
		if($("input[name=brand_id]:checked").length==totalCheckboxes ){
	    $("#select_all").prop('checked', true);
	    

	  }  
	  if($("input[name=brand_id]:checked").length!=totalCheckboxes ){
	    $("#select_all").prop('checked', false);
	  } 
	  if($("input[name=brand_id]:checked").length==0){
	    $('#btn_brief_permission').prop("disabled", true);
	  } 
	  if($("input[name=brand_id]:checked").length>=1){
	  	$('#btn_brief_permission').removeAttr("disabled");
	  }
	});
$("#select_all").click(function () {
	

	if($(this).prop("checked") == true){
		$(".brand_check").prop('checked', true);
		$('#btn_brief_permission').removeAttr("disabled");
		
  }
  else if($(this).prop("checked") == false){
  	$(".brand_check").prop('checked', false);
    $('#btn_brief_permission').prop("disabled", true);
  }
});

$("#upload_brief_pm").click(function(){
	if($(this).prop("checked") == true){
		$("#brand_list").removeClass('d-none');
		//$("#btn_brief_permission").attr("disabled");
		$('#btn_brief_permission').prop("disabled", true);
  }
  else if($(this).prop("checked") == false){
      $("#brand_list").addClass('d-none');
      //$("#btn_brief_permission").removeAttr("disabled");
       $('#btn_brief_permission').removeAttr("disabled");
  }
}); 
	$("#product_price").keypress(function(e) {
		//console.log("hai");
		if (((e.which != 46 || (e.which == 46 && $(this).val() == '')) ||$(this).val().indexOf('.') != 0) && (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57))) {
			$("#errmsg_product_price").html("Digits Only").show().fadeOut(1500);
			return false;
		}
	}).on('paste', function(e) {
		$("#errmsg_product_price").html("Type the value").show().fadeOut(1500);
		return false;
	});
	$("#product_price").on("input", function() {
		if (/^0/.test(this.value)) {
			this.value = this.value.replace(/^0/, "");
			$("#errmsg_product_price").html("Invalid").show().fadeOut(1500);
		}

	}); 
	
	$('#update_project_desc').click(function(){
		var project_description = encodeURIComponent(CKEDITOR.instances['project_description'].getData());
		var client_id=$("#client_id").val();
		//alert(project_description);

		if(project_description!=""){
			$("#loader").show();
			$.ajax({
		    type: "POST",
		    url: "<?php echo base_url();?>front/Settings/update_project_desc", 
		    dataType: 'html',
		    data: 'client_id='+client_id+"&project_description="+project_description,
		    success: function(data){
		     	$("#loader").hide();
				if(data=="1"){
					//alert(data);
					location.reload();
				}
		  	}
		  });
		}
		else{
			if(project_description==""){
				$("#errmsg_project_description").html("Enter the Some Content").show().fadeOut(1500);
			}
			
		}
	});
	$('#add_product').click(function(){
		var product_name=$("#product_name").val();
		var product_description=$("#product_description").val();
		var product_price = $("#product_price").val();
		//alert(product_name+product_description+product_price);
		if(product_name!="" &&product_description!=""&& product_price!=""){
			$("#loader").show();
			$.ajax({
		    type: "POST",
		    url: "<?php echo base_url();?>front/Settings/add_product", 
		    dataType: 'html',
		    data: 'product_name='+product_name+"&product_description="+product_description+"&product_price="+product_price,
		    success: function(data){
				$("#loader").hide();
				$('#product_details_table').html('');
				$('#product_details_table').html(data);
				$('#addModel').modal('hide');
				$("#product_name").val("");
				$("#product_description").val("");
				$("#product_price").val("");
		  	}
		  });
		}
		else{
			if(product_name==""){
				$("#errmsg_product_name").html("Enter the value").show().fadeOut(1500);
			}
			if(product_description==""){
				$("#errmsg_product_description").html("Enter the Value").show().fadeOut(1500);
			}
			if(product_price==""){
				$("#errmsg_product_price").html("Enter the Value").show().fadeOut(1500);
			}
		}
	})
	function edit_product(product_id){
		//alert(product_id);
		var product_name=$("#product_name_"+product_id).val();
		var product_description=$("#product_description_"+product_id).val();
		var product_price = $("#product_price_"+product_id).val();
		if(product_name!="" &&product_description!=""&& product_price!=""){
			$("#loader").show();
			$.ajax({
		    type: "POST",
		    url: "<?php echo base_url();?>front/Settings/update_product", 
		    data: 'product_id='+product_id+"&product_name="+product_name+"&product_description="+product_description+"&product_price="+product_price,
		    success: function(data){
		    	$('#product_details_table').html("");
				$('#product_details_table').html(data);
				// $('#editModel'+product_id).modal('hide');
				// $("#product_name"+product_id).val("");
				// $("#product_description"+product_id).val("");
				// $("#product_price"+product_id).val("");
			    $("#loader").hide();
			    //alert(data);
				
		  	}
		  });
		}
		else{
			if(product_name==""){
				$("#errmsg_product_name"+product_id).html("Enter the value").show().fadeOut(1500);
			}
			if(product_description==""){
				$("#errmsg_product_description"+product_id).html("Enter the Value").show().fadeOut(1500);
			}
			if(product_price==""){
				$("#errmsg_product_price"+product_id).html("Enter the Value").show().fadeOut(1500);
			}
		}
		
	}
	function delete_product(product_id){
    $("#loader").show();
	  $.ajax({
      type: "POST",
      url: "<?php echo base_url();?>front/Settings/delete_product", 
      data: 'product_id='+product_id,
      success: function(data){
      	$('#product_details_table').html("");
        $('#product_details_table').html(data);
        //$('#deleteModel'+product_id).modal('hide');
        $("#loader").hide();
        
    	}
		});
	}
		function add_discount(user_id,add_discount_status){
    $("#loader").show();
	  $.ajax({
      type: "POST",
      url: "<?php echo base_url();?>front/Settings/add_discount", 
      data: 'user_id='+user_id+"&add_discount_status="+add_discount_status,
      success: function(data){
      	$("#loader").hide();
      	$('#modal_msg').modal('show');
		$('#txtMsg').html(data);
        
    	}
		});
	}
	function add_extra_charges(user_id,add_extra_charges_status){
    $("#loader").show();
	  $.ajax({
      type: "POST",
      url: "<?php echo base_url();?>front/Settings/add_extra_charges", 
      data: 'user_id='+user_id+"&add_extra_charges_status="+add_extra_charges_status,
      success: function(data){
      	 $("#loader").hide();
      	$('#modal_msg').modal('show');
		$('#txtMsg').html(data);
       
        
    	}
		});
	}
</script>
<script src="//cdn.ckeditor.com/4.13.0/full/ckeditor.js"></script>
<script>
$(document).ready(function() {
	CKEDITOR.replace('project_description', {
		allowedContent:true,
	});
});
function activate(user_id)
{
	var checkBox = document.getElementById("chkActivatenotification");
  // If the checkbox is checked, display the output text
  if (checkBox.checked == true){
    var status=1;
  } else {
    var status=0;
  }
  
  $.ajax({
      type: "POST",
      url: "<?php echo base_url();?>front/Settings/update_notifications", 
      data: "user_id="+user_id+"&status="+status,
		success: function(data){
      	//$('#product_details_table').html("");
        //$('#product_details_table').html(data);
        //$('#deleteModel'+product_id).modal('hide');
        //$("#loader").hide();
        //alert(data);
		$('#modal_msg').modal('show');
			$('#txtMsg').html(data);
    	}
		});
  
  
	//alert(status);
}
</script>
<?php $this->load->view('front/includes/footer'); ?>
