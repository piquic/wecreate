<?php
session_start();
error_reporting(0);
$data['page_title'] = "Proofing";

// $this->load->view('front/includes/header',$data);


// // $this->load->view('front/includes/menu');
// $this->load->view('front/includes/nav');


if ($this->session->userdata('front_logged_in')) {
	$session_data = $this->session->userdata('front_logged_in');
	$user_id = $session_data['user_id'];
	$user_name = $session_data['user_name'];
	$_SESSION['user_id']=$user_id;
	$_SESSION['user_name']=$user_name;
	$type1=$this->session->userdata('type1');

}
else
{
	$user_id = '';
	$user_name = ''; 
	$_SESSION['user_id']=$user_id;
	$_SESSION['user_name']=$user_name;
}
$this->load->view('front/includes/header',$data);
$this->load->view('front/includes/topnav');
?>
<!-----------poulami start-------------->
	<!-- <link rel="stylesheet" href="<?php echo base_url();?>/website-assets/folderTree/css/filetree.css" type="text/css" >
	-->
	<!-----------poulami end-------------->

<div class="container-fluid">
	<div class="row">

		<!-- Main Body -->
		<div class="col-12 col-sm-12 col-md-12">
			<main class="p-3">

				<div class="row">
					<div class="col-12 col-sm-12 col-md-12">
						<div class="p-3">
							<a href="<?php echo base_url() ?>dashboard"><p class="text-wc lead"><i class="fas fa-arrow-alt-circle-left"></i>&nbsp;Back to Dashboard</p></a>
						</div>
					</div>
				</div>

				<div class="row">
					<div class="col-12 col-sm-12 col-md-12">
						<div class="p-3 text-wc">
							<h3 class="h3 text-uppercase">
								<?php
								$wc_brief_sql = "SELECT * FROM `wc_brief` where brief_id='$brief_id'";
								$wc_brief_query = $this->db->query($wc_brief_sql);

								$wc_brief_result=$wc_brief_query->result_array();
								//print_r($wc_brief_result);
								// $wc_brief_qrydata = mysqli_fetch_array($wc_brief_query);
								$brief_title=$wc_brief_result[0]['brief_title'];
								echo $brief_title." (ID: ".$brief_id.")";
								?>
							</h3>
						</div>

						<div class="p-3 text-wc d-flex justify-content-start">
							<div class="p-1">
								<select class="custom-select">
									<option selected>Revision</option>
									<option value="r1">Revision 1</option>
									<option value="r2">Revision 2</option>
									<option value="r3">Revision 3</option>
								</select>
							</div>

							<div class="p-1">
								<select class="custom-select">
									<option selected>Download</option>
									<option value="d1">Download 1</option>
									<option value="d2">Download 2</option>
									<option value="d3">Download 3</option>
								</select>
							</div>
						</div>
					</div>
				</div>

				<div class="row" id='image_click'>
					
				</div>

				<div class="row">
					<div class="col-12 col-sm-12 col-md-12">
						<div class="p-4">

							<div class="row">
								<?php   
						//echo "<pre>";
						$files_sql = "SELECT * FROM `wc_image_upload` where brief_id='$brief_id'";
						$files_query = $this->db->query($files_sql);
						$files_result=$files_query->result_array();
						//print_r($files_result);
						foreach($files_result as $keybill => $files_result1) {
							
							$img_status=$files_result1['img_status'];
							$image_path=$files_result1['image_path'];
							$img_title=$files_result1['img_title'];
							$image_id=$files_result1['image_id'];
							//$files_name=explode('.', $image_path);
							// $img_status=$files_result1['status'];
							//print_r($files_name[0]);
          				?>                 
			          		<div class="col-12 col-sm-12 col-md-3 p-2">
									<div class="card">
										<img src="<?php echo base_url(); ?>brief_upload/<?php echo $brief_id ?>/image/<?php echo $image_path ?>" class="card-img-top" alt="<?php echo $img_title ?>" onclick="useimg('<?php echo $brief_id ?>','<?php echo $image_path ?>','<?php echo $image_id ?>','<?php echo $img_status ?>')">
										<div class="card-body">
											<div class="d-flex justify-content-between">
												<span class="lead"><?php echo $img_title ?></span>
												<?php
												if($img_status=="1") {
												?>
												<i class="fas fa-circle text-wc fa-2x"></i>
												<?php
												}
												elseif($img_status=="2") {
												?>
												<i class="fas fa-circle text-danger fa-2x"></i>
												<?php
												}elseif($img_status=="3") 
												{
												?>
												<i class="fas fa-sync-alt fa-2x"></i>
												<?php
												}
												
												?>
												
											</div>
										</div>
									</div>
								</div>
                          	<?php 
                      			}
                      		?>
						
							</div>
						</div>
					</div>
				</div>
			</main>
		</div>

	</div>
</div>

<div class="w-100 p-3 border-top bg-light">
	<h4>Notes:</h4>
	<textarea class="form-control bg-light" rows="10"></textarea>
</div>


<?php
$this->load->view('front/includes/footer');
?>


<script src='<?php echo base_url();?>/website-assets/js/dragdropfolder.js'></script>

<script type="text/javascript">


$(document).ready(function(){
<?php

$main_folder_sql = "SELECT * FROM `wc_image_upload` where brief_id='$brief_id'";
$main_folder_query = $this->db->query($main_folder_sql);

$main_folder_result=$main_folder_query->result_array();

$img_status=$main_folder_result[0]['img_status'];
$image_id=$main_folder_result[0]['image_id'];
$image_path=$main_folder_result[0]['image_path'];
?>
	useimg('<?php echo $brief_id; ?>','<?php echo $image_path; ?>','<?php echo $image_id; ?>','<?php echo $img_status; ?>');
<?php
	if($img_status=="2" || $img_status=="3") {
?>
review('<?php echo $image_id; ?>','<?php echo $brief_id; ?>','<?php echo $image_path; ?>');
<?php
}
?>
});
function updateApprove(image_id,brief_id)
{
	$.ajax({
		type: "POST", 
		url: "<?php echo base_url()?>front/proofing/update_status", 
		data: "&image_id="+image_id+"&brief_id="+brief_id+"&img_status=1",
		success: function(response){
			//alert(data);
			if(response=="1")
			{
				$("#update_approve").html('<button type="submit" class="btn btn-wc btn-block m-2 px-5">&emsp;&emsp;Approved&nbsp;<i class="fas fa-check"></i>&emsp;&emsp;</button>');
			}
	  	}
	});

}

function review(image_id,brief_id,image_path)
{
	//alert("jkh");
	$.ajax({
		type: "POST", 
		url: "<?php echo base_url()?>front/proofing/review", 
		data: "&image_id="+image_id+"&brief_id="+brief_id+"&image_path="+image_path,
		success: function(response){
			$('#image_click').empty();
			$('#image_click').append(response);
			viewtag(image_id);
			annotation(image_id);
	  	}
	});

} 

function sendRevision(image_id,brief_id)
{

		$.ajax({
			type: "POST", 
			url: "<?php echo base_url()?>front/proofing/update_status", 
			data: "&image_id="+image_id+"&brief_id="+brief_id+"&img_status=3",
			success: function(response){
				if(response=="1")
				{
					$("#sendRevision").html('<button type="submit" class="btn btn-outline-wc btn-block px-5 mb-2">&emsp;&emsp;<?php echo $brief_title; ?> Sent&nbsp;<i class="fas fa-sync-alt"></i>&emsp;&emsp;</button>');
				}
		  	}
		});
	 
}

function useimg(brief_id,image_path,image_id,img_status){
	if(img_status=='1' || img_status=='0')
	{
		$.ajax({
			type: "POST", 
			url: "<?php echo base_url()?>front/proofing/use_img", 
			data: "brief_id="+brief_id+"&image_path="+image_path+"&image_id="+image_id+"&img_status="+img_status,
			success: function(response){
				$('#image_click').empty();
				$('#image_click').append(response);
			}
		});
	}
	else{
		review(image_id,brief_id,image_path);
	}
}

function viewtag( image_id )
{
	// alert("Hi viewtag fun called....");
	//  alert(image_id);
	// get the tag list with action remove and tag boxes and place it on the image.
  	$.post( "<?php echo base_url()?>front/proofing/annotation_list" ,  "image_id=" + image_id, function( data ) {
	     //console.log(data.lists);
	    $('#list-id').empty();
	    $('#list-id').html(data.lists);
	    // $('#tagbox'+image_id).empty();
	    $('#tagbox').html(data.boxes);
	}, "json");
} 
function btnsave(image_id,mouseX,mouseY)
{
	var descr = $('#tagname').val();

	// tagid=$('#tag_id').val();
	//alert(id);
	// alert(name);
	//alert(img_src);
	$.ajax({
		type: "POST", 
		url: "<?php echo base_url()?>front/proofing/annotation_crud", 
		data: "image_id=" + image_id +"&descr=" + descr + "&position_left=" + mouseX + "&position_top=" + mouseY + "&type=insert",
		cache: true, 
		success: function(data){
			// alert(data);
			viewtag(image_id);
			$('#tagit').fadeOut();
		}
	});
}

function edit_button(id){
	//  alert("work edit");
	$("#name"+id).removeClass("d-none");
	$("#span_name"+id).addClass("d-none");
	$("#editBtn"+id).addClass("d-none");
	$("#delete_"+id).addClass("d-none");
	$("#updateBtn"+id).removeClass("d-none");
	$("#cancleBtn"+id).removeClass("d-none");    
}
function cancle_button(id){
	$("#name"+id).addClass("d-none");
	$("#span_name"+id).removeClass("d-none");
	$("#editBtn"+id).removeClass("d-none");
	$("#delete_"+id).removeClass("d-none");
	$("#updateBtn"+id).addClass("d-none");
	$("#cancleBtn"+id).addClass("d-none");

}
function cancle_button1(id){
	$("#name"+id).addClass("d-none");
	$("#span_name"+id).removeClass("d-none");
	$("#editBtn"+id).removeClass("d-none");
	$("#delete_"+id).removeClass("d-none");
	$("#remove_"+id).addClass("d-none");
	$("#cancleBtn1"+id).addClass("d-none");
	$("")
}
function remove1(id)
{
	//alert(id);
	$("#remove_"+id).removeClass("d-none");
	$("#delete_"+id).addClass("d-none");
	$("#cancleBtn1"+id).removeClass("d-none");
	$("#editBtn"+id).addClass("d-none");

}
function remove(id,image_id)
{
  	$.ajax({
        type: "POST", 
        url: "<?php echo base_url()?>front/proofing/annotation_crud", 
        data: "id=" + id + "&type=remove",
        success: function(data) {
			var img = $('#imgtag').find( 'img' );
			var img_id = $(img).attr( 'id' );
			//get tags if present
			// var files_id=$('#files_id').val(); 
			viewtag(image_id);
    	}
  	});
	//}
}
function annotation(image_id){
	//alert("clicked");
	var counter = 0;
	var mouseX = 0;
	var mouseY = 0;
	$('img[id^="imgtag"]').click(function(e) {
		// make sure the image is click
		//  imgtag=this.id;
		var imgtag = $(this).parent();
		// var img_id=this.id;
		// var id=img_id.substring(6)
		mouseX = ( e.pageX - $(imgtag).offset().left ) - 10; // x and y axis
		mouseY = ( e.pageY - $(imgtag).offset().top ) - 10;
		// alert(mouseX);
		// alert(mouseY);
		$( '#tagit').remove(); // remove any tagit div first
		$(imgtag).append( '<div id="tagit" ><div class="box"></div><div class="name editor" ><textarea class="editor-text" type="text" name="txtname" id="tagname" placeholder="Add a Comment..." ></textarea><input class="editor-button" type="button" name="btnsave" value="Save" id="btnsave" onclick=btnsave('+image_id+','+mouseX+','+mouseY+') /><input class="editor-button" type="button" name="btncancel" value="Cancel" id="btncancel" /></div></div>' );

		$( '#tagit' ).css({ top:mouseY, left:mouseX });

		$('#tagname').focus();
	});

	// Cancel the tag box.
	$( document ).on( 'click', '#tagit #btncancel', function() {
		$('#tagit').fadeOut();
	});
	
	  // mouseover the taglist 
	$('div[id^="taglist"]').on( 'mouseover', 'tr', function( ) {
	    id = $(this).attr("id");
	    //alert(id);
	    $('#view_' + id).css({ opacity: 1.0 });
	 }).on( 'mouseout', 'tr', function() {
	    $('#view_' + id).css({ opacity: 0.0 });
	 });
	

	// load the tags for the image when page loads.
	var img = $('#imgtag').find( 'img' );
	var id = $( img ).attr( 'id' );
}


function update_button(image_id,id,position_left,position_top)
{

	// alert("Update");
	var descr = $("#name"+id).val();  
	
	$.ajax({
		type: "POST", 
		url: "savetag.php", 
		url: "<?php echo base_url()?>front/proofing/annotation_crud", 
		data: "id="+id+"&image_id=" + image_id +"&descr=" + descr + "&position_left=" + position_left + "&position_top=" + position_top + "&type=update",

		cache: true, 
		success: function(data){
			//alert(data);
			viewtag( image_id );
			$('#tagit').fadeOut();   
			$("#span_name"+id).removeClass("d-none");
			$("#editBtn"+id).removeClass("d-none");
			$("#delete_"+id).removeClass("d-none");
			$("#updateBtn"+id).addClass("d-none");
			$("#cancleBtn"+id).addClass("d-none"); 
			$("#name"+id).addClass("d-none");

		}
	});

}
</script>