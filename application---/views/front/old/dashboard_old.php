<?php
session_start();
error_reporting(0);

//echo $user_type_id;
 $checkquerys = $this->db->query("select user_type_name from wc_users_type where  user_type_id='$user_type_id' ")->result_array();
$user_type_name_sel=$checkquerys[0]['user_type_name'];

 $checkquerys = $this->db->query("select * from wc_users where  user_id='$user_id' ")->result_array();
$brand_id_str=trim($checkquerys[0]['brand_id'],",");



//echo "<pre>";print_r($dashboradinfo);

if(!empty($dashboradinfo))
{

//print_r($labelimagesinfo);
foreach($dashboradinfo as $row){ 
$brief_id=$row->brief_id;
$brief_title=$row->brief_title;
$brief_doc=$row->brief_doc;
$brief_due_date=$row->brief_due_date;
$brief_due_date= date('d F Y H:i', strtotime($brief_due_date));
$status=$row->status;

	if($status==0)
	{
        $brief_review[]=$brief_id;
	}

	if($status==1)
	{
        $work_progress[]=$brief_id;
	}

	if($status==2)
	{
        $work_completed[]=$brief_id;
	}

	if($status==3)
	{
        $revision_work[]=$brief_id;
	}
	if($status==4)
	{
        $brief_rejected[]=$brief_id;
	}
	if($status==5)
	{
        $feedback_pending[]=$brief_id;
	}
	if($status==6)
	{
        $proofing_pending[]=$brief_id;
	}
	



}
}

//echo "<pre>";print_r($brief_review);

?>

<?php $data['page_title'] = "upload image";

$this->load->view('front/includes/header',$data);
$this->load->view('front/includes/topnav');
 ?>

<div class="container-fluid">
	<div class="row">

		<!-- Sidebar -->
		<div class="col-12 col-sm-12 col-md-3">
			<?php
		$this->load->view('front/includes/sidebar');

		?>
		</div>

		<!-- Main Body -->
		<div class="col-12 col-sm-12 col-md-9">
			<main class="border-left p-3">

				<div class="row">
					<div class="col-12 col-sm-12 col-md-8">
						<div class="p-3">
							<h2 class="text-wc">Dashboard (<?php echo $user_type_name_sel;?>)</h2>
						</div>
					</div>

					<div class="col-12 col-sm-12 col-md-4">
						<div class="p-3 text-md-right">
							<form>
								<input type="text" class="form-control" id="brief_id_search" name="brief_id_search" placeholder="Search Brief Id">
							</form>
						</div>
					</div>
				</div>

				<div class="row">
					<div class="col-12 col-sm-12 col-md-12">
						<div class="p-3">
							<table id="tblbrf" class="table lead">
								<thead>
									<tr class="table-success border-bottom">
										<th scope="col">ID</th>
										<th scope="col">Title</th>
										<th scope="col">Brief</th>
										<th scope="col">Due Date</th>
										<th scope="col">Status</th>
									</tr>
								</thead>
								


<?php
			if(!empty($dashboradinfo))
			{
				?>
				<tbody>

				<?php
				//print_r($labelimagesinfo);
				foreach($dashboradinfo as $row){ 
					$brief_id=$row->brief_id;
					$brief_title=$row->brief_title;
					$brief_doc=$row->brief_doc;
					$brief_due_date=$row->brief_due_date;
					$brief_due_date= date('d F Y H:i', strtotime($brief_due_date));
					$status=$row->status;
					$brand_id=$row->brand_id;
					$rejected_reasons=$row->rejected_reasons;

					?>

					<tr <?php if($status!='4') { ?> class="border-bottom" <?php } ?> >
										<th scope="row"><?php echo $brief_id;?></th>
										<td><?php echo $brief_title;?>
										<?php
										if($status!='0' && $status!='4')
										{
										?>
										<br><br><a class="btn btn-outline-wc" href="">View Files</a>
										<?php
										}
										?>
									    </td>


										<td>
										<?php
										if($status=='0' )
										{
										?>
										<i class="fas fa-sync icon-md px-1"></i>
										<?php
										}
										else if($status=='4')
										{
										?>
										<span class="sts-ico-f">PP</span>
										<?php
										}
										else 
										{
										?>
										<span class="sts-ico-s">PP</span>
										<?php
										}
										?>	

											

										</td>
										<td>
										<?php
										if($status=='0' )
										{
										echo "N/A";
										}
										else
										{
											echo $brief_due_date;
										}
										?>

											
												

											</td>
										<td>

										<?php
										if($status=='0' )
										{
										?>
										Brief in Review
									<?php
									$accept_reject_permission=$this->Dashboard_model->checkPermission($user_type_id,3,'accept_reject');
									if($accept_reject_permission=='1')
									{
									?>


										<br><br>
											<select class="custom-select mb-3">
												<option value="approve" selected>Approve</option>
												<option value="reject">Reject</option>
											</select><br><br>

											Delivery Date:

											<div class="input-group mb-2 mr-sm-2">
												<div class="input-group-prepend">
													<div class="input-group-text"><i class="far fa-calendar-alt"></i></div>
												</div>
												<input type="text" class="form-control" id="datePick" placeholder="00-00-0000 00:00">
											</div>

											<div class="row pt-3">
												<div class="col-12 col-sm-12 col-md-4"></div>
												<div class="col-12 col-sm-12 col-md-4"><button type="submit" class="btn btn-outline-danger btn-block">Cancel</button></div>
												<div class="col-12 col-sm-12 col-md-4"><button type="submit" class="btn btn-outline-wc btn-block">Confirm</button></div>
											</div>
										<?php
										}
										?>

										<?php
										}
										else if($status=='1')
										{
										?>
										Work in Progress
										<?php
										}
										else if($status=='2')
										{
										?>
										Work Completed
										<?php
										}
										else if($status=='3')
										{
										?>
										Revision Work

										<?php
										$proofing_permission=$this->Dashboard_model->checkPermission($user_type_id,4,'view');
										if($proofing_permission=='1')
										{
										?>

										<br><br><a class="btn btn-outline-wc" href="">Open Proofing</a>
										<?php
										}?>


										<?php
										}
										else if($status=='4')
										{
										?>
										Brief Rejected
										<?php
										}
										else if($status=='5')
										{
										?>
										Feedback Pending
										<?php
										if($user_type_id=='4')
										{
										?>

										<br><br><a class="btn btn-outline-wc" href="">Add Feedback</a>
										<?php
										}?>
										<?php
										}
										else if($status=='6')
										{
										?>
										Proofing Pending
										<?php
										}
										else 
										{
										?>
										
										<?php
										}
										?>	

										

										



										</td>
									</tr>

									
									<?php
										if($status=='4')
										{
										?>
										<tr class="border-bottom">										
										<th class="border-0" scope="row"></th>
										<td class="border-0" colspan="4">
											<div class="d-flex justify-content-around flex-wrap">
												<div class="mt-3 w-50 text-justify">

													<?php
													if($rejected_reasons!='')
													{
														?>
										<div class="border px-3 pb-3">
										<span class="b-text bg-white p-2">REASON FOR REJECTION</span><br>
										<span><?php echo $rejected_reasons;?></span>
										</div>
														<?php

													}
													?>
													

											<?php
											$accept_reject_permission=$this->Dashboard_model->checkPermission($user_type_id,3,'accept_reject');
											if($accept_reject_permission=='1' )
											{
											?>

											<div class="row pt-3">
											<div class="col-12 col-sm-12 col-md-4"></div>
											<div class="col-12 col-sm-12 col-md-4"><button type="submit" class="btn btn-outline-danger btn-block">Cancel</button></div>
											<div class="col-12 col-sm-12 col-md-4"><button type="submit" class="btn btn-outline-wc btn-block">Confirm</button></div>
											</div>

											<?php
											}
											?>

												</div>

												<div class="px-3 py-5 text-center text-wc">
													<a class="text-wc" href="<?php echo base_url();?>brief_upload/<?php echo $brief_id;?>/<?php echo $brief_doc;?>"><span class="m-3 px-3 py-4 border rounded-circle"><i class="fas fa-download fa-2x"></i></span><br><br>Download Rejected<br> Brief</a>
												</div>
												<?php
											if($user_type_id=='4' )
											{
											?>

										<div class="px-3 py-5 text-center text-wc">

									<label class="w3-button w3-blue w3-round">
									<span class="m-3 px-3 py-4 border rounded-circle"><i class="fas fa-download fa-2x"></i></span>
									<input type="file" style="display: none" class="re_upload_doc fas fa-download fa-2x" id="files1" alt='1' name="files1[]" multiple val="<?php echo $brief_id;?>" />
									</label>
									<br><br>Re-Upload<br> Brief

										<!-- <a class="text-wc " href="#"><span class="m-3 px-3 py-4 border rounded-circle">


										
										

										<i class="fas fa-download fa-2x">
											

										</i></span>


										<br><br>Re-Upload<br> Brief</a> -->



												</div>
											</div>
											<?php
											}
											?>

										</td>
									</tr>
										<?php
										}	
										?>


									


					<?php
					}

					?>



				





								</tbody>

					<?php
					}
					?>

							</table>
						</div>
					</div>
				</div>

			</main>
		</div>

	</div>
</div>

<div class="w-100 p-3 border-top bg-light">
	<h4>Notes:</h4>
	<textarea class="form-control bg-light" rows="10"></textarea>
</div>


<?php
$this->load->view('front/includes/footer');
?>

<script type="text/javascript">

	<?php if($brand_id_sel!='' && $brand_id_sel!='-') { 
		?>
		selectAccount('<?php echo $brand_id_sel;?>');
		<?php

	} ?>  
	function selectAccount(brand_id_sel)
	{

		//alert(brand_id_sel);
		 var account_id=$('#account_id').val();
		 $.ajax({
	   			method:'POST',
	   			url: "<?php echo base_url();?>front/dashboard/getBrandDropDown",
	   			data:"&account_id="+account_id+"&brand_id_sel="+brand_id_sel,

	   			success: function(result){
	   				//alert(result);
	   				if(result) {
	   					$("#divBrand").html(result);
	   					
	   				}
	   			}
	   		});
	}



	function filterStatus(brief_status)
	{

		
		 var account_id=$('#account_id').val();
		 var brand_id=$('#brand_id').val();
		 var brief_id_search=$('#brief_id_search').val();

		alert(brief_status+"--"+brief_id_search+"--"+brand_id+"--"+account_id);
		
		if(brief_status=='')
		 {
		 	brief_status="-";
		 }
		 if(brand_id=='')
		 {
		 	brand_id="-";
		 }
		 if(account_id=='')
		 {
		 	account_id="-";
		 }
		 if(brief_id_search=='')
		 {
		 	brief_id_search="-";
		 }

		 var url="<?php echo base_url();?>dashboard/"+brief_status+"/"+brief_id_search+"/"brand_id+"/"account_id+"/";

		 alert(url);

        window.location.href=url;
        
	}






$(".re_upload_doc").on("change", function(e) {

 //alert("goko");
var no = $(this).attr('alt') ;
var brief_id= $(this).attr('val') ;

 alert(brief_id);

var files = e.target.files,
filesLength = files.length;
for (var i = 0; i < filesLength; i++) {
	var f = files[i]
	var fileReader = new FileReader();
	fileReader.onload = (function(e) {
		var file = e.target;

	  




//alert(no);
var input = document.getElementById("files"+no);
file = input.files[0];
if(file != undefined){
	formData= new FormData();

	var ext = $("#files"+no).val().split('.',2).pop().toLowerCase();

	//alert(ext);


	//if ($.inArray(ext, ['doc','docx']) == 1)
    if (ext=='doc' || ext=='docx')
	{
		formData.append("doc_file", file);
		formData.append("brief_id", brief_id);


  //alert("aaaa");
  $.ajax({
  	url: "<?php echo base_url();?>front/dashboard/upload_file",
  	type: "POST",
  	data: formData,
  	processData: false,
  	contentType: false,
  	success: function(data){

	 //alert("koko");
     alert(data);



}
});
}else{

	alert("Please Upload Doc");
	return false;




}


}
else{
alert('Input something!');
}

    

});
fileReader.readAsDataURL(f);
}


});






</script>
