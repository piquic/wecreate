<?php
session_start();
error_reporting(0);
if ($this->session->userdata('front_logged_in')) {
$session_data = $this->session->userdata('front_logged_in');
$user_id = $session_data['user_id'];
$user_name = $session_data['user_name'];
$_SESSION['user_id']=$user_id;
$_SESSION['user_name']=$user_name;
//$type1=$this->session->userdata('user_type_id');
 $type1 = $session_data['user_type_id'];
 $brand_access=$session_data['brand_access'];
	if($type1==3)
	{
		$job_title="Project Manager";
	}
	else
	{
		$job_title="Branch Manager";
	}
}
else
{
$user_id = '';
$user_name = ''; 
$_SESSION['user_id']=$user_id;
$_SESSION['user_name']=$user_name;
}
?>

<?php $data['page_title'] = "upload image";

$this->load->view('front/includes/header',$data);
$this->load->view('front/includes/topnav');
 ?>

<div class="container-fluid">
	<div class="row">

		<!-- Main Body -->
		<div class="col-12 col-sm-12 col-md-12">
			<main class="p-3">

				<div class="row">
					<div class="col-12 col-sm-12 col-md-12">
						<div class="p-3">
							<p class="text-wc lead"><i class="fas fa-arrow-alt-circle-left"></i><a href="dashboard">&nbsp;Back to Dashboard</a></p>
						</div>
					</div>
				</div>

				<div class="row">
					<div class="col-12 col-sm-12 col-md-12">
						<div class="p-3 text-wc">
							<h3 class="h3 text-uppercase">
								Bio Oil Banners For March (ID: 2) - Project Manager
							</h3>
						</div>

						<div class="p-3 text-wc d-flex justify-content-start">
							<div class="p-1">
								<select class="custom-select">
									<option selected>Status</option>
									<option value="s1">Status 1</option>
									<option value="s2">Status 2</option>
									<option value="s3">Status 3</option>
								</select>
							</div>

							<div class="p-1">
								<select class="custom-select">
									<option selected>Download</option>
									<option value="d1">Download 1</option>
									<option value="d2">Download 2</option>
									<option value="d3">Download 3</option>
								</select>
							</div>

							<div class="p-1">
								<select class="custom-select" onchange="uploadimages()">
									<option selected>Upload</option>
									<option value="u1">Upload 1</option>
									<option value="u2">Upload 2</option>
									<option value="u3">Upload 3</option>
								</select>
							</div>

							<div class="p-1">
								<select class="custom-select">
									<option selected>Delete</option>
									<option value="del1">Delete 1</option>
									<option value="del2">Delete 2</option>
									<option value="del3">Delete 3</option>
								</select>
							</div>
						</div>
					</div>
				</div>

				<div class="row">
					<div class="col-12 col-sm-12 col-md-12">
						<div class="p-4">

							<div class="row">
								<div class="col-12 col-sm-12 col-md-3 p-2">
									<div class="card">
										<img src="..." class="card-img-top" alt="...">
										<div class="card-body">
											<div class="d-flex justify-content-between">
												<span class="lead">Image Name</span>

												<i class="fas fa-circle text-danger fa-2x"></i>
											</div>
										</div>
									</div>
								</div>

								<div class="col-12 col-sm-12 col-md-3 p-2">
									<div class="card">
										<img src="..." class="card-img-top" alt="...">
										<div class="card-body">
											<div class="d-flex justify-content-between">
												<span class="lead">Image Name</span>

												<i class="fas fa-sync-alt fa-2x"></i>
											</div>
										</div>
									</div>
								</div>

								<div class="col-12 col-sm-12 col-md-3 p-2">
									<div class="card">
										<img src="..." class="card-img-top" alt="...">
										<div class="card-body">
											<div class="d-flex justify-content-between">
												<span class="lead">Image Name</span>

												<i class="fas fa-circle text-wc fa-2x"></i>
											</div>
										</div>
									</div>
								</div>

								<div class="col-12 col-sm-12 col-md-3 p-2">
									<div class="card">
										<img src="..." class="card-img-top" alt="...">
										<div class="card-body">
											<div class="d-flex justify-content-between">
												<span class="lead">Image Name</span>

												<i class="fas fa-circle text-wc fa-2x"></i>
											</div>
										</div>
									</div>
								</div>

								<div class="col-12 col-sm-12 col-md-3 p-2">
									<div class="card">
										<img src="..." class="card-img-top" alt="...">
										<div class="card-body">
											<div class="d-flex justify-content-between">
												<span class="lead">Image Name</span>

												<i class="fas fa-circle text-danger fa-2x"></i>
											</div>
										</div>
									</div>
								</div>

								<div class="col-12 col-sm-12 col-md-3 p-2">
									<div class="card">
										<img src="..." class="card-img-top" alt="...">
										<div class="card-body">
											<div class="d-flex justify-content-between">
												<span class="lead">Image Name</span>

												<i class="fas fa-sync-alt fa-2x"></i>
											</div>
										</div>
									</div>
								</div>

								<div class="col-12 col-sm-12 col-md-3 p-2">
									<div class="card">
										<img src="..." class="card-img-top" alt="...">
										<div class="card-body">
											<div class="d-flex justify-content-between">
												<span class="lead">Image Name</span>

												<i class="fas fa-circle text-wc fa-2x"></i>
											</div>
										</div>
									</div>
								</div>

							</div>
						</div>
					</div>
				</div>
			</main>
		</div>

	</div>
</div>
<script>
function sortbrief() {
$(document).ready(function() {
        var myCheckboxes = new Array();
        $("input:checked").each(function() {
           myCheckboxes.push($(this).val());
        });
		var brand_id = $("#brand_id").val()
        $.ajax({
            type: "POST",
            url: "<?php echo base_url();?>front/dashboard/getbriefsort", 
            dataType: 'html',
            data: 'brand_id='+brand_id+'&myCheckboxes='+myCheckboxes,
            success: function(data){
                $('#myResponse').html(data)
               getbir(brand_id);
			   getbr(brand_id);
			   getwip(brand_id);
			   getrw(brand_id);
			   getpp(brand_id);
			   getwc(brand_id);
			   getfp(brand_id);
            }
        });
		
        return false;
});
}
function getbir(brand_id)
{

	$.ajax({
            type: "POST",
            url: "<?php echo base_url();?>front/dashboard/getbir", 
            dataType: 'html',
            data: 'brand_id='+brand_id,
            success: function(data){
				//alert(data);
                $('#bir').html(data)			
            }
        });
}


function getbr(brand_id)
{
	$.ajax({
            type: "POST",
            url: "<?php echo base_url();?>front/dashboard/getbr", 
            dataType: 'html',
            data: 'brand_id='+brand_id,
            success: function(data){
				//alert(data);
                $('#br').html(data)			
            }
        });
}

function getwip(brand_id)
{
	$.ajax({
            type: "POST",
            url: "<?php echo base_url();?>front/dashboard/getwip", 
            dataType: 'html',
            data: 'brand_id='+brand_id,
            success: function(data){
				//alert(data);
                $('#wip').html(data)			
            }
        });
}


function getwc(brand_id)
{
	$.ajax({
            type: "POST",
            url: "<?php echo base_url();?>front/dashboard/getwc", 
            dataType: 'html',
            data: 'brand_id='+brand_id,
            success: function(data){
				//alert(data);
                $('#wc').html(data)			
            }
        });
}

function getrw(brand_id)
{
	$.ajax({
            type: "POST",
            url: "<?php echo base_url();?>front/dashboard/getrw", 
            dataType: 'html',
            data: 'brand_id='+brand_id,
            success: function(data){
				//alert(data);
                $('#rw').html(data)			
            }
        });
}

function getpp(brand_id)
{
	$.ajax({
            type: "POST",
            url: "<?php echo base_url();?>front/dashboard/getpp", 
            dataType: 'html',
            data: 'brand_id='+brand_id,
            success: function(data){
				//alert(data);
                $('#pp').html(data)			
            }
        });
}

function getfp(brand_id)
{
	$.ajax({
            type: "POST",
            url: "<?php echo base_url();?>front/dashboard/getfp", 
            dataType: 'html',
            data: 'brand_id='+brand_id,
            success: function(data){
				//alert(data);
                $('#fp').html(data)			
            }
        });
}


function uploadimages()
{
	//alert('hi');
		url="<?php echo base_url();?>uploadimages"
	window.location = url;
}




function getbranddetails(brand_id)
{
	alert(brand_id);
}




</script>

<?php
$this->load->view('front/includes/footer');
?>
