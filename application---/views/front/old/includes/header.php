

<?php //echo base_url(); ?>
<!doctype html>
<html lang="en">
<head>
	<!-- Required meta tags -->
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

	<!-- Bootstrap CSS -->
	<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>website-assets/css/bootstrap.min.css">

	<!-- Fontawesome -->
	<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>website-assets/fontawesome/css/all.css">

	<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>website-assets/css/custom.css">




	<script src="https://code.jquery.com/jquery-1.12.4.js"></script>
	<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>



	<title><?php echo ucwords($page_title); ?> - WeCreate</title>

	<script type="text/javascript">

	function myCheckBrowserFunction() {

	//console.log(navigator.userAgent);
	if((navigator.userAgent.indexOf("Opera") || navigator.userAgent.indexOf('OPR')) != -1 )
	{
	//alert('Opera');
	return 'Opera';
	}
	else if(navigator.userAgent.indexOf("Chrome") != -1 )
	{
	//alert('Chrome');
	return 'Chrome';
	}
	else if(navigator.userAgent.indexOf("Safari") != -1)
	{
	//alert('Safari');
	return 'Safari';
	}
	else if(navigator.userAgent.indexOf("Firefox") != -1 )
	{
	//alert('Firefox');
	return 'Firefox';
	}
	else if((navigator.userAgent.indexOf("MSIE") != -1 ) || (!!document.documentMode == true )) //IF IE > 10
	{
	//alert('IE');
	return 'IE';
	}
	else
	{
	//alert('unknown');
	return 'unknown';
	}
	}



	</script>

	
			<script type="text/javascript">
				function goDashboard()
				{
					//alert("aaaaa");
					window.location.href="<?php echo base_url();?>dashboard";
				}
			</script>
</head>
<body id="<?php echo str_replace(" ","-",$page_title); ?>">
	<div id="loader" style="display: none;"><i class="fas fa-spinner fa-spin icon"></i></div>

	<div class="modal fade" id="modal_msg" tabindex="-1" role="dialog" aria-hidden="true">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<h5 class="modal-title text-dark" id="txtMsg"></h5>
					<button type="button" class="close text-wc" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true" >&times;</span>
					</button>
				</div>
			</div>
		</div>
	</div>