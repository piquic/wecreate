<?php
$class_name=$this->router->fetch_class();
$method_name=$this->router->fetch_method();


if ($this->session->userdata('front_logged_in')) {
	$session_data = $this->session->userdata('front_logged_in');
	$user_id = $session_data['user_id'];
	$user_name = $session_data['user_name'];
	$type1=$this->session->userdata('type1');
}
else
{
	$user_id = '';
	$user_name = '';
}


$temp = explode(' ', $user_name);

// print_r($temp);

if(isset($temp[1]) != '')
	$uico = substr($temp[0], 0, 1) . substr($temp[1], 0, 1);
else
	$uico = substr($temp[0], 0, 1); 



?>
<!-- <header>
	<nav class="navbar navbar-expand-lg navbar-light bg-light">
		<a class="navbar-brand" href="#"><img class="w-50" src="images/logo.png"></a>

		<div class="navbar-collapse" id="usrnav">
			<ul class="navbar-nav ml-auto">
				<li class="nav-item dropdown">
					<a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
						Pranav Kumar <span class="usr-ico">PK</span>
					</a>
					<div class="dropdown-menu" aria-labelledby="navbarDropdown">
						<a class="dropdown-item" href="#">Logout</a>
					</div>
				</li>
			</ul>
		</div>
	</nav>
</header> -->

<header class="bg-light border-bottom">
	<div class="d-flex justify-content-between flex-wrap">
		<div class="p-2">
			<a class="" href="<?php echo base_url();?>dashboard"><img class="w-50" src="<?php echo base_url();?>/website-assets/images/logo.png"></a>
		</div>

		 <?php
		if ($this->session->userdata('front_logged_in')) {
        ?>


		<div class="dropdown text-center pt-3 mt-3 pr-3">
			<a class="text-dark dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
				<!-- <?php //echo $user_name;?> <span class="usr-ico"><?php  //echo substr(strtoupper($user_name), 0, 1);  // abcd;?></span> -->
				<?php echo $user_name;?> <span class="usr-ico"><?php  echo strtoupper($uico);?></span>
			</a>
			<div class="dropdown-menu" aria-labelledby="navbarDropdown">
				<a class="dropdown-item" href="<?php echo base_url();?>myaccount">My Account</a>
				<a class="dropdown-item" href="<?php echo base_url();?>logout">Logout</a>
			</div>
		</div>

		<?php
		}
		?>
	</div>
</header>