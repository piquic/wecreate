<?php
$class_name=$this->router->fetch_class();
$method_name=$this->router->fetch_method();


if ($this->session->userdata('front_logged_in')) {
	$session_data = $this->session->userdata('front_logged_in');
	$user_id = $session_data['user_id'];
	$user_name = $session_data['user_name'];
	$type1=$this->session->userdata('type1');
}
else
{
	$user_id = '';
	$user_name = '';
}


$temp = explode(' ', $user_name);

if(isset($temp[1]) != '')
	$uico = substr($temp[0], 0, 1) . substr($temp[1], 0, 1);
else
	$uico = substr($temp[0], 0, 1); 



?>

<header class="bg-light border-bottom">
	<div class="d-flex justify-content-between flex-wrap">
		<div class="d-flex justify-content-start flex-wrap">
			<div class="p-2">

				<?php

				$session_data = $this->session->userdata('front_logged_in');
				$user_id = $session_data['user_id'];
				$user_name = $session_data['user_name'];

				$type1 = $session_data['user_type_id'];
				$user_type_id=$type1;
				$client_access=$session_data['client_access'];

				if($client_access!='' && $user_type_id!='') {
					$checkquerys = $this->db->query("select * from wc_clients where client_id='$client_access' ")->result_array();

					$client_image=$checkquerys[0]['client_image'];

					$header_logo=base_url()."/upload_client_logo/".$client_image;
				}
				else
				{
					$header_logo=base_url()."/website-assets/images/logo.png";
				}
				
				?>
				<a class="" href="<?php echo base_url();?>dashboard">
					<div class="d-flex justify-content-around" style="height: 50px; width: 100px; text-align: center;">
						<img class="h-100" src="<?php echo $header_logo;?>">
					</div>
				</a>
			</div>

			<?php
			if($type1=='1')
			{
				$query=$this->db->query("select * from wc_settings WHERE setting_key='WEKAN_URL_LINK' ");
				$settingDetails= $query->result_array();
				$WEKAN_URL_LINK=$settingDetails[0]['setting_value'];


				$wekan_link_url=$WEKAN_URL_LINK;


			}
			else
			{
				$query=$this->db->query("select * from wc_settings WHERE setting_key='WEKAN_URL_LINK' ");
				$settingDetails= $query->result_array();
				$WEKAN_URL_LINK=$settingDetails[0]['setting_value'];

				$session_data = $this->session->userdata('front_logged_in');
				$user_id = $session_data['user_id'];
				$user_name = $session_data['user_name'];

				$type1 = $session_data['user_type_id'];
				$user_type_id=$type1;
				$client_access=$session_data['client_access'];

				$checkquerys = $this->db->query("select * from wc_clients where client_id='$client_access' ")->result_array();

				$wekan_client_id=$checkquerys[0]['wekan_client_id'];
				//$client_name=$checkquerys[0]['client_name'];
				$client_name=str_replace(" ","-",strtolower($checkquerys[0]['client_name']));


				$wekan_link_url=$WEKAN_URL_LINK."b/".$wekan_client_id."/".$client_name;



				
			}


			$query=$this->db->query("select * from wc_settings WHERE setting_key='REPORTICO_LINK' ");
			$settingDetails= $query->result_array();
			$REPORTICO_LINK=$settingDetails[0]['setting_value'];
			?>

			<div class="p-2 py-4">
				<div class="d-flex justify-content-start flex-wrap">
					<a class="lead pr-5 <?php if($class_name == 'dashboard'){ echo "active"; } else { echo "text-dark"; } ?>" href="<?php echo base_url();?>dashboard">Dashboard</a>
					<a class="lead pr-5 <?php if($class_name == 'team'){ echo "active"; } else { echo "text-dark"; } ?>" href="<?php echo $wekan_link_url;?>" target="_blank">Team</a>
					<a class="lead pr-5 <?php if($class_name == 'report'){ echo "active"; } else { echo "text-dark"; } ?>" href="<?php echo $REPORTICO_LINK;?>" target="_blank">Report</a>					
				</div>
			</div>
		</div>

		<?php
		if ($this->session->userdata('front_logged_in')) {
			?>


			<div class="dropdown text-center p-2 py-3 mt-1 pr-3">
				<a class="text-dark dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
					<!-- <?php //echo $user_name;?> <span class="usr-ico"><?php  //echo substr(strtoupper($user_name), 0, 1);  // abcd;?></span> -->
					<?php echo $user_name;?> <span class="usr-ico" style="height: 32px; width: 32px; line-height: 32px; font-size: 14px;"><?php  echo strtoupper($uico);?></span>
				</a>
				<div class="dropdown-menu" aria-labelledby="navbarDropdown">
					<a class="dropdown-item" href="<?php echo base_url();?>myaccount">My Account</a>
					<?php
					if($user_type_id=='2'){
						?>
						<a class="dropdown-item" href="<?php echo base_url();?>settings">Settings</a>
						<?php
					}
					?>
					<a class="dropdown-item" href="<?php echo base_url();?>logout">Logout</a>
				</div>
			</div>

			<?php
		}
		?>
	</div>
</header>