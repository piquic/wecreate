<?php
session_start();
error_reporting(0);

if ($this->session->userdata('front_logged_in')) {
	$session_data = $this->session->userdata('front_logged_in');
	$user_id = $session_data['user_id'];
	$user_name = $session_data['user_name'];
	$_SESSION['user_id']=$user_id;
	$_SESSION['user_name']=$user_name;
	$type1 = $session_data['user_type_id'];
	$user_type_id=$type1;
	$brand_access=$session_data['brand_access'];

	$checkquerys = $this->db->query("select user_type_name from wc_users_type where user_type_id='$user_type_id' ")->result_array();
	$job_title=$checkquerys[0]['user_type_name'];

}
else
{
	$user_id = '';
	$user_name = ''; 
	$_SESSION['user_id']=$user_id;
	$_SESSION['user_name']=$user_name;
}
?>

<?php $data['page_title'] = "dashboard";

$this->load->view('front/includes/header',$data);
$this->load->view('front/includes/topnav');
?>
<style type="text/css">
	.errmsg{
		color:red;
	}
	.error{
		color:red;
	}
</style>
<div class="container-fluid">
	<div class="row">

		<!-- Main Body -->
		<div class="col-12 col-sm-12 col-md-12">
			<main class="p-3">

				<div class="row">
					<div class="col-12 col-sm-12 col-md-12">
						<div class="px-3">
							<a href="<?php echo base_url() ?>dashboard"><p class="text-wc lead"><i class="fas fa-arrow-alt-circle-left"></i>&nbsp;Back to Dashboard</p></a>
						</div>
					</div>
				</div>

				<div class="row">
					<div class="col-12 col-sm-12 col-md-12">
						<div class="px-3 text-wc">
							<h2>My Account</h2>
						</div>
					</div>
				</div>

				<!-- <hr class="text-center w-50 "> -->

				<div class="row">
					<div class="col-12 col-sm-12 col-md-12">
						<div class="p-3">
							<ul class="nav nav-tabs" id="myTab" role="tablist">
								<li class="nav-item">
									<a class="nav-link text-wc text-uppercase active" id="general-tab" data-toggle="tab" href="#general" role="tab" aria-controls="general" aria-selected="true">General</a>
								</li>
								<li class="nav-item">
									<a class="nav-link text-wc text-uppercase" id="items-tab" data-toggle="tab" href="#items" role="tab" aria-controls="items" aria-selected="false">Password</a>
								</li>
							</ul>
							<div class="tab-content" id="myTabContent">


								<div class="tab-pane fade show active" id="general" role="tabpanel" aria-labelledby="general-tab">


									<div class="p-4">

								<div class="form-row">
									<div class="form-group col-1">
										
									</div>
									<div class="form-group col-4">
										<div id="general_success"></div>
									</div>
								</div>





								<form id="User_form" name="User_form">

								<input type='hidden' name='user_id' value='<?php echo $user_details['0']['user_id'];?>' id='user_id'>

								<input type='hidden' name='status' value='<?php echo $user_details['0']['status'];?>' id='status'>
								<input type='hidden' name='user_password' value='<?php echo $user_details['0']['user_password'];?>' id='user_password'>

								<div class="form-row">
									<div class="form-group col-1">
										<label class="lead" for="txtusr">User Type : </label>
									</div>
									<div class="form-group col-4">
										<select name="user_type_id" id="user_type_id" disabled=""   class="form-control" style="width: 100%"  >
								 <option value=''>Select</option>
								<?php


								$users_type_details=$this->db->query("select * from wc_users_type where user_type_id!='1' and deleted='0' and status='Active' ")->result_array();
								if(!empty($users_type_details))
								{
								foreach($users_type_details as $key => $userstypedetails)
								{
								$user_type_id=$userstypedetails['user_type_id'];
								$user_type_name=$userstypedetails['user_type_name'];
								?>
								<option value="<?php echo $user_type_id;?>" <?php if($user_details['0']['user_type_id']==$user_type_id) { ?> selected="selected" <?php } ?>  ><?php echo $user_type_name;?></option>
								<?php

								}
								}
								?>
								
								 </select>
									</div>
								</div>

								<?php
								if($user_details['0']['user_type_id']=='6')
								{

								
								?>


								<div class="form-row">
									<div class="form-group col-1">
										<label class="lead" for="txtusr">Client Name : </label>
									</div>
									<div class="form-group col-4">

										 <?php

						//echo "<pre>";
						//print_r($client_wekan_list_arr);
						//echo "select * from wc_clients where deleted='0' and status='Active' order by  client_name asc";

						$client_id_sel=$user_details['0']['client_id'];
						
						$client_id_arr=array();

						if(!empty($client_id_sel))
						{
						$client_id_arr=explode(",",trim($client_id_sel,","));
						}

						$clients_details=$this->db->query("select * from wc_clients where deleted='0' and status='Active' and client_id in (".trim($client_id_sel,",").")  order by  client_name asc")->result_array();
						if(!empty($clients_details))
						{
						foreach($clients_details as $key => $clientdetails)
						{
						$client_id=$clientdetails['client_id'];
						$client_name=$clientdetails['client_name'];
						?>
						<div class="form-check">
						<input class="form-check-input client_id_class" type="checkbox" value="<?php echo $client_id;?>" name="client_id[]" id="client_id<?php echo $client_id;?>" <?php if(in_array($client_id, $client_id_arr)) { ?> checked disabled  <?php } ?>  >
						<label class="form-check-label" for="client_id<?php echo $client_id;?>">
						<?php echo $client_name;?>
						</label>
						</div>
						<?php

						}

						?>
						  <input type='hidden' name='brand_id' value='<?php echo $user_details['0']['brand_id'];?>' id='brand_id'>
						<?php
						}
						else
						{
							?>
							There is no Clients.
							 <input type='hidden' name='client_id' value='<?php echo $user_details['0']['client_id'];?>' id='client_id'>
							 <input type='hidden' name='brand_id' value='<?php echo $user_details['0']['brand_id'];?>' id='brand_id'>

							<?php

						}
						?>
									
								
									</div>
								</div>

                        



								<?php
								
									
								}
								else
								{
								?>

								<div class="form-row">
									<div class="form-group col-1">
										<label class="lead" for="txtusr">Client Name : </label>
									</div>
									<div class="form-group col-4">
									<select name="client_id[]" id="client_id" disabled=""  class="form-control"   style="width: 100%" >

									<option value=""  >Select</option>
								
								<?php
								$client_details=$this->db->query("select * from wc_clients where deleted='0' and status='Active' order by  client_name asc")->result_array();
								if(!empty($client_details))
								{
								foreach($client_details as $key => $clientdetails)
								{
								$client_id=$clientdetails['client_id'];
								$client_name=$clientdetails['client_name'];
								?>
								<option value="<?php echo $client_id;?>" <?php if($user_details['0']['client_id']==$client_id) { ?> selected="selected" <?php }  ?>  ><?php echo $client_name;?></option>
								<?php

								}
								}
								?>
								
								 </select>
									</div>
								</div>

								<?php
								if($user_details['0']['user_type_id']=='4')
								{

								
								?>
								<div class="form-row">
									<div class="form-group col-1">
										<label class="lead" for="txtusr">Brand Name : </label>
									</div>
									<div class="form-group col-4">
										<!-- <select name="brand_id" id="brand_id" disabled=""  class="custom-select"  style="width: 100%" >

									<option value=""  >Select</option>
								
								<?php
								$brand_details=$this->db->query("select * from wc_brands where deleted='0' and status='Active' order by  brand_name asc")->result_array();
								if(!empty($brand_details))
								{
								foreach($brand_details as $key => $branddetails)
								{
								$brand_id=$branddetails['brand_id'];
								$brand_name=$branddetails['brand_name'];
								?>
								<option value="<?php echo $brand_id;?>" <?php if($user_details['0']['brand_id']==$brand_id) { ?> selected="selected" <?php }  ?>   ><?php echo $brand_name;?></option>
								<?php

								}
								}
								?>
								
								 </select> -->

								 <?php

						
						

						$client_id_sel=$user_details['0']['client_id'];

						$brand_id_sel=$user_details['0']['brand_id'];

						$brand_id_arr=array();

						if(!empty($brand_id_sel))
						{
						$brand_id_arr=explode(",",trim($brand_id_sel,","));
						}


						if($client_id_sel!='')
								{
									$brand_details=$this->db->query("select * from wc_brands where deleted='0' and status='Active' and client_id='$client_id_sel' and brand_id in (".trim($brand_id_sel,",").") order by  brand_name asc")->result_array();
								}
								else
								{
									$brand_details=$this->db->query("select * from wc_brands where deleted='0' and status='Active' order by  brand_name asc")->result_array();

								}
								
						if(!empty($brand_details))
						{
						foreach($brand_details as $key => $branddetails)
						{
						$brand_id=$branddetails['brand_id'];
						$brand_name=$branddetails['brand_name'];
						?>
						<div class="form-check">
						<input class="form-check-input brand_id_class" type="checkbox" value="<?php echo $brand_id;?>" name="brand_id[]" id="brand_id<?php echo $brand_id;?>" <?php if(in_array($brand_id, $brand_id_arr)) { ?> checked disabled   <?php } ?>  >
						<label class="form-check-label" for="brand_id<?php echo $brand_id;?>">
						<?php echo $brand_name;?>
						</label>
						</div>
						<?php

						}
						}
						else
						{
							?>
							There is no brands.
							 <input type='hidden' name='brand_id' value='0' id='brand_id'>
							<?php

						}
						?>


									</div>
								</div>

							<?php
								
									
								}
								else
								{
									?>
									<input type='hidden' name='brand_id' value='<?php echo $user_details['0']['brand_id'];?>' id='brand_id'>
									<?php
								}
								?>

									
								<?php
								}
								?>


								













								<div class="form-row">
									<div class="form-group col-1">
										<label class="lead" for="txtusr">User Name : </label>
									</div>
									<div class="form-group col-4">
										<input class="form-control" type="text" name="user_name" id="user_name" value="<?php echo $user_details['0']['user_name']; ?>">
									</div>
								</div>

								

								<div class="form-row">
									<div class="form-group col-1">
										<label class="lead" for="txtemail">User Email : </label>
									</div>
									<div class="form-group col-4">
										<input class="form-control" type="email" name="user_email" id="user_email" value="<?php echo $user_details['0']['user_email']; ?>">
									</div>
								</div>

								<div class="form-row">
									<div class="form-group col-1">
										<label class="lead" for="txtpwd">Phone : </label>
									</div>
									<div class="form-group col-4">
										<input class="form-control" type="text" name="user_mobile" id="user_mobile" value="<?php echo $user_details['0']['user_mobile']; ?>">
									</div>
								</div>

								

								

								<div class="form-row">
									<div class="form-group col-1">
										
									</div>
									<div class="form-group col-4">

										<?php
									if($user_details['0']['user_type_id']!='4')
									{


									?>
									

										<input class="btn btn-outline-wc m-1" type="submit" value="&emsp;Save&emsp;">
										<input class="btn btn-outline-secondary m-1" type="reset"  value="&emsp;Reset&emsp;">
										<!-- <button type="button" class="btn btn-outline-wc m-1">&emsp;Save&emsp;</button>
									<button type="button" class="btn btn-outline-secondary m-1">&emsp;Reset&emsp;</button> -->

									<?php
									}


									?>
									</div>
								</div>

								<!-- <div class="d-flex justify-content-center">
									<button type="button" class="btn btn-outline-wc m-1">&emsp;Save&emsp;</button>
									<button type="button" class="btn btn-outline-secondary m-1">&emsp;Reset&emsp;</button>
								</div> -->
							</form>


									</div>


								</div>




								<div class="tab-pane fade" id="items" role="tabpanel" aria-labelledby="items-tab">
									<div class="p-4">

										<div class="form-row">
									<div class="form-group col-1">
										
									</div>
									<div class="form-group col-4">
										<div id="password_success"></div>
									</div>
								</div>

										<form id="User_password_form"  name="User_password_form" >
								
									<input type='hidden' name='user_id' value='<?php echo $user_details['0']['user_id'];?>' id='user_id'>
								<div class="form-row">
									<div class="form-group col-1">
										<label class="lead" for="txtpwd">Password : </label>
									</div>
									<div class="form-group col-4">
										<input class="form-control" type="password" name="user_change_password" id="user_change_password" value="">
									</div>
								</div>

								<div class="form-row">
									<div class="form-group col-1">
										<label class="lead" for="txtcpwd">Confirm Password : </label>
									</div>
									<div class="form-group col-4">
										<input class="form-control" type="password" name="user_change_cpassword" id="user_change_cpassword" value="">
									</div>
								</div>

								
								<div class="form-row">
									<div class="form-group col-1">
										
									</div>
									<div class="form-group col-4">
										<input class="btn btn-outline-wc m-1" type="submit" value="&emsp;Save&emsp;">
										<input class="btn btn-outline-secondary m-1" type="reset"  value="&emsp;Reset&emsp;">
										<!-- <button type="button" class="btn btn-outline-wc m-1">&emsp;Save&emsp;</button>
									    <button type="button" class="btn btn-outline-secondary m-1">&emsp;Reset&emsp;</button> -->
									</div>
								</div>

							</form>





									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</main>
		</div>

	</div>
</div>

<?php $this->load->view('front/includes/footer'); ?>
<script>
 $(document).ready(function() {



		
		
		
		//alert("popo");
		
		
		jQuery.validator.addMethod("alphanumeric", function(value, element) {
        return this.optional(element) || /^[a-zA-Z\-\s]+$/.test(value);
        }, "It must contain only letters."); 
		
		jQuery.validator.addMethod("atLeastOneLetter", function (value, element) {
		return this.optional(element) || /[a-zA-Z]+/.test(value);
		}, "Must have at least one  letter");
		
		/*jQuery.validator.addMethod("phoneno", function(phone_number, element) {
    	    phone_number = phone_number.replace(/\s+/g, "");
    	    return this.optional(element) || phone_number.length > 9 && 
    	    phone_number.match(/^((\+[1-9]{1,4}[ \-]*)|(\([0-9]{2,3}\)[ \-]*)|([0-9]{2,4})[ \-]*)*?[0-9]{3,4}?[ \-]*[0-9]{3,4}?$/);
    	}, "<br />Please specify a valid phone number");*/
		
		/*jQuery.validator.addMethod('fnType', function(value, element) {
		return value.match(/^\+(?:[0-9] ?){6,14}[0-9]$/);
		},'Enter Valid  phone number');*/
		
		/*jQuery.validator.addMethod('fnType', function(value, element) {
		return /^\d{10}$/.test(value.replace(/[()\s+-]/g,''));
		},'Enter Valid  phone number');*/
		
		
		/*jQuery.validator.addMethod('fnType', function(value, element) {
		return /^\d{10}$/.test(value.replace(/[()\s+-]/g,''));
		},'Enter Valid  phone number');*/
		
		jQuery.validator.addMethod("phoneno", function(value, element) {
        return this.optional(element) || /^[0-9+() ]*$/.test(value);
        }, "Enter Valid  phone number."); 
		
		
		
		$('#User_form').validate({
		
           rules: {
			user_type_id: {
                required: true,
			},

			/*client_id: {
                required: true,
			},


			brand_id: {
                required: true,
			},
			*/
            user_name:{
				required:true,
				minlength: 2,
				//alphanumeric:true,
				//atLeastOneLetter:true,
			},
			user_email:{
				required:true,
				email: true,
				<?php
				if($user_id=="")
				{
				?>
				remote: {
				url: "<?php echo base_url();?>admin/user/checkexistemail",
				type: "post"
				},
				<?php
				}
				?>	 
				 
			 },
			
			
			<?php
			if($user_id=='')
			{
			?>
			user_password:{
				required:true,
				minlength:5,
			},
			user_cpassword: {
				required:true,
                equalTo: "#user_password" ,
				minlength:5,
            },
			<?php
			}
			?>
		   user_mobile: {
		   required: true,
		   /*digits: true,*/
		   minlength:10,
		  /* maxlength:10,*/
		   phoneno: true,
		   /*number: true,*/
		  /* maxlength: 10,*/
		    <?php
		   if($user_id=="")
		   {
		   ?>
		    remote: {
                    url: "<?php echo base_url();?>admin/user/checkexistmobile",
                    type: "post"
                   },
				 
		   <?php
		   }
		   ?>		 
				 
		},
		/*user_telephone: {
		   
		   digits: true,
		   minlength:10,
		   maxlength:10,
		    phoneno: true,
		   
		},
		*/
		
			status: {
                required: true,
			}
			
			
		},
		 messages: {
		 
		    user_email:{
		      remote:"This email already exists"
		     },
			user_mobile: {
			digits: "This field can only contain numbers.",
			/*maxlength: "this must be 10 digit number.",*/
			remote:"This mobile already exists",
			
			},
			
			 /*user_mobile:{
		      remote:"This email already exists"
		     }*/
                 <?php
				if($user_id=='')
				{
				?>
                user_cpassword: {
            	equalTo:"password not match",
			     },
			    <?php
				}
				?>   
		  
			
        },
				submitHandler: function(form) {
		
		       //alert("koko");

		       $("#loader").show();
		
		        var user_id=$('#user_id').val();
				var user_type_id=$('#user_type_id').val();

				/*if(user_type_id=='1' || user_type_id=='2'  || user_type_id=='3')
				{
				var brand_id=$('#hid_brand_id').val();
				}
				else
				{*/
				if(user_type_id=='6')
				{
						var client_id_all = [];
						var client_id_list = $('input:checkbox:checked.client_id_class').map(function(){
						client_id_all.push(this.value);
						return this.value; }).get().join(",");

						if($.trim(client_id_all)!='')
						{
						var client_id =","+client_id_all+",";
						}
						else
						{
						var client_id ="";
						}
						//alert(client_id);
						console.log(client_id);

				}
				else
				{
					var client_id=$('#client_id').val();
				}

				var brand_id=$('#brand_id').val();
				/*var brand_id = $('#brand_id option:selected')
				.toArray().map(item => item.value);
				var brand_id=","+brand_id+",";*/
				//alert(brand_id);
				//}
				//alert(client_id);

			    var user_name=$('#user_name').val();
			    //var user_address=$('#user_address').val();
				var user_email=$('#user_email').val();
				//var user_telephone=$('#user_telephone').val();
				var user_mobile=$('#user_mobile').val();
				var user_password=$('#user_password').val();
				
				var status=$('#status').val();
				
				
				
				
					
					
				/*alert("<?php echo base_url();?>front/myaccount/update_user?&user_type_id="+user_type_id+"&client_id="+client_id+"&brand_id="+brand_id+"&user_id="+user_id+"&user_name="+user_name+"&user_email="+user_email+"&user_mobile="+user_mobile+"&user_password="+user_password+"&status="+status);	*/
					
				/*$.ajax({
				method:'post',
				url: "<?php echo base_url();?>front/myaccount/update_user", 
				data:"&user_type_id="+user_type_id+"&client_id="+client_id+"&brand_id="+brand_id+"&user_id="+user_id+"&user_name="+user_name+"&user_email="+user_email+"&user_mobile="+user_mobile+"&user_password="+user_password+"&status="+status,
				success: function(result){
				if(result)
				{
				  var val=$.trim(result);
				  if(val=='wekanexist')
					{
						//alert("That email is already exist in Wekan.");
						$("#general_success").html("<span style='color: red;'>That email is already exist in Wekan!.</span>");
						$("#loader").hide();

					}
					else
					{
					
						//window.reload();
						$("#general_success").html("Successfully Updated!.");
						setTimeout(function(){$("#general_success").html(""); }, 8000);
						$("#loader").hide();

				    }
					
				
				}
				}
				});*/


				

				//alert("ppp");


				$.ajax({
				method:'post',
				url: "<?php echo base_url();?>front/myaccount/update_user", 
				data:"&user_type_id="+user_type_id+"&client_id="+client_id+"&brand_id="+brand_id+"&user_id="+user_id+"&user_name="+user_name+"&user_email="+user_email+"&user_mobile="+user_mobile+"&user_password="+user_password+"&status="+status,
				cache: true,
				timeout: 5000
				}).done(function( result ) {
				console.log( "SUCCESS: " + result );
				if(result)
				{
				  var val=$.trim(result);

				  //alert(val);
				  if(val=='wekanexist')
					{
						//alert("That email is already exist in Wekan.");
						$("#general_success").html("<span style='color: red;'>That email is already exist in Wekan!.</span>");
						$("#loader").hide();

					}
					else
					{
					
						//window.reload();
						$("#general_success").html("Successfully Updated!.");
						setTimeout(function(){$("#general_success").html(""); }, 8000);
						$("#loader").hide();

				    }
					
				
				}
				
				//window.location.href="<?php echo base_url();?>dashboard";
				}).fail(function() {

				//alert("Request failed");
				console.log( "Request failed");
				//window.reload();
						$("#general_success").html("Successfully Updated!.");
						setTimeout(function(){$("#general_success").html(""); }, 8000);
						$("#loader").hide();
						
				});	

					
					
					
					
					
						
							
					
					   
		
		} 
		 
		
		});
		


 		$('#User_password_form').validate({
        rules: {
			user_change_password:{
				
				required:true
			},
			
			 user_change_cpassword: {
				required:true,
                equalTo: "#user_change_password"              
            }
			
        },
		 messages: {
          
			user_change_cpassword: {
            	equalTo:"password not match"
			}
			
        },
		submitHandler: function(form) {
			    $("#loader").show();
		        var user_id=$('#user_id').val();
				var user_password=$('#user_change_password').val();
				
				
				
				
					
					
				//alert("<?php echo base_url();?>user/addUser?&user_id="+user_id+"&UserName="+UserName+"&UserComments="+UserComments+"&UserIsActive="+UserIsActive);	
					
				/*$.ajax({
				method:'post',
				url: "<?php echo base_url();?>front/myaccount/update_change_password", 
				data:"&user_id="+user_id+"&user_password="+user_password,
				success: function(result){
				if(result)
				{
				//alert(result);

				//window.reload();
				$("#password_success").html("Successfully Updated!.");
				setTimeout(function(){$("#password_success").html(""); $("#loader").hide(); }, 8000);
				$("#loader").hide();
					
				
				
				}
				}
				});
*/




				$.ajax({
				method:'post',
				url: "<?php echo base_url();?>front/myaccount/update_change_password", 
				data:"&user_id="+user_id+"&user_password="+user_password,
				cache: true,
				timeout: 3000
				}).done(function( result ) {
				console.log( "SUCCESS: " + result );
				if(result)
				{
				//alert(result);

				//window.reload();
				$("#password_success").html("Successfully Updated!.");
				setTimeout(function(){$("#password_success").html(""); $("#loader").hide(); }, 8000);
				$("#loader").hide();
					
				
				
				}
				//window.location.href="<?php echo base_url();?>dashboard";
				}).fail(function() {
				console.log( "Request failed");
				
				$("#password_success").html("Successfully Updated!.");
				setTimeout(function(){$("#password_success").html(""); $("#loader").hide(); }, 8000);
				$("#loader").hide();
					
						
				});	


















				
				
				
		 },
    });
	
 
    });
</script>