<?php
session_start();
error_reporting(0);

//print_r($_SESSION);

if ($this->session->userdata('front_logged_in')) {
$session_data = $this->session->userdata('front_logged_in');
$user_id = $session_data['user_id'];
$user_name = $session_data['user_name'];
$_SESSION['user_id']=$user_id;
$_SESSION['user_name']=$user_name;
//$type1=$this->session->userdata('user_type_id');
  $type1 = $session_data['user_type_id'];
 $user_type_id=$type1;
 $brand_access=$session_data['brand_access'];

  $checkquerys = $this->db->query("select user_type_name from wc_users_type where  user_type_id='$user_type_id' ")->result_array();
$job_title=$checkquerys[0]['user_type_name'];
	
}
else
{
$user_id = '';
$user_name = ''; 
$_SESSION['user_id']=$user_id;
$_SESSION['user_name']=$user_name;
}
?>

<?php $data['page_title'] = "upload image";

$this->load->view('front/includes/header',$data);
$this->load->view('front/includes/topnav');
 ?>


<div class="container-fluid">
	<div class="row">

		<!-- Sidebar -->
		<div class="col-12 col-sm-12 col-md-3">


			<!-- <aside class="p-3">
				<div class="row">
					<div class="col-12 col-sm-12 col-md-12">
					<?php if ($type1==4) { ?>
						<div class="p-3">
							<a class="text-wc h5" href="#"><i class="fas fa-cloud-upload-alt"></i>&nbsp;UPLOAD BRIEF</a>
						</div>
					<?php } elseif($type1==3) { ?>
						<div class="py-3">
							<select class="custom-select" name="brand_id" id="brand_id" onchange="sortbrief()">
								<option selected value="">Select Brand</option>
								<?php 	if(!empty($brandsinfo))
											{
												foreach($brandsinfo as $keybill => $brands)
												{ 
												$brand_id=$brands['brand_id'];
													$brand_name=$brands['brand_name'];
												?>
								<option value="<?php echo $brand_id; ?>"><?php echo $brand_name; ?></option>
											<?php } }?>
							</select>
						</div>						
					<?php } else { ?>
					<div class="py-3">
					
					</div>
					
					<?php } ?>

						<div class="p-3">
							<p class="text-wc lead">DASHBOARD</p>
						</div>

						<div class="p-3">
							<p class="text-wc lead">Show by Stages</p>

							<div class="pl-3">
								<div class="custom-control custom-checkbox">
									<input type="checkbox" class="custom-control-input" name="breif_status[]" id="brfrev"  value="0" onchange="sortbrief()">
									<label class="custom-control-label roundCheck lead" for="brfrev">Brief in Review <span class="badge badge-wc" id="bir"><?php echo $briefinreview; ?></span></label>
								</div>

								<div class="custom-control custom-checkbox">
									<input type="checkbox" class="custom-control-input" name="breif_status[]" id="brfrej" value="4" onchange="sortbrief()">
									<label class="custom-control-label roundCheck lead" for="brfrej">Brief Rejected <span class="badge badge-wc" id="br"><?php echo $briefrejected; ?></span></label>
								</div>

								<div class="custom-control custom-checkbox">
									<input type="checkbox" class="custom-control-input" name="breif_status[]" id="wrkprog" value="1" onchange="sortbrief()">
									<label class="custom-control-label roundCheck lead" for="wrkprog">Work in Progress <span class="badge badge-wc" id="wip"><?php echo $workinprogress; ?></span></label>
								</div>

								<div class="custom-control custom-checkbox">
									<input type="checkbox" class="custom-control-input" name="breif_status[]" id="prfpend" value="6" onchange="sortbrief()">
									<label class="custom-control-label roundCheck lead" for="prfpend">Proofing Pending <span class="badge badge-wc" id="pp"><?php echo $proffing_pending; ?></span></label>
								</div>

								<div class="custom-control custom-checkbox">
									<input type="checkbox" class="custom-control-input" name="breif_status[]" id="revwrk" value="3" onchange="sortbrief()">
									<label class="custom-control-label roundCheck lead" for="revwrk">Revision Work <span class="badge badge-wc" id="rw"><?php echo $revision_work; ?></span></label>
								</div>

								<div class="custom-control custom-checkbox">
									<input type="checkbox" class="custom-control-input" name="breif_status[]" id="wrkcomp" value="2" onchange="sortbrief()">
									<label class="custom-control-label roundCheck lead" for="wrkcomp">Work Complete <span class="badge badge-wc" id="wc"><?php echo $work_complete; ?></span></label>
								</div>								

								<div class="custom-control custom-checkbox">
									<input type="checkbox" class="custom-control-input" name="breif_status[]" id="fdbkpend" value="5" onchange="sortbrief()">
									<label class="custom-control-label roundCheck lead" for="fdbkpend">Feedback Pending <span class="badge badge-wc" id="fp"><?php echo $feedback_pending; ?></span></label>
								</div>
							</div>
						</div>
					</div>
				</div>
			</aside>
 -->

<?php
		$this->load->view('front/includes/sidebar');

		?>

		</div>

		<!-- Main Body -->
		<div class="col-12 col-sm-12 col-md-9">
			<main class="border-left p-3">

				<div class="row">
					<div class="col-12 col-sm-12 col-md-8">
						<div class="p-3">
							<h2 class="text-wc text-capitalize">Dashboard (<?php echo $job_title; ?>)</h2>
						</div>
					</div>

					<div class="col-12 col-sm-12 col-md-4">
						<div class="p-3 text-md-right">
							<form>
								<input type="text" class="form-control" id="search_key" name="search_key" placeholder="Search Brief Id" onkeyup="sortbrief();">
							</form>
						</div>
					</div>
				</div>

				<div class="row">
					<div class="col-12 col-sm-12 col-md-12">
						<div class="p-3" id="myResponse">
							<table id="tblbrf" class="table lead">
								<thead>
									<tr class="table-success border-bottom">
										<th scope="col">ID</th>
										<th scope="col">Title</th>
										<th scope="col">Brief</th>
										<th scope="col">Due Date</th>
										<th scope="col">Status</th>
									</tr>
								</thead>
								<tbody>
								<?php if(!empty($dashboradinfo))
						  {
							  $i=1;
							foreach($dashboradinfo as $keybill => $brief)
												{ 
												$brief_id=$brief['brief_id'];
												$added_by_user_id=$brief['added_by_user_id'];
												$brief_title=$brief['brief_title'];
												$brief_name=str_replace(" ","_",$brief_title);
												$brief_doc=$brief['brief_doc'];
												$brief_due_date=$brief['brief_due_date'];
												
												$status=$brief['status'];
												$brand_id=$brief['brand_id'];
												$rejected_reasons=$brief['rejected_reasons'];

								
								//$dst = "brief_upload/".$brief_id;




								

								/*$checkquerys = $this->db->query("select wc_brands.*,wc_clients.client_name from wc_brands inner join wc_clients on wc_brands.client_id=wc_clients.client_id where  wc_brands.brand_id='$brand_id' ")->result_array();*/

								$checkquerys = $this->db->query("select wc_brands.*,wc_clients.client_name from wc_users 
								inner join wc_brands on wc_users.brand_id=wc_brands.brand_id  
								inner join wc_clients on wc_brands.client_id=wc_clients.client_id 
								where  wc_users.user_id='$added_by_user_id' ")->result_array();



								$brand_id=$checkquerys[0]['brand_id'];
								$brand_name=str_replace(" ","_",$checkquerys[0]['brand_name']);
								$client_id=$checkquerys[0]['client_id'];
								$client_name=str_replace(" ","_",$checkquerys[0]['client_name']);



								//$dst = "upload/".$client_id."-".$client_name."/".$brand_id."-".$brand_name."/".$brief_id."-brief";

								$dst = "upload/".$client_id."-".$client_name."/".$brand_id."-".$brand_name."/".$brief_id."-".$brief_name;  

								//$download_rej_brief=base_url()."/upload/".$client_id."-".$client_name."/".$brand_id."-".$brand_name."/".$brief_id."-brief/".$brief_doc;
								$download_rej_brief=base_url()."/upload/".$client_id."-".$client_name."/".$brand_id."-".$brand_name."/".$brief_id."-".$brief_name."/".$brief_doc;

								



													?>
								
									<tr <?php if($status!='4') { ?> class="border-bottom" <?php } ?> id="tr_<?php echo $brief_id;?>" >
										<th scope="row"><?php echo $brief_id;?></th>
										<td><?php echo $brief_title;?>
										<?php
										if($status!='0' && $status!='4')
										{
										?>
										<br><br><a class="btn btn-outline-wc" href="<?php echo base_url();?>view-files/<?php echo $brief_id; ?>">View Files</a>
										<?php
										}
										?>
									    </td>


										<td>
										<?php
										if($status=='0' )
										{
										?>
										<i class="fas fa-sync icon-md px-1"></i>
										<?php
										}
										else if($status=='4')
										{
										?>
										<i class="fas fa-circle text-danger icon-md px-1"></i>
										<?php
										}
										else 
										{
										?>
										<i class="fas fa-circle text-wc icon-md px-1"></i>
										<?php
										}
										?>	

											

										</td>
										<td>
											<span class="w-100">
										<?php
										if($status=='0' )
										{
											echo "N/A";				
										}
										else
										{

											if($brief_due_date=='0000-00-00 00:00:00')
											{
												echo "N/A";
											}
											else
											{
												$brief_due_date= date('d F Y H:i', strtotime($brief_due_date));
												echo $brief_due_date;
											}
											
										}
										?>
										</span>


										<?php
										$accept_reject_permission=$this->Dashboard_model->checkPermission($user_type_id,3,'accept_reject');
										if($accept_reject_permission=='1')
										{
										?>

										<br><br>

										<div class="text-center text-wc brief_download_<?php echo $brief_id;?>" <?php if($status=='4') { ?> style="display: none;" <?php } ?> >
										<a class="text-wc" href="<?php echo $download_rej_brief;?>"><span  class="m-3 px-3 py-4 border rounded-circle"><i class="fas fa-download fa-2x"></i></span><br><br>Download Brief</a>
										</div>
										<?php
										}
										?>


											</td>
										<td>

										<?php
										if($status=='0' )
										{
										?>
										Brief in Review
									<?php
									$accept_reject_permission=$this->Dashboard_model->checkPermission($user_type_id,3,'accept_reject');
									if($accept_reject_permission=='1')
									{
									?>

									
										<br><br>
											<select class="custom-select mb-3" name="brief_status" id="brief_status<?php echo $brief_id;?>" onChange="changeBriefStatus('<?php echo $brief_id;?>')" >
												<option value="">Select Status</option>
												<option value="1" >Approve</option>
												<option value="4">Reject</option>
											</select><br><br>


											<!-- <div class="text-center text-wc brief_download_<?php echo $brief_id;?>" >
											<a class="text-wc" href="<?php echo $download_rej_brief;?>"><span  class="m-3 px-3 py-4 border rounded-circle"><i class="fas fa-download fa-2x"></i></span><br><br>Download Brief</a>
											</div> -->


											<?php

										}
										else
										{
											?>
											<br><br>
											<?php

										}
										?>

										  

											
										
										<?php

										if($accept_reject_permission=='1')
									    {
										?>	

											<div class=" brief_review_approve_<?php echo $brief_id;?>" style="display: none;">




											Delivery Date:

											<div class="input-group mb-2 mr-sm-2 " >
												<!-- <div class="input-group-prepend">
													<div class="input-group-text"><i class="far fa-calendar-alt"></i></div>
												</div>
												<input type="text" class="form-control datePick" name="brief_due_date" id="brief_due_date<?php echo $brief_id;?>" placeholder="00-00-0000 00:00"> -->


												 <!-- <div class='input-group date' id='datetimepicker<?php echo $brief_id;?>'>
												<input type='text' class="form-control" name="brief_due_date" id="brief_due_date<?php echo $brief_id;?>"  />
												<span class="input-group-addon">
												<span class="glyphicon glyphicon-calendar"></span>
												</span>
												</div>
												 

												 <script type="text/javascript">
												$(function () {
												$('#brief_due_date<?php echo $brief_id;?>').datetimepicker();
												});
												</script> -->

												<!-- <div class="controls input-append date form_datetime<?php echo $brief_id;?>" data-date="1979-09-16T05:25:07Z" data-date-format="dd MM yyyy - HH:ii p" data-link-field="dtp_input1">
												<input size="16" type="text" value="" readonly>
												<span class="add-on"><i class="icon-remove"></i></span>
												<span class="add-on"><i class="icon-th"></i></span>
												</div> -->

												 <div class="input-group-prepend ">
													<div class="input-group-text "><i class="far fa-calendar-alt"></i></div>
												</div>

												<input type="text" class="form-control datePick " name="brief_due_date" id="brief_due_date<?php echo $brief_id;?>" value="<?php echo date("Y-m-d H:i")?>" placeholder="<?php echo date("Y-m-d H:i")?>">



												<script type="text/javascript">
																			
												$(function () {
												$('#brief_due_date<?php echo $brief_id;?>').datetimepicker({
												//language:  'fr',
												weekStart: 1,
												todayBtn:  1,
												autoclose: 1,
												todayHighlight: 1,
												startView: 2,
												forceParse: 0,
												showMeridian: 1
												});

												});
												</script>

												<!-- <div class="controls input-append date datePick" data-date="1979-09-16T05:25:07Z" data-date-format="dd MM yyyy - HH:ii p" data-link-field="dtp_input1">

												<span class="add-on"><i class="icon-th"></i></span>
												<span class="add-on"><i class="icon-remove"></i></span>
												<input  size="16" type="text" value="" readonly>
												
												</div> -->



												
												
											</div>

                                            


											  <div class="px-3 py-5 text-center text-wc " <?php if($status=='0') {  ?> style="display: none;" <?php } ?>>
													<a class="text-wc" href="<?php echo $download_rej_brief;?>"><span class="m-3 px-3 py-4 border rounded-circle"><i class="fas fa-download fa-2x"></i></span><br><br>Download Rejected<br> Brief</a>
												</div>



											<div class="row pt-3">
												<div class="col-12 col-sm-12 col-md-4"></div>
												<div class="col-12 col-sm-12 col-md-4"><button type="submit" class="btn btn-outline-danger btn-block" onclick="submitCancel(<?php echo $brief_id;?>);">Cancel</button></div>
												<div class="col-12 col-sm-12 col-md-4"><button type="submit" class="btn btn-outline-wc btn-block" id="<?php echo $brief_id;?>" onclick="submitCorfirm(<?php echo $brief_id;?>);">Confirm</button></div>
											</div>




											</div>



										<?php
										}
										?>

										<?php
										}
										else if($status=='1')
										{
										?>
										Work in Progress
										<?php
										}
										else if($status=='2')
										{
										?>
										Work Completed
										<?php
										}
										else if($status=='3')
										{
										?>
										Revision Work

										<?php
										$proofing_permission=$this->Dashboard_model->checkPermission($user_type_id,4,'view');
										if($proofing_permission=='1')
										{
										?>

										<br><br><a class="btn btn-outline-wc" href="">Open Proofing</a>
										<?php
										}?>


										<?php
										}
										else if($status=='4')
										{
										?>
										Brief Rejected
										<?php
										}
										else if($status=='5')
										{
										?>
										Feedback Pending
										<?php
										if($user_type_id=='4')
										{
										?>

										<br><br><a class="btn btn-outline-wc" href="<?php echo base_url() ?>feedback/<?php echo $brief_id;?>">Add Feedback</a>
										<?php
										}?>

										<?php
										}
										else if($status=='6')
										{
										?>
										Proofing Pending


										<?php
										$proofing_permission=$this->Dashboard_model->checkPermission($user_type_id,4,'view');
										if($proofing_permission=='1')
										{
										?>

										<br><br><a class="btn btn-outline-wc" href="<?php echo base_url() ?>open-proofing\<?php echo $brief_id;?>">Open Proofing</a>
										<?php
										}?>
										<?php
										}

										else if($status=='7')
										{
										?>
										Feedback Sent


										<?php
										}
										else 
										{
										?>
										
										<?php
										}
										?>	

										

										



										</td>
									</tr>

									







									<?php
										if($status=='0' || $status=='4')
										{
										?>

										









										<tr class="border-bottom <?php if($status=='0') {  ?> brief_review_reject_<?php echo $brief_id;?>  <?php } ?>"  <?php if($status=='0') {  ?> style="display: none;" <?php } ?> >										
										<th class="border-0" scope="row"></th>
										<td class="border-0" colspan="4">
											<div class="d-flex justify-content-around flex-wrap">
												<div class="mt-3 w-50 text-justify">

													
													

											<?php
											$accept_reject_permission=$this->Dashboard_model->checkPermission($user_type_id,3,'accept_reject');
											if($accept_reject_permission=='1' )
											{
											?>

										<?php if($status=='0') { 
										?>

										<div class="border px-3 mt-3 text-justify">
										<span class="b-text bg-white p-2 ">REASON FOR REJECTION</span><br>
										<input type="text" name="rejected_reasons" id="rejected_reasons<?php echo $brief_id;?>" class="form-control mb-3 border-0" value="<?php echo $rejected_reasons;?>" >
										</div>

										<?php
										}
										else
										{


										?>


										<div class="border px-3 pb-3">
										<span class="b-text bg-white p-2">REASON FOR REJECTION</span><br>
										<span><?php echo $rejected_reasons;?></span>
										</div>

										<?php	}
										?>


										<?php
										if($user_type_id=='3' && $status=='0')
													{
										?>

											<div class="row pt-3">
											<div class="col-12 col-sm-12 col-md-4"></div>
											<div class="col-12 col-sm-12 col-md-4"><button type="submit" class="btn btn-outline-danger btn-block" onclick="submitCancel(<?php echo $brief_id;?>);">Cancel</button></div>
											<div class="col-12 col-sm-12 col-md-4"><button type="submit" class="btn btn-outline-wc btn-block" onclick="submitCorfirm(<?php echo $brief_id;?>);">Confirm</button></div>
											</div>

											<?php

										}
											}

											else
											{


													if($rejected_reasons!='')
													{
														?>
										<div class="border px-3 pb-3">
										<span class="b-text bg-white p-2">REASON FOR REJECTION</span><br>
										<span><?php echo $rejected_reasons;?></span>
										</div>
														<?php

													}
													

											}
											?>

												</div>

												<div class="px-3 py-5 text-center text-wc">
													<a class="text-wc" href="<?php echo $download_rej_brief;?>"><span class="m-3 px-3 py-4 border rounded-circle"><i class="fas fa-download fa-2x"></i></span><br><br><?php
														if($status=='0')
														{
															?>Download Rejected<br> Brief
															<?php

														}
														else
														{
															?>Download Rejected<br> Brief
															<?php
														}
														?>
														

													</a>
												</div>
												<?php
											if($user_type_id=='4' )
											{
											?>

										<div class="px-3 py-5 text-center text-wc">

									<label class="w3-button w3-blue w3-round">
									<span class="m-3 px-3 py-4 border rounded-circle"><i class="fas fa-download fa-2x"></i></span>
									<input type="file" style="display: none" class="re_upload_doc fas fa-download fa-2x" id="files1" alt='1' name="files1[]"  multiple val="<?php echo $brief_id;?>"  folder=""/>
									</label>

									<input type="hidden" name="file_nm_hid_<?php echo $brief_id;?>" id="file_nm_hid_<?php echo $brief_id;?>" value="">
									        <br><br>Re-Upload<br> Brief

												</div>
											
											<?php
											}
											?>
											</div>
										</td>
									</tr>
									
												<?php } ?>
									
						<?php   $i++;
						} 
						
						} 

						else
							{
                           ?>
                           <tr><td colspan="5" align="center">No Record Found</td></tr>

                           <?php

							}?>
								</tbody>
							</table>
						</div>
					</div>
				</div>

			</main>
		</div>

	</div>
</div>


		


<?php
$this->load->view('front/includes/footer');
?>

<script>

	

	function changeBriefStatus(brief_id)
	{
			var brief_status=$("#brief_status"+brief_id).val();
		//alert(brief_status);

		
		     
		
				if(brief_status=='1')
				{
				$(".brief_review_approve_"+brief_id).show();
				$(".brief_review_reject_"+brief_id).hide();
				$(".brief_download_"+brief_id).show();
				}
				else if(brief_status=='4')
				{
					$("#rejected_reasons"+brief_id).val('');

				$(".brief_review_approve_"+brief_id).hide();
				$(".brief_review_reject_"+brief_id).show();
				$( "#tr_"+brief_id ).removeClass( "border-bottom" );

				$(".brief_download_"+brief_id).hide();

				
				}
				else
				{
					$(".brief_download_"+brief_id).show();
					$(".brief_review_approve_"+brief_id).hide();
				    $(".brief_review_reject_"+brief_id).hide();

				}

		
		

		
		 
	}

	function submitCancel(brief_id)
	{
		sortbrief();
	}

	function submitCorfirm(brief_id)
	{

		
		var brief_status=$("#brief_status"+brief_id).val();
		//alert("--"+brief_status);
		if(brief_status=='')
		{
			alert("Please select Status");
		}
		else
		{



		if(brief_status=='1')
		{
			var brief_due_date=$("#brief_due_date"+brief_id).val();
			//alert(brief_due_date);
			//var brief_due_date = $("#brief_due_date"+brief_id).attr('placeholder'); 

			//alert(brief_due_date);
			var rejected_reasons='';
			$("#loader").show();

		
		}
		else if(brief_status=='4')
		{
			var brief_due_date='';
			var rejected_reasons=$("#rejected_reasons"+brief_id).val();
			if(rejected_reasons=='')
			{
				alert("Please Give Reason");
				return false;
			}
			else
			{
				$("#loader").show();
			}
		
		}

		//alert("<?php //echo base_url();?>front/uploadbrief/updateBriefStatus?&brief_id="+brief_id+"&brief_status="+brief_status+"&brief_due_date="+brief_due_date+"&rejected_reasons="+rejected_reasons);

		$.ajax({
	   			method:'POST',
	   			url: "<?php echo base_url();?>front/uploadbrief/updateBriefStatus",
	   			data:"&brief_id="+brief_id+"&brief_status="+brief_status+"&brief_due_date="+brief_due_date+"&rejected_reasons="+rejected_reasons,

	   			success: function(result){
	   				//alert(result);
	   				if(result) {
	   					sortbrief();
	   					$("#loader").hide();
	   					
	   				}
	   			}
	   		});

	   }
	}


/*$(document).ready(function() {*/

	function selectAccount(brand_id_sel)
	{

		//alert(brand_id_sel);
		 var account_id=$('#account_id').val();
		 //alert(account_id);
		 $.ajax({
	   			method:'POST',
	   			url: "<?php echo base_url();?>front/dashboard/getBrandDropDown",
	   			data:"&account_id="+account_id+"&brand_id_sel="+brand_id_sel,

	   			success: function(result){
	   				//alert(result);
	   				if(result) {
	   					$("#divBrand").html(result);
	   					sortbrief();
	   					
	   				}
	   			}
	   		});
	}


	function sortbrief() {

		//alert("aaaaaa");
        var myCheckboxes = new Array();
        $("input:checked").each(function() {
           myCheckboxes.push($(this).val());
        });
		var brand_id = $("#brand_id").val();
		var account_id = $("#account_id").val();
		var search_key = $("#search_key").val();

		//alert('search_key='+search_key+'&brand_id='+brand_id+'&account_id='+account_id+'&myCheckboxes='+myCheckboxes);

		//alert('search_key='+search_key+'&brand_id='+brand_id+'&account_id='+account_id+'&myCheckboxes='+myCheckboxes);

        $.ajax({
            type: "POST",
            url: "<?php echo base_url();?>front/dashboard/getbriefsort", 
            dataType: 'html',
            data: 'search_key='+search_key+'&brand_id='+brand_id+'&account_id='+account_id+'&myCheckboxes='+myCheckboxes,
            success: function(data){
				//alert(data);
                $('#myResponse').html(data)
               getbir(brand_id);
			   getbr(brand_id);
			   getwip(brand_id);
			   getrw(brand_id);
			   getpp(brand_id);
			   getwc(brand_id);
			   getfp(brand_id);
            }
        });
		
        return false;

        }
/*});*/

function getbir(brand_id)
{

	var account_id=$('#account_id').val();
	var search_key = $("#search_key").val();

	$.ajax({
            type: "POST",
            url: "<?php echo base_url();?>front/dashboard/getbir", 
            dataType: 'html',
            data: '&search_key='+search_key+'&account_id='+account_id+'&brand_id='+brand_id,
            success: function(data){
				//alert(data);
                $('#bir').html(data)			
            }
        });
}


function getbr(brand_id)
{

	var account_id=$('#account_id').val();
	var search_key = $("#search_key").val();
	$.ajax({
            type: "POST",
            url: "<?php echo base_url();?>front/dashboard/getbr", 
            dataType: 'html',
            data: '&search_key='+search_key+'&account_id='+account_id+'&brand_id='+brand_id,
            success: function(data){
				//alert(data);
                $('#br').html(data)			
            }
        });
}

function getwip(brand_id)
{

	var account_id=$('#account_id').val();
	var search_key = $("#search_key").val();
	$.ajax({
            type: "POST",
            url: "<?php echo base_url();?>front/dashboard/getwip", 
            dataType: 'html',
            data: '&search_key='+search_key+'&account_id='+account_id+'&brand_id='+brand_id,
            success: function(data){
				//alert(data);
                $('#wip').html(data)			
            }
        });
}


function getwc(brand_id)
{

	var account_id=$('#account_id').val();
	var search_key = $("#search_key").val();
	$.ajax({
            type: "POST",
            url: "<?php echo base_url();?>front/dashboard/getwc", 
            dataType: 'html',
            data: '&search_key='+search_key+'&account_id='+account_id+'&brand_id='+brand_id,
            success: function(data){
				//alert(data);
                $('#wc').html(data)			
            }
        });
}

function getrw(brand_id)
{

	var account_id=$('#account_id').val();
	var search_key = $("#search_key").val();
	$.ajax({
            type: "POST",
            url: "<?php echo base_url();?>front/dashboard/getrw", 
            dataType: 'html',
            data: '&search_key='+search_key+'&account_id='+account_id+'&brand_id='+brand_id,
            success: function(data){
				//alert(data);
                $('#rw').html(data)			
            }
        });
}

function getpp(brand_id)
{

	var account_id=$('#account_id').val();
	var search_key = $("#search_key").val();
	$.ajax({
            type: "POST",
            url: "<?php echo base_url();?>front/dashboard/getpp", 
            dataType: 'html',
            data: '&search_key='+search_key+'&account_id='+account_id+'&brand_id='+brand_id,
            success: function(data){
				//alert(data);
                $('#pp').html(data)			
            }
        });
}

function getfp(brand_id)
{

	var account_id=$('#account_id').val();
	var search_key = $("#search_key").val();
	$.ajax({
            type: "POST",
            url: "<?php echo base_url();?>front/dashboard/getfp", 
            dataType: 'html',
            data: '&search_key='+search_key+'&account_id='+account_id+'&brand_id='+brand_id,
            success: function(data){
				//alert(data);
                $('#fp').html(data)			
            }
        });
}







function getbranddetails(brand_id)
{
	//alert(brand_id);
}




</script>



<!-- <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.15.1/moment.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/eonasdan-bootstrap-datetimepicker/4.17.47/js/bootstrap-datetimepicker.min.js"></script>

<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/eonasdan-bootstrap-datetimepicker/4.17.47/css/bootstrap-datetimepicker.css">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/eonasdan-bootstrap-datetimepicker/4.17.47/css/bootstrap-datetimepicker.min.css">
 -->


<!-- <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.0/js/bootstrap.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.3.0/js/bootstrap-datepicker.js"></script>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.3.0/css/datepicker.css"/> -->




 <link href="<?php echo base_url(); ?>website-assets/datetimepicker/bootstrap/css/bootstrap.min.css" rel="stylesheet" media="screen">
<link href="<?php echo base_url(); ?>website-assets/datetimepicker/css/bootstrap-datetimepicker.min.css" rel="stylesheet" media="screen">

<script type="text/javascript" src="<?php echo base_url(); ?>website-assets/datetimepicker/js/bootstrap-datetimepicker.js" charset="UTF-8"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>website-assets/datetimepicker/js/locales/bootstrap-datetimepicker.fr.js" charset="UTF-8"></script>
</head>


<script type="text/javascript">


/*$(function () {
$('.datePick').datetimepicker();
});
*/

var functionUpload = function(e,no,brief_id) {

  
		//alert("goko");
		

		//alert(brief_id);

		var responseText = $.ajax({
		type:'POST',
		url:"<?php echo base_url();?>front/dashboard/getTempFolder",
		//data: {'skufolderfile_nm':skufolderfile_nm},
		async: false
		}).responseText;

		$(this).attr("folder",responseText);


		var folder= $(this).attr('folder') ;

		//alert(folder);

		var files = e.target.files,
		filesLength = files.length;

		console.log(files);

		for (var i = 0; i < filesLength; i++) {
		var file = files[i];



		console.log(file);
		console.log("bbbbbbbbbb");



		if(file != undefined){
		formData= new FormData();

		//var ext = $("#files"+no).val().split('.',2).pop().toLowerCase();

		//alert(ext);


		//if ($.inArray(ext, ['doc','docx']) == 1)
		/* if (ext=='doc' || ext=='docx')
		{*/
		formData.append("doc_file", file);
		formData.append("brief_id", brief_id);
		formData.append("folder", folder);
		formData.append("check", i);



		//alert("aaaa");
		$.ajax({
		url: "<?php echo base_url();?>front/dashboard/upload_file",
		type: "POST",
		data: formData,
		processData: false,
		contentType: false,
		success: function(response){

		//alert("koko");
		//alert(data);

		var responseArr=response.split("@@@@@@@@@@@");
      	var doc_file_name=$.trim(responseArr[0]);
		var check=$.trim(responseArr[1]);

		


		var fileName =$('#file_nm_hid_'+brief_id).val();
		//alert(fileName);

		if(fileName=='')
		{
		var fileNameCurrent=",,,"+doc_file_name+",,,";
		}
		else
		{
			if(fileName.indexOf(",,,"+doc_file_name+",,,")==-1)
			{
			var fileNameCurrent=fileName+doc_file_name+",,,";
			}
			else
			{
			var fileNameCurrent=fileName;
			}

		}

		$('#file_nm_hid_'+brief_id).val(fileNameCurrent);


		if(check==parseInt(filesLength)-1)
		{
			functionMove(e,no,brief_id);
		}

		



		}
		});
		/*}else{

		alert("Please Upload Doc");
		return false;
		}
		*/

		}
		else{
		//alert('Input something!');
		}



		//alert(i+"-----"+filesLength);

		

		
		}



    //functionMove(e,no,brief_id);
    //return deferred;
}

var functionMove = function(e,no,brief_id) {


	// alert("koko");


	var file_nm =$('#file_nm_hid_'+brief_id).val();

	//alert('&brief_id='+brief_id+'&temp_folder='+folder+'&file_nm='+file_nm);


	$.ajax({
	method:"post",
	url: "<?php echo base_url();?>front/dashboard/move_upload_file",
	data:'&brief_id='+brief_id+'&temp_folder='+folder+'&file_nm='+file_nm,
	success: function(result){

	//alert(result);
	$("#loader").hide();
	sortbrief();

	$('.re_upload_doc').on('change', function (e) {

	var no = $(this).attr('alt') ;
	var brief_id= $(this).attr('val') ;
	functionUpload(e,no,brief_id);

	/*functionUpload(e,no,brief_id).then(functionMove(e,no,brief_id));*/
	});


	//window.location.href="<?php echo base_url();?>dashboard";
	}
	});



}




$('.re_upload_doc').on('change', function (e) {

	var no = $(this).attr('alt') ;
	var brief_id= $(this).attr('val') ;
	functionUpload(e,no,brief_id);

    /*functionUpload(e,no,brief_id).then(functionMove(e,no,brief_id));*/
});



$(".re_upload_doc_old").on("change", function(e) {

 //alert("goko");
var no = $(this).attr('alt') ;
var brief_id= $(this).attr('val') ;

//alert(brief_id);

var responseText = $.ajax({
	 	type:'POST',
	 	url:"<?php echo base_url();?>front/dashboard/getTempFolder",
	 	//data: {'skufolderfile_nm':skufolderfile_nm},
	 	async: false
	 }).responseText;

$(this).attr("folder",responseText);


var folder= $(this).attr('folder') ;

 //alert(folder);

var files = e.target.files,
filesLength = files.length;

console.log(files);
for (var i = 0; i < filesLength; i++) {
	var f = files[i]
	var fileReader = new FileReader();
	fileReader.onload = (function(e) {
		var file = e.target;

    console.log(file);
    console.log("aaaaaaaaaaaaaa");




//alert(no);
var input = document.getElementById("files"+no);
//file = input.files[0];

for (var k = 0; k < filesLength; k++) {
file = input.files[k];

console.log(file);
console.log("bbbbbbbbbb");



if(file != undefined){
	formData= new FormData();

	var ext = $("#files"+no).val().split('.',2).pop().toLowerCase();

	//alert(ext);


	//if ($.inArray(ext, ['doc','docx']) == 1)
   /* if (ext=='doc' || ext=='docx')
	{*/
		formData.append("doc_file", file);
		formData.append("brief_id", brief_id);
		formData.append("folder", folder);



  //alert("aaaa");
  $.ajax({
  	url: "<?php echo base_url();?>front/dashboard/upload_file",
  	type: "POST",
  	data: formData,
  	processData: false,
  	contentType: false,
  	success: function(data){

	 //alert("koko");
     //alert(data);

     var doc_file_name=$.trim(data);


        var fileName =$('#file_nm_hid_'+brief_id).val();

		if(fileName=='')
		{
			var fileNameCurrent=",,,"+doc_file_name+",,,";
		}
		else
		{
			if(fileName.indexOf(",,,"+doc_file_name+",,,")==-1)
			{
				var fileNameCurrent=fileName+doc_file_name+",,,";
			}
			else
			{
				var fileNameCurrent=fileName;
			}
			
		}
        
      	$('#file_nm_hid_'+brief_id).val(fileNameCurrent);



}
});
/*}else{

	alert("Please Upload Doc");
	return false;
}
*/

}
else{
alert('Input something!');
}


}

    

});
fileReader.readAsDataURL(f);
}






});






</script>



