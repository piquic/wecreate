<?php
session_start();
error_reporting(0); 
 
if ($this->session->userdata('front_logged_in')) {
	$session_data = $this->session->userdata('front_logged_in');
	$user_id = $session_data['user_id'];
	$user_name = $session_data['user_name'];
	$_SESSION['user_id']=$user_id;
	$_SESSION['user_name']=$user_name;
	$type1 = $session_data['user_type_id'];
	$user_type_id=$type1;
	$brand_access=$session_data['brand_access'];
	$brief_access=$session_data['brief_access'];

	$checkquerys = $this->db->query("select user_type_name from wc_users_type where user_type_id='$user_type_id' ")->result_array();
	$job_title=$checkquerys[0]['user_type_name'];
	$checkquerys1 = $this->db->query("select * from wc_users where user_id='$user_id' ")->result_array();
	$brief_permission=$checkquerys1[0]['brief_permission'];
	//$briefstatus_permission=$checkquerys1[0]['briefstatus_permission'];
} 
else
{
	$user_id = '';
	$user_name = ''; 
	$_SESSION['user_id']=$user_id;
	$_SESSION['user_name']=$user_name;
}
$lastID ="";
?>

<?php $data['page_title'] = "dashboard";

$this->load->view('front/includes/header',$data);
$this->load->view('front/includes/topnav');
?>


<!-----------------------------------------------pop up----------------------------------------------------->
<style>
.feed_back {
  position: fixed;
  top: 0;
  left: 0;
  z-index: 1072;
  display: block;
  width: 100%;
  height: 100%;
  overflow-x: hidden;
  overflow-y: auto;
  outline: 0;
  background-color: #0000009c;
  background-size: cover;
}
/*.feed_back*/
.feed_back::-webkit-scrollbar { width: 0em; } 
.feed_back::-webkit-scrollbar-track { -webkit-box-shadow: inset 0 0 1px rgba(0,0,0,0); background-clip:content-box;  }
.feed_back::-webkit-scrollbar-thumb { background-color: #76aea1; border-radius:.7em; width:5px; }

.feed_back_form {
  width: auto;
  margin: .5rem;

  transition: -webkit-transform .3s ease-out;
  transition: transform .3s ease-out;
  transition: transform .3s ease-out,-webkit-transform .3s ease-out;
  -webkit-transform: none;
  transform: none;

  max-width: 45rem;
  margin: 1.75rem auto;
}

.feed_back_form_content {
  position: relative;
  display: -ms-flexbox;
  display: flex;
  -ms-flex-direction: column;
  flex-direction: column;
  width: 100%;
  pointer-events: auto;
  background-color: #fff;
  background-clip: padding-box;
  outline: 0;
}

</style>

<?php 

    $session_data = $this->session->userdata('front_logged_in');
	$user_id = $session_data['user_id'];
	$user_type_id = $session_data['user_type_id'];

	if($user_type_id=='4')
	{

	
	$user_name = $session_data['user_name'];
	$type1 = $session_data['user_type_id'];
	$brand_access=$session_data['brand_access'];
	//$brief_access=$session_data['brief_access'];
	$client_access=$session_data['client_access'];
	$user_type_id=$type1;
	$checkquerys = $this->db->query("select brief_id from wc_users where user_id='$user_id' ")->result_array();
	$brief_access=$checkquerys[0]['brief_id'];
	

	$sql="  select * from  wc_brief WHERE 1=1 ";
	 $sql.="  and  status ='5' ";
    $sql.="  and  brand_id IN(".trim($brand_access,",").")";
	$sql.=" order by brief_id desc ";
	
	//echo $sql;
	$query=$this->db->query($sql);
	$all_result= $query->result_array();

	//echo "<pre>";
	//print_r($all_result);

	if(!empty($all_result))
	{


	$feedback_brief_id=$all_result[0]['brief_id'];
	$feedback_brief_title=stripslashes($all_result[0]['brief_title']);


	 $query=$this->db->query("select * from  wc_settings  WHERE setting_key='MAIL_FROM_TEXT' ");
     $userDetails= $query->result_array();
     $MAIL_FROM_TEXT=$userDetails[0]['setting_value'];

?>

 <div class=" feed_back" id="feedback_form">
	<div class="feed_back_form">
		<div class="feed_back_form_content"> 
			<div class="container-fluid pt-5 pb-4">
				<div class="row">
					<div class="col-12 col-sm-12 col-md-12">
						<div class="h1 text-center"><?php echo $MAIL_FROM_TEXT;?> Feedback</div>
					</div>
				</div>

				<div class="row">
					<div class="col-12 col-sm-12 col-md-12">
						<div class="lead text-center px-3">Rate the work on "<b><?php echo $feedback_brief_title;?></b>"</div>
					</div>
				</div>

				<div class="row">
					<div class="col-12 col-sm-12 col-md-12 pt-4 px-5">


						<input type="hidden" name="brief_id" id="brief_id" value="<?php echo $feedback_brief_id;?>">
					    <input type="hidden" name="user_id" id="user_id" value="<?php echo $user_id;?>">


					    	<?php

					$feedback_type_sql = "SELECT * FROM `wc_feedback_type` where status='Active' and deleted='0'  ";

					$feedback_type_query = $this->db->query($feedback_type_sql);
					$feedback_type_result=$feedback_type_query->result_array();

					if(!empty($feedback_type_result))
					{
					foreach($feedback_type_result as $key => $value)
					{
						$feedback_type_id=$value['feedback_type_id'];
						$feedback_type=$value['feedback_type'];

						?>	


						<div class="row pb-3">
							<div class="col-12 col-sm-12 col-md-6">
								<p style="font-size: 1.5em"><?php echo $feedback_type;?></p>
								<input type="hidden" name="hid_feedback_type_id[]" id="hid_feedback_type_id" value="<?php echo $feedback_type_id;?>">
							</div>

							<div class="col-12 col-sm-12 col-md-6">
								<div class="d-flex justify-content-between <?php if($feedback_type_id == 4 || $feedback_type_id == 7) { ?>pt-3<?php } ?>">
									<span><i id="far_<?php echo $feedback_type_id;?>_1" class="far_all<?php echo $feedback_type_id;?> h1 text-muted far fa-angry" onclick="getReviewValue('1','<?php echo $feedback_type_id;?>');"></i></span>
									<span><i id="far_<?php echo $feedback_type_id;?>_2" class="far_all<?php echo $feedback_type_id;?> h1 text-muted far fa-frown" onclick="getReviewValue('2','<?php echo $feedback_type_id;?>');"></i></span>
									<span><i id="far_<?php echo $feedback_type_id;?>_3" class="far_all<?php echo $feedback_type_id;?> h1 text-muted far fa-meh" onclick="getReviewValue('3','<?php echo $feedback_type_id;?>');"></i></span>
									<span><i id="far_<?php echo $feedback_type_id;?>_4" class="far_all<?php echo $feedback_type_id;?> h1 text-muted far fa-smile" onclick="getReviewValue('4','<?php echo $feedback_type_id;?>');"></i></span>
									<span><i id="far_<?php echo $feedback_type_id;?>_5" class="far_all<?php echo $feedback_type_id;?> h1 text-muted far fa-laugh" onclick="getReviewValue('5','<?php echo $feedback_type_id;?>');"></i></span>
								</div>
							</div>
							<input type="hidden" name="hid_review[]" id="hid_review_<?php echo $feedback_type_id;?>" value="">
						</div>


							<?php
						
							}
							}
							?>
						

					</div>
				</div>

				<div class="row">
					<div class="col-12 col-sm-12 col-md-12 px-5">
						<textarea class="form-control rounded-0" rows="4" placeholder="Anything you would like to add as a reason for your rating" name="hid_review_reason" id="hid_review_reason"></textarea>
					</div>
				</div>

				<div class="row">
					<div class="col-12 col-sm-12 col-md-12 px-5 pt-3 text-center">
						<button id="submitFeedback" type="button" class="btn btn-dark btn-lg w-75" disabled onclick="submitFeedback();">Send Feedback</button>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>


<script>

	function getReviewValue(val,id)
		{
			//alert(val);
			//alert(id);

			/*var arr=val.split("-");
			var feedback_rating_id=$.trim(arr[0]);
			var feedback_rating=$.trim(arr[1]);*/
			var feedback_rating_id='';
			var feedback_rating=val;
			
			$("#hid_review_"+id).val(feedback_rating);
			$(".far_all"+id).addClass("text-muted");
			$("#far_"+id+"_"+val).removeClass("text-muted");

			var myArray= new Array();
			$("input[name='hid_review[]']").each(function(){
				if($(this).val()!='')
				{
					myArray.push($(this).val());
				}
				else
				{
					myArray.push('error');
				}
			
			}); 


			if( $.inArray("error", myArray) !== -1 ) {
               $("#submitFeedback").attr( "disabled", "disabled" );

			}
			else
			{
				
				$("#submitFeedback").removeAttr( "disabled" );
			}


		


		}
		


/*	function setReviewReason(val,id,content_id)
	{
		$("#hid_review_reason_"+id).val(val);

		var val = val.replace(/ /g, '_');

		//alert(val);

		$(".qty_btn_"+id).removeClass("btn-wc-green");
		$("#qty_"+content_id).addClass("btn-wc-green");
		 

	}*/

	


	function submitFeedback()
	{

		var brief_id=$("#brief_id").val();
		var user_id=$("#user_id").val();

		var hid_feedback_type_id = $('input[name="hid_feedback_type_id[]"]').map(function () {
        if(this.value!='')
        {return this.value; // $(this).val()
        }
        else
        {return 0;}
		
        }).get();



        console.log(hid_feedback_type_id);

        var hid_review = $('input[name="hid_review[]"]').map(function () {
        if(this.value!='')
        {return this.value; // $(this).val()
        }
        else
        {return 0;}
		
        }).get();

        console.log(hid_review);


        /*var check_hid_review=$.inArray( 0, hid_review );
         console.log(check_hid_review);*/

        if($.inArray( 0, hid_review )==0||$.inArray( 0, hid_review )==1)
        {
        	// alert("You need to enter review before proceed.");
        	$('#modal_msg').modal('show');
			$('#txtMsg').html('You need to enter review before proceed.');
        	return false;
        }

        var hid_review_reason=$("#hid_review_reason").val();

       /* var hid_review_reason = $('input[name="hid_review_reason[]"]').map(function () {
        if(this.value!='')
        {return this.value; // $(this).val()
        }
        else
        {return 0;}
		
        }).get();

        console.log(hid_review_reason);

         if($.inArray( 0, hid_review_reason )==0||$.inArray( 0, hid_review_reason )==1)
        {
        	// alert("You need to enter reason before proceed.");
        	$('#modal_msg').modal('show');
			$('#txtMsg').html('You need to enter reason before proceed.');
        	return false;
        }
*/

    // alert("&user_id="+user_id+"&brief_id="+brief_id+"&feedback_type_id="+hid_feedback_type_id+"&review="+hid_review+"&review_reason="+hid_review_reason);


		$.ajax({

		method:'POST',
		url: "<?php echo base_url();?>front/feedback/insertFeedback",
		/*data:"&brief_id="+brief_id+"&quality="+quality+"&quality_reason="+quality_reason+"&speed="+speed+"&speed_reason="+speed_reason,*/
		data: {user_id:user_id,brief_id:brief_id,feedback_type_id:hid_feedback_type_id,review:hid_review,review_reason:hid_review_reason},
		success: function(result){
		//alert(result);
		//if(result) {

				<?php
				if ($this->session->userdata('front_logged_in')) {
				?>
				window.location.href="<?php echo base_url();?>dashboard";
				<?php
				}
				else
				{
				?>

				window.location.href="<?php echo base_url();?>feedback-thankyou";
				<?php

				}
				?>

		


		//}
		}
		});




		
		
		/*var quality=$("#hid_quality").val();
		var quality_reason=$("#hid_quality_reason").val();

		var speed=$("#hid_speed").val();
		var speed_reason=$("#hid_speed_reason").val();




		if(quality=='')
		{
			alert("Please select Quality Rating.");
			return false;
		}
        else if(quality_reason=='')
		{
			alert("Please select Quality Reason.");
			return false;
		}
        else if(speed=='')
		{
			alert("Please select Speed Rating.");
			return false;
		}
        else if(speed_reason=='')
		{
			alert("Please select Speed Reason.");
			return false;
		}

        else
		{

		

		$.ajax({

	   			method:'POST',
	   			url: "<?php echo base_url();?>front/feedback/insertFeedback",
	   			data:"&brief_id="+brief_id+"&quality="+quality+"&quality_reason="+quality_reason+"&speed="+speed+"&speed_reason="+speed_reason,

	   			success: function(result){
	   				//alert(result);
	   				//if(result) {

	   					window.location.href="<?php echo base_url();?>dashboard";
	   					
	   					
	   				//}
	   			}
	   		});







	   }*/




	}

</script>



<?php

}
  }
?>


<!---------------------------------------------------------------------------------------------------->






<div class="bg-wc-light px-5" id="div_filter">
	<div class="row">
		<div class="col-12 col-sm-12 col-md-10">
			<div class="d-flex justify-content-start flex-wrap">
				<?php if($user_type_id=='4'||($brief_permission==1 && $user_type_id=='3')) { ?>
				<!-- <div class="p-2 icon-md">|</div> -->
						<div class="pt-2 mr-4 mt-1">
							
								<a class="text-wc lead" href="<?php echo base_url() ?>uploadbrief"><i class="fas fa-cloud-upload-alt"></i>&nbsp;ADD PROJECT</a>
							
						</div>
				<?php } ?>

				<?php
				$acount_permission=$this->Dashboard_model->checkPermission($user_type_id,1,'access');
				if($acount_permission=='1') {
					?>

					<div class="py-2 mr-2">
						<span class="btn btn-wc" data-toggle="collapse" data-target="#collapseAccount" aria-expanded="false" aria-controls="collapseAccount">Select Account&emsp;<i class="fas fa-chevron-down"></i></span>

						<div id="collapseAccount" class="card collapse" style="width: 20rem; position: absolute; z-index: 2221;" data-parent="#div_filter">
							<div class="card-body p-3">
								<div class="d-flex justify-content-around">
									<p class="flex-grow-1 font-weight-bold">Filter by Account</p>
									<span data-toggle="collapse" data-target="#collapseAccount" aria-expanded="false" aria-controls="collapseAccount"><i class="fas fa-times"></i></span>
								</div>

								<hr class="pt-0 mt-0">

								<div class="pl-3">
									<?php
									
									if($user_type_id==2)
										
										{
											$users_type_details=$this->db->query("SELECT * FROM `wc_clients` INNER JOIN wc_users WHERE wc_clients.client_id=wc_users.client_id and wc_clients.deleted='0' and wc_clients.status='Active' and wc_users.user_id='$user_id'")->result_array();
											
										}
										else
										{
									$users_type_details=$this->db->query("select * from  wc_clients where wc_clients.deleted='0' and wc_clients.status='Active'  ")->result_array();
										}

									if(!empty($users_type_details)) {
										foreach($users_type_details as $key => $userstypedetails) {
											$client_id=$userstypedetails['client_id'];
											$client_name=$userstypedetails['client_name'];
											?>

											<div class="custom-control custom-radio">
												<input type="radio" class="custom-control-input account_id" id="account<?php echo $client_id;?>" name="<?php echo $client_id;?>" >
												<label class="custom-control-label roundCheck" for="account<?php echo $client_id;?>"><?php echo ucfirst($client_name);?></label>
											</div>

										<?php } } ?>
									</div>
								</div>
							</div>
						</div>

					<?php } else { ?>
						<input type="hidden" name="account_id" id="account_id" value="">
					<?php } ?>

					<?php
					$account_permission = $this->Dashboard_model->checkPermission($user_type_id, 1, 'access');
					$brand_permission   = $this->Dashboard_model->checkPermission($user_type_id, 2, 'access');
					$admin_permission   = $this->Dashboard_model->checkPermission($user_type_id, 3, 'access');

					if( $account_permission == '1' || $brand_permission == '1' || $admin_permission == '1' )
					{

						$checkquerys = $this->db->query("select * from wc_users where  user_id='$user_id' ")->result_array();
						$brand_id = $checkquerys[0]['brand_id'];
						$client_id = $checkquerys[0]['client_id'];

						?>

						<div class="py-2 mr-2">
							<span class="btn btn-wc" data-toggle="collapse" data-target="#collapseBrand" aria-expanded="false" aria-controls="collapseBrand">Select Brand&emsp;<i class="fas fa-chevron-down"></i></span>

							<div id="collapseBrand" class="card collapse" style="width: 20rem; position: absolute; z-index: 2222;" data-parent="#div_filter">
								<div class="card-body p-3">
									<div class="d-flex justify-content-around">
										<p class="flex-grow-1 font-weight-bold">Filter by Brand</p>
										<span data-toggle="collapse" data-target="#collapseBrand" aria-expanded="false" aria-controls="collapseBrand"><i class="fas fa-times"></i></span>
									</div>

									<hr class="pt-0 mt-0"><div class="mb-3">
										<input class="form-control border" type="text" placeholder="enter brand name" onkeyup="sortbrand(this.value,<?php echo $user_type_id; ?>,<?php echo $client_id; ?>,<?php echo $lastID; ?>)">
									</div>

									<div class="pl-3" id="brand_list">
										<?php 
										$brand_id_check_add="";
										$sql="select * from wc_brands where status='Active' and client_id=$client_id";

										if( ($user_type_id=='2' || $user_type_id=='3') &&  $client_id!='0')
										{
											$sql.=" and brand_id in (select brand_id from wc_brands where client_id='".$client_id."') ";
										}

										$brand_type_check=$this->db->query($sql)->result_array();

										foreach($brand_type_check as $key => $brandstypedetails){
											$brand_id_check_add.=$brandstypedetails['brand_id'].",";
										}
										//echo $brand_id_check_add;
										$sql1="select * from wc_brief WHERE 1=1 and brand_id IN(".rtrim($brand_id_check_add,",").")";
												$brand_id_check_add=$this->db->query($sql1)->result_array();

									$all_count_check=count($brand_id_check_add); ?>
										<?php /*<div class="custom-control custom-radio">
											<input type="radio" class="custom-control-input brand_id" id="brand_all>" name="brand_id[]" onchange="sortbrief('')" value=''>
											<label class="custom-control-label roundCheck lead" for="brand_all>">All Brand <span class="badge badge-wc" id="cnt_<?php echo $brand_id;?>"><?php echo $all_count_check; ?></span></label>
										</div>*/?>
										<?php

										$sql="select * from wc_brands where status='Active' ";

										if( ($user_type_id=='2' || $user_type_id=='3') &&  $client_id!='0')
										{
											$sql.=" and brand_id in (select brand_id from wc_brands where client_id='".$client_id."') ";
										}

										if( $user_type_id=='4' &&  $brand_id!='0' )
										{
											//$sql.=" and brand_id in (".$brand_id.") ";
											$sql.=" and brand_id in (".trim($brand_id,",").") ";
										}

										$brand_type_details=$this->db->query($sql)->result_array();
										if(!empty($brand_type_details)){
											foreach($brand_type_details as $key => $brandstypedetails){
												$brand_id=$brandstypedetails['brand_id'];
												$brand_name=$brandstypedetails['brand_name'];

												$brand_id_check=$brandstypedetails['brand_id'];

												$sql="select * from wc_brief WHERE 1=1 and brand_id IN(".trim($brand_id_check,",").")";
												$brand_type_check=$this->db->query($sql)->result_array();

												$count_check=count($brand_type_check);
												?>

												<div class="custom-control custom-radio">
													<input type="radio" class="custom-control-input brand_id" id="brand_<?php echo $brand_id;?>" name="brand_id[]" onchange="sortbrief('<?php echo $lastID  ?>')" value='<?php echo $brand_id; ?>' brand_name="<?php echo $brand_name ?>">
													<label class="custom-control-label roundCheck lead" for="brand_<?php echo $brand_id;?>"><?php echo ucfirst($brand_name);?> <span class="badge badge-wc" id="cnt_<?php echo $brand_id;?>"><?php echo $count_check; ?></span></label>
												</div>

											<?php } } ?>
										</div>
									</div>
								</div>
							</div>

						<?php } else { ?>
							<input type="hidden" name="brand_id" id="brand_id" value="">
							<!-- <input type="hidden" name="search_key" id="search_key" value=""> -->
						<?php } ?>

						<?php /*<!-- <div class="p-2 icon-md">|</div> -->
						<div class="pt-2 mr-4 mt-1">
							<?php if($user_type_id=='4') { ?>
								<a class="text-wc lead" href="<?php echo base_url() ?>uploadbrief"><i class="fas fa-cloud-upload-alt"></i>&nbsp;ADD PROJECT</a>
							<?php } ?>
						</div>*/?>

						<div class="py-2 mr-2">
							<span class="btn btn-wc" data-toggle="collapse" data-target="#collapseFilter" aria-expanded="false" aria-controls="collapseFilter">Filter&emsp;<i class="fas fa-chevron-down"></i></span>

							<div id="collapseFilter" class="card collapse" style="width: 20rem; position: absolute; z-index: 2223;" data-parent="#div_filter">
								<div class="card-body p-3">
									<div class="d-flex justify-content-around">
										<p class="flex-grow-1 font-weight-bold">Filter by Stages</p>
										<span data-toggle="collapse" data-target="#collapseFilter" aria-expanded="false" aria-controls="collapseFilter"><i class="fas fa-times"></i></span>
									</div>

									<hr class="pt-0 mt-0">

									<div class="pl-3">
										<!-- <div class="custom-control custom-radio">
											<input type="radio" class="custom-control-input status_filter" name="breif_status[]" id="bsfall"  value="" onchange="sortbrief('')">
											<label class="custom-control-label roundCheck lead" for="bsfall">All Brand <span class="badge badge-wc" id="bir"></span></label>
										</div> -->
										<div class="custom-control custom-radio">
											<input type="radio" class="custom-control-input status_filter" name="breif_status[]" id="bsf0"  value="0" onchange="sortbrief('<?php echo $lastID  ?>')">
											<label class="custom-control-label roundCheck lead" for="bsf0">Brief in Review <span class="badge badge-wc" id="bir"><?php echo $briefinreview; ?></span></label>
										</div>

										<div class="custom-control custom-radio">
											<input type="radio" class="custom-control-input status_filter" name="breif_status[]" id="bsf4" value="4" onchange="sortbrief('<?php echo $lastID  ?>')">
											<label class="custom-control-label roundCheck lead" for="bsf4">Brief Rejected <span class="badge badge-wc" id="br"><?php echo $briefrejected; ?></span></label>
										</div>

										<div class="custom-control custom-radio">
											<input type="radio" class="custom-control-input status_filter" name="breif_status[]" id="bsf1" value="1" onchange="sortbrief('<?php echo $lastID  ?>')">
											<label class="custom-control-label roundCheck lead" for="bsf1">Work in Progress <span class="badge badge-wc" id="wip"><?php echo $workinprogress; ?></span></label>
										</div>

										<div class="custom-control custom-radio">
											<input type="radio" class="custom-control-input status_filter" name="breif_status[]" id="bsf6" value="6" onchange="sortbrief('<?php echo $lastID  ?>')">
											<label class="custom-control-label roundCheck lead" for="bsf6">Proofing Pending <span class="badge badge-wc" id="pp"><?php echo $proffing_pending; ?></span></label>
										</div>

										<div class="custom-control custom-radio">
											<input type="radio" class="custom-control-input status_filter" name="breif_status[]" id="bsf3" value="3" onchange="sortbrief('<?php echo $lastID  ?>')">
											<label class="custom-control-label roundCheck lead" for="bsf3">Revision Work <span class="badge badge-wc" id="rw"><?php echo $revision_work; ?></span></label>
										</div>

										<div class="custom-control custom-radio">
											<input type="radio" class="custom-control-input status_filter" name="breif_status[]" id="bsf2" value="2" onchange="sortbrief('<?php echo $lastID  ?>')">
											<label class="custom-control-label roundCheck lead" for="bsf2">Work Complete <span class="badge badge-wc" id="wc"><?php echo $work_complete; ?></span></label>
										</div>                              

                        <!-- <div class="custom-control custom-radio">
                            <input type="radio" class="custom-control-input" name="breif_status[]" id="bsf5" value="5" onchange="sortbrief('<?php echo $lastID  ?>')">
                            <label class="custom-control-label roundCheck lead" for="bsf5">Feedback Pending <span class="badge badge-wc" id="fp"><?php echo $feedback_pending; ?></span></label>
                          </div> -->

                          <div class="custom-control custom-radio">
                          	<input type="radio" class="custom-control-input status_filter" name="breif_status[]" id="bsf7" value="7" onchange="sortbrief('<?php echo $lastID  ?>')">
                          	<label class="custom-control-label roundCheck lead" for="bsf7">Archived <span class="badge badge-wc" id="fc"><?php echo $feedback_completed; ?></span></label>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
             <!-- <div class="pt-2 mt-1">
             	<input type="hidden" class="form-control" id="search_key" name="search_key" placeholder="Search Brief Id" onkeyup="sortbrief('<?php echo $lastID  ?>');">
             </div> -->

             <!-- <div class="pt-1 mr-2 mt-1">
             	<form>
             		<input type="text" class="form-control" id="search_key" name="search_key" placeholder="Search By Id, Title" onkeyup="sortbrief('<?php echo $lastID  ?>');">
             	</form>

             </div>-->

           </div>
         </div>
         <div class="col-12 col-sm-12 col-md-2">
         	<div class="pt-1 my-1">
         		<form>
         			<input type="text" class="form-control" id="search_key" name="search_key" placeholder="Search By Id, Title" onkeyup="sortbrief('<?php echo $lastID  ?>');">
         		</form>

         	</div>
         </div>
       </div>
     </div>

     <div class="d-flex justify-content-start flex-wrap p-2 pt-4 px-5 mb-3" id="div_sel_filter">
     	<!-- <span class="py-1 px-2 text-wc border-wc rounded-pill">In Review&emsp;<i class="fas fa-times text-danger"></i></span> -->
     </div>

     <div class="container-fluid px-5">
     	<div class="row">

     		<!-- Main Body -->
     		<div class="col-12 col-sm-12 col-md-12" id="myResponse">
     			<div id="tblbrf" class="row">

     				<?php // print_r($dashboradinfo);
     				//print_r($showLimit);	
     				$this->load->view('front/dashboard_search', $dashboradinfo);   ?>
     			</div>
				</div>

			</div>
		</div>

<?php $this->load->view('front/includes/footer'); ?>
<!--<link href="<?php echo base_url(); ?>website-assets/datetimepicker/bootstrap/css/bootstrap-datetimepicker.min.css" rel="stylesheet" media="screen">
<link href="<?php echo base_url(); ?>website-assets/datetimepicker/css/bootstrap-datetimepicker.min.css" rel="stylesheet" media="screen">

<script type="text/javascript" src="<?php echo base_url(); ?>website-assets/datetimepicker/js/bootstrap-datetimepicker.js" charset="UTF-8"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>website-assets/datetimepicker/js/locales/bootstrap-datetimepicker.fr.js" charset="UTF-8"></script>-->
<script type="text/javascript">
$(document).ready(function(){

	//alert("aa");
	$(window).scroll(function(){
		//alert("scroll");
		
		var lastID = $('.load-more').attr('lastID');
		//alert(lastID);
	//	console.log(lastID);

		if(($(window).scrollTop() == $(document).height() - $(window).height()) && (lastID != 0)){
			
			sortbrief(lastID);
			
		}
	});
});




function delete_card(brief_id){
	$("#loader").show();
	$.ajax({
		type: "POST",
		url: "<?php echo base_url();?>front/dashboard/delete_card", 
		dataType: 'html',
		data: 'brief_id='+brief_id,
		success: function(data){
			$("#loader").hide();
			
			//$('#modal_confirm_'+brief_id).modal('hide');
			var lastID="";
			sortbrief(lastID);
			// $("#click_"+brief_id).collapse('hide');
			// $("#card_"+brief_id).addClass('d-none');		
		}
	});
}

function price_cal(brief_id,add_discount,add_extra_charges){
	
	var product_id=$("#product_id_"+brief_id).val();
	var txtQty=$("#txtQty_"+brief_id).val();
	//alert(product_id);
	if(product_id!="0" && txtQty!=""){
		$("#no-result"+brief_id).addClass('d-none');
		//btnadd_ec_
		var calculated_total_sum = 0;
		$.ajax({
			type: "POST",
			url: "<?php echo base_url();?>front/dashboard/price_cal", 
			dataType: 'html',
			data: 'product_id='+product_id+"&txtQty="+txtQty+"&brief_id="+brief_id,
			success: function(data){
				if(product_id=='discount' || product_id=='extra_charges'){
					$('#product_discount_extra'+brief_id).append(data);
				}
				else{
					$('#product-list'+brief_id).append(data);
				}
				$("#product_id_"+brief_id+" option[value="+product_id +"]").prop('disabled', true);
				$("#product_id_"+brief_id).prop("selectedIndex", 0);
				$("#txtQty_"+brief_id).val("");
				$("#product-list"+brief_id+" #subtotal_price"+brief_id).each(function () {
					var get_textbox_value = $(this).val();
					if ($.isNumeric(get_textbox_value)){
						calculated_total_sum += parseFloat(get_textbox_value);
					}                  
				});
				$("#total_price"+brief_id).val(calculated_total_sum);
				//alert($("#discount"+brief_id).val());
				if(typeof $("#discount"+brief_id).val()!=="undefined"){
					var txtDscnt=$("#discount"+brief_id).val();
					calculated_total_sum = calculated_total_sum-txtDscnt;
				
				}
				if(typeof $("#extra_charges"+brief_id).val()!="undefined"){
					var txtEC=$("#extra_charges"+brief_id).val();
					calculated_total_sum = calculated_total_sum+parseFloat(txtEC);
				}
				//alert(calculated_total_sum);
				$("#total_price_values"+brief_id).html(calculated_total_sum);
				
			}
		});
	}
	if(product_id=="0"){
		$("#errmsg_product_id"+brief_id).html("Select Product").show().fadeOut(1500);
	}
	if(txtQty==""){
		$("#errmsg_txtQty"+brief_id).html("Enter the value").show().fadeOut(1500);
	}
}
function view_product_add(brief_id,add_discount,add_extra_charges){
	
	var product_id=$("#view_product_id_"+brief_id).val();
	var txtQty=$("#view_txtQty_"+brief_id).val();
	var txtEC="";
	var txtDscnt="";

	if(product_id=="discount"){
		var txtDscnt=txtQty;
		
	}
	if(product_id=="extra_charges"){
		var txtEC=txtQty;
	}
	if(product_id!="0" && txtQty!=""){
		$("#view_no-result"+brief_id).addClass('d-none');
		var calculated_total_sum = 0;
		//var txtQty=1;
		//alert(txtDscnt+"---"+txtEC);
		$.ajax({
			type: "POST",
			url: "<?php echo base_url();?>front/dashboard/view_product_add", 
			dataType: 'html',
			data: 'product_id='+product_id+"&txtQty="+txtQty+"&brief_id="+brief_id+"&txtDscnt="+txtDscnt+"&txtEC="+txtEC,
			success: function(data){

				if(product_id=='discount' || product_id=='extra_charges'){
					$('#view_product_discount_extra'+brief_id).append(data);
				}
				else{
					$('#view_product-list'+brief_id).append(data);
				}

				$("#view_product_id_"+brief_id+" option[value="+product_id +"]").prop('disabled', true);
				$("#view_product_id_"+brief_id).prop("selectedIndex", 0);
				$("#view_txtQty_"+brief_id).val("");
				$("#view_product-list"+brief_id+" .subtotal_price_"+brief_id).each(function () {
					var get_textbox_value = $(this).val();
					if ($.isNumeric(get_textbox_value)) {
						calculated_total_sum += parseFloat(get_textbox_value);
					}                  
				});
				$("#total_price"+brief_id).val(calculated_total_sum);	
				if(typeof $("#discount"+brief_id).val()!=="undefined"){
					var txtDscnt=$("#discount"+brief_id).val();
					calculated_total_sum = calculated_total_sum-txtDscnt;
				
				}
				if(typeof $("#extra_charges"+brief_id).val()!="undefined"){
					var txtEC=$("#extra_charges"+brief_id).val();
					calculated_total_sum = calculated_total_sum+parseFloat(txtEC);
				}
				//alert(calculated_total_sum);
				$("#total_price_values"+brief_id).html(calculated_total_sum);
			
				
				
			}
		});
	}
	if(product_id=="0"){
		$("#view_errmsg_product_id"+brief_id).html("Select Product").show().fadeOut(1500);
	}
	if(txtQty==""){
		$("#view_errmsg_txtQty"+brief_id).html("Enter the value").show().fadeOut(1500);
	}	
}
function update_price_cal(brief_id,brief_product_id,update_quty,add_discount,add_extra_charges ){
	//	$("#no-result").addClass('d-none');
	var product_id=$("#product_id"+brief_product_id).val();
	var txtQty=$("#product_qty"+brief_product_id).val();
	//alert(txtQty);
	var calculated_total_sum=0;
	if(txtQty==""){
		txtQty=0;
	} 
	//alert(add_discount);

	$.ajax({
		type: "POST",
		url: "<?php echo base_url();?>front/dashboard/price_cal", 
		dataType: 'html',
		data: 'product_id='+product_id+"&txtQty="+txtQty+"&brief_id="+brief_id+"&update_quty="+update_quty,
		success: function(data){
    	//alert(data);
    	$('#subtotal_price_'+brief_product_id).val(data);
    	$('#div_subtotal_price_'+brief_product_id).html("Rs."+data);
    	$("#view_product-list"+brief_id+" .subtotal_price_"+brief_id).each(function () {
    		var get_textbox_value = $(this).val();
    		if ($.isNumeric(get_textbox_value)) {
    			calculated_total_sum += parseFloat(get_textbox_value);
    		}                  
    	});

				$("#total_price"+brief_id).val(calculated_total_sum);
				// alert(calculated_total_sum+"asdfa-----"+txtDscnt+"sdrfag---"+txtEC);
				if(typeof $("#discount"+brief_id).val()!=="undefined"){
					var txtDscnt=$("#discount"+brief_id).val();
					calculated_total_sum = calculated_total_sum-txtDscnt;
				
				}
				if(typeof $("#extra_charges"+brief_id).val()!="undefined"){
					var txtEC=$("#extra_charges"+brief_id).val();
					calculated_total_sum = calculated_total_sum+parseFloat(txtEC);
				}
				//alert(calculated_total_sum);
				$("#total_price_values"+brief_id).html(calculated_total_sum);
    }
  });
}

function price_cancel(brief_id,product_id,add_discount,add_extra_charges){
	$('#'+brief_id+"_"+product_id).remove();
	$("#product_id_"+brief_id+" option[value="+product_id +"]").prop('disabled', false);
	var calculated_total_sum = 0;

	$("#product-list"+brief_id+" #subtotal_price"+brief_id).each(function () {
		var get_textbox_value = $(this).val();
		if ($.isNumeric(get_textbox_value)) {
			calculated_total_sum += parseFloat(get_textbox_value);
		}                  
	});
	if($("#product-list"+brief_id+" tr").length ==1){
	    $("#no-result"+brief_id).removeClass('d-none');
	}
	$("#total_price"+brief_id).val(calculated_total_sum);
	// alert(calculated_total_sum+"asdfa-----"+txtDscnt+"sdrfag---"+txtEC);
	if(typeof $("#discount"+brief_id).val()!=="undefined"){
		var txtDscnt=$("#discount"+brief_id).val();
		calculated_total_sum = calculated_total_sum-txtDscnt;
	
	}
	if(typeof $("#extra_charges"+brief_id).val()!="undefined"){
		var txtEC=$("#extra_charges"+brief_id).val();
		calculated_total_sum = calculated_total_sum+parseFloat(txtEC);
	}
	//alert(calculated_total_sum);
	$("#total_price_values"+brief_id).html(calculated_total_sum);
}
function price_edit(brief_product_id){
	//  alert("work edit");
	$("#div_product_quty"+brief_product_id).addClass("d-none");
	$("#product_qty"+brief_product_id).removeClass("d-none");
	$("#edit_product"+brief_product_id).addClass("d-none");
	$("#delete_product"+brief_product_id).addClass("d-none");
	$("#update_product"+brief_product_id).removeClass("d-none");
	$("#cancel_product"+brief_product_id).removeClass("d-none");    
}
function cancel_product(brief_product_id){
	$("#div_product_quty"+brief_product_id).removeClass("d-none");
	$("#product_qty"+brief_product_id).addClass("d-none");
	$("#edit_product"+brief_product_id).removeClass("d-none");
	$("#delete_product"+brief_product_id).removeClass("d-none");
	$("#update_product"+brief_product_id).addClass("d-none");
	$("#cancel_product"+brief_product_id).addClass("d-none"); 
}

function price_delete(brief_id,brief_product_id){
	//$('#click_'+brief_id).addClass('hide');
	$("#div_product_quty"+brief_product_id).removeClass("d-none");
	$("#product_qty"+brief_product_id).addClass("d-none");
	$("#edit_product"+brief_product_id).removeClass("d-none");
	$("#delete_product"+brief_product_id).removeClass("d-none");
	$("#update_product"+brief_product_id).addClass("d-none");
	$("#cancel_product"+brief_product_id).addClass("d-none"); 

}
function delete_product(brief_id,brief_product_id,add_discount,add_extra_charges){
	$("#loader").show();
	var product_id=$("#product_id"+brief_product_id).val();
	//alert(product_id);
 // console.log("brief_id="+brief_id+"&product_qty="+product_qty+"&subtotal_price="+subtotal_price+"&brief_product_id="+brief_product_id+"&total_price="+total_price);
 	var txtEC="";
	var txtDscnt="";

	if(brief_product_id=="discount"){
		var txtDscnt=0;
	}
	else{		
		var txtDscnt=$("#discount"+brief_id).val();
	}
	if(brief_product_id=='extra_charges'){
		var txtEC=0;
	}
	else{		
		var txtEC=$("#extra_charges"+brief_id).val();
		
	}
 $.ajax({
 	type: "POST",
 	url: "<?php echo base_url();?>front/dashboard/delete_product_list", 
 	dataType: 'html',
 	data: "brief_id="+brief_id+"&brief_product_id="+brief_product_id+"&txtDscnt="+txtDscnt+"&txtEC="+txtEC,
 	success: function(data){
    	//alert(brief_product_id);
    	$("#loader").hide();
    	$('#view_'+brief_id+"_"+brief_product_id).remove();
    	$("#view_product_id_"+brief_id+" option[value="+brief_product_id+"]").prop('disabled', false);
    	//$("#click_"+brief_id).collapse('hide');
			//$("#modal_confirm_product"+brief_product_id).modal('hide');
    	
    	$("#total_price"+brief_id).val(data);
    	data = parseFloat(data);
    	if(typeof $("#discount"+brief_id).val()!=="undefined"){
			var txtDscnt=$("#discount"+brief_id).val();
			data = data-txtDscnt;
		
		}
		if(typeof $("#extra_charges"+brief_id).val()!="undefined"){
			var txtEC=$("#extra_charges"+brief_id).val();
			data = data+parseFloat(txtEC);
		}

		$('#total_price_values'+brief_id).html("Rs."+data);

    	if($("#view_product-list"+brief_id+" tr").length ==1){

    		$("#view_no-result"+brief_id).removeClass('d-none');
		}

    }
  });
}
function update_product(brief_id,brief_product_id,add_discount,add_extra_charges){
	//var product_id=$("#product_id"+brief_product_id).val();
	var product_qty=$("#product_qty"+brief_product_id).val();
	var subtotal_price= $('#subtotal_price_'+brief_product_id).val();
	var total_price=$('#total_price'+brief_id).val(); 
	if(product_qty!=""){
		//alert(total_price);
		var txtEC="";
		var txtDscnt="";

		if(typeof $("#discount"+brief_id).val()!=="undefined"){
		var txtDscnt=$("#discount"+brief_id).val();

		}
		if(typeof $("#extra_charges"+brief_id).val()!="undefined"){
		var txtEC=$("#extra_charges"+brief_id).val();

		}

		$("#loader").show();
		$("#div_product_quty"+brief_product_id).html(product_qty);

		$("#div_product_quty"+brief_product_id).removeClass("d-none");
		$("#product_qty"+brief_product_id).addClass("d-none");
		$("#edit_product"+brief_product_id).removeClass("d-none");
		$("#delete_product"+brief_product_id).removeClass("d-none");
		$("#update_product"+brief_product_id).addClass("d-none");
		$("#cancel_product"+brief_product_id).addClass("d-none"); 
		//console.log("brief_id="+brief_id+"&product_qty="+product_qty+"&subtotal_price="+subtotal_price+"&brief_product_id="+brief_product_id+"&total_price="+total_price);
		$.ajax({
			type: "POST",
			url: "<?php echo base_url();?>front/dashboard/update_product_list", 
			dataType: 'html',
			data: "brief_id="+brief_id+"&product_qty="+product_qty+"&subtotal_price="+subtotal_price+"&brief_product_id="+brief_product_id+"&total_price="+total_price+"&txtDscnt="+txtDscnt+"&txtEC="+txtEC,
			success: function(data){
				//alert(data);
				$("#loader").hide();
				//$('#subtotal_price_'+brief_product_id).html("Rs."+data);

			}
		});
	}
	else{
	$("#errmsg_product_qty_"+brief_product_id).html("Enter the Value").show().fadeOut(1500);
	}
}
function changeBriefStatus(brief_id,brief_status){
  //var brief_status=$("#brief_status"+brief_id).val();

  if(brief_status=='1')
  {
    //alert(brief_status);
    //$('.collapse').collapse('show');
    $("#div_brief_due_date"+brief_id).removeClass('d-none');

    $("#brief_review_approve_"+brief_id).removeClass('d-none');
    $("#brief_review_reject_"+brief_id).addClass('d-none');
    $(".btn_yes"+brief_id).removeClass('btn-outline-wc');
    $(".btn_yes"+brief_id).addClass('btn-wc');
    $(".btn_no"+brief_id).addClass('btn-outline-wc');
    $(".btn_no"+brief_id).removeClass('btn-wc');

    // $("#brief_approve_reject_"+brief_id).addClass('d-none');
   // $("#brief_approve_reject_"+brief_id).attr('style', 'display:none!important');
    //$(".brief_review_reject_"+brief_id).addClass('d-none');
    //$(".brief_download_"+brief_id).removeClass('d-none');
  }
  else if(brief_status=='4')
  {
  	$(".btn_no"+brief_id).removeClass('btn-outline-wc');
  	$(".btn_no"+brief_id).addClass('btn-wc');
  	$(".btn_yes"+brief_id).addClass('btn-outline-wc');
  	$(".btn_yes"+brief_id).removeClass('btn-wc');
  	$("#rejected_reasons"+brief_id).val('');
  	$("#div_brief_due_date"+brief_id).addClass('d-none');
  	$("#brief_review_approve_"+brief_id).addClass('d-none');
  	$("#brief_review_reject_"+brief_id).removeClass('d-none');
		//$("#brief_approve_reject_"+brief_id).addClass('d-none');
		//	$("#brief_approve_reject_"+brief_id).attr('style', 'display:none!important');
		$("#brief_review_reject_"+brief_id).removeClass('d-none');
		$( "#tr_"+brief_id ).removeClass( "border-bottom" );
	}
else
{

	$("#brief_review_approve_"+brief_id).addClass('d-none');
	$("#brief_review_reject_"+brief_id).addClass('d-none');

}
}
function submitCancel(brief_id)
{
	$("#click_"+brief_id).collapse('hide');
}

function submitCorfirm(brief_id,brief_status,add_discount,add_extra_charges)
{


	// var brief_status=$("#brief_status"+brief_id).val();
	//alert("--"+brief_status);
	if(brief_status=='')
	{
		// alert("Please select Status");
		$('#modal_msg').modal('show');
		$('#txtMsg').html('Please select Status');
	}
	else
	{
		if(brief_status=='1')
		{
			$('#click_'+brief_id).modal('hide');
			var brief_due_date=$("#brief_due_date"+brief_id).val();
			//alert(brief_due_date);
			//var brief_due_date = $("#brief_due_date"+brief_id).attr('placeholder'); 

			//alert(brief_due_date);
			var rejected_reasons='';
	
			var product_id=$("input[name='product_id"+brief_id+"[]']").map(function () {
				return this.value;

			});
			product_id=product_id.get();

			var total_qty = $("input[id='total_qty"+brief_id+"']")
			.map(function(){return this.value;}).get();
			var subtotal_price = $("input[id='subtotal_price"+brief_id+"']")
			.map(function(){return this.value;}).get();
			var total_price = $("#total_price"+brief_id+"").val();
			//alert(product_id);
			var txtEC="";
			var txtDscnt="";
			if(typeof $("#discount"+brief_id).val()!=="undefined"){
				var txtDscnt=$("#discount"+brief_id).val();
			}
			if(typeof $("#extra_charges"+brief_id).val()!="undefined"){
				var txtEC=$("#extra_charges"+brief_id).val();
			}
			$("#loader").show();


		}
		else if(brief_status=='4')
		{
			var brief_due_date='';
			var rejected_reasons=$("#rejected_reasons"+brief_id).val();
			var product_id = '';
			var total_qty = '';
			var subtotal_price = '';
			var total_price = '';
			var txtDscnt="";
			var txtEC="";
			if(rejected_reasons=='')
			{
			  // alert("Please Give Reason");
			  $('#modal_msg').modal('show');
			  $('#txtMsg').html('Please Give Reason');
			  return false;
			}
			else
			{
				$("#loader").show();
			}

		}
		else if(brief_status=='0')
		{
			var brief_due_date=$("#brief_due_date"+brief_id).val();
			//alert(brief_due_date);
			//var brief_due_date = $("#brief_due_date"+brief_id).attr('placeholder'); 

			//alert(brief_due_date);
			var rejected_reasons='';
			var product_id = '';
			var total_qty = '';
			var subtotal_price = '';
			var total_price = '';
			var txtDscnt="";
			var txtEC="";
			$("#loader").show();
		}
	
		/*$.ajax({
			method:'POST',
			url: "<?php echo base_url();?>front/uploadbrief/updateBriefStatus",
			data:"&brief_id="+brief_id+"&brief_status="+brief_status+"&brief_due_date="+brief_due_date+"&rejected_reasons="+rejected_reasons+"&total_price="+total_price+"&product_id="+product_id+"&total_qty="+total_qty+"&subtotal_price="+subtotal_price,
			success: function(result){
				//alert(result);
				if(result) {

					$("#loader").hide();
					//$("#click_"+brief_id).collapse('hide');
					//$("#card_"+brief_id).addClass('d-none');
					var lastID="";
					//var lastID = $('.load-more').attr('lastID');
					sortbrief(lastID);
				}
			}
		});*/

		//alert(brief_status);

		$.ajax({
			method:'POST',
			url: "<?php echo base_url();?>front/uploadbrief/updateBriefStatus",
			data:"&brief_id="+brief_id+"&brief_status="+brief_status+"&brief_due_date="+brief_due_date+"&rejected_reasons="+rejected_reasons+"&total_price="+total_price+"&product_id="+product_id+"&total_qty="+total_qty+"&subtotal_price="+subtotal_price+"&txtDscnt="+txtDscnt+"&txtEC="+txtEC,
			cache: true,
			timeout: 2000
			}).done(function( result ) {
					//console.log( "SUCCESS: " + result );
					if(result) {

					$("#loader").hide();
					if(brief_status=='0' && result=="success")
					{
						$('#modal_msg').modal('show');
						$('#txtMsg').html('The Project #P'+brief_id+' delivery date has been updated');
					}
					//$("#click_"+brief_id).collapse('hide');
					//$("#card_"+brief_id).addClass('d-none');
					var lastID="";
					//var lastID = $('.load-more').attr('lastID');
					sortbrief(lastID);
					}
			}).fail(function() {

				//alert("aaaa");
			       console.log( "Request failed");
			        $("#loader").hide();
					//$("#click_"+brief_id).collapse('hide');
					//$("#card_"+brief_id).addClass('d-none');
					var lastID="";
					//var lastID = $('.load-more').attr('lastID');
					sortbrief(lastID);
			});

	}
}
/*$(document).ready(function() {*/

function selectAccount(brand_id_sel)
{
  //alert(brand_id_sel);
  var account_id=$('#account_id').val();
   //alert(account_id);
   $.ajax({
   	method:'POST',
   	url: "<?php echo base_url();?>front/dashboard/getBrandDropDown",
   	data:"&account_id="+account_id+"&brand_id_sel="+brand_id_sel,

   	success: function(result){
      //alert(result);
      if(result) {
      	$("#divBrand").html(result);
      	var lastID = $('.load-more').attr('lastID');
				sortbrief(lastID);

      }
    }
  });
}

function removeFilter(filter_id) {
	//alert(filter_id);
	document.getElementById("bsf"+filter_id).checked = false;
	var lastID = "";
	sortbrief(lastID);
}
function removeBrand(brand_id) {
	//alert(brand_id);
	document.getElementById("brand_"+brand_id).checked = false;
	var lastID = "";
	sortbrief(lastID);
}
function sortbrief(lastID) {
		//alert(lastID);
	if(lastID==""){
		$("#loader").show();
	}
 
  var status_filter = new Array();
  var status_filter_name = "";
  $(".status_filter:checked").each(function() {
  	status_filter.push($(this).val());

  	var filter_id=$(this).val();
  	if(filter_id=="0"){
  		var filt_name="Brief in Review";
  	}if(filter_id=="1"){
  		var filt_name="Work in Progress";
  	}if(filter_id=="2"){
  		var filt_name="Work Completed";
  	}if(filter_id=="3"){
  		var filt_name="Revision Work";
  	}if(filter_id=="4"){
  		var filt_name="Brief Rejected";
  	}if(filter_id=="5"){
  		var filt_name="Feedback Pending";
  	}if(filter_id=="6"){
  		var filt_name="Proofing Pending";
  	}if(filter_id=="7"){
  		var filt_name="Archived";
  	}if(filter_id==""){
  		var filt_name="All Status";
  	}
  	status_filter_name+='<span class="py-1 px-2 mr-2 text-wc border-wc rounded-pill">'+filt_name+'&emsp;<span onclick="removeFilter('+filter_id+');"><i class="fas fa-times" ></i></span></span>';
  });

	//alert(status_filter_name);

	$("#div_sel_filter").html(status_filter_name);
	var brand_id = new Array();
	$(".brand_id:checked").each(function() {
		brand_id.push($(this).val());
		if(brand_id!=""){
  			var brand_filt_name=$('#brand_'+brand_id).attr('brand_name');
  		}  	
  		//alert(brand_filt_name);
  		status_filter_name+='<span class="py-1 px-2 mr-2 text-wc border-wc rounded-pill">'+brand_filt_name+'&emsp;<span onclick="removeBrand('+brand_id+');"><i class="fas fa-times" ></i></span></span>';
	});
$("#div_sel_filter").html(status_filter_name);
	var account_id = new Array();
	$(".account_id:checked").each(function() {
		account_id.push($(this).val());
	});
 
  var search_key = $("#search_key").val();
  

  $.ajax({
  	type: "POST",
  	url: "<?php echo base_url();?>front/dashboard/getbriefsort",
  	 async: false,
  	dataType: 'html',
  	data: 'search_key='+search_key+'&brand_id='+brand_id+'&account_id='+account_id+'&status_filter='+status_filter+'&lastID='+lastID,
  	// beforeSend:function(){
  	// 	alert('beforeSend');
   //     $('.load-more').show();
   //  },
  	success: function(data){

      //alert(data);

      if($.trim(data)!='sessionout')
      {
		
		if(lastID==""){
		$('#tblbrf').html("");
		$('#tblbrf').html(data);

		$("#loader").hide();
		//alert(lastID);
		}
		else{
		$('.load-more').remove();
		$('#tblbrf').append(data);

		}


		getbir(brand_id);
		getbr(brand_id);
		getwip(brand_id);
		getrw(brand_id);
		getpp(brand_id);
		getwc(brand_id);
		getfp(brand_id);
		getfc(brand_id);

	}
	else
      {
      	    window.location = "<?php echo base_url(); ?>login";

      }



      
    }
  });
  return false;
}

function getbir(brand_id) {
	var account_id=$('#account_id').val();
	var search_key = $("#search_key").val();

	$.ajax({
		type: "POST",
		url: "<?php echo base_url();?>front/dashboard/getbir", 
		dataType: 'html',
		data: '&search_key='+search_key+'&account_id='+account_id+'&brand_id='+brand_id,
		success: function(data){
      //alert(data);
      $('#bir').html(data)            
    }
  });
}

function getbr(brand_id) {

	var account_id=$('#account_id').val();
	var search_key = $("#search_key").val();
	$.ajax({
		type: "POST",
		url: "<?php echo base_url();?>front/dashboard/getbr", 
		dataType: 'html',
		data: '&search_key='+search_key+'&account_id='+account_id+'&brand_id='+brand_id,
		success: function(data){
      //alert(data);
      $('#br').html(data)         
    }
  });
}

function getwip(brand_id) {

	var account_id=$('#account_id').val();
	var search_key = $("#search_key").val();
	$.ajax({
		type: "POST",
		url: "<?php echo base_url();?>front/dashboard/getwip", 
		dataType: 'html',
		data: '&search_key='+search_key+'&account_id='+account_id+'&brand_id='+brand_id,
		success: function(data){
	    //alert(data);
	    $('#wip').html(data)            
	  }
	});
}

function getrw(brand_id) {

	var account_id=$('#account_id').val();
	var search_key = $("#search_key").val();
	$.ajax({
		type: "POST",
		url: "<?php echo base_url();?>front/dashboard/getrw", 
		dataType: 'html',
		data: '&search_key='+search_key+'&account_id='+account_id+'&brand_id='+brand_id,
		success: function(data){
	    //alert(data);
	    $('#rw').html(data)
	  }
	});
}

function getpp(brand_id)
{

	var account_id=$('#account_id').val();
	var search_key = $("#search_key").val();
	$.ajax({
		type: "POST",
		url: "<?php echo base_url();?>front/dashboard/getpp", 
		dataType: 'html',
		data: '&search_key='+search_key+'&account_id='+account_id+'&brand_id='+brand_id,
		success: function(data){
		    //alert(data);
		    $('#pp').html(data)         
		}
	});
}

function getwc(brand_id)
{

	var account_id=$('#account_id').val();
	var search_key = $("#search_key").val();
	$.ajax({
		type: "POST",
		url: "<?php echo base_url();?>front/dashboard/getwc", 
		dataType: 'html',
		data: '&search_key='+search_key+'&account_id='+account_id+'&brand_id='+brand_id,
		success: function(data){
		    //alert(data);
		    $('#wc').html(data)         
		}
	});
}

function getfp(brand_id)
{

	var account_id=$('#account_id').val();
	var search_key = $("#search_key").val();
	$.ajax({
		type: "POST",
		url: "<?php echo base_url();?>front/dashboard/getfp", 
		dataType: 'html',
		data: '&search_key='+search_key+'&account_id='+account_id+'&brand_id='+brand_id,
		success: function(data){
		  //alert(data);
		  $('#fp').html(data)         
		}
	});
}


function getfc(brand_id)
{

	var account_id=$('#account_id').val();
	var search_key = $("#search_key").val();
	$.ajax({
		type: "POST",
		url: "<?php echo base_url();?>front/dashboard/getfc", 
		dataType: 'html',
		data: '&search_key='+search_key+'&account_id='+account_id+'&brand_id='+brand_id,
		success: function(data){
		  //alert(data);
		  $('#fc').html(data)         
		}
	});
}

      var functionUpload = function(e,no,brief_id) {


        //alert("goko");
        

        //alert(brief_id);

        var responseText = $.ajax({
        	type:'POST',
        	url:"<?php echo base_url();?>front/dashboard/getTempFolder",
        //data: {'skufolderfile_nm':skufolderfile_nm},
        async: false
      }).responseText;

        $(this).attr("folder",responseText);


        var folder= $(this).attr('folder') ;

        //alert(folder);

        var files = e.target.files,
        filesLength = files.length;

        console.log(files);

        for (var i = 0; i < filesLength; i++) {
        	var file = files[i];



        	console.log(file);
        	console.log("bbbbbbbbbb");



        	if(file != undefined){
        		formData= new FormData();

        //var ext = $("#files"+no).val().split('.',2).pop().toLowerCase();

        //alert(ext);


        //if ($.inArray(ext, ['doc','docx']) == 1)
        /* if (ext=='doc' || ext=='docx')
        {*/
        	formData.append("doc_file", file);
        	formData.append("brief_id", brief_id);
        	formData.append("folder", folder);
        	formData.append("check", i);



        //alert("aaaa");
        $.ajax({
        	url: "<?php echo base_url();?>front/dashboard/upload_file",
        	type: "POST",
        	data: formData,
        	processData: false,
        	contentType: false,
        	success: function(response){

        //alert("koko");
        //alert(data);

        var responseArr=response.split("@@@@@@@@@@@");
        var doc_file_name=$.trim(responseArr[0]);
        var check=$.trim(responseArr[1]);

        


        var fileName =$('#file_nm_hid_'+brief_id).val();
        //alert(fileName);

        if(fileName=='')
        {
        	var fileNameCurrent=",,,"+doc_file_name+",,,";
        }
        else
        {
        	if(fileName.indexOf(",,,"+doc_file_name+",,,")==-1)
        	{
        		var fileNameCurrent=fileName+doc_file_name+",,,";
        	}
        	else
        	{
        		var fileNameCurrent=fileName;
        	}

        }

        $('#file_nm_hid_'+brief_id).val(fileNameCurrent);


        if(check==parseInt(filesLength)-1)
        {
        	functionMove(e,no,brief_id);
        }

        



      }
    });
        /*}else{

        alert("Please Upload Doc");
        return false;
        }
        */

      }
      else{
        //alert('Input something!');
      }



        //alert(i+"-----"+filesLength);

        

        
      }



    //functionMove(e,no,brief_id);
    //return deferred;
  }

  var functionMove = function(e,no,brief_id) {


    // alert("koko");


    var file_nm =$('#file_nm_hid_'+brief_id).val();

    //alert('&brief_id='+brief_id+'&temp_folder='+folder+'&file_nm='+file_nm);
    $("#loader").show();

/*    $.ajax({
    	method:"post",
    	url: "<?php echo base_url();?>front/dashboard/move_upload_file",
    	data:'&brief_id='+brief_id+'&temp_folder='+folder+'&file_nm='+file_nm,
    	success: function(result){

				//alert(result);
				$("#loader").hide();
				var lastID = "";
				sortbrief(lastID);

				$('.re_upload_doc').on('change', function (e) {

				var no = $(this).attr('alt') ;
				var brief_id= $(this).attr('val') ;
				functionUpload(e,no,brief_id);

				
				});

        }
        });*/


        $.ajax({
			method:"post",
			url: "<?php echo base_url();?>front/dashboard/move_upload_file",
			data:'&brief_id='+brief_id+'&temp_folder='+folder+'&file_nm='+file_nm,
			cache: true,
			timeout: 2000
			}).done(function( result ) {
			console.log( "SUCCESS: " + result );
			//alert(result);
				$("#loader").hide();
				var lastID = "";
				sortbrief(lastID);

				$('.re_upload_doc').on('change', function (e) {

				var no = $(this).attr('alt') ;
				var brief_id= $(this).attr('val') ;
				functionUpload(e,no,brief_id);

				/*functionUpload(e,no,brief_id).then(functionMove(e,no,brief_id));*/
				});
			}).fail(function() {
			console.log( "Request failed");
			//alert(result);
				$("#loader").hide();
				var lastID = "";
				sortbrief(lastID);

				$('.re_upload_doc').on('change', function (e) {

				var no = $(this).attr('alt') ;
				var brief_id= $(this).attr('val') ;
				functionUpload(e,no,brief_id);

				/*functionUpload(e,no,brief_id).then(functionMove(e,no,brief_id));*/
				});
			});






  }




  $('.re_upload_doc').on('change', function (e) {

  	var no = $(this).attr('alt') ;
  	var brief_id= $(this).attr('val') ;
  	functionUpload(e,no,brief_id);

  	/*functionUpload(e,no,brief_id).then(functionMove(e,no,brief_id));*/
  });



  $(".re_upload_doc_old").on("change", function(e) {

 //alert("goko");
 var no = $(this).attr('alt') ;
 var brief_id= $(this).attr('val') ;

//alert(brief_id);

var responseText = $.ajax({
	type:'POST',
	url:"<?php echo base_url();?>front/dashboard/getTempFolder",
        //data: {'skufolderfile_nm':skufolderfile_nm},
        async: false
      }).responseText;

$(this).attr("folder",responseText);


var folder= $(this).attr('folder') ;

 //alert(folder);

 var files = e.target.files,
 filesLength = files.length;

 console.log(files);
 for (var i = 0; i < filesLength; i++) {
 	var f = files[i]
 	var fileReader = new FileReader();
 	fileReader.onload = (function(e) {
 		var file = e.target;

 		console.log(file);
 		console.log("aaaaaaaaaaaaaa");




//alert(no);
var input = document.getElementById("files"+no);
//file = input.files[0];

for (var k = 0; k < filesLength; k++) {
	file = input.files[k];

	console.log(file);
	console.log("bbbbbbbbbb");



	if(file != undefined){
		formData= new FormData();

		var ext = $("#files"+no).val().split('.',2).pop().toLowerCase();

    //alert(ext);


    //if ($.inArray(ext, ['doc','docx']) == 1)
   /* if (ext=='doc' || ext=='docx')
   {*/
   	formData.append("doc_file", file);
   	formData.append("brief_id", brief_id);
   	formData.append("folder", folder);



  //alert("aaaa");
  $.ajax({
  	url: "<?php echo base_url();?>front/dashboard/upload_file",
  	type: "POST",
  	data: formData,
  	processData: false,
  	contentType: false,
  	success: function(data){

     //alert("koko");
     //alert(data);

     var doc_file_name=$.trim(data);


     var fileName =$('#file_nm_hid_'+brief_id).val();

     if(fileName=='')
     {
     	var fileNameCurrent=",,,"+doc_file_name+",,,";
     }
     else
     {
     	if(fileName.indexOf(",,,"+doc_file_name+",,,")==-1)
     	{
     		var fileNameCurrent=fileName+doc_file_name+",,,";
     	}
     	else
     	{
     		var fileNameCurrent=fileName;
     	}

     }

     $('#file_nm_hid_'+brief_id).val(fileNameCurrent);



   }
 });
/*}else{

    alert("Please Upload Doc");
    return false;
}
*/

}
else{
// alert('Input something!');
$('#modal_msg').modal('show');
$('#txtMsg').html('Input something!');
}


}



});
 	fileReader.readAsDataURL(f);
 }
});

function updateuser_brief(emp_id,brief_id,client_id)
{
	$("#loader").show();

	/*alert('brief_id='+brief_id+'&emp_id='+emp_id+'&client_id='+client_id);
	$.ajax({
		type: "POST",
		url: "<?php echo base_url();?>front/dashboard/updateuser_brief", 
		dataType: 'html',
		data: 'brief_id='+brief_id+'&emp_id='+emp_id+'&client_id='+client_id,
		success: function(data){
			alert(data);
			$("#loader").hide();
			var lastID="";
			//sortbrief(lastID);
			var key=data;
			//sortemp(key,brief_id)
			sortbrief(lastID);
		}
	});	*/

	//alert('brief_id='+brief_id+'&emp_id='+emp_id+'&client_id='+client_id);

	$.ajax({
		    method:'POST',
			url: "<?php echo base_url();?>front/dashboard/updateuser_brief", 
			data:'brief_id='+brief_id+'&emp_id='+emp_id+'&client_id='+client_id,
			cache: true,
			timeout: 2000
			}).done(function( data ) {

					//console.log( "SUCCESS: " + result );
					$("#loader").hide();
					var lastID="";
					//sortbrief(lastID);
					//var key=data;
					//sortemp(key,brief_id)
					sortbrief(lastID);
			}).fail(function() {

				//alert("aaaa");
			       console.log( "Request failed");
					$("#loader").hide();
					var lastID="";
					//sortbrief(lastID);
					//var key=data;
					//sortemp(key,brief_id)
					sortbrief(lastID);
			});







}

function sortemp(key,brief_id)
{
	$.ajax({
		type: "POST",
		url: "<?php echo base_url();?>front/dashboard/sortemp", 
		dataType: 'html',
		data: 'key='+key+'&brief_id='+brief_id,
		success: function(data){
			$('#emp_div_'+brief_id).html(data);
		}
	});	
}

function submitlog(brief_id)
{
	var logdate=$('#log_date_'+brief_id).val();
	var loghours=$('#log_hrs_'+brief_id).val();
	var logmins=$('#log_mins_'+brief_id).val();
	if ((logdate=='') || (loghours=='') || (logmins==''))
	{
		//alert();
		$('#modal_msg').modal('show');
				$('#txtMsg').html("please fill all the fields");
	}
	else{
	//alert(logdate);
	//alert(loghours);
	//alert(logmins);
	$.ajax({
		type: "POST",
		url: "<?php echo base_url();?>front/dashboard/storelog", 
		dataType: 'html',
		data: 'logdate='+logdate+'&brief_id='+brief_id+'&loghours='+loghours+'&logmins='+logmins,
		success: function(data){
			//$('#logdiv_'+brief_id).html(data);
			//alert(data);
			console.log(data);
			// $('#click_'+brief_id).trigger( "click" );
			//$('#addlog_'+brief_id).collapse('hide');
			//$('#savelog_'+brief_id).collapse('toggle');
			$('#logdiv_'+brief_id).addClass('d-none');
			$('#savelogdiv_'+brief_id).removeClass('d-none');
			listviewlogs(brief_id)

		}
	});	
	}
	
}
function restaddlog(brief_id)
{
    $('#savelogdiv_'+brief_id).addClass('d-none');
			$('#logdiv_'+brief_id).removeClass('d-none');
$('#log_hrs_'+brief_id).prop('selectedIndex',0);
$('#log_mins_'+brief_id).prop('selectedIndex',0);

}




function listviewlogs(brief_id)
{
	var brief_id=$('#brief_list_'+brief_id).val();
	var logdate=$('#view_log_date_'+brief_id).val();
	//alert(logdate);
	$.ajax({
		type: "POST",
		url: "<?php echo base_url();?>front/dashboard/listlog", 
		dataType: 'html',
		data: 'logdate='+logdate+'&brief_id='+brief_id,
		success: function(data){
			$('#listsortview_'+brief_id).html(data);
			//alert(data);

		}
	});	
}

function removelog(log_id,brief_id)
{
  $.ajax({
		type: "POST",
		url: "<?php echo base_url();?>front/dashboard/removelog", 
		dataType: 'html',
		data: 'log_id='+log_id+'&brief_id='+brief_id,
		success: function(data){
			//$('#listsortview_'+brief_id).html(data);
			//alert(data);
			listviewlogs(brief_id)
		}
	});	   

   
}

function uploadFile(brief_id){

	//alert(brief_id);
	
		//var allfiles = _("#upld_img"+brief_id).files;
		//console.log(allfiles);

   var allfiles = document.getElementById('upld_img'+brief_id).files;
   console.log(allfiles);
//alert(allfiles);
			//var image_id =img_id;
			
    var img_id=brief_id;

		for(var i=0; i<allfiles.length;i++)
		{
          //alert(i);
			//var file = _("upld_img").files[0];
			var file = allfiles[i];
			console.log(file);


			

			var file1=file.name;
			var fileExtension = file1.split(".");
			var fileType=file.type;
			//alert(fileType);
			//if(fileType === "application/x-zip-compressed")
			if(file != undefined && (file.type=='image/png' || file.type=='image/jpg'   || file.type=='image/jpeg' || file.type=='video/webm' || file.type=='video/mp4' || file.type=='video/3gpp' || file.type=='video/x-sgi-movie' || file.type=='video/x-msvideo' || file.type=='video/mpeg' || file.type=='video/x-ms-wmv' )    )
			{
			//$("#loader").show();	
			$("#circular_progress").removeClass('d-none');
			$('#upmgs').removeClass('d-none'); 
			var formdata = new FormData();

			/*formdata.append("upld_img", file);
			formdata.append("temp_folder", temp_folder);
			formData.append("brief_id", brief_id);*/

			formdata.append("image", file);
			formdata.append("brief_id", brief_id);
			formdata.append("image_num", i);
			formdata.append("image_tot_num", allfiles.length);
			


			var ajax = new XMLHttpRequest();
			ajax.upload.addEventListener("progress", progressHandler, false);
			ajax.addEventListener("load", completeHandler1, false);
			ajax.addEventListener("error", errorHandler, false);
			ajax.addEventListener("abort", abortHandler, false);
			ajax.open("POST", "<?php echo base_url();?>front/uploadimages/upload_files_image");
            /////////////////////////////////new code//////////////////////////////////////////////////
			var tot_len=parseInt(allfiles.length)-1;
			//alert(image_id+"----------"+i+"=="+tot_len);
      //       if(i==tot_len)
      //       {
      //       	if(file.type=='image/png' || file.type=='image/jpg'   || file.type=='image/jpeg')
      //       	{
						// ajax.timeout = 5000;
						// ajax.ontimeout = function () { 
						// //alert("Timed out!!!"); 
						// $("#loader").hide();
						// window.location.href="<?php echo base_url();?>view-files/<?php echo $brief_id; ?>";
						// }
      //       	}
				

      //       }
            //////////////////////////////////////////////////////////////////////////////////
			ajax.send(formdata);
			$("#imageDanger"+img_id).addClass("d-none");
			//_("imagetxtDanger"+img_id).innerHTML = "";
			} 
			else {
			//$("#loader").hide();
			//alert('its not a zip file');
			$("#imageDanger"+img_id).removeClass("d-none");
			//_("imagetxtDanger"+img_id).innerHTML = "Please Upload Image/Video File Only";
			}

		}
		
	}


	function progressHandler(event){
		var percent = (event.loaded / event.total) * 100;
		// console.log(percent);
		// $("#file_pc").attr("aria-valuenow", Math.round(percent));
		// $("#file_pc").css("width", Math.round(percent) + "%");
		$("#circular_progress_data").attr("data-value", Math.round(percent));
		$("#circular_progress_value").html(Math.round(percent) + "%");
		circular_progress();
	}

	
		function completeHandler1(event){
			
			var res = event.target.responseText;
			//alert(res);
			console.log(res);
			var arr =res.split("@@@@@");
			var msg=$.trim(arr[0]);
			var cur=$.trim(arr[1]);
			var tot=$.trim(arr[2]);
			var checkvalid=$.trim(arr[3]);
			var img_id=$.trim(arr[4]);

			//alert(checkvalid);

			if(checkvalid=='notvalid' && tot=='1')
			{
			$("#loader").hide();
			$("#imageDanger"+img_id).removeClass("d-none");
			//_("imagetxtDanger"+img_id).innerHTML = "Please Upload Proper Image/Video File Only";
			$("#circular_progress").addClass('d-none');
					

			}
			else if(checkvalid=='notvalid')
			{

				    $("#loader").hide();
					$('#modal_msg').modal('show');
					$('#txtMsg').html(msg);


					setTimeout(function(){ 


					$('#modal_msg').modal('hide');
					$('#txtMsg').html("");
					$("#loader").show();

					if(cur==tot)
					{

					$("#loader").hide();
					window.location.href="<?php echo base_url();?>dashboard";
					//window.location.href="<?php echo base_url();?>view-files/<?php echo $brief_id; ?>";
					//window.location.href = window.location.href

					}

						

					}, 2000);

					



					

			}
			else
			{
				if(cur==tot)
				{

				$("#loader").hide();
			window.location.href="<?php echo base_url();?>dashboard";
				//window.location.href="<?php echo base_url();?>view-files/<?php echo $brief_id; ?>";
				//window.location.href = window.location.href

				}

			}
			



	}


function sortbrand(keytosearch,user_type_id,client_id)
{
	//alert(keytosearch);
	//console.log(keytosearch);
	var lastID = 0;
	var brand_id = new Array();
	$(".brand_id:checked").each(function() {
		brand_id.push($(this).val());
		if(brand_id!=""){
  			//var brand_filt_name=$('#brand_'+brand_id).attr('brand_name');

  		}  	
  	});

  	var brandid = brand_id.toString();

	//alert('keytosearch='+keytosearch+'&user_type_id='+user_type_id+'&client_id='+client_id+'&lastID='+lastID+'&brandid='+brandid);
	$.ajax({
		type: "POST",
		url: "<?php echo base_url();?>front/dashboard/sortbrand", 
		dataType: 'html',
		data: 'keytosearch='+keytosearch+'&user_type_id='+user_type_id+'&client_id='+client_id+'&lastID='+lastID+'&brandid='+brandid,
		success: function(data){
			//alert(data);
			$('#brand_list').html(data);
			//sortbrief(lastID);
		}
	});	
}





	function completeHandler(event){
		$("#zipSuccess").removeClass("d-none");
		$('#endocimg').removeClass('d-none');
		//_("txtSuccess").innerHTML = event.target.responseText;
	}
	function errorHandler(event){
		$("#docDanger").removeClass("d-none");
		//_("txtDanger").innerHTML = "Upload Failed";
	}
	function abortHandler(event){
		$("#docDanger").removeClass("d-none");
		//_("txtDanger").innerHTML = "Upload Aborted";
	}

</script>