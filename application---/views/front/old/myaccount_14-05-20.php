<?php
session_start();
// error_reporting(0);

$data['page_title'] = "my account";

$this->load->view('front/includes/header',$data);
$this->load->view('front/includes/topnav');
?>

<div class="container-fluid">
	<div class="row">

		<!-- Main Body -->
		<div class="col-12 col-sm-12 col-md-12">
			<main class="p-3">

				<div class="row">
					<div class="col-12 col-sm-12 col-md-12">
						<div class="p-3">
							<p class="text-wc lead" onclick="goDashboard();"  style="cursor: pointer;"><i class="fas fa-arrow-alt-circle-left"></i>&nbsp;Back to Dashboard</p>
						</div>
					</div>
				</div>

				<div class="row">
					<div class="col-12 col-sm-12 col-md-12">
						<div class="p-3 text-center text-wc">
							<h2>My Account</h2>
						</div>
					</div>
				</div>

				<hr class="text-center w-50 ">

				<div class="row">
					<div class="col-12 col-sm-12 col-md-3">&nbsp;</div>
					<div class="col-12 col-sm-12 col-md-6">
						<div class="p-4">
							<form>
								<div class="form-row">
									<div class="form-group col-5">
										<label class="lead" for="txtusr">User Name : </label>
									</div>
									<div class="form-group col-7">
										<input class="form-control" type="text" name="txtusr" id="txtusr" value="<?php echo $user_details['0']['user_name']; ?>">
									</div>
								</div>

								<div class="form-row">
									<div class="form-group col-5">
										<label class="lead" for="txtrole">User Role : </label>
									</div>
									<div class="form-group col-7">
										<input class="form-control" type="text" name="txtrole" id="txtrole" value="">
									</div>
								</div>

								<div class="form-row">
									<div class="form-group col-5">
										<label class="lead" for="txtemail">User Email : </label>
									</div>
									<div class="form-group col-7">
										<input class="form-control" type="email" name="txtemail" id="txtemail" value="<?php echo $user_details['0']['user_email']; ?>">
									</div>
								</div>

								<div class="form-row">
									<div class="form-group col-5">
										<label class="lead" for="txtpwd">Password : </label>
									</div>
									<div class="form-group col-7">
										<input class="form-control" type="password" name="txtpwd" id="txtpwd" value="<?php echo $user_details['0']['user_password']; ?>">
									</div>
								</div>

								<div class="form-row">
									<div class="form-group col-5">
										<label class="lead" for="txtcpwd">Confirm Password : </label>
									</div>
									<div class="form-group col-7">
										<input class="form-control" type="password" name="txtcpwd" id="txtcpwd" value="<?php echo $user_details['0']['user_password']; ?>">
									</div>
								</div>

								<div class="form-row">
									<div class="form-group col-5">
										<label class="lead" for="txtmbl">User Mobile : </label>
									</div>
									<div class="form-group col-7">
										<input class="form-control" type="tel" name="txtmbl" id="txtmbl" pattern="[0-9]{5}-[0-9]{5}" value="<?php echo $user_details['0']['user_mobile']; ?>">
									</div>
								</div>

								<div class="form-row">
									<div class="form-group col-5">
										<label class="lead" for="txtadrs">User Address : </label>
									</div>
									<div class="form-group col-7">
										<textarea class="form-control" rows="3" name="txtadrs" id="txtadrs"><?php echo $user_details['0']['user_address']; ?></textarea>
									</div>
								</div>

								<div class="d-flex justify-content-center">
									<button type="button" class="btn btn-outline-wc m-1">&emsp;Save&emsp;</button>
									<button type="button" class="btn btn-outline-secondary m-1">&emsp;Reset&emsp;</button>
								</div>
							</form>
						</div>
					</div>
					<div class="col-12 col-sm-12 col-md-3">&nbsp;</div>
				</div>
			</main>
		</div>

	</div>
</div>

<?php $this->load->view('front/includes/footer'); ?>