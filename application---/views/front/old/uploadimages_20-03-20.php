<?php
session_start();
error_reporting(0);
if ($this->session->userdata('front_logged_in')) {
$session_data = $this->session->userdata('front_logged_in');
$user_id = $session_data['user_id'];
$user_name = $session_data['user_name'];
$_SESSION['user_id']=$user_id;
$_SESSION['user_name']=$user_name;
//$type1=$this->session->userdata('user_type_id');
 $type1 = $session_data['user_type_id'];
 $brand_access=$session_data['brand_access'];
	if($type1==3)
	{
		$job_title="Project Manager";
	}
	else
	{
		$job_title="Branch Manager";
	}
}
else
{
$user_id = '';
$user_name = ''; 
$_SESSION['user_id']=$user_id;
$_SESSION['user_name']=$user_name;
}
?>

<?php $data['page_title'] = "upload image";

$this->load->view('front/includes/header',$data);
$this->load->view('front/includes/topnav');
 ?>
<div class="container-fluid">
	<div class="row">

		<!-- Main Body -->
		<div class="col-12 col-sm-12 col-md-12">
			<main class="p-3">

				<div class="row">
					<div class="col-12 col-sm-12 col-md-12">
						<div class="p-3">
							<p class="text-wc lead"><i class="fas fa-arrow-alt-circle-left"></i><a href="dashboard">&nbsp;Back to Dashboard</a></p>
						</div>
					</div>
				</div>

				<div class="row">
					<div class="col-12 col-sm-12 col-md-12">
						<div class="p-3 text-wc">
							<h3 class="h3 text-uppercase">
								Bio Oil Banners For March (ID: 2) - Upload - Project Manager
							</h3>
						</div>
					</div>
				</div>

				<div class="row">
					<div class="col-12 col-sm-12 col-md-12">
						<div class="p-2">

							<div class="row">
								<div class="col-12 col-sm-12 col-md-3">
									<div class="text-center">V1</div>
								</div>

								<div class="col-12 col-sm-12 col-md-3">
									<div class="text-center">V2</div>
								</div>

								<div class="col-12 col-sm-12 col-md-3">
									<div class="text-center">V3</div>
								</div>

								<div class="col-12 col-sm-12 col-md-3">
									<div class="text-center">V4</div>
								</div>
							</div>
						</div>
					</div>
				</div>

				<hr class="border-wc">

				<div class="row">
					<div class="col-12 col-sm-12 col-md-12">
						<div class="p-4">

							<div class="row">
								<div class="col-12 col-sm-12 col-md-3 p-2">
									<div class="mb-3">
										<div class="bg-light" style="min-height: 20rem;">
											<form action="" method="post" enctype="multipart/form-data" id="upldForm1" name="upldForm1">
												<div class="custom-file">
													<input type="file" class="custom-file-input customfileinput" id="upld_img1" name="upld_img1" onchange="uploadFile()">
													<label class="custom-file-label customfilelabel text-center" for="upld_img1"><i class="fas fa-upload fa-3x"></i><br><br>Drop the file to upload or <i><u>browse</u></i>.</label>
												</div>
											</form>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>

				<div class="row">
					<div class="col-12 col-sm-12 col-md-12">
						<div class="p-4">

							<div class="row">
								<div class="col-12 col-sm-12 col-md-3 p-2">
									<div class="card">
										<img src="https://placeimg.com/1366/1024/any" class="card-img-top" alt="...">
										<div class="card-body">
											<div class="d-flex justify-content-between">
												<span class="lead">Image Name</span>

												<i class="fas fa-circle text-wc fa-2x"></i>
											</div>
										</div>
									</div>
								</div>

								<div class="col-12 col-sm-12 col-md-3 p-2">
									<div class="mb-3">
										<div class="bg-light" style="min-height: 20rem;">
											<form action="" method="post" enctype="multipart/form-data" id="upldForm2" name="upldForm2">
												<div class="custom-file">
													<input type="file" class="custom-file-input customfileinput" id="upld_img2" name="upld_img2" onchange="uploadFile()">
													<label class="custom-file-label customfilelabel text-center" for="upld_img2"><i class="fas fa-upload fa-3x"></i><br><br>Drop the file to upload or <i><u>browse</u></i>.</label>
												</div>
											</form>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>

				<div class="row">
					<div class="col-12 col-sm-12 col-md-12">
						<div class="p-4">

							<div class="row">
								<div class="col-12 col-sm-12 col-md-3 p-2">
									<div class="card">
										<img src="https://placeimg.com/1366/1024/any" class="card-img-top" alt="...">
										<div class="card-body">
											<div class="d-flex justify-content-between">
												<span class="lead">Image Name</span>

												<i class="fas fa-circle text-danger fa-2x"></i>
											</div>
										</div>
									</div>
								</div>

								<div class="col-12 col-sm-12 col-md-3 p-2">
									<div class="card">
										<img src="https://placeimg.com/1366/1024/any" class="card-img-top" alt="...">
										<div class="card-body">
											<div class="d-flex justify-content-between">
												<span class="lead">Image Name</span>

												<i class="fas fa-circle text-danger fa-2x"></i>
											</div>
										</div>
									</div>
								</div>

								<div class="col-12 col-sm-12 col-md-3 p-2">
									<div class="mb-3">
										<div class="bg-light" style="min-height: 20rem;">
											<form action="" method="post" enctype="multipart/form-data" id="upldForm3" name="upldForm3">
												<div class="custom-file">
													<input type="file" class="custom-file-input customfileinput" id="upld_img3" name="upld_img3" onchange="uploadFile()">
													<label class="custom-file-label customfilelabel text-center" for="upld_img3"><i class="fas fa-upload fa-3x"></i><br><br>Drop the file to upload or <i><u>browse</u></i>.</label>
												</div>
											</form>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</main>
		</div>

	</div>
</div>

<script>
function sortbrief() {
$(document).ready(function() {
        var myCheckboxes = new Array();
        $("input:checked").each(function() {
           myCheckboxes.push($(this).val());
        });
		var brand_id = $("#brand_id").val()
        $.ajax({
            type: "POST",
            url: "<?php echo base_url();?>front/dashboard/getbriefsort", 
            dataType: 'html',
            data: 'brand_id='+brand_id+'&myCheckboxes='+myCheckboxes,
            success: function(data){
                $('#myResponse').html(data)
               getbir(brand_id);
			   getbr(brand_id);
			   getwip(brand_id);
			   getrw(brand_id);
			   getpp(brand_id);
			   getwc(brand_id);
			   getfp(brand_id);
            }
        });
		
        return false;
});
}
function getbir(brand_id)
{

	$.ajax({
            type: "POST",
            url: "<?php echo base_url();?>front/dashboard/getbir", 
            dataType: 'html',
            data: 'brand_id='+brand_id,
            success: function(data){
				//alert(data);
                $('#bir').html(data)			
            }
        });
}


function getbr(brand_id)
{
	$.ajax({
            type: "POST",
            url: "<?php echo base_url();?>front/dashboard/getbr", 
            dataType: 'html',
            data: 'brand_id='+brand_id,
            success: function(data){
				//alert(data);
                $('#br').html(data)			
            }
        });
}

function getwip(brand_id)
{
	$.ajax({
            type: "POST",
            url: "<?php echo base_url();?>front/dashboard/getwip", 
            dataType: 'html',
            data: 'brand_id='+brand_id,
            success: function(data){
				//alert(data);
                $('#wip').html(data)			
            }
        });
}


function getwc(brand_id)
{
	$.ajax({
            type: "POST",
            url: "<?php echo base_url();?>front/dashboard/getwc", 
            dataType: 'html',
            data: 'brand_id='+brand_id,
            success: function(data){
				//alert(data);
                $('#wc').html(data)			
            }
        });
}

function getrw(brand_id)
{
	$.ajax({
            type: "POST",
            url: "<?php echo base_url();?>front/dashboard/getrw", 
            dataType: 'html',
            data: 'brand_id='+brand_id,
            success: function(data){
				//alert(data);
                $('#rw').html(data)			
            }
        });
}

function getpp(brand_id)
{
	$.ajax({
            type: "POST",
            url: "<?php echo base_url();?>front/dashboard/getpp", 
            dataType: 'html',
            data: 'brand_id='+brand_id,
            success: function(data){
				//alert(data);
                $('#pp').html(data)			
            }
        });
}

function getfp(brand_id)
{
	$.ajax({
            type: "POST",
            url: "<?php echo base_url();?>front/dashboard/getfp", 
            dataType: 'html',
            data: 'brand_id='+brand_id,
            success: function(data){
				//alert(data);
                $('#fp').html(data)			
            }
        });
}







function getbranddetails(brand_id)
{
	alert(brand_id);
}




</script>

<?php
$this->load->view('front/includes/footer');
?>
