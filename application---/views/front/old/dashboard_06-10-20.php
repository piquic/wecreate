<?php
session_start();
//error_reporting(0); 

if ($this->session->userdata('front_logged_in')) {
	$session_data = $this->session->userdata('front_logged_in');
	$user_id = $session_data['user_id'];
	$user_name = $session_data['user_name'];
	$_SESSION['user_id']=$user_id;
	$_SESSION['user_name']=$user_name;
	$type1 = $session_data['user_type_id'];
	$user_type_id=$type1;
	$brand_access=$session_data['brand_access'];

	$checkquerys = $this->db->query("select user_type_name from wc_users_type where user_type_id='$user_type_id' ")->result_array();
	$job_title=$checkquerys[0]['user_type_name'];

} 
else
{
	$user_id = '';
	$user_name = ''; 
	$_SESSION['user_id']=$user_id;
	$_SESSION['user_name']=$user_name;
}
$lastID ="";
?>

<?php $data['page_title'] = "dashboard";

$this->load->view('front/includes/header',$data);
$this->load->view('front/includes/topnav');
?>

<div class="bg-wc-light px-5" id="div_filter">
	<div class="row">
		<div class="col-12 col-sm-12 col-md-10">
			<div class="d-flex justify-content-start flex-wrap">
				<?php if($user_type_id=='4') { ?>
				<!-- <div class="p-2 icon-md">|</div> -->
						<div class="pt-2 mr-4 mt-1">
							
								<a class="text-wc lead" href="<?php echo base_url() ?>uploadbrief"><i class="fas fa-cloud-upload-alt"></i>&nbsp;ADD PROJECT</a>
							
						</div>
				<?php } ?>

				<?php
				$acount_permission=$this->Dashboard_model->checkPermission($user_type_id,1,'access');
				if($acount_permission=='1') {
					?>

					<div class="py-2 mr-2">
						<span class="btn btn-wc" data-toggle="collapse" data-target="#collapseAccount" aria-expanded="false" aria-controls="collapseAccount">Select Account&emsp;<i class="fas fa-chevron-down"></i></span>

						<div id="collapseAccount" class="card collapse" style="width: 20rem; position: absolute; z-index: 2221;" data-parent="#div_filter">
							<div class="card-body p-3">
								<div class="d-flex justify-content-around">
									<p class="flex-grow-1 font-weight-bold">Filter by Account</p>
									<span data-toggle="collapse" data-target="#collapseAccount" aria-expanded="false" aria-controls="collapseAccount"><i class="fas fa-times"></i></span>
								</div>

								<hr class="pt-0 mt-0">

								<div class="pl-3">
									<?php
									
									if($user_type_id==2)
										
										{
											$users_type_details=$this->db->query("SELECT * FROM `wc_clients` INNER JOIN wc_users WHERE wc_clients.client_id=wc_users.client_id and wc_clients.deleted='0' and wc_clients.status='Active' and wc_users.user_id='$user_id'")->result_array();
											
										}
										else
										{
									$users_type_details=$this->db->query("select * from  wc_clients where wc_clients.deleted='0' and wc_clients.status='Active'  ")->result_array();
										}

									if(!empty($users_type_details)) {
										foreach($users_type_details as $key => $userstypedetails) {
											$client_id=$userstypedetails['client_id'];
											$client_name=$userstypedetails['client_name'];
											?>

											<div class="custom-control custom-radio">
												<input type="radio" class="custom-control-input account_id" id="account<?php echo $client_id;?>" name="<?php echo $client_id;?>" >
												<label class="custom-control-label roundCheck" for="account<?php echo $client_id;?>"><?php echo ucfirst($client_name);?></label>
											</div>

										<?php } } ?>
									</div>
								</div>
							</div>
						</div>

					<?php } else { ?>
						<input type="hidden" name="account_id" id="account_id" value="">
					<?php } ?>

					<?php
					$account_permission = $this->Dashboard_model->checkPermission($user_type_id, 1, 'access');
					$brand_permission   = $this->Dashboard_model->checkPermission($user_type_id, 2, 'access');
					$admin_permission   = $this->Dashboard_model->checkPermission($user_type_id, 3, 'access');

					if( $account_permission == '1' || $brand_permission == '1' || $admin_permission == '1' )
					{

						$checkquerys = $this->db->query("select * from wc_users where  user_id='$user_id' ")->result_array();
						$brand_id = $checkquerys[0]['brand_id'];
						$client_id = $checkquerys[0]['client_id'];

						?>

						<div class="py-2 mr-2">
							<span class="btn btn-wc" data-toggle="collapse" data-target="#collapseBrand" aria-expanded="false" aria-controls="collapseBrand">Select Brand&emsp;<i class="fas fa-chevron-down"></i></span>

							<div id="collapseBrand" class="card collapse" style="width: 20rem; position: absolute; z-index: 2222;" data-parent="#div_filter">
								<div class="card-body p-3">
									<div class="d-flex justify-content-around">
										<p class="flex-grow-1 font-weight-bold">Filter by Brand</p>
										<span data-toggle="collapse" data-target="#collapseBrand" aria-expanded="false" aria-controls="collapseBrand"><i class="fas fa-times"></i></span>
									</div>

									<hr class="pt-0 mt-0">

									<div class="pl-3">
										<?php 
										$brand_id_check_add="";
										$sql="select * from wc_brands where status='Active' and client_id=$client_id";

										if( ($user_type_id=='2' || $user_type_id=='3') &&  $client_id!='0')
										{
											$sql.=" and brand_id in (select brand_id from wc_brands where client_id='".$client_id."') ";
										}

										$brand_type_check=$this->db->query($sql)->result_array();

										foreach($brand_type_check as $key => $brandstypedetails){
											$brand_id_check_add.=$brandstypedetails['brand_id'].",";
										}
										//echo $brand_id_check_add;
										$sql1="select * from wc_brief WHERE 1=1 and brand_id IN(".rtrim($brand_id_check_add,",").")";
												$brand_id_check_add=$this->db->query($sql1)->result_array();

									$all_count_check=count($brand_id_check_add); ?>
										<?php /*<div class="custom-control custom-radio">
											<input type="radio" class="custom-control-input brand_id" id="brand_all>" name="brand_id[]" onchange="sortbrief('')" value=''>
											<label class="custom-control-label roundCheck lead" for="brand_all>">All Brand <span class="badge badge-wc" id="cnt_<?php echo $brand_id;?>"><?php echo $all_count_check; ?></span></label>
										</div>*/?>
										<?php

										$sql="select * from wc_brands where status='Active' ";

										if( ($user_type_id=='2' || $user_type_id=='3') &&  $client_id!='0')
										{
											$sql.=" and brand_id in (select brand_id from wc_brands where client_id='".$client_id."') ";
										}

										if( $user_type_id=='4' &&  $brand_id!='0' )
										{
											//$sql.=" and brand_id in (".$brand_id.") ";
											$sql.=" and brand_id in (".trim($brand_id,",").") ";
										}

										$brand_type_details=$this->db->query($sql)->result_array();
										if(!empty($brand_type_details)){
											foreach($brand_type_details as $key => $brandstypedetails){
												$brand_id=$brandstypedetails['brand_id'];
												$brand_name=$brandstypedetails['brand_name'];

												$brand_id_check=$brandstypedetails['brand_id'];

												$sql="select * from wc_brief WHERE 1=1 and brand_id IN(".trim($brand_id_check,",").")";
												$brand_type_check=$this->db->query($sql)->result_array();

												$count_check=count($brand_type_check);
												?>

												<div class="custom-control custom-radio">
													<input type="radio" class="custom-control-input brand_id" id="brand_<?php echo $brand_id;?>" name="brand_id[]" onchange="sortbrief('<?php echo $lastID  ?>')" value='<?php echo $brand_id; ?>' brand_name="<?php echo $brand_name ?>">
													<label class="custom-control-label roundCheck lead" for="brand_<?php echo $brand_id;?>"><?php echo ucfirst($brand_name);?> <span class="badge badge-wc" id="cnt_<?php echo $brand_id;?>"><?php echo $count_check; ?></span></label>
												</div>

											<?php } } ?>
										</div>
									</div>
								</div>
							</div>

						<?php } else { ?>
							<input type="hidden" name="brand_id" id="brand_id" value="">
							<!-- <input type="hidden" name="search_key" id="search_key" value=""> -->
						<?php } ?>

						<?php /*<!-- <div class="p-2 icon-md">|</div> -->
						<div class="pt-2 mr-4 mt-1">
							<?php if($user_type_id=='4') { ?>
								<a class="text-wc lead" href="<?php echo base_url() ?>uploadbrief"><i class="fas fa-cloud-upload-alt"></i>&nbsp;ADD PROJECT</a>
							<?php } ?>
						</div>*/?>

						<div class="py-2 mr-2">
							<span class="btn btn-wc" data-toggle="collapse" data-target="#collapseFilter" aria-expanded="false" aria-controls="collapseFilter">Filter&emsp;<i class="fas fa-chevron-down"></i></span>

							<div id="collapseFilter" class="card collapse" style="width: 20rem; position: absolute; z-index: 2223;" data-parent="#div_filter">
								<div class="card-body p-3">
									<div class="d-flex justify-content-around">
										<p class="flex-grow-1 font-weight-bold">Filter by Stages</p>
										<span data-toggle="collapse" data-target="#collapseFilter" aria-expanded="false" aria-controls="collapseFilter"><i class="fas fa-times"></i></span>
									</div>

									<hr class="pt-0 mt-0">

									<div class="pl-3">
										<!-- <div class="custom-control custom-radio">
											<input type="radio" class="custom-control-input status_filter" name="breif_status[]" id="bsfall"  value="" onchange="sortbrief('')">
											<label class="custom-control-label roundCheck lead" for="bsfall">All Brand <span class="badge badge-wc" id="bir"></span></label>
										</div> -->
										<div class="custom-control custom-radio">
											<input type="radio" class="custom-control-input status_filter" name="breif_status[]" id="bsf0"  value="0" onchange="sortbrief('<?php echo $lastID  ?>')">
											<label class="custom-control-label roundCheck lead" for="bsf0">Brief in Review <span class="badge badge-wc" id="bir"><?php echo $briefinreview; ?></span></label>
										</div>

										<div class="custom-control custom-radio">
											<input type="radio" class="custom-control-input status_filter" name="breif_status[]" id="bsf4" value="4" onchange="sortbrief('<?php echo $lastID  ?>')">
											<label class="custom-control-label roundCheck lead" for="bsf4">Brief Rejected <span class="badge badge-wc" id="br"><?php echo $briefrejected; ?></span></label>
										</div>

										<div class="custom-control custom-radio">
											<input type="radio" class="custom-control-input status_filter" name="breif_status[]" id="bsf1" value="1" onchange="sortbrief('<?php echo $lastID  ?>')">
											<label class="custom-control-label roundCheck lead" for="bsf1">Work in Progress <span class="badge badge-wc" id="wip"><?php echo $workinprogress; ?></span></label>
										</div>

										<div class="custom-control custom-radio">
											<input type="radio" class="custom-control-input status_filter" name="breif_status[]" id="bsf6" value="6" onchange="sortbrief('<?php echo $lastID  ?>')">
											<label class="custom-control-label roundCheck lead" for="bsf6">Proofing Pending <span class="badge badge-wc" id="pp"><?php echo $proffing_pending; ?></span></label>
										</div>

										<div class="custom-control custom-radio">
											<input type="radio" class="custom-control-input status_filter" name="breif_status[]" id="bsf3" value="3" onchange="sortbrief('<?php echo $lastID  ?>')">
											<label class="custom-control-label roundCheck lead" for="bsf3">Revision Work <span class="badge badge-wc" id="rw"><?php echo $revision_work; ?></span></label>
										</div>

										<div class="custom-control custom-radio">
											<input type="radio" class="custom-control-input status_filter" name="breif_status[]" id="bsf2" value="2" onchange="sortbrief('<?php echo $lastID  ?>')">
											<label class="custom-control-label roundCheck lead" for="bsf2">Work Complete <span class="badge badge-wc" id="wc"><?php echo $work_complete; ?></span></label>
										</div>                              

                        <!-- <div class="custom-control custom-radio">
                            <input type="radio" class="custom-control-input" name="breif_status[]" id="bsf5" value="5" onchange="sortbrief('<?php echo $lastID  ?>')">
                            <label class="custom-control-label roundCheck lead" for="bsf5">Feedback Pending <span class="badge badge-wc" id="fp"><?php echo $feedback_pending; ?></span></label>
                          </div> -->

                          <div class="custom-control custom-radio">
                          	<input type="radio" class="custom-control-input status_filter" name="breif_status[]" id="bsf7" value="7" onchange="sortbrief('<?php echo $lastID  ?>')">
                          	<label class="custom-control-label roundCheck lead" for="bsf7">Archived <span class="badge badge-wc" id="fc"><?php echo $feedback_completed; ?></span></label>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
             <!-- <div class="pt-2 mt-1">
             	<input type="hidden" class="form-control" id="search_key" name="search_key" placeholder="Search Brief Id" onkeyup="sortbrief('<?php echo $lastID  ?>');">
             </div> -->

             <!-- <div class="pt-1 mr-2 mt-1">
             	<form>
             		<input type="text" class="form-control" id="search_key" name="search_key" placeholder="Search By Id, Title" onkeyup="sortbrief('<?php echo $lastID  ?>');">
             	</form>

             </div>-->

           </div>
         </div>
         <div class="col-12 col-sm-12 col-md-2">
         	<div class="pt-1 my-1">
         		<form>
         			<input type="text" class="form-control" id="search_key" name="search_key" placeholder="Search By Id, Title" onkeyup="sortbrief('<?php echo $lastID  ?>');">
         		</form>

         	</div>
         </div>
       </div>
     </div>

     <div class="d-flex justify-content-start flex-wrap p-2 pt-4 px-5 mb-3" id="div_sel_filter">
     	<!-- <span class="py-1 px-2 text-wc border-wc rounded-pill">In Review&emsp;<i class="fas fa-times text-danger"></i></span> -->
     </div>

     <div class="container-fluid px-5">
     	<div class="row">

     		<!-- Main Body -->
     		<div class="col-12 col-sm-12 col-md-12" id="myResponse">
     			<div id="tblbrf" class="row">

     				<?php // print_r($dashboradinfo);
     				//print_r($showLimit);	
     				$this->load->view('front/dashboard_search', $dashboradinfo);   ?>
     			</div>
				</div>

			</div>
		</div>

<?php $this->load->view('front/includes/footer'); ?>
<link href="<?php echo base_url(); ?>website-assets/datetimepicker/bootstrap/css/bootstrap-datetimepicker.min.css" rel="stylesheet" media="screen">
<link href="<?php echo base_url(); ?>website-assets/datetimepicker/css/bootstrap-datetimepicker.min.css" rel="stylesheet" media="screen">

<script type="text/javascript" src="<?php echo base_url(); ?>website-assets/datetimepicker/js/bootstrap-datetimepicker.js" charset="UTF-8"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>website-assets/datetimepicker/js/locales/bootstrap-datetimepicker.fr.js" charset="UTF-8"></script>
<script type="text/javascript">
$(document).ready(function(){

	//alert("aa");
	$(window).scroll(function(){
		//alert("scroll");
		
		var lastID = $('.load-more').attr('lastID');
		//alert(lastID);
	//	console.log(lastID);

		if(($(window).scrollTop() == $(document).height() - $(window).height()) && (lastID != 0)){
			
			sortbrief(lastID);
			
		}
	});
});

function delete_card(brief_id){
	$("#loader").show();
	$.ajax({
		type: "POST",
		url: "<?php echo base_url();?>front/dashboard/delete_card", 
		dataType: 'html',
		data: 'brief_id='+brief_id,
		success: function(data){
			$("#loader").hide();
			
			//$('#modal_confirm_'+brief_id).modal('hide');
			var lastID="";
			sortbrief(lastID);
			// $("#click_"+brief_id).collapse('hide');
			// $("#card_"+brief_id).addClass('d-none');		
		}
	});
}

function price_cal(brief_id){
	
	var product_id=$("#product_id_"+brief_id).val();
	var txtQty=$("#txtQty_"+brief_id).val();
	if(product_id!="" && txtQty!=""){
		$("#no-result"+brief_id).addClass('d-none');
		var calculated_total_sum = 0;
		$.ajax({
			type: "POST",
			url: "<?php echo base_url();?>front/dashboard/price_cal", 
			dataType: 'html',
			data: 'product_id='+product_id+"&txtQty="+txtQty+"&brief_id="+brief_id,
			success: function(data){
				$('#product-list'+brief_id).append(data);
				$("#product_id_"+brief_id+" option[value="+product_id +"]").prop('disabled', true);
				$("#product_id_"+brief_id).prop("selectedIndex", 0);
				$("#txtQty_"+brief_id).val("");
				$("#product-list"+brief_id+" #subtotal_price"+brief_id).each(function () {
					var get_textbox_value = $(this).val();
					if ($.isNumeric(get_textbox_value)) {
						calculated_total_sum += parseFloat(get_textbox_value);
					}                  
				});
				$("#total_price_values"+brief_id).html(calculated_total_sum);
				$("#total_price"+brief_id).val(calculated_total_sum);
			}
		});
	}
	if(product_id=="0"){
		
		$("#errmsg_product_id"+brief_id).html("Select Product").show().fadeOut(1500);
	}
	if(txtQty==""){
		$("#errmsg_txtQty"+brief_id).html("Enter the value").show().fadeOut(1500);
	}
}
function view_product_add(brief_id){
	
	var product_id=$("#view_product_id_"+brief_id).val();
	var txtQty=$("#view_txtQty_"+brief_id).val();
	if(product_id!="" && txtQty!=""){
		$("#view_no-result"+brief_id).addClass('d-none');
		var calculated_total_sum = 0;
	

		$.ajax({
			type: "POST",
			url: "<?php echo base_url();?>front/dashboard/view_product_add", 
			dataType: 'html',
			data: 'product_id='+product_id+"&txtQty="+txtQty+"&brief_id="+brief_id,
			success: function(data){

				$('#view_product-list'+brief_id).append(data);

				$("#view_product_id_"+brief_id+" option[value="+product_id +"]").prop('disabled', true);
				$("#view_product_id_"+brief_id).prop("selectedIndex", 0);
				$("#view_txtQty_"+brief_id).val("");
				$("#view_product-list"+brief_id+" .subtotal_price_"+brief_id).each(function () {
					var get_textbox_value = $(this).val();
					if ($.isNumeric(get_textbox_value)) {
						calculated_total_sum += parseFloat(get_textbox_value);
					}                  
				});
				//alert(calculated_total_sum);
				$("#total_price_values"+brief_id).html(calculated_total_sum);
				$("#total_price"+brief_id).val(calculated_total_sum);	
				
			}
		});
	}
	if(product_id=="0"){
		$("#view_errmsg_product_id"+brief_id).html("Select Product").show().fadeOut(1500);
	}
	if(txtQty==""){
		$("#view_errmsg_txtQty"+brief_id).html("Enter the value").show().fadeOut(1500);
	}	
}
function update_price_cal(brief_id,brief_product_id,update_quty ){
	//	$("#no-result").addClass('d-none');
	var product_id=$("#product_id"+brief_product_id).val();
	var txtQty=$("#product_qty"+brief_product_id).val();
	//alert(txtQty);
	var calculated_total_sum=0;
	if(txtQty=="")
	{
		txtQty=0;
	}

	$.ajax({
		type: "POST",
		url: "<?php echo base_url();?>front/dashboard/price_cal", 
		dataType: 'html',
		data: 'product_id='+product_id+"&txtQty="+txtQty+"&brief_id="+brief_id+"&update_quty="+update_quty,
		success: function(data){
    	//alert(data);
    	$('#subtotal_price_'+brief_product_id).val(data);
    	$('#div_subtotal_price_'+brief_product_id).html("Rs."+data);
    	$("#view_product-list"+brief_id+" .subtotal_price_"+brief_id).each(function () {
    		var get_textbox_value = $(this).val();
    		if ($.isNumeric(get_textbox_value)) {
    			calculated_total_sum += parseFloat(get_textbox_value);
    		}                  
    	});
    	$("#total_price_values"+brief_id).html(calculated_total_sum);
    	$("#total_price"+brief_id).val(calculated_total_sum);
    }
  });
}

function price_cancel(brief_id,product_id){
	$('#'+brief_id+"_"+product_id).remove();
	$("#product_id_"+brief_id+" option[value="+product_id +"]").prop('disabled', false);
	var calculated_total_sum = 0;
	$("#product-list"+brief_id+" #subtotal_price"+brief_id).each(function () {
		var get_textbox_value = $(this).val();
		if ($.isNumeric(get_textbox_value)) {
			calculated_total_sum += parseFloat(get_textbox_value);
		}                  
	});
	if($("#product-list"+brief_id+" tr").length ==1){
	    $("#no-result"+brief_id).removeClass('d-none');
	}
	$("#total_price_values"+brief_id).html(calculated_total_sum);
	$("#total_price"+brief_id).val(calculated_total_sum);

}
function price_edit(brief_product_id){
	//  alert("work edit");
	$("#div_product_quty"+brief_product_id).addClass("d-none");
	$("#product_qty"+brief_product_id).removeClass("d-none");
	$("#edit_product"+brief_product_id).addClass("d-none");
	$("#delete_product"+brief_product_id).addClass("d-none");
	$("#update_product"+brief_product_id).removeClass("d-none");
	$("#cancel_product"+brief_product_id).removeClass("d-none");    
}
function cancel_product(brief_product_id){
	$("#div_product_quty"+brief_product_id).removeClass("d-none");
	$("#product_qty"+brief_product_id).addClass("d-none");
	$("#edit_product"+brief_product_id).removeClass("d-none");
	$("#delete_product"+brief_product_id).removeClass("d-none");
	$("#update_product"+brief_product_id).addClass("d-none");
	$("#cancel_product"+brief_product_id).addClass("d-none"); 
}

function price_delete(brief_id,brief_product_id){
	//$('#click_'+brief_id).addClass('hide');
	$("#div_product_quty"+brief_product_id).removeClass("d-none");
	$("#product_qty"+brief_product_id).addClass("d-none");
	$("#edit_product"+brief_product_id).removeClass("d-none");
	$("#delete_product"+brief_product_id).removeClass("d-none");
	$("#update_product"+brief_product_id).addClass("d-none");
	$("#cancel_product"+brief_product_id).addClass("d-none"); 

}
function delete_product(brief_id,brief_product_id){
	$("#loader").show();
	var product_id=$("#product_id"+brief_product_id).val();
 // console.log("brief_id="+brief_id+"&product_qty="+product_qty+"&subtotal_price="+subtotal_price+"&brief_product_id="+brief_product_id+"&total_price="+total_price);
 $.ajax({
 	type: "POST",
 	url: "<?php echo base_url();?>front/dashboard/delete_product_list", 
 	dataType: 'html',
 	data: "brief_id="+brief_id+"&brief_product_id="+brief_product_id,
 	success: function(data){
    	//alert(data);
    	$("#loader").hide();
    	$('#view_'+brief_id+"_"+brief_product_id).remove();
    	$("#view_product_id_"+brief_id+" option[value="+product_id +"]").prop('disabled', false);
    	//$("#click_"+brief_id).collapse('hide');
			//$("#modal_confirm_product"+brief_product_id).modal('hide');
    	$('#total_price_values'+brief_id).html("Rs."+data);
    	$('#total_price'+brief_id).html(data);
    	if($("#view_product-list"+brief_id+" tr").length ==1){

	    	$("#view_no-result"+brief_id).removeClass('d-none');
			}

    }
  });
}
function update_product(brief_id,brief_product_id){
	//var product_id=$("#product_id"+brief_product_id).val();
	var product_qty=$("#product_qty"+brief_product_id).val();
	var subtotal_price= $('#subtotal_price_'+brief_product_id).val();
	var total_price=$('#total_price'+brief_id).val(); 
	if(product_qty!=""){
		//alert(total_price);
		$("#loader").show();
		$("#div_product_quty"+brief_product_id).html(product_qty);

		$("#div_product_quty"+brief_product_id).removeClass("d-none");
		$("#product_qty"+brief_product_id).addClass("d-none");
		$("#edit_product"+brief_product_id).removeClass("d-none");
		$("#delete_product"+brief_product_id).removeClass("d-none");
		$("#update_product"+brief_product_id).addClass("d-none");
		$("#cancel_product"+brief_product_id).addClass("d-none"); 
		//console.log("brief_id="+brief_id+"&product_qty="+product_qty+"&subtotal_price="+subtotal_price+"&brief_product_id="+brief_product_id+"&total_price="+total_price);
		$.ajax({
			type: "POST",
			url: "<?php echo base_url();?>front/dashboard/update_product_list", 
			dataType: 'html',
			data: "brief_id="+brief_id+"&product_qty="+product_qty+"&subtotal_price="+subtotal_price+"&brief_product_id="+brief_product_id+"&total_price="+total_price,
			success: function(data){
				//alert(data);
				$("#loader").hide();
				//$('#subtotal_price_'+brief_product_id).html("Rs."+data);

			}
		});
	}
	else{
	$("#errmsg_product_qty_"+brief_product_id).html("Enter the Value").show().fadeOut(1500);
	}
}
function changeBriefStatus(brief_id,brief_status){
  //var brief_status=$("#brief_status"+brief_id).val();

  if(brief_status=='1')
  {
    //alert(brief_status);
    //$('.collapse').collapse('show');
    $("#div_brief_due_date"+brief_id).removeClass('d-none');

    $("#brief_review_approve_"+brief_id).removeClass('d-none');
    $("#brief_review_reject_"+brief_id).addClass('d-none');
    $(".btn_yes"+brief_id).removeClass('btn-outline-wc');
    $(".btn_yes"+brief_id).addClass('btn-wc');
    $(".btn_no"+brief_id).addClass('btn-outline-wc');
    $(".btn_no"+brief_id).removeClass('btn-wc');

    // $("#brief_approve_reject_"+brief_id).addClass('d-none');
   // $("#brief_approve_reject_"+brief_id).attr('style', 'display:none!important');
    //$(".brief_review_reject_"+brief_id).addClass('d-none');
    //$(".brief_download_"+brief_id).removeClass('d-none');
  }
  else if(brief_status=='4')
  {
  	$(".btn_no"+brief_id).removeClass('btn-outline-wc');
  	$(".btn_no"+brief_id).addClass('btn-wc');
  	$(".btn_yes"+brief_id).addClass('btn-outline-wc');
  	$(".btn_yes"+brief_id).removeClass('btn-wc');
  	$("#rejected_reasons"+brief_id).val('');
  	$("#div_brief_due_date"+brief_id).addClass('d-none');
  	$("#brief_review_approve_"+brief_id).addClass('d-none');
  	$("#brief_review_reject_"+brief_id).removeClass('d-none');
		//$("#brief_approve_reject_"+brief_id).addClass('d-none');
		//	$("#brief_approve_reject_"+brief_id).attr('style', 'display:none!important');
		$("#brief_review_reject_"+brief_id).removeClass('d-none');
		$( "#tr_"+brief_id ).removeClass( "border-bottom" );
	}
else
{

	$("#brief_review_approve_"+brief_id).addClass('d-none');
	$("#brief_review_reject_"+brief_id).addClass('d-none');

}
}
function submitCancel(brief_id)
{
	$("#click_"+brief_id).collapse('hide');
}

function submitCorfirm(brief_id,brief_status)
{


	// var brief_status=$("#brief_status"+brief_id).val();
	//alert("--"+brief_status);
	if(brief_status=='')
	{
		// alert("Please select Status");
		$('#modal_msg').modal('show');
		$('#txtMsg').html('Please select Status');
	}
	else
	{
		if(brief_status=='1')
		{
			$('#click_'+brief_id).modal('hide');
			var brief_due_date=$("#brief_due_date"+brief_id).val();
			//alert(brief_due_date);
			//var brief_due_date = $("#brief_due_date"+brief_id).attr('placeholder'); 

			//alert(brief_due_date);
			var rejected_reasons='';
			var product_id = $("input[id='product_id"+brief_id+"']")
			.map(function(){return $(this).val();}).get();
			var total_qty = $("input[id='total_qty"+brief_id+"']")
			.map(function(){return $(this).val();}).get();
			var subtotal_price = $("input[id='subtotal_price"+brief_id+"']")
			.map(function(){return $(this).val();}).get();
			var total_price = $("#total_price"+brief_id+"").val();
			//alert(total_price);
			$("#loader").show();


		}
		else if(brief_status=='4')
		{
			var brief_due_date='';
			var rejected_reasons=$("#rejected_reasons"+brief_id).val();
			var product_id = '';
			var total_qty = '';
			var subtotal_price = '';
			var total_price = '';
			if(rejected_reasons=='')
			{
			  // alert("Please Give Reason");
			  $('#modal_msg').modal('show');
			  $('#txtMsg').html('Please Give Reason');
			  return false;
			}
			else
			{
				$("#loader").show();
			}

		}
		else if(brief_status=='0')
		{
			var brief_due_date=$("#brief_due_date"+brief_id).val();
			//alert(brief_due_date);
			//var brief_due_date = $("#brief_due_date"+brief_id).attr('placeholder'); 

			//alert(brief_due_date);
			var rejected_reasons='';
			var product_id = '';
			var total_qty = '';
			var subtotal_price = '';
			var total_price = '';
			$("#loader").show();
		}
	
		/*$.ajax({
			method:'POST',
			url: "<?php echo base_url();?>front/uploadbrief/updateBriefStatus",
			data:"&brief_id="+brief_id+"&brief_status="+brief_status+"&brief_due_date="+brief_due_date+"&rejected_reasons="+rejected_reasons+"&total_price="+total_price+"&product_id="+product_id+"&total_qty="+total_qty+"&subtotal_price="+subtotal_price,
			success: function(result){
				//alert(result);
				if(result) {

					$("#loader").hide();
					//$("#click_"+brief_id).collapse('hide');
					//$("#card_"+brief_id).addClass('d-none');
					var lastID="";
					//var lastID = $('.load-more').attr('lastID');
					sortbrief(lastID);
				}
			}
		});*/

		//alert(brief_status);

		$.ajax({
			method:'POST',
			url: "<?php echo base_url();?>front/uploadbrief/updateBriefStatus",
			data:"&brief_id="+brief_id+"&brief_status="+brief_status+"&brief_due_date="+brief_due_date+"&rejected_reasons="+rejected_reasons+"&total_price="+total_price+"&product_id="+product_id+"&total_qty="+total_qty+"&subtotal_price="+subtotal_price,
			cache: true,
			timeout: 2000
			}).done(function( result ) {
					//console.log( "SUCCESS: " + result );
					if(result) {

					$("#loader").hide();
					if(brief_status=='0' && result=="success")
					{
						$('#modal_msg').modal('show');
						$('#txtMsg').html('The Project #P'+brief_id+' delivery date has been updated');
					}
					//$("#click_"+brief_id).collapse('hide');
					//$("#card_"+brief_id).addClass('d-none');
					var lastID="";
					//var lastID = $('.load-more').attr('lastID');
					sortbrief(lastID);
					}
			}).fail(function() {

				//alert("aaaa");
			       console.log( "Request failed");
			        $("#loader").hide();
					//$("#click_"+brief_id).collapse('hide');
					//$("#card_"+brief_id).addClass('d-none');
					var lastID="";
					//var lastID = $('.load-more').attr('lastID');
					sortbrief(lastID);
			});

	}
}
/*$(document).ready(function() {*/

function selectAccount(brand_id_sel)
{
  //alert(brand_id_sel);
  var account_id=$('#account_id').val();
   //alert(account_id);
   $.ajax({
   	method:'POST',
   	url: "<?php echo base_url();?>front/dashboard/getBrandDropDown",
   	data:"&account_id="+account_id+"&brand_id_sel="+brand_id_sel,

   	success: function(result){
      //alert(result);
      if(result) {
      	$("#divBrand").html(result);
      	var lastID = $('.load-more').attr('lastID');
				sortbrief(lastID);

      }
    }
  });
}

function removeFilter(filter_id) {
	//alert(filter_id);
	document.getElementById("bsf"+filter_id).checked = false;
	var lastID = "";
	sortbrief(lastID);
}
function removeBrand(brand_id) {
	//alert(brand_id);
	document.getElementById("brand_"+brand_id).checked = false;
	var lastID = "";
	sortbrief(lastID);
}
function sortbrief(lastID) {
		//alert("sortbrief");
	if(lastID==""){
		$("#loader").show();
	}
 
  var status_filter = new Array();
  var status_filter_name = "";
  $(".status_filter:checked").each(function() {
  	status_filter.push($(this).val());

  	var filter_id=$(this).val();
  	if(filter_id=="0"){
  		var filt_name="Brief in Review";
  	}if(filter_id=="1"){
  		var filt_name="Work in Progress";
  	}if(filter_id=="2"){
  		var filt_name="Work Completed";
  	}if(filter_id=="3"){
  		var filt_name="Revision Work";
  	}if(filter_id=="4"){
  		var filt_name="Brief Rejected";
  	}if(filter_id=="5"){
  		var filt_name="Feedback Pending";
  	}if(filter_id=="6"){
  		var filt_name="Proofing Pending";
  	}if(filter_id=="7"){
  		var filt_name="Archived";
  	}if(filter_id==""){
  		var filt_name="All Status";
  	}
  	status_filter_name+='<span class="py-1 px-2 mr-2 text-wc border-wc rounded-pill">'+filt_name+'&emsp;<span onclick="removeFilter('+filter_id+');"><i class="fas fa-times" ></i></span></span>';
  });

	//alert(status_filter_name);

	$("#div_sel_filter").html(status_filter_name);
	var brand_id = new Array();
	$(".brand_id:checked").each(function() {
		brand_id.push($(this).val());
		if(brand_id!=""){
  			var brand_filt_name=$('#brand_'+brand_id).attr('brand_name');
  		}  	
  		//alert(brand_filt_name);
  		status_filter_name+='<span class="py-1 px-2 mr-2 text-wc border-wc rounded-pill">'+brand_filt_name+'&emsp;<span onclick="removeBrand('+brand_id+');"><i class="fas fa-times" ></i></span></span>';
	});
$("#div_sel_filter").html(status_filter_name);
	var account_id = new Array();
	$(".account_id:checked").each(function() {
		account_id.push($(this).val());
	});
 
  var search_key = $("#search_key").val();
  

  $.ajax({
  	type: "POST",
  	url: "<?php echo base_url();?>front/dashboard/getbriefsort",
  	 async: false,
  	dataType: 'html',
  	data: 'search_key='+search_key+'&brand_id='+brand_id+'&account_id='+account_id+'&status_filter='+status_filter+'&lastID='+lastID,
  	// beforeSend:function(){
  	// 	alert('beforeSend');
   //     $('.load-more').show();
   //  },
  	success: function(data){

      //alert(data);

      if($.trim(data)!='sessionout')
      {
		
		if(lastID==""){
		$('#tblbrf').html("");
		$('#tblbrf').html(data);

		$("#loader").hide();
		//alert(lastID);
		}
		else{
		$('.load-more').remove();
		$('#tblbrf').append(data);

		}


		getbir(brand_id);
		getbr(brand_id);
		getwip(brand_id);
		getrw(brand_id);
		getpp(brand_id);
		getwc(brand_id);
		getfp(brand_id);
		getfc(brand_id);

	}
	else
      {
      	    window.location = "<?php echo base_url(); ?>login";

      }



      
    }
  });
  return false;
}

function getbir(brand_id) {
	var account_id=$('#account_id').val();
	var search_key = $("#search_key").val();

	$.ajax({
		type: "POST",
		url: "<?php echo base_url();?>front/dashboard/getbir", 
		dataType: 'html',
		data: '&search_key='+search_key+'&account_id='+account_id+'&brand_id='+brand_id,
		success: function(data){
      //alert(data);
      $('#bir').html(data)            
    }
  });
}

function getbr(brand_id) {

	var account_id=$('#account_id').val();
	var search_key = $("#search_key").val();
	$.ajax({
		type: "POST",
		url: "<?php echo base_url();?>front/dashboard/getbr", 
		dataType: 'html',
		data: '&search_key='+search_key+'&account_id='+account_id+'&brand_id='+brand_id,
		success: function(data){
      //alert(data);
      $('#br').html(data)         
    }
  });
}

function getwip(brand_id) {

	var account_id=$('#account_id').val();
	var search_key = $("#search_key").val();
	$.ajax({
		type: "POST",
		url: "<?php echo base_url();?>front/dashboard/getwip", 
		dataType: 'html',
		data: '&search_key='+search_key+'&account_id='+account_id+'&brand_id='+brand_id,
		success: function(data){
	    //alert(data);
	    $('#wip').html(data)            
	  }
	});
}

function getrw(brand_id) {

	var account_id=$('#account_id').val();
	var search_key = $("#search_key").val();
	$.ajax({
		type: "POST",
		url: "<?php echo base_url();?>front/dashboard/getrw", 
		dataType: 'html',
		data: '&search_key='+search_key+'&account_id='+account_id+'&brand_id='+brand_id,
		success: function(data){
	    //alert(data);
	    $('#rw').html(data)
	  }
	});
}

function getpp(brand_id)
{

	var account_id=$('#account_id').val();
	var search_key = $("#search_key").val();
	$.ajax({
		type: "POST",
		url: "<?php echo base_url();?>front/dashboard/getpp", 
		dataType: 'html',
		data: '&search_key='+search_key+'&account_id='+account_id+'&brand_id='+brand_id,
		success: function(data){
		    //alert(data);
		    $('#pp').html(data)         
		}
	});
}

function getwc(brand_id)
{

	var account_id=$('#account_id').val();
	var search_key = $("#search_key").val();
	$.ajax({
		type: "POST",
		url: "<?php echo base_url();?>front/dashboard/getwc", 
		dataType: 'html',
		data: '&search_key='+search_key+'&account_id='+account_id+'&brand_id='+brand_id,
		success: function(data){
		    //alert(data);
		    $('#wc').html(data)         
		}
	});
}

function getfp(brand_id)
{

	var account_id=$('#account_id').val();
	var search_key = $("#search_key").val();
	$.ajax({
		type: "POST",
		url: "<?php echo base_url();?>front/dashboard/getfp", 
		dataType: 'html',
		data: '&search_key='+search_key+'&account_id='+account_id+'&brand_id='+brand_id,
		success: function(data){
		  //alert(data);
		  $('#fp').html(data)         
		}
	});
}


function getfc(brand_id)
{

	var account_id=$('#account_id').val();
	var search_key = $("#search_key").val();
	$.ajax({
		type: "POST",
		url: "<?php echo base_url();?>front/dashboard/getfc", 
		dataType: 'html',
		data: '&search_key='+search_key+'&account_id='+account_id+'&brand_id='+brand_id,
		success: function(data){
		  //alert(data);
		  $('#fc').html(data)         
		}
	});
}

      var functionUpload = function(e,no,brief_id) {


        //alert("goko");
        

        //alert(brief_id);

        var responseText = $.ajax({
        	type:'POST',
        	url:"<?php echo base_url();?>front/dashboard/getTempFolder",
        //data: {'skufolderfile_nm':skufolderfile_nm},
        async: false
      }).responseText;

        $(this).attr("folder",responseText);


        var folder= $(this).attr('folder') ;

        //alert(folder);

        var files = e.target.files,
        filesLength = files.length;

        console.log(files);

        for (var i = 0; i < filesLength; i++) {
        	var file = files[i];



        	console.log(file);
        	console.log("bbbbbbbbbb");



        	if(file != undefined){
        		formData= new FormData();

        //var ext = $("#files"+no).val().split('.',2).pop().toLowerCase();

        //alert(ext);


        //if ($.inArray(ext, ['doc','docx']) == 1)
        /* if (ext=='doc' || ext=='docx')
        {*/
        	formData.append("doc_file", file);
        	formData.append("brief_id", brief_id);
        	formData.append("folder", folder);
        	formData.append("check", i);



        //alert("aaaa");
        $.ajax({
        	url: "<?php echo base_url();?>front/dashboard/upload_file",
        	type: "POST",
        	data: formData,
        	processData: false,
        	contentType: false,
        	success: function(response){

        //alert("koko");
        //alert(data);

        var responseArr=response.split("@@@@@@@@@@@");
        var doc_file_name=$.trim(responseArr[0]);
        var check=$.trim(responseArr[1]);

        


        var fileName =$('#file_nm_hid_'+brief_id).val();
        //alert(fileName);

        if(fileName=='')
        {
        	var fileNameCurrent=",,,"+doc_file_name+",,,";
        }
        else
        {
        	if(fileName.indexOf(",,,"+doc_file_name+",,,")==-1)
        	{
        		var fileNameCurrent=fileName+doc_file_name+",,,";
        	}
        	else
        	{
        		var fileNameCurrent=fileName;
        	}

        }

        $('#file_nm_hid_'+brief_id).val(fileNameCurrent);


        if(check==parseInt(filesLength)-1)
        {
        	functionMove(e,no,brief_id);
        }

        



      }
    });
        /*}else{

        alert("Please Upload Doc");
        return false;
        }
        */

      }
      else{
        //alert('Input something!');
      }



        //alert(i+"-----"+filesLength);

        

        
      }



    //functionMove(e,no,brief_id);
    //return deferred;
  }

  var functionMove = function(e,no,brief_id) {


    // alert("koko");


    var file_nm =$('#file_nm_hid_'+brief_id).val();

    //alert('&brief_id='+brief_id+'&temp_folder='+folder+'&file_nm='+file_nm);
    $("#loader").show();

/*    $.ajax({
    	method:"post",
    	url: "<?php echo base_url();?>front/dashboard/move_upload_file",
    	data:'&brief_id='+brief_id+'&temp_folder='+folder+'&file_nm='+file_nm,
    	success: function(result){

				//alert(result);
				$("#loader").hide();
				var lastID = "";
				sortbrief(lastID);

				$('.re_upload_doc').on('change', function (e) {

				var no = $(this).attr('alt') ;
				var brief_id= $(this).attr('val') ;
				functionUpload(e,no,brief_id);

				
				});

        }
        });*/


        $.ajax({
			method:"post",
			url: "<?php echo base_url();?>front/dashboard/move_upload_file",
			data:'&brief_id='+brief_id+'&temp_folder='+folder+'&file_nm='+file_nm,
			cache: true,
			timeout: 2000
			}).done(function( result ) {
			console.log( "SUCCESS: " + result );
			//alert(result);
				$("#loader").hide();
				var lastID = "";
				sortbrief(lastID);

				$('.re_upload_doc').on('change', function (e) {

				var no = $(this).attr('alt') ;
				var brief_id= $(this).attr('val') ;
				functionUpload(e,no,brief_id);

				/*functionUpload(e,no,brief_id).then(functionMove(e,no,brief_id));*/
				});
			}).fail(function() {
			console.log( "Request failed");
			//alert(result);
				$("#loader").hide();
				var lastID = "";
				sortbrief(lastID);

				$('.re_upload_doc').on('change', function (e) {

				var no = $(this).attr('alt') ;
				var brief_id= $(this).attr('val') ;
				functionUpload(e,no,brief_id);

				/*functionUpload(e,no,brief_id).then(functionMove(e,no,brief_id));*/
				});
			});






  }




  $('.re_upload_doc').on('change', function (e) {

  	var no = $(this).attr('alt') ;
  	var brief_id= $(this).attr('val') ;
  	functionUpload(e,no,brief_id);

  	/*functionUpload(e,no,brief_id).then(functionMove(e,no,brief_id));*/
  });



  $(".re_upload_doc_old").on("change", function(e) {

 //alert("goko");
 var no = $(this).attr('alt') ;
 var brief_id= $(this).attr('val') ;

//alert(brief_id);

var responseText = $.ajax({
	type:'POST',
	url:"<?php echo base_url();?>front/dashboard/getTempFolder",
        //data: {'skufolderfile_nm':skufolderfile_nm},
        async: false
      }).responseText;

$(this).attr("folder",responseText);


var folder= $(this).attr('folder') ;

 //alert(folder);

 var files = e.target.files,
 filesLength = files.length;

 console.log(files);
 for (var i = 0; i < filesLength; i++) {
 	var f = files[i]
 	var fileReader = new FileReader();
 	fileReader.onload = (function(e) {
 		var file = e.target;

 		console.log(file);
 		console.log("aaaaaaaaaaaaaa");




//alert(no);
var input = document.getElementById("files"+no);
//file = input.files[0];

for (var k = 0; k < filesLength; k++) {
	file = input.files[k];

	console.log(file);
	console.log("bbbbbbbbbb");



	if(file != undefined){
		formData= new FormData();

		var ext = $("#files"+no).val().split('.',2).pop().toLowerCase();

    //alert(ext);


    //if ($.inArray(ext, ['doc','docx']) == 1)
   /* if (ext=='doc' || ext=='docx')
   {*/
   	formData.append("doc_file", file);
   	formData.append("brief_id", brief_id);
   	formData.append("folder", folder);



  //alert("aaaa");
  $.ajax({
  	url: "<?php echo base_url();?>front/dashboard/upload_file",
  	type: "POST",
  	data: formData,
  	processData: false,
  	contentType: false,
  	success: function(data){

     //alert("koko");
     //alert(data);

     var doc_file_name=$.trim(data);


     var fileName =$('#file_nm_hid_'+brief_id).val();

     if(fileName=='')
     {
     	var fileNameCurrent=",,,"+doc_file_name+",,,";
     }
     else
     {
     	if(fileName.indexOf(",,,"+doc_file_name+",,,")==-1)
     	{
     		var fileNameCurrent=fileName+doc_file_name+",,,";
     	}
     	else
     	{
     		var fileNameCurrent=fileName;
     	}

     }

     $('#file_nm_hid_'+brief_id).val(fileNameCurrent);



   }
 });
/*}else{

    alert("Please Upload Doc");
    return false;
}
*/

}
else{
// alert('Input something!');
$('#modal_msg').modal('show');
$('#txtMsg').html('Input something!');
}


}



});
 	fileReader.readAsDataURL(f);
 }






});




</script>