<?php
session_start();
error_reporting(0);
if ($this->session->userdata('front_logged_in')) {
$session_data = $this->session->userdata('front_logged_in');
$user_id = $session_data['user_id'];
$user_name = $session_data['user_name'];
$user_type_id = $session_data['user_type_id'];
$_SESSION['user_id']=$user_id;
$_SESSION['user_name']=$user_name;
//$type1=$this->session->userdata('user_type_id');
 $type1 = $session_data['user_type_id'];
 $brand_access=$session_data['brand_access'];
	if($type1==3)
	{
		$job_title="Project Manager";
	}
	else
	{
		$job_title="Branch Manager";
	}
}
else
{
$user_id = '';
$user_name = ''; 
$_SESSION['user_id']=$user_id;
$_SESSION['user_name']=$user_name;
}
$wc_brief_sql = "SELECT * FROM `wc_brief` where brief_id='$brief_id'";
								$wc_brief_query = $this->db->query($wc_brief_sql);

								$wc_brief_result=$wc_brief_query->result_array();
								//print_r($wc_brief_result);
								// $wc_brief_qrydata = mysqli_fetch_array($wc_brief_query);
								$brief_title=$wc_brief_result[0]['brief_title'];
								//echo $brief_title." (ID: ".$brief_id.")";


?>

<?php $data['page_title'] = "upload image";

$this->load->view('front/includes/header',$data);
$this->load->view('front/includes/topnav');
 //echo $brief_id;
 ?>
 <style>
 /* styles for slideshow */
* {box-sizing: border-box}
.mySlides {display: none}
img {vertical-align: middle;}

/* Slideshow container */
.slideshow-container {
  max-width: 100%;
  position: relative;
  margin: auto;
}

/* Next & previous buttons */
.prev, .next {
  cursor: pointer;
  position: absolute;
  top: 50%;
  width: auto;
  padding: 16px;
  margin-top: -22px;
  color: white;
  font-weight: bold;
  font-size: 18px;
  transition: 0.6s ease;
  border-radius: 0 3px 3px 0;
  user-select: none;
}

/* Position the "next button" to the right */
.next {
  right: 0;
  border-radius: 3px 0 0 3px;
}

/* On hover, add a black background color with a little bit see-through */
.prev:hover, .next:hover {
  background-color: rgba(0,0,0,0.8);
}

/* Caption text */
.cortext {
  color: red;
  font-size: 15px;
  padding: 8px 12px;
  position: absolute;
  bottom: 0px;
  width: 100%;
  text-align: center;
  background-color:#000;
}

/* Number text (1/3 etc) */
.numbertext {
  color: red;
  font-size: 15px;
  padding: 8px 12px;
  position: absolute;
  top: 0;
}

/* On smaller screens, decrease text size */
@media only screen and (max-width: 300px) {
  .prev, .next,.text {font-size: 11px}
}
</style>

<style>
/* styles for multiple checkbox */
input[type="checkbox"][id^="myCheckbox"] {
  display: none;
}

label {
  border: 1px solid #fff;
  padding: 10px;
  display: block;
  position: relative;
  margin: 10px;
  cursor: pointer;
}

label:before {
  background-color: #ccc;
  color: white;
  content: " ";
  display: block;
  border-radius: 50%;
  border: 1px solid grey;
  position: absolute;
  top: -5px;
  left: -5px;
  width: 25px;
  height: 25px;
  text-align: center;
  line-height: 28px;
  transition-duration: 0.4s;
  transform: scale(0);
}

label img {
//  height: 100px;
  //width: 100px;
  transition-duration: 0.2s;
  transform-origin: 50% 50%;
}

:checked + label {
  border-color: #000;
  background-color:#ccc;
}

:checked + label:before {
  content: "✓";
  background-color: grey;
  transform: scale(1);
}

:checked + label img {
  transform: scale(0.9);  
  z-index: -2;
}
</style>

		
<div class="container-fluid">
	<div class="row">

		<!-- Main Body -->
		<div class="col-12 col-sm-12 col-md-12">
			<main class="p-3">

				<div class="row">
					<div class="col-12 col-sm-12 col-md-12">
						<div class="p-3">
							<p class="text-wc lead" onclick="goDashboard();"  style="cursor: pointer;"><i class="fas fa-arrow-alt-circle-left"></i>&nbsp;Back to Dashboard</p>
						</div>
					</div>
				</div>

				<div class="row">
					<div class="col-12 col-sm-12 col-md-12">
						<div class="p-3 text-wc">
							<h3 class="h3 text-uppercase">
								<?php echo $brief_title; ?> (ID: <?php echo $brief_id; ?>) - Project Manager
							</h3>
						</div>
						
						<div class="p-3 text-wc d-flex justify-content-start">
							<div class="p-1">
							<input type="hidden" name="brief_id" id="brief_id" value="<?php echo $brief_id; ?>">
								<select class="custom-select" onchange="listfiles(this.value)">
									<option value="" >Status</option>
									<option value="0">Waiting for Approval</option>
									<option value="2">Rejected</option>
									<option value="1">Approved</option>
									<option value="3">Reviewed</option>
								</select>
							</div>

							<div class="p-1">
								<select class="custom-select" onchange="changefilestatus(this.value)" id="changestatus" >
									<option selected value='' >Select Option</option>
									<!--<option value="1" >Approve Selected Images</option>
									<option value="2" >Reject Selected Images</option>-->
									<?php
									if($user_type_id!='4')
									{
									?>
									<option value="5">Delete Selected Images</option>
									<?php

									}
									?>
									
									<option value="4">Download Selected Images</option>
								</select>
							</div>

							<div class="p-1">
								<?php
									if($user_type_id!='4')
									{
									?>
							<input type="button" value="Upload" onclick="uploadimages()" class="btn btn-outline-wc">

								<select class="custom-select" onchange="uploadimages()" style="display:none">
									<option selected>Upload</option>
									<option value="u1">Upload 1</option>
									<option value="u2">Upload 2</option>
									<option value="u3">Upload 3</option>
								</select>
								<?php

									}
									?>
							</div>

							<div class="p-1" style="display:none">
								<select class="custom-select">
									<option selected>Delete</option>
									<option value="del1">Delete 1</option>
									<option value="del2">Delete 2</option>
									<option value="del3">Delete 3</option>
								</select>
							</div>
							
							<div class="p-1">
								<button type="button" class="btn btn-outline-wc" id="btnPreview">&emsp;Preview&emsp;</button>
							</div>
							
						</div>
					</div>
				</div>

				<div class="row">
					<div class="col-12 col-sm-12 col-md-12">
						<div class="p-4">

							<div  id="myResponse">
							<?php 
							//$checkquerys = $this->db->query("select * from wc_users where  user_id='$user_id' ")->result_array();
							$checkquerys = $this->db->query("SELECT * FROM `wc_brief` WHERE `brief_id` ='$brief_id' ")->result_array();
                            $brand_id=$checkquerys[0]['brand_id'];
							$checkquerys = $this->db->query("select wc_brands.*,wc_clients.client_name from wc_brands inner join wc_clients on wc_brands.client_id=wc_clients.client_id where  wc_brands.brand_id='$brand_id'")->result_array();
										$brand_id=$checkquerys[0]['brand_id'];
										$brand_name=str_replace(" ","_",$checkquerys[0]['brand_name']);
										$client_id=$checkquerys[0]['client_id'];
										$client_name=str_replace(" ","_",$checkquerys[0]['client_name']);
										//$dst = "upload/".$client_id."-".$client_name."/".$brand_id."-".$brand_name."/".$brief_id."-brief";  
										$client_folder=$client_id."-".$client_name;
										$brand_folder=$brand_id."-".$brand_name;
										$brief_folder=$brief_id."-brief";
									
									    $brief_path="../upload/".$client_folder."/".$brand_folder."/".$brief_folder;
									
							
							//$wc_brief_sql = "SELECT * FROM `wc_image_upload` where brief_id='$brief_id' and parent_img='0'";
							$wc_brief_sql = "SELECT * FROM `wc_image_upload` where brief_id='$brief_id' and image_id=parent_img";
							//$wc_brief_sql = "SELECT * FROM `wc_image_upload` where brief_id='$brief_id'";
								$wc_brief_query = $this->db->query($wc_brief_sql);

								$wc_brief_result=$wc_brief_query->result_array();
								/*echo "<span style='float:left;width:100%;text-align:center;color:red;'>".sizeof($wc_brief_result). " results found<br></span>";*/

								if(empty($wc_brief_result))
								{
									echo '<span class="lead">No Uploaded Image Found. You need to upload image.</span>';
								}


								foreach($wc_brief_result as $keybill => $briefimages)
								{
									 $image_id=$briefimages['image_id'];
									 $status=$briefimages['img_status'];
									 $revision_id=$briefimages['version_num'];
									 $image_id=$parent_img=$briefimages['parent_img'];
									 
									 $rev_folder="Revision".$revision_id;
									 $image=$brief_path."/".$image_id."/".$rev_folder."/thumb_".$briefimages['image_path'];
									 $image_org=$brief_path."/".$image_id."/".$rev_folder."/".$briefimages['image_path'];
									$ext = pathinfo($image_org, PATHINFO_EXTENSION);
									
									
							//$check_latest_status = $this->db->query("SELECT * FROM `wc_image_upload` WHERE `parent_img` = '$image_id' ORDER BY `wc_image_upload`.`image_id` DESC limit 1")->result_array();
                            //$check_latest_status=$check_latest_status[0]['img_status'];
							//$stats=$check_latest_status;

										 
									/* if wants to display as groupby
									$wc_image_sql = "SELECT * FROM `wc_image_upload` WHERE `parent_img` = '$image_id' ORDER BY `wc_image_upload`.`version_num` DESC LIMIT 1";
									 $wc_image_query = $this->db->query($wc_image_sql);

								$wc_image_result=$wc_image_query->result_array();
								$sub_count=count($wc_image_result);
								if($sub_count>0)
								{
									 $image="../brief_upload/".$brief_id."/image/thumb/thumb_".$wc_image_result[0]['image_path'];
									 $status=$wc_image_result[0]['img_status'];
								}
								*/

								?>
								<div class="row">
								<div class="col-12 col-sm-12 col-md-3 p-2">
									<div class="card">
										<!--<img src="/brief_upload/Bried_id/image/<?php //echo $briefimages['image_path'] ?>" class="card-img-top" alt="<?php //echo $briefimages['img_title'] ?>">-->
										<input type="checkbox" id="myCheckbox<?php echo $briefimages['image_id']; ?>" name="listimages[]" value="<?php echo $briefimages['image_id']; ?>" />
										<label for="myCheckbox<?php echo $briefimages['image_id']; ?>">
										
										<!--<embed src="<?php //echo $image; ?>"   class="card-img-top" autostart="false"  />-->
										<img src="<?php echo $image; ?>" class="card-img-top" alt="<?php echo $briefimages['img_title']; ?>">
								
										</label>
										<div class="card-body">
											<div class="d-flex justify-content-between">
												<span class="lead"><?php echo $rev_folder; //$briefimages['img_title']; ?></span>
                                             <?php if($status==2) { ?>
												<i class="fas fa-circle text-danger fa-2x"></i>
											 <?php } elseif($status==3) { ?>
												
												<i class="fas fa-sync-alt fa-2x"></i>
											<?php } elseif($status==1) { ?>
												<i class="fas fa-circle text-wc fa-2x"></i>
											<?php } ?>
											</div>
										</div>
									</div>
								</div>
								
								
								<!-- Slider Modal -->
		<div class="modal fade" id="sliderModal<?php echo $briefimages['image_id']; ?>" tabindex="-1" role="dialog" aria-labelledby="sliderModalLabel" aria-hidden="true">
			<div class="modal-dialog" role="document">
				<div class="modal-content">
				<div class="modal-header">
						<h5 class="modal-title" id="sliderModalLabel"></h5>
						<button type="button" class="close" data-dismiss="modal" aria-label="Close">
							<span aria-hidden="true">&times;</span>
						</button>
					</div>
				<div class="modal-header">				
<div class="slideshow-container">
<?php  $cor_sql="SELECT * FROM `wc_image_upload` where brief_id='$brief_id' and parent_img='$image_id'"; 
								$cor_sql = $this->db->query($cor_sql);
								$wcor_sql_result=$cor_sql->result_array();
								$tot=sizeof($wcor_sql_result);
								$ic=1;
								foreach($wcor_sql_result as $keybill => $cor_sqlimages)
								{
									 $revision_id_cor=$cor_sqlimages['version_num'];
									 $image_id_cor=$parent_img_cor=$cor_sqlimages['parent_img'];
									 $rev_folder_cor="Revision".$revision_id_cor;
									 $image_org=$brief_path."/".$image_id_cor."/".$rev_folder_cor."/".$cor_sqlimages['image_path'];
									$ext = pathinfo($image_org, PATHINFO_EXTENSION);
?>
    <div class="mySlides<?php echo $image_id; ?>">
  <div class="numbertext"><?php echo $ic."  /  ".$tot; ?></div>
  <?php if($ext=='gif' || $ext=='png' || $ext=='bmp' || $ext=='jpg' || $ext=='jpeg'){ ?>
  <img src="<?php echo $image_org; ?>" style="width:100%">
  <?php } else { ?>
  <video id="video_<?php echo $briefimages['image_id']; ?>" width="100%" height="100%" controls>
							  <source src="<?php echo $image_org; ?>" type="video/mp4">
							Your browser does not support the video tag.
							</video>
  <?php } ?>
  <div class="cortext"><?php echo $rev_folder_cor; ?></div>
</div><?php $ic++; } ?>
  <a class="prev" onclick="plusSlides(-1,<?php echo $image_id; ?>)">&#10094;</a>
  <a class="next" onclick="plusSlides(1,<?php echo $image_id; ?>)">&#10095;</a>
</div>
			</div>	
					<!--
					<?php if($ext=='gif' || $ext=='png' || $ext=='bmp' || $ext=='jpg' || $ext=='jpeg'){ ?>
					<div class="modal-header">
						<h5 class="modal-title" id="sliderModalLabel"></h5>
						<button type="button" class="close" data-dismiss="modal" aria-label="Close">
							<span aria-hidden="true">&times;</span>
						</button>
					</div>
					<img src="<?php echo $image_org; ?>" class="card-img-top" alt="<?php echo $briefimages['img_title']; ?>">
					<?php } else { ?>
					<div class="modal-header">
						<h5 class="modal-title" id="sliderModalLabel"></h5>
						<button type="button" class="close" data-dismiss="modal" aria-label="Close" onclick="checkvideo('video_<?php echo $briefimages['image_id']; ?>')">
							<span aria-hidden="true">&times;</span>
						</button>
					</div>
					<!--<embed src="<?php //echo $image_org; ?>"   class="card-img-top" autostart="false"  />
					
							 <video id="video_<?php echo $briefimages['image_id']; ?>" width="100%" height="100%" controls>
							  <source src="<?php echo $image_org; ?>" type="video/mp4">
							
							Your browser does not support the video tag.
							</video>-->
					
					
					
					<?php } ?>
					<div id="sliderModalBody" class="modal-body"></div>
					<!-- <div class="modal-footer">
						<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
						<button type="button" class="btn btn-primary">Save changes</button>
					</div> -->
				</div>
			</div>
		</div>
								
								
								
								
								<!-- Pranav new code 
								<div class="card" id="upld_id_<?php //echo $briefimages['image_id']; ?>" onclick="slct(<?php //echo $briefimages['image_id']; ?>)">
										<div class="img-div" style="background-image: url(<?php //echo $image; ?>);"></div>
										<div class="card-body">
											<div class="d-flex justify-content-between">
												<span class="lead"><?php //echo $briefimages['img_title']; ?></span>

												<i class="fas fa-circle text-danger fa-2x"></i>
											</div>
										</div>
									</div>
								-->
								<!-- start of sub images code	-->
								<?php
								$wc_brief_sub_sql = "SELECT * FROM `wc_image_upload` where brief_id='$brief_id' and parent_img='$image_id' and parent_img!=image_id";
								$wc_brief_sub_sql = $this->db->query($wc_brief_sub_sql);

								$wc_brief_sub_sql_result=$wc_brief_sub_sql->result_array();
								foreach($wc_brief_sub_sql_result as $keybill => $briefsubimages)
								{
									 $image_sub_id=$briefsubimages['image_id'];
									 $status=$briefsubimages['img_status'];
									 $revision_id=$briefsubimages['version_num'];
									 $image_id=$parent_img=$briefsubimages['parent_img'];
									 
									 $rev_folder="Revision".$revision_id;
									 $image=$brief_path."/".$image_id."/".$rev_folder."/thumb_".$briefsubimages['image_path'];
									 $image_org=$brief_path."/".$image_id."/".$rev_folder."/".$briefsubimages['image_path'];
									$ext = pathinfo($image_org, PATHINFO_EXTENSION);
																
								?>
								<div class="col-12 col-sm-12 col-md-3 p-2">
									<div class="card">
										<input type="checkbox" id="myCheckbox<?php echo $briefsubimages['image_id']; ?>" name="listimages[]" value="<?php echo $briefsubimages['image_id']; ?>" />
										<label for="myCheckbox<?php echo $briefsubimages['image_id']; ?>">
										<img src="<?php echo $image; ?>" class="card-img-top" alt="<?php echo $briefsubimages['img_title']; ?>">
								</label>
										<div class="card-body">
											<div class="d-flex justify-content-between">
												<span class="lead"><?php echo $rev_folder; //$briefsubimages['img_title']; ?></span>
                                             <?php if($status==2) { ?>
												<i class="fas fa-circle text-danger fa-2x"></i>
											 <?php } elseif($status==3) { ?>
												
												<i class="fas fa-sync-alt fa-2x"></i>
											<?php } elseif($status==1) { ?>
												<i class="fas fa-circle text-wc fa-2x"></i>
											<?php } ?>
											</div>
										</div>
									</div>
								</div>
								<!-- Slider Modal -->
		<div class="modal fade" id="sliderModal<?php echo $briefsubimages['image_id']; ?>" tabindex="-1" role="dialog" aria-labelledby="sliderModalLabel" aria-hidden="true">
			<div class="modal-dialog" role="document">
				<div class="modal-content">
					
					<?php if($ext=='gif' || $ext=='png' || $ext=='bmp' || $ext=='jpg' || $ext=='jpeg'){ ?>
					<div class="modal-header">
						<h5 class="modal-title" id="sliderModalLabel"></h5>
						<button type="button" class="close" data-dismiss="modal" aria-label="Close">
							<span aria-hidden="true">&times;</span>
						</button>
					</div>
					<img src="<?php echo $image_org; ?>" class="card-img-top" alt="<?php echo $briefsubimages['img_title']; ?>">
					<?php } else { ?>
					<div class="modal-header">
						<h5 class="modal-title" id="sliderModalLabel"></h5>
						<button type="button" class="close" data-dismiss="modal" aria-label="Close" onclick="checkvideo('video_<?php echo $briefsubimages['image_id']; ?>')">
							<span aria-hidden="true">&times;</span>
						</button>
					</div>
					<!--<embed src="<?php //echo $image_org; ?>"   class="card-img-top" autostart="false"  />-->
					
							 <video id="video_<?php echo $briefsubimages['image_id']; ?>" width="100%" height="100%" controls>
							  <source src="<?php echo $image_org; ?>" type="video/mp4">
							
							Your browser does not support the video tag.
							</video>
					<?php } ?>
					<div id="sliderModalBody" class="modal-body"></div>
					</div>
			</div>
		</div>
								<?php } ?> 
		<!-- end of Sub images code -->
								
								
								</div>
								<?php } ?>



<!--
								<div class="col-12 col-sm-12 col-md-3 p-2">
									<div class="card">
										<img src="..." class="card-img-top" alt="...">
										<div class="card-body">
											<div class="d-flex justify-content-between">
												<span class="lead">Image Name</span>

												<i class="fas fa-sync-alt fa-2x"></i>
											</div>
										</div>
									</div>
								</div>

								<div class="col-12 col-sm-12 col-md-3 p-2">
									<div class="card">
										<img src="..." class="card-img-top" alt="...">
										<div class="card-body">
											<div class="d-flex justify-content-between">
												<span class="lead">Image Name</span>

												<i class="fas fa-circle text-wc fa-2x"></i>
											</div>
										</div>
									</div>
								</div>

								<div class="col-12 col-sm-12 col-md-3 p-2">
									<div class="card">
										<img src="..." class="card-img-top" alt="...">
										<div class="card-body">
											<div class="d-flex justify-content-between">
												<span class="lead">Image Name</span>

												<i class="fas fa-circle text-wc fa-2x"></i>
											</div>
										</div>
									</div>
								</div>

								<div class="col-12 col-sm-12 col-md-3 p-2">
									<div class="card">
										<img src="..." class="card-img-top" alt="...">
										<div class="card-body">
											<div class="d-flex justify-content-between">
												<span class="lead">Image Name</span>

												<i class="fas fa-circle text-danger fa-2x"></i>
											</div>
										</div>
									</div>
								</div>

								<div class="col-12 col-sm-12 col-md-3 p-2">
									<div class="card">
										<img src="..." class="card-img-top" alt="...">
										<div class="card-body">
											<div class="d-flex justify-content-between">
												<span class="lead">Image Name</span>

												<i class="fas fa-sync-alt fa-2x"></i>
											</div>
										</div>
									</div>
								</div>

								<div class="col-12 col-sm-12 col-md-3 p-2">
									<div class="card">
										<img src="..." class="card-img-top" alt="...">
										<div class="card-body">
											<div class="d-flex justify-content-between">
												<span class="lead">Image Name</span>

												<i class="fas fa-circle text-wc fa-2x"></i>
											</div>
										</div>
									</div>
								</div>
-->
							</div>
						</div>
					</div>
				</div>
			</main>
		</div>

	</div>
</div>
<script>
function listfiles(status) {

		//alert(status);  
	   
		var brief_id = $("#brief_id").val()
        $.ajax({
            type: "POST",
            url: "<?php echo base_url();?>front/view_files/getfilessort", 
            dataType: 'html',
            data: 'brief_id='+brief_id+'&status='+status,
            success: function(data){
				//alert(data);
                $('#myResponse').html(data)
               
            }
        });
		
        return false;

        }


function changefilestatus1(status)
{
	//alert(status);
}


$(document).ready(function() {
        $("#changestatus").change(function(){
			var status=$("#changestatus").val()
			var brief_id = $("#brief_id").val()
            var favorite = [];
            $.each($("input[name='listimages[]']:checked"), function(){
                favorite.push($(this).val());
            });
			var selected_files=favorite.join(",");
			if(favorite.length==0)
			{
				alert("pls select files");
			}
			else{
            $.ajax({
            type: "POST",
            url: "<?php echo base_url();?>front/view_files/changefilestatus", 
            dataType: 'html',
            data: 'image_id='+selected_files+'&status='+status+'&brief_id='+brief_id,
            success: function(data){
                $('#myResponse').html(data)
				if(status==4)
		{
		//return downloadzip(brief_id);
		$.ajax({
            type: "POST",
            url: "<?php echo base_url();?>front/view_files/getzipfile", 
            dataType: 'html',
            data: 'brief_id='+brief_id,
            success: function(data){
				//alert(data);
				 var url="<?php echo base_url(); ?>"+data;
				window.location = url;
				return removezip(brief_id);
            }
        });
        }
               
            }
        });
		
		return false;
			
		
			
			
			}
			
			
        });
    });

function downloadzip(brief_id)
{
	$.ajax({
            type: "POST",
            url: "<?php echo base_url();?>front/view_files/getzipfile", 
            dataType: 'html',
            data: 'brief_id='+brief_id,
            success: function(data){
				 var url="<?php echo base_url(); ?>"+data;
				window.location = url;
				return removezip(brief_id);
            }
        });
		
}
function removezip(brief_id)
{
	$.ajax({
            type: "POST",
            url: "<?php echo base_url();?>front/view_files/removezipfile", 
            dataType: 'html',
            data: 'brief_id='+brief_id,
            success: function(data){
				 var url="<?php echo base_url(); ?>view-files/"+brief_id;
				window.location = url;
				//return raj();
				//alert('done');
            }
        });
}

function uploadimages()
{
	//alert('hi');
		url="<?php echo base_url();?>uploadimages/"+<?php echo $brief_id; ?>;
	window.location = url;
}




function getbranddetails(brand_id)
{
	alert(brand_id);
}

$(document).ready(function() {
		$('#btnPreview').on('click', function() {
			var favorite = [];
            $.each($("input[name='listimages[]']:checked"), function(){
                favorite.push($(this).val());
            });
			var selected_files=favorite.join(",");
			if(favorite.length!=1)
			{
				alert("pls select only one file to Preview");
			}
			else{
			
			var modelname="#sliderModal"+selected_files;
			//alert(selected_files);
			
			$(modelname).modal('show');
            var slideIndex = 1;
			showSlides(slideIndex,selected_files);
			$('#sliderModelLabel').text('Page tile goes here');

			$('#sliderModelBody').text('carousel code goes here');
			}
		});
	});

function checkvideo(a)
{
	var myVideo = document.getElementById(a); 
    myVideo.pause(); 

}



var slideIndex = 1;
//showSlides(slideIndex);

function plusSlides(n,m) {
  showSlides(slideIndex += n,m);
}

function currentSlide(n,m) {
  showSlides(slideIndex = n,m);
}

function showSlides(n,m) {
  var i;
  var slides = document.getElementsByClassName("mySlides"+m);
  if (n > slides.length) {slideIndex = 1}    
  if (n < 1) {slideIndex = slides.length}
  for (i = 0; i < slides.length; i++) {
      slides[i].style.display = "none";  
  }
  slides[slideIndex-1].style.display = "block";  
}	
</script>

<?php
$this->load->view('front/includes/footer');
?>
