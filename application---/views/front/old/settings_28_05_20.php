<?php
session_start();
error_reporting(0);

if ($this->session->userdata('front_logged_in')) {
	$session_data = $this->session->userdata('front_logged_in');
	$user_id = $session_data['user_id'];
	$user_name = $session_data['user_name'];
	$_SESSION['user_id']=$user_id;
	$_SESSION['user_name']=$user_name;
	$type1 = $session_data['user_type_id'];
	$user_type_id=$type1;
	$brand_access=$session_data['brand_access'];
 
	$checkquerys = $this->db->query("select user_type_name from wc_users_type where user_type_id='$user_type_id' ")->result_array();
	$job_title=$checkquerys[0]['user_type_name'];
	$client_access=$session_data['client_access'];
}
else
{
	$user_id = '';
	$user_name = ''; 
	$_SESSION['user_id']=$user_id;
	$_SESSION['user_name']=$user_name;
}
?>

<?php $data['page_title'] = "dashboard";

$this->load->view('front/includes/header',$data);
$this->load->view('front/includes/topnav');
?>
<style type="text/css">
	.errmsg{
		color:red;
	}
</style>
<div class="container-fluid">
	<div class="row">

		<!-- Main Body -->
		<div class="col-12 col-sm-12 col-md-12">
			<main class="p-3">

				<!-- <div class="row">
					<div class="col-12 col-sm-12 col-md-12">
						<div class="px-3">
							<a href="<?php echo base_url() ?>dashboard"><p class="text-wc lead"><i class="fas fa-arrow-alt-circle-left"></i>&nbsp;Back to Dashboard</p></a>
						</div>
					</div>
				</div> -->

				<div class="row">
					<div class="col-12 col-sm-12 col-md-12">
						<div class="px-3 text-wc">
							<h2>Settings</h2>
						</div>
					</div>
				</div>

				<!-- <hr class="text-center w-50 "> -->

				<div class="row">
					<div class="col-12 col-sm-12 col-md-12">
						<div class="p-3">
							<ul class="nav nav-tabs" id="myTab" role="tablist">
								<li class="nav-item">
									<a class="nav-link text-wc text-uppercase active" id="general-tab" data-toggle="tab" href="#general" role="tab" aria-controls="general" aria-selected="true">General</a>
								</li>
								<li class="nav-item">
									<a class="nav-link text-wc text-uppercase" id="items-tab" data-toggle="tab" href="#items" role="tab" aria-controls="items" aria-selected="false">Items</a>
								</li>
							</ul>
							<div class="tab-content" id="myTabContent">

								<div class="tab-pane fade show active" id="general" role="tabpanel" aria-labelledby="general-tab">
									<div class="p-4">
										<div class="custom-control custom-checkbox">
											<input type="checkbox" class="custom-control-input" id="chkActivateLink">
											<label class="custom-control-label roundCheck" for="chkActivateLink">Activate Link Sharing</label>
										</div>
									</div>
									<hr>

									<style type="text/css">
										.tox-statusbar__branding { display: none; }
									</style>

									<div class="p-4">
										<p class="h4 mb-4">Project Description</p>
										<?php 
										$checkquerys = $this->db->query("select * from wc_clients where client_id='$client_access' ")->result_array();
										
									    $project_description=$checkquerys[0]['project_description'];
									    ?>
										<div class="form-group">
											<textarea id="project_description" name="project_description"><?php echo $project_description ?></textarea>
											<span class="small text-danger" id="errmsg_project_description"></span>
											<input type="hidden" id="client_id" name="client_id" value="<?php echo $client_access ?>">
										</div>
										<button type="button" class="btn btn-wc" id="update_project_desc">Submit</button>
									</div>
								</div>

								<div class="tab-pane fade" id="items" role="tabpanel" aria-labelledby="items-tab">
									<div class="p-4">
										<div>
											<p class="text-wc lead" data-toggle="modal" data-target="#addModel"><i class="far fa-plus-square"></i>&nbsp;Add Item</p>
										</div>
										<div class="table-responsive" id="product_details_table">
											<?php $this->load->view('front/settings_list', $product_details);   ?>
												
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</main>
		</div>

	</div>
</div>
<div class="modal fade" id="addModel" tabindex="-1" role="dialog" aria-labelledby="addModelLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
    
      <div class="modal-header">
        <h5 class="modal-title" id="addModelLabel">Add Product Details</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        
          <div class="form-group">
            <label for="product_name" class="col-form-label">Product Name</label>
            <input type="text" class="form-control" id="product_name" name="product_name" required >
            <span class="small text-danger" id="errmsg_product_name"></span>
          </div>
          <div class="form-group">
            <label for="product_description" class="col-form-label">Product Description</label>
            <textarea class="form-control" id="product_description" name="product_description" required ></textarea>
            <span class="small text-danger" id="errmsg_product_description"></span>
          </div>
          <div class="form-group">
            <label for="product_price" class="col-form-label">Product Price</label>
            <input type="text" class="form-control" id="product_price" name="product_price" required >
            <span class="small text-danger" id="errmsg_product_price"></span>
          </div>

       
      </div>
      <div class="modal-footer">
      	<button type="button" class="btn btn-outline-wc" id="add_product">&emsp;Add&emsp;</button>
        <button type="button" class="btn btn-outline-wc" data-dismiss="modal">&emsp;Close&emsp;</button>
        
      </div>
       
    </div>
  </div>
</div>
<script type="text/javascript">
	
	$("#product_price").keypress(function(e) {
		//console.log("hai");
		if (((e.which != 46 || (e.which == 46 && $(this).val() == '')) ||$(this).val().indexOf('.') != 0) && (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57))) {
			$("#errmsg_product_price").html("Digits Only").show().fadeOut(1500);
			return false;
		}
	}).on('paste', function(e) {
		$("#errmsg_product_price").html("Type the value").show().fadeOut(1500);
		return false;
	});
	$("#txtQty_<?php echo $brief_id ?>").on("input", function() {
		if (/^0/.test(this.value)) {
			this.value = this.value.replace(/^0/, "");
			$("#errmsg_product_price").html("Invalid").show().fadeOut(1500);
		}

	}); 
	
	$('#update_project_desc').click(function(){
		var project_description = encodeURIComponent(CKEDITOR.instances['project_description'].getData());
		var client_id=$("#client_id").val();
		//alert(project_description);

		if(project_description!=""){
			$("#loader").show();
			$.ajax({
		    type: "POST",
		    url: "<?php echo base_url();?>front/Settings/update_project_desc", 
		    dataType: 'html',
		    data: 'client_id='+client_id+"&project_description="+project_description,
		    success: function(data){
		     	$("#loader").hide();
				if(data=="1"){
					//alert(data);
					location.reload();
				}
		  	}
		  });
		}
		else{
			if(project_description==""){
				$("#errmsg_project_description").html("Enter the Some Content").show().fadeOut(1500);
			}
			
		}
	});
	$('#add_product').click(function(){
		var product_name=$("#product_name").val();
		var product_description=$("#product_description").val();
		var product_price = $("#product_price").val();
		//alert(product_name+product_description+product_price);
		if(product_name!="" &&product_description!=""&& product_price!=""){
			$("#loader").show();
			$.ajax({
		    type: "POST",
		    url: "<?php echo base_url();?>front/Settings/add_product", 
		    dataType: 'html',
		    data: 'product_name='+product_name+"&product_description="+product_description+"&product_price="+product_price,
		    success: function(data){
				$("#loader").hide();
				$('#product_details_table').html('');
				$('#product_details_table').html(data);
				$('#addModel').modal('hide');
				$("#product_name").val("");
				$("#product_description").val("");
				$("#product_price").val("");
		  	}
		  });
		}
		else{
			if(product_name==""){
				$("#errmsg_product_name").html("Enter the value").show().fadeOut(1500);
			}
			if(product_description==""){
				$("#errmsg_product_description").html("Enter the Value").show().fadeOut(1500);
			}
			if(product_price==""){
				$("#errmsg_product_price").html("Enter the Value").show().fadeOut(1500);
			}
		}
	})
	function edit_product(product_id){
		//alert(product_id);
		var product_name=$("#product_name_"+product_id).val();
		var product_description=$("#product_description_"+product_id).val();
		var product_price = $("#product_price_"+product_id).val();
		if(product_name!="" &&product_description!=""&& product_price!=""){
			$("#loader").show();
			$.ajax({
		    type: "POST",
		    url: "<?php echo base_url();?>front/Settings/update_product", 
		    data: 'product_id='+product_id+"&product_name="+product_name+"&product_description="+product_description+"&product_price="+product_price,
		    success: function(data){
		    	$('#product_details_table').html("");
				$('#product_details_table').html(data);
				// $('#editModel'+product_id).modal('hide');
				// $("#product_name"+product_id).val("");
				// $("#product_description"+product_id).val("");
				// $("#product_price"+product_id).val("");
			    $("#loader").hide();
			    //alert(data);
				
		  	}
		  });
		}
		else{
			if(product_name==""){
				$("#errmsg_product_name"+product_id).html("Enter the value").show().fadeOut(1500);
			}
			if(product_description==""){
				$("#errmsg_product_description"+product_id).html("Enter the Value").show().fadeOut(1500);
			}
			if(product_price==""){
				$("#errmsg_product_price"+product_id).html("Enter the Value").show().fadeOut(1500);
			}
		}
		
	}
	function delete_product(product_id){
    $("#loader").show();
	  $.ajax({
      type: "POST",
      url: "<?php echo base_url();?>front/Settings/delete_product", 
      data: 'product_id='+product_id,
      success: function(data){
      	$('#product_details_table').html("");
        $('#product_details_table').html(data);
        //$('#deleteModel'+product_id).modal('hide');
        $("#loader").hide();
        
    	}
		});
	}

</script>
<script src="//cdn.ckeditor.com/4.13.0/full/ckeditor.js"></script>
<script>
$(document).ready(function() {
	CKEDITOR.replace('project_description', {
		allowedContent:true,
	});
});
</script>
<?php $this->load->view('front/includes/footer'); ?>
