<?php
session_start();
error_reporting(0);
$data['page_title'] = "upload image";

$this->load->view('front/includes/header',$data);


// $this->load->view('front/includes/menu');
$this->load->view('front/includes/nav');


if ($this->session->userdata('front_logged_in')) {
$session_data = $this->session->userdata('front_logged_in');
$user_id = $session_data['user_id'];
$user_name = $session_data['user_name'];
$_SESSION['user_id']=$user_id;
$_SESSION['user_name']=$user_name;
$type1=$this->session->userdata('type1');

}
else
{
$user_id = '';
$user_name = ''; 
$_SESSION['user_id']=$user_id;
$_SESSION['user_name']=$user_name;
}
?>
<!-----------poulami start-------------->
<!-- <link rel="stylesheet" href="<?php echo base_url();?>/website-assets/folderTree/css/filetree.css" type="text/css" >
-->
<!-----------poulami end-------------->

<div class="container-fluid">
	<div class="row">

		<?php
		$this->load->view('front/includes/sidebar');

		?>
		<div class="col-12 col-sm-12 col-md-9 col-lg-9 col-xl-9 border mainscroll" style="min-height: 29rem">
			<table class="table">
				<thead>
					<tr>
						<th scope="col">ID</th>
						<th scope="col">Title</th>
						<th scope="col">Brief</th>
						<th scope="col">Due Date</th>
						<th scope="col">Status</th>

					</tr>
				</thead>
				<tbody>
					<tr>
						<th scope="row">5453</th>
						<td>Bio Oil Banners 'for March <br><input type="submit" class="btn btn-piquic " value="view Files"></td>
						<td>
							<span  class="checkbox-checkmark"></span>
						</td>
						<td>4 Aug 2020 <br>14:00</td>
						<td>Proofing Pending <br><input type="submit" class="btn btn-piquic " class="btn btn-piquic " value="open Proofing"></td>
					</tr>
					<tr>
						<th scope="row">5453</th>
						<td>Bio Oil Banners for March</td>
						<td><span  class="checkbox-checkmark-red"></span></td>
						<td>4 Aug 2020 <br>14:00</td>
						<td>Brief Rejected</td>
						<tr>
							<td colspan="5" style="border: none;">
							<div class="form-row">
								<div class="form-group col-6">
									<label for="notes" class="lblNotes">&nbsp;NOTES&nbsp; (Optional)</label><label for="notes" class="form-control reason" ></label>
								</div>
								<div class="form-group col-3 px-4">
									<button type="submit" class="btn btn-outline-piquic mb-2 btn-circle btn-xl" ><i class="fas fa-download"></i></button><br>
									<span>Download Rejected Brief</span>
									</div>
									<div class="form-group col-3 px-4">
										<button type="submit" class="btn btn-outline-piquic mb-2 btn-circle btn-xl"><i class="fas fa-upload"></i></button><br>
										<span>Re-Upload Brief</span>
								</div>
							</div>
							</td>
						</tr>
					</tr>
					<tr>
						<th scope="row">5453</th>
						<td>Bio Oil Banners for March <br><input type="submit" class="btn btn-piquic " value="view Files"></td>
						<td><span  class="checkbox-checkmark"></span></td>
						<td>4 Aug 2020 <br>14:00</td>
						<td>Feedback Pending <br><input type="submit" class="btn btn-piquic " value="Add Feedback"></td>
					</tr>
					<tr>
						<th scope="row">5453</th>
						<td>Bio Oil Banners for March</td>
						<td><span > <i class="fas fa-sync"></i></span></td>
						<td>4 Aug 2020 <br>14:00</td>
						<td>Brief in Review</td>
					</tr>
					<tr>
						<th scope="row">5453</th>
						<td>Bio Oil Banners for March</td>
						<td><span  class="checkbox-checkmark"></span></td>
						<td>4 Aug 2020 <br>14:00</td>
						<td>Work in Progresss</td>
					</tr>
					<tr>
						<th scope="row">5453</th>
						<td>Bio Oil Banners for March <br><input type="submit" class="btn btn-piquic " value="view Files"></td>
						<td><span  class="checkbox-checkmark"></span></td>
						<td>4 Aug 2020 <br>14:00</td>
						<td>Work Completed</td>
					</tr>
					<tr>
						<th scope="row">5453</th>
						<td>Bio Oil Banners for March<br><input type="submit" class="btn btn-piquic " value="view Files"></td>
						<td><span  class="checkbox-checkmark"></span></td>
						<td>4 Aug 2020 <br>14:00</td>
						<td>Revision Work<br><input type="submit" class="btn btn-piquic " value="Open Proofing"></td>
					</tr>

				</tbody>
			</table>
	
	</div>
</div>
</div>


<?php
$this->load->view('front/includes/footer');
?>


<script src='<?php echo base_url();?>/website-assets/js/dragdropfolder.js'></script>
