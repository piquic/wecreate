<?php
session_start();
error_reporting(0);
if ($this->session->userdata('front_logged_in')) {
$session_data = $this->session->userdata('front_logged_in');
$user_id = $session_data['user_id'];
$user_name = $session_data['user_name'];
$user_type_id = $session_data['user_type_id'];
$_SESSION['user_id']=$user_id;
$_SESSION['user_name']=$user_name;


$user_type_id = $session_data['user_type_id'];

$checkquerys = $this->db->query("select user_type_name from wc_users_type where  user_type_id='$user_type_id' ")->result_array();
$job_title=$checkquerys[0]['user_type_name'];
//$type1=$this->session->userdata('user_type_id');
$type1 = $session_data['user_type_id'];
$brand_access=$session_data['brand_access'];
if($type1==3)
{
	$job_title="Project Manager";
} 
else
{
	$job_title="Branch Manager"; 
}
}
else
{
$user_id = '';
$user_name = ''; 
$_SESSION['user_id']=$user_id;
$_SESSION['user_name']=$user_name;
}
$wc_brief_sql = "SELECT * FROM `wc_brief` where brief_id='$brief_id'";
$wc_brief_query = $this->db->query($wc_brief_sql);

$wc_brief_result=$wc_brief_query->result_array();
						//print_r($wc_brief_result);
						// $wc_brief_qrydata = mysqli_fetch_array($wc_brief_query);
$brief_title=$wc_brief_result[0]['brief_title'];
$status=$wc_brief_result[0]['status'];
						//echo $brief_title." (ID: ".$brief_id.")";


?>

<?php $data['page_title'] = "upload image";

$this->load->view('front/includes/header',$data);
$this->load->view('front/includes/topnav');
//echo $brief_id;
?>



<div class="container-fluid">
<div class="row">

	<!-- Main Body -->
	<div class="col-12 col-sm-12 col-md-12">
		<main class="p-3">

			<div class="row">
				<div class="col-12 col-sm-12 col-md-12">
					<div class="p-3">
						<p class="text-wc lead" onclick="goDashboard();"  style="cursor: pointer;"><i class="fas fa-arrow-alt-circle-left"></i>&nbsp;Back to Dashboard</p>
					</div>
				</div>
			</div>

			<div class="row">
				<div class="col-12 col-sm-12 col-md-12">
					<div class="p-3 text-wc">
						<h3 class="h3 text-uppercase">
							<?php echo $brief_title; ?> (ID: <?php echo $brief_id; ?>) - <?php echo $job_title;?>
						</h3>
					</div>

					<div class="p-3 text-wc d-flex justify-content-start">
						<div class="p-1">
							<input type="hidden" name="brief_id" id="brief_id" value="<?php echo $brief_id; ?>">
							<select class="custom-select" onchange="listfiles(this.value)">
								<option value="all" >Status</option>
								<option value="0">Waiting for Approval</option>
								<option value="1">Approved</option>
								<!--<option value="2">Reviewed</option>-->
								<option value="2">Under Revision</option>
							</select>
						</div>

						<div class="p-1">
							<select class="custom-select" id="changestatus" >
								<option selected value='' >Select Option</option>
							<!--<option value="1" >Approve Selected Images</option>
								<option value="2" >Reject Selected Images</option>-->
								<?php
								if($user_type_id!='4'  && $status!='7'){
									?>
									<option value="5">Delete Selected Images</option>
									<?php
								}
								?>
								<option value="4">Download Selected Images</option>
							</select>
						</div>

						<div class="p-1">
							<?php
							if($user_type_id!='4')
							{
								?>
								<input type="button" value="Upload" onclick="uploadimages()" class="btn btn-outline-wc">

								<select class="custom-select" onchange="uploadimages()" style="display:none">
									<option selected>Upload</option>
									<option value="u1">Upload 1</option>
									<option value="u2">Upload 2</option>
									<option value="u3">Upload 3</option>
								</select>
								<?php

							}
							?>
						</div>

						<div class="p-1" style="display:none">
							<select class="custom-select">
								<option selected>Delete</option>
								<option value="del1">Delete 1</option>
								<option value="del2">Delete 2</option>
								<option value="del3">Delete 3</option>
							</select>
						</div>

						<div class="p-1">
							<button type="button" class="btn btn-outline-wc" id="btnPreview">&emsp;Preview&emsp;</button>
						</div>

					</div>
				</div>
			</div>

			<div class="row">
				<div class="col-12 col-sm-12 col-md-12">
					<div class="p-4">

						<div class="row" id="myResponse">
							<?php // print_r($dashboradinfo);
     				//print_r($showLimit);	
     				$this->load->view('front/viewfiles_search', $viewfilesinfo);   ?>

					</div>
				</div>
			</div>
		</div>
	</main>
</div>

</div>
</div>
<div class="modal fade" id="modal_alert_<?php  echo $brief_id; ?>" tabindex="-1" role="dialog" aria-hidden="true">
<div class="modal-dialog" role="document">
	<div class="modal-content">
		<div class="modal-header">
			<h5 class="modal-title text-dark" id="modMsg"></h5>
			<button type="button" class="close text-danger" data-dismiss="modal" aria-label="Close">
				<span aria-hidden="true" >&times;</span>
			</button>
		</div>
		<div class="modal-footer">
			<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
			<button type="button" id="hideDel" class="btn btn-danger" onclick="delete_image();" data-dismiss="modal">Delete</button>
		</div>
	</div>
</div>
</div>

<script>
function listfiles(status) {

//alert(status);  

var brief_id = $("#brief_id").val()
$.ajax({
	type: "POST",
	url: "<?php echo base_url();?>front/view_files/getfilessort", 
	dataType: 'html',
	data: 'brief_id='+brief_id+'&status='+status,
	success: function(data){
		//alert(data);
		$('#myResponse').html(data)

	}
});

return false;

}
function delete_image(){
	var favorite = [];
	$.each($("input[name='listimages[]']:checked"), function(){
		favorite.push($(this).val());
	});
	var selected_files=favorite.join(",");
	//alert(selected_files);
	var status='5';
	var brief_id = $("#brief_id").val();
	//alert(selected_files+"-"+status+"-"+brief_id);
	$.ajax({
		type: "POST",
		url: "<?php echo base_url();?>front/view_files/changefilestatus", 
		dataType: 'html',
		data: 'image_id='+selected_files+'&status='+status+'&brief_id='+brief_id,
		success: function(data){
			$("#changestatus").prop("selectedIndex", 0);
			$('#myResponse').html("");
		  $('#myResponse').html(data);
			//alert(data);
			//var url="<?php echo base_url(); ?>view-files/"+brief_id;
			//window.location = url;
		}
	});
}

$(document).ready(function() {
$("#changestatus").change(function(){
	var favorite = [];
	$.each($("input[name='listimages[]']:checked"), function(){
		favorite.push($(this).val());
	});
	var selected_files=favorite.join(",");
	if(favorite.length==0){
	// alert("pls select files");
	$('#modal_msg').modal('show');
	$('#txtMsg').html('Please Select Files');
	$("#changestatus").prop("selectedIndex", 0);
}
else{
	var status=$("#changestatus").val()
	var brief_id = $("#brief_id").val()
	if(status==4){
		$.ajax({
			type: "POST",
			url: "<?php echo base_url();?>front/view_files/changefilestatus", 
			dataType: 'html',
			data: 'image_id='+selected_files+'&status='+status+'&brief_id='+brief_id,
			success: function(data){
                //$('#myResponse').html(data)
				///alert(data);
				$("#changestatus").prop("selectedIndex", 0);
				
					$('.card').removeClass('border-wc');
					$('.card-body').removeClass('bg-wc');
				// }

				var url="<?php echo base_url(); ?>"+data;
				window.location = url;
				
			}
		});
	}
	else if(status==5){
    	//var success = confirm('Are you sure you want to Delete this File?');
    	$('#modal_alert_' + brief_id).modal('show');
    	$('#modMsg').html('Are you sure you want to delete?');
    }
}			
});
});
function myFunction(item, index) {
//document.getElementById("demo").innerHTML += index + ":" + item + "<br>";
//alert(item);
// var sel_id=""+item;
slct(item);
}


function removezip_lat(brief_id)
{
var url="<?php echo base_url(); ?>view-files/"+brief_id;
window.location = url;
}


function uploadimages()
{
//alert('hi');
url="<?php echo base_url();?>uploadimages/"+<?php echo $brief_id; ?>;
window.location = url;
}




function getbranddetails(brand_id)
{
// alert(brand_id);
}

$(document).ready(function() {
$('#btnPreview').on('click', function() {
	var favorite = [];
	$.each($("input[name='listimages[]']:checked"), function(){
		favorite.push($(this).val());
	});
	var selected_files=favorite.join(",");
	if(favorite.length!=1)
	{
		// alert("pls select only one file to Preview");
		$('#modal_msg').modal('show');
		$('#txtMsg').html('Please select only one file to preview.');
	}
	else{

		var modelname="#sliderModal"+selected_files;
	//alert(selected_files);
	
	$(modelname).modal('show');
	var slideIndex = 1;
	showSlides(slideIndex,selected_files);
	// $('#sliderModelLabel').text('Page tile goes here');

	// $('#sliderModelBody').text('carousel code goes here');
}
});
});

function checkvideo(a)
{
var myVideo = document.getElementById(a); 
myVideo.pause(); 

}



var slideIndex = 1;
//showSlides(slideIndex);

function plusSlides(n,m) {
showSlides(slideIndex += n,m);
}

function currentSlide(n,m) {
showSlides(slideIndex = n,m);
}

function showSlides(n,m) {
var i;
var slides = document.getElementsByClassName("mySlides"+m);
if (n > slides.length) {slideIndex = 1}    
	if (n < 1) {slideIndex = slides.length}
		for (i = 0; i < slides.length; i++) {
			slides[i].style.display = "none";  
		}
		slides[slideIndex-1].style.display = "block";  
	}	



	function slct(id) {
//alert(id);
if($('#upld_id_'+id+' div').hasClass('bg-wc')){
	//alert('yes');
	document.getElementById("myCheckbox"+id).checked = false;
	$('#upld_id_'+id+'').removeClass('border-wc');
	$('#upld_id_'+id+' div.card-body').removeClass('bg-wc');
} else {
	//alert('no');
	document.getElementById("myCheckbox"+id).checked = true;
	$('#upld_id_'+id+'').addClass('border-wc');
	$('#upld_id_'+id+' div.card-body').addClass('bg-wc');
}
}


</script>

<?php
$this->load->view('front/includes/footer');
?>
