
							<table id="tblbrf" class="table lead">
								<thead>
									<tr class="table-success border-bottom">
										<th scope="col">ID</th>
										<th scope="col">Title</th>
										<th scope="col">Brief</th>
										<th scope="col">Due Date</th>
										<th scope="col">Status</th>
									</tr>
								</thead>
								<tbody>
								<?php if(!empty($dashboradinfo))
						  {
							  $i=1;
							foreach($dashboradinfo as $keybill => $brief)
												{ 
												$brief_id=$brief['brief_id'];
												$brief_title=$brief['brief_title'];
												$brief_doc=$brief['brief_doc'];
												$brief_due_date=$brief['brief_due_date'];
												$brief_due_date= date('d F Y H:i', strtotime($brief_due_date));
												$status=$brief['status'];
												$brand_id=$brief['brand_id'];
												$rejected_reasons=$brief['rejected_reasons'];

								




													?>
								
									<tr <?php if($status!='4') { ?> class="border-bottom" <?php } ?> id="tr_<?php echo $brief_id;?>" >
										<th scope="row"><?php echo $brief_id;?></th>
										<td><?php echo $brief_title;?>
										<?php
										if($status!='0' && $status!='4')
										{
										?>
										<br><br><a class="btn btn-outline-wc" href="<?php echo base_url();?>view-files/<?php echo $brief_id; ?>">View Files</a>
										<?php
										}
										?>
									    </td>


										<td>
										<?php
										if($status=='0' )
										{
										?>
										<i class="fas fa-sync icon-md px-1"></i>
										<?php
										}
										else if($status=='4')
										{
										?>
										<span class="sts-ico-f">PP</span>
										<?php
										}
										else 
										{
										?>
										<span class="sts-ico-s">PP</span>
										<?php
										}
										?>	

											

										</td>
										<td>
										<?php
										if($status=='0' )
										{
										echo "N/A";
										}
										else
										{
											echo $brief_due_date;
										}
										?>

											
												

											</td>
										<td>

										<?php
										if($status=='0' )
										{
										?>
										Brief in Review
									<?php
									$accept_reject_permission=$this->Dashboard_model->checkPermission($user_type_id,3,'accept_reject');
									if($accept_reject_permission=='1')
									{
									?>


										<br><br>
											<select class="custom-select mb-3" name="brief_status" id="brief_status<?php echo $brief_id;?>" onChange="changeBriefStatus('<?php echo $brief_id;?>')" >
												<option value="1" selected>Approve</option>
												<option value="4">Reject</option>
											</select><br><br>

											<div class="brief_review_approve_<?php echo $brief_id;?>">

											Delivery Date:

											<div class="input-group mb-2 mr-sm-2 " >
												<!-- <div class="input-group-prepend">
													<div class="input-group-text"><i class="far fa-calendar-alt"></i></div>
												</div> -->
												<!-- <input type="text" class="form-control datePick" name="datePick" id="datePick<?php echo $brief_id;?>" placeholder="00-00-0000 00:00"> -->


												<div class='input-group date' id='datetimepicker<?php echo $brief_id;?>'>
												<input type='text' class="form-control" name="brief_due_date" id="brief_due_date<?php echo $brief_id;?>"  />
												<span class="input-group-addon">
												<span class="glyphicon glyphicon-calendar"></span>
												</span>
												</div>
												<script type="text/javascript">
												$(function () {
												$('#datetimepicker<?php echo $brief_id;?>').datetimepicker();
												});
												</script>
												
											</div>




											  <div class="px-3 py-5 text-center text-wc " <?php if($status=='0') {  ?> style="display: none;" <?php } ?>>
													<a class="text-wc" href="<?php echo base_url();?>brief_upload/<?php echo $brief_id;?>/<?php echo $brief_doc;?>"><span class="m-3 px-3 py-4 border rounded-circle"><i class="fas fa-download fa-2x"></i></span><br><br>Download Rejected<br> Brief</a>
												</div>



											<div class="row pt-3">
												<div class="col-12 col-sm-12 col-md-4"></div>
												<div class="col-12 col-sm-12 col-md-4"><button type="submit" class="btn btn-outline-danger ">Cancel</button></div>
												<div class="col-12 col-sm-12 col-md-4"><button type="submit" class="btn btn-outline-wc " id="<?php echo $brief_id;?>" onclick="submitCorfirm(<?php echo $brief_id;?>);">Confirm</button></div>
											</div>




											</div>



										<?php
										}
										?>

										<?php
										}
										else if($status=='1')
										{
										?>
										Work in Progress
										<?php
										}
										else if($status=='2')
										{
										?>
										Work Completed
										<?php
										}
										else if($status=='3')
										{
										?>
										Revision Work

										<?php
										$proofing_permission=$this->Dashboard_model->checkPermission($user_type_id,4,'view');
										if($proofing_permission=='1')
										{
										?>

										<br><br><a class="btn btn-outline-wc" href="">Open Proofing</a>
										<?php
										}?>


										<?php
										}
										else if($status=='4')
										{
										?>
										Brief Rejected
										<?php
										}
										else if($status=='5')
										{
										?>
										Feedback Pending
										<?php
										if($user_type_id=='4')
										{
										?>

										<br><br><a class="btn btn-outline-wc" href="">Add Feedback</a>
										<?php
										}?>
										<?php
										}
										else if($status=='6')
										{
										?>
										Proofing Pending


										<?php
										$proofing_permission=$this->Dashboard_model->checkPermission($user_type_id,4,'view');
										if($proofing_permission=='1')
										{
										?>

										<br><br><a class="btn btn-outline-wc" href="<?php echo base_url() ?>open-proofing\<?php echo $brief_id;?>">Open Proofing</a>
										<?php
										}?>
										<?php
										}
										else 
										{
										?>
										
										<?php
										}
										?>	

										

										



										</td>
									</tr>

									
									<?php
										if($status=='0' || $status=='4')
										{
										?>
										<tr class="border-bottom <?php if($status=='0') {  ?> brief_review_reject_<?php echo $brief_id;?>  <?php } ?>"  <?php if($status=='0') {  ?> style="display: none;" <?php } ?> >										
										<th class="border-0" scope="row"></th>
										<td class="border-0" colspan="4">
											<div class="d-flex justify-content-around flex-wrap">
												<div class="mt-3 w-50 text-justify">

													
													

											<?php
											$accept_reject_permission=$this->Dashboard_model->checkPermission($user_type_id,3,'accept_reject');
											if($accept_reject_permission=='1' )
											{
											?>

										<div class="border px-3 mt-3 text-justify">
										<span class="b-text bg-white p-2 ">REASON FOR REJECTION</span><br>
										<input type="text" name="rejected_reasons" id="rejected_reasons<?php echo $brief_id;?>" class="form-control mb-3 border-0" value="<?php echo $rejected_reasons;?>">
										</div>

										<?php
										if($rejected_reasons=='')
													{
										?>

											<div class="row pt-3">
											<div class="col-12 col-sm-12 col-md-4"></div>
											<div class="col-12 col-sm-12 col-md-4"><button type="submit" class="btn btn-outline-danger ">Cancel</button></div>
											<div class="col-12 col-sm-12 col-md-4"><button type="submit" class="btn btn-outline-wc " onclick="submitCorfirm(<?php echo $brief_id;?>);">Confirm</button></div>
											</div>

											<?php
										}
										
											}

											else
											{


													if($rejected_reasons!='')
													{
														?>
										<div class="border px-3 pb-3">
										<span class="b-text bg-white p-2">REASON FOR REJECTION</span><br>
										<span><?php echo $rejected_reasons;?></span>
										</div>
														<?php

													}
													

											}
											?>

												</div>

												<div class="px-3 py-5 text-center text-wc">
													<a class="text-wc" href="<?php echo base_url();?>brief_upload/<?php echo $brief_id;?>/<?php echo $brief_doc;?>"><span class="m-3 px-3 py-4 border rounded-circle"><i class="fas fa-download fa-2x"></i></span><br><br>Download Rejected<br> Brief</a>
												</div>
												<?php
											if($user_type_id=='4' )
											{
											?>

										<div class="px-3 py-5 text-center text-wc">

									<label class="w3-button w3-blue w3-round">
									<span class="m-3 px-3 py-4 border rounded-circle"><i class="fas fa-download fa-2x"></i></span>
									<input type="file" style="display: none" class="re_upload_doc fas fa-download fa-2x" id="files1" alt='1' name="files1[]" multiple val="<?php echo $brief_id;?>" />
									</label>
									<br><br>Re-Upload<br> Brief

										<!-- <a class="text-wc " href="#"><span class="m-3 px-3 py-4 border rounded-circle">


										
										

										<i class="fas fa-download fa-2x">
											

										</i></span>


										<br><br>Re-Upload<br> Brief</a> -->



												</div>
											</div>
											<?php
											}
											?>

										</td>
									</tr>
									
												<?php } ?>
									
						<?php   $i++;
						} 
						
						} 

                                 else
							{
                           ?>
                           <tr><td colspan="5" align="center">No Record Found</td></tr>

                           <?php

							}


						?>
								</tbody>
							</table>
						