<?php
session_start();
error_reporting(0);
if ($this->session->userdata('front_logged_in')) {
$session_data = $this->session->userdata('front_logged_in');
$user_id = $session_data['user_id'];
$user_name = $session_data['user_name'];
$user_type_id = $session_data['user_type_id'];
$_SESSION['user_id']=$user_id;
$_SESSION['user_name']=$user_name;


$user_type_id = $session_data['user_type_id'];

$checkquerys = $this->db->query("select user_type_name from wc_users_type where  user_type_id='$user_type_id' ")->result_array();
$job_title=$checkquerys[0]['user_type_name'];
//$type1=$this->session->userdata('user_type_id');
$type1 = $session_data['user_type_id']; 
$brand_access=$session_data['brand_access'];
if($type1==3)
{
	$job_title="Project Manager";
} 
else
{ 
	$job_title="Branch Manager"; 
}
}
else
{
$user_id = '';
$user_name = ''; 
$_SESSION['user_id']=$user_id;
$_SESSION['user_name']=$user_name;
}
$wc_brief_sql = "SELECT * FROM `wc_brief` where brief_id='$brief_id'";
$wc_brief_query = $this->db->query($wc_brief_sql);

$wc_brief_result=$wc_brief_query->result_array();
						//print_r($wc_brief_result);
						// $wc_brief_qrydata = mysqli_fetch_array($wc_brief_query);
$brief_title=$wc_brief_result[0]['brief_title'];
$status=$wc_brief_result[0]['status'];
						//echo $brief_title." (ID: ".$brief_id.")";

$cor_sql="SELECT * FROM `wc_image_upload` where brief_id='$brief_id'"; 
$cor_sql = $this->db->query($cor_sql);
$wcor_sql_result=$cor_sql->result_array();
$tot=sizeof($wcor_sql_result);
?>

<?php $data['page_title'] = "upload image";

$this->load->view('front/includes/header',$data);
$this->load->view('front/includes/topnav');
//echo $brief_id;

//code to remove old downloaded zip files from download folder
$files1 = glob("./downloads/*.{zip}", GLOB_BRACE);
foreach ($files1 as $value){ 
  //code to be executed; 
 // echo $value;
   $timestamp=substr($value,27,-4);
 $filedate=date('d-m-Y', $timestamp); 
 $today=date('d-m-Y');
if($today!=$filedate)
{
  if (!unlink($value)) {  
    //echo ("$value cannot be deleted due to an error");  
}  
else {  
    //echo ("$value has been deleted");  
}  
}
} 
//code to remove old downloaded zip files from download folder
?>



<div class="container-fluid">
<div class="row">

	<!-- Main Body -->
	<div class="col-12 col-sm-12 col-md-12">
		<main class="p-3">

			<div class="row">
				<div class="col-12 col-sm-12 col-md-12">
					<div class="p-3">
						<p class="text-wc lead" onclick="goDashboard();"  style="cursor: pointer;"><i class="fas fa-arrow-alt-circle-left"></i>&nbsp;Back to Dashboard</p>
					</div>
				</div>
			</div>

			<div class="row">
				<div class="col-12 col-sm-12 col-md-12">
					<div class="p-3 text-wc">
						<h3 class="h3 text-uppercase">
							<?php echo $brief_title; ?> (ID: <?php echo $brief_id; ?>) - <?php echo $job_title;?>
						</h3>
					</div>

					<div class="p-3 text-wc d-flex justify-content-between">
					<div class="p-3 text-wc d-flex justify-content-start">

						<div class="p-1">
							<input type="hidden" name="brief_id" id="brief_id" value="<?php echo $brief_id; ?>">
							<select class="custom-select" onchange="listfiles(this.value)">
								<option value="" >Status</option>
								<option value="0">Waiting for Approval</option>
								<option value="1">Approved</option>
								<!--<option value="2">Reviewed</option>-->
								<option value="2">Under Revision</option>
								<option value="5">Send For QC</option>
							</select>
						</div>
						<?php if($tot>0) { ?>
						<div class="p-1">
							<button type="button" class="btn btn-outline-wc" id="btnPreview123" onclick="return downloadall(<?php echo $brief_id; ?>)">&emsp;Download All Files&emsp;</button>
						</div>
						<?php } ?>
						<div class="p-1 d-none">
							<select class="custom-select" id="changestatus" >
								<option selected value='' >Select Option</option>
							<!--<option value="1" >Approve Selected Images</option>
								<option value="2" >Reject Selected Images</option>-->
								<?php
								if($user_type_id!='4'  && $status!='7'){
									?>
									<option value="5">Delete Selected Images</option>
									<?php
								}
								?>
								<option value="4">Download Selected Images</option>
							</select>
						</div>

						<div class="p-1 d-none">
							<?php
							if($user_type_id!='4')
							{
								?>
								<input type="button" value="Upload" onclick="uploadimages()" class="btn btn-outline-wc">

								<select class="custom-select" onchange="uploadimages()" style="display:none">
									<option selected>Upload</option>
									<option value="u1">Upload 1</option>
									<option value="u2">Upload 2</option>
									<option value="u3">Upload 3</option>
								</select>
								<?php

							}
							?>
						</div>

										

						<div class="p-1 d-none">
							<button type="button" class="btn btn-outline-wc" id="btnPreview">&emsp;Preview&emsp;</button>
						</div>
						</div>
					<?php if(($type1==3)||($type1==6))
					{

						if($status=='1'|| $status=='6')

						{ ?>
						<div class="pt-2 mr-4 mt-1">
								<a class="text-wc lead" data-toggle="collapse" role="button" href="#upld_file_id" aria-expanded="false" aria-controls="upld_file_id"><i class="fas fa-plus-square"></i>&nbsp;ADD NEW FILES</a>

								<div id="upld_file_id" class="collapse border rounded-lg pt-3 px-3 bg-white" style="width: 25rem; top: 90%; right: 5%; position: absolute; z-index: 1113;">
									<div class="d-flex justify-content-around">
										<p class="flex-grow-1 text-center font-weight-bold">Add New Files</p>
										<span data-toggle="collapse" data-target="#upld_file_id" aria-expanded="false" aria-controls="upld_file_id"><i class="fas fa-times"></i></span>
									</div>

									<hr class="border p-0 mt-0">

									<div class="bg-light" style="min-height: 12.8rem; margin: 0 0 1rem 0;">
										<form action="" method="post" enctype="multipart/form-data" id="addNewFiles<?php echo $img_id; ?>" name="addNewFiles<?php echo $img_id; ?>">
											<input type="hidden" name="img_id<?php echo $img_id; ?>" value="<?php echo $img_id; ?>">
											<input type="hidden" name="brief_id" id="brief_id" value="<?php echo $brief_id; ?>">
											<div class="custom-file">
												<!--<input type="file" class="custom-file-input custom_file_input" id="upld_img<?php echo $img_id; ?>" name="upld_img<?php //echo $img_id; ?>"  onchange="uploadFile('<?php //echo $img_id; ?>')">-->
												
												<!-- <input type="file" multiple="" accept="jpg,JPG,png,PNG,jpeg,JPEG,gif,GIF,MPG, MP2,MPEG,MPE,MPV,OGG,MP4,M4P,M4V,AVI,WMV,MOV,QT,FLV,SWF,AVCHD"  class="custom-file-input customfileinput  upload_brief_files_oo" accept="" id="upld_img" name="upld_img" onchange="uploadFile('')"> -->

												<input type='file' multiple=""  accept='image/jpg,image/jpeg,image/png,video/mp4,video/mpg,video/mpeg,video/mpe,video/mpv,video/m4p,video/m4v,video/avi,video/wmv,video/mov,video/qt,video/flv,video/swf,video/avchd' class='custom-file-input customfileinput  upload_brief_files_oo' onchange='uploadFile("")' name='upld_img'  id='upld_img'/>


												<label class="custom-file-label custom_file_label text-center" for="upld_img<?php echo $img_id; ?>"><i class="fas fa-upload fa-3x"></i><br><br>Drop the file to upload or <i><u>browse</u></i>.</label>
											</div>
										</form>
									</div>

									<div class='alert alert-danger text-center lead d-none' id='imageDanger<?php echo $img_id; ?>' role='alert'>
										<i class='far fa-thumbs-down'></i>&nbsp;<span id='imagetxtDanger<?php echo $img_id; ?>'></span>
									</div>
								</div>
							</div>
						<?php } } ?>
					</div>
					
				</div>
			</div>

			<div class="row">
				<div class="col-12 col-sm-12 col-md-12">
					<div class="p-4">

						<div class="row" id="myResponse">
							<?php // print_r($dashboradinfo);
     				//print_r($showLimit);	
     				$this->load->view('front/viewfiles_search', $viewfilesinfo);   ?>

					</div>
				</div>
			</div>
		</div>
	</main>
</div>

</div>
</div>
<div class="modal fade" id="modal_alert_<?php  echo $brief_id; ?>" tabindex="-1" role="dialog" aria-hidden="true">
<div class="modal-dialog" role="document">
	<div class="modal-content">
		<div class="modal-header">
			<h5 class="modal-title text-dark" id="modMsg"></h5>
			<button type="button" class="close text-danger" data-dismiss="modal" aria-label="Close">
				<span aria-hidden="true" >&times;</span>
			</button>
		</div>
		<div class="modal-footer">
			<button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
			<button type="button" id="hideDel" class="btn btn-danger" onclick="delete_image();" data-dismiss="modal">Delete</button>
		</div>
	</div>
</div>
</div>

<script>
function listfiles(status) {

		//alert(status);  
	   
		var brief_id = $("#brief_id").val()
        $.ajax({
            type: "POST",
            url: "<?php echo base_url();?>front/view_files/getfilessort", 
            dataType: 'html',
            data: 'brief_id='+brief_id+'&status='+status,
            success: function(data){
				//alert(data);
                $('#myResponse').html(data)
               
            }
        });
		
        return false;

        }
function delete_image(a){
	var selected_files=a;
	//alert(selected_files);
	var status='5';
	var brief_id = $("#brief_id").val();
	//alert(selected_files+"-"+status+"-"+brief_id);
	$.ajax({
        type: "POST",
        url: "<?php echo base_url();?>front/view_files/changefilestatus", 
        dataType: 'html',
        data: 'image_id='+selected_files+'&status='+status+'&brief_id='+brief_id,
        success: function(data){
   
        	var url="<?php echo base_url(); ?>view-files/"+brief_id;
			window.location = url;
    	}
	});
	
	
}
		
		
function delete_image_old(){
	var favorite = [];
    $.each($("input[name='listimages[]']:checked"), function(){
        favorite.push($(this).val());
    });
	var selected_files=favorite.join(",");
	//alert(selected_files);
	var status='5';
	var brief_id = $("#brief_id").val();
	//alert(selected_files+"-"+status+"-"+brief_id);
	$.ajax({
        type: "POST",
        url: "<?php echo base_url();?>front/view_files/changefilestatus", 
        dataType: 'html',
        data: 'image_id='+selected_files+'&status='+status+'&brief_id='+brief_id,
        success: function(data){
   
        	var url="<?php echo base_url(); ?>view-files/"+brief_id;
			window.location = url;
    	}
	});
}


function downloadall(brief_id){
$.ajax({
		            type: "POST",
		            url: "<?php echo base_url();?>front/view_files/downloadall", 
		            dataType: 'html',
		            data: 'brief_id='+brief_id,
		            success: function(data){
						var url="<?php echo base_url(); ?>"+data;
								window.location = url;
			        }
		    	});
}

function downloadall_old(brief_id){
//alert(brief_id);
//var status=4;
$.ajax({
		            type: "POST",
		            url: "<?php echo base_url();?>front/view_files/downloadall", 
		            dataType: 'html',
		            data: 'brief_id='+brief_id,
		            success: function(data){
		                //$('#myResponse').html(data)
						alert(data);
				
						var dropDown = document.getElementById("changestatus");  
				        dropDown.selectedIndex = 0; 
						//return downloadzip(brief_id);
						var str=selected_files;
						var res = str.split(",");
						res.forEach(slct);

						$.ajax({
				            type: "POST",
				            url: "<?php echo base_url();?>front/view_files/getzipfile", 
				            dataType: 'html',
				            data: 'brief_id='+brief_id,
				            success: function(data){
								//alert(data);
								$("#changestatus").prop("selectedIndex", 0);
								var url="<?php echo base_url(); ?>"+data;
								window.location = url;
								//return removezip(brief_id);
				            }
				        });
			        }
		    	});






}


$(document).ready(function() {
    $("#changestatus").change(function(){
		var favorite = [];
        $.each($("input[name='listimages[]']:checked"), function(){
            favorite.push($(this).val());
        });
		var selected_files=favorite.join(",");
		if(favorite.length==0){
			// alert("pls select files");
			$('#modal_msg').modal('show');
			$('#txtMsg').html('Please Select Files');
			$("#changestatus").prop("selectedIndex", 0);
		}
		else{
			var status=$("#changestatus").val()
			var brief_id = $("#brief_id").val()
			if(status==4){
				$.ajax({
		            type: "POST",
		            url: "<?php echo base_url();?>front/view_files/changefilestatus", 
		            dataType: 'html',
		            data: 'image_id='+selected_files+'&status='+status+'&brief_id='+brief_id,
		            success: function(data){
		                //$('#myResponse').html(data)
						//alert(data);
				
						var dropDown = document.getElementById("changestatus");  
				        dropDown.selectedIndex = 0; 
						//return downloadzip(brief_id);
						var str=selected_files;
						var res = str.split(",");
						res.forEach(slct);

						$.ajax({
				            type: "POST",
				            url: "<?php echo base_url();?>front/view_files/getzipfile", 
				            dataType: 'html',
				            data: 'brief_id='+brief_id,
				            success: function(data){
								//alert(data);
								$("#changestatus").prop("selectedIndex", 0);
								var url="<?php echo base_url(); ?>"+data;
								window.location = url;
								//return removezip(brief_id);
				            }
				        });
			        }
		    	});
			}
			else if(status==5){
            	//var success = confirm('Are you sure you want to Delete this File?');
				$('#modal_alert_' + brief_id).modal('show');
				$('#modMsg').html('Are you sure you want to delete?');
			}
		}			
    });
});
function myFunction(item, index) {
  //document.getElementById("demo").innerHTML += index + ":" + item + "<br>";
  //alert(item);
 // var sel_id=""+item;
slct(item);
}

function downloadzip(brief_id)
{
	$.ajax({
            type: "POST",
            url: "<?php echo base_url();?>front/view_files/getzipfile", 
            dataType: 'html',
            data: 'brief_id='+brief_id,
            success: function(data){
				 var url="<?php echo base_url(); ?>"+data;
				window.location = url;
				return removezip(brief_id);
            }
        });
		
}
function removezip_lat(brief_id)
{
	var url="<?php echo base_url(); ?>view-files/"+brief_id;
				window.location = url;
}


function removezip(brief_id)
{
	$.ajax({
            type: "POST",
            url: "<?php echo base_url();?>front/view_files/removezipfile", 
            dataType: 'html',
            data: 'brief_id='+brief_id,
            success: function(data){
				 var url="<?php echo base_url(); ?>view-files/"+brief_id;
				window.location = url;
				//return raj();
				//alert('done');
            }
        });
}

function uploadimages()
{
	//alert('hi');
		url="<?php echo base_url();?>uploadimages/"+<?php echo $brief_id; ?>;
	window.location = url;
}




function getbranddetails(brand_id)
{
	// alert(brand_id);
}

$(document).ready(function() {
		$('#btnPreview').on('click', function() {
			var favorite = [];
            $.each($("input[name='listimages[]']:checked"), function(){
                favorite.push($(this).val());
            });
			var selected_files=favorite.join(",");
			if(favorite.length!=1)
			{
				// alert("pls select only one file to Preview");
				$('#modal_msg').modal('show');
				$('#txtMsg').html('Please select only one file to preview.');
			}
			else{
			
			var modelname="#sliderModal"+selected_files;
			//alert(selected_files);
			
			$(modelname).modal('show');
            var slideIndex = 1;
			showSlides(slideIndex,selected_files);
			// $('#sliderModelLabel').text('Page tile goes here');

			// $('#sliderModelBody').text('carousel code goes here');
			}
		});
	});
	
	
	function preview(image_id)
	{
		var modelname="#sliderModal"+image_id;
		var selected_files=image_id;
			//alert(modelname);
			$(modelname).modal('show');
            var slideIndex = 1;
			showSlides(slideIndex,selected_files);		
	}

function checkvideo(a)
{
	var myVideo = document.getElementById(a); 
    myVideo.pause(); 

}



var slideIndex = 1;
//showSlides(slideIndex);

function plusSlides(n,m) {
  showSlides(slideIndex += n,m);
}

function currentSlide(n,m) {
  showSlides(slideIndex = n,m);
}

function showSlides(n,m) {
  var i;
  var slides = document.getElementsByClassName("mySlides"+m);
  if (n > slides.length) {slideIndex = 1}    
  if (n < 1) {slideIndex = slides.length}
  for (i = 0; i < slides.length; i++) {
      slides[i].style.display = "none";  
  }
  slides[slideIndex-1].style.display = "block";  
}	



function slct(id) {
	//alert(id);
		if($('#upld_id_'+id+' div').hasClass('bg-wc')){
			//alert('yes');
		     document.getElementById("myCheckbox"+id).checked = false;
			$('#upld_id_'+id+'').removeClass('border-wc');
			$('#upld_id_'+id+' div.card-body').removeClass('bg-wc');
		} else {
			//alert('no');
			 document.getElementById("myCheckbox"+id).checked = true;
			$('#upld_id_'+id+'').addClass('border-wc');
			$('#upld_id_'+id+' div.card-body').addClass('bg-wc');
		}
	}




function _(el){
		return document.getElementById(el);
	}




function uploadFile(img_id){

	//alert(img_id);
	
		var allfiles = _("upld_img"+img_id).files;
		console.log(allfiles);


			var image_id =img_id;
			

		var brief_id =<?php echo $brief_id; ?>;

		for(var i=0; i<allfiles.length;i++)
		{

			//var file = _("upld_img").files[0];
			var file = allfiles[i];
			console.log(file);


			

			var file1=file.name;
			var fileExtension = file1.split(".");
			var fileType=file.type;
			//alert(fileType);
			//if(fileType === "application/x-zip-compressed")
			if(file != undefined && (file.type=='image/png' || file.type=='image/jpg'   || file.type=='image/jpeg' || file.type=='video/webm' || file.type=='video/mp4' || file.type=='video/3gpp' || file.type=='video/x-sgi-movie' || file.type=='video/x-msvideo' || file.type=='video/mpeg' || file.type=='video/x-ms-wmv' )    )
			{
			//$("#loader").show();	
			$("#circular_progress").removeClass('d-none');
			$('#upmgs').removeClass('d-none'); 
			var formdata = new FormData();

			/*formdata.append("upld_img", file);
			formdata.append("temp_folder", temp_folder);
			formData.append("brief_id", brief_id);*/

			formdata.append("image", file);
			formdata.append("brief_id", brief_id);
			formdata.append("image_num", i);
			formdata.append("image_tot_num", allfiles.length);
			if (image_id!="")
			{
			formdata.append("image_id", image_id);
			}



			var ajax = new XMLHttpRequest();
			ajax.upload.addEventListener("progress", progressHandler, false);
			ajax.addEventListener("load", completeHandler1, false);
			ajax.addEventListener("error", errorHandler, false);
			ajax.addEventListener("abort", abortHandler, false);
			ajax.open("POST", "<?php echo base_url();?>front/uploadimages/upload_files_image");
            /////////////////////////////////new code//////////////////////////////////////////////////
			var tot_len=parseInt(allfiles.length)-1;
			//alert(image_id+"----------"+i+"=="+tot_len);
      //       if(i==tot_len)
      //       {
      //       	if(file.type=='image/png' || file.type=='image/jpg'   || file.type=='image/jpeg')
      //       	{
						// ajax.timeout = 5000;
						// ajax.ontimeout = function () { 
						// //alert("Timed out!!!"); 
						// $("#loader").hide();
						// window.location.href="<?php echo base_url();?>view-files/<?php echo $brief_id; ?>";
						// }
      //       	}
				

      //       }
            //////////////////////////////////////////////////////////////////////////////////
			ajax.send(formdata);
			$("#imageDanger"+img_id).addClass("d-none");
			_("imagetxtDanger"+img_id).innerHTML = "";
			} 
			else {
			//$("#loader").hide();
			//alert('its not a zip file');
			$("#imageDanger"+img_id).removeClass("d-none");
			_("imagetxtDanger"+img_id).innerHTML = "Please Upload Image/Video File Only";
			}

		}
		
	}


	function progressHandler(event){
		var percent = (event.loaded / event.total) * 100;
		// console.log(percent);
		// $("#file_pc").attr("aria-valuenow", Math.round(percent));
		// $("#file_pc").css("width", Math.round(percent) + "%");
		$("#circular_progress_data").attr("data-value", Math.round(percent));
		$("#circular_progress_value").html(Math.round(percent) + "%");
		circular_progress();
	}

	function completeHandler1_old(event){
			
			var res = event.target.responseText;
			console.log(res);
			var arr =res.split("@@@@@");
			var cur=$.trim(arr[1]);
			var tot=$.trim(arr[2]);
			if(cur==tot)
			{

				//$("#loader").hide();
				$("#circular_progress").addClass('d-none');
				window.location.href="<?php echo base_url();?>view-files/<?php echo $brief_id; ?>";

			}



	}


		function completeHandler1(event){
			
			var res = event.target.responseText;
			//alert(res);
			console.log(res);
			var arr =res.split("@@@@@");
			var msg=$.trim(arr[0]);
			var cur=$.trim(arr[1]);
			var tot=$.trim(arr[2]);
			var checkvalid=$.trim(arr[3]);
			var img_id=$.trim(arr[4]);

			//alert(checkvalid);

			if(checkvalid=='notvalid' && tot=='1')
			{
			$("#loader").hide();
			$("#imageDanger"+img_id).removeClass("d-none");
			_("imagetxtDanger"+img_id).innerHTML = "Please Upload Proper Image/Video File Only";
			$("#circular_progress").addClass('d-none');
					

			}
			else if(checkvalid=='notvalid')
			{

				    $("#loader").hide();
					$('#modal_msg').modal('show');
					$('#txtMsg').html(msg);


					setTimeout(function(){ 


					$('#modal_msg').modal('hide');
					$('#txtMsg').html("");
					$("#loader").show();

					if(cur==tot)
					{

					$("#loader").hide();
					window.location.href="<?php echo base_url();?>view-files/<?php echo $brief_id; ?>";
					//window.location.href="<?php echo base_url();?>view-files/<?php echo $brief_id; ?>";
					//window.location.href = window.location.href

					}

						

					}, 2000);

					



					

			}
			else
			{
				if(cur==tot)
				{

				$("#loader").hide();
			window.location.href="<?php echo base_url();?>view-files/<?php echo $brief_id; ?>";
				//window.location.href="<?php echo base_url();?>view-files/<?php echo $brief_id; ?>";
				//window.location.href = window.location.href

				}

			}
			



	}







	function completeHandler(event){
		$("#zipSuccess").removeClass("d-none");
		$('#endocimg').removeClass('d-none');
		_("txtSuccess").innerHTML = event.target.responseText;
	}
	function errorHandler(event){
		$("#docDanger").removeClass("d-none");
		_("txtDanger").innerHTML = "Upload Failed";
	}
	function abortHandler(event){
		$("#docDanger").removeClass("d-none");
		_("txtDanger").innerHTML = "Upload Aborted";
	}






</script>

<?php
$this->load->view('front/includes/footer');
?>
