<?php
session_start();
error_reporting(0);

if ($this->session->userdata('front_logged_in')) {
	$session_data = $this->session->userdata('front_logged_in');
	$user_id = $session_data['user_id'];
	$user_name = $session_data['user_name'];
	$_SESSION['user_id']=$user_id;
	$_SESSION['user_name']=$user_name;
	$type1 = $session_data['user_type_id'];
	$user_type_id=$type1;
	$brand_access=$session_data['brand_access'];

	$checkquerys = $this->db->query("select user_type_name from wc_users_type where user_type_id='$user_type_id' ")->result_array();
	$job_title=$checkquerys[0]['user_type_name'];

	$checkquerys = $this->db->query("select client_id from wc_users where user_id='$user_id' ")->result_array();
	$client_id=$checkquerys[0]['client_id'];

	$checkquerys = $this->db->query("select client_name from wc_clients where client_id='$client_id' ")->result_array();
	$client_name=$checkquerys[0]['client_name'];




}
else
{
	$user_id = '';
	$user_name = ''; 
	$_SESSION['user_id']=$user_id;
	$_SESSION['user_name']=$user_name;
}
?>

<?php $data['page_title'] = "Reports";

$this->load->view('front/includes/header',$data);
$this->load->view('front/includes/topnav');
?>

<div class="bg-wc-light px-5" id="div_filter">
	<div class="row">
		<div class="col-12 col-sm-12 col-md-10">
			<div class="d-flex justify-content-start flex-wrap">

			</div>
		</div>
		<div class="col-12 col-sm-12 col-md-2">
			<div class="pt-1 my-1">
				<form>
					<input type="text" class="form-control" id="search_key" name="search_key" placeholder="Search By Title" onkeyup="">
				</form>

			</div>
		</div>
	</div>
</div>



<div class="container-fluid p-5">
	<div class="row">
		<div class="col-12 col-sm-12 col-md-12">
			<div class="p-3 border-bottom">
				<h2 class="h2">Name</h2>
			</div>
		</div>
	</div>
	<?php if($user_type_id==2){ ?>
		<div class="row">
			<div class="col-12 col-sm-12 col-md-12">
				<!--<div class="p-3 border-bottom">
					<a class="text-dark" href="<?php //echo base_url();?>loglistreport" ><span class="c-ico"><i class="fas fa-signal"></i></i></span>&nbsp;&nbsp;<span style="font-size: 1.2em; font-weight: 400;">Logs Work</span></a>
				</div>

				<div class="p-3 border-bottom">
					<a class="text-dark" href="<?php //echo base_url();?>productivityreport" ><span class="c-ico"><i class="fas fa-signal"></i></i></span>&nbsp;&nbsp;<span style="font-size: 1.2em; font-weight: 400;">Productivity Report</span></a>
				</div>-->
				<?php if($client_name!=''){ ?>

				<div class="p-3 border-bottom">
					<a class="text-dark" href="<?php //echo base_url();?>costsavingreport" ><span class="c-ico"><i class="fas fa-signal"></i></i></span>&nbsp;&nbsp;<span style="font-size: 1.2em; font-weight: 400;">Cost Saving Report</span></a>
				</div>

					<div class="p-3 border-bottom">
					<a class="text-dark" href="<?php //echo base_url();?>feedbackreport" ><span class="c-ico"><i class="fas fa-signal"></i></i></span>&nbsp;&nbsp;<span style="font-size: 1.2em; font-weight: 400;">Feedback Report</span></a>
				</div>


			<?php } ?>
			</div>
		</div>
	<?php } ?>
</div>

<?php $this->load->view('front/includes/footer'); ?>
