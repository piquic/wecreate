<?php
session_start();
error_reporting(0);

if ($this->session->userdata('front_logged_in')) {
	$session_data = $this->session->userdata('front_logged_in');
	$user_id = $session_data['user_id'];
	$user_name = $session_data['user_name'];
	$_SESSION['user_id']=$user_id;
	$_SESSION['user_name']=$user_name;
	$type1 = $session_data['user_type_id'];
	$user_type_id=$type1;
	$brand_access=$session_data['brand_access'];

	$checkquerys = $this->db->query("select user_type_name from wc_users_type where user_type_id='$user_type_id' ")->result_array();
	$job_title=$checkquerys[0]['user_type_name'];

	$checkquerys = $this->db->query("select client_id from wc_users where user_id='$user_id' ")->result_array();
	$client_id=$checkquerys[0]['client_id'];

	$checkquerys = $this->db->query("select client_name from wc_clients where client_id='$client_id' ")->result_array();
	$client_name=$checkquerys[0]['client_name'];

}
else
{
	$user_id = '';
	$user_name = ''; 
	$_SESSION['user_id']=$user_id;
	$_SESSION['user_name']=$user_name;
}
?>

<?php $data['page_title'] = "Reports";

$this->load->view('front/includes/header',$data);
$this->load->view('front/includes/topnav');
?>

<style type="text/css">


/* Extra small devices (phones, 600px and down) */
@media only screen and (max-width: 600px) {
	#iframe {
    width: 48%;
    height: 500px;
    border; 1px solid black;
    zoom: -1.00;
    -moz-transform: scale(0.80);
    -moz-transform-orgin: 0 0;
    -o-transform: scale(0.80);
    -o-transform-origin: 0 0;
    -webkit-transform: scale(0.80);
    -webkit-transform-origin: 0 0;
    }


}

/* Small devices (portrait tablets and large phones, 600px and up) */
@media only screen and (min-width: 600px) {

#iframe {
    width: 58%;
    height: 500px;
    border; 1px solid yellow;
    zoom: -1.00;
    -moz-transform: scale(0.80);
    -moz-transform-orgin: 0 0;
    -o-transform: scale(0.80);
    -o-transform-origin: 0 0;
    -webkit-transform: scale(0.80);
    -webkit-transform-origin: 0 0;
    }


}

/* Medium devices (landscape tablets, 768px and up) */
@media only screen and (min-width: 768px) {

	#iframe {
    width: 80%;
    height: 500px;
    border; 1px solid red;
    zoom: -1.00;
    -moz-transform: scale(0.80);
    -moz-transform-orgin: 0 0;
    -o-transform: scale(0.80);
    -o-transform-origin: 0 0;
    -webkit-transform: scale(0.80);
    -webkit-transform-origin: 0 0;
    }



}

/* Medium devices (landscape tablets, 768px and up) */
@media only screen and (min-width: 828px) {

	#iframe {
    width: 85%;
    height: 500px;
    border; 1px solid red;
    zoom: -1.00;
    -moz-transform: scale(0.80);
    -moz-transform-orgin: 0 0;
    -o-transform: scale(0.80);
    -o-transform-origin: 0 0;
    -webkit-transform: scale(0.80);
    -webkit-transform-origin: 0 0;
    }



}

/* Large devices (laptops/desktops, 992px and up) */
@media only screen and (min-width: 992px) {


	#iframe {
    width: 100%;
    height: 500px;
    border; 1px solid purple;
    zoom: -1.00;
    -moz-transform: scale(0.80);
    -moz-transform-orgin: 0 0;
    -o-transform: scale(0.80);
    -o-transform-origin: 0 0;
    -webkit-transform: scale(0.80);
    -webkit-transform-origin: 0 0;
    }
}

/* Extra large devices (large laptops and desktops, 1200px and up) */
@media only screen and (min-width: 1200px) {

	#iframe {
    width: 115%;
    height: 500px;
    border; 1px solid blue;
    zoom: -1.00;
    -moz-transform: scale(0.80);
    -moz-transform-orgin: 0 0;
    -o-transform: scale(0.80);
    -o-transform-origin: 0 0;
    -webkit-transform: scale(0.80);
    -webkit-transform-origin: 0 0;
    }


}

/* Extra large devices (large laptops and desktops, 1200px and up) */
@media only screen and (min-width: 1300px) {

	#iframe {
    width: 125%;
    height: 500px;
    border; 1px solid blue;
    zoom: -1.00;
    -moz-transform: scale(0.80);
    -moz-transform-orgin: 0 0;
    -o-transform: scale(0.80);
    -o-transform-origin: 0 0;
    -webkit-transform: scale(0.80);
    -webkit-transform-origin: 0 0;
    }


}


	

</style>

<div class="bg-wc-light px-5" id="div_filter">
	<div class="row">
		<div class="col-12 col-sm-12 col-md-10">
			<div class="d-flex justify-content-start flex-wrap">

			</div>
		</div>
		<div class="col-12 col-sm-12 col-md-2">
			<div class="pt-1 my-1">
				<form>
					<input type="text" class="form-control" id="search_key" name="search_key" placeholder="Search By Title" onkeyup="">
				</form>

			</div>
		</div>
	</div>
</div>

<div class="container-fluid p-5">
	<?php if($user_type_id==2){ ?>
<div class="row">

<table  style="min-width:1280px; " border="0" cellpadding="0" cellspacing="5">
<tr>
<td  align="left" nowrap="nowrap">

	<?php if($client_name!=''){ ?>
<iframe id="iframe" src="https://datastudio.google.com/embed/reporting/6af52442-d805-4b06-8537-cc1b49c02a7d/page/YmdZB?params=%7B%22df7%22:%22include%25EE%2580%25800%25EE%2580%2580EQ%25EE%2580%2580<?php echo $client_name; ?>%22%7D"  scrolling="no" frameborder="0" style="border:0" allowfullscreen></iframe>

<?php } ?>

</td>

</tr>

</table>

</div>
<?php } ?>

</div>


<!--<div class="col-12 col-sm-12 col-md-12">
				<div class="p-3 border-bottom">
					<a class="text-dark" href="http://collabtest.piquic.com/report/loglistreport.html" target="_new"><span class="c-ico"><i class="fas fa-signal"></i></i></span>&nbsp;&nbsp;<span style="font-size: 1.2em; font-weight: 400;">Logs Work</span></a>
				</div>

				<div class="p-3 border-bottom">
					<a class="text-dark" href="http://collabtest.piquic.com/report/productivityreport.html" target="_new"><span class="c-ico"><i class="fas fa-signal"></i></i></span>&nbsp;&nbsp;<span style="font-size: 1.2em; font-weight: 400;">Productivity Report</span></a>
				</div>

				<div class="p-3 border-bottom">
					<a class="text-dark" href="http://collabtest.piquic.com/report/costsavingreport.html" target="_new"><span class="c-ico"><i class="fas fa-signal"></i></i></span>&nbsp;&nbsp;<span style="font-size: 1.2em; font-weight: 400;">Cost Saving Report</span></a>
				</div>
				<div class="col-12 col-sm-12 col-md-12">
				<iframe width="100%" height="500px" src="https://datastudio.google.com/embed/reporting/c353060d-11f6-434b-845d-73cb2466ec74/page/stPYB" frameborder="0" style="border:0" allowfullscreen></iframe></div>-->


<!-- <div class="container-fluid p-5">
	<div class="row">
		<div class="col-12 col-sm-12 col-md-12">
			<div class="p-3 border-bottom">
				<h2 class="h2"></h2>
			</div>
		</div>
	</div>
	<?php //if($user_type_id==2){ ?>
		<div class="row">


			


				<?php //if($client_name!=''){ ?>
				<div class="col-12 col-sm-12 col-md-12">
				<iframe width="100%" height="500px" src="https://datastudio.google.com/embed/reporting/6af52442-d805-4b06-8537-cc1b49c02a7d/page/YmdZB?params=%7B%22df7%22:%22include%25EE%2580%25800%25EE%2580%2580EQ%25EE%2580%2580<?php //echo $client_name; ?>%22%7D" frameborder="0" style="border:0" allowfullscreen></iframe></div>
			<?php } ?>


				
			
		</div>
	<?php } ?>

	</div> -->
	
</div>

<?php $this->load->view('front/includes/footer'); ?>
