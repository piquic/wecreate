<?php
session_start();
error_reporting(0);
$this->output->disable_cache();

 if(!empty($this->session->userdata('type1')))
 {
 	$this->session->unset_userdata('type1');
 }

$data['page_title'] = "home";

$this->load->view('front/includes/header',$data);
// $this->load->view('front/includes/menu');

if ($this->session->userdata('front_logged_in')) {
	$session_data = $this->session->userdata('front_logged_in');
	$user_id = $session_data['user_id'];
	$user_name = $session_data['user_name'];
	$_SESSION['user_id']=$user_id;
	$_SESSION['user_name']=$user_name;
}
else
{
	$user_id = '';
	$user_name = ''; 
	$_SESSION['user_id']=$user_id;
	$_SESSION['user_name']=$user_name;
}
?>

<div class="homescroll">
	<div class="container">
		<div class="row mt-3 mb-2">
			<div class="col-12 col-sm-12 col-md-6 col-lg-6 col-xl-6">
				<h3>Choose your option:</h3>
			</div>
		</div>

		<div class="row">

			<div class="col-12 col-sm-12 col-md-12 col-lg-6 col-xl-6 pb-4">
				<div class="shadow">
					<div class="row">
						<div class="col-12 col-sm-5 col-md-5 col-lg-5 col-xl-5">
							<div class="overflow-hidden w-100 text-center">
								<img src="<?php echo site_url();?>assets/images/pages-img/<?php echo $pagecontents_first[0]['pimg']; ?>" alt="..." style="height: 26.25rem;">
							</div>
						</div>

						<div class="col-12 col-sm-7 col-md-7 col-lg-7 col-xl-7">
							<div class="pt-5 px-3 w-100">
								<h2 class="h2"><?php  echo $pagecontents_first[0]['page_title']; ?></h2>
								<p class="lead"><?php  echo $pagecontents_first[0]['content']; ?></p>
							</div>
						</div>
					</div>

					<div class="row">
						<div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
							<?php
							if($user_id!='')
							{
								?>
								<div class="w-100 border pt-3 pl-3 btn-cursor" id="btn_book_nextpage">
									<p class="lead"><i class="fas fa-arrow-right"></i>&nbsp;Get Started</p>
								</div>
								<?php
							}
							else
							{


								?>
								<div class="w-100 border pt-3 pl-3 btn-cursor" id="btn_book">
									<p class="lead"><i class="fas fa-arrow-right"></i>&nbsp;Get Started</p>
								</div>
								<?php
							}
							?>
						</div>
					</div>

				</div>
			</div>

			<div class="col-12 col-sm-12 col-md-12 col-lg-6 col-xl-6 pb-4">
				<div class="shadow">


					<div class="row">
						<div class="col-12 col-sm-5 col-md-5 col-lg-5 col-xl-5">
							<div class="overflow-hidden w-100 text-center">
								<img src="<?php echo site_url();?>assets/images/pages-img/<?php echo $pagecontents_second[0]['pimg']; ?>" alt="..." style="height: 26.25rem;">
							</div>
						</div>

						<div class="col-12 col-sm-7 col-md-7 col-lg-7 col-xl-7">
							<div class="pt-5 px-3 w-100">
								<h2 class="h2"><?php  echo $pagecontents_second[0]['page_title']; ?></h2>
								<p class="lead"><?php  echo $pagecontents_second[0]['content']; ?></p>
							</div>
						</div>
					</div>

					<div class="row">
						<div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
							<?php
							if($user_id!='')
							{
								?>
								<div class="w-100 border pt-3 pl-3 btn-cursor" id="btn_upld_img_nextpage">
									<p class="lead"><i class="fas fa-arrow-right"></i>&nbsp;Get Started</p>
								</div>
								<?php
							}
							else
							{


								?>
								<div class="w-100 border pt-3 pl-3 btn-cursor" id="btn_upld_img">
									<p class="lead"><i class="fas fa-arrow-right"></i>&nbsp;Get Started</p>
								</div>
								<?php
							}
							?>



						</div>
					</div>

				</div>
			</div>

		</div>
	</div>
</div>






<!-- <a href="#code" data-toggle="modal" data-target="#myModal" class="btn code-dialog">Display code</a> -->
<!-- <button type="button" class="btn btn-info btn-lg" data-toggle="modal" data-target="#myModal">Open Modal</button> -->

<!-- Modal -->
<!--   <div class="modal fade" id="myModal" role="dialog">
    <div class="modal-dialog">
    
      
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Modal Header</h4>
        </div>
        <div class="modal-body">
          <p>Some text in the modal.</p>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
      </div>
      
    </div>
 </div> -->

 <div id="myModal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="loginModalLabel" aria-hidden="true">
 	<div class="modal-dialog" role="document">
 		<div class="modal-content">
 			<!-- <div class="text-center mt-3 mb-3"></div> -->
 			<div class="modal-header">
 				<!-- <h5 class="modal-title" id="loginModalLabel">Modal title</h5> -->
 				<img class="img-fluid" src="<?php echo PIQUIC_URL;?>images/logo-piquic-sm.png">


         <button type="button" class="close" data-dismiss="modal" aria-label="Close" id="reg_close">
          <i class="far fa-times-circle"></i>
       </button>  


    </div>
    <div id="modal-tab" class="modal-header">
    	<ul class="nav nav-pills mb-3" id="pills-tab" role="tablist">
    		<li class="nav-item">
    			<a class="nav-link active" id="pills-login-tab" data-toggle="pill" href="#pills-login" role="tab" aria-controls="pills-login" aria-selected="true"><i class="fas fa-user"></i> LOGIN</a>
    		</li>
    		<li class="nav-item">
    			<a class="nav-link" id="pills-register-tab" data-toggle="pill" href="#pills-register" role="tab" aria-controls="pills-register" aria-selected="false"><i class="fas fa-user-plus"></i> REGISTER</a>
    		</li>
    	</ul>
    </div>
    <div class="modal-body">
    	<div class="tab-content" id="pills-tabContent">
    		<div class="tab-pane fade show active" id="pills-login" role="tabpanel" aria-labelledby="pills-login-tab">
    			<form class="login_form"  id="login_form" method="post">  

    				<div id="error_message" style="display:none;">
    					<div class="alert alert-danger">
    						<strong>Invalid username or password!</strong>.
    					</div>
    				</div>

    				<div id="error_message_disable" style="display:none;">
    					<div class="alert alert-danger">
    						<strong>Account is not active please contact Piquic Admin</strong>.
    					</div>
    				</div>
    				
    				<table class="table table-borderless pr-4">
    					<tr>
    						<td class="pt-3 text-right"><i class="fas fa-envelope"></i></td>  
    						<input type="hidden" class="form-control mb-2" name="type1" id="type1" >            
    						<td><input type="email" class="form-control mb-2" name="email" id="email" placeholder="Enter your email" <?php  if($activate_user_id!='') { ?>  value="<?php echo $activate_user_email;?>"  <?php } else if($cookie_remember_me=='1') { ?> value="<?php echo $cookie_user_name;?>" <?php }  ?>>
    							<?php echo form_error('email', '<div class="error" style="color:red;">', '</div>'); ?>

    						</td>
    					</tr>
    					<tr>
    						<td class="pt-3 text-right"><i class="fas fa-lock"></i></td>
    						<td><input type="password" class="form-control mb-2" name="password" id="password" placeholder="Password" <?php if($activate_user_id!='') { ?>  value=""  <?php } else if($cookie_remember_me=='1') { ?> value="<?php echo $cookie_user_password;?>" <?php } ?>>
    							<?php echo form_error('password', '<div class="error" style="color:red;">', '</div>'); ?>

    						</td>
    					</tr>


    					<tr>
							<!-- <td class="pt-3 text-right"></td> -->
							<td colspan="2" class="text-center">
								<a href="<?php echo $login_url;?>"><img src="<?php echo site_url();?>/assets/img/gmail-login.jpg" width="300"></a>
							</td>
						</tr>
						<tr>
							<!-- <td class="pt-3 text-right"></td> -->
							<td colspan="2" class="text-center">
								<a href="<?php echo $authURL; ?>"><img src="<?php echo site_url();?>/assets/img/facebook-login.jpg" width="300"></a>
							</td>
						</tr>
    					<tr>
    						<td class="pt-3 text-right"></td>
							<td>
							<label>

							<input type="checkbox" name="remember_me" id="remember_me" value="1" 
							<?php if($activate_user_id!='') { ?>   <?php } else if($cookie_remember_me=='1') { ?> checked <?php } ?> >
							Remember me 
							</label>
							</td>
    					</tr>



    					<!-- Section ID -->
    					<input type="hidden" name="section-id" id="section-id">
    					<tr>
    						<td colspan="2" class="text-center"><p><a href="#" class="text-danger" data-dismiss="modal" data-toggle="modal" data-target="#forgetModal">Forgot Password?</a></p></td>
    					</tr>
    					<tr>
    						<td colspan="2" class="text-center">
    							<button id="signin" type="submit" class="btn btn-piquic" name="signin">Login <i class="fas fa-sign-in-alt"></i></button>
    						</td>
    					</tr>
    				</table>
    			</form>
    			<span id="lsuccess" class="error" style="display:none;"></span>
    			<?php// echo form_close(); ?>
    		</div>
    		<div class="tab-pane fade" id="pills-register" role="tabpanel" aria-labelledby="pills-register-tab">
	    		<div id="success_message" style="display:none;">
	    			<div class="card-body">
						<div class=" text-center text-piquic" role="alert" style="display: table; height: 50vh; width: 100%; ">
							<div style="display: table-cell; vertical-align: middle;">
								<i class="fas fa-check-circle icon-lg"></i><br>
								<span class="h4" >Registered Successfully!! Please check your email inbox/spam folder for account activation email!!!</span>
							</div>
						</div>
					</div>
    			</div>

    			<form class="form form-horizontal has-validation-callback"  id="register_form" method="post">  
    				
    				<table class="table table-borderless">
    					<tr>
    						<td class="pt-3 text-right"><i class="fas fa-user"></i></td>
    						<td><input type="text" name="uname" id="uname" class="form-control mb-2" placeholder="Enter your name" required minlength="6"  maxlength="50">
    							<?php echo form_error('uname', '<div class="error" style="color:red;">', '</div>'); ?>

    						</td>
    					</tr>
    					<tr>
    						<td class="pt-3 text-right"><i class="fas fa-user"></i></td>
    						<td><input type="text" id="cname" name="cname" class="form-control mb-2"  placeholder="Enter your Company name" required maxlength="100">
    							<?php echo form_error('cname', '<div class="error" style="color:red;">', '</div>'); ?>

    						</td>
    					</tr>
    					<tr>
    						<td class="pt-3 text-right"><i class="fas fa-envelope"></i></td>
    						<td><input type="email" name="uemail" id="uemail" class="form-control mb-2" placeholder="Enter your email" required>
    							<?php echo form_error('uemail', '<div class="error" style="color:red;">', '</div>'); ?>

    						</td>
    					</tr>
    					<tr>
    						<td class="pt-3 text-right"><i class="fas fa-lock"></i></td>
    						<td><input type="password" class="form-control mb-2" id="upassword" name="upassword" placeholder="Set a password" required>
    							<?php echo form_error('upassword', '<div class="error" style="color:red;">', '</div>'); ?>

    						</td>
    					</tr>
    					<tr>
    						<td class="pt-3 text-right"><i class="fas fa-lock"></i></td>
    						<td><input type="password" required class="form-control mb-2" name="ure_password" id="ure_password" placeholder="Re-enter the password">
    							<?php echo form_error('ure_password', '<div class="error" style="color:red;">', '</div>'); ?>
    							<span id='message' class="error"></span>
    						</td>
    					</tr>
    					<tr>
    						<td class="pt-3 text-right"><i class="fas fa-phone"></i></td>
    						<td><input type="tel" class="form-control mb-2" name="umobile" id="umobile"  placeholder="Enter your mobile" required>
    							<?php echo form_error('umobile', '<div class="error" style="color:red;">', '</div>'); ?>

    						</td>
    					</tr>
    					<tr>
    						<td colspan="2" class="text-center">
    							<button name="regSubmit" type="submit" class="btn btn-piquic ">Register <i class="fas fa-sign-in-alt"></i></button>
    							<span id="success" class="successMsg" style="display:none;"></span>
    							<!-- <span id="loding"><i class="fas fa-circle-notch text-success fa-spin"></i></span> -->
    						</td>
    					</tr>
    				</table>
    			</form>
    			<?php// echo form_close(); ?>
    		</div>
    	</div>
    </div>
 </div>
</div>

</div>
<!-- Modal -->
<!-- FORGET PASSWORD Modal -->
<div class="modal fade" id="forgetModal" tabindex="-1" role="dialog" aria-labelledby="forgetModalLabel" aria-hidden="true">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<img class="img-fluid" src="<?php echo PIQUIC_URL;?>images/logo-piquic-sm.png">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close" id="forgotclose">
					<i class="far fa-times-circle"></i>
				</button>
			</div>
			<div id="modal-tab" class="modal-header">
				<h4 class="modal-title text-white" id="forgetModalLabel">FORGET PASSWORD</h4>
			</div>
			<div class="modal-body">
				<div id="success_message1" style="display:none;">
					<div class="card-body">
						<div class=" text-center text-piquic" role="alert" style="display: table; height: 50vh; width: 100%; ">
							<div style="display: table-cell; vertical-align: middle;">
								<i class="fas fa-check-circle icon-lg"></i><br>
								<span class="h4" >Please check your email inbox/spam folder for reset password email!!!</span>
							</div>
						</div>
					</div>
					
				</div>
				<div id="error_message1" style="display:none;">
					<div class="alert alert-danger">
						<strong>Mail id is not correct!!!</strong>.
					</div>
				</div>
				<div class="tab-content" id="pills-tabContent">
					<div class="tab-pane fade show active" id="pills-login" role="tabpanel" aria-labelledby="pills-login-tab">
						<form class="form form-horizontal has-validation-callback" id="forgetreset"  method="post">   
							<table class="table table-borderless pr-4">
								<tr>
									<td class="pt-3 text-right"><i class="fas fa-envelope"></i></td>
									<td><input type="email" class="form-control mb-2" name="femail" id="femail"  placeholder="Enter your registered email" required>
										<?php echo form_error('femail', '<div class="error" style="color:red;">', '</div>'); ?>
									</td>
								</tr>
								<tr>
									<td colspan="2" class="text-center">
										<button name="forgotSubmit" type="submit" class="btn btn-piquic">Submit <i class="fas fa-key"></i></button>
									</td>
								</tr>
							</table>
						</form>
					</div>
				</div>
			</div>
			<span id="fsuccess" class="error" style="display:none;"></span>
		</div>
	</div>
</div>
<?php
$this->load->view('front/includes/footer');
?>


<script type="text/javascript">

	// Rajedra Prasad - 20-01-20 ---------
	/*$( document ).ready(function() {
		<?php if($user_id!='' && $type1=='') { ?>

		$("#type1").val("book");
		var type1=$("#type1").val();

		$.ajax({
			method:'post',
			url: "<?php //echo base_url();?>front/home/setTypeSession", 
			data:"&type1="+type1,
			success: function(result){
				if($.trim(result)=='success') {
					//window.location.href = "<?php //echo base_url();?>home";
				}
			}
		});

		<?php } ?>
	});*/
	// -----------------------------------

	$(function () {
		<?php
		if($user_id=='')
		{
			?>
			$('#myModal').modal({
				backdrop: 'static',
				keyboard: false
			});

			<?php
		}
		?>


		$('#btn_upld_img_nextpage').on('click', function(){
			<?php
			if($user_id!='')
			{
				?>
				$("#type1").val("upload");
				var type1=$("#type1").val();

          //alert(type1);
          $.ajax({
          	method:'post',
          	url: "<?php echo base_url();?>front/home/setTypeSession", 
          	data:"&type1="+type1,
          	success: function(result){

              //alert(result);
              if($.trim(result)=='success')
              {
              	window.location.href = "<?php echo base_url();?>uploadimage";
              }

           }
        });


          
          <?php
       }
       ?>


    });
		$('#btn_book_nextpage').on('click', function(){
			<?php
			if($user_id!='')
			{
				?>
				$("#type1").val("book");
				var type1=$("#type1").val();

				$.ajax({
					method:'post',
					url: "<?php echo base_url();?>front/home/setTypeSession", 
					data:"&type1="+type1,
					success: function(result){

              //alert(result);
              if($.trim(result)=='success')
              {
              	window.location.href = "<?php echo base_url();?>booking";
              }
           }
        });



				<?php
			}
			?>
		});



		$("#login_form").validate({
			rules: {
				email:{
					required:true,
					email: true,
				},
				password: "required"
			},
			submitHandler: function (form) {
     /// alert("form")
     // form.submit();
     var email=$('#email').val();
     var password=$('#password').val();
     var type1=$('#type1').val();
     //var remember_me=$('#remember_me').val();

	if($("#remember_me").is(":checked")){
	//alert("Checkbox is checked.");
	var remember_me='1';
	}
	else if($("#remember_me").is(":not(:checked)")){
	//alert("Checkbox is unchecked.");
	var remember_me='0';
	}


     $.ajax({
     	method:'post',
     	url: "<?php echo base_url();?>front/home/check_login", 
     	data:"&email="+email+"&password="+password+"&type1="+type1+"&remember_me="+remember_me,
     	success: function(result){

            //alert($.trim(result));
            if($.trim(result)=='success')
            {
              window.location.href = "<?php echo base_url();?>home";
            }
            else if($.trim(result)=='Disable')
            {

              $("#error_message").hide();
              $("#error_message_disable").show();
              setTimeout(function(){
              	$("#error_message_disable").hide();
              }, 15000);

              
            }
            else{
              $("#error_message").show();
              setTimeout(function(){
              	$("#error_message").hide();
              }, 15000);
            }
        }
     });
  }
});
	});
	$(function () {
		$("#register_form").validate({
			rules: {
				uname:{
					required:true,
				},
				cname:{
					required:true,         
				},
				uemail:{
					required:true,
					email: true,
					<?php
					if($user_id=="")
						{ ?>
							remote: {
								url: "<?php echo base_url();?>front/home/checkexistemail",
								type: "post"
							},
						<?php }  ?>   
					},
					<?php
					if($user_name=='')
						{ ?>
							upassword:{
								required:true,
								minlength:5,
							},
							ure_password: {
								required:true,
								equalTo: "#upassword" ,
								minlength:5,
							},
						<?php  }  ?>
						umobile: {
							number: true,
							required: true,
							minlength:10,
							maxlength:10,
							<?php
							if($user_name=="")
								{ ?>
									remote: {
										url: "<?php echo base_url();?>front/home/checkexistmobile",
										type: "post"
									},
								<?php   }  ?>   
							},
						},
						messages:
						{
							uemail:
							{
							   remote:"Mail already exists."
							},
							umobile:
							{
							   remote:"Mobile Number already exists."
							}
						},
						submitHandler: function (form) {
							var user_id='';
							var uname=$('#uname').val();
							var cname=$('#cname').val();
							var uemail=$('#uemail').val();
							var umobile=$('#umobile').val();
							var upassword=$('#upassword').val();
							$("#loader").show();
							$.ajax({
								method:'post',
								url: "<?php echo base_url();?>front/home/create_user", 
								data:"&user_id="+user_id+"&uname="+uname+"&cname="+cname+"&uemail="+uemail+"&umobile="+umobile+"&upassword="+upassword,
								success: function(result){
									if(result)
									{
										$("#success_message").show();
	         //   							setTimeout(function(){
	    					// 				$("#success_message").hide();
										// }, 5000);
										$('#uname').val("");
										$('#cname').val("");
										$('#uemail').val("");
										$('#umobile').val("");
										$('#upassword').val("");
										$('#ure_password').val("");
										$('#register_form').hide();
										// $('#pills-login').addClass('active show');
										// $('#pills-login-tab').addClass('active');
										// $('#pills-register').removeClass('active show');
										// $('#pills-register-tab').removeClass('active');
										$("#loader").hide();
									}
								}
							});
						}
					});
	});
	$(function () {
		$("#forgetreset").validate({
			rules: {
				femail:{
					required:true,
					email: true,
					<?php
					if($user_id=="")
						{ ?>
							remote: {
								url: "<?php echo base_url();?>front/home/checkexistemail",
								type: "post"
							},
						<?php }  ?>   
					},
				},
				messages:
				{
					femail:
					{
					   remote:"Mail Not exists."
					},
					
				},
				submitHandler: function (form) {
      //alert('forgotpassword');
      $("#loader").show();
      var femail=$('#femail').val();

      $.ajax({
      	method:'post',
      	url: "<?php echo base_url();?>front/home/forgotpassword", 
      	data:"&femail="+femail,
      	success: function(result){
      		$("#loader").hide();
      		if(result)
      		{
      			$("#success_message1").show();
      			$('#forgetreset').hide();
      			// setTimeout(function(){
      			// 	$("#success_message1").hide();
      			// }, 5000);
      		} else {
      			$("#error_message1").show();
      			setTimeout(function(){
      				$("#error_message1").hide();
      			}, 15000);
      		}
      	}
      });
   }

});

	});


	$(document).ready(function() {
		$('#btn_upld_img').on('click', function(){
			$("#type1").val("upload");
			<?php
			if($user_id=='')
			{
				?>
          //$('#myModal').modal('show');
          $('#myModal').modal({
          	backdrop: 'static',
          	keyboard: false
          })
          <?php
       }
       ?>


    });

		$('#btn_book').on('click', function(){
			$("#type1").val("book");
			<?php
			if($user_id=='')
			{
				?>
          //$('#myModal').modal('show');
          $('#myModal').modal({
          	backdrop: 'static',
          	keyboard: false
          })
          <?php
       }
       ?>


    });
	});

/* $(window).load(function(){        
   $('#myModal').modal('show');
}); */

</script>


