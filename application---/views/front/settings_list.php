<table class="table border-bottom text-wc" >
                        <thead>
                          <tr>
                            <th scope="col"></th>
                            <th scope="col">Name</th>
                            <th scope="col">Description</th>
                            <th scope="col">Unit Price</th>
                            <th scope="col">Competitor Price</th>
                          </tr>
                        </thead>
                        <tbody >
<?php foreach($product_details as $key => $product_value){ 
                    $product_id=$product_value['product_id'];
                    $product_name=$product_value['product_name'];
                    $product_dest=$product_value['product_dest'];
                    $product_price=$product_value['product_price'];
                    $competitor_price=$product_value['competitor_price'];
                    ?>
                    <tr>
                        <th scope="row"><span id="edit_<?php echo $product_id ?>" data-toggle="modal" data-target="#editModel<?php echo $product_id; ?>"><i class="far fa-edit"></i></span> <span id="delete_<?php echo $product_id ?>" data-toggle="modal" data-target="#deleteModel<?php echo $product_id; ?>"><i class="far fa-trash-alt"></i></span></th>
                        <td><?php echo $product_name ?></td>
                        <td><?php echo $product_dest ?></td>
                        <td>Rs.<?php echo $product_price ?></td>
                        <td>Rs.<?php echo $competitor_price ?></td>
                        <div class="modal fade" id="editModel<?php echo $product_id; ?>" tabindex="-1" role="dialog" aria-labelledby="editModel<?php echo $product_id; ?>Label" aria-hidden="true">
                          <div class="modal-dialog" role="document">
                            <div class="modal-content">
                              <div class="modal-header">
                                <h5 class="modal-title" id="editModel<?php echo $product_id; ?>Label">Update Product Details</h5>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                  <span aria-hidden="true">&times;</span>
                                </button>
                              </div>
                              <div class="modal-body">
                                <form>
                                  <div class="form-group">
                                    <label for="product_name_<?php echo $product_id; ?>" class="col-form-label">Product Name</label>
                                    <input type="text" class="form-control" id="product_name_<?php echo $product_id; ?>" name="product_name_<?php echo $product_id; ?>" value="<?php echo $product_name ?>" required >
                                    <span class="small text-danger" id="errmsg_product_name<?php echo $product_id; ?>"></span>
                                  </div>
                                  <div class="form-group">
                                    <label for="product_description_<?php echo $product_id; ?>" class="col-form-label">Product Description</label>
                                    <textarea class="form-control" id="product_description_<?php echo $product_id; ?>" name="product_description_<?php echo $product_id; ?>" required ><?php echo $product_dest ?></textarea>
                                    <span class="small text-danger" id="errmsg_product_description<?php echo $product_id; ?>"></span>
                                  </div>
                                  <div class="form-group">
                                    <label for="product_price_<?php echo $product_id; ?>" class="col-form-label">Product Price</label>
                                    <input type="text" class="form-control" id="product_price_<?php echo $product_id; ?>" name="product_price_<?php echo $product_id; ?>" value="<?php echo $product_price ?>" required >
                                    <span class="small text-danger" id="errmsg_product_price<?php echo $product_id; ?>"></span>
                                    
                                  </div>

                                  <div class="form-group">
                                    <label for="product_price_<?php echo $product_id; ?>" class="col-form-label">Competitor Price</label>
                                    <input type="text" class="form-control" id="competitor_price_<?php echo $product_id; ?>" name="competitor_price_<?php echo $product_id; ?>" value="<?php echo $competitor_price ?>" required >
                                    <span class="small text-danger" id="errmsg_competitor_price<?php echo $product_id; ?>"></span>
                                    
                                  </div>

                                </form>
                              </div>
                              <div class="modal-footer">
                                <button type="submit" class="btn btn-outline-wc" id="edit_product<?php echo $product_id ?>" onclick='edit_product("<?php echo $product_id ?>")' data-dismiss="modal">&emsp;Update&emsp;</button>
                                <button type="button" class="btn btn-outline-wc" data-dismiss="modal">&emsp;Close&emsp;</button>
                                
                              </div>
                            </div>
                          </div>
                        </div>
                        <script type="text/javascript">
                            $("#product_price_"+<?php echo $product_id ?>).keypress(function (e) {
                             //if the letter is not digit then display error and don't type anything
                             if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {
                                //display error message
                                $("#errmsg_product_price"+<?php echo $product_id; ?>).html("Digits Only").show().fadeOut("slow");
                                       return false;
                            }
                           });
                        </script>
                        <div class="modal fade" id="deleteModel<?php  echo $product_id; ?>" tabindex="-1" role="dialog" aria-hidden="true">
                            <div class="modal-dialog" role="document">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <h5 class="modal-title text-dark" id="modMsg">Are you sure you want to delete?</h5>
                                        <button type="button" class="close text-danger" data-dismiss="modal" aria-label="Close">
                                            <span aria-hidden="true" >&times;</span>
                                        </button>
                                    </div>
                                    <div class="modal-footer">
                                      <button type="button" id="hideDel" class="btn btn-outline-wc" onclick="delete_product('<?php  echo $product_id; ?>');" data-dismiss="modal">&emsp;Delete&emsp;</button>
                                        <button type="button" class="btn btn-outline-wc" data-dismiss="modal">&emsp;Close&emsp;</button>
                                        
                                    </div>
                                </div>
                            </div>
                        </div>
                    </tr>
                    
<?php }
?>
</tbody>
                      </table>