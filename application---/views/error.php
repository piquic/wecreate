<script src="<?php echo base_url('theme'); ?>/assets/dist/js/pages/dashboard.js"></script>
<?php
$this->load->view('admin/includes/header');
$this->load->view('admin/includes/globalcss');
$this->load->view('admin/includes/sidebar');?>

 <div class="content-wrapper">

 <section class="content-header">
   
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#">Examples</a></li>
        <li class="active">404 error</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="error-page">
  

        <div class="error-content">
          <h3><i class="fa fa-warning text-yellow"></i> You don't have the permission to access this page.</h3>

          <p>
        Click <a href="dashboard">return to home</a>.
          </p>

        </div>
        <!-- /.error-content -->
      </div>
      <!-- /.error-page -->
    </section>
    <!-- /.content --> 
 
	
</div>
  
  <?php
  $this->load->view('admin/includes/globaljs');
  $this->load->view('admin/includes/footer');?>