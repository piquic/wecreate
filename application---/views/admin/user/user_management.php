<?php  $this->load->view('admin/includes/globalcss');?>

	<!-- Fontawesome -->
	<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>website-assets/fontawesome/css/all.css">
<style>
/*.table-bordered {
    border: 1px solid #666666;
}*/
.showdbutton {
    cursor:pointer; 
	background-color:#666666; 
	margin:10px 0px; 
	padding:10px; 
	color:#FFFFFF;
	width:100%;
}

.disabledbutton {
    pointer-events: none;
    opacity: 0.9;
	
	cursor:pointer; 
	background-color:#999999; 
	margin:10px 0px; 
	padding:10px; 
	color:#FFFFFF;
	width:100%;
}
label.error{
 color:#FF0000;
 font-size:12px;
}
#loader {
    background-color: rgba(0, 0, 0, 0.5);
    position: fixed;
    height: 100vh;
    width: 100%;
    background-size: cover;
    z-index: 1111;
    display: table;
    top: 0px;
    overflow: hidden;
}
.icon {
    display: table-cell;
    vertical-align: middle;
    text-align: center;
    font-size: 8rem;
    color: #76aea1;
}
</style>




<div>
  <!-- Section content header style="display: none;"-->
  <div id="loader" style="display: none;"><i class="fas fa-spinner fa-spin icon"></i></div>
 
  <!-- Section content -->
    <section class="content">
		<div class="col-md-12">
		  <!-- start Information Box -->
			<div class="box">
			<!-- box header style start-->
				<div class="col-md-12 box-header with-border" ><h3 class="box-title"><?php if($user_id==''){ echo $lang['Create User']; }else {echo $lang['Update User']; }?></h3></div>
					<!-- /.box-header -->
					<!-- form start -->
					
					<div id="success_message" style="display:none;">
					<div class="alert alert-success">
					<strong>Updated successfully!</strong>.
					</div>
				    </div>
					
				
				<div class="box-body">
				
				
				   <div class="col-md-12">
						<ul class="nav nav-tabs">
							<li id="li_home" class="active"><a data-toggle="tab" href="#home">User Information</a></li>
							<?php
							//if($rolecode=='1' || $rolecode=='2')
							if($user_id!='' )
							{
							?>
							<li id="li_menu1"  ><a data-toggle="tab" href="#menu1">Change Password</a></li>
							
							
                              <?php
							}
							?>
							
						</ul>
					</div>
				
				
				<div class="addmargin10">&nbsp;</div>
					<div class="tab-content">
					
				     <div id="home" class="tab-pane fade in active">
					 
					 		    <?php  $array = array('id'=>'User_form', 'role'=>'form', 'class'=>'form form-horizontal has-validation-callback');
				    echo form_open_multipart('User/insert_user', $array); ?>
					
					
					<input type='hidden' name='user_id' value='<?php echo $user_id;?>' id='user_id'>
					
					
					
					<div class="form-group">
					<label for="inputEmail3"  class="col-sm-2 control-label">User Type</label>
					 <span style="color:#F00">*</span>
					<div class="col-sm-8">
				
							<!-- class="form-control" -->
								<select name="user_type_id" id="user_type_id"  onchange="selectUserType();" style="width: 100%" >
								 <option value=''>Select</option>
								<?php


								$users_type_details=$this->db->query("select * from wc_users_type where user_type_id!='1' and deleted='0' and status='Active' ")->result_array();
								if(!empty($users_type_details))
								{
								foreach($users_type_details as $key => $userstypedetails)
								{
								$user_type_id=$userstypedetails['user_type_id'];
								$user_type_name=$userstypedetails['user_type_name'];
								?>
								<option value="<?php echo $user_type_id;?>" <?php if($user_type_id_sel==$user_type_id) { ?> selected="selected" <?php } ?>  ><?php echo $user_type_name;?></option>
								<?php

								}
								}
								?>
								
								 </select>
					
					</div>
					</div>
					
					<div id="divClientBrand" style="display:none;">
                    <input type='text' name='client_id' value='0' id='client_id'>
                    <input type='text' name='brand_id' value='0' id='brand_id'>
					</div>



					


					



					
					
					
					<div class="form-group">
					    <label for="inputEmail3"  class="col-sm-2 control-label"><?php echo $lang['user_name'];?>
                        <span style="color:#F00">*</span>
                        </label>
						<div class="col-sm-8">
						    <input  type='text'  class="form-control" id="user_name" name="user_name" value='<?php echo $user_name;?>'>
                            <?php echo form_error('user_name', '<div class="error" style="color:red;">', '</div>'); ?>
						</div>
						
					</div>
					
					
					<div class="form-group">
					    <label for="inputEmail3"  class="col-sm-2 control-label"><?php echo $lang['user_email'];?>
                        <span style="color:#F00">*</span>
                        </label>
						<div class="col-sm-8">
						    <input  type='text'  class="form-control" id="user_email" name="user_email" value='<?php echo $user_email;?>'>
                            <?php echo form_error('user_email', '<div class="error" style="color:red;">', '</div>'); ?>
						</div>
						
					</div>
					
					 <?php
				if($user_id=='')
				{
				?>
					
					<div class="form-group">
					    <label for="inputEmail3"  class="col-sm-2 control-label"><?php echo $lang['user_password'];?>
                        <span style="color:#F00">*</span>
                        </label>
						<div class="col-sm-8">
						    <input  type='text'  class="form-control" id="user_password" name="user_password" value='<?php echo $user_password;?>'>
                            <?php echo form_error('user_password', '<div class="error" style="color:red;">', '</div>'); ?>
							
						</div>
						
					</div>
					
					
					
					<!--<div class="form-group">
					<label class="col-sm-2 control-label">Password Strength</label>
					<div class="col-sm-4" id="example-progress-bar-container">
					
					</div>
					</div>-->
					
					
					
					
					
					
					<div class="form-group">
					    <label for="inputEmail3"  class="col-sm-2 control-label"><?php echo $lang['user_cpassword'];?>
                        <span style="color:#F00">*</span>
                        </label>
						<div class="col-sm-8">
						    <input  type='text'  class="form-control" id="user_cpassword" name="user_cpassword" value=''>
                            <?php echo form_error('user_cpassword', '<div class="error" style="color:red;">', '</div>'); ?>
						</div>
						
					</div>
					
					 <?php
				}
				?>
				
				<div class="form-group">
					    <label for="inputEmail3"  class="col-sm-2 control-label"><?php echo $lang['user_mobile'];?>
                        <span style="color:#F00">*</span>
                        </label>
						<div class="col-sm-8">
						    <input  type='text'  class="form-control" id="user_mobile" name="user_mobile" value='<?php echo $user_mobile;?>'>
                            <?php echo form_error('user_mobile', '<div class="error" style="color:red;">', '</div>'); ?>
						</div>
						
					</div>
					
					
					<div class="form-group">
					    <label for="inputEmail3"  class="col-sm-2 control-label">Country
                        
                        </label>
						<div class="col-sm-8">
						    
								<select name="user_country" id="user_country"  style="width: 100%"  >
								 <option value=''>Select</option>
								<?php
								$country_details=$this->db->query("select * from wc_country where 1=1 ")->result_array();
								if(!empty($country_details))
								{
								foreach($country_details as $key => $countrydetails)
								{
								$country_id=$countrydetails['country_id'];
								$country_name=$countrydetails['country_name'];
								?>
								<option value="<?php echo $country_id;?>" <?php if($user_country==$country_id) { ?> selected="selected" <?php } ?>  ><?php echo $country_name;?></option>
								<?php

								}
								}
								?>
								
								 </select>
						</div>
						
					</div>
					
					
					
					<div class="form-group">
					<label for="inputEmail3"  class="col-sm-2 control-label"><?php echo $lang['status'];?></label>
					<div class="col-sm-8">
				
							
								<select name="status" id="status" class="form-control" >
								 <!--<option value=''>Select</option>-->
								 <option value="Active" <?php if($status=='Active') { ?> selected="selected" <?php } ?>  >Active</option>
								 <option value="Inactive" <?php if($status=='Inactive') { ?> selected="selected" <?php } ?>   >Inactive</option>
								 </select>
					
					</div>
					</div>
					
					
					
					<!-- Contractor type fields end-->
					<div class="row">
					<?php if($user_id!=''){
						$button_name='Update';
					}else {
						$button_name='Save';
					} ?>
						<div class="col-md-6"><input class="btn btn-success pull-right" type="submit" value="<?php echo $button_name;?>"></div>	   
						<div class="col-md-6"><input class="btn btn-danger pull-left" type="reset"></div>
					</div>
					
					
					
					
					
					
					
					
					
					
					
					
					
					
					
					<?php echo form_close();?> 
					 
					 
					 
				    </div>
					
					
					
					
					<!--Tab Password Change-->
						<div id="menu1" class="tab-pane fade">
						<!--<div id="" class=""></div>-->
							<!-- form start -->
							
                        <?php 
				$array = array('id'=>'User_password_form', 'role'=>'form', 'class'=>'form form-horizontal has-validation-callback');
				echo form_open_multipart('admin/User/update_change_password', $array); ?>
                
                <input type="hidden" class="form-control" id="user_id" name="user_id" value="<?php echo $user_id ?>" >
                
						<div class="form-group">
							<label for="inputEmail3"  class="col-sm-2 control-label">Password</label>
							<div class="col-sm-8">
								<input type="password" class="form-control" id="user_change_password" name="user_change_password">
								<?php echo form_error('user_change_password', '<div class="error" style="color:red;">', '</div>'); ?>
							</div>
						</div>
						<div class="form-group">
							<label for="inputEmail3"  class="col-sm-2 control-label">Confirm Password</label>
							<div class="col-sm-8">
								<input type="password" class="form-control"  id="user_change_cpassword" name="user_change_cpassword">
								<?php echo form_error('user_change_cpassword', '<div class="error" style="color:red;">', '</div>'); ?>
							</div>
						</div>
						
						<div class="addmargin10">&nbsp;</div>
						
						<div class="row">
						<?php if($user_id!=''){
						$button_name='Update';
						}else {
						$button_name='Save';
						} ?>
						<div class="col-md-6"><input class="btn btn-success pull-right" type="submit" value="<?php echo $button_name;?>"></div>	   
						<div class="col-md-6"><input class="btn btn-danger pull-left" type="reset"></div>
						</div>
					
					
					
						<?php echo form_close(); ?>	
						</div>	
					
					
				    </div>
					
					
					
					
				
		
					
					
					
				</div>
					
			</div>
		</div>
		
	</section>
</div>

<?php $this->load->view('admin/includes/globaljs');?>
<link type="text/css" href="<?php echo base_url();?>assets/form-select2/select2.css" rel="stylesheet"/>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootswatch/3.3.7/lumen/bootstrap.min.css">



<script type="text/javascript" src="<?php echo base_url();?>assets/form-select2/select2.min.js"></script>



<!--<script  type="text/javascript" src="<?php echo base_url();?>assets/dist/password-score.js"></script>
<script  type="text/javascript" src="<?php echo base_url();?>assets/dist/password-score-options.js"></script>
<script  type="text/javascript" src="<?php echo base_url();?>assets/dist/bootstrap-strength-meter.js"></script>-->

 <script>

 	selectUserType();


 	function selectUserType()
 	{
 		var user_type_id=$('#user_type_id').val();
 		var client_id='<?php echo $client_id_sel;?>';
 		var brand_id='<?php echo $brand_id_sel;?>';

 		//alert(client_id);
 		//alert(brand_id);

 		if(user_type_id=="")
 		{
 			var hid_html="<input type='text' name='client_id' value='0' id='client_id'><input type='text' name='brand_id' value='0' id='brand_id'>";
					
			$('#divClientBrand').html(hid_html);
			$('#divClientBrand').hide();

 		}
		else if(user_type_id=='2' ||  user_type_id=='3' ||  user_type_id=='5')
		{
		
		 
		 


		 $.ajax({
				method:'post',
				url: "<?php echo base_url();?>admin/user/showDivClient", 
				data:"&user_type_id="+user_type_id+"&client_id="+client_id+"&brand_id="+brand_id+"&user_id="+user_id+"&user_type_id="+user_type_id,
				success: function(result){
				if(result)
				{
				 //alert(result);

				 $('#divClientBrand').html(result);

				 $('#divClientBrand').show();
				 $('#client_id').select2();
		        
				}
				}
				});



		}
		else if( user_type_id=='6')
		{
		
		 $.ajax({
				method:'post',
				url: "<?php echo base_url();?>admin/user/showDivClientMultiple", 
				data:"&user_type_id="+user_type_id+"&client_id="+client_id+"&brand_id="+brand_id+"&user_id="+user_id+"&user_type_id="+user_type_id,
				success: function(result){
				if(result)
				{
				 //alert(result);

				 $('#divClientBrand').html(result);

				 $('#divClientBrand').show();
				 $('#client_id').select2();
		        
				}
				}
				});



		}
		else if( user_type_id=='4' )
		{
		
		 

		 $.ajax({
				method:'post',
				url: "<?php echo base_url();?>admin/user/showDivClientBrand", 
				data:"&user_type_id="+user_type_id+"&client_id="+client_id+"&brand_id="+brand_id+"&user_id="+user_id+"&user_type_id="+user_type_id,
				success: function(result){
				if(result)
				{
				 //alert(result);
				 $('#divClientBrand').html(result);
				  $('#divClientBrand').show();

				    $('#client_id').select2();
					//$('#brand_id').select2();

				<?php
				if( $user_type_id_sel=='4' )
				{
				?>
				selectClient();
				<?php
				}
				?>
					
				
				}
				}
				});

		}
		
 	}



 	function selectClient()
 	{
 		var user_type_id=$('#user_type_id').val();
 		var client_id=$('#client_id').val();
 		var brand_id='<?php echo $brand_id_sel;?>';

 		//alert(client_id);
 		//alert(brand_id);

		if( user_type_id=='4' )
		{
		
		 

		 $.ajax({
				method:'post',
				url: "<?php echo base_url();?>admin/user/showDivBrand", 
				data:"&client_id="+client_id+"&brand_id="+brand_id+"&user_id="+user_id+"&user_type_id="+user_type_id,
				success: function(result){
				if(result)
				{
				//alert(result);
				  $('#brandDiv').html(result);
				  $('#brandDiv').show();

				    //$('#brand_id').select2();
					
				
				}
				}
				});

		}
		
 	}
    
    
	jQuery(function ($) {
	   // $('.content-wrapper').css('min-height','900px');
		
		//selectUserType();
		
	 $('#user_type_id').select2();
	 
		
		
		/*$('#user_password').strengthMeter('progressBar', {
            container: $('#example-progress-bar-container')
        });*/
		
		
		
	    /*$.validator.addMethod("regex", function(value, element, regexpr) {          
		 return regexpr.test(value);
		}, "Please enter a valid Mobile Number.");*/
		
		
		
		
		
		
		
		
		
		//alert("popo");
		
		
		jQuery.validator.addMethod("alphanumeric", function(value, element) {
        return this.optional(element) || /^[a-zA-Z\-\s]+$/.test(value);
        }, "It must contain only letters."); 
		
		jQuery.validator.addMethod("atLeastOneLetter", function (value, element) {
		return this.optional(element) || /[a-zA-Z]+/.test(value);
		}, "Must have at least one  letter");
		
		/*jQuery.validator.addMethod("phoneno", function(phone_number, element) {
    	    phone_number = phone_number.replace(/\s+/g, "");
    	    return this.optional(element) || phone_number.length > 9 && 
    	    phone_number.match(/^((\+[1-9]{1,4}[ \-]*)|(\([0-9]{2,3}\)[ \-]*)|([0-9]{2,4})[ \-]*)*?[0-9]{3,4}?[ \-]*[0-9]{3,4}?$/);
    	}, "<br />Please specify a valid phone number");*/
		
		/*jQuery.validator.addMethod('fnType', function(value, element) {
		return value.match(/^\+(?:[0-9] ?){6,14}[0-9]$/);
		},'Enter Valid  phone number');*/
		
		/*jQuery.validator.addMethod('fnType', function(value, element) {
		return /^\d{10}$/.test(value.replace(/[()\s+-]/g,''));
		},'Enter Valid  phone number');*/
		
		
		/*jQuery.validator.addMethod('fnType', function(value, element) {
		return /^\d{10}$/.test(value.replace(/[()\s+-]/g,''));
		},'Enter Valid  phone number');*/
		
		jQuery.validator.addMethod("phoneno", function(value, element) {
        return this.optional(element) || /^[0-9+() ]*$/.test(value);
        }, "Enter Valid  phone number."); 
		
		
		
		$('#User_form').validate({
		
           rules: {
			user_type_id: {
                required: true,
			},

			/*client_id: {
                required: true,
			},


			brand_id: {
                required: true,
			},
			*/
			/*client_id: {
                required: true,
			},*/
			"client_id[]":{
				required:true,
				
			},
			"brand_id[]":{
				required:true,
				
			},
            user_name:{
				required:true,
				minlength: 2,
				//alphanumeric:true,
				//atLeastOneLetter:true,
			},
			user_email:{
				required:true,
				email: true,
				<?php
				if($user_id=="")
				{
				?>
				remote: {
				url: "<?php echo base_url();?>admin/user/checkexistemail",
				type: "post"
				},
				<?php
				}
				?>	 
				 
			 },
			
			
			<?php
			if($user_id=='')
			{
			?>
			user_password:{
				required:true,
				minlength:5,
			},
			user_cpassword: {
				required:true,
                equalTo: "#user_password" ,
				minlength:5,
            },
			<?php
			}
			?>
		   user_mobile: {
		   required: true,
		   /*digits: true,*/
		   minlength:10,
		  /* maxlength:10,*/
		   phoneno: true,
		   /*number: true,*/
		  /* maxlength: 10,*/
		    <?php
		   if($user_id=="")
		   {
		   ?>
		    remote: {
                    url: "<?php echo base_url();?>admin/user/checkexistmobile",
                    type: "post"
                   },
				 
		   <?php
		   }
		   ?>		 
				 
		},
		/*user_telephone: {
		   
		   digits: true,
		   minlength:10,
		   maxlength:10,
		    phoneno: true,
		   
		},
		*/
		
			status: {
                required: true,
			}
			
			
		},
		 messages: {
		 
		    user_email:{
		      remote:"This email already exists"
		     },
			user_mobile: {
			digits: "This field can only contain numbers.",
			/*maxlength: "this must be 10 digit number.",*/
			remote:"This mobile already exists",
			
			},
			
			 /*user_mobile:{
		      remote:"This email already exists"
		     }*/
                 <?php
				if($user_id=='')
				{
				?>
                user_cpassword: {
            	equalTo:"password not match",
			     },
			    <?php
				}
				?>   
		  
			
        },
				submitHandler: function(form) {
		
		       //alert("koko");

		       $("#loader").show();
		
		        var user_id=$('#user_id').val();
				var user_type_id=$('#user_type_id').val();

				/*if(user_type_id=='1' || user_type_id=='2'  || user_type_id=='3')
				{
				var brand_id=$('#hid_brand_id').val();
				}
				else
				{*/
				//var client_id=$('#client_id').val();
				//var brand_id=$('#brand_id').val();


				if(user_type_id=='6')
				{
						var client_id_all = [];
						var client_id_list = $('input:checkbox:checked.client_id_class').map(function(){
						client_id_all.push(this.value);
						return this.value; }).get().join(",");

						if($.trim(client_id_all)!='')
						{
						var client_id =","+client_id_all+",";
						}
						else
						{
						var client_id ="";
						}
						//alert(client_id);
						console.log(client_id);

				}
				else
				{
					var client_id=$('#client_id').val();
				}



				var brand_id_all = [];
				var brand_id_list = $('input:checkbox:checked.brand_id_class').map(function(){
					brand_id_all.push(this.value);
				return this.value; }).get().join(",");

				if($.trim(brand_id_all)!='')
				{
					var brand_id =","+brand_id_all+",";
				}
				else
				{
					var brand_id ="";
				}
				//alert(brand_id);
				console.log(brand_id);












				/*var brand_id = $('#brand_id option:selected')
				.toArray().map(item => item.value);
				var brand_id=","+brand_id+",";*/
				//alert(brand_id);
				//}
				//alert(brand_id);

			    var user_name=$('#user_name').val();
			    //var user_address=$('#user_address').val();
				var user_email=$('#user_email').val();
				//var user_telephone=$('#user_telephone').val();
				var user_mobile=$('#user_mobile').val();
				var user_password=$('#user_password').val();
				
				var status=$('#status').val();
				
				var user_country=$('#user_country').val();	
				
				
					
					
				/*alert("<?php echo base_url();?>admin/user/insert_user?&user_type_id="+user_type_id+"&client_id="+client_id+"&brand_id="+brand_id+"&user_id="+user_id+"&user_name="+user_name+"&user_email="+user_email+"&user_mobile="+user_mobile+"&user_password="+user_password+"&status="+status);*/



				$.ajax({
				method:'post',
				url: "<?php echo base_url();?>admin/user/insert_user", 
				data:"&user_type_id="+user_type_id+"&client_id="+client_id+"&brand_id="+brand_id+"&user_id="+user_id+"&user_name="+user_name+"&user_email="+user_email+"&user_mobile="+user_mobile+"&user_password="+user_password+"&status="+status+"&user_country="+user_country,
				cache: true,
				timeout: 5000
				}).done(function( result ) {
				console.log( "SUCCESS: " + result );
				$('#loader').hide();


				if(result)
				{
				    //alert(result);
					
					//alert("koko");
					
					var val=$.trim(result);
					//alert(val);



					if(val=='wekanexist')
					{
						alert("That email is already exist in Wekan.");

					}
					else
					{
					
						$("#success_message").show();
						setTimeout(function(){setInterval(function(){
						//alert("ppp");
						parent.$.fancybox.close();
						 $("#loader").hide();

						}, 3000)}, 3000);


						 $("#loader").hide();
						parent.$.fancybox.close();

						//alert("fofo");
						//alert("koko");

						parent.$('#example1').dataTable().fnStandingRedraw();

				}
					
					
				  
					
				
				  
				  
				 
				
				}
				//window.location.href="<?php echo base_url();?>dashboard";
				}).fail(function() {
				console.log( "Request failed");
				$('#loader').hide();
				//window.location.href="<?php echo base_url();?>dashboard";

				$("#success_message").show();
						setTimeout(function(){setInterval(function(){
						//alert("ppp");
						parent.$.fancybox.close();
						 $("#loader").hide();

						}, 3000)}, 3000);


						 $("#loader").hide();
						parent.$.fancybox.close();

						//alert("fofo");
						//alert("koko");

						parent.$('#example1').dataTable().fnStandingRedraw();
						
				});	


					
				/*$.ajax({
				method:'post',
				url: "<?php echo base_url();?>admin/user/insert_user", 
				data:"&user_type_id="+user_type_id+"&client_id="+client_id+"&brand_id="+brand_id+"&user_id="+user_id+"&user_name="+user_name+"&user_email="+user_email+"&user_mobile="+user_mobile+"&user_password="+user_password+"&status="+status,
				success: function(result){
				if(result)
				{
				    //alert(result);
					
					//alert("koko");
					
					var val=$.trim(result);
					//alert(val);



					if(val=='wekanexist')
					{
						alert("That email is already exist in Wekan.");

					}
					else
					{
					
						$("#success_message").show();
						setTimeout(function(){setInterval(function(){
						//alert("ppp");
						parent.$.fancybox.close();
						 $("#loader").hide();

						}, 3000)}, 3000);


						 $("#loader").hide();
						parent.$.fancybox.close();

						//alert("fofo");
						//alert("koko");

						parent.$('#example1').dataTable().fnStandingRedraw();

				}
					
					
				  
					
				
				  
				  
				 
				
				}
				}
				});
					*/
					
					
					
					
						
							
					
					   
		
		} 
		 
		
		});
		
		
		
	$('#User_password_form').validate({
        rules: {
			user_change_password:{
				
				required:true
			},
			
			 user_change_cpassword: {
				required:true,
                equalTo: "#user_change_password"              
            }
			
        },
		 messages: {
          
			user_change_cpassword: {
            	equalTo:"password not match"
			}
			
        },
		submitHandler: function(form) {

			$("#loader").show();
		        var user_id=$('#user_id').val();
				var user_password=$('#user_change_password').val();
				
				
				
				
					
					
				//alert("<?php echo base_url();?>user/addUser?&user_id="+user_id+"&UserName="+UserName+"&UserComments="+UserComments+"&UserIsActive="+UserIsActive);	
					
				/*$.ajax({
				method:'post',
				url: "<?php echo base_url();?>admin/user/update_change_password", 
				data:"&user_id="+user_id+"&user_password="+user_password,
				success: function(result){
				if(result)
				{
				// alert(result);
					
					
					
					var val=$.trim(result);
					
					
					$("#success_message").show();
				  setTimeout(function(){setInterval(function(){
				  //alert("ppp");
				  parent.$.fancybox.close();
				  $("#loader").hide();
				  
				  }, 3000)}, 3000);
					
					//alert(val);
					parent.$.fancybox.close();
					parent.$('#example1').dataTable().fnStandingRedraw();
					
				
				  
				  
				 
				
				}
				}
				});*/



				$.ajax({
				method:'post',
				url: "<?php echo base_url();?>admin/user/update_change_password", 
				data:"&user_id="+user_id+"&user_password="+user_password,
				cache: true,
				timeout: 3000
				}).done(function( result ) {
				console.log( "SUCCESS: " + result );
				if(result)
				{
				// alert(result);
					
					
					
					var val=$.trim(result);
					
					
					$("#success_message").show();
				  setTimeout(function(){setInterval(function(){
				  //alert("ppp");
				  parent.$.fancybox.close();
				  $("#loader").hide();
				  
				  }, 3000)}, 3000);
					
					//alert(val);
					parent.$.fancybox.close();
					parent.$('#example1').dataTable().fnStandingRedraw();
					
				
				  
				  
				 
				
				}


				
				//window.location.href="<?php echo base_url();?>dashboard";
				}).fail(function() {
				console.log( "Request failed");
				
					
					
					
					$("#success_message").show();
				  setTimeout(function(){setInterval(function(){
				  //alert("ppp");
				  parent.$.fancybox.close();
				  $("#loader").hide();
				  
				  }, 3000)}, 3000);
					
					//alert(val);
					parent.$.fancybox.close();
					parent.$('#example1').dataTable().fnStandingRedraw();
					
						
				});	


















				
				
				
		 },
    });
	
		
		
		
		
	});	
</script>   
<script>
 $(document).ready(function() {
 //alert("popo");
	  /* $('#example-getting-started-input').strengthMeter('text', {
            container: $('#example-getting-started-text')
        });
        $('#example-tooltip').strengthMeter('tooltip');*/
		/*$('#user_passowrd').strengthMeter('progressBar', {
            container: $('#example-progress-bar-container')
        });*/
    });
</script>
<script type="text/javascript" src="<?php echo base_url();?>theme/assets/source/jquery.fancybox.js?v=2.1.5"></script>
<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>theme/assets/source/jquery.fancybox.css?v=2.1.5" media="screen" />				