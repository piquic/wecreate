<!-- Last Edited: KW 25/09/16 -->
<?php
$session_data = $this->session->userdata('logged_in');
$rolecode = $session_data['rolecode'];
$usertype= $session_data['rolecode'];
$roles= $session_data['roles'];
	//$businesstype = $session_data['businesstype'];

$role=trim($roles,',');
if(strstr($role,","))
{
	$role=explode(',',$role);
}
else
{
	$role=array($role);
}



		//echo "<pre>";
		//print_r($role); exit;				

?>

<aside class="main-sidebar">
	<section class="sidebar">
		<div class="user-panel">
			<div class="pull-left info"></div>
		</div>

		
		<ul class="sidebar-menu">
			<?php //echo $this->router->fetch_class()."----".$this->router->fetch_method();?>

			<li class="treeview <?php if($this->router->fetch_class()=='User' && ($this->router-> fetch_method()=='index' || $this->router->fetch_method()=='index' )) { ?> active <?php } ?> ">
				<a href="<?php echo base_url('administrator/manage-client'); ?>">
					<div class="d-flex">
						<i class="far fa-user"></i>
						<span>&emsp; Client</span>
					</div>
				</a>
			</li>



			<li class="treeview <?php if($this->router->fetch_class()=='Category' && ($this->router-> fetch_method()=='index' || $this->router->fetch_method()=='index' )) { ?> active <?php } ?> ">
				<a href="<?php echo base_url('administrator/manage-category'); ?>">
					<div class="d-flex">
						<i class="far fa-user"></i>
						<span>&emsp; Category</span>
					</div>
				</a>
			</li>

			<li class="treeview <?php if($this->router->fetch_class()=='User' && ($this->router-> fetch_method()=='index' || $this->router->fetch_method()=='index' )) { ?> active <?php } ?> ">
				<a href="<?php echo base_url('administrator/manage-brand'); ?>">
					<div class="d-flex">
						<i class="far fa-user"></i>
						<span>&emsp; Brand</span>
					</div>
				</a>
			</li>
			
			<li class="treeview <?php if($this->router->fetch_class()=='User' && ($this->router-> fetch_method()=='index' || $this->router->fetch_method()=='index' )) { ?> active <?php } ?> ">
				<a href="<?php echo base_url('administrator/manage-usertype'); ?>">
					<div class="d-flex">
						<i class="far fa-user"></i>
						<span>&emsp; Users Type</span>
					</div>
				</a>
			</li>

			<li class="treeview <?php if($this->router->fetch_class()=='User' && ($this->router-> fetch_method()=='index' || $this->router->fetch_method()=='index' )) { ?> active <?php } ?> ">
				<a href="<?php echo base_url('administrator/manage-user'); ?>">
					<div class="d-flex">
						<i class="far fa-user"></i>
						<span>&emsp; Users</span>
					</div>
				</a>
			</li>

			<li class="treeview <?php if($this->router->fetch_class()=='Plan' && ($this->router->fetch_method()=='index' || $this->router->fetch_method()=='index' )) { ?> active <?php } ?> ">
				<a href="<?php echo base_url('administrator/manage-module'); ?>">
					<div class="d-flex">
						<i class="fas fa-wallet"></i>
						<span>&emsp;Modules</span>
					</div>
				</a>
			</li>

			<li class="treeview <?php if($this->router->fetch_class()=='Plan' && ($this->router->fetch_method()=='index' || $this->router->fetch_method()=='index' )) { ?> active <?php } ?> ">
				<a href="<?php echo base_url('administrator/manage-module-permission'); ?>">
					<div class="d-flex">
						<i class="fas fa-wallet"></i>
						<span>&emsp;Modules Permission</span>
					</div>
				</a>
			</li>



			<li class="treeview <?php if($this->router->fetch_class()=='User' && ($this->router->fetch_method()=='index' || $this->router->fetch_method()=='index' )) { ?> active <?php } ?> ">
				<a href="<?php echo base_url('administrator/manage-page-content'); ?>">
					<div class="d-flex">
						<i class="far fa-sticky-note"></i>
						<span>&emsp; Page Contents</span>
					</div>
				</a>
			</li>

			<li class="treeview <?php if($this->router->fetch_class()=='Plan' && ($this->router->fetch_method()=='index' || $this->router->fetch_method()=='index' )) { ?> active <?php } ?> ">
				<a href="<?php echo base_url('administrator/manage-setting'); ?>">
					<div class="d-flex">
						<i class="fas fa-cogs"></i>
						<span>&emsp;Settings</span>
					</div>
				</a>
			</li>





			<li class="treeview <?php if($this->router->fetch_class()=='Feedbacktype' && ($this->router-> fetch_method()=='index' || $this->router->fetch_method()=='index' )) { ?> active <?php } ?> ">
				<a href="<?php echo base_url('administrator/manage-feedbacktype'); ?>">
					<div class="d-flex">
						<i class="far fa-sticky-note"></i>
						<span>&emsp; Feedback Type</span>
					</div>
				</a>
			</li>


			<li class="treeview <?php if($this->router->fetch_class()=='Feedbackrating' && ($this->router-> fetch_method()=='index' || $this->router->fetch_method()=='index' )) { ?> active <?php } ?> ">
				<a href="<?php echo base_url('administrator/manage-feedbackrating'); ?>">
					<div class="d-flex">
						<i class="far fa-sticky-note"></i>
						<span>&emsp; Feedback Rating</span>
					</div>
				</a>
			</li>

			<!-- <li class="treeview <?php if($this->router->fetch_class()=='Feedbackcontent' && ($this->router-> fetch_method()=='index' || $this->router->fetch_method()=='index' )) { ?> active <?php } ?> ">
				<a href="<?php echo base_url('administrator/manage-feedbackcontent'); ?>">
					<div class="d-flex">
						<i class="far fa-sticky-note"></i>
						<span>&emsp; Feedback Content</span>
					</div>
				</a>
			</li> -->


			<li class="treeview <?php if($this->router->fetch_class()=='feedbacksubmitlist' && ($this->router-> fetch_method()=='index' || $this->router->fetch_method()=='index' )) { ?> active <?php } ?> ">
				<a href="<?php echo base_url('administrator/manage-feedback-submit-list'); ?>">
					<div class="d-flex">
						<i class="far fa-sticky-note"></i>
						<span>&emsp; Feedback Submitted List</span>
					</div>
				</a>
			</li>





	<li class="treeview <?php if($this->router->fetch_class()=='wekanboardlist' && ($this->router-> fetch_method()=='index' || $this->router->fetch_method()=='index' )) { ?> active <?php } ?> ">
				<a href="<?php echo base_url('administrator/manage-wekan-board-list'); ?>">
					<div class="d-flex">
						<i class="far fa-sticky-note"></i>
						<span>&emsp; Wekan Board List</span>
					</div>
				</a>
			</li>

    

    <li class="treeview <?php if($this->router->fetch_class()=='downloadproducts' && ($this->router-> fetch_method()=='index' || $this->router->fetch_method()=='index' )) { ?> active <?php } ?> ">
				<a href="<?php echo base_url('administrator/download-products-csv'); ?>">
					<div class="d-flex">
						<i class="far fa-sticky-note"></i>
						<span>&emsp; Download Products CSV</span>
					</div>
				</a>
			</li>


<li class="treeview <?php if($this->router->fetch_class()=='Plan' && ($this->router->fetch_method()=='index' || $this->router->fetch_method()=='index' )) { ?> active <?php } ?> ">
				<a href="<?php echo base_url('administrator/maintenance-setting'); ?>">
					<div class="d-flex">
						<i class="fas fa-cogs"></i>
						<span>&emsp;Maintenance</span>
					</div>
				</a>
			</li>










			<<!-- li class="treeview <?php if($this->router->fetch_class()=='User' && ($this->router->fetch_method()=='index' || $this->router->fetch_method()=='index' )) { ?> active <?php } ?> ">
				<a href="<?php echo base_url('administrator/image_size'); ?>">
					<div class="d-flex">
						<i class="far fa-image"></i>
						<span>&emsp;Image Sizes</span>
					</div>
				</a>
			</li>

			<li class="treeview <?php if($this->router->fetch_class()=='Paymenttype' && ($this->router->fetch_method()=='index' || $this->router->fetch_method()=='index' )) { ?> active <?php } ?> ">
				<a href="<?php echo base_url('administrator/manage-paymenttype'); ?>">
					<div class="d-flex">
						<i class="fas fa-wallet"></i>
						<span>&emsp;Payment Types</span>
					</div>
				</a>
			</li>

			<li class="treeview <?php if($this->router->fetch_class()=='Plantype' && ($this->router->fetch_method()=='index' || $this->router->fetch_method()=='index' )) { ?> active <?php } ?> ">
				<a href="<?php echo base_url('administrator/manage-plantype'); ?>">
					<div class="d-flex">
						<i class="fab fa-paypal"></i>
						<span>&emsp;  Plan Types</span>
					</div>
				</a>
			</li>

			<li class="treeview <?php if($this->router->fetch_class()=='Plan' && ($this->router->fetch_method()=='index' || $this->router->fetch_method()=='index' )) { ?> active <?php } ?> ">
				<a href="<?php echo base_url('administrator/manage-plan'); ?>">
					<div class="d-flex">
						<i class="fab fa-paypal"></i>
						<span>&emsp;  Plans</span>
					</div>
				</a>
			</li>

			

			<li class="treeview <?php if($this->router->fetch_class()=='Order_upload' && ($this->router->fetch_method()=='index' || $this->router->fetch_method()=='index' )) { ?> active <?php } ?> ">
				<a href="<?php echo base_url('administrator/manage-order-upload'); ?>">
					<div class="d-flex">
						<i class="fas fa-shopping-cart"></i>
						<span>&emsp;Orders (Upload)</span>
					</div>
				</a>
			</li>

			<li class="treeview <?php if($this->router->fetch_class()=='Order' && ($this->router->fetch_method()=='index' || $this->router->fetch_method()=='index' )) { ?> active <?php } ?> ">
				<a href="<?php echo base_url('administrator/manage-order'); ?>">
					<div class="d-flex">
						<i class="fas fa-shopping-cart"></i>
						<span>&emsp;Orders (Booking)</span>
					</div>
				</a>
			</li> -->
		</ul>
	</section>
</aside>