<?php  $this->load->view('admin/includes/globalcss');?>


<style>
/*.table-bordered {
    border: 1px solid #666666;
}*/
.showdbutton {
    cursor:pointer; 
	background-color:#666666; 
	margin:10px 0px; 
	padding:10px; 
	color:#FFFFFF;
	width:100%;
}

.disabledbutton {
    pointer-events: none;
    opacity: 0.9;
	
	cursor:pointer; 
	background-color:#999999; 
	margin:10px 0px; 
	padding:10px; 
	color:#FFFFFF;
	width:100%;
}
label.error{
 color:#FF0000;
 font-size:12px;
}
</style>
<div>
  <!-- Section content header -->
 
  <!-- Section content -->
    <section class="content">
		<div class="col-md-12">
		  <!-- start Information Box -->
			<div class="box">
			<!-- box header style start-->
				<div class="col-md-12 box-header with-border" ><h3 class="box-title"><?php if($brand_id==''){ echo 'Create Brand'; }else {echo 'Update Brand'; }?></h3></div>
					<!-- /.box-header -->
					<!-- form start -->
					
					<div id="success_message" style="display:none;">
					<div class="alert alert-success">
					<strong>Updated successfully!</strong>.
					</div>
				    </div>
					
				
				<div class="box-body">
				
				
				   <div class="col-md-12">
						<ul class="nav nav-tabs">
							<li id="li_home" class="active"><a data-toggle="tab" href="#home">Brand Information</a></li>
							
							
						</ul>
					</div>
				
				
				<div class="addmargin10">&nbsp;</div>
					<div class="tab-content">
					
				     <div id="home" class="tab-pane fade in active">
					 
					 		    <?php  $array = array('id'=>'User_form', 'role'=>'form', 'class'=>'form form-horizontal has-validation-callback');
				    echo form_open_multipart('User/insert_user', $array); ?>
					
					
					<input type='hidden' name='brand_id' value='<?php echo $brand_id;?>' id='brand_id'>
					
					
					
					
					
					
					
					
					
					<div class="form-group">
					    <label for="inputEmail3"  class="col-sm-2 control-label">Brand Code
                        <span style="color:#F00">*</span>
                        </label>
						<div class="col-sm-8">
						    <input  type='text'  class="form-control" id="brand_code" name="brand_code" value='<?php echo $brand_code;?>'>
                            <?php echo form_error('brand_code', '<div class="error" style="color:red;">', '</div>'); ?>
						</div>
						
					</div>



					<div class="form-group">
					    <label for="inputEmail3"  class="col-sm-2 control-label">Brand Name
                        <span style="color:#F00">*</span>
                        </label>
						<div class="col-sm-8">
						    <input  type='text'  class="form-control" id="brand_name" name="brand_name" value='<?php echo $brand_name;?>'>
                            <?php echo form_error('brand_name', '<div class="error" style="color:red;">', '</div>'); ?>
						</div>
						
					</div>



					<div class="form-group">
					    <label for="inputEmail3"  class="col-sm-2 control-label">Business Model Type
                        <span style="color:#F00">*</span>
                        </label>
						<div class="col-sm-8">
						    <input  type='text'  class="form-control" id="business_model_type" name="business_model_type" value='<?php echo $business_model_type;?>'>
                            <?php echo form_error('business_model_type', '<div class="error" style="color:red;">', '</div>'); ?>
						</div>
						
					</div>
					
					
					<div class="form-group">
					    <label for="inputEmail3"  class="col-sm-2 control-label">Client Name
                        <span style="color:#F00">*</span>
                        </label>
						<div class="col-sm-8">
						    <input  type='text'  class="form-control" id="client_name" name="client_name" value='<?php echo $client_name;?>'>
                            <?php echo form_error('client_name', '<div class="error" style="color:red;">', '</div>'); ?>
						</div>
						
					</div>
					
					
				
				<div class="form-group">
					    <label for="inputEmail3"  class="col-sm-2 control-label">Location Code
                        <span style="color:#F00">*</span>
                        </label>
						<div class="col-sm-8">
						    <input  type='text'  class="form-control" id="location_code" name="location_code" value='<?php echo $location_code;?>'>
                            <?php echo form_error('location_code', '<div class="error" style="color:red;">', '</div>'); ?>
						</div>
						
					</div>
					
					<div class="form-group">
					    <label for="inputEmail3"  class="col-sm-2 control-label">Location Name</label>
						<div class="col-sm-8">
						    <input  type='text'  class="form-control" id="location_name" name="location_name" value='<?php echo $location_name;?>'>
                            <?php echo form_error('user_telephone', '<div class="error" style="color:red;">', '</location_name>'); ?>
						</div>
						
					</div>
					
					
					
			
					
					
					
					<div class="form-group   ">
								<label for="inputEmail3"  class="col-sm-2 control-label">Brand Desc</label>
								<div class="col-sm-8">
								<textarea  class="form-control" id="brand_desc" name="brand_desc"><?php echo $brand_desc; ?></textarea>
									
									<?php echo form_error('brand_desc', '<div class="error" style="color:red;">', '</div>'); ?>
								</div>
								
								
							
							
								
							</div>
					
					
					
					<div class="form-group">
					    <label for="inputEmail3"  class="col-sm-2 control-label">Client Phone</label>
						<div class="col-sm-8">
						    <input  type='text'  class="form-control" id="client_phone" name="client_phone" value='<?php echo $client_phone;?>'>
                            <?php echo form_error('client_phone', '<div class="error" style="color:red;">', '</div>'); ?>
						</div>
						
					</div>
					
					<div class="form-group">
					    <label for="inputEmail3"  class="col-sm-2 control-label">Client Email</label>
						<div class="col-sm-8">
						    <input  type='text'  class="form-control" id="client_email" name="client_email" value='<?php echo $client_email;?>'>
                            <?php echo form_error('client_email', '<div class="error" style="color:red;">', '</div>'); ?>
						</div>
						
					</div>
					
					
					
			
					
					
					<div class="form-group">
					<label for="inputEmail3"  class="col-sm-2 control-label"><?php echo $lang['status'];?></label>
					<div class="col-sm-8">
				
							
								<select name="status" id="status" class="form-control" >
								 <!--<option value=''>Select</option>-->
								 <option value="Active" <?php if($status=='Active') { ?> selected="selected" <?php } ?>  >Active</option>
								 <option value="Inactive" <?php if($status=='Inactive') { ?> selected="selected" <?php } ?>   >Inactive</option>
								 </select>
					
					</div>
					</div>
					
					
					
					
					
					
					
					
					
					
					
					
					
					
					
					
					
					
					
				
					
				
					
				
					
					
					
				
					
					
					
					
					
					
					
					
					
					<?php echo form_close();?> 
					 
					 
					 
				    </div>
					
					
					
					
					<!--Tab Password Change-->
						<div id="menu1" class="tab-pane fade">
						<!--<div id="" class=""></div>-->
							<!-- form start -->
							
                        <?php 
				$array = array('id'=>'User_password_form', 'role'=>'form', 'class'=>'form form-horizontal has-validation-callback');
				echo form_open_multipart('admin/User/update_change_password', $array); ?>
                
                <input type="hidden" class="form-control" id="brand_id" name="brand_id" value="<?php echo $brand_id ?>" >
                
						<div class="form-group">
							<label for="inputEmail3"  class="col-sm-2 control-label">Password</label>
							<div class="col-sm-8">
								<input type="password" class="form-control" id="user_change_password" name="user_change_password">
								<?php echo form_error('user_change_password', '<div class="error" style="color:red;">', '</div>'); ?>
							</div>
						</div>
						<div class="form-group">
							<label for="inputEmail3"  class="col-sm-2 control-label">Confirm Password</label>
							<div class="col-sm-8">
								<input type="password" class="form-control"  id="user_change_cpassword" name="user_change_cpassword">
								<?php echo form_error('user_change_cpassword', '<div class="error" style="color:red;">', '</div>'); ?>
							</div>
						</div>
						
						<div class="addmargin10">&nbsp;</div>
						
						<div class="row">
						<?php if($brand_id!=''){
						$button_name='Update';
						}else {
						$button_name='Save';
						} ?>
						<div class="col-md-6"><input class="btn btn-success pull-right" type="submit" value="<?php echo $button_name;?>"></div>	   
						<div class="col-md-6"><input class="btn btn-danger pull-left" type="reset"></div>
						</div>
					
					
					
						<?php echo form_close(); ?>	
						</div>	
					
					
				    </div>
					
					
					
					
				
		
					
					
					
				</div>
					
			</div>
		</div>
		
	</section>
</div>

<?php $this->load->view('admin/includes/globaljs');?>
<link type="text/css" href="<?php echo base_url();?>assets/form-select2/select2.css" rel="stylesheet"/>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootswatch/3.3.7/lumen/bootstrap.min.css">



<script type="text/javascript" src="<?php echo base_url();?>assets/form-select2/select2.min.js"></script>



<!--<script  type="text/javascript" src="<?php echo base_url();?>assets/dist/password-score.js"></script>
<script  type="text/javascript" src="<?php echo base_url();?>assets/dist/password-score-options.js"></script>
<script  type="text/javascript" src="<?php echo base_url();?>assets/dist/bootstrap-strength-meter.js"></script>-->

 <script>
    
    
	jQuery(function ($) {
	   // $('.content-wrapper').css('min-height','900px');
		
		
		
	 $('#entity_id').select2();
		
		
		/*$('#user_password').strengthMeter('progressBar', {
            container: $('#example-progress-bar-container')
        });*/
		
		
		
	    /*$.validator.addMethod("regex", function(value, element, regexpr) {          
		 return regexpr.test(value);
		}, "Please enter a valid Mobile Number.");*/
		
		
		
		
		
		
		
		
		
		//alert("popo");
		
		
		jQuery.validator.addMethod("alphanumeric", function(value, element) {
        return this.optional(element) || /^[a-zA-Z\-\s]+$/.test(value);
        }, "It must contain only letters."); 
		
		jQuery.validator.addMethod("atLeastOneLetter", function (value, element) {
		return this.optional(element) || /[a-zA-Z]+/.test(value);
		}, "Must have at least one  letter");
		
		/*jQuery.validator.addMethod("phoneno", function(phone_number, element) {
    	    phone_number = phone_number.replace(/\s+/g, "");
    	    return this.optional(element) || phone_number.length > 9 && 
    	    phone_number.match(/^((\+[1-9]{1,4}[ \-]*)|(\([0-9]{2,3}\)[ \-]*)|([0-9]{2,4})[ \-]*)*?[0-9]{3,4}?[ \-]*[0-9]{3,4}?$/);
    	}, "<br />Please specify a valid phone number");*/
		
		/*jQuery.validator.addMethod('fnType', function(value, element) {
		return value.match(/^\+(?:[0-9] ?){6,14}[0-9]$/);
		},'Enter Valid  phone number');*/
		
		/*jQuery.validator.addMethod('fnType', function(value, element) {
		return /^\d{10}$/.test(value.replace(/[()\s+-]/g,''));
		},'Enter Valid  phone number');*/
		
		
		/*jQuery.validator.addMethod('fnType', function(value, element) {
		return /^\d{10}$/.test(value.replace(/[()\s+-]/g,''));
		},'Enter Valid  phone number');*/
		
		jQuery.validator.addMethod("phoneno", function(value, element) {
        return this.optional(element) || /^[0-9+() ]*$/.test(value);
        }, "Enter Valid  phone number."); 
		
		
		
		$('#User_form').validate({
		
           rules: {
			user_type_id: {
                required: true,
			},
			
            user_name:{
				required:true,
				minlength: 2,
				//alphanumeric:true,
				//atLeastOneLetter:true,
			},
			user_email:{
				required:true,
				email: true,
				<?php
				if($brand_id=="")
				{
				?>
				remote: {
				url: "<?php echo base_url();?>admin/user/checkexistemail",
				type: "post"
				},
				<?php
				}
				?>	 
				 
			 },
			
			
			<?php
			if($brand_id=='')
			{
			?>
			user_password:{
				required:true,
				minlength:5,
			},
			user_cpassword: {
				required:true,
                equalTo: "#user_password" ,
				minlength:5,
            },
			<?php
			}
			?>
		   user_mobile: {
		   required: true,
		   /*digits: true,*/
		   minlength:10,
		  /* maxlength:10,*/
		   phoneno: true,
		   /*number: true,*/
		  /* maxlength: 10,*/
		    <?php
		   if($brand_id=="")
		   {
		   ?>
		    remote: {
                    url: "<?php echo base_url();?>admin/user/checkexistmobile",
                    type: "post"
                   },
				 
		   <?php
		   }
		   ?>		 
				 
		},
		user_telephone: {
		   
		  /* digits: true,*/
		   minlength:10,
		  /* maxlength:10,*/
		    phoneno: true,
		   
		},
		
		
			status: {
                required: true,
			}
			
			
		},
		 messages: {
		 
		    user_email:{
		      remote:"This email already exists"
		     },
			user_mobile: {
			digits: "This field can only contain numbers.",
			/*maxlength: "this must be 10 digit number.",*/
			remote:"This mobile already exists",
			
			},
			
			 /*user_mobile:{
		      remote:"This email already exists"
		     }*/
                 <?php
				if($brand_id=='')
				{
				?>
                user_cpassword: {
            	equalTo:"password not match",
			     },
			    <?php
				}
				?>   
		  
			
        },
				submitHandler: function(form) {
		
		       //alert("koko");
		
		        var brand_id=$('#brand_id').val();
				var user_type_id=$('#user_type_id').val();
			    var user_name=$('#user_name').val();
			    var user_address=$('#user_address').val();
				var user_email=$('#user_email').val();
				var user_telephone=$('#user_telephone').val();
				var user_mobile=$('#user_mobile').val();
				var user_password=$('#user_password').val();
				
				var status=$('#status').val();
				
				
				
				
					
					
				/*alert("<?php echo base_url();?>admin/user/insert_user?&user_type_id="+user_type_id+"&brand_id="+brand_id+"&user_name="+user_name+"&user_address="+user_address+"&user_email="+user_email+"&user_telephone="+user_telephone+"&user_mobile="+user_mobile+"&user_password="+user_password+"&status="+status);	
					*/
				$.ajax({
				method:'post',
				url: "<?php echo base_url();?>admin/user/insert_user", 
				data:"&user_type_id="+user_type_id+"&brand_id="+brand_id+"&user_name="+user_name+"&user_address="+user_address+"&user_email="+user_email+"&user_telephone="+user_telephone+"&user_mobile="+user_mobile+"&user_password="+user_password+"&status="+status,
				success: function(result){
				if(result)
				{
				    //alert(result);
					
					//alert("koko");
					
					var val=$.trim(result);
					//alert(val);
					
					$("#success_message").show();
				  setTimeout(function(){setInterval(function(){
				  //alert("ppp");
				  parent.$.fancybox.close();
				  
				  }, 3000)}, 3000);
				  
				  
					
					parent.$.fancybox.close();
					
					//alert("fofo");
					//alert("koko");
					
					parent.$('#example1').dataTable().fnStandingRedraw();
					
					
				  
					
				
				  
				  
				 
				
				}
				}
				});
					
					
					
					
					
						
							
					
					   
		
		} 
		 
		
		});
		
		
		
	$('#User_password_form').validate({
        rules: {
			user_change_password:{
				
				required:true
			},
			
			 user_change_cpassword: {
				required:true,
                equalTo: "#user_change_password"              
            }
			
        },
		 messages: {
          
			user_change_cpassword: {
            	equalTo:"password not match"
			}
			
        },
		submitHandler: function(form) {
		        var brand_id=$('#brand_id').val();
				var user_password=$('#user_change_password').val();
				
				
				
				
					
					
				//alert("<?php echo base_url();?>user/addUser?&brand_id="+brand_id+"&UserName="+UserName+"&UserComments="+UserComments+"&UserIsActive="+UserIsActive);	
					
				$.ajax({
				method:'post',
				url: "<?php echo base_url();?>admin/user/update_change_password", 
				data:"&brand_id="+brand_id+"&user_password="+user_password,
				success: function(result){
				if(result)
				{
				// alert(result);
					
					
					
					var val=$.trim(result);
					
					
					$("#success_message").show();
				  setTimeout(function(){setInterval(function(){
				  //alert("ppp");
				  parent.$.fancybox.close();
				  
				  }, 3000)}, 3000);
					
					//alert(val);
					parent.$.fancybox.close();
					parent.$('#example1').dataTable().fnStandingRedraw();
					
				
				  
				  
				 
				
				}
				}
				});
				
				
				
		 },
    });
	
		
		
		
		
	});	
</script>   
<script>
 $(document).ready(function() {
 //alert("popo");
	  /* $('#example-getting-started-input').strengthMeter('text', {
            container: $('#example-getting-started-text')
        });
        $('#example-tooltip').strengthMeter('tooltip');*/
		/*$('#user_passowrd').strengthMeter('progressBar', {
            container: $('#example-progress-bar-container')
        });*/
    });
</script>
<script type="text/javascript" src="<?php echo base_url();?>theme/assets/source/jquery.fancybox.js?v=2.1.5"></script>
<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>theme/assets/source/jquery.fancybox.css?v=2.1.5" media="screen" />				