<?php  $this->load->view('admin/includes/globalcss');?>


<style>
/*.table-bordered {
    border: 1px solid #666666;
}*/
.showdbutton {
    cursor:pointer; 
	background-color:#666666; 
	margin:10px 0px; 
	padding:10px; 
	color:#FFFFFF;
	width:100%;
}

.disabledbutton {
    pointer-events: none;
    opacity: 0.9;
	
	cursor:pointer; 
	background-color:#999999; 
	margin:10px 0px; 
	padding:10px; 
	color:#FFFFFF;
	width:100%;
}
label.error{
 color:#FF0000;
 font-size:12px;
}
</style>
<div>
  <!-- Section content header -->
 
  <!-- Section content -->
    <section class="content">
		<div class="col-md-12">
		  <!-- start Information Box -->
			<div class="box">
			<!-- box header style start-->
				<div class="col-md-12 box-header with-border" ><h3 class="box-title"><?php if($feedback_content_id==''){ echo 'Create Feedback Content'; }else {echo 'Update Feedback Content'; }?></h3></div>
					<!-- /.box-header -->
					<!-- form start -->
					
					<div id="success_message" style="display:none;">
					<div class="alert alert-success">
					<strong>Updated successfully!</strong>.
					</div>
				    </div>
					
				
				<div class="box-body">
				
				
				   <div class="col-md-12">
						<ul class="nav nav-tabs">
							<li id="li_home" class="active"><a data-toggle="tab" href="#home">Feedback Content Information</a></li>
							
							
						</ul>
					</div>
				
				
				<div class="addmargin10">&nbsp;</div>
					<div class="tab-content">
					
				     <div id="home" class="tab-pane fade in active">
					 
					 		    <?php  $array = array('id'=>'feedbackcontent_form', 'role'=>'form', 'class'=>'form form-horizontal has-validation-callback');
				    echo form_open_multipart('feedbackcontent/insert_feedbackcontent', $array); ?>
					
					
					<input type='hidden' name='feedback_content_id' value='<?php echo $feedback_content_id;?>' id='feedback_content_id'>
					
					
					
					<div class="form-group">
					    <label for="inputEmail3"  class="col-sm-2 control-label"><?php echo 'Feedback Type Name';?>
                        <span style="color:#F00">*</span>
                        </label>
						<div class="col-sm-8">
						 <select name="feedback_type_id" id="feedback_type_id"   style="width: 100%" >
								 <option value=''>Select</option>
								<?php


								$feedback_type_details=$this->db->query("select * from wc_feedback_type where deleted='0'  ")->result_array();
								if(!empty($feedback_type_details))
								{
								foreach($feedback_type_details as $key => $feedback_type_details)
								{
								$feedback_type_id=$feedback_type_details['feedback_type_id'];
								$feedback_type=$feedback_type_details['feedback_type'];
								?>
								<option value="<?php echo $feedback_type_id;?>" <?php if($feedback_type_id_sel==$feedback_type_id) { ?> selected="selected" <?php } ?>  ><?php echo $feedback_type;?></option>
								<?php

								}
								}
								?>
								
								 </select>
                            <?php echo form_error('feedback_type_id', '<div class="error" style="color:red;">', '</div>'); ?>
						</div>
						
					</div>


					
					
					
					
					
					
					<div class="form-group">
					    <label for="inputEmail3"  class="col-sm-2 control-label"><?php echo 'Feedback Content';?>
                        <span style="color:#F00">*</span>
                        </label>
						<div class="col-sm-8">
						    <input  type='text'  class="form-control" id="feedback_content" name="feedback_content" value='<?php echo $feedback_content;?>'>
                            <?php echo form_error('feedback_content', '<div class="error" style="color:red;">', '</div>'); ?>
						</div>
						
					</div>
					
				
					
					
					
					
					
					
					
					
				
					
					<!-- Contractor type fields end-->
					<div class="row">
					<?php if($feedback_content_id!=''){
						$button_name='Update';
					}else {
						$button_name='Save';
					} ?>
						<div class="col-md-6"><input class="btn btn-success pull-right" type="submit" value="<?php echo $button_name;?>"></div>	   
						<div class="col-md-6"><input class="btn btn-danger pull-left" type="reset"></div>
					</div>
					
					
					
					
					
					
					
					
					
					
					
					
					
					
					
					<?php echo form_close();?> 
					 
					 
					 
				    </div>
					
					
					
					
				
					
				    </div>
					
					
					
					
				
		
					
					
					
				</div>
					
			</div>
		</div>
		
	</section>
</div>

<?php $this->load->view('admin/includes/globaljs');?>
<link type="text/css" href="<?php echo base_url();?>assets/form-select2/select2.css" rel="stylesheet"/>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootswatch/3.3.7/lumen/bootstrap.min.css">



<script type="text/javascript" src="<?php echo base_url();?>assets/form-select2/select2.min.js"></script>



<!--<script  type="text/javascript" src="<?php echo base_url();?>assets/dist/password-score.js"></script>
<script  type="text/javascript" src="<?php echo base_url();?>assets/dist/password-score-options.js"></script>
<script  type="text/javascript" src="<?php echo base_url();?>assets/dist/bootstrap-strength-meter.js"></script>-->

 <script>
    
    
	jQuery(function ($) {
	   // $('.content-wrapper').css('min-height','900px');
	   //$('#entity_id').select2();
		
		
		$('#feedbackcontent_form').validate({
		
           rules: {
		
          
		   feedback_type_id: {
                required: true,
			},
		   feedback_content: {
                required: true,
			}
			
			
		},
		 messages: {
		 
		  
		  
			
        },
				submitHandler: function(form) {
		
		       //alert("koko");
		
		        var feedback_content_id=$('#feedback_content_id').val();
				
				var feedback_type_id=$('#feedback_type_id').val();
			    var feedback_content=$('#feedback_content').val();
			   
				//alert("<?php echo base_url();?>admin/feedbackcontent/insert_feedbackcontent?&feedback_content_id="+feedback_content_id+"&feedback_content="+feedback_content+"&status="+status);	
					
				$.ajax({
				method:'post',
				url: "<?php echo base_url();?>admin/feedbackcontent/insert_feedbackcontent", 
				data:"&feedback_content_id="+feedback_content_id+"&feedback_type_id="+feedback_type_id+"&feedback_content="+feedback_content,
				success: function(result){
				if(result)
				{
					//alert(result);

					//alert("koko");

					var val=$.trim(result);
					//alert(val);

					$("#success_message").show();
					setTimeout(function(){setInterval(function(){
					//alert("ppp");
					parent.$.fancybox.close();

					}, 3000)}, 3000);
					parent.$.fancybox.close();
					parent.$('#example1').dataTable().fnStandingRedraw();
					
					
				  
					
				
				  
				  
				 
				
				}
				}
				});
					
					
					
					
					
						
							
					
					   
		
		} 
		 
		
		});
		

		
		
	});	
</script>   
<script>
 $(document).ready(function() {

 	 $('#feedback_type_id').select2();
 	 
 	 
 //alert("popo");
	  /* $('#example-getting-started-input').strengthMeter('text', {
            container: $('#example-getting-started-text')
        });
        $('#example-tooltip').strengthMeter('tooltip');*/
		/*$('#user_passowrd').strengthMeter('progressBar', {
            container: $('#example-progress-bar-container')
        });*/
    });
</script>
<script type="text/javascript" src="<?php echo base_url();?>theme/assets/source/jquery.fancybox.js?v=2.1.5"></script>
<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>theme/assets/source/jquery.fancybox.css?v=2.1.5" media="screen" />				