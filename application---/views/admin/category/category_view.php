<?php  $this->load->view('admin/includes/globalcss');?>


<style>
/*.table-bordered {
    border: 1px solid #666666;
}*/
.showdbutton {
    cursor:pointer; 
	background-color:#666666; 
	margin:10px 0px; 
	padding:10px; 
	color:#FFFFFF;
	width:100%;
}

.disabledbutton {
    pointer-events: none;
    opacity: 0.9;
	
	cursor:pointer; 
	background-color:#999999; 
	margin:10px 0px; 
	padding:10px; 
	color:#FFFFFF;
	width:100%;
}
label.error{
 color:#FF0000;
 font-size:12px;
}
</style>
<div>
  <!-- Section content header -->
 
  <!-- Section content -->
    <section class="content">
		<div class="col-md-12">
		  <!-- start Information Box -->
			<div class="box">
			<!-- box header style start-->
				<div class="col-md-12 box-header with-border" ><h3 class="box-title"><?php if($category_id==''){ echo 'Create category'; }else {echo 'Update category'; }?></h3></div>
					<!-- /.box-header -->
					<!-- form start -->
					
					<div id="success_message" style="display:none;">
					<div class="alert alert-success">
					<strong>Updated successfully!</strong>.
					</div>
				    </div>
					
				
				<div class="box-body">
				
				
				   <div class="col-md-12">
						<ul class="nav nav-tabs">
							<li id="li_home" class="active"><a data-toggle="tab" href="#home">category Information</a></li>
							
							
						</ul>
					</div>
				
				
				<div class="addmargin10">&nbsp;</div>
					<div class="tab-content">
					
				     <div id="home" class="tab-pane fade in active">
					 
					 		    <?php  $array = array('id'=>'category_form', 'role'=>'form', 'class'=>'form form-horizontal has-validation-callback');
				    echo form_open_multipart('User/insert_user', $array); ?>
					
					
					<input type='hidden' name='category_id' value='<?php echo $category_id;?>' id='category_id'>
					
					
					
					<div class="form-group">
					    <label for="inputEmail3"  class="col-sm-2 control-label">Client Name
                        <span style="color:#F00">*</span>
                        </label>
						<div class="col-sm-8">
						    	<!-- class="form-control" -->
								<select name="client_id" id="client_id"  style="width: 100%" >
								 <option value=''>Select</option>
								<?php
								$clients_details=$this->db->query("select * from wc_clients where deleted='0' and status='Active' ")->result_array();
								if(!empty($clients_details))
								{
								foreach($clients_details as $key => $clientsdetails)
								{
								$client_id=$clientsdetails['client_id'];
								$client_name=$clientsdetails['client_name'];
								?>
								<option value="<?php echo $client_id;?>" <?php if($client_id_sel==$client_id) { ?> selected="selected" <?php } ?>  ><?php echo $client_name;?></option>
								<?php

								}
								}
								?>
								
								 </select>
						</div>
						
					</div>
					
					
					
					
					
					



					<div class="form-group">
					    <label for="inputEmail3"  class="col-sm-2 control-label">category Name
                        <span style="color:#F00">*</span>
                        </label>
						<div class="col-sm-8">
						    <input  type='text'  class="form-control" id="category_name" name="category_name" value='<?php echo $category_name;?>'>
                            <?php echo form_error('category_name', '<div class="error" style="color:red;">', '</div>'); ?>
						</div>
						
					</div>



					<!-- <div class="form-group">
					    <label for="inputEmail3"  class="col-sm-2 control-label">category Email
                        <span style="color:#F00">*</span>
                        </label>
						<div class="col-sm-8">
						    <input  type='text'  class="form-control" id="category_email" name="category_email" value='<?php //echo $category_email;?>'>
                            <?php //echo form_error('category_email', '<div class="error" style="color:red;">', '</div>'); ?>
						</div>
						
					</div> -->
					
					
					
				
					
				
				
			
					
					
					<div class="form-group">
					<label for="inputEmail3"  class="col-sm-2 control-label"><?php echo $lang['status'];?></label>
					<div class="col-sm-8">
				
							
								<select name="status" id="status" class="form-control" >
								 <!--<option value=''>Select</option>-->
								 <option value="Active" <?php if($status=='Active') { ?> selected="selected" <?php } ?>  >Active</option>
								 <option value="Inactive" <?php if($status=='Inactive') { ?> selected="selected" <?php } ?>   >Inactive</option>
								 </select>
					
					</div>
					</div>
					
					
					
					
					
					
					
					
					
					
					
					
					
					
					
					
					
					<?php echo form_close();?> 
					 
					 
					 
				    </div>
					
					
					
				
					
				    </div>
					
					
					
					
				
		
					
					
					
				</div>
					
			</div>
		</div>
		
	</section>
</div>

<?php $this->load->view('admin/includes/globaljs');?>
<link type="text/css" href="<?php echo base_url();?>assets/form-select2/select2.css" rel="stylesheet"/>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootswatch/3.3.7/lumen/bootstrap.min.css">



<script type="text/javascript" src="<?php echo base_url();?>assets/form-select2/select2.min.js"></script>



<!--<script  type="text/javascript" src="<?php echo base_url();?>assets/dist/password-score.js"></script>
<script  type="text/javascript" src="<?php echo base_url();?>assets/dist/password-score-options.js"></script>
<script  type="text/javascript" src="<?php echo base_url();?>assets/dist/bootstrap-strength-meter.js"></script>-->

 <script>
    
    
	jQuery(function ($) {
	   // $('.content-wrapper').css('min-height','900px');
		
		
		
	 //$('#entity_id').select2();
		
		
		/*$('#user_password').strengthMeter('progressBar', {
            container: $('#example-progress-bar-container')
        });*/
		
		
		
	    /*$.validator.addMethod("regex", function(value, element, regexpr) {          
		 return regexpr.test(value);
		}, "Please enter a valid Mobile Number.");*/
		
		
		
		
		
		
		
		
		
		//alert("popo");
		
		
		jQuery.validator.addMethod("alphanumeric", function(value, element) {
        return this.optional(element) || /^[a-zA-Z\-\s]+$/.test(value);
        }, "It must contain only letters."); 
		
		jQuery.validator.addMethod("atLeastOneLetter", function (value, element) {
		return this.optional(element) || /[a-zA-Z]+/.test(value);
		}, "Must have at least one  letter");
		
		/*jQuery.validator.addMethod("phoneno", function(phone_number, element) {
    	    phone_number = phone_number.replace(/\s+/g, "");
    	    return this.optional(element) || phone_number.length > 9 && 
    	    phone_number.match(/^((\+[1-9]{1,4}[ \-]*)|(\([0-9]{2,3}\)[ \-]*)|([0-9]{2,4})[ \-]*)*?[0-9]{3,4}?[ \-]*[0-9]{3,4}?$/);
    	}, "<br />Please specify a valid phone number");*/
		
		/*jQuery.validator.addMethod('fnType', function(value, element) {
		return value.match(/^\+(?:[0-9] ?){6,14}[0-9]$/);
		},'Enter Valid  phone number');*/
		
		/*jQuery.validator.addMethod('fnType', function(value, element) {
		return /^\d{10}$/.test(value.replace(/[()\s+-]/g,''));
		},'Enter Valid  phone number');*/
		
		
		/*jQuery.validator.addMethod('fnType', function(value, element) {
		return /^\d{10}$/.test(value.replace(/[()\s+-]/g,''));
		},'Enter Valid  phone number');*/
		
		jQuery.validator.addMethod("phoneno", function(value, element) {
        return this.optional(element) || /^[0-9+() ]*$/.test(value);
        }, "Enter Valid  phone number."); 
		
		
		
		$('#category_form').validate({
		
           rules: {
			client_id:{
				required:true,
			},
			category_name: {
                required: true,
			},
			
			category_email:{
				required:true,
				email: true,
				<?php
				if($category_id=="")
				{
				?>
				remote: {
				url: "<?php echo base_url();?>admin/category/checkexistemail",
				type: "post"
				},
				<?php
				}
				?>	 
				 
			 },
			status: {
                required: true,
			}
			
			
		},
		 messages: {
		 
		   category_email:{
		      remote:"This email already exists"
		     }
			
			
        },
				submitHandler: function(form) {
		
		       //alert("koko");
		
		        var category_id=$('#category_id').val();
				var client_id=$('#client_id').val();
			    var category_name=$('#category_name').val();
			    var category_email=$('#category_email').val();
				var status=$('#status').val();
				
					
				//alert("<?php echo base_url();?>admin/category/insert_category?&category_id="+category_id+"&client_id="+client_id+"&category_name="+category_name+"&category_email="+category_email+"&status="+status);	
					
				$.ajax({
				method:'post',
				url: "<?php echo base_url();?>admin/category/insert_category", 
				data:"&category_id="+category_id+"&client_id="+client_id+"&category_name="+category_name+"&category_email="+category_email+"&status="+status,
				success: function(result){
				if(result)
				{
				    //alert(result);
					
					//alert("koko");
					
					var val=$.trim(result);
					//alert(val);
					
					$("#success_message").show();
				  setTimeout(function(){setInterval(function(){
				  //alert("ppp");
				  parent.$.fancybox.close();
				  
				  }, 3000)}, 3000);
				  
				  
					
					parent.$.fancybox.close();
					
					//alert("fofo");
					//alert("koko");
					
					parent.$('#example1').dataTable().fnStandingRedraw();
					
					
				  
					
				
				  
				  
				 
				
				}
				}
				});
					
					
					
					
					
						
							
					
					   
		
		} 
		 
		
		});
		
		
		
	$('#User_password_form').validate({
        rules: {
			user_change_password:{
				
				required:true
			},
			
			 user_change_cpassword: {
				required:true,
                equalTo: "#user_change_password"              
            }
			
        },
		 messages: {
          
			user_change_cpassword: {
            	equalTo:"password not match"
			}
			
        },
		submitHandler: function(form) {
		        var category_id=$('#category_id').val();
				var user_password=$('#user_change_password').val();
				
				
				
				
					
					
				//alert("<?php echo base_url();?>user/addUser?&category_id="+category_id+"&UserName="+UserName+"&UserComments="+UserComments+"&UserIsActive="+UserIsActive);	
					
				$.ajax({
				method:'post',
				url: "<?php echo base_url();?>admin/user/update_change_password", 
				data:"&category_id="+category_id+"&user_password="+user_password,
				success: function(result){
				if(result)
				{
				// alert(result);
					
					
					
					var val=$.trim(result);
					
					
					$("#success_message").show();
				  setTimeout(function(){setInterval(function(){
				  //alert("ppp");
				  parent.$.fancybox.close();
				  
				  }, 3000)}, 3000);
					
					//alert(val);
					parent.$.fancybox.close();
					parent.$('#example1').dataTable().fnStandingRedraw();
					
				
				  
				  
				 
				
				}
				}
				});
				
				
				
		 },
    });
	
		
		
		
		
	});	
</script>   
<script>
 $(document).ready(function() {
 //alert("popo");
	  /* $('#example-getting-started-input').strengthMeter('text', {
            container: $('#example-getting-started-text')
        });
        $('#example-tooltip').strengthMeter('tooltip');*/
		/*$('#user_passowrd').strengthMeter('progressBar', {
            container: $('#example-progress-bar-container')
        });*/
    });
</script>
<script type="text/javascript" src="<?php echo base_url();?>theme/assets/source/jquery.fancybox.js?v=2.1.5"></script>
<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>theme/assets/source/jquery.fancybox.css?v=2.1.5" media="screen" />				