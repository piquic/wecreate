jQuery(function ($) {
	   $('.content-wrapper').css('min-height','100');
	//alert('mngj');
	$('#prabhss').validate({
		rules: {
			business_name: {
				required: true
			},
		   name: {
				required: true
			},
			email: {
				required: true,
				email: true,
				remote: {
					url: "admin/Customer/checkemaill",
					type: "post"
				 }
			},
			password:{
				 required: true
			},
			gender:{
				 required: true
			},
			cnfrmpassword:{
				 required: true,
				 equalTo: "#password"
			},
			 address: {
				required: true
			},
			 mobile: {
				required: true,
				  digits: true,
				  maxlength: 10
			},
			 city: {
				required: true
			},
			 state: {
				required: true
			},
			 country: {
				required: true
			}

		},
		messages: {

			mobile: {
				digits: "This field can only contain numbers.",
				maxlength: "this must be 10 digit number."

			},
			email:{
				remote:"This email already exists"
			}

		},
	});
});