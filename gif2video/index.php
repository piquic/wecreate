

	<!doctype html>
	<html lang="en">
	<head>
		<!-- Required meta tags -->
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

		<!-- Bootstrap CSS -->
		<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css" integrity="sha384-9aIt2nRpC12Uk9gS9baDl411NQApFmC26EwAOH8WgZl5MYYxFfc+NcPb1dKGj7Sk" crossorigin="anonymous">

		<title>GIF to VIDEO</title>
	</head>
	<body>
		<header class="bg-light border-bottom">
			<div class="container-fluid">
				<div class="row">
					<div class="col-12 col-sm-12 col-md-12">
						<nav class="navbar navbar-expand-md navbar-light bg-light text-center">

							<?php
								$base_url = "http://collabtest.piquic.com/";
								$header_logo = $base_url."website-assets/images/logo.png";
							?>
							<a class="navbar-brand" href="<?php echo $base_url;?>dashboard">
								<div class="d-flex justify-content-around" style="height: 50px; width: 100px; text-align: center; background: url(<?php echo $header_logo;?>) no-repeat center; background-size: contain;">
								</div>
							</a>
							<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
								<span class="navbar-toggler-icon"></span>
							</button>

							
							<div class="collapse navbar-collapse" id="navbarSupportedContent">
								<ul class="navbar-nav mr-auto">
									<li class="nav-item pl-1 pr-2">
										<a class="nav-link nl lead <?php if($class_name == 'dashboard'){ echo "tn active"; } else { echo "text-dark"; } ?>" href="<?php echo $base_url;?>dashboard" target="_blank">Dashboard</a>
									</li>
								</ul>
							</div>
						</nav>
					</div>
				</div>
			</div>
		</header>

		<div class="container">
			<div class="row">
				<div class="col-12 col-sm-12 col-md-1">&nbsp;</div>
				<div class="col-12 col-sm-12 col-md-4 p-2">
					<div class="card">
						<div class="card-header">
							<h5>GIF</h5>
						</div>
						<div class="card-body">
							<form action="" class="text-center border border-light p-5" method="post" enctype="multipart/form-data">
								Select image to upload:
								<input type="file" class="form-control" name="fileToUpload" id="fileToUpload">
								<input type="submit" class="btn btn-outline-info btn-rounded btn-block my-4 waves-effect z-depth-0" value="Upload Image" name="submit">
							</form>
						</div>
					</div>
				</div>
				<div class="col-12 col-sm-12 col-md-2">
					<div style="display: table; text-align: center; height: 320px; width: 100%;">
						<div style="display: table-cell; vertical-align: middle;">
							<h5>To</h5>
						</div>
					</div>
				</div>
				<div class="col-12 col-sm-12 col-md-4 p-2">
					<div class="card">
						<div class="card-header">
							<h5>Video</h5>
						</div>
						<div class="card-body">
							<div class="p-1">
								<?php
								// Check if image file is a actual image or fake image
								if(isset($_POST["submit"])) {
									//$target_dir = "uploads/";
									$input="uploads/".basename($_FILES["fileToUpload"]["name"]);

									if(move_uploaded_file($_FILES["fileToUpload"]["tmp_name"], $input))
									{
										$output="uploads/".time().".mp4";
										exec("ffmpeg.exe"); // load ffmpeg.exe
										exec("ffmpeg -i $input -an $output");
										// echo "Image Converted Success";
									}
								?>

								<video width="320" height="240" controls autoplay>
									<source src="<?php echo $output ?>" type="video/mp4">
									Your browser does not support the video tag.
								</video>

								<?php } ?>



								<video width="320" height="240" controls autoplay>
									<source src="uploads/rikkkk_1597232974.mp4" type="video/mp4">
									Your browser does not support the video tag.
								</video>
							</div>
						</div>
					</div>
				</div>
				<div class="col-12 col-sm-12 col-md-1">&nbsp;</div>
			</div>
		</div>

		<!-- Optional JavaScript -->
		<!-- jQuery first, then Popper.js, then Bootstrap JS -->
		<script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
		<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
		<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.min.js" integrity="sha384-OgVRvuATP1z7JjHLkuOU7Xw704+h835Lr+6QL9UvYjZE3Ipu6Tp75j7Bh/kR0JKI" crossorigin="anonymous"></script>
	</body>
</html>

<?php /*<!DOCTYPE html>
<html>
<? //php include("../application/views/front/includes/header.php"); ?>
<link href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.5.0/css/bootstrap.min.css" rel="stylesheet">
<body>
<div class="card">

  <h5 class="card-header info-color white-text text-center py-4">
    <strong>GIF to Video Convertion</strong>
  </h5>

  <!--Card content-->
  <div class="card-body px-lg-5 pt-0">

<form action="" class="text-center border border-light p-5" method="post" enctype="multipart/form-data">
  Select image to upload:
  <input type="file" name="fileToUpload" id="fileToUpload">
  <input type="submit" class="btn btn-outline-info btn-rounded btn-block my-4 waves-effect z-depth-0" value="Upload Image" name="submit">
</form>

</div>
</div>

</body>
</html>*/?>
